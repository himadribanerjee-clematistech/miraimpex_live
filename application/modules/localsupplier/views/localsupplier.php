<style>
    .partnerImg{border-radius: 6%;object-fit: cover;height:250px; margin:10px;}

    nav.navbar.navbar-default.navbar-for-mobile{
        background-color: rgb(103, 65, 114) !important;
        margin-bottom: 0px;
    }


    @media only screen and (max-width: 768px) {
        .blue .product-banner > .banner-link a {
    color: #fff;
    padding: 10px;
    background: rgba(10, 10, 10, 0.5803921568627451) !important;
    -webkit-appearance: none !important;
}
    }
</style>
<section class="logistics_pase safe_pay bck" style="background-image: url(<?php echo base_url();?>assets/images/new_banner/localsupplierpage.jpg);"> 
    <div class="container" style="height:540px;">
		<div class="row">

        <!-- New nav start -->
        <!-- #dev-isuru 2020/03/27 -->
        <nav class="navbar navbar-default new-nav-bar">
            <div class="container-fluid">
                <div class="navbar-header"><a class="" href="#">
                <img src="<?php echo base_url();?>assets/images/logo_local_supplier.png" style="width: 150px;">
                <a href="#" class="slogan"><span >Digital Transformation In Africa's Retail Industry</span></a> </li>
                </a><button data-toggle="collapse" data-target="#navcol-1" class="navbar-toggle collapsed">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>

            <div class="collapse navbar-collapse nab-bg" id="navcol-1">
                    <ul class="nav navbar-nav navbar-left nav-l">
                        <li role="presentation"> <a href="#"><span >Digital Transformation In Africa's Retail Industry</span></a> </li>                        
                    </ul>
                    <ul class="nav navbar-nav navbar-right nav-r">                       
                        <li role="presentation"><a  href="<?php echo base_url();?>">Home</a></li>
                        <li role="presentation"><a id="localSupplierLoginProfile" href="<?php echo base_url();?>profile">My Profile </a></li>                        
                    </ul>
            </div>
            </div>
        </nav>

        <!-- new nav end -->



			<!-- <div class="col-sm-8">
                <div class="row">
                    <div class="col-lg-4" style="padding: 0; margin-right: -90px;">
                        <a href="">
                            <img src="<?php echo base_url();?>assets/images/logo_local_supplier.png" style="width: 170px;">
                        </a>
                    </div>
                    <div class="col-lg-8 SMEs_info" style="padding-top: 3%;">
                        <p style="font-size:20px; font-weight:500;">Digital Transformation In Africa's Retail Industry</p>
                    </div>
                </div> -->

				<!-- <div class="logo_logistic">
                    <div style="float: left;">
                        <a href="">
                            <img src="<?php echo base_url();?>assets/images/logo_local_supplier.png">
                        </a>
                    </div>
                    <div>
                        <h4>Digital transformation in Africa's retail Industry</h4>
                    </div>                  
				</div> -->
			<!-- </div>
			<div class="col-sm-4">
				<div class="menu_log text-right">
				   <ul>
                       <li><a href="<?php echo base_url();?>">Home</a></li>
                       <li><a id="localSupplierLoginProfile" href="<?php echo base_url();?>profile">My Profile</a></li>
				   </ul>
			    </div>
			</div>
			
		</div> -->
		<!-- <div class="row">
			<div class="col-sm-6">
				<div class="banner_info">
					<h2>Welcome to Local Supplier Page</h2>
					<h4>Now you can search items locally from Gambia, Senegal, Ivory coast and Ghana</h4>					
				</div>
			</div>
			
		</div> -->
	    
    </div> 

</section>

<div class="  clearfix logis_SMEs_section" style="padding-top: 15px !important;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
            	<div class="SMEs_info">
            		<h2>ABOUT MIRA IMPEX</h2>
            		<P>Mira Impex is exploring Africa’s Fragmented and Informal Markets by introducing retailers to technology in order to join the digital economy, pursue improved revenue stream and new business models.</P>
                    <p>The biggest problem faced by retailers is the fluctuation in prices when ordering stock. This, combined with tracking multiple deliveries from different suppliers, proves to be a logistical burden. Mira Impex gives retailers the convenience of ordering all the products they need at once, where they no longer need to deal with middlemen.</p>
            	</div>
            </div>
        </div>
    </div>
</div>


<div class="  clearfix how_it_work">
    <div class="container">
        <div class="row">
            <div class="col-md-12" id="featuredSubmenuPanel">
            	
            </div>
            
        </div>
    </div>
</div>

<div class="  clearfix logis_SMEs_section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
            	<div class="SMEs_info">
            		<h2>HOW IT WORKS</h2>
            		<P>Retailers can download the MiraImpex app or open our Web-Application and browse the catalogue of goods before placing their orders digitally. Alternatively, they can contact the Mira Impex's customer service team or message them via SMS or on WhatsApp to place an order. Gone are the middlemen that inflate the prices of everyday goods as MiraImpex connects manufacturer and Wholesalers with retailers directly.</P>
            	</div>
            </div>
        </div>
    </div>
</div>

<div class="  clearfix logis_SMEs_section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
            	<div class="SMEs_info">
                    <h2>OUR PARTNERS</h2>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="bg">
                                <img class="partnerImg" src="<?php echo base_url();?>assets/images/logo/partner_l1_lc.jpg" alt="Partners">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="bg">
                                <img class="partnerImg" src="<?php echo base_url();?>assets/images/logo/partner_l2_lc.jpg" alt="Partners">      
                            </div>                            
                        </div>
                        <div class="col-md-3">
                            <div class="bg">
                                <img class="partnerImg" src="<?php echo base_url();?>assets/images/logo/partner_l3_lc.jpg" alt="Partners">
                            </div>                            
                        </div>
                        <div class="col-md-3">
                            <div class="bg">
                                <img class="partnerImg" src="<?php echo base_url();?>assets/images/logo/partner_l4_lc.jpg" alt="Partners">
                            </div>                            
                        </div>
                    </div>
                    
                    
                    
                   
            	</div>
            </div>
        </div>
    </div>
</div>

<div class="know_more">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
            	<div class="know_more_info">
                    <h4>Are you a manufacturer / supplier of FMCG or Building materials looking for a wider reach?</h4>
                    <p> <i class="fa fa-envelope"></i>admin@miraimpextrade.com</p>
                    <p><i class="fa fa-phone"></i>+220 5851200 (call, WhatsApp or SMS)</p>
                    <p><i class="fa fa-phone"></i>Europe: +447763415928</p>
            		<p> <i class="fa fa-envelope"></i>info@miraimpextrade .com</p>
            	</div>
            </div>
         </div>
    </div>
</div>



<!-- --------------------footer-------------- -->
<div class="logistics_footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
            	<a href="#"><img src="<?php echo base_url();?>assets/images/logo.png"></a>
            </div>
            <div class=" col-sm-6 sl-block text-right">
                <h4>Follow us</h4>
                <div>
                    <a href="#"><img src="<?php echo base_url();?>assets/images/i-1.png" alt=""/></a>
                    <a href="#"><img src="<?php echo base_url();?>assets/images/i-2.png" alt=""/></a>
                    <a href="#"><img src="<?php echo base_url();?>assets/images/i-3.png" alt=""/></a>
                    <a href="#"><img src="<?php echo base_url();?>assets/images/i-4.png" alt=""/></a>
                    <a href="#"><img src="<?php echo base_url();?>assets/images/i-5.png" alt=""/></a>
                </div>  
                <div class="sub_menu_logis">
                	<a href="#">Contact us</a>
                	<a href="#">User Terms and Privacy Notice</a>
                </div>
            </div>
            <div class="col-sm-12 text-cenrter">
            	<div class="copy"><p>© 2020 mira impex. All Rights Reserved. Designed by <a href="#">clematistech.com</a></p></div>
                 
             </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/custom_js/localsupplier.js?version=<?php echo time(); ?>"></script>