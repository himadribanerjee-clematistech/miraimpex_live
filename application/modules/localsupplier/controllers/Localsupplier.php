<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Localsupplier extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		error_reporting('E_ALL^E_NOTICE');
		$this->load->model("category_model","catModel");
	}

	public function index(){
		//$parent_id = base64_decode($parent_id);
		//echo $parent_id;
		//die;
		$data['lang'] = $this->session->userdata('lang');
		$data['view_file'] = 'localsupplier';
		customview($data);
		
	}

	public function view($id = NULL){		
		
		$parent_id = base64_decode($id);
		$get_parent_mapping = $this->catModel->getParentMapping($parent_id);
		
		if(is_array($get_parent_mapping) && count($get_parent_mapping)>0)
		{
			foreach($get_parent_mapping as $k=>$v)
			{
				$parentMappingArr = json_decode($v,true);
				$mappingArr[$k]['id'] = $parentMappingArr['id'];
				$mappingArr[$k]['parent_id'] = $parentMappingArr['parent_id'];
				if($this->session->userdata('lang') == 'en')
				{
					$mappingArr[$k]['category_name'] = $parentMappingArr['category_name_en'];
				}elseif($this->session->userdata('lang') == 'fr')
				{
					$mappingArr[$k]['category_name'] = $parentMappingArr['category_name_fr'];
				}else{
					$mappingArr[$k]['category_name'] = $parentMappingArr['category_name_en'];
				}
				
			}
		}	
		
		$data['mappingArr'] = $mappingArr;
			
		$data['cat_id'] = $id;
		$data['lang'] = $this->session->userdata('lang');
		$data['view_file'] = 'viewcategory';
        view($data);
	}

	public function all($type = NULL){
		
		$category = $this->catModel->getParentMappingOneQuery($type);
		
		$catHtml = $this->catModel->buildCategory(0, $category);
		if($type == 'products')
		{
			$data['typeToken'] = 1;
		}elseif($type == 'services')
		{
			$data['typeToken'] = 2;

		}else{
			
		}
		$data['catHtml'] = $catHtml;
		$data['type'] = $type;
		$data['lang'] = $this->session->userdata('lang');
		$data['view_file'] = 'categorycatalogue';
        view($data);
	}	
	
}