<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Requestforquotations_Model extends CI_Model{

	public function __construct() {
        parent::__construct();
    }
    
    public function buildCategory($parent, $category,$ik) {
		$html = "";
		
		if (isset($category['parent_cats'][$parent])) {	
            if($ik == 1)
            {
                $html .= "<ul id=\"rfqnav\">\n";
            }	else{
                $html .= "<ul class=\"subul\">\n";
            }
            
            if($parent == 0)
            {
                $boldS = "<p style=\"font-weight:bold; padding:0;margin:0;color:#6e6e6e;\">";
                $boldE = "</p>";
                $rootanchor = "ancroot";
            }else{
                $boldS = "";
                $boldE = "";
                $rootanchor = "anc";
            }
			
			foreach ($category['parent_cats'][$parent] as $cat_id) {
				if (!isset($category['parent_cats'][$cat_id])) {

                    $html .= "<li class=\"list\">\n<a href=\"javascript:void(0)\" class=\"$rootanchor\"><div class=\"radio\"><label><input type=\"radio\" name=\"cat_id\" value=\"" . base64_encode($category['categories'][$cat_id]['category_id']) . "\">" .$boldS.$category['categories'][$cat_id]['category name'].$boldE. "</label></div></a></li> \n";

									}
				if (isset($category['parent_cats'][$cat_id])) {
                    
                   
                   $html .= "<li class=\"list\">\n<a href=\"javascript:void(0)\" class=\"$rootanchor\"><div class=\"radio\"><label><input type=\"radio\" name=\"cat_id\" value=\"" . base64_encode($category['categories'][$cat_id]['category_id']) . "\">" .$boldS. $category['categories'][$cat_id]['category name'] .$boldE. "</label></div></a> \n";
                    

					$html .= $this->buildCategory($cat_id, $category,0);
					$html .= "</li> \n";
				}
			}
			$html .= "</ul> \n";
		}
		return $html;
    }
    

    public function getParentMappingOneQuery($type)
	{
		/* if($type == 'products')
		{
			$typeSql = "WHERE `cat`.`type` = '1'";
		}elseif($type == 'services')
		{
			$typeSql = "WHERE `cat`.`type` = '2'";

		}else{
			$typeSql = "";
        } */
        
        $typeSql = "";

		if($this->session->userdata('lang') == 'en')
		{
			$catSql = "`cat`.`category_name_en` AS 'category name',";
		}elseif($this->session->userdata('lang') == 'fr')
		{
			$catSql = "`cat`.`category_name_fr` AS 'category name',";
		}else{
			$catSql = "`cat`.`category_name_en` AS 'category name',";
		}
		
		$sql = "SELECT
					$catSql
					`cat`.`id` AS 'category_id',
					`cat2`.`category_name_en` AS 'parent category',
					`cat2`.`id` AS 'parent_category_id'
				FROM
					`t_category` AS `cat`
				LEFT JOIN `t_category` AS `cat2` ON `cat`.`parent_id` = `cat2`.`id`
				$typeSql
				AND `cat`.`active`='1' AND `cat`.`display`='1'
				ORDER BY
					'parent category'";
		

		$query = $this->db->query($sql);

		//create a multidimensional array to hold a list of category and parent category
		$category = array(
			'categories' => array(),
			'parent_cats' => array()
		);

		//build the array lists with data from the category table
		foreach($query->result_array() as $row){
			//creates entry into categories array with current category id ie. $categories['categories'][1]
			$category['categories'][$row['category_id']] = $row;
			if($row['parent_category_id'] > 0)
			{
				//creates entry into parent_cats array. parent_cats array contains a list of all categories with children
			$category['parent_cats'][$row['parent_category_id']][] = $row['category_id'];
			}else{
				$category['parent_cats'][0][] = $row['category_id'];
			}
			
		}
		

		

		return $category;
	}

}