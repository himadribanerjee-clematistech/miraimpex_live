<style>
	.open {
    display:block;
}
</style>
<section class="rfq_page">
	<div class="container">
		<div class="row group">
			<div class="col-sm-8">
				<div class="rfq_header">
					<h2>Tell suppliers what you need</h2>
					<h4>Complete Your RFQ</h4>
					<p>The more specific your information, the more accurately we can match your request to the right suppliers</p>
				</div>
			
				<form action="javascript:void(0);" method="post" id="rfqquoteForm">
						<div class="rfq_form_design">
            <input type="hidden" name="rfq_cat_id" id="rfq_cat_id" value="">
							<input type="text" class="txt_fails moo" placeholder="Key words of products you are looking for" name="rfqName" value="<?php echo $rfqName; ?>" autocomplete="off">
							
						</div>

						<div class="rfq_form_design">
							<p class="txt_fails click_here" id="displaycatBreadcumb">Please select a category</p>
							
							<div class="show_panel">

								<input type="text" class="txt_fails moo" id="catKeyword" placeholder="Key words of category you are looking for" name="catKeyword" value="" autocomplete="off">
                <div class="loading_txt d-none" id="catSearchloading"><img src="<?php echo base_url('assets/images/loader.gif'); ?>"/></div>
								<h4 id="rfqcatsrchHeading" class="d-none">Recommended Categories</h4>
								<ul id="rfqcatsrchList">
								</ul>
								<p class="d-none" id="notsatisfied">Not satisfied with the Recommended Categories? <a href="javascript:void(0);" onclick="javascript:openmanualcat();">Choose  Your own category here</a></p>
							 </div>
						</div>

						

						<?php
						  if(isset($catHtml))
						  {
						?>
							<div id="catSelectManual" class="rfq_form_design d-none">
								<p class="category_title">Select category:</p>
							<?php echo $catHtml; ?>
							</div>
						<?php	
						  }
						?>

						
						
						<div class="rfq_form_design">
							<input type="text" class="txt_fails moo" placeholder="Enter quantity" name="quantity" id="qty" value="<?php echo $rfqQuantity; ?>">
							<select class="txt_fails quantity_value moo" id="uom" name="uom_id">
								
							</select>
						</div>
						<div class="rfq_form_design">
							<textarea class="txt_fails moo" placeholder="To receive fast and accurate quotations from suitable suppliers, please present your supplier requirements as specifically as possible. " rows="8" name="message" id="description"></textarea>
						</div>
						<div class="rfq_form_design">
						<p class="category_title">Attachments like product pictures/images would improve your RFQ:</p>
						<div class="md-form" style="padding-top: 10px;">
                                     <input type="file" name="image[]" id="rfq-image-add" accept="image/x-png,image/gif,image/jpeg" value="Upload Attachments">
                                    </div>
                                    <div class="md-form">
                                        <div id="rfqimgdv" class="gallery"></div>
                                    </div>
						</div>

						<div class="rfq_form_design c_gap">
							<textarea name="other_requerment" class="txt_fails" rows="8" placeholder="Other Requirements [Optional]"></textarea>
						</div>

						<div class="rfq_form_design c_gap">
							<span class="iconpointarea"><i class="fa fa-map-marker fa-lg" aria-hidden="true"></i></span>

                        <input type="text" id="rfqLocationAddress" name="rfqLocation" class="form-control inputpointarea " placeholder="Destination Port"> 
						      <input type="hidden" id="rfqLocationLat" name="lat" value=""> 
                  		<input type="hidden" id="rfqLocationLang" name="long" value="">

                  		<div class="clear"></div>
                       </div>

						<div class="rfq_form_design">
							<label class="txt_checkbox"><input type="checkbox" id="tnc"><span> I have read, understood and agreed to abide by Terms and Conditions Governing RFQ</span></label>
						</div>
						<div class="rfq_form_design">
							<button type="submit" class="btn_rfq" id="rfqsubmit">Request For Quotation</button>
						</div>
					</form>
					<div class="rfq_footer">
					<h2>How to Get Quotes quickly?</h2>
					<ul>
						<li><p>Submit RFQ <i class="fa fa-long-arrow-right" aria-hidden="true"></i> </p></li>
						<li><p>Compare Quotes <i class="fa fa-long-arrow-right" aria-hidden="true"></i> </p></li>
						<li><p>Contact Supplier</p></li>
					</ul>
				</div>
				</div>
				<div class="col-sm-4">
					<div class="rfq_right_cover">
						<div class="rfq_header">
							<h2>Request For Quotation</h2>
							<p>One Request, Multiple Quotes</p>
					    </div>
					    <div>
					    	<h4>Completeness of Information</h4>
	                                <ul class="progress-indicator custom-complex">

	                                    <li id="one" class="" style="padding: 0">
	                                        <span class="bubble"></span>
	                                        <i class="fa fa-check-circle"></i>
	                                        Fair
	                                    </li>
	                                    <li id="two" class="" style="padding: 0">
	                                        <span class="bubble"></span>
	                                        <i class="fa fa-check-circle"></i>
	                                        Great
	                                    </li>
	                                    <li id="three" class="" style="padding: 0">
	                                        <span class="bubble"></span>
	                                        <i class="fa fa-check-circle"></i>
	                                        Excellent
	                                    </li>           
	                                </ul>
                      <p>A buying request with higher completeness can get higher quality and more quotations.</p>            
	                    </div>
                    </div>

				</div>
			
		</div>
	</div>
</section>
<script src="<?php echo base_url(); ?>assets/custom_js/requestforquotation.js?version=<?php echo time(); ?>"></script>