<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Supplier extends CI_Controller {

	protected $token_id='';

    public function __construct() {
        parent::__construct(); 
		$this->load->model("category_model","catModel");
        error_reporting('E_ALL^E_NOTICE');
    }


    public function index() {} 

    public function view($catId) {

		$data['cat_id'] = $catId;
        $catId = base64_decode($catId);

        $get_parent_mapping = $this->catModel->getParentMapping($catId);
		
		if(is_array($get_parent_mapping) && count($get_parent_mapping)>0)
		{
			foreach($get_parent_mapping as $k=>$v)
			{
				$parentMappingArr = json_decode($v,true);
				$mappingArr[$k]['id'] = $parentMappingArr['id'];
				$mappingArr[$k]['parent_id'] = $parentMappingArr['parent_id'];
				if($this->session->userdata('lang') == 'en')
				{
					$mappingArr[$k]['category_name'] = $parentMappingArr['category_name_en'];
				}elseif($this->session->userdata('lang') == 'fr')
				{
					$mappingArr[$k]['category_name'] = $parentMappingArr['category_name_fr'];
				}else{
					$mappingArr[$k]['category_name'] = $parentMappingArr['category_name_en'];
				}
				
			}
		}
        
        $data['mappingArr'] = $mappingArr;
			
		
        $data['lang'] = $this->session->userdata('lang');
        
		
        $data['view_file'] = 'SupplierByCategory';
		// print_r($data);
        view($data);
    }

    public function details($slug) {

        $this->load->helper('url');
        $currentURL = current_url();
        $params   = $_SERVER['QUERY_STRING'];
        parse_str($params, $params_array);

		$data['seller_id'] =$slug;
		
        $data['view_file'] = 'SupplierDetails';
		// print_r($data);
        view($data);
    }


    
    

    
}