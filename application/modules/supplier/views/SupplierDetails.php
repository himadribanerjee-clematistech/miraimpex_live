<div class="wrapper-main brandshop clearfix supplier_details">
        	<div class="spacer15"></div><!--spacer-->
        	<div class="container">
            	<div class="inner-block"><!------Main Inner-------->
                	<div class="row">
                		<div class="col-md-3 col-sm-4 menu_sec">
	                       	<div class="main-contant details-view details_view_contact ">
	                       		<ul class="supp_menu">
	                       			<li><a href="#">
	                       				<img src="<?php echo base_url();?>assets/images/all_icon/hotel.png">
	                       				<span> Company Overview </span>
	                       			</a>
	                       			</li>
	                       			<li><a href="#">
	                       				<img src="<?php echo base_url();?>assets/images/all_icon/box.png">
	                       				<span>Products</span>
	                       			</a>
	                       			</li>
	                       		</ul>
	                       	</div>
	                       
                       </div>
                        <div class="col-md-9 col-sm-8">
                        	<div class="main-contant xs-spacer20 clearfix">
                            	<div class="contant-wrapper">
                                    <div class="details-view"><!-- Start Product Details -->
                                    	<div class="products_heading"><h4 id="companyDetails_SuppName">Gentella</h4></div>
                                        <div class="clearfix">
                                            <div class="product-img"><!-- Product Images -->
                                                <div id="info-img">
													<!---
                                                    <span class="pro offer">30% off</span>
                                                    --->
                                                    <div class="swiper-container top-img">
                                                        <div class="swiper-wrapper">

															  <section id="default" class="padding-top0">
																    <div>

																      <div class="large-6 column">
																       <div class="owl-carousel supplier_details_slg_sld" id="supplierGalleryPanel">
									                                    							                                    
									                                    
									                                 </div>      
																      </div>
																      <div class="large-6 column"></div>
																    </div>
																    </section>
                                                        </div>
                                                        
                                                    </div>
                                                    
                                                </div>
                                            </div><!-- End Product Images -->
                                            <div class="product-info">
                                                <h4 id="productName"></h4>
                                                
                                                <div class="attribute clearfix Customization_info">
												<div class="custo_item">
                                                		<div class="custo_heading"><p>Contact Name:</p></div>
                                                		<div class="custo_value"><p id="companyDetails_SuppContName">Demo address</p></div>
                                                	</div>
													<div class="custo_item">
                                                		<div class="custo_heading"><p>Contact No:</p></div>
                                                		<div class="custo_value"><p id="companyDetails_SuppContNo">Demo address</p></div>
                                                	</div>	
                                                	<div class="custo_item">
                                                		<div class="custo_heading"><p>Address:</p></div>
                                                		<div class="custo_value"><p id="companyDetails_Address">Demo address</p></div>
                                                	</div>                                                	
                                                	<div class="custo_item">
                                                		<div class="custo_heading"><p>City:</p></div>
                                                		<div class="custo_value"><p id="companyDetails_City">demo city.</p></div>
                                                	</div>
													<div class="custo_item">
                                                		<div class="custo_heading"><p>State:</p></div>
                                                		<div class="custo_value"><p id="companyDetails_State">demo state.</p></div>
                                                	</div>
													<div class="custo_item">
                                                		<div class="custo_heading"><p>Country:</p></div>
                                                		<div class="custo_value"><p id="companyDetails_Country">demo country.</p></div>
                                                	</div>
                                                </div>
                                                
                                            </div>

                                            <div class="main_pro_services" style="display:none;">
	                                            <div class="col-md-6 main_pro_list">
	                                            	<div class="products_heading"><h4>Main Product</h4></div>

	                                            	<div class="panel-group wrap" id="accordion" role="tablist" aria-multiselectable="true">

												      <div class="panel">
												        <div class="panel-heading" role="tab" id="headingOne">
												          <h4 class="panel-title">
												        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#Diary" aria-expanded="false" class="collapsed" aria-controls="Diary">
												          Diary
												        </a>
												      </h4>
												        </div>
												        <div id="Diary" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
												          <div class="panel-body">
												            <ul class="sub_menu">
												            	<li><a href="#">Diary</a></li>
												            	<li><a href="#">Live Animals</a></li>
												            	<li><a href="#">Honey</a></li>
												            	<li><a href="#">Hiring of Labor</a></li>
												            	<li><a href="#">Grains & Legumes</a></li>
												            </ul>
												          </div>
												        </div>
												      </div>
												      <!-- end of panel -->

												      <div class="panel">
												        <div class="panel-heading" role="tab" id="headingTwo">
												          <h4 class="panel-title">
												        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#Animals" aria-expanded="false" aria-controls="Animals">
												          Live Animals
												        </a>
												      </h4>
												        </div>
												        <div id="Animals" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
												          <div class="panel-body">
												            <ul class="sub_menu">
												            	<li><a href="#">Diary</a></li>
												            	<li><a href="#">Live Animals</a></li>
												            	<li><a href="#">Honey</a></li>
												            	<li><a href="#">Hiring of Labor</a></li>
												            	<li><a href="#">Grains & Legumes</a></li>
												            </ul>
												          </div>
												        </div>
												      </div>
												      <!-- end of panel -->

												      <div class="panel">
												        <div class="panel-heading" role="tab" id="headingThree">
												          <h4 class="panel-title">
												        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#Honey" aria-expanded="false" aria-controls="Honey">
												          Honey
												        </a>
												      </h4>
												        </div>
												        <div id="Honey" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
												          <div class="panel-body">
												            <ul class="sub_menu">
												            	<li><a href="#">Diary</a></li>
												            	<li><a href="#">Live Animals</a></li>
												            	<li><a href="#">Honey</a></li>
												            	<li><a href="#">Hiring of Labor</a></li>
												            	<li><a href="#">Grains & Legumes</a></li>
												            </ul>
												          </div>
												        </div>
												      </div>
												      <!-- end of panel -->

												      <div class="panel">
												        <div class="panel-heading" role="tab" id="headingFour">
												          <h4 class="panel-title">
												        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#Seeds" aria-expanded="false" aria-controls="Seeds">
												          Seeds
												        </a>
												      </h4>
												        </div>
												        <div id="Seeds" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
												          <div class="panel-body">
												            <ul class="sub_menu">
												            	<li><a href="#">Diary</a></li>
												            	<li><a href="#">Live Animals</a></li>
												            	<li><a href="#">Honey</a></li>
												            	<li><a href="#">Hiring of Labor</a></li>
												            	<li><a href="#">Grains & Legumes</a></li>
												            </ul>
												          </div>
												        </div>
												      </div>
												      <!-- end of panel -->

												    </div>
	                                            </div>
	                                            <div class="col-md-6 main_pro_list">
	                                            	<div class="products_heading"><h4>Main Services</h4></div>
	                                            	<div class="panel-group wrap" id="accordion" role="tablist" aria-multiselectable="true">

												      <div class="panel">
												        <div class="panel-heading" role="tab" id="headingOne">
												          <h4 class="panel-title">
												        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" class="collapsed" aria-controls="collapseOne">
												          Diary
												        </a>
												      </h4>
												        </div>
												        <div id="collapseOne" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">
												          <div class="panel-body">
												            <ul class="sub_menu">
												            	<li><a href="#">Diary</a></li>
												            	<li><a href="#">Live Animals</a></li>
												            	<li><a href="#">Honey</a></li>
												            	<li><a href="#">Hiring of Labor</a></li>
												            	<li><a href="#">Grains & Legumes</a></li>
												            </ul>
												          </div>
												        </div>
												      </div>
												      <!-- end of panel -->

												      <div class="panel">
												        <div class="panel-heading" role="tab" id="headingTwo">
												          <h4 class="panel-title">
												        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
												          Live Animals
												        </a>
												      </h4>
												        </div>
												        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
												          <div class="panel-body">
												            <ul class="sub_menu">
												            	<li><a href="#">Diary</a></li>
												            	<li><a href="#">Live Animals</a></li>
												            	<li><a href="#">Honey</a></li>
												            	<li><a href="#">Hiring of Labor</a></li>
												            	<li><a href="#">Grains & Legumes</a></li>
												            </ul>
												          </div>
												        </div>
												      </div>
												      <!-- end of panel -->

												      <div class="panel">
												        <div class="panel-heading" role="tab" id="headingThree">
												          <h4 class="panel-title">
												        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
												          Honey
												        </a>
												      </h4>
												        </div>
												        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
												          <div class="panel-body">
												            <ul class="sub_menu">
												            	<li><a href="#">Diary</a></li>
												            	<li><a href="#">Live Animals</a></li>
												            	<li><a href="#">Honey</a></li>
												            	<li><a href="#">Hiring of Labor</a></li>
												            	<li><a href="#">Grains & Legumes</a></li>
												            </ul>
												          </div>
												        </div>
												      </div>
												      <!-- end of panel -->

												      <div class="panel">
												        <div class="panel-heading" role="tab" id="headingFour">
												          <h4 class="panel-title">
												        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
												          Seeds
												        </a>
												      </h4>
												        </div>
												        <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
												          <div class="panel-body">
												            <ul class="sub_menu">
												            	<li><a href="#">Diary</a></li>
												            	<li><a href="#">Live Animals</a></li>
												            	<li><a href="#">Honey</a></li>
												            	<li><a href="#">Hiring of Labor</a></li>
												            	<li><a href="#">Grains & Legumes</a></li>
												            </ul>
												          </div>
												        </div>
												      </div>
												      <!-- end of panel -->

												    </div>
	                                            </div>
                                            </div>




                                        </div>
                                    </div><!-- End Product Details -->
                                   

                                    <div class="spacer30"></div><!--spacer-->
                                    <div class="tab-panel clearfix" id="seller_other_items_panel"><!-- Tab -->
                                    	<div class="products_heading"><h4>You may also like</h4></div>
										<div class="owl-carousel product_details_sld" id="seller_other_items">
		                                    
		                                    
		                                    
		                                </div>
                                    </div>
                                    <div class="spacer30"></div><!--spacer-->
                                    <div class="tab-panel clearfix"><!-- Tab -->
                                    	<div class="products_heading"><h4>Supplier's Certificate</h4></div>
                                    	
									    <section id="gallery">
										    <div id="image-gallery">
										      <div class="row" id="sellerCertificatePanel">
										        
										      </div><!-- End row -->
										    </div><!-- End image gallery -->
										  
										</section>
                                    </div>



        							
                            	</div>
                            </div>
                        </div>

                       
                    </div>
                </div>
            </div>
            <div class="spacer30"></div><!--spacer-->


			<input type="hidden" name="seller_id" id="seller_id" value="<?php echo $seller_id; ?>">
			<script src="<?php echo base_url(); ?>assets/custom_js/supplier_details.js?version=<?php echo time(); ?>"></script>			

										