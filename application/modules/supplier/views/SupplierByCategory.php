<input type="hidden" name="enableLocalSupplier" id="enableLocalSupplier" value="0">
<section class="category_list">
	<div class="container">
		<div class="row group_category">
			<div class="col-md-12">
			<div class="bradecap">
					<ul>
						<li><p>You are In: Supplier</p></li>
                        <?php
                           if(is_array($mappingArr) && count($mappingArr)>0)
                           {
                              foreach($mappingArr as $k=>$v)
                              {
                            ?>
                               <li><a href="<?php echo base_url();?>supplier/view/<?php echo base64_encode($v['id']); ?>"><?php echo $v['category_name'] ?></a></li> 
                            <?php      
                              }
                           } 
                        ?>
						<li class="viewsupplier"><a href="<?php echo base_url();?>category/view/<?php echo $cat_id; ?>">View Products</a></li>
					</ul>
				</div>
			</div>
		</div>
		
		<div class="row other_category_item supplier_list">
			<div class="col-md-12">
				<div class="heading back_color">
					<h4>Experienced Suppliers</h4>					
				</div>
				<div class="local_suppliers">
					<div class="custom-control custom-radio custom-control-inline">
						<input type="checkbox" class="custom-control-input" id="localsupplier" name="localsupplier" value="1" onChange="javascript:viewLocalSupplierOnly(this);"><label class="custom-control-label" for="localsupplier">View Local Suppliers</label>
					</div>
	   
				</div>
			</div>
			<div class="col-md-12" id="supplierListsPanel">
			

			</div>
			
			

			<div class="col-md-12 text-center">
            	<div class="know_more">
	            	<div class="know_more_info">
	            		<h4>Are you a supplier? </h4>
	            		<p>Join Miraimpex.com as a verified supplier and connect with buyers around the world any questions,<br> email us at at premimember@miraimpex.com</p>
	            		<p> <a href="#">Join Now !</a></p>
	            	</div>
                </div>
         
			</div>

<div id="supplierListSchema" style="display:none">
<div class="col-sm-4 list_item">				
					<div class="other_cate_item">
					<a thref="<?php echo base_url();?>supplier/details/{SID}" title="View supplier">
						<div class="basic_info">
							<div class="other_cate_item_img"><img tsrc="<?php echo base_url();?>{SIMG}" alt="img-1"/></div>
							<div class="supplier_list_info">
								<h4>{SBN}</h4>
								<p>{SST}</p>
							</div>
						</div>
						<div class="other_info">
						<p><i class="fa fa-user"></i>: {SCN}</p>						
						<p><i class="fa fa-map-marker"></i>: {SLOC}</p>
						<p class="callnowbutton">Call Now: <i class="fa fa-phone"></i>{SCNO}</p>
						</div>
						</a>   
	                    <div class="other_supplier_img">
						{SPIMG}
	                    </div>
						
						<a thref="<?php echo base_url();?>contactsupplier?seller={SID}">
						<div class="other_cate_item_contact"><h5><i class="fa fa-envelope"></i> Send Enquiry</h5></div>
						</a>
					</div>
			    
			</div>					   
</div>			

		</div>
	</div>
</section>

<script type="text/javascript">
$(document).ready(function(){
  getsupplierlists();  
});

function viewLocalSupplierOnly(obj){
	if($('#localsupplier'). is(":checked")){
		$("#enableLocalSupplier").val("1");
	}
	else if($('#localsupplier'). is(":not(:checked)")){
		$("#enableLocalSupplier").val("0");
	}
	getsupplierlists();  
}

function getsupplierlists() {
  var base_url = $("#base_url").val();
  $('#supplierListsPanel').html('<div style="text-align:center"><img src="'+$("#base_url").val()+'assets/images/all_icon/Loading_icon.gif" alt=""></div>');
  	var values = {};
	values.lang = $("#viewLang").val();
	values.localSupplier = $("#enableLocalSupplier").val();
	values.category_id = '<?php echo $cat_id; ?>';

	$.ajax({
		url: base_url + "api/main/getsupplierByCategory",
		data: values,
		beforeSend: function (xhr) {
              /* Authorization header */
              xhr.setRequestHeader("Authorization", localStorage.getItem("miraimpex_token"));
              xhr.setRequestHeader("X-Mobile", "false");
        },
		method: "post",
		success: function (result) {
			console.log(result);
			if (result.status == 200) {
				$('#supplierListsPanel').empty();
				if (result.item.length > 0) {
					$.each(result.item, function (i1, seller) {
						var supplierListSchema = $("#supplierListSchema").html();

						supplierListSchema = supplierListSchema.replace(/{SID}/g, seller.seller_id); 
						if(seller.profile_image)
						{
							supplierListSchema = supplierListSchema.replace(/{SIMG}/g, 'admin/uploads/bussiness_logo/'+seller.profile_image); 
						}else{
							supplierListSchema = supplierListSchema.replace(/{SIMG}/g, 'assets/images/user.png'); 
						}
						
						supplierListSchema = supplierListSchema.replace(/{SBN}/g, seller.business_name); 
						supplierListSchema = supplierListSchema.replace(/{SCN}/g, seller.contact_name); 
						supplierListSchema = supplierListSchema.replace(/{SCNO}/g, seller.contact_number); 
						supplierListSchema = supplierListSchema.replace(/{SST}/g, (seller.sellerCategory == "1") ? "Manufacturers" : (seller.sellerCategory == "2") ? "Importers & Exporters" : (seller.sellerCategory == "3") ? "Commission agents" : (seller.sellerCategory == "4") ? "Local traders" : (seller.sellerCategory == "5") ? "Local producers" : (seller.sellerCategory == "6") ? "Distributors" : (seller.sellerCategory == "7") ? "Wholesalers" : (seller.sellerCategory == "8") ? "Service providers" : "NA"); 
						supplierListSchema = supplierListSchema.replace(/{SLOC}/g, seller.state+', '+seller.country); 
						
						var selleritemimg = '';
						$.each(seller.seller_items, function (si, selleritems) {
							if(si>2)
							{
								return false;
							}
							selleritemimg += '<a href="'+base_url+'product/details/'+selleritems.item_id+'"><img src="'+base_url+'admin/uploads/selleritem/'+selleritems.thumbnail+'" title="View item" alt="Image of '+selleritems.item_name+'"/></a>';
						});

						supplierListSchema = supplierListSchema.replace(/{SPIMG}/g, selleritemimg); 
						supplierListSchema = supplierListSchema.replace(/thref/g, 'href'); 
						supplierListSchema = supplierListSchema.replace(/tsrc/g, 'src'); 
						

						$("#supplierListsPanel").append(supplierListSchema);
						
					});
				} else {
                    // No item found
                    $('#supplierListsPanel').append('<div class=""><h2 style="text-align:center;">No seller yet.</h2></div>');
                }
			
			}else {
                // Something went wrong
            }		

			
		},
        error: function (result) {
            console.log(result.responseJSON.msg);
        }
	});
	
  
  
}

</script>

            