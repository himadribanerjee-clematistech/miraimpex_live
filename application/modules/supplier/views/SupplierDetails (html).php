<!-- <input type="hidden" name="itemID" id="itemID" value="<?php //echo $itemID; ?>"> -->

<!-- <section class="category_list products_page">
	<div class="container">
    <div class="row group_category">
			<div class="col-md-12">
				<div class="bradecap">
					<?php echo $catPath;?>
				</div>
			</div>
		</div>
	</div>
</section> -->

<div class="wrapper-main brandshop clearfix supplier_details">
        	<div class="spacer15"></div><!--spacer-->
        	<div class="container">
            	<div class="inner-block"><!------Main Inner-------->
                	<div class="row">
                		<div class="col-md-3 col-sm-4 menu_sec">
	                       	<div class="main-contant details-view details_view_contact ">
	                       		<ul class="supp_menu">
	                       			<li><a href="#">
	                       				<img src="<?php echo base_url();?>assets/images/all_icon/hotel.png">
	                       				<span> Company Overview </span>
	                       			</a>
	                       			</li>
	                       			<li><a href="#">
	                       				<img src="<?php echo base_url();?>assets/images/all_icon/box.png">
	                       				<span>Products</span>
	                       			</a>
	                       			</li>
	                       		</ul>
	                       	</div>
	                       
                       </div>
                        <div class="col-md-9 col-sm-8">
                        	<div class="main-contant xs-spacer20 clearfix">
                            	<div class="contant-wrapper">
                                    <div class="details-view"><!-- Start Product Details -->
                                    	<div class="products_heading"><h4>Gentella</h4></div>
                                        <div class="clearfix">
                                            <div class="product-img"><!-- Product Images -->
                                                <div id="info-img">
													<!---
                                                    <span class="pro offer">30% off</span>
                                                    --->
                                                    <div class="swiper-container top-img">
                                                        <div class="swiper-wrapper">

															  <section id="default" class="padding-top0">
																    <div>

																      <div class="large-6 column">
																       <div class="owl-carousel supplier_details_slg_sld">
									                                    <div class="item">
									                                    	<div><a href="#"><img src="<?php echo base_url();?>assets/images/product/product5.png" alt="img-1"/></a></div>
									                                        
									                                    </div>
									                                    <div class="item">
									                                    	<div><a href="#"><img src="<?php echo base_url();?>assets/images/product/product5.png" alt="img-1"/></a></div>
									                                        
									                                    </div>
									                                    
									                                    
									                                 </div>      
																      </div>
																      <div class="large-6 column"></div>
																    </div>
																    </section>
                                                        </div>
                                                        
                                                    </div>
                                                    
                                                </div>
                                            </div><!-- End Product Images -->
                                            <div class="product-info">
                                                <h4 id="productName"></h4>
                                                
                                             
                                               <!--  <div  class="attribute clearfix">
                                                    <div class="price-box">
                                                    	<p class="new-price"><span><i class="fa fa-dollar"></i> <span id="price_min"></span> </span></p>
                                                    	<p class="new-price">-</p>
                                                    	<p class="new-price"><span><i class="fa fa-dollar"></i> <span id="price_max"></span></span></p>
                                                    	<p class="new-price">/ Piece | 1 Piece/Pieces (Min. Order)</p>
                                                    
                                                    </div>
	                                                <div><fieldset class="attribute_fieldset"> 
	                                                        <label class="attribute_label">Size:</label>
	                                                        <div class="attribute_list"> 
	                                                            <ul class="attribute_size">
	                                                                <li class="active"><a href="#">S</a></li>
	                                                                <li><a href="#">M</a></li>
	                                                                <li><a href="#">L</a></li>
	                                                                <li><a href="#">XS</a></li>
	                                                                <li><a href="#">XL</a></li>
	                                                            </ul>
	                                                        </div>
	                                                    </fieldset></div>


		                                                <div><fieldset class="attribute_fieldset"> 
		                                                        <label class="attribute_label">Color:</label>
		                                                        <div class="attribute_list"> 
		                                                            <ul class="attribute_color">
		                                                                <li class="active"><a href="#" class="_1"></a></li>
		                                                                <li><a href="#" class="_2"></a></li>
		                                                                <li><a href="#" class="_3"></a></li>
		                                                                <li><a href="#" class="_4"></a></li>
		                                                            </ul>
		                                                        </div>
		                                                    </fieldset></div>
                                                    
                                                
                                                </div> -->
                                                <div class="attribute clearfix Customization_info">
                                                	<div class="custo_item">
                                                		<div class="custo_heading"><p>Address:</p></div>
                                                		<div class="custo_value"><p>Demo address</p></div>
                                                	</div>
                                                	<div class="custo_item">
                                                		<div class="custo_heading"><p>State:</p></div>
                                                		<div class="custo_value"><p>demo state.</p></div>
                                                	</div>
                                                	<div class="custo_item">
                                                		<div class="custo_heading"><p>City:</p></div>
                                                		<div class="custo_value"><p>demo city.</p></div>
                                                	</div>
                                                </div>
                                                
                                            </div>

                                            <div class="main_pro_services">
	                                            <div class="col-md-6 main_pro_list">
	                                            	<div class="products_heading"><h4>Main Product</h4></div>

	                                            	<div class="panel-group wrap" id="accordion" role="tablist" aria-multiselectable="true">

												      <div class="panel">
												        <div class="panel-heading" role="tab" id="headingOne">
												          <h4 class="panel-title">
												        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#Diary" aria-expanded="false" class="collapsed" aria-controls="Diary">
												          Diary
												        </a>
												      </h4>
												        </div>
												        <div id="Diary" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
												          <div class="panel-body">
												            <ul class="sub_menu">
												            	<li><a href="#">Diary</a></li>
												            	<li><a href="#">Live Animals</a></li>
												            	<li><a href="#">Honey</a></li>
												            	<li><a href="#">Hiring of Labor</a></li>
												            	<li><a href="#">Grains & Legumes</a></li>
												            </ul>
												          </div>
												        </div>
												      </div>
												      <!-- end of panel -->

												      <div class="panel">
												        <div class="panel-heading" role="tab" id="headingTwo">
												          <h4 class="panel-title">
												        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#Animals" aria-expanded="false" aria-controls="Animals">
												          Live Animals
												        </a>
												      </h4>
												        </div>
												        <div id="Animals" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
												          <div class="panel-body">
												            <ul class="sub_menu">
												            	<li><a href="#">Diary</a></li>
												            	<li><a href="#">Live Animals</a></li>
												            	<li><a href="#">Honey</a></li>
												            	<li><a href="#">Hiring of Labor</a></li>
												            	<li><a href="#">Grains & Legumes</a></li>
												            </ul>
												          </div>
												        </div>
												      </div>
												      <!-- end of panel -->

												      <div class="panel">
												        <div class="panel-heading" role="tab" id="headingThree">
												          <h4 class="panel-title">
												        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#Honey" aria-expanded="false" aria-controls="Honey">
												          Honey
												        </a>
												      </h4>
												        </div>
												        <div id="Honey" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
												          <div class="panel-body">
												            <ul class="sub_menu">
												            	<li><a href="#">Diary</a></li>
												            	<li><a href="#">Live Animals</a></li>
												            	<li><a href="#">Honey</a></li>
												            	<li><a href="#">Hiring of Labor</a></li>
												            	<li><a href="#">Grains & Legumes</a></li>
												            </ul>
												          </div>
												        </div>
												      </div>
												      <!-- end of panel -->

												      <div class="panel">
												        <div class="panel-heading" role="tab" id="headingFour">
												          <h4 class="panel-title">
												        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#Seeds" aria-expanded="false" aria-controls="Seeds">
												          Seeds
												        </a>
												      </h4>
												        </div>
												        <div id="Seeds" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
												          <div class="panel-body">
												            <ul class="sub_menu">
												            	<li><a href="#">Diary</a></li>
												            	<li><a href="#">Live Animals</a></li>
												            	<li><a href="#">Honey</a></li>
												            	<li><a href="#">Hiring of Labor</a></li>
												            	<li><a href="#">Grains & Legumes</a></li>
												            </ul>
												          </div>
												        </div>
												      </div>
												      <!-- end of panel -->

												    </div>
	                                            </div>
	                                            <div class="col-md-6 main_pro_list">
	                                            	<div class="products_heading"><h4>Main Services</h4></div>
	                                            	<div class="panel-group wrap" id="accordion" role="tablist" aria-multiselectable="true">

												      <div class="panel">
												        <div class="panel-heading" role="tab" id="headingOne">
												          <h4 class="panel-title">
												        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" class="collapsed" aria-controls="collapseOne">
												          Diary
												        </a>
												      </h4>
												        </div>
												        <div id="collapseOne" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">
												          <div class="panel-body">
												            <ul class="sub_menu">
												            	<li><a href="#">Diary</a></li>
												            	<li><a href="#">Live Animals</a></li>
												            	<li><a href="#">Honey</a></li>
												            	<li><a href="#">Hiring of Labor</a></li>
												            	<li><a href="#">Grains & Legumes</a></li>
												            </ul>
												          </div>
												        </div>
												      </div>
												      <!-- end of panel -->

												      <div class="panel">
												        <div class="panel-heading" role="tab" id="headingTwo">
												          <h4 class="panel-title">
												        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
												          Live Animals
												        </a>
												      </h4>
												        </div>
												        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
												          <div class="panel-body">
												            <ul class="sub_menu">
												            	<li><a href="#">Diary</a></li>
												            	<li><a href="#">Live Animals</a></li>
												            	<li><a href="#">Honey</a></li>
												            	<li><a href="#">Hiring of Labor</a></li>
												            	<li><a href="#">Grains & Legumes</a></li>
												            </ul>
												          </div>
												        </div>
												      </div>
												      <!-- end of panel -->

												      <div class="panel">
												        <div class="panel-heading" role="tab" id="headingThree">
												          <h4 class="panel-title">
												        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
												          Honey
												        </a>
												      </h4>
												        </div>
												        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
												          <div class="panel-body">
												            <ul class="sub_menu">
												            	<li><a href="#">Diary</a></li>
												            	<li><a href="#">Live Animals</a></li>
												            	<li><a href="#">Honey</a></li>
												            	<li><a href="#">Hiring of Labor</a></li>
												            	<li><a href="#">Grains & Legumes</a></li>
												            </ul>
												          </div>
												        </div>
												      </div>
												      <!-- end of panel -->

												      <div class="panel">
												        <div class="panel-heading" role="tab" id="headingFour">
												          <h4 class="panel-title">
												        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
												          Seeds
												        </a>
												      </h4>
												        </div>
												        <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
												          <div class="panel-body">
												            <ul class="sub_menu">
												            	<li><a href="#">Diary</a></li>
												            	<li><a href="#">Live Animals</a></li>
												            	<li><a href="#">Honey</a></li>
												            	<li><a href="#">Hiring of Labor</a></li>
												            	<li><a href="#">Grains & Legumes</a></li>
												            </ul>
												          </div>
												        </div>
												      </div>
												      <!-- end of panel -->

												    </div>
	                                            </div>
                                            </div>




                                        </div>
                                    </div><!-- End Product Details -->
                                   

                                    <div class="spacer30"></div><!--spacer-->
                                    <div class="tab-panel clearfix"><!-- Tab -->
                                    	<div class="products_heading"><h4>You may also like</h4></div>
										<div class="owl-carousel product_details_sld">
		                                    <div class="item">
		                                    	<div><a href="#"><img src="http://192.168.2.95/miraimpex/admin/uploads/category/1579850985.jpeg" alt="img-1"/></a></div>
		                                        <div class="products_slider_info">
		                                        	<div><a href="#">OEM Hoodie Sweatshirt Cotton Long Sleeve Custom Logo Printed Oversize Pullover Hoodies</a></div>
		                                        	<div><h5>Ready to Ship</h5></div>
		                                        	<div><h4>₹ 505.92 - ₹ 681.89</h4></div>
		                                        	<div><p>1 Piece <span>(MOQ)</span></p></div>
		                                        </div>
		                                        
		                                    </div>
		                                    <div class="item">
		                                    	<div><a href="#"><img src="http://192.168.2.95/miraimpex/admin/uploads/category/1579850985.jpeg" alt="img-1"/></a></div>
		                                        <div class="products_slider_info">
		                                        	<div><a href="#">OEM Hoodie Sweatshirt Cotton Long Sleeve Custom Logo Printed Oversize Pullover Hoodies</a></div>
		                                        	<div><h5>Ready to Ship</h5></div>
		                                        	<div><h4>₹ 505.92 - ₹ 681.89</h4></div>
		                                        	<div><p>1 Piece <span>(MOQ)</span></p></div>
		                                        </div>
		                                        
		                                    </div>
		                                    <div class="item">
		                                    	<div><a href="#"><img src="http://192.168.2.95/miraimpex/admin/uploads/category/1579850985.jpeg" alt="img-1"/></a></div>
		                                        <div class="products_slider_info">
		                                        	<div><a href="#">OEM Hoodie Sweatshirt Cotton Long Sleeve Custom Logo Printed Oversize Pullover Hoodies</a></div>
		                                        	<div><h5>Ready to Ship</h5></div>
		                                        	<div><h4>₹ 505.92 - ₹ 681.89</h4></div>
		                                        	<div><p>1 Piece <span>(MOQ)</span></p></div>
		                                        </div>
		                                        
		                                    </div>
		                                    <div class="item">
		                                    	<div><a href="#"><img src="http://192.168.2.95/miraimpex/admin/uploads/category/1579850985.jpeg" alt="img-1"/></a></div>
		                                        <div class="products_slider_info">
		                                        	<div><a href="#">OEM Hoodie Sweatshirt Cotton Long Sleeve Custom Logo Printed Oversize Pullover Hoodies</a></div>
		                                        	<div><h5>Ready to Ship</h5></div>
		                                        	<div><h4>₹ 505.92 - ₹ 681.89</h4></div>
		                                        	<div><p>1 Piece <span>(MOQ)</span></p></div>
		                                        </div>
		                                        
		                                    </div>
		                                    <div class="item">
		                                    	<div><a href="#"><img src="http://192.168.2.95/miraimpex/admin/uploads/category/1579850985.jpeg" alt="img-1"/></a></div>
		                                        <div class="products_slider_info">
		                                        	<div><a href="#">OEM Hoodie Sweatshirt Cotton Long Sleeve Custom Logo Printed Oversize Pullover Hoodies</a></div>
		                                        	<div><h5>Ready to Ship</h5></div>
		                                        	<div><h4>₹ 505.92 - ₹ 681.89</h4></div>
		                                        	<div><p>1 Piece <span>(MOQ)</span></p></div>
		                                        </div>
		                                        
		                                    </div>
		                                    
		                                    
		                                </div>
                                    </div>
                                    <div class="spacer30"></div><!--spacer-->
                                    <div class="tab-panel clearfix"><!-- Tab -->
                                    	<div class="products_heading"><h4>Suppliers Certificate</h4></div>
                                    	
									    <section id="gallery">
										    <div id="image-gallery">
										      <div class="row">

										        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 image">
										          <div class="img-wrapper">
										            <a href="<?php echo base_url();?>assets/images/Certificate/Certificate.jpg">
										            	<img src="<?php echo base_url();?>assets/images/Certificate/Certificate.jpg" class="img-responsive">
										            </a>
										            <div class="img-overlay">
										              <i class="fa fa-plus-circle" aria-hidden="true"></i>
										            </div>
										          </div>
										        </div>
										        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 image">
										          <div class="img-wrapper">
										            <a href="<?php echo base_url();?>assets/images/Certificate/Certificate.jpg">
										            	<img src="<?php echo base_url();?>assets/images/Certificate/Certificate.jpg" class="img-responsive">
										            </a>
										            <div class="img-overlay">
										              <i class="fa fa-plus-circle" aria-hidden="true"></i>
										            </div>
										          </div>
										        </div>
										        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 image">
										          <div class="img-wrapper">
										            <a href="<?php echo base_url();?>assets/images/Certificate/Certificate.jpg">
										            	<img src="<?php echo base_url();?>assets/images/Certificate/Certificate.jpg" class="img-responsive">
										            </a>
										            <div class="img-overlay">
										              <i class="fa fa-plus-circle" aria-hidden="true"></i>
										            </div>
										          </div>
										        </div>
										        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 image">
										          <div class="img-wrapper">
										            <a href="<?php echo base_url();?>assets/images/Certificate/Certificate.jpg">
										            	<img src="<?php echo base_url();?>assets/images/Certificate/Certificate.jpg" class="img-responsive">
										            </a>
										            <div class="img-overlay">
										              <i class="fa fa-plus-circle" aria-hidden="true"></i>
										            </div>
										          </div>
										        </div>
										        
										      </div><!-- End row -->
										    </div><!-- End image gallery -->
										  
										</section>
                                    </div>



        							
                            	</div>
                            </div>
                        </div>

                       
                    </div>
                </div>
            </div>
            <div class="spacer30"></div><!--spacer-->


<!--  -->

<script type="text/javascript">
	$(".owl-carousel.product_details_sld").owlCarousel({
			items: 4,
			itemsDesktop : [1200,3],
			itemsDesktopSmall: [1199,3],
			itemsTablet: [950,3],
			itemsTabletSmall:[767,3],
			itemsMobile : [479,2],
			slideSpeed: 600,
			autoPlay:3000,
			stopOnHover: true,
			navigation:true,
			pagination:false,
			responsive: true,
			autoHeight : true,
			navigationText:["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"]
		});
</script>
<script type="text/javascript">
	$(".owl-carousel.supplier_details_slg_sld").owlCarousel({
			items: 1,
			itemsDesktop : [1200,3],
			itemsDesktopSmall: [1199,3],
			itemsTablet: [950,3],
			itemsTabletSmall:[767,3],
			itemsMobile : [479,2],
			slideSpeed: 800,
			autoPlay:6000,
			stopOnHover: true,
			navigation:true,
			pagination:false,
			responsive: true,
			autoHeight : true,
			navigationText:["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"]
		});
</script>
<script type="text/javascript">
	$(".owl-carousel.supplier_certificate_sld").owlCarousel({
			items: 4,
			itemsDesktop : [1200,3],
			itemsDesktopSmall: [1199,3],
			itemsTablet: [950,3],
			itemsTabletSmall:[767,3],
			itemsMobile : [479,2],
			slideSpeed: 800,
			autoPlay:6000,
			stopOnHover: true,
			navigation:true,
			pagination:false,
			responsive: true,
			autoHeight : true,
			navigationText:["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"]
		});
</script>
<script>var itemId='<?php echo $itemID;?>'</script>	


<script type="text/javascript">
	// Gallery image hover
$( ".img-wrapper" ).hover(
  function() {
    $(this).find(".img-overlay").animate({opacity: 1}, 600);
  }, function() {
    $(this).find(".img-overlay").animate({opacity: 0}, 600);
  }
);

// Lightbox
var $overlay = $('<div id="overlay"></div>');
var $image = $("<img>");
var $prevButton = $('<div id="prevButton"><i class="fa fa-chevron-left"></i></div>');
var $nextButton = $('<div id="nextButton"><i class="fa fa-chevron-right"></i></div>');
var $exitButton = $('<div id="exitButton"><i class="fa fa-times"></i></div>');

// Add overlay
$overlay.append($image).prepend($prevButton).append($nextButton).append($exitButton);
$("#gallery").append($overlay);

// Hide overlay on default
$overlay.hide();

// When an image is clicked
$(".img-overlay").click(function(event) {
  // Prevents default behavior
  event.preventDefault();
  // Adds href attribute to variable
  var imageLocation = $(this).prev().attr("href");
  // Add the image src to $image
  $image.attr("src", imageLocation);
  // Fade in the overlay
  $overlay.fadeIn("slow");
});

// When the overlay is clicked
$overlay.click(function() {
  // Fade out the overlay
  $(this).fadeOut("slow");
});

// When next button is clicked
$nextButton.click(function(event) {
  // Hide the current image
  $("#overlay img").hide();
  // Overlay image location
  var $currentImgSrc = $("#overlay img").attr("src");
  // Image with matching location of the overlay image
  var $currentImg = $('#image-gallery img[src="' + $currentImgSrc + '"]');
  // Finds the next image
  var $nextImg = $($currentImg.closest(".image").next().find("img"));
  // All of the images in the gallery
  var $images = $("#image-gallery img");
  // If there is a next image
  if ($nextImg.length > 0) { 
    // Fade in the next image
    $("#overlay img").attr("src", $nextImg.attr("src")).fadeIn(800);
  } else {
    // Otherwise fade in the first image
    $("#overlay img").attr("src", $($images[0]).attr("src")).fadeIn(800);
  }
  // Prevents overlay from being hidden
  event.stopPropagation();
});

// When previous button is clicked
$prevButton.click(function(event) {
  // Hide the current image
  $("#overlay img").hide();
  // Overlay image location
  var $currentImgSrc = $("#overlay img").attr("src");
  // Image with matching location of the overlay image
  var $currentImg = $('#image-gallery img[src="' + $currentImgSrc + '"]');
  // Finds the next image
  var $nextImg = $($currentImg.closest(".image").prev().find("img"));
  // Fade in the next image
  $("#overlay img").attr("src", $nextImg.attr("src")).fadeIn(800);
  // Prevents overlay from being hidden
  event.stopPropagation();
});

// When the exit button is clicked
$exitButton.click(function() {
  // Fade out the overlay
  $("#overlay").fadeOut("slow");
});
</script>

										