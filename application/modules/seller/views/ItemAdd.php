<!----Add For select2 --->
<link href="<?php echo base_url();?>assets/vendors/select2/dist/css/select2.min.css" rel="stylesheet" />
<script src="<?php echo base_url();?>assets/vendors/select2/dist/js/select2.min.js"></script>
<style>
.required{position:static !important;}
.group_box{background-color:#e4dede52 !important; border:1px solid #d4d1d1;}
</style>
<section class="my_profile">
  <div class="container">
    <div class="box_wrap">
      <div class="row">
       <?php
	         $this->load->view('profile/commonnav');
        ?>
      </div>
    </div>
  </div>
</section>
<section class="add_sec">
	<div class="container">
		<div class="box_wrap">
			<div class="row">

				   <div class="col-sm-12 p-no">
						<div class="update_pro_info back_color">
							<h4>Add New Product</h4>
						</div>
					</div>				    
				    <div class="col-md-12 p-no group_box" >
				    <div class="form-group col-sm-12"><label>Select Category <span class="required">*</span></label></div>
				    <div class="col-sm-3">
						<select id="category_level_1" onchange="select_cat('1');" name="category_level_1" class="form-control" tabindex="1" style="border-radius: 0">
					<option value="">-- Please select --</option>
					</select>
				    </div>
				    <div class="col-sm-3">
						<select id="category_level_2" onchange="select_cat('2');" name="category_level_2" class="form-control hide" tabindex="2" style="border-radius: 0">
						<option value="">-- Please select --</option>
					</select>
				    </div>	
				    <div class="col-sm-3">
						<select id="category_level_3" onchange="select_cat('3');" name="category_level_3" class="form-control hide" tabindex="3" style="border-radius: 0">
						<option value="">-- Please select --</option>
					</select>
				    </div>	
				    <div class="col-sm-3">
						<select id="category_level_4" onchange="select_cat('4');" name="category_level_4" multiple class="form-control hide" tabindex="4" style="border-radius: 0">
						<option value="">-- Please select --</option>
					</select>
				    </div>
						
				    </div>	
				
				    <div class="col-md-12 p-no group_box" >
					   <div class="form-group col-sm-12"><label>Title <span class="required">*</span></label></div>	
						<div class="form-group col-sm-6">
									<input type="text" class="form-control"  placeholder="Please enter product title in english"  id="item_name_en" name="item_name_en" />
							        <button type="button" class="btn-transfer hide" id="item_translator_title" lang=''>>></button>
						</div>
						<div class="form-group col-sm-6">
									<input type="text" class="form-control" placeholder="Please enter product title in french"  id="item_name_fr" name="item_name_fr" />
						</div>
				    </div>	
						
				    <div class="col-md-12 p-no group_box" >
						<div class="form-group col-sm-12"><label>Item Description <span class="required">*</span></label></div>	
						<div class="form-group col-sm-6">
							<textarea class="form-control" placeholder="Please enter product details in english" id="item_desc_en" name="item_desc_en" style="margin: 0px; width: 535px; height: 224px;" ></textarea>
							<button type="button" class="btn-transfer hide" id="item_desc_translator" lang=''>>></button>
						</div>
						<div class="form-group col-sm-6">
							<textarea class="form-control" id="item_desc_fr" name="item_desc_fr"  placeholder="Please enter product details in french" style="margin: 0px; width: 535px; height: 224px;"></textarea>
						</div>
				    </div>
					
					<div class="col-md-12 p-no group_box" >
						<div class="form-group col-sm-4">
							<label>Thumbnail: <span class="required">*</span></label>
							<label class="thum_img"><input type="file" id="thumbnail_image" name="thumbnail_image" accept="image/x-png,image/gif,image/jpeg"><span>Choose Files</span><i class="fa fa-file-image-o" aria-hidden="true"></i>
					        <label id="thmpname"></label>
					        </label>
									
						</div>
						<div class="form-group col-sm-8">
							<label>Keywords: <span class="required">*</span></label>	
							<textarea class="form-control" id="item_keywords" name="item_keywords"  placeholder="In order for customers to find your products on Mira Impex, it’s important to provide Search Terms (Comma Separated) they might use when searching for what they want to buy." style="margin: 0px; width: 721px; height: 68px;"></textarea>
						</div>
				    </div>	

					<div class="col-md-12 p-no group_box" >
					<div class="form-group col-sm-4" style="padding:0px;">
					<div class="form-group col-sm-12"><label>Negotiable Price: </label><span style="font-size:10px;">(e.g 1-2 USD/Piece)</span> <span class="required" style="font-size: 20px;">*</span></div>	
						<div class="form-group col-sm-6">
						<div class="input-group">
							<span class="input-group-addon symbolLeft"><i class="fa fa-dollar" style="font-size:15px"></i></span>
							<input type="number" id="min_price" name="min_price" class="price select-width3" style="width:100% !important;" placeholder="Min Price" autocomplete="off" required value="">
						</div>
						<div style="font-size:12px;color:darkgreen" id="min_price_approx"></div>						
						</div>
						
						<div class="form-group col-sm-6">
						<div class="input-group">
							<span class="input-group-addon symbolLeft"><i class="fa fa-dollar" style="font-size:15px"></i></span>
							<input type="number" id="max_price" name="max_price" class="price form-control" style="width:100% !important;" placeholder="Max Price" autocomplete="off" required value="">
						</div>
						<div style="font-size:12px;color:darkgreen" id="max_price_approx"></div>
						</div>
					</div>
					    

					<div class="form-group col-sm-5" style="padding:0px;">
					<div class="form-group col-sm-12"><label>Non-negotiable Price: </label><span style="font:size:10px;">(e.g Add to Cart Price)</span> <span class="required" style="font-size: 20px;">*</span></div>
					<div class="form-group col-sm-6">
						<div class="input-group">
							<span class="input-group-addon symbolLeft"><i class="fa fa-dollar" style="font-size:15px"></i></span>
							<input type="number" placeholder="Fixed Price"  id="fixed_price" name="fixedPrice" class="price form-control" style="width:100% !important;" autocomplete="off" required value="">
						</div>
						<div style="font-size:12px;color:darkgreen" id="fixed_price_approx"></div>
						</div>	
					</div>

					<div class="form-group col-sm-3" style="padding:0px;">
					<div class="form-group col-sm-12"><label>Unit Of Measurement: </label><span class="required" style="font-size: 20px;">*</span></div>
					<div class="form-group col-sm-12">
						<div class="input-group">
						<select class="form-control" id="uom_1" name="uom" style="border-radius: 0">
						    </select>
							<input type="hidden" id="currency" name="currency" value="USD" />
						</div>
						</div>	
					</div>
			     </div>
					
				
				<div class="col-md-12 p-no group_box" >
					    <div class="form-group col-sm-12"><label>Min Order:</label><span style="font:size:10px;">(e.g 100 Piece)</span> <span class="required" style="font-size: 20px;">*</span></div>	
					   	<div class="form-group col-sm-2">
							<input type="number" class="form-control" placeholder="Min Order"  id="min_order" name="min_order" />
						</div>
					    <div class="form-group col-sm-2">
							<select class="form-control" id="uom_2" name="uom" style="border-radius: 0">
						    </select>
						</div>
			    </div>
				
				<div class="col-md-12 p-no group_box" >
					    <div class="form-group col-sm-6" style="padding:0px;">
						<div class="form-group col-sm-12"><label>Packaging Information <span class="required">*</span></label></div>	
						<div class="form-group col-sm-6">
							<textarea class="form-control" placeholder="Please enter packaging information" id="packaging" name="packaging" style="margin: 0px; width: 535px; height: 124px;" ></textarea>
						</div>						
						</div>

						<div class="form-group col-sm-6" style="padding:0px;">
						<div class="form-group col-sm-12"><label>Delivery/Availablity Information <span class="required">*</span></label></div>	
						<div class="form-group col-sm-6">
							<textarea class="form-control" placeholder="Please specify where the item is available/deliverable." id="delivery_available" name="delivery_available" style="margin: 0px; width: 535px; height: 124px;" ></textarea>
						</div>						
						</div>	
					   	
						   
			    </div>
				<div class="col-md-12 p-no group_box" >
				<div class="form-group col-sm-6" style="padding:0px;">
				<div class="form-group col-sm-12"><label>Lead Time:</label></div>	
					   	<div class="form-group col-sm-12">
							<input type="text" class="form-control" placeholder="Lead Time"  id="lead_time" name="lead_time" />
						</div>	
				</div>

				<div class="form-group col-sm-6" style="padding:0px;">
				<div class="form-group col-sm-12"><label>Shipping Info:</label></div>	
					   	<div class="form-group col-sm-12">
							<input type="text" class="form-control" placeholder="Shipping Information(eg shipping carrier, estimated time etc.)"  id="shipping_time" name="shipping_time" />
						</div>	
				</div>
					    
			    </div>
				<div class="col-md-12 p-no group_box" >
					    <div class="form-group col-sm-12"><label>Item property(variance):</label></div>	
					    <div id="varience_div"></div>
				</div>
				<div class="col-md-12 p-no group_box" >
					    <div class="form-group col-sm-12"><label>Item Attributes:</label></div>	
					  
					    <div id="attribute_div" class="row">  
							<div class="col-sm-12">
								<div class="form-group col-sm-4">
									<select class="form-control" id="attribute_1_sel" name="attribute_1_sel"  >
									</select>
								</div>
								<div class="form-group col-sm-4">
									<input type="text" class="form-control" placeholder="Value"  id="attribute_1_txt" name="attribute_1_txt" />
								</div>
							</div>
					    </div>

						<div class="form-group col-sm-8" align="right"><button class="btn_browse add_more"  onclick="addAttributes();" > + Add More</button></div>
 
			    </div>
				

			    <div class="col-md-8 p-no group_box browse_img_sec" >
			    	<div class="form-group col-sm-12">
			    		<p>More Images</p>
			    	</div>
					    <input type="file" id="browse_image" class="hide" accept="image/x-png,image/gif,image/jpeg">
					   	<div class="form-group col-sm-3">
					   		<div class="bro_img_cover"><img src="<?php echo base_url();?>assets/images/no-images.png"  id="more_img_1" >
					   			<button class="btn_browse" id="btn_browse_1"  onclick="uploadImage('1')" >Browse</button>
								<a href="javascript:void(0)" id="btn_browse_rm_1" onclick="removeImage('1')" class="hide" ><i class="fa fa-trash" aria-hidden="true"></i></a>
							</div>
						</div>
					    <div class="form-group col-sm-3">
					   		<div class="bro_img_cover"><img src="<?php echo base_url();?>assets/images/no-images.png"  id="more_img_2">
					   			<button class="btn_browse" id="btn_browse_2" onclick="uploadImage('2')" >Browse</button>

							    <a href="javascript:void(0)" id="btn_browse_rm_2" onclick="removeImage('2')" class="hide" ><i class="fa fa-trash" aria-hidden="true"></i></a>
							</div>
						</div>
						<div class="form-group col-sm-3">
					   		<div class="bro_img_cover"><img src="<?php echo base_url();?>assets/images/no-images.png"  id="more_img_3">
					   			<button class="btn_browse" id="btn_browse_3" onclick="uploadImage('3')" >Browse</button>

							    <a href="javascript:void(0)" id="btn_browse_rm_3" onclick="removeImage('3')" class="hide" ><i class="fa fa-trash" aria-hidden="true"></i></a>
							</div>
						</div>
					    <div class="form-group col-sm-3">
					   		<div class="bro_img_cover"><img src="<?php echo base_url();?>assets/images/no-images.png"  id="more_img_4">
					   			<button class="btn_browse" id="btn_browse_4" onclick="uploadImage('4')" >Browse</button>
					   		
							    <a href="javascript:void(0)" id="btn_browse_rm_4" onclick="removeImage('4')" class="hide" ><i class="fa fa-trash" aria-hidden="true"></i></a></div>
						</div>
			    </div>
				
			    <div class="col-md-12 group_box" >
				
				 <div class="col-md-3 form-group col-sm-3">
					<div class="form-group col-sm-12"><label>ShowCase</label></div>	
					<div class="form-group col-sm-12">
							<select class="form-control" id="showcase" name="showcase" style="border-radius: 0">
						    	<option value="1">Yes</option>
								<option value="0">No</option>
						    </select>
					        </div>		
				  </div>
				
				   <div class="col-md-3 form-group col-sm-3">
					    <div class="form-group col-sm-12"><label>Status</label></div>	
					    <div class="form-group col-sm-12">
							<select class="form-control" id="status" name="status" style="border-radius: 0">
						    	<option value="1">On-Display</option>
						    </select>
					    </div>		
				  </div>
				
				  <div class="col-md-3 form-group col-sm-3">
				        <div class="form-group col-sm-10" style="padding-top: 18px;">
				          <button type="button" class="btn_browse add_more"  onclick="add_item();" id="btn_step_7" >Add New Item</button>
				        </div>		
				  </div>
				</div>

	    </div>
	</div>
	</div>		
</section>
<script src="<?php echo base_url();?>assets/custom_js/seller/additem.js?<?php echo rand();?>"></script>