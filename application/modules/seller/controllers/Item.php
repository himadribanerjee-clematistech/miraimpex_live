<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Item extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct() {
		parent::__construct();
		$this->load->model('Seller_Model'); 
		$this->load->helper('site_helper');

	}
	
	public function index()
	{
		$this->load->view('Item');
	}
	
	public function addItem()
	{

		if($this->uri->segment(5)!=null)
		  $catID=$this->uri->segment(5);
		else
		  $catID=0;
		
		$data=$uomData=$varienceData=$specData=array(); 
		$item=$this->Seller_Model->getItemCategory($catID);
		$catPath=$this->Seller_Model->categoryBreadcumbsPath($catID);  
		
		if(sizeof($item)==0){
		$uomData=$this->Seller_Model->getUOM($catID);  
		$varienceData=$this->Seller_Model->getVariance();  
		$specData=$this->Seller_Model->getSpecification();  	
		}

		$data['items']=$item;
		$data['catPath']=$catPath['html']; 
		$data['catIdArr']=$catPath['catIdArr']; 
		$data['uomData']=$uomData; 
	    $data['varienceData']=$varienceData; 
	    $data['specData']=$specData; 
		$data['catID']=$catID; 
		
		$data['view_file'] = 'ItemAdd';
        view($data);
		
	}
	

	
	public function getLangApi(){
		$str=languageConvertor($this->input->post('str'),$this->input->post('lang'));
		echo json_encode(array('lang'=>$str));
	}
	
	public function editItem()
	{

		if($this->uri->segment(4)!=null)
		  $itemID=$this->uri->segment(4);
		else
		  $itemID=0;
		
		$itemID=base64_decode($itemID);
		

		$data=$uomData=$varienceData=$specData=array(); 
		$catID=$this->Seller_Model->getItemDetails($itemID);
		
		$catID=$catID['category_id'];		
		$catPath=$this->Seller_Model->categoryBreadcumbsPath($catID);  
		

		$uomData=$this->Seller_Model->getUOM($catID);  
		$varienceData=$this->Seller_Model->getVariance();  
		$specData=$this->Seller_Model->getSpecification();  	

		$data['catPath']=$catPath['html']; 
		$data['itemID']=$itemID; 
		$data['catIdArr']=$catPath['catIdArr']; 
		$data['uomData']=$uomData; 
	    $data['varienceData']=$varienceData; 
	    $data['specData']=$specData; 
		$data['catID']=$catID; 
		
		$data['view_file'] = 'ItemEdit';
		// print_r($data);
        view($data);
		
	}
	
	
	
	
}
