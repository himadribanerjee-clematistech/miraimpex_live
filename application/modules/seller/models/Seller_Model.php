<?php

class Seller_Model extends CI_Model {


	function __construct() {
		parent::__construct();
	}
   
	public function getItemCategory($parentId){
		
		$this->db->select('category_name_en,category_name_fr,image,id');
		$this->db->where('active','1');
		$this->db->where('type','1');
	    $this->db->where('parent_id',$parentId);
	    $category_list=$this->db->get('category')->result_array();
		return $category_list;
	}
	
	public function categoryBreadcumbsPath($catID){
		
	    $html='';
	
		$list=array();
		if($catID>0){
			    
			    getParent :{ 
			   
					    $catData=$this->getCategoryById($catID);  
						$row=array();
						$row['id']=$catData['id'];
						$row['name']=$catData['category_name_'.$this->session->userdata('lang')];	
						$catID=$catData['parent_id'];
						$list[]=$row;
	                    if($catData['parent_id']>0)																 
						goto getParent;
				}
				
				
		}
		$list=array_reverse($list);
		$catIdArr=array();
		if(sizeof($list)>0){
			$html.='<div class="row"><ol class="breadcrumb"><li class="breadcrumb-item">You are In: </li>';
			foreach($list as $data){
				$html.='<li class="breadcrumb-item"><a href="'.base_url().'seller/item/addItem/catId/'.$data['id'].'">'.$data['name'].'</a></li>';
                $catIdArr[]=trim($data['id']);
			}
			$html.='</ol></div>';
		}

					
		return array('html'=>$html,'catIdArr'=>$catIdArr);
	}
	
	private function getParentCategory($catID){
		
		$this->db->select('category_name_en,category_name_fr,id');
		$this->db->where('active','1');
		$this->db->where('type','1');
	    $this->db->where('parent_id',$catID);
		$category_list=$this->db->get('category')->row_array();
		return $category_list;
	}
	
	private function getCategoryById($catID){
		
		$this->db->select('category_name_en,category_name_fr,id,parent_id');
		$this->db->where('active','1');
		$this->db->where('type','1');
	    $this->db->where('id',$catID);
		$category_list=$this->db->get('category')->row_array(); 
		return $category_list;
	}
	
	public function getUOM($catID){
		
		$this->db->select('name_'.$this->session->userdata('lang').' as name,uom.id');
		$this->db->join('cat_uom_relation','cat_uom_relation.uom_id=uom.id');
		$this->db->where('uom.active','1');
		$this->db->where('type','1');
	    $this->db->where('cat_id',$catID);
		$uomlist=$this->db->get('uom')->result_array();
		return $uomlist;
		
	}
	
	public function getVariance(){
		
		$this->db->select('name_'.$this->session->userdata('lang').' as name,id');
		$this->db->where('type','1');
		$this->db->where('status','1');
		$variencelist=$this->db->get('variance')->result_array();
		return $variencelist;
	}
	
	public function getSpecification(){
		
		$this->db->select('name_'.$this->session->userdata('lang').' as name,id');
		$this->db->where('type','1');
		$this->db->where('status','1');
		$speclist=$this->db->get('attribute')->result_array();
		return $speclist;
	}
	
	public function getItemDetails($itemId){
		
		$this->db->select('supp_id,local_sup_id,category_id');
		$this->db->where('active','1');
		$this->db->where('id',$itemId);
		$this->db->where('is_deleted','0');
		$record=$this->db->get('item')->row_array();
		return $record;
		
	}
	
	
}