<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Messagecenter extends CI_Controller {

	protected $token_id='';

    public function __construct() {
        parent::__construct(); 
    }


    public function index() {
    	$this->load->helper('url');
        $currentURL = current_url();
        $params   = $_SERVER['QUERY_STRING'];
        parse_str($params, $params_array);        

         $data = [];
         
         $data['uriParams'] = $params_array;
      
        $data['view_file'] = 'messagecenter';
        view($data);
    } 

   
}