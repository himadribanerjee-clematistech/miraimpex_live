<script src="<?php echo base_url(); ?>assets/custom_js/messageCenter.js?version=<?php echo time(); ?>"></script>

<?php
  if(!isset($uriParams['type']))
  {
    $uriParams['type'] = '';
  }
  if(!isset($uriParams['id']))
  {
    $uriParams['id'] = '';
  }
  
  $sType = ($uriParams['type'] != '' ? $uriParams['type'] : '');
  $sId = ($uriParams['id'] != '' ? $uriParams['id'] : '');

?>
<input type="hidden" name="sType" id="sType" value="<?php echo $sType; ?>">
<input type="hidden" name="sId" id="sId" value="<?php echo $sId; ?>">

<div class="cover_section">
<!-- <div class="qutation_left_menuber">
  
  <div class="">
    <ul id="leftPanelNavi">
      <li><a href="<?php echo base_url(); ?>profile"><i class="fa fa-home"></i> <span>Home</span></a></li>
      <li class="active"><a href="javascript:void(0);" onclick="javascript:selectMsgType('all');  changeState(this);"><i class="fa fa-list"></i> <span>Message Center</span></a></li>
      <li><a href="<?php echo base_url(); ?>profile/myenquiries"><i class="fa fa-file-text"></i> <span>Enquiry</span></a></li>
      <li><a href="<?php echo base_url(); ?>profile/myrfqs"><i class="fa fa-file-text"></i> <span>RFQs</span></a></li>
      
    </ul>

  </div>
</div> -->



<section class="message_center_section">
  <div class="container-fluid">
    <div class="box_wrap">
      <div class="row" id="withContent">

        <div class="col-md-3">
          <div class="Inqulrles_sec">
            <a href="javascript:void(0);" class="displayMsgType1" id="topDisp">ALL</a>
          </div>
          <div class="Message_search">
            <form>
              <input type="text" id="localSearch" placeholder="Type To Search" name="localSearch">
              <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
            </form>

          </div>
          <div class="all_statuses">
            <div class="all_statuses_left displayMsgType2"><a href="javascript:void(0);">All <i class="fa fa-angle-down" aria-hidden="true"></i></a> </div>
            <div class="all_statuses_unreed"></div>
              <div class="all_statuses_view">
                <ul>
                  <li><a href="javascript:void(0);" onclick="javascript:selectMsgType('all');">ALL</a></li>
                  <li><a href="javascript:void(0);" onclick="javascript:selectMsgType('rfq');">RFQ</a></li>
                  <li><a href="javascript:void(0);" onclick="javascript:selectMsgType('enquiry');">ENQUIRY</a></li>
                </ul>
              </div>
          </div>
          <div class="all_messages_list">
            <ul class="nav nav-tabs" role="tablist" id="messageList">
              <div class="loading_section">
                <img src="<?php echo base_url(); ?>assets/images/icon/Loading_icon.gif" alt="">
            </div>
              
            </ul>
            
         </div>

        </div>

        <div class="col-md-5" id="p2">
          
          <div class="Message_box">
            <div class="tab-content">
              <div class="tab-pane active" id="tabs-1" role="tabpanel">
                <div class="Message_view_header">
                  <div class="Message_list_img">
                      <img id="sellerProfileImg_msg" src="<?php echo base_url(); ?>assets/images/user.png" alt="">
                  </div>

                  <div class="Message_list_info">
                    <div class="top_mess"><h4 id="sellerProfileName_msg">Username</h4>
                    </div>
                    <div class="top_mess_Proposal" id="sendProposalDiv"><a href="#" data-toggle="modal" data-target="#send_proposal_model">Send Proposal</a></div>
                    
                  </div>
                  
                </div>
                <div class="Message_box_control" id="chatBoxDetails">
                  <div class="loading_section">
                <img src="<?php echo base_url(); ?>assets/images/icon/Loading_icon.gif" alt="">
            </div>
                </div>
                
              </div>
              <div id="cantReplyPanel" class="d-none"><p style="background-color: #ececec;font-size: 14px; padding:10px 15px; border-radius: 5px;">You can't send messages because this RFQ has closed. </p></div>
              <div class="reply_section d-none" id="replyMsgCenter">
                <input type="hidden" name="rfq_id" id="rfq_id" value="">
                <input type="hidden" name="to_id" id="to_id" value="">
                <input type="hidden" name="msgType" id="msgType" value="">
                <span id="sendingMessageIcon" class="d-none">Sending message...</span>

                <!-- <div id="msgAddImgdv" class="gallery"></div> -->

                  <form name="messagecenterForm" action="javascript:void(0);" method="post" id="messagecenterForm" enctype="multipart/form-data">
                    <textarea id="chatcontent" onkeyup="AutoGrowTextArea(this)" rows="1" placeholder="Please Enter Message..."></textarea>
                    <input type="file" name="image[]" id="msgcenter-photo-addd" accept="image/x-png,image/gif,image/jpeg">
                    <button type="button" id="btn-chat">Send</button>
                  </form>
                </div>
                <div class="report_incident_info">
                  <a href="#" data-toggle="modal" data-target="#report_incident_model">
                    <img src="<?php echo base_url(); ?>assets/images/report_incident.png" alt="">Report Incident</a>
                </div>

            </div>
          </div>




        </div>
        <div class="col-md-4" id="p3">
        	<div class="panel-group traking_message" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="headingOne">
        <h4 class="panel-title">
        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">        
         <ul id="rfqTrackId" class="d-none">
             </ul>
        </a>
      </h4>
      </div>
      <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
        <div class="panel-body">
          <div id="rfqMsg" class="d-none"></div>
             <div id="rfqOthReq" class="d-none"></div>
             <div class="opt_img d-none" id="rfqSmallImgPanel"></div>
        </div>
      </div>
    </div>
    <div class="panel panel-default traking_message " id="traking_proposal">
      <div class="panel-heading" role="tab" id="headingTwo">
        <h4 class="panel-title">        
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
         <ul><li>Active Proposal <i class="fa fa-arrow-right" aria-hidden="true"></i></li></ul>
        </a>
      </h4>
      </div>
      <div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
        <div class="panel-body">
          <div class="pay_sec_mess">

             <div class="pay_sec_mess_head">
               <div class="pay_sec_mess_img"><a href="javascript:void(0);" id="proposal_doc_link" target="_blank" title="view proposal details"><img src="<?php echo base_url(); ?>assets/images/icon/proposal-icon.png" alt=""></a></div>
               <div class="pay_sec_mess_cnt"><a href="javascript:void(0);" id="proposal_title" target="_blank" title="view proposal details">Proposal title.</a></div>
             </div>
             <div class="pay_sec_mess_info">
             <p id="proposal_desc"></p>
             </div>
             <div class="pay_sec_mess_info">
              <p><b>Date </b> <span id="proposal_date"><b></b></span></p>
               <p><b>Total </b> <span id="proposal_amount"><b>$0.00</b></span></p>
             </div>
             <div class="pay_sec_mess_info">
               <a href="" id="proposal_pay_link"><button type="button">Accept This Proposal</button></a>
             </div>

           </div> 
        </div>
      </div>
    </div>
    
  </div>
          <div class="traking_message">
            <ul id="rfqTrackId" class="d-none">
             </ul>
            <div id="rfqMsg" class="d-none"></div>
             <div id="rfqOthReq" class="d-none"></div>
             <div class="opt_img d-none" id="rfqSmallImgPanel"></div>
          </div>

          <div id="traking_proposal" class="traking_message d-none">
           <ul><li>Active Proposal <i class="fa fa-arrow-right" aria-hidden="true"></i></li></ul>

           <div class="pay_sec_mess">

             <div class="pay_sec_mess_head">
               <div class="pay_sec_mess_img"><a href="javascript:void(0);" id="proposal_doc_link" target="_blank" title="view proposal details"><img src="<?php echo base_url(); ?>assets/images/icon/doc.png" alt=""></a></div>
               <div class="pay_sec_mess_cnt"><a href="javascript:void(0);" id="proposal_title" target="_blank" title="view proposal details">Proposal title.</a></div>
             </div>
             <div class="pay_sec_mess_info">
             <p id="proposal_desc"></p>
             </div>
             <div class="pay_sec_mess_info">
              <p><b>Date </b> <span id="proposal_date"><b></b></span></p>
               <p><b>Total </b> <span id="proposal_amount"><b>$0.00</b></span></p>
             </div>
             <div class="pay_sec_mess_info">
               <a href="" id="proposal_pay_link"><button type="button">Confirm & Pay</button></a>
             </div>

           </div>
          </div>
        </div>
       
       </div>
       <div class="row" id="withoutContent"></div>
     </div>
  </div>
</section>



</div>







<div id="report_incident_model" class="modal profile_model_design" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">  
  <form action="javascript:void(0);" method="post" name="report_incidentForm" id="report_incidentForm" enctype="multipart/form-data">     
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" onclick="javascript:doclose();" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
          <!-- <span onclick="doclose()" class="close" title="Close Modal">&times;</span> -->
          <h4 class="modal-title w-100 font-weight-bold">Report Incident</h4>
        </div>
        <div class="modal-body">
          <div class="md-form " style="padding-top: 10px;">
           <textarea class="rerort_file" rows="6" name="message" id="report_incident_msg" placeholder="Report Message"></textarea>
           <input type="hidden" name="from_customer_id" id="report_incident_from_id" value="">
           <input type="hidden" name="to_customer_id" id="report_incident_to_id" value="">
           <input type="hidden" name="ref_id" id="report_incident_ref_id" value="">
           <input type="hidden" name="type" id="report_incident_ref_type" value="">
          </div>
          <div id="msgAddImgdv" class="gallery"></div>
          <div class="md-form " style="padding-top: 10px;">
           <input type="file" name="image[]" id="msgcenter-photo-add" accept="image/x-png,image/gif,image/jpeg">
          </div>
          <!-- <div class="form-group row  btn_cover_design">
          <button type="submit" class="btn_submit">Submit </button>
        </div> -->
          <div class="md-form btn_cover_design" style="padding-top: 10px;">
            <input type="submit"  class="btn_submit" id="report_incident_Submit" value="Report" style="width:auto;">

          </div>
        </div>
      </div>
    </div>   
  </form>
</div>


<div id="send_proposal_model" class="modal  profile_model_design " aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">  
  <form action="javascript:void(0);" method="post" name="send_proposalForm" id="send_proposalForm" enctype="multipart/form-data">     
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" onclick="javascript:doclose();" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
          <h4 class="modal-title w-100 font-weight-bold">Send Proposal</h4>
        </div>
        <div class="modal-body buyer_profile_edit">
        
          <div class="row">
          <div class="col-md-6">
              <div class="form-group row ">
                <label for="inputPassword" class="col-md-12 col-form-label">Title :</label>
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <input type="text" name="title" id="send_proposal_title" class="form-control" placeholder="Title">
                        </div>
                        
                    </div>                               
                </div>
            </div>
          
            <div class="form-group row">
                <label for="inputPassword" class="col-sm-12 col-form-label">Currency:</label>
                <div class="col-sm-12">
                  <select name="currency" class="form-control">
                    <option value="USD">US Dollar</option>
                    <option value="EUR">Euro</option>
                  </select>
                  
                </div>
            </div>                      
            
          </div>
          <div class="col-md-6">
          <div class="form-group row">
                  <label for="inputPassword" class="col-md-12 col-form-label">Extra Note(if any) :</label>
                  <div class="col-md-12">
                      <div class="row">
                          <div class="col-md-12">
                              <input type="text" id="send_proposal_extranote" class="form-control" placeholder="Note ">
                          </div>
                          
                      </div>                               
                  </div>
              </div>
          <div class="form-group row Language">
                <label for="inputPassword" class="col-sm-12 col-form-label">Amount:</label>
                <div class="col-sm-12">
                  <input type="number" name="amount" id="send_proposal_amount" class="form-control" placeholder="Amount ">
                </div>
            </div>
            
            
          </div>
          <div class="col-md-12">
            <div class="form-group row">
                <label for="inputPassword" class="col-sm-12 col-form-label">Description:</label>
                <div class="col-sm-12">
                  <textarea name="description" id="send_proposal_desc" class="form-control" placeholder="Description" rows="5"></textarea>
                   
                </div>
            </div>
          <div class="form-group row Language">
                <label for="inputPassword" class="col-sm-12 col-form-label">Attachment:</label>
                <div class="col-sm-12">
                <input type="file" name="file" id="sendproposal-photo-add">
                </div>
            </div>
          </div>
      </div>

       
        <div class="form-group row  btn_cover_design">
        <input type="hidden" name="to_id" id="send_proposal_to_id" value="">
        <input type="hidden" name="from_id" id="send_proposal_from_id" value="">
        <input type="hidden" name="refference_id" id="send_proposal_ref_id" value="">
        <input type="hidden" name="refference_type" id="send_proposal_ref_type" value="">
        <input type="hidden" name="lang" value="en">
          <input type="submit" class="btn_submit" value="Send Proposal">
        </div>
                            
        
      </div>
      </div>
    </div>   
  </form>
</div>

















<script type="text/javascript">

  $(document).ready(function(){
    $(".all_statuses_left").click(function(){
      $(".all_statuses_view").slideToggle(200);
    });

  });

</script>  
<script>
  
  $(window).scroll(function() {    
    var scroll = $(window).scrollTop();

    if(scroll >= 210) {
        $(".qutation_left_menuber").addClass("fixed-top");
    } else {
        $(".qutation_left_menuber").removeClass("fixed-top");
    }
});


</script>