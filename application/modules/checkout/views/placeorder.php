<style type="text/css">
    .bs-example{
        margin: 10px;
    }
</style>
<section class="payment_page">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<div class="group_pay">
					<div class="pay_heading back_color">
			          <h4>PAYMENT</h4>          
			        </div>
			        <div class="payment_form_design">
			        	
			        	<form action="javascript:void(0);" method="post" name="send_checkoutForm" id="send_checkoutForm">
      
        <div class="row">
          <div class="col-md-6">
            <h3>Delivery Address</h3>
            <label for="fname"><i class="fa fa-user"></i> Full Name</label>
            <input type="text" id="fname" name="firstname" placeholder="John M. Doe">
            <label for="email"><i class="fa fa-envelope"></i> Email</label>
            <input type="text" id="email" name="email" placeholder="john@example.com">
            <label for="adr"><i class="fa fa-address-card-o"></i> Address</label>
            <textarea id="adr" name="address" placeholder="542 W. 15th Street" row="4" style="margin: 0px; width: 343px; height: 110px;"></textarea>
            <label for="city"><i class="fa fa-institution"></i> City</label>
            <input type="text" id="city" name="city" placeholder="New York">

            <div class="row">
              <div class="col-md-6">
                <label for="state">State</label>
                <input type="text" id="state" name="state" placeholder="NY">
              </div>
              <div class="col-md-6">
                <label for="zip">Zip</label>
                <input type="text" id="zip" name="zip" placeholder="10001">
              </div>
            </div>

            <div class="row">
              <div class="col-md-6">
                <label for="state">Contact No</label>
                <input type="text" id="contactnumber1" name="contactnumber1" placeholder="9999999999">
              </div>
              <div class="col-md-6">
                <label for="zip">Contact No(Optional)</label>
                <input type="text" id="contactnumber2" name="contactnumber2" placeholder="9999999999">
              </div>
            </div>
          </div>

          <div class="col-md-6 credit-card-box">
            <h3>Payment Method</h3>
            <label>
          <input type="radio" checked="checked" name="paymentMode" value="2"> Bank Transfer (For Gambia)
        </label>
        <label>
          <input type="radio" name="paymentMode" value="3"> Bank Transfer (For US)
        </label>
        <label>
          <input type="radio" name="paymentMode" value="4"> Bank Transfer (For Europe)
        </label>
        <label>
          <input type="radio" name="paymentMode" value="1"> Cash On Delivery
        </label> 

        <div id="bankpay-AF" style="">
          <div class="row display-tr" >
              <h3 class="panel-title display-td " >Bank Account Details</h3>            
          </div>        
              <div class="row">
                  <div class="col-xs-12">                      
                      <h5>Bank name : <strong> Ecobank </strong> </h5>
                      <h5>Account name : <strong> Mira Impex </strong> </h5>
                      <h5>Account Number : <strong> 6261006673</strong> </h5>
                      <h5>BBAN : <strong> 008203626100667301</strong> </h5>
                  <br>     
                  <h5 style="color:red;">NOTE : <strong> Your order will only be processed and delivered when we receive full payment of order value and delivery fee if applicable.</strong><br/><br/><span style="color:blueviolet">Debit Card and Credit Card payment option will be activated soon.</span></h5>                 
                  </div>
              </div>
          </div>

          <div id="bankpay-US" style="display:none;"> 
          <div class="row display-tr" >
              <h3 class="panel-title display-td " >ZELLE PAYMENT DETAILS</h3>                          
          </div>      
          <div class="row">
                  <div class="col-xs-12">
                  <p>Living in the US and want to place an order ? Kindly make your payment using  ZELLE. </p>
                  </div>
          </div>  
              <div class="row">
                  <div class="col-xs-12" style="padding-top:10px;">                      
                      <h5>Account name : <strong> AWA SINYAN</strong> </h5>
                      <h5>ZELLE Email : <strong> miraimpex15@gmail.com</strong> </h5>
                  <br>  
                  <h5 style="color:red;">NOTE : <strong> Your order will only be processed and delivered when we receive full payment of order value and delivery fee if applicable.</strong><br/><br/><span style="color:blueviolet">Debit Card and Credit Card payment option will be activated soon.</span></h5>                     
                  </div>
              </div>
          </div>

          <div id="bankpay-EUR" style="display:none;"> 
          <div class="row display-tr" >
              <h3 class="panel-title display-td " >UK Bank Account Details</h3>                          
          </div>      
          <div class="row">
                  <div class="col-xs-12">
                  <p>living in EUROPE ? Kindly make a bank transfer to our UK Bank account. </p>
                  </div>
          </div>  
              <div class="row">
                  <div class="col-xs-12" style="padding-top:10px;">                      
                  <h5>Account name : <strong> QAYYAH TRADING </strong> </h5>
                      <h5>Account Number : <strong> 75058468</strong> </h5>
                      <h5>SC Number : <strong> 30-98-97</strong> </h5>
                  <br> 
                  <h5 style="color:red;">NOTE : <strong> Your order will only be processed and delivered when we receive full payment of order value and delivery fee if applicable.</strong><br/><br/><span style="color:blueviolet">Debit Card and Credit Card payment option will be activated soon.</span></h5>                      
                  </div>
              </div>
          </div>

            <!-- <label for="fname">Accepted Cards</label>
            <div class="icon-container">
              <i class="fa fa-cc-visa" style="color:navy;"></i>
              <i class="fa fa-cc-amex" style="color:blue;"></i>
              <i class="fa fa-cc-mastercard" style="color:red;"></i>
              <i class="fa fa-cc-discover" style="color:orange;"></i>
            </div>
            <label for="cname">Name on Card</label>
            <input type="text" id="cname" name="cardname" placeholder="John More Doe">
            <label for="ccnum">Credit card number</label>
            <input type="text" id="ccnum" name="cardnumber" placeholder="1111-2222-3333-4444">
            <label for="expmonth">Exp Month</label>
            <input type="text" id="expmonth" name="expmonth" placeholder="September">
            <div class="row">
              <div class="col-md-6">
                <label for="expyear">Exp Year</label>
                <input type="text" id="expyear" name="expyear" placeholder="2018">
              </div>
              <div class="col-md-6">
                <label for="cvv">CVV</label>
                <input type="text" id="cvv" name="cvv" placeholder="352">
              </div>
            </div> -->
          </div>
          
        </div>
         
        <input type="hidden" name="totalToPay" value="0" id="totalToPay">
        <input type="submit" value="Confirm & Order Now" class="btn" id="confirmPayButton">
      </form>
			        </div>
				</div>

        <div class="group_pay">
					<div class="pay_heading back_color">
			          <h4>DELIVERY CHARGES</h4>                
          </div>
          <div class="row payment_form_design">
          <div class="col-xs-12">
          <h5 style="color:red;">NOTE :DELIVERY / TRANSPORTATION COST ARE SUBJECT TO CHANGE.</h5> 
  <ul class="nav nav-tabs" style="margin-top:25px;">
    <li class="active"><a data-toggle="tab" href="#menu1" style="font-weight:bold;">FOOD AND BEVERAGES</a></li>
    <li><a data-toggle="tab" href="#menu2" style="font-weight:bold;">CONSTRUCTION AND BUILDING MATERIALS</a></li>
  </ul>

  <div class="tab-content">
    <div id="menu1" class="tab-pane fade in active">
    <div class="card-body">
                <style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:12px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ddd;}
.tg th{font-family:Arial, sans-serif;font-size:12px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ddd;}
.tg .tg-0lax{text-align:left;vertical-align:top}
</style>
<table class="tg">
  <tr>
    <th class="tg-0lax" style="white-space:nowrap">ITEM</th>
    <th class="tg-0lax" style="white-space:nowrap">UNIT</th>
    <th class="tg-0lax" style="white-space:nowrap">BANSANG</th>
    <th class="tg-0lax" style="white-space:nowrap">BASSE</th>
    <th class="tg-0lax" style="white-space:nowrap">FARRAFENNI</th>
    <th class="tg-0lax" style="white-space:nowrap">BRIKAMA</th>
    <th class="tg-0lax" style="white-space:nowrap">BRIKAMA BA</th>
    <th class="tg-0lax" style="white-space:nowrap">GBA</th>
    <th class="tg-0lax" style="white-space:nowrap">SOMA</th>
  </tr>
  <tr>
    <td class="tg-0lax"></td>
    <td class="tg-0lax"></td>
    <td class="tg-0lax"></td>
    <td class="tg-0lax"></td>
    <td class="tg-0lax"></td>
    <td class="tg-0lax"></td>
    <td class="tg-0lax"></td>
    <td class="tg-0lax"></td>
    <td class="tg-0lax"></td>
  </tr>
  <tr>
    <td class="tg-0lax">FLOUR</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">55</td>
    <td class="tg-0lax">60</td>
    <td class="tg-0lax">45</td>
    <td class="tg-0lax">15</td>
    <td class="tg-0lax">55</td>
    <td class="tg-0lax">FREE</td>
    <td class="tg-0lax">45</td>
  </tr>
  <tr>
    <td class="tg-0lax">RICE AND SUGAR&nbsp;&nbsp;50 KG</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">55</td>
    <td class="tg-0lax">60</td>
    <td class="tg-0lax">49</td>
    <td class="tg-0lax">15</td>
    <td class="tg-0lax">55</td>
    <td class="tg-0lax">15</td>
    <td class="tg-0lax">49</td>
  </tr>
  <tr>
    <td class="tg-0lax">ONIONS AND POTATOS 18KG / 20KG</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">27</td>
    <td class="tg-0lax">30</td>
    <td class="tg-0lax">39</td>
    <td class="tg-0lax">10</td>
    <td class="tg-0lax">27</td>
    <td class="tg-0lax">10</td>
    <td class="tg-0lax">19.5</td>
  </tr>
  <tr>
    <td class="tg-0lax">CONDENCED MILK 24*1KG</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">27</td>
    <td class="tg-0lax">30</td>
    <td class="tg-0lax">19</td>
    <td class="tg-0lax">10</td>
    <td class="tg-0lax">20</td>
    <td class="tg-0lax">10</td>
    <td class="tg-0lax">19</td>
  </tr>
  <tr>
    <td class="tg-0lax">EGGS 12 CRATES</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">27</td>
    <td class="tg-0lax">30</td>
    <td class="tg-0lax">19</td>
    <td class="tg-0lax">15</td>
    <td class="tg-0lax">27</td>
    <td class="tg-0lax">10</td>
    <td class="tg-0lax">19</td>
  </tr>
  <tr>
    <td class="tg-0lax">CAN DRINKS&nbsp;&nbsp;24 PIECES</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">12</td>
    <td class="tg-0lax">15</td>
    <td class="tg-0lax">12</td>
    <td class="tg-0lax">10</td>
    <td class="tg-0lax">12</td>
    <td class="tg-0lax">10</td>
    <td class="tg-0lax">10</td>
  </tr>
  <tr>
    <td class="tg-0lax">PASTA</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">30</td>
    <td class="tg-0lax">35</td>
    <td class="tg-0lax">20</td>
    <td class="tg-0lax">10</td>
    <td class="tg-0lax">30</td>
    <td class="tg-0lax">10</td>
    <td class="tg-0lax">20</td>
  </tr>
  <tr>
    <td class="tg-0lax">SARDINES 50 CANS CARTON</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">12</td>
    <td class="tg-0lax">15</td>
    <td class="tg-0lax">10</td>
    <td class="tg-0lax">7</td>
    <td class="tg-0lax">12</td>
    <td class="tg-0lax">7</td>
    <td class="tg-0lax">10</td>
  </tr>
  <tr>
    <td class="tg-0lax">TEA 12 PACKS CARTON</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">27</td>
    <td class="tg-0lax">30</td>
    <td class="tg-0lax">15</td>
    <td class="tg-0lax">7</td>
    <td class="tg-0lax">27</td>
    <td class="tg-0lax">7</td>
    <td class="tg-0lax">15</td>
  </tr>
  <tr>
    <td class="tg-0lax">JUMBO AND MAGGI</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">27</td>
    <td class="tg-0lax">30</td>
    <td class="tg-0lax">12</td>
    <td class="tg-0lax">10</td>
    <td class="tg-0lax">27</td>
    <td class="tg-0lax">10</td>
    <td class="tg-0lax">12</td>
  </tr>
  <tr>
    <td class="tg-0lax">FRUIT JUICE CARTON</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">27</td>
    <td class="tg-0lax">30</td>
    <td class="tg-0lax">12</td>
    <td class="tg-0lax">10</td>
    <td class="tg-0lax">27</td>
    <td class="tg-0lax">10</td>
    <td class="tg-0lax">12</td>
  </tr>
  <tr>
    <td class="tg-0lax">BUTTER, MAYORNAICE AND CHOCLATE 5L, 4.5KG</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">15</td>
    <td class="tg-0lax">20</td>
    <td class="tg-0lax">12</td>
    <td class="tg-0lax">7</td>
    <td class="tg-0lax">15</td>
    <td class="tg-0lax">7</td>
    <td class="tg-0lax">12</td>
  </tr>
  <tr>
    <td class="tg-0lax">DIAPERS 4/5 PACK CARTON</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">15</td>
    <td class="tg-0lax">20</td>
    <td class="tg-0lax">15</td>
    <td class="tg-0lax">7</td>
    <td class="tg-0lax">15</td>
    <td class="tg-0lax">7</td>
    <td class="tg-0lax">12</td>
  </tr>
  <tr>
    <td class="tg-0lax">KETCHUP 24 PIECES CARTON</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">13.5</td>
    <td class="tg-0lax">15</td>
    <td class="tg-0lax">10</td>
    <td class="tg-0lax">5</td>
    <td class="tg-0lax">13.5</td>
    <td class="tg-0lax">5</td>
    <td class="tg-0lax">10</td>
  </tr>
  <tr>
    <td class="tg-0lax">MILK POWDER&nbsp;&nbsp;25KG</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">27</td>
    <td class="tg-0lax">30</td>
    <td class="tg-0lax">15</td>
    <td class="tg-0lax">7</td>
    <td class="tg-0lax">20</td>
    <td class="tg-0lax">7</td>
    <td class="tg-0lax">15</td>
  </tr>
  <tr>
    <td class="tg-0lax">MILK POWDER CAN</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">27</td>
    <td class="tg-0lax">30</td>
    <td class="tg-0lax">15</td>
    <td class="tg-0lax">7</td>
    <td class="tg-0lax">27</td>
    <td class="tg-0lax">7</td>
    <td class="tg-0lax">15</td>
  </tr>
  <tr>
    <td class="tg-0lax">SPAGHETTI</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">15</td>
    <td class="tg-0lax">20</td>
    <td class="tg-0lax">12</td>
    <td class="tg-0lax">7</td>
    <td class="tg-0lax">15</td>
    <td class="tg-0lax">7</td>
    <td class="tg-0lax">12</td>
  </tr>
  <tr>
    <td class="tg-0lax">CANDIES, AWEETS AND SNACKS</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">15</td>
    <td class="tg-0lax">20</td>
    <td class="tg-0lax">12</td>
    <td class="tg-0lax">5</td>
    <td class="tg-0lax">15</td>
    <td class="tg-0lax">5</td>
    <td class="tg-0lax">12</td>
  </tr>
  <tr>
    <td class="tg-0lax">EVAPORATED MILK CARTON</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">27</td>
    <td class="tg-0lax">30</td>
    <td class="tg-0lax">19</td>
    <td class="tg-0lax">10</td>
    <td class="tg-0lax">27</td>
    <td class="tg-0lax">10</td>
    <td class="tg-0lax">19</td>
  </tr>
  <tr>
    <td class="tg-0lax">BISCUITS</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">15</td>
    <td class="tg-0lax">20</td>
    <td class="tg-0lax">12</td>
    <td class="tg-0lax">7</td>
    <td class="tg-0lax">15</td>
    <td class="tg-0lax">7</td>
    <td class="tg-0lax">12</td>
  </tr>
  <tr>
    <td class="tg-0lax">COOKING OIL 20 LTRS</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">27</td>
    <td class="tg-0lax">30</td>
    <td class="tg-0lax">19</td>
    <td class="tg-0lax">10</td>
    <td class="tg-0lax">27</td>
    <td class="tg-0lax">7</td>
    <td class="tg-0lax">19</td>
  </tr>
  <tr>
    <td class="tg-0lax">COOKING OIL 10 LTRS</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">12</td>
    <td class="tg-0lax">15</td>
    <td class="tg-0lax">10</td>
    <td class="tg-0lax">7</td>
    <td class="tg-0lax">12</td>
    <td class="tg-0lax">5</td>
    <td class="tg-0lax">10</td>
  </tr>
  <tr>
    <td class="tg-0lax">COOKING OIL 5 LTRS</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">7</td>
    <td class="tg-0lax">10</td>
    <td class="tg-0lax">5</td>
    <td class="tg-0lax">5</td>
    <td class="tg-0lax">7</td>
    <td class="tg-0lax">3</td>
    <td class="tg-0lax">5</td>
  </tr>
  <tr>
    <td class="tg-0lax">FROZEN CHICKEN 10 KG/ 15KG</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">25</td>
    <td class="tg-0lax">30</td>
    <td class="tg-0lax">20</td>
    <td class="tg-0lax">15</td>
    <td class="tg-0lax">25</td>
    <td class="tg-0lax">10</td>
    <td class="tg-0lax">20</td>
  </tr>
  <tr>
    <td class="tg-0lax">TOMATO PASTE</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">17</td>
    <td class="tg-0lax">20</td>
    <td class="tg-0lax">12</td>
    <td class="tg-0lax">7</td>
    <td class="tg-0lax">17</td>
    <td class="tg-0lax">5</td>
    <td class="tg-0lax">12</td>
  </tr>
</table>
                </div>
    </div>
    <div id="menu2" class="tab-pane fade">
    <div class="card-body">
                <style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:12px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ddd;}
.tg th{font-family:Arial, sans-serif;font-size:12px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ddd;}
.tg .tg-0lax{text-align:left;vertical-align:top}
</style>
<table class="tg">
  <tr>
    <th class="tg-0lax" style="white-space:nowrap">ITEM</th>
    <th class="tg-0lax" style="white-space:nowrap">UNIT</th>
    <th class="tg-0lax" style="white-space:nowrap">BANSANG</th>
    <th class="tg-0lax" style="white-space:nowrap">BASSE</th>
    <th class="tg-0lax" style="white-space:nowrap">FARRENNI</th>
    <th class="tg-0lax" style="white-space:nowrap">BRIKAMA</th>
    <th class="tg-0lax" style="white-space:nowrap">GBA</th>
    <th class="tg-0lax" style="white-space:nowrap">BRIKAMA BA</th>
    <th class="tg-0lax" style="white-space:nowrap">SOMA</th>
  </tr>
  <tr>
    <td class="tg-0lax"></td>
    <td class="tg-0lax"></td>
    <td class="tg-0lax"></td>
    <td class="tg-0lax"></td>
    <td class="tg-0lax"></td>
    <td class="tg-0lax"></td>
    <td class="tg-0lax"></td>
    <td class="tg-0lax"></td>
    <td class="tg-0lax"></td>
  </tr>
  <tr>
    <td class="tg-0lax">CEMENT AND WHITE CEMENT</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">55</td>
    <td class="tg-0lax">60</td>
    <td class="tg-0lax">45</td>
    <td class="tg-0lax">20</td>
    <td class="tg-0lax">15</td>
    <td class="tg-0lax">55</td>
    <td class="tg-0lax">45</td>
  </tr>
  <tr>
    <td class="tg-0lax">IRON RODS</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">7</td>
    <td class="tg-0lax">10</td>
    <td class="tg-0lax">5</td>
    <td class="tg-0lax">5</td>
    <td class="tg-0lax">3</td>
    <td class="tg-0lax">7</td>
    <td class="tg-0lax">5</td>
  </tr>
  <tr>
    <td class="tg-0lax">PLYWOOD</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">30</td>
    <td class="tg-0lax">40</td>
    <td class="tg-0lax">20</td>
    <td class="tg-0lax">10</td>
    <td class="tg-0lax">7</td>
    <td class="tg-0lax">30</td>
    <td class="tg-0lax">20</td>
  </tr>
  <tr>
    <td class="tg-0lax">HARDWOOD</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">27</td>
    <td class="tg-0lax">32</td>
    <td class="tg-0lax">17</td>
    <td class="tg-0lax">10</td>
    <td class="tg-0lax">5</td>
    <td class="tg-0lax">27</td>
    <td class="tg-0lax">17</td>
  </tr>
  <tr>
    <td class="tg-0lax">PAINT 18 LITRS DRUM</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">27</td>
    <td class="tg-0lax">32</td>
    <td class="tg-0lax">17</td>
    <td class="tg-0lax">10</td>
    <td class="tg-0lax">5</td>
    <td class="tg-0lax">27</td>
    <td class="tg-0lax">17</td>
  </tr>
  <tr>
    <td class="tg-0lax">PAINT 3.6 LTRS * 4</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">30</td>
    <td class="tg-0lax">35</td>
    <td class="tg-0lax">20</td>
    <td class="tg-0lax">12</td>
    <td class="tg-0lax">8</td>
    <td class="tg-0lax">30</td>
    <td class="tg-0lax">20</td>
  </tr>
  <tr>
    <td class="tg-0lax">WALL TILE</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">15</td>
    <td class="tg-0lax">20</td>
    <td class="tg-0lax">10</td>
    <td class="tg-0lax">10</td>
    <td class="tg-0lax">8</td>
    <td class="tg-0lax">15</td>
    <td class="tg-0lax">10</td>
  </tr>
  <tr>
    <td class="tg-0lax">FLOOR TILE 30 * 30</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">15</td>
    <td class="tg-0lax">20</td>
    <td class="tg-0lax">10</td>
    <td class="tg-0lax">8</td>
    <td class="tg-0lax">5</td>
    <td class="tg-0lax">15</td>
    <td class="tg-0lax">10</td>
  </tr>
  <tr>
    <td class="tg-0lax">REGULAR CORRUGATE 20 SHEETS PACK</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">50</td>
    <td class="tg-0lax">60</td>
    <td class="tg-0lax">45</td>
    <td class="tg-0lax">20</td>
    <td class="tg-0lax">15</td>
    <td class="tg-0lax">50</td>
    <td class="tg-0lax">45</td>
  </tr>
  <tr>
    <td class="tg-0lax">NAILS CARTON</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">30</td>
    <td class="tg-0lax">35</td>
    <td class="tg-0lax">20</td>
    <td class="tg-0lax">10</td>
    <td class="tg-0lax">7</td>
    <td class="tg-0lax">30</td>
    <td class="tg-0lax">20</td>
  </tr>
  <tr>
    <td class="tg-0lax">COLORED CORRUGATE 20 SHEETS PACK</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">100</td>
    <td class="tg-0lax">110</td>
    <td class="tg-0lax">75</td>
    <td class="tg-0lax">50</td>
    <td class="tg-0lax">30</td>
    <td class="tg-0lax">100</td>
    <td class="tg-0lax">75</td>
  </tr>
</table>
                </div>
    </div>
  </div>
</div>
          </div>
        </div>        

			</div>
			<div class="col-sm-4 left_menu" style="margin-top:10px;">
      <ul class="shopping-cart-items floating-cart" style="width:350px;">
        <div class="" id="cartDetails">
                          
        </div>
      </ul>
			</div>
		</div>
	</div>
</section>
<script src="<?php echo base_url(); ?>assets/custom_js/placeorder.js?version=<?php echo time(); ?>"></script>