<section class="payment_page">
	<div class="container">
		<div class="row">
    <h2>My Shopping Cart</h2>
      <div class="col-sm-12 col-md-10 col-md-offset-1">        
        
          <table class="table table-hover cart-table" id="myTable">
          
            <thead>
              <tr>
                <th width="45%" class="text-center">Product</th>
                <th width="18%" class="text-center">Price</th>
                <th width="20%" class="text-center">Quantity</th>
                <th width="15%" class="text-center">Total</th>
                <th width="15%"></th>
              </tr>
            </thead>
            <tbody id="myTabletbody">
            </tbody>
            <tfoot>
                            <tr>
                                <td><h3 class="mbl">Total</h3></td>
                                <td><h3 class="mbl finalPayment">$0.0</h3></td>
                                <td>   </td>
                                <td><h3 class="fll">Total</h3></td>
                                <td class="text-right"><h3 class="fll finalPayment">$0.0</h3></td>
                            </tr>
                            <tr>
                                <td colspan="2" class="text-right gmdVal mbll">   </td>
                                <td>   </td>
                                <td colspan="3" class="text-right gmdVal flll"></td>
                            </tr>
                            <tr>
                                <td><a href="<?php echo base_url(); ?>localsupplier" type="button" class="mbl btn btn-default">
                                      <span class="glyphicon glyphicon-shopping-cart"></span> Continue Shopping
                                  </a>   </td>
                                <td> <a href="<?php echo base_url(); ?>checkout/placeorder" type="button" class="mbl btn btn-warning text-white" id="placeorderbut" >
                                      Checkout <span class="glyphicon glyphicon-play"></span>
                                  </a>  </td>
                                <td>   </td>
                                <td>
                                  <a href="<?php echo base_url(); ?>localsupplier" type="button" class="fll btn btn-default">
                                      <span class="glyphicon glyphicon-shopping-cart"></span> Continue Shopping
                                  </a>
                                </td>
                                <td>
                                  <a href="<?php echo base_url(); ?>checkout/placeorder" type="button" class="fll btn btn-warning text-white" id="placeorderbut" >
                                      Checkout <span class="glyphicon glyphicon-play"></span>
                                  </a>
                                </td>
                            </tr>
                        </tfoot>            
          </table>
        
      </div>
			
			<!-- <div class="col-sm-4 left_menu">
				<div class="group_pay" id="cartDetails">
									
					
				</div>
			</div> -->
		</div>
	</div>
</section>
<script src="<?php echo base_url(); ?>assets/custom_js/viewcart.js?version=<?php echo time(); ?>"></script>