<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Checkout extends CI_Controller {
	protected $salt='HA{t~xOC;07Dp,Q+$y02F??:}/?LMj_';
	function __construct() {
		parent::__construct();
		ERROR_REPORTING('E_ALL^E_NOTICE');
	}

	public function index(){
		$params   = $_SERVER['QUERY_STRING'];
		parse_str($params, $params_array);
		
		
		
		$data['uriParams'] = $params_array;
		
		$data['view_file'] = 'viewcart';
        view($data);
	}

	public function viewcart(){
		$params   = $_SERVER['QUERY_STRING'];
		parse_str($params, $params_array);	
		
		
		$data['uriParams'] = $params_array;
		
		$data['view_file'] = 'viewcart';
        view($data);
	}

	public function placeorder(){
		$params   = $_SERVER['QUERY_STRING'];
		parse_str($params, $params_array);
		
		
		
		$data['uriParams'] = $params_array;
		
		$data['view_file'] = 'placeorder';
        view($data);
	}



	
}