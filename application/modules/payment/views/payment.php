<section class="payment_page">
	<div class="container">
		<div class="row">
			<div class="col-sm-8">
				<div class="group_pay">
					<div class="pay_heading back_color">
			          <h4>PAYMENT</h4>          
			        </div>
			        <div class="payment_form_design">
			        	
			        	<form action="javascript:void(0);" method="post" name="send_checkoutForm" id="send_checkoutForm">
      
        <div class="row">
          <div class="col-md-6">
            <h3>Billing Address</h3>
            <label for="fname"><i class="fa fa-user"></i> Full Name</label>
            <input type="text" id="fname" name="firstname" placeholder="John M. Doe">
            <label for="email"><i class="fa fa-envelope"></i> Email</label>
            <input type="text" id="email" name="email" placeholder="john@example.com">
            <label for="adr"><i class="fa fa-address-card-o"></i> Address</label>
            <input type="text" id="adr" name="address" placeholder="542 W. 15th Street">
            <label for="city"><i class="fa fa-institution"></i> City</label>
            <input type="text" id="city" name="city" placeholder="New York">

            <div class="row">
              <div class="col-md-6">
                <label for="state">State</label>
                <input type="text" id="state" name="state" placeholder="NY">
              </div>
              <div class="col-md-6">
                <label for="zip">Zip</label>
                <input type="text" id="zip" name="zip" placeholder="10001">
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <h3>Payment</h3>
            <label for="fname">Accepted Cards</label>
            <div class="icon-container">
              <i class="fa fa-cc-visa" style="color:navy;"></i>
              <i class="fa fa-cc-amex" style="color:blue;"></i>
              <i class="fa fa-cc-mastercard" style="color:red;"></i>
              <i class="fa fa-cc-discover" style="color:orange;"></i>
            </div>
            <label for="cname">Name on Card</label>
            <input type="text" id="cname" name="cardname" placeholder="John More Doe">
            <label for="ccnum">Credit card number</label>
            <input type="text" id="ccnum" name="cardnumber" placeholder="1111-2222-3333-4444">
            <label for="expmonth">Exp Month</label>
            <input type="text" id="expmonth" name="expmonth" placeholder="September">
            <div class="row">
              <div class="col-md-6">
                <label for="expyear">Exp Year</label>
                <input type="text" id="expyear" name="expyear" placeholder="2018">
              </div>
              <div class="col-md-6">
                <label for="cvv">CVV</label>
                <input type="text" id="cvv" name="cvv" placeholder="352">
              </div>
            </div>
          </div>
          
        </div>
        <label>
          <input type="checkbox" checked="checked" name="sameadr"> Shipping address same as billing
        </label>
        <input type="submit" value="Confirm & Pay Now" class="btn" id="confirmPayButton">
      </form>
			        </div>
				</div>
			</div>
			<div class="col-sm-4 left_menu">
				<div class="group_pay">
					<div class="right_pay_head">
						<div class="pay_img"><a href="javascript:void(0);" id="proposal_doc_link" target="_blank" title="view proposal details"><img src="<?php echo base_url(); ?>assets/images/icon/proposal-icon.png" alt=""></a></div>
						<div class="pay_cnt"><a href="javascript:void(0);" id="proposal_title" target="_blank" title="view proposal details">Proposal title.</a></div>
					</div>
					<div class="payment_page_info">
					<p id="proposal_desc"></p>
					</div>
					<div class="payment_page_info">
						<p class="pay_title">Date</p>
						<p class="pay_value" id="proposal_date">dd-mm-yyyy</p>
					</div>
					<div class="payment_page_info">
						<h5 class="pay_title"><b>Total To Pay</b></h5>
						<h5 class="pay_value" id="proposal_amount">0</h5>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<input type="hidden" name="proposal_id" id="proposal_id" value="<?php echo $uriParams['token']; ?>">
<script src="<?php echo base_url(); ?>assets/custom_js/proposal_payment.js?version=<?php echo time(); ?>"></script>