<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Category_Model extends CI_Model{

	public function __construct() {
        parent::__construct();
	}
	
	protected $returnArr = [];
    
    public function buildCategory($parent, $category) {
		$html = "";
		
		if (isset($category['parent_cats'][$parent])) {			
			$html .= "<ul>\n";
			foreach ($category['parent_cats'][$parent] as $cat_id) {
				if (!isset($category['parent_cats'][$cat_id])) {
					$html .= "<li>\n  <a href='../../supplier/view/" . base64_encode($category['categories'][$cat_id]['category_id']) . "'>" . $category['categories'][$cat_id]['category name'] . "</a>\n</li> \n";
				}
				if (isset($category['parent_cats'][$cat_id])) {
					$html .= "<li>\n  <a href='../../supplier/view/" . base64_encode($category['categories'][$cat_id]['category_id']) . "'>" . $category['categories'][$cat_id]['category name'] . "</a> \n";
					$html .= $this->buildCategory($cat_id, $category);
					$html .= "</li> \n";
				}
			}
			$html .= "</ul> \n";
		}
		return $html;
	}

	public function getParentMappingOneQuery($type)
	{
		if($type == 'products')
		{
			$typeSql = "WHERE `cat`.`type` = '1'";
		}elseif($type == 'services')
		{
			$typeSql = "WHERE `cat`.`type` = '2'";

		}else{
			$typeSql = "";
		}

		if($this->session->userdata('lang') == 'en')
		{
			$catSql = "`cat`.`category_name_en` AS 'category name',";
		}elseif($this->session->userdata('lang') == 'fr')
		{
			$catSql = "`cat`.`category_name_fr` AS 'category name',";
		}else{
			$catSql = "`cat`.`category_name_en` AS 'category name',";
		}
		
		$sql = "SELECT
					$catSql
					`cat`.`id` AS 'category_id',
					`cat2`.`category_name_en` AS 'parent category',
					`cat2`.`id` AS 'parent_category_id'
				FROM
					`t_category` AS `cat`
				LEFT JOIN `t_category` AS `cat2` ON `cat`.`parent_id` = `cat2`.`id`
				$typeSql
				AND `cat`.`active`='1'
				ORDER BY
					'parent category'";
		

		$query = $this->db->query($sql);

		//create a multidimensional array to hold a list of category and parent category
		$category = array(
			'categories' => array(),
			'parent_cats' => array()
		);

		//build the array lists with data from the category table
		foreach($query->result_array() as $row){
			//creates entry into categories array with current category id ie. $categories['categories'][1]
			$category['categories'][$row['category_id']] = $row;
			if($row['parent_category_id'] > 0)
			{
				//creates entry into parent_cats array. parent_cats array contains a list of all categories with children
			$category['parent_cats'][$row['parent_category_id']][] = $row['category_id'];
			}else{
				$category['parent_cats'][0][] = $row['category_id'];
			}
			
		}
		

		

		return $category;
	}


	public function getParentMapping($parent_id=0)
	{
		try {
			$result=[];
			 if($parent_id > 0)
			 {
				$this->db->db_debug = true;
				$this->db->select("c.id,c.category_name_en,c.category_name_fr,c.parent_id");
				$this->db->from('category c' );
				$this->db->where("c.active",1);
				$this->db->where("c.is_approved",1);
				$this->db->where("c.id",$parent_id);
				$query=$this->db->get();
				$db_error = $this->db->error();
				
				if (!empty($db_error) && $db_error['code']>0) 
				{
					throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
					return false; // unreachable retrun statement !!!
					
				}else{

					$listcat = $query->result_array();
					
					if(is_array($listcat) && count($listcat)>0)
					{
						$i=0;
						while($listcat[$i]){
							$this->returnArr[] = json_encode($listcat[$i]);
							$this->getParentMapping($listcat[$i]['parent_id']);
							$i++;
						}
					}
				}
			 }			

			return array_reverse($this->returnArr);
		}catch (Exception $e) {
	       throw new Exception();
	    }
		
	}
    


}