<script src="<?php echo base_url();?>assets/js/main.js" type="text/javascript"></script> 
<script src="<?php echo base_url();?>assets/custom_js/viewCategory.js?version=<?php echo time(); ?>" type="text/javascript"></script> 
<input type="hidden" name="viewCatId" id="viewCatId" value="<?php echo $cat_id; ?>">
<input type="hidden" name="enableLocalSupplier" id="enableLocalSupplier" value="0">
<section class="category_list">
	<div class="container">
		<div class="row group_category">
			<div class="col-md-12">
				<div class="bradecap">
					<ul>
						<li><p>You are In: Products</p></li>
                        <?php
                           if(is_array($mappingArr) && count($mappingArr)>0)
                           {
                              foreach($mappingArr as $k=>$v)
                              {
                            ?>
                               <li><a href="<?php echo base_url();?>category/view/<?php echo base64_encode($v['id']); ?>"><?php echo $v['category_name'] ?></a></li> 
                            <?php      
                              }
                           } 
                        ?>
						<li class="viewsupplier"><a href="<?php echo base_url();?>supplier/view/<?php echo $cat_id; ?>">View Suppliers</a></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="row group_category" id="sliderCategoryPanel">
			<div class="col-md-12">
				<div class="owl-carousel sub_category_slider" id="sliderCategory">
                <div class="loading"><img src="<?php echo base_url();?>assets/images/lg.ring-loading-gif.gif" alt="Loading Category"></div>
                </div>
			</div>			
		</div>
		
		<div class="row other_category_item" id="categoryitemsPanel">
		<div class="col-md-12">
		<div class="local_suppliers_cat">
					<div class="custom-control custom-radio custom-control-inline">
						<input type="checkbox" class="custom-control-input" id="localsupplier" name="localsupplier" value="1" onChange="javascript:viewLocalSupplierOnly(this);"><label class="custom-control-label" for="localsupplier">From Local Suppliers</label>
					</div>
	   
				</div>			   
		</div>
		<div class="col-md-12" id="categoryitemsContent">
		<div class="loading"><img src="<?php echo base_url();?>assets/images/lg.ring-loading-gif.gif" alt="Loading Items"></div>			   
		</div>
		
		</div>
	</div>
</section>