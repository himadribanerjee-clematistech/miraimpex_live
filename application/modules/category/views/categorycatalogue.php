<section class="category_list">
	<div class="container">
    <div class="row group_category">
			<div class="col-md-12">
				<div class="bradecap">
					<ul>
						<li><p>You are In:</p></li>
                        <li><a href="<?php echo base_url();?>">Suppliers</a></li> 
                        <li><a href="javascript:void(0);">All Categories</a></li> 

					</ul>
				</div>
			</div>
		</div>
    <div class="row group_category" id="sliderCategoryPanel">
        <div class="col-md-12">
            <div class="owl-carousel sub_category_slider" id="sliderCategory">
            <div class="loading"><img src="<?php echo base_url();?>assets/images/lg.ring-loading-gif.gif" alt="Loading Category"></div>
            </div>
        </div>

        
    </div>    
    <div class="row group_category">
    <div class="col-md-12" id="categoryTree">

        <?php echo $catHtml; ?>
    </div>
    </div>
    </div>
</section>    
<input type="hidden" name="typeToken" id="typeToken" value="<?php echo $typeToken; ?>">
<script src="<?php echo base_url(); ?>assets/custom_js/categorycatalogue.js?version=<?php echo time(); ?>"></script>