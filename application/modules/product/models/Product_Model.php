<?php

class Product_Model extends CI_Model {


	function __construct() {
		parent::__construct();
	}
   
    public function categoryBreadcumbsPath($catID){
		
	    $html='';
	
		$list=array();
		if($catID>0){
			    
			    getParent :{ 
			   
					    $catData=$this->getCategoryById($catID);  
						$row=array();
						$row['id']=$catData['id'];
						$row['name']=$catData['category_name_'.$this->session->userdata('lang')];	
						$catID=$catData['parent_id'];
						$list[]=$row;
	                    if($catData['parent_id']>0)																 
						goto getParent;
				}
				
				
		}
		$list=array_reverse($list);
		$catIdArr=array();
		if(sizeof($list)>0){
			$html.='<ul><li>You are In: </li>';
			foreach($list as $data){
				$html.='<li><a href="'.base_url().'category/view/'.base64_encode($data['id']).'">'.$data['name'].'</a></li>';
                $catIdArr[]=trim($data['id']);
			}
			$html.='</ul>';
		}

					
		return array('html'=>$html,'catIdArr'=>$catIdArr);
	}
	
	
	private function getCategoryById($catID){
		
		$this->db->select('category_name_en,category_name_fr,id,parent_id');
		$this->db->where('active','1');
		$this->db->where('type','1');
	    $this->db->where('id',$catID);
		$category_list=$this->db->get('category')->row_array(); 
		return $category_list;
	}
	
	public function getProductCategoryId($itemId){
		
		$this->db->select('category_id');
		$this->db->where('active','1');
	    $this->db->where('id',$itemId);
		$record=$this->db->get('item')->row_array(); 
		if(!empty($record)){
			return $record['category_id'];
		}
		else
			return 0;
		
	}
	
	
}