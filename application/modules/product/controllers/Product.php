<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

	protected $token_id='';

    public function __construct() {
        parent::__construct(); 
		$this->load->model('Product_Model'); 
        error_reporting('E_ALL^E_NOTICE');
    }


    public function index() {} 

    public function details($slug) {

        $this->load->helper('url');
        $currentURL = current_url();
        $params   = $_SERVER['QUERY_STRING'];
        parse_str($params, $params_array);

        $data['uriParams'] = $params_array;
        
		
		if($this->uri->segment(3)!=null)
		  $itemID=$this->uri->segment(3);
		else
		  $itemID=0;
		
		// $itemId=base64_decode($itemID);
		$itemId=$itemID;
		$data['itemID'] =$itemId;
		$catId=$this->Product_Model->getProductCategoryId($itemId);
		$catPath=$this->Product_Model->categoryBreadcumbsPath($catId);  

		
        $data['view_file'] = 'ProductDetails';
		$data['catPath']=$catPath['html'];
		// print_r($data);
        view($data);
    }


    
    

    
}