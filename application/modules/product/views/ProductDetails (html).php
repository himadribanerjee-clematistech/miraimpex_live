<!-- <input type="hidden" name="itemID" id="itemID" value="<?php //echo $itemID; ?>"> -->

<section class="category_list products_page">
	<div class="container">
    <div class="row group_category">
			<div class="col-md-12">
				<div class="bradecap">
					<?php echo $catPath;?>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="wrapper-main brandshop clearfix">
        	<div class="spacer15"></div><!--spacer-->
        	<div class="container">
            	<div class="inner-block"><!------Main Inner-------->
                	<div class="row">
                        <div class="col-md-9 col-sm-8">
                        	<div class="main-contant xs-spacer20 clearfix">
                            	<div class="contant-wrapper">
                                    <div class="details-view"><!-- Start Product Details -->
                                        <div class="clearfix">
                                            <div class="product-img"><!-- Product Images -->
                                                <div id="info-img">
													<!---
                                                    <span class="pro offer">30% off</span>
                                                    --->
                                                    <div class="swiper-container top-img">
                                                        <div class="swiper-wrapper">
															  <section id="default" class="padding-top0">
																    <div>

																      <div class="large-5 column">
																        <div class="xzoom-container">
																          <img class="xzoom" id="xzoom-default" src="<?php echo base_url();?>assets/images/images-700X880.png" xoriginal="<?php echo base_url();?>assets/images/images-700X880.png" />

																          <div class="xzoom-thumbs">
																            <a href="<?php echo base_url();?>assets/images/images-700X880.png" src="<?php echo base_url();?>assets/images/images-700X880.png">

																            	<img class="xzoom-gallery"  src="<?php echo base_url();?>assets/images/images-700X880.png" src="<?php echo base_url();?>assets/images/images-700X880.png"  xpreview="<?php echo base_url();?>assets/images/images-700X880.png" src="<?php echo base_url();?>assets/images/images-700X880.png" title="The description goes here"></a>
																              
																            <a href="<?php echo base_url();?>assets/images/images-700X880.png" src="images/images-700X880.png">
																            	<img class="xzoom-gallery" src="<?php echo base_url();?>assets/images/images-700X880.png" src="<?php echo base_url();?>assets/images/images-700X880.png" title="The description goes here"></a>
																              
																            <a href="<?php echo base_url();?>assets/images/images-700X880.png" src="<?php echo base_url();?>assets/images/images-700X880.png"><img class="xzoom-gallery" src="<?php echo base_url();?>assets/images/images-700X880.png" src="<?php echo base_url();?>assets/images/images-700X880.png" title="The description goes here"></a>
																              
																            <a href="<?php echo base_url();?>assets/images/images-700X880.png" src="<?php echo base_url();?>assets/images/images-700X880.png"><img class="xzoom-gallery" src="<?php echo base_url();?>assets/images/images-700X880.png" src="<?php echo base_url();?>assets/images/images-700X880.png" title="The description goes here"></a>
																          </div>
																        </div>        
																      </div>
																      <div class="large-7 column"></div>
																    </div>
																    </section>
                                                        </div>
                                                        
                                                    </div>
                                                    
                                                </div>
                                            </div><!-- End Product Images -->
                                            <div class="product-info">
                                                <h4 id="productName"></h4>
                                                
                                             
                                                <div  class="attribute clearfix">
                                                    <div class="price-box">
                                                    	<p class="new-price"><span><i class="fa fa-dollar"></i> <span id="price_min"></span> </span></p>
                                                    	<p class="new-price">-</p>
                                                    	<p class="new-price"><span><i class="fa fa-dollar"></i> <span id="price_max"></span></span></p>
                                                    	<p class="new-price">/ Piece | 1 Piece/Pieces (Min. Order)</p>
                                                    
                                                    </div>
	                                                <div><fieldset class="attribute_fieldset"> 
	                                                        <label class="attribute_label">Size:</label>
	                                                        <div class="attribute_list"> 
	                                                            <ul class="attribute_size">
	                                                                <li class="active"><a href="#">S</a></li>
	                                                                <li><a href="#">M</a></li>
	                                                                <li><a href="#">L</a></li>
	                                                                <li><a href="#">XS</a></li>
	                                                                <li><a href="#">XL</a></li>
	                                                            </ul>
	                                                        </div>
	                                                    </fieldset></div>


		                                                <div><fieldset class="attribute_fieldset"> 
		                                                        <label class="attribute_label">Color:</label>
		                                                        <div class="attribute_list"> 
		                                                            <ul class="attribute_color">
		                                                                <li class="active"><a href="#" class="_1"></a></li>
		                                                                <li><a href="#" class="_2"></a></li>
		                                                                <li><a href="#" class="_3"></a></li>
		                                                                <li><a href="#" class="_4"></a></li>
		                                                            </ul>
		                                                        </div>
		                                                    </fieldset></div>
                                                    
                                                
                                                </div>
                                                <div class="attribute clearfix Customization_info">
                                                	<div class="custo_item">
                                                		<div class="custo_heading"><p>Customization:</p></div>
                                                		<div class="custo_value"><p>Customized logo <span>(Min. Order: 1 Pieces)</span></p></div>
                                                	</div>
                                                	<div class="custo_item">
                                                		<div class="custo_heading"><p>Samples:</p></div>
                                                		<div class="custo_value"><p>₹ 367.28 /Piece, 1 Piece<span>(Min. Order)</span></p></div>
                                                	</div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div><!-- End Product Details -->
                                    <div class="spacer30"></div><!--spacer-->
                                    <div class="tab-panel clearfix"><!-- Tab -->
                                    	<div class="products_heading"><h4>You may also like</h4></div>
										<div class="owl-carousel product_details_sld">
		                                    <div class="item">
		                                    	<div><a href="#"><img src="http://192.168.2.95/miraimpex/admin/uploads/category/1579850985.jpeg" alt="img-1"/></a></div>
		                                        <div class="products_slider_info">
		                                        	<div><a href="#">OEM Hoodie Sweatshirt Cotton Long Sleeve Custom Logo Printed Oversize Pullover Hoodies</a></div>
		                                        	<div><h5>Ready to Ship</h5></div>
		                                        	<div><h4>₹ 505.92 - ₹ 681.89</h4></div>
		                                        	<div><p>1 Piece <span>(MOQ)</span></p></div>
		                                        </div>
		                                        
		                                    </div>
		                                    <div class="item">
		                                    	<div><a href="#"><img src="http://192.168.2.95/miraimpex/admin/uploads/category/1579850985.jpeg" alt="img-1"/></a></div>
		                                        <div class="products_slider_info">
		                                        	<div><a href="#">OEM Hoodie Sweatshirt Cotton Long Sleeve Custom Logo Printed Oversize Pullover Hoodies</a></div>
		                                        	<div><h5>Ready to Ship</h5></div>
		                                        	<div><h4>₹ 505.92 - ₹ 681.89</h4></div>
		                                        	<div><p>1 Piece <span>(MOQ)</span></p></div>
		                                        </div>
		                                        
		                                    </div>
		                                    <div class="item">
		                                    	<div><a href="#"><img src="http://192.168.2.95/miraimpex/admin/uploads/category/1579850985.jpeg" alt="img-1"/></a></div>
		                                        <div class="products_slider_info">
		                                        	<div><a href="#">OEM Hoodie Sweatshirt Cotton Long Sleeve Custom Logo Printed Oversize Pullover Hoodies</a></div>
		                                        	<div><h5>Ready to Ship</h5></div>
		                                        	<div><h4>₹ 505.92 - ₹ 681.89</h4></div>
		                                        	<div><p>1 Piece <span>(MOQ)</span></p></div>
		                                        </div>
		                                        
		                                    </div>
		                                    <div class="item">
		                                    	<div><a href="#"><img src="http://192.168.2.95/miraimpex/admin/uploads/category/1579850985.jpeg" alt="img-1"/></a></div>
		                                        <div class="products_slider_info">
		                                        	<div><a href="#">OEM Hoodie Sweatshirt Cotton Long Sleeve Custom Logo Printed Oversize Pullover Hoodies</a></div>
		                                        	<div><h5>Ready to Ship</h5></div>
		                                        	<div><h4>₹ 505.92 - ₹ 681.89</h4></div>
		                                        	<div><p>1 Piece <span>(MOQ)</span></p></div>
		                                        </div>
		                                        
		                                    </div>
		                                    <div class="item">
		                                    	<div><a href="#"><img src="http://192.168.2.95/miraimpex/admin/uploads/category/1579850985.jpeg" alt="img-1"/></a></div>
		                                        <div class="products_slider_info">
		                                        	<div><a href="#">OEM Hoodie Sweatshirt Cotton Long Sleeve Custom Logo Printed Oversize Pullover Hoodies</a></div>
		                                        	<div><h5>Ready to Ship</h5></div>
		                                        	<div><h4>₹ 505.92 - ₹ 681.89</h4></div>
		                                        	<div><p>1 Piece <span>(MOQ)</span></p></div>
		                                        </div>
		                                        
		                                    </div>
		                                    
		                                    
		                                </div>
                                    </div>
                                    <div class="spacer30"></div><!--spacer-->
                                    <div class="tab-panel clearfix"><!-- Tab -->
                                    	<div class="products_heading"><h4>Suppliers Certificate</h4></div>
                                    	
									    <section id="gallery">
										    <div id="image-gallery">
										      <div class="row">

										        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 image">
										          <div class="img-wrapper">
										            <a href="<?php echo base_url();?>assets/images/Certificate/Certificate.jpg">
										            	<img src="<?php echo base_url();?>assets/images/Certificate/Certificate.jpg" class="img-responsive">
										            </a>
										            <div class="img-overlay">
										              <i class="fa fa-plus-circle" aria-hidden="true"></i>
										            </div>
										          </div>
										        </div>
										        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 image">
										          <div class="img-wrapper">
										            <a href="<?php echo base_url();?>assets/images/Certificate/Certificate.jpg">
										            	<img src="<?php echo base_url();?>assets/images/Certificate/Certificate.jpg" class="img-responsive">
										            </a>
										            <div class="img-overlay">
										              <i class="fa fa-plus-circle" aria-hidden="true"></i>
										            </div>
										          </div>
										        </div>
										        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 image">
										          <div class="img-wrapper">
										            <a href="<?php echo base_url();?>assets/images/Certificate/Certificate.jpg">
										            	<img src="<?php echo base_url();?>assets/images/Certificate/Certificate.jpg" class="img-responsive">
										            </a>
										            <div class="img-overlay">
										              <i class="fa fa-plus-circle" aria-hidden="true"></i>
										            </div>
										          </div>
										        </div>
										        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 image">
										          <div class="img-wrapper">
										            <a href="<?php echo base_url();?>assets/images/Certificate/Certificate.jpg">
										            	<img src="<?php echo base_url();?>assets/images/Certificate/Certificate.jpg" class="img-responsive">
										            </a>
										            <div class="img-overlay">
										              <i class="fa fa-plus-circle" aria-hidden="true"></i>
										            </div>
										          </div>
										        </div>
										        
										      </div><!-- End row -->
										    </div><!-- End image gallery -->
										  
										</section>
                                    </div>



        							<div class="spacer30"></div><!--spacer-->
                                    <div class="tab-panel clearfix"><!-- Tab -->
                                        <!-- Tabs Nav -->
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li role="presentation" class="active"><a href="#description" aria-controls="description" role="tab" data-toggle="tab">Products Description</a></li>
                                            <li role="presentation"><a href="#reviews" aria-controls="reviews" role="tab" data-toggle="tab">Comments</a></li>
                                            <li role="presentation"><a href="#tags" aria-controls="tags" role="tab" data-toggle="tab">Tags</a></li>
                                        </ul>
                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            <div id="description" class="tab-pane active fade in" role="tabpanel">
                                                <div class="pro_desc_info">
                                                	<div>
                                                		<ul>
	                                                    	<h4>Quick Details</h4>
	                                                    	<li><p>Technics: </p><p class="value">Printed</p></li>
	                                                    	<li><p>Technics: </p><p class="value">Printed</p></li>
	                                                    	<li><p>Technics: </p><p class="value">Printed</p></li>
	                                                    	<li><p>Technics: </p><p class="value">Printed</p></li>
	                                                    	<li><p>Technics: </p><p class="value">Printed</p></li>
	                                                    </ul>
                                                	</div>
                                                	<div>
                                                		<ul>
	                                                    	<h4>Quick Details</h4>
	                                                    	<li><p>Technics: </p><p class="value">Printed</p></li>
	                                                    	<li><p>Technics: </p><p class="value">Printed</p></li>
	                                                    	<li><p>Technics: </p><p class="value">Printed</p></li>
	                                                    	<li><p>Technics: </p><p class="value">Printed</p></li>
	                                                    	<li><p>Technics: </p><p class="value">Printed</p></li>
	                                                    </ul>
                                                	</div>
                                                    
                                                </div>
                                            </div>
                                            <div id="reviews" class="tab-pane fade" role="tabpanel">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="comment-block xs-spacer20">
                                                            <h4>2 Review for “Product Name”</h4>
                                                            <ul>
                                                                <li>
                                                                    <img class="user-img" src="<?php echo base_url();?>assets/images/user_icon.png" alt=""/>
                                                                    <div class="meta">
                                                                        <strong>Edo</strong>
                                                                        <time datetime="2016-06-11T11:45:00+00:00">Jun 9, 2016</time>
                                                                    </div>
                                                                    <div class="product_review clearfix">
                                                                        <div class="review_star">
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star-half"></i>
                                                                        </div>
                                                                    </div>
                                                                    <div class="co-desc">
                                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis condimentum pretium turpis, vel pulvinar diam vulputate quis. Donec porttitor volutpat rutrum. Suspendisse suscipit arcu velit,</p>
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <img class="user-img" src="<?php echo base_url();?>assets/images/user_icon.png" alt=""/>
                                                                    <div class="meta">
                                                                        <strong>Edo</strong>
                                                                        <time datetime="2016-06-11T11:45:00+00:00">Jun 9, 2016</time>
                                                                    </div>
                                                                    <div class="product_review clearfix">
                                                                        <div class="review_star">
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star-half"></i>
                                                                        </div>
                                                                    </div>
                                                                    <div class="co-desc">
                                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis condimentum pretium turpis, vel pulvinar diam vulputate quis. Donec porttitor volutpat rutrum. Suspendisse suscipit arcu velit,</p>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 comment-form">
                                                        <div>
                                                            <form action="#">
                                                                <label>Nickname<em>*</em></label>
                                                                <input type="text" class="form-control"/>
                                                                <label>Summary of Your Review<em>*</em></label>
                                                                <input type="text" class="form-control"/>
                                                                <label>Review<em>*</em></label>
                                                                <textarea class="form-control"></textarea>
                                                                <button class="btn" type="submit">Submit Review</button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="tags" class="tab-pane fade" role="tabpanel">
                                                <div class="f-categories clearfix">
                                                    <div class="col-sm-12">
                                                        <ul>
                                                            <li><a href="#">Bag</a></li>
                                                            <li><a href="#">Men's Bags</a></li>
                                                            <li><a href="#">Women's Bags</a></li>
                                                            <li><a href="#">Clothing</a></li>
                                                            <li><a href="#">Dress</a></li>
                                                            <li><a href="#">Pants</a></li>
                                                            <li><a href="#">Shirt</a></li>
                                                            <li><a href="#">T-Shirt</a></li>
                                                            <li><a href="#">Electronics</a></li>
                                                            <li><a href="#">Head phone</a></li>
                                                            <li><a href="#">Phone</a></li>
                                                            <li><a href="#">Laptop</a></li>
                                                            <li><a href="#">electronics</a></li>
                                                            <li><a href="#">Health & Beauty</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
        							<div class="spacer30"></div><!--spacer-->
                                    <div class="upsell clearfix">
                                        <h4 class="heading">Upsell Products</h4>
                                        <div class="owl-carousel upsell-products">
                                            <div class="product-item">
                                                <ul class="products-row">
                                                    <li class="image-block">
                                                        <a href="#"><span><img src="<?php echo base_url();?>assets/images/products/women/clothing/products-img-6.jpg" alt=""/></span></a>
                                                        <a class="add-to-cart" href="#">Add to cart</a>
                                                        <div class="a-link">
                                                            <a class="l-1" href="#"><i class="fa fa-shopping-bag"></i></a>
                                                            <a class="l-2" href="#"><i class="fa fa-heart"></i></a>
                                                            <a class="l-3" href="#"><i class="fa fa-exchange"></i></a>
                                                            <a class="l-4" href="#"><i class="fa fa-search"></i></a>
                                                        </div>
                                                        <div class="review_star">
                                                        	<i class="fa fa-star"></i>
                                                          	<i class="fa fa-star"></i>
                                                          	<i class="fa fa-star"></i>
                                                           	<i class="fa fa-star"></i>
                                                          	<i class="fa fa-star-half"></i>
                                                   		</div>
                                                    </li>
                                                    <li class="products-details">
                                                        <a href="#">
                                                            Professional Hxr-Mc2500 
                                                            Camcorder Camera
                                                        </a>
                                                        <span>$1600</span>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="product-item">
                                                <ul class="products-row">
                                                    <li class="image-block">
                                                        <a href="#"><span><img src="<?php echo base_url();?>assets/images/products/women/clothing/products-img-6.jpg" alt=""/></span></a>
                                                        <a class="add-to-cart" href="#">Add to cart</a>
                                                        <div class="a-link">
                                                            <a class="l-1" href="#"><i class="fa fa-shopping-bag"></i></a>
                                                            <a class="l-2" href="#"><i class="fa fa-heart"></i></a>
                                                            <a class="l-3" href="#"><i class="fa fa-exchange"></i></a>
                                                            <a class="l-4" href="#"><i class="fa fa-search"></i></a>
                                                        </div>
                                                        <div class="review_star">
                                                        	<i class="fa fa-star"></i>
                                                          	<i class="fa fa-star"></i>
                                                          	<i class="fa fa-star"></i>
                                                           	<i class="fa fa-star"></i>
                                                          	<i class="fa fa-star-half"></i>
                                                   		</div>
                                                    </li>
                                                    <li class="products-details">
                                                        <a href="#">
                                                            Professional Hxr-Mc2500 
                                                            Camcorder Camera
                                                        </a>
                                                        <span>$1600</span>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="product-item">
                                                <ul class="products-row">
                                                    <li class="image-block">
                                                        <a href="#"><span><img src="<?php echo base_url();?>assets/images/products/women/clothing/products-img-6.jpg" alt=""/></span></a>
                                                        <a class="add-to-cart" href="#">Add to cart</a>
                                                        <div class="a-link">
                                                            <a class="l-1" href="#"><i class="fa fa-shopping-bag"></i></a>
                                                            <a class="l-2" href="#"><i class="fa fa-heart"></i></a>
                                                            <a class="l-3" href="#"><i class="fa fa-exchange"></i></a>
                                                            <a class="l-4" href="#"><i class="fa fa-search"></i></a>
                                                        </div>
                                                        <div class="review_star">
                                                        	<i class="fa fa-star"></i>
                                                          	<i class="fa fa-star"></i>
                                                          	<i class="fa fa-star"></i>
                                                           	<i class="fa fa-star"></i>
                                                          	<i class="fa fa-star-half"></i>
                                                   		</div>
                                                    </li>
                                                    <li class="products-details">
                                                        <a href="#">
                                                            Professional Hxr-Mc2500 
                                                            Camcorder Camera
                                                        </a>
                                                        <span>$1600</span>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="product-item">
                                                <ul class="products-row">
                                                    <li class="image-block">
                                                        <a href="#"><span><img src="<?php echo base_url();?>assets/images/products/women/clothing/products-img-6.jpg" alt=""/></span></a>
                                                        <a class="add-to-cart" href="#">Add to cart</a>
                                                        <div class="a-link">
                                                            <a class="l-1" href="#"><i class="fa fa-shopping-bag"></i></a>
                                                            <a class="l-2" href="#"><i class="fa fa-heart"></i></a>
                                                            <a class="l-3" href="#"><i class="fa fa-exchange"></i></a>
                                                            <a class="l-4" href="#"><i class="fa fa-search"></i></a>
                                                        </div>
                                                        <div class="review_star">
                                                        	<i class="fa fa-star"></i>
                                                          	<i class="fa fa-star"></i>
                                                          	<i class="fa fa-star"></i>
                                                           	<i class="fa fa-star"></i>
                                                          	<i class="fa fa-star-half"></i>
                                                   		</div>
                                                    </li>
                                                    <li class="products-details">
                                                        <a href="#">
                                                            Professional Hxr-Mc2500 
                                                            Camcorder Camera
                                                        </a>
                                                        <span>$1600</span>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="product-item">
                                                <ul class="products-row">
                                                    <li class="image-block">
                                                        <a href="#"><span><img src="<?php echo base_url();?>assets/images/products/women/clothing/products-img-6.jpg" alt=""/></span></a>
                                                        <a class="add-to-cart" href="#">Add to cart</a>
                                                        <div class="a-link">
                                                            <a class="l-1" href="#"><i class="fa fa-shopping-bag"></i></a>
                                                            <a class="l-2" href="#"><i class="fa fa-heart"></i></a>
                                                            <a class="l-3" href="#"><i class="fa fa-exchange"></i></a>
                                                            <a class="l-4" href="#"><i class="fa fa-search"></i></a>
                                                        </div>
                                                        <div class="review_star">
                                                        	<i class="fa fa-star"></i>
                                                          	<i class="fa fa-star"></i>
                                                          	<i class="fa fa-star"></i>
                                                           	<i class="fa fa-star"></i>
                                                          	<i class="fa fa-star-half"></i>
                                                   		</div>
                                                    </li>
                                                    <li class="products-details">
                                                        <a href="#">
                                                            Professional Hxr-Mc2500 
                                                            Camcorder Camera
                                                        </a>
                                                        <span>$1600</span>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="product-item">
                                                <ul class="products-row">
                                                    <li class="image-block">
                                                        <a href="#"><span><img src="<?php echo base_url();?>assets/images/products/women/clothing/products-img-6.jpg" alt=""/></span></a>
                                                        <a class="add-to-cart" href="#">Add to cart</a>
                                                        <div class="a-link">
                                                            <a class="l-1" href="#"><i class="fa fa-shopping-bag"></i></a>
                                                            <a class="l-2" href="#"><i class="fa fa-heart"></i></a>
                                                            <a class="l-3" href="#"><i class="fa fa-exchange"></i></a>
                                                            <a class="l-4" href="#"><i class="fa fa-search"></i></a>
                                                        </div>
                                                        <div class="review_star">
                                                        	<i class="fa fa-star"></i>
                                                          	<i class="fa fa-star"></i>
                                                          	<i class="fa fa-star"></i>
                                                           	<i class="fa fa-star"></i>
                                                          	<i class="fa fa-star-half"></i>
                                                   		</div>
                                                    </li>
                                                    <li class="products-details">
                                                        <a href="#">
                                                            Professional Hxr-Mc2500 
                                                            Camcorder Camera
                                                        </a>
                                                        <span>$1600</span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                            	</div>
                            </div>
                        </div>
                       <div class="col-md-3 col-sm-4">
	                       	<div class="main-contant details-view details_view_contact ">
	                       		<div><p>Lead Time <span>5 days</span>  </p></div>
	                       		<div><p>Shipping time <span>6-10 days</span></p></div>
	                       		<div class="cont_supplier"><a href="#"><i class="fa fa-envelope"></i> Contact Supplier</a></div>
	                       	</div>
	                       	<div class="main-contant details-view verified">
	                       		<div class="verified_heading"><img src="<?php echo base_url();?>assets/images/verified.png" alt=""> Supplier</div>
	                       		<div><a href="#">Hexin Jucai (Wuhan) Digital Design </a></div>
	                       		<div><p>Manufacturer, Trading Company</p></div>
	                       		<div><h5><a href="#"><span>89.7%</span> Response Rate</a></h5></div>
	                       		<div><h5><a href="#"><span>	9,000+</span> Response Rate</a></h5></div>
	                       			<div><h5><a href="#"><span>89.7%</span> Response Rate</a></h5></div>
	                       	</div>
                       </div>
                    </div>
                </div>
            </div>
            <div class="spacer30"></div><!--spacer-->




<link rel="stylesheet" href="<?php echo base_url();?>assets/css/xzoom.css">

    
   

<script type="text/javascript" src="<?php echo base_url();?>assets/js/xzoom.min.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/images_zoom.js"></script>


<script type="text/javascript">
	$(".owl-carousel.product_details_sld").owlCarousel({
			items: 4,
			itemsDesktop : [1200,3],
			itemsDesktopSmall: [1199,3],
			itemsTablet: [950,3],
			itemsTabletSmall:[767,3],
			itemsMobile : [479,2],
			slideSpeed: 600,
			autoPlay:3000,
			stopOnHover: true,
			navigation:true,
			pagination:false,
			responsive: true,
			autoHeight : true,
			navigationText:["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"]
		});
</script>
<script>var itemId='<?php echo $itemID;?>'</script>	
<script src="<?php echo base_url();?>assets/custom_js/product/productDetails.js?<?php echo rand();?>"></script>  
<script type="text/javascript">
	// Gallery image hover
$( ".img-wrapper" ).hover(
  function() {
    $(this).find(".img-overlay").animate({opacity: 1}, 600);
  }, function() {
    $(this).find(".img-overlay").animate({opacity: 0}, 600);
  }
);

// Lightbox
var $overlay = $('<div id="overlay"></div>');
var $image = $("<img>");
var $prevButton = $('<div id="prevButton"><i class="fa fa-chevron-left"></i></div>');
var $nextButton = $('<div id="nextButton"><i class="fa fa-chevron-right"></i></div>');
var $exitButton = $('<div id="exitButton"><i class="fa fa-times"></i></div>');

// Add overlay
$overlay.append($image).prepend($prevButton).append($nextButton).append($exitButton);
$("#gallery").append($overlay);

// Hide overlay on default
$overlay.hide();

// When an image is clicked
$(".img-overlay").click(function(event) {
  // Prevents default behavior
  event.preventDefault();
  // Adds href attribute to variable
  var imageLocation = $(this).prev().attr("href");
  // Add the image src to $image
  $image.attr("src", imageLocation);
  // Fade in the overlay
  $overlay.fadeIn("slow");
});

// When the overlay is clicked
$overlay.click(function() {
  // Fade out the overlay
  $(this).fadeOut("slow");
});

// When next button is clicked
$nextButton.click(function(event) {
  // Hide the current image
  $("#overlay img").hide();
  // Overlay image location
  var $currentImgSrc = $("#overlay img").attr("src");
  // Image with matching location of the overlay image
  var $currentImg = $('#image-gallery img[src="' + $currentImgSrc + '"]');
  // Finds the next image
  var $nextImg = $($currentImg.closest(".image").next().find("img"));
  // All of the images in the gallery
  var $images = $("#image-gallery img");
  // If there is a next image
  if ($nextImg.length > 0) { 
    // Fade in the next image
    $("#overlay img").attr("src", $nextImg.attr("src")).fadeIn(800);
  } else {
    // Otherwise fade in the first image
    $("#overlay img").attr("src", $($images[0]).attr("src")).fadeIn(800);
  }
  // Prevents overlay from being hidden
  event.stopPropagation();
});

// When previous button is clicked
$prevButton.click(function(event) {
  // Hide the current image
  $("#overlay img").hide();
  // Overlay image location
  var $currentImgSrc = $("#overlay img").attr("src");
  // Image with matching location of the overlay image
  var $currentImg = $('#image-gallery img[src="' + $currentImgSrc + '"]');
  // Finds the next image
  var $nextImg = $($currentImg.closest(".image").prev().find("img"));
  // Fade in the next image
  $("#overlay img").attr("src", $nextImg.attr("src")).fadeIn(800);
  // Prevents overlay from being hidden
  event.stopPropagation();
});

// When the exit button is clicked
$exitButton.click(function() {
  // Fade out the overlay
  $("#overlay").fadeOut("slow");
});
</script>