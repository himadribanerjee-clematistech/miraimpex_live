
<section class="category_list products_page">
	<div class="container">
    <div class="row group_category">
			<div class="col-md-12">
				<div class="bradecap">
					<?php echo $catPath;?>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="row other_category_item">
			<div class="col-sm-3"><a href="">
				<div class="other_cate_item">
					<div class="other_cate_item_img"><img src="<?php echo base_url();?>assets/images/category/img-1.jpg" alt="img-1"/></div>
					<div class="other_cate_item_titel"><h4>GMP Standard High Quality neem hemp oil extraction machine</h4></div>
                    <div class="product-info viewCate_attri">
                        <div class="attribute clearfix">
                             <div><fieldset class="attribute_fieldset"> 
                                  <label class="attribute_label">Size:</label>
                                  <div class="attribute_list"> 
                                      <ul class="attribute_size">
                                          <li class="active">S</li>
                                          <li>M</li>
                                          <li>L</li>
                                          <li>XS</li>
                                          <li>XL</li>
                                      </ul>
                                  </div>
                              </fieldset></div>


                               <div><fieldset class="attribute_fieldset"> 
                                       <label class="attribute_label">Color:</label>
                                       <div class="attribute_list"> 
                                           <ul class="attribute_color">
                                               <li class="_1"></li>
                                               <li class="_2"></li>
                                               <li class="_3"></li>
                                               <li class="_4"></li>
                                           </ul>
                                       </div>
                                   </fieldset></div>
                        </div>
                    </div>
					<div class="other_cate_item_price"><p>Price: <span><i class="fa fa-dollar"></i> 550 INR</span></p></div>
					<div class="other_cate_item_contact"><h5><i class="fa fa-envelope"></i> Contact Supplier</h5></div>
				</div></a>
			</div>
		</div>

