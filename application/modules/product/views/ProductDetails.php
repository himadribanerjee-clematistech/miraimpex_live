<input type="hidden" name="seller_id" id="seller_id" value="">
<input type="hidden" id="itemVariance" name="addtocartvariance" value="">

<style>
.varianceActive{border:2px solid #00ff00 !important}
</style>

<section class="category_list products_page">
	<div class="container">
    <div class="row group_category">
			<div class="col-md-12">
				<div class="bradecap">
					<?php echo $catPath;?>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="wrapper-main brandshop clearfix">
        	<div class="spacer15"></div><!--spacer-->
        	<div class="container">
            	<div class="inner-block"><!------Main Inner-------->
                	<div class="row">
                        <div class="col-md-9 col-sm-8">
                        	<div class="main-contant xs-spacer20 clearfix">
                            	<div class="contant-wrapper">
                                    <div class="details-view"><!-- Start Product Details -->
                                        <div class="clearfix">
                                            <div class="product-img"><!-- Product Images -->
                                                <div id="info-img">
													<!---
                                                    <span class="pro offer">30% off</span>
                                                    --->
                                                    <div class="swiper-container top-img">
                                                        <div class="swiper-wrapper">
															  <section id="default" class="padding-top0">
																    <div>

																      <div class="large-5 column">
																        <div class="xzoom-container">
																          <img class="xzoom" id="xzoom-default" src="<?php echo base_url();?>admin/assets/images/no-item-image.png" xoriginal="<?php echo base_url();?>admin/assets/images/no-item-image.png" />

																          <div class="xzoom-thumbs" id="xzoom-thumbs">
																            <a href="<?php echo base_url();?>admin/assets/images/no-item-image.png" src="<?php echo base_url();?>admin/assets/images/no-item-image.png"><img class="xzoom-gallery"  src="<?php echo base_url();?>admin/assets/images/no-item-image.png" src="<?php echo base_url();?>admin/assets/images/no-item-image.png"  xpreview="<?php echo base_url();?>admin/assets/images/no-item-image.png" src="<?php echo base_url();?>admin/assets/images/no-item-image.png" title=""></a>
																              
																            
																          </div>
																        </div>        
																      </div>
																      <div class="large-7 column"></div>
																    </div>
																    </section>
                                                        </div>
                                                        
                                                    </div>
                                                    
                                                </div>
                                            </div><!-- End Product Images -->
                                            <div class="product-info">
                                                <h4 id="productName"></h4>
                                                
                                             
                                                <div  class="attribute clearfix">
                                                    <div class="price-box">
                                                    	<p class="new-price"><span id="price_min"></span></p>
                                                    	<p class="new-price" id="sep">-</p>
                                                    	<p class="new-price"><span id="price_max"></span></p>
                                                    	<p class="new-price" id="price-uom-name"></p>
                                                    
                                                    </div>
	                                                <div id="attributePanel">                                                    
                                                    </div>
                                                
                                                </div>
                                                <div class="attribute clearfix Customization_info">
                                                	<div class="custo_item">
                                                		<div class="custo_heading"><p>Minimum Order:</p></div>
                                                		<div class="custo_value"><p id="item_uom"></p></div>
                                                	</div>
                                                	<div class="custo_item">
                                                		<div class="custo_heading"><p>Availability:</p></div>
                                                		<div class="custo_value"><p id="delivery_available">Gambia, Senegal, Ivory coast and Ghana</p></div>
                                                	</div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div><!-- End Product Details -->
                                    



        							<div class="spacer30"></div><!--spacer-->
                                    <div class="tab-panel clearfix"><!-- Tab -->
                                        <!-- Tabs Nav -->
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li role="presentation" class="active"><a href="#description" aria-controls="description" role="tab" data-toggle="tab">Products Description</a></li>
                                            <li role="presentation"><a href="#reviews" aria-controls="reviews" role="tab" data-toggle="tab">Comments</a></li>
                                            <li role="presentation"><a href="#tags" aria-controls="tags" role="tab" data-toggle="tab">Tags</a></li>
                                        </ul>
                                        <!-- Tab panes -->
                                        
                                        <div class="tab-content">
                                            <div id="description" class="tab-pane active fade in" role="tabpanel">
                                            <p id="productDescription"></p>
                                            <div class="spacer30"></div>
                                                <div class="pro_desc_info">                                               
                                                	<div id="detailsVarianceDiv">
                                                    <h4>Variance</h4>
                                                		<ul id="detailsVariance">	                                                    	
	                                                    	<li><p>Technics: </p><p class="value">Printed</p></li>
	                                                    	<li><p>Technics: </p><p class="value">Printed</p></li>
	                                                    	<li><p>Technics: </p><p class="value">Printed</p></li>
	                                                    	<li><p>Technics: </p><p class="value">Printed</p></li>
	                                                    	<li><p>Technics: </p><p class="value">Printed</p></li>
	                                                    </ul>
                                                	</div>
                                                	<div id="detailsAttributesDiv">
                                                    <h4>Attributes</h4>
                                                		<ul id="detailsAttributes">	                                                    	
	                                                    	<li><p>Technics: </p><p class="value">Printed</p></li>
	                                                    	<li><p>Technics: </p><p class="value">Printed</p></li>
	                                                    	<li><p>Technics: </p><p class="value">Printed</p></li>
	                                                    	<li><p>Technics: </p><p class="value">Printed</p></li>
	                                                    	<li><p>Technics: </p><p class="value">Printed</p></li>
	                                                    </ul>
                                                	</div>
                                                    
                                                </div>
                                            </div>
                                            <div id="reviews" class="tab-pane fade" role="tabpanel">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="comment-block xs-spacer20">
                                                            <h4>2 Review for “Product Name”</h4>
                                                            <ul>
                                                                <li>
                                                                    <img class="user-img" src="<?php echo base_url();?>assets/images/user_icon.png" alt=""/>
                                                                    <div class="meta">
                                                                        <strong>Edo</strong>
                                                                        <time datetime="2016-06-11T11:45:00+00:00">Jun 9, 2016</time>
                                                                    </div>
                                                                    <div class="product_review clearfix">
                                                                        <div class="review_star">
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star-half"></i>
                                                                        </div>
                                                                    </div>
                                                                    <div class="co-desc">
                                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis condimentum pretium turpis, vel pulvinar diam vulputate quis. Donec porttitor volutpat rutrum. Suspendisse suscipit arcu velit,</p>
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <img class="user-img" src="<?php echo base_url();?>assets/images/user_icon.png" alt=""/>
                                                                    <div class="meta">
                                                                        <strong>Edo</strong>
                                                                        <time datetime="2016-06-11T11:45:00+00:00">Jun 9, 2016</time>
                                                                    </div>
                                                                    <div class="product_review clearfix">
                                                                        <div class="review_star">
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star"></i>
                                                                            <i class="fa fa-star-half"></i>
                                                                        </div>
                                                                    </div>
                                                                    <div class="co-desc">
                                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis condimentum pretium turpis, vel pulvinar diam vulputate quis. Donec porttitor volutpat rutrum. Suspendisse suscipit arcu velit,</p>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 comment-form">
                                                        <div>
                                                            <form action="#">
                                                                <label>Nickname<em>*</em></label>
                                                                <input type="text" class="form-control"/>
                                                                <label>Summary of Your Review<em>*</em></label>
                                                                <input type="text" class="form-control"/>
                                                                <label>Review<em>*</em></label>
                                                                <textarea class="form-control"></textarea>
                                                                <button class="btn" type="submit">Submit Review</button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="tags" class="tab-pane fade" role="tabpanel">
                                                <div class="f-categories clearfix">
                                                    <div class="col-sm-12">
                                                        <ul id="itemTags">
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
        							<div class="spacer30"></div><!--spacer-->
                                    <div class="tab-panel clearfix" id="seller_other_items_panel"><!-- Tab -->
                                    	<div class="products_heading"><h4>You may also like</h4></div>
										<div class="owl-carousel product_details_sld" id="seller_other_items">
		                                    
		                                    
		                                    
		                                </div>
                                    </div>
                                    
                                    <div class="spacer30"></div><!--spacer-->
                                    <div class="tab-panel clearfix"><!-- Tab -->
                                    	<div class="products_heading"><h4>Supplier's Certificate</h4></div>
                                    	
									    <section id="gallery">
										    <div id="image-gallery">
										      <div class="row" id="sellerCertificatePanel">
										        
										      </div><!-- End row -->
										    </div><!-- End image gallery -->
										  
										</section>
                                    </div>
                            	</div>
                            </div>
                        </div>
                       <div class="col-md-3 col-sm-4">
	                       	<div class="main-contant details-view details_view_contact ">
	                       		<div><p>Lead Time: <span id="leadTime">5 days</span>  </p></div>
	                       		<div><p>Shipping Info: <span id="shippingTime">6-10 days</span></p></div>
	                       		<div class="cont_supplier" id="contact_supplier_addtocart"><a href="#"><i class="fa fa-envelope"></i> Contact Supplier</a></div>
	                       	</div>
	                       	<div class="main-contant details-view verified">
	                       		<div class="verified_heading"><img src="<?php echo base_url();?>assets/images/verified.png" alt=""> Supplier Info</div>
	                       		<div><a href="javascript:void(0);" id="supplierName">Hexin Jucai (Wuhan) Digital Design </a></div>
	                       		<div><p id="supplierType">Manufacturer, Trading Company</p></div>
	                       		<div><h5><a href="javascript:void(0);"><span>89.7%</span> Response Rate</a></h5></div>
	                       		<div><h5><a href="javascript:void(0);"><i class="fa fa-map-marker"></i> <span id="supplierLocation"></span></a></h5></div>
	                       	</div>
                       </div>
                    </div>
                </div>
            </div>
            <div class="spacer30"></div><!--spacer-->




<link rel="stylesheet" href="<?php echo base_url();?>assets/css/xzoom.css">

    
   

<script type="text/javascript" src="<?php echo base_url();?>assets/js/xzoom.min.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>assets/js/images_zoom.js"></script>


<script>var itemId='<?php echo $itemID;?>'</script>	
<script src="<?php echo base_url();?>assets/custom_js/product/productDetails.js?<?php echo rand();?>"></script>  
<script type="text/javascript">
	// Gallery image hover
$( ".img-wrapper" ).hover(
  function() {
    $(this).find(".img-overlay").animate({opacity: 1}, 600);
  }, function() {
    $(this).find(".img-overlay").animate({opacity: 0}, 600);
  }
);

// Lightbox
var $overlay = $('<div id="overlay"></div>');
var $image = $("<img>");
var $prevButton = $('<div id="prevButton"><i class="fa fa-chevron-left"></i></div>');
var $nextButton = $('<div id="nextButton"><i class="fa fa-chevron-right"></i></div>');
var $exitButton = $('<div id="exitButton"><i class="fa fa-times"></i></div>');

// Add overlay
$overlay.append($image).prepend($prevButton).append($nextButton).append($exitButton);
$("#gallery").append($overlay);

// Hide overlay on default
$overlay.hide();

// When an image is clicked
$(".img-overlay").click(function(event) {
  // Prevents default behavior
  event.preventDefault();
  // Adds href attribute to variable
  var imageLocation = $(this).prev().attr("href");
  // Add the image src to $image
  $image.attr("src", imageLocation);
  // Fade in the overlay
  $overlay.fadeIn("slow");
});

// When the overlay is clicked
$overlay.click(function() {
  // Fade out the overlay
  $(this).fadeOut("slow");
});

// When next button is clicked
$nextButton.click(function(event) {
  // Hide the current image
  $("#overlay img").hide();
  // Overlay image location
  var $currentImgSrc = $("#overlay img").attr("src");
  // Image with matching location of the overlay image
  var $currentImg = $('#image-gallery img[src="' + $currentImgSrc + '"]');
  // Finds the next image
  var $nextImg = $($currentImg.closest(".image").next().find("img"));
  // All of the images in the gallery
  var $images = $("#image-gallery img");
  // If there is a next image
  if ($nextImg.length > 0) { 
    // Fade in the next image
    $("#overlay img").attr("src", $nextImg.attr("src")).fadeIn(800);
  } else {
    // Otherwise fade in the first image
    $("#overlay img").attr("src", $($images[0]).attr("src")).fadeIn(800);
  }
  // Prevents overlay from being hidden
  event.stopPropagation();
});

// When previous button is clicked
$prevButton.click(function(event) {
  // Hide the current image
  $("#overlay img").hide();
  // Overlay image location
  var $currentImgSrc = $("#overlay img").attr("src");
  // Image with matching location of the overlay image
  var $currentImg = $('#image-gallery img[src="' + $currentImgSrc + '"]');
  // Finds the next image
  var $nextImg = $($currentImg.closest(".image").prev().find("img"));
  // Fade in the next image
  $("#overlay img").attr("src", $nextImg.attr("src")).fadeIn(800);
  // Prevents overlay from being hidden
  event.stopPropagation();
});

// When the exit button is clicked
$exitButton.click(function() {
  // Fade out the overlay
  $("#overlay").fadeOut("slow");
});
</script>