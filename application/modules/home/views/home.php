<script src="<?php echo base_url();?>assets/js/main.js" type="text/javascript"></script>  
<script src="<?php echo base_url();?>assets/custom_js/home.js?version=<?php echo time(); ?>" type="text/javascript"></script> 
<!-- <div class="loading">
    <img src="<?php echo base_url();?>assets/images/lg.ring-loading-gif.gif" alt="">
</div> -->
<!--------------Slider and categories-------------->
        <div class="wrapper-slider clearfix">
        
            <div class="container">
                <div class="row">
                    <!---------Start categories----------->
                    <div class="col-md-3 category">
                        <div class="category-contant">
                            <h4 class="heading">Product Categories<i class="pull-right fa fa-angle-down"></i></h4>
                            <a id="categories" class="mobile-categories" href="#"><span class="all_categories">Product Categories</span><i class="pull-right fa fa-angle-right"></i></a>
                            <ul id="category-menu-product"></ul>
                        </div>
                    </div>
                    <!---------End category----------->
                    <!---------Slider block----------->
                    <div class="col-md-6 col-sm-8 slider-main">
                        <div>
                            <div class="owl-carousel top_slider">
                                    <div class="item">
                                        <a href="<?php echo base_url();?>pages/logistics" target="_blank">
                                           <img src="<?php echo base_url();?>assets/images/new_banner/LOGISTIC.jpg" alt="logistics"/>
                                        </a>
                                        
                                    </div>
                                    <div class="item">
                                        <a href="<?php echo base_url();?>pages/safePay" target="_blank">
                                          <img src="<?php echo base_url();?>assets/images/new_banner/PAYMENT.jpg" alt="safePay"/>
                                        </a>
                                        
                                    </div>
                                    <div class="item">
                                        <a href="<?php echo base_url();?>/pages/forum" target="_blank">
                                          <img src="<?php echo base_url();?>assets/images/new_banner/corporate.jpg" alt="forum"/>
                                        </a>
                                        
                                    </div>
                                    <div class="item">
                                        <a href="<?php echo base_url();?>localsupplier" target="_blank">
                                          <img src="<?php echo base_url();?>assets/images/new_banner/localsupplierBanner.jpg" alt="localsupplier"/>
                                        </a>
                                        
                                    </div>
                                </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-4 category cate_right">
                     
                        <div class="category-contant">
                          <h4 class="heading">Service Categories<i class="pull-right fa fa-angle-down"></i></h4>
                            <a id="categories" class="mobile-categories" href="#"><span class="all_categories">Service Categories</span><i class="pull-right fa fa-angle-right"></i></a>
                           <ul id="category-menu-service">
                                                             
                            </ul>
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--------------End slider and categories-------------->            
        <div class="spacer30"></div><!--spacer-->
        <!--------------Start wrapper main-------------->
        <!-- <div class="wrapper-main"> -->
            <!-------------- Start banner block -------------->
            <div class="banner-block clearfix register_section" style="background-image: url(<?php echo base_url();?>assets/images/banner/banner-img-3.jpg);">
                <div class="">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <!-- <div class="col-sm-4">
                              <div class="register_list">
                                <h4><span>TOTAL</span> REGISTERED SELLERS</h4>
                                <h3>21,482</h3>
                              </div>
                            </div>
                            <div class="col-sm-4">
                              <div class="register_list">
                                <h4><span>TOTAL</span> REGISTERED BUYERS</h4>
                                <h3>32,405</h3>
                              </div>
                            </div>
                            <div class="col-sm-4">
                              <div class="register_list">
                                <h4><span>TOTAL</span> INTERACTIONS LAST WEEK </h4>
                                <h3>38,355</h3>
                              </div>
                            </div>  -->                              
                            <div class="owl-carousel register_slider">
                                    <div class="item">
                                        <div class="register_list">
                                          <h4>Looking For</h4>
                                          <h5>Mango</h5>
                                          <p><i class="fa fa-map-marker" aria-hidden="true"></i> India</p>
                                          <p><span>Quantity :</span> 1gm</p>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="register_list">
                                          <h4>Looking For</h4>
                                          <h5>Mango</h5>
                                          <p><i class="fa fa-map-marker" aria-hidden="true"></i> India</p>
                                          <p><span>Quantity :</span> 1gm</p>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="register_list">
                                          <h4>Looking For</h4>
                                          <h5>Mango</h5>
                                          <p><i class="fa fa-map-marker" aria-hidden="true"></i> India</p>
                                          <p><span>Quantity :</span> 1gm</p>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="register_list">
                                          <h4>Looking For</h4>
                                          <h5>Mango</h5>
                                          <p><i class="fa fa-map-marker" aria-hidden="true"></i> India</p>
                                          <p><span>Quantity :</span> 1gm</p>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="register_list">
                                          <h4>Looking For</h4>
                                          <h5>Mango</h5>
                                          <p><i class="fa fa-map-marker" aria-hidden="true"></i> India</p>
                                          <p><span>Quantity :</span> 1gm</p>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="register_list">
                                          <h4>Looking For</h4>
                                          <h5>Mango</h5>
                                          <p><i class="fa fa-map-marker" aria-hidden="true"></i> India</p>
                                          <p><span>Quantity :</span> 1gm</p>
                                        </div>
                                    </div>

                                </div>
                        </div>
                    </div>
                </div>
                <div class="spacer30"></div><!-- Spacer -->
            </div>
            <!-------------- End banner -------------->            
              <div class="blog-wrapper quotes">
                <div class="container">
                    <div class="">
                    <div class="row">
                        
                        <div class="col-sm-6">
                            <div class="blog-main">
                                <div class="post-item clearfix">
                                <h4 class="heading">One Request, <span>Multiple Quotes</span></h4>
                                <div class="blog-contant">
                                    
                                    <div class="min-detail">
                                         <div class="quotes_left_item">
                                        <div>
                                          <img src="<?php echo base_url();?>assets/images/ico5.png" alt="">
                                        </div>
                                        <div>
                                          <p>Tell us what You Need</p>
                                        </div> 
                                       </div>

                                       <div class="quotes_left_item">
                                        <div>
                                          <img src="<?php echo base_url();?>assets/images/ico6.png" alt="">
                                        </div>
                                        <div>
                                          <p>Receive Quotes From Suppliers</p>
                                        </div> 
                                       </div>
                                       <div class="quotes_left_item">
                                        <div>
                                          <img src="<?php echo base_url();?>assets/images/ico7.png" alt="">
                                        </div>
                                        <div>
                                          <p>Seal the Deal</p>
                                        </div> 
                                       </div>

                                      </div>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="blog-main">
                                <div class="post-item clearfix">
                                    <h4 class="heading">Tell us your <span>Requirement</span></h4>
                                    <div class="blog-contant">
                                        <div class="min-detail faq_form">
                                        <form id="linkrfqform" action="<?php echo base_url();?>requestforquotations" method="POST">
                                          <div><input type="text" name="rfqName" placeholder="What are you looking for..."></div>
                                           <div class="Quantity"><input type="text" name="rfqQuantity" placeholder="Quantity"></div>
                                            <div class="Quantity"><select name="rfqUOM" id="uom">
                                              <option>Per Unit</option>
                                            </select>
                                            <input type="hidden" name="process" value="ok">
                                            </div>
                                            <div><button type="submit">Request For Quotation</button></div>
                                        </form>
                                        </div>
                                    </div>
                                </div>
                           </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>    
            <!--------------Womens products-------------->
            <div class="row" id="featuredSubmenuPanel">
            <div class="loading center"><img src="<?php echo base_url();?>assets/images/lg.ring-loading-gif.gif" alt="Loading Category"></div>
            </div>
            
            <!-------------- End womens wroducts -------------->
            <!-------------- Mens products -------------->
            
            <!-------------- End mens products -------------->
            <!-------------- End banner -------------->
            <!-------------- End  accessories products -------------->
            
            <!-------------- Start Latest Blog -------------->

            <!-- <div class="blog-wrapper Customized_products">
              <div class="container">
                  <div class="blog-main">
                  <div class="row">
                      
                      <div class="col-sm-6">
                          <div class="post-item clearfix">
                              
                                <div class="blog-contant">
                                  <div class="post-title row">
                                    <div class="col-sm-9 p-no">
                                      <h4 class="heading">Customized products</h4>
                                      <p>Partner with one of 60,000 experienced manufacturers with design & production </p>
                                    </div>
                                      <div class="col-sm-3"><img src="<?php echo base_url();?>assets/images/65-512.png" alt=""></div>
                                     
                                    </div>
                                   
                                    <div class="min-detail">
                                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum ipsam ullam in, esse numquam quasi autem earum sunt magni eligendi a perspiciatis officiis placeat, tenetur saepe, sed necessitatibus tempore similique!</p>

                                  </div>
                                    
                                </div>
                            </div>
                      </div>
                      <div class="col-sm-6">
                          <div class="post-item clearfix">
                              
                                <div class="blog-contant">
                                  <div class="post-title row">
                                    <div class="col-sm-9 p-no">
                                      <h4 class="heading">Ready to Service</h4>
                                      <p>Partner with one of 60,000 experienced manufacturers with design & production </p>
                                    </div>
                                      <div class="col-sm-3"><img src="<?php echo base_url();?>assets/images/65-512.png" alt=""></div>
                                     
                                    </div>
                                   
                                    <div class="min-detail">
                                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum ipsam ullam in, esse numquam quasi autem earum sunt magni eligendi a perspiciatis officiis placeat, tenetur saepe, sed necessitatibus tempore similique!</p>

                                  </div>
                                    
                                </div>
                            </div>
                      </div>
                    </div>
                    </div>
                </div>
            </div> -->
          
            <!-------------- End Latest Blog -------------->
                
            <div class="section_how_work">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4"><a href="#">
                            <div class="how_info">
                                <div><img src="<?php echo base_url();?>assets/images/how-it-work.png" alt=""></div>
                                <div><h2>How It Works</h2></div>
                                <div><p>Learn how to use MiraImpex</p></div>
                                <div class="view_more">View More <i class="fa fa-angle-right"></i></div>
                            </div></a>
                        </div>

                        <div class="col-sm-4"><a href="#">
                            <div class="how_info">
                                <div><img src="<?php echo base_url();?>assets/images/b2b-services.png" alt=""></div>
                                <div><h2>B2B Service</h2></div>
                                <div><p>Get suppliers info on Email</p></div>
                                <div class="view_more">View More <i class="fa fa-angle-right"></i></div>
                            </div></a>
                        </div>

                        <div class="col-sm-4"><a href="#">
                            <div class="how_info">
                                <div><img src="<?php echo base_url();?>assets/images/need-help.png" alt=""></div>
                                <div><h2>Need Help?</h2></div>
                                <div><p>Browse Help Topics</p></div>
                                <div class="view_more">View More <i class="fa fa-angle-right"></i></div>
                            </div></a>
                        </div>
                    </div>
                </div>
            </div> 