<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

	protected $token_id='';

    public function __construct() {
        parent::__construct(); 
    }


    public function index() {
         $data = [];
         
         
        $data['view_file'] = 'myProfile';
        view($data);
    } 

    public function myaccount() {
        $data = [];
     
       $data['view_file'] = 'myAccount';
       view($data);
   } 

   public function changeprofilepicture() {
    $data = [];
 
   $data['view_file'] = 'changeProfilePicture';
   view($data);
}

   public function emailchange() {
    $data = [];
 
   $data['view_file'] = 'changeEmail';
   view($data);
}

public function changepassword() {
    $data = [];
 
   $data['view_file'] = 'changePassword';
   view($data);
}

public function myproducts() {
    $data = [];
 
   $data['view_file'] = 'myProducts';
   view($data);
}

public function myservices() {
    $data = [];
 
   $data['view_file'] = 'myServices';
   view($data);
}

public function myrfqs() {
    $data = [];
 
   $data['view_file'] = 'myRfq';
   view($data);
}

public function myenquiries() {
    $data = [];
 
   $data['view_file'] = 'myEnquiry';
   view($data);
}

public function notifications() {
    $data = [];
 
   $data['view_file'] = 'myNotification';
   view($data);
}

public function sellerregistration() {
    $data = [];
 
   $data['view_file'] = 'sellerregistration';
   view($data);
}

public function myorders() {
    $data = [];
 
   $data['view_file'] = 'myorders';
   view($data);
}

public function orderdetails($id = NULL) {
    
    $data = [];
    $data['order_id'] = $id;
   $data['view_file'] = 'orderdetails';
   view($data);
}

public function mysubscription() {
    $data = [];
 
   $data['view_file'] = 'mysubscription';
   view($data);
}

   
}
