<section class="my_profile">
  <div class="container">
    <div class="box_wrap">
      <div class="row">
      <?php
	          include_once "commonnav.php";
        ?>
      </div>
    </div>
  </div>
</section>
<section class="update_profile buyer_profile upgrade_supplier">
  <div class="container">
    <div class="box_wrap">
      <div class="row">
      
      <div class=" col-md-10 col-sm-9 col-lg-offset-2 info_sec">
        <div class="update_pro_info">
          <h4>Gentella</h4>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
          <div class="row">
            <div class="col-sm-6">
              <p><span><b>Linked Mobile : </b></span><span>9674391164 <a href="javascript:void(0);">[Verified]</a></span></p>
              <p><span><b>Email : </b></span><span>gentella@example.com <a href="javascript:void(0);">[Verified]</a></span></p>
            </div>
            <div class="col-sm-6 border-left">
              <p>Joined miraimpex.com in <span>2019 </span></p>              
            </div>
          </div>
        </div>
      </div>
      </div>
      </div>
  </div>
</section>

<section class="update_profile buyer_profile ">
  <div class="container">
    <div class="box_wrap">
      <div class="row">
        
      
      <div class="col-sm-12 p-no">
        <div class="update_pro_info back_color">
          <h4>My Services</h4>          
        </div>
      </div>

      <div class="col-md-12">
          <div class="view_edit">
            <a href="javascript:void(0);"><i class="fa fa-plus" aria-hidden="true"></i> Add New</a>
            <a href="javascript:void(0);"><i class="fa fa-plus" aria-hidden="true"></i> Add Service</a>
          </div>
        </div>

      <div class="col-sm-12 Product_list">
          <div class="row">
            <div class="col-sm-3 my_pro_item">
              <div class="pro_img">
                <img src="<?php echo base_url();?>assets/images/product/product1.jpg" alt="">
               </div>
               <div class="product_info">
                <h4>Rice Grains & Legumes > Rice</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                <div>
                	<a href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                	<a class="delete" href="#"><i class="fa fa-trash" aria-hidden="true"></i> Delete</a>
                </div>
               </div>               
              
            </div>
            <div class="col-sm-3 my_pro_item">
              <div class="pro_img">
                <img src="<?php echo base_url();?>assets/images/product/product2.jpg" alt="">
               </div>
               <div class="product_info">
                <h4>Rice Grains & Legumes > Rice</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                <div>
                	<a href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                	<a class="delete" href="#"><i class="fa fa-trash" aria-hidden="true"></i> Delete</a>
                </div>
               </div>               
              
            </div>
            <div class="col-sm-3 my_pro_item">
              <div class="pro_img">
                <img src="<?php echo base_url();?>assets/images/product/product3.png" alt="">
               </div>
               <div class="product_info">
                <h4>Rice Grains & Legumes > Rice</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                <div>
                	<a href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                	<a class="delete" href="#"><i class="fa fa-trash" aria-hidden="true"></i> Delete</a>
                </div>
               </div>               
              
            </div>
            <div class="col-sm-3 my_pro_item">
              <div class="pro_img">
                <img src="<?php echo base_url();?>assets/images/product/product4.jpg" alt="">
               </div>
               <div class="product_info">
                <h4>Rice Grains & Legumes > Rice</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                <div>
                	<a href="#"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
                	<a class="delete" href="#"><i class="fa fa-trash" aria-hidden="true"></i> Delete</a>
                </div>
               </div>               
              
            </div>
            
           
          </div>
      </div>
      </div>
      </div>
  </div>
</section>