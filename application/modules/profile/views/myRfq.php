<section class="my_profile">
  <div class="container">
    <div class="box_wrap">
      <div class="row">
      <?php
	          include_once "commonnav.php";
        ?>
      </div>
    </div>
  </div>
</section>
<section class="myrfq_page">
  <div class="container">
    <div class="box_wrap">
      <div class="row">
      <div class="col-sm-12 p-no">
        <div class="update_pro_info back_color">
          <h4>MY RFQs</h4>          
        </div>
      </div>
      <div class="col-md-12">
          <div class="view_edit">
            <a href="javascript:void(0);" id="totalUnreadRFQs"> Total 0 RFQ found.</a>
          </div>
        </div>
      <div class="col-md-12">
            <div class="quotation_mess" id="quotation_mess" style="display:none;">        
                           

            </div>
       
      </div>
      </div>
      </div>
  </div>
</section>

<div id="schema_rfq" class="d-none">
<div class="Message_list ">
            <div class="Message_list_info">
              <a href="#"  title="View details">
                <div class="Message_list_info">
                  <div class="Message_list_info_img">
                    <img src="http://agro.clematistech.com/admin/uploads/bussiness_logo/1576230821-NUPE.jpg" alt="">
                  </div>
                  <div>
                    <div class="name_date">
                      <p>Nikon Buyer</p>
                      <span class="date">12/12/2019 </span>
                    </div>
                    <p class="rfqCat">
                      <span>Live Animals</span>
                      <span><i class="fa fa-arrow-right" aria-hidden="true"></i></span>
                      <span>Dogs (For meat, Guard &amp; Pet)</span>
                      <span><i class="fa fa-arrow-right" aria-hidden="true"></i></span>
                      <span>Dogs (For meat, Guard &amp; Pet)</span>
                    </p>
                    <p class="quantity_messages">
                      <b>Quantity:</b>
                      <span>10 Per Unit</span>
                    </p>
                    <p class="quantity_messages">
                      <b>Message:</b>
                      <span> I need 10 Dogs</span>
                    </p>
                    <div class="rfq_img_list">
                      <div class="rfq_img">
                        <img src="http://agro.clematistech.com/admin/uploads/rfq/1576145871-1.jpeg" alt=""></div>
                      </div>
                    </div>
                  </div>
                </a>
              </div>
            </div>
</div>
<script src="<?php echo base_url(); ?>assets/custom_js/my_rfq.js?version=<?php echo time(); ?>"></script>

