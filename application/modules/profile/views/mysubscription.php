<section class="my_profile">
  <div class="container">
    <div class="box_wrap">
      <div class="row">
      <?php
	          include_once "commonnav.php";
        ?>
      </div>
    </div>
  </div>
</section>


<section class="myrfq_page my_enquery">
  <div class="container">
    <div class="box_wrap">
      <div class="row">
         <div class="col-sm-12 p-no">
            <div class="update_pro_info back_color">
              <h4>MY SUBSCRIPTION</h4>          
            </div>
          </div>
            <div class="col-md-12">
                <div class="quotation_mess" id="quotation_mess" style="display:block;">

                      <div class="Message_list ">
                      <div class="Message_list_info">
                        <p>
                          <b style="font-weight:bold">For traders of goods</b>
                        </p>
                        <p>Becoming a member and listing products is free for traders of goods, however we will charge a commission fee on goods moved per order ones a trade transaction is completed.</p>

                        </div>
                      </div>

                      <div class="Message_list ">
                      <div class="Message_list_info">
<p><b style="font-weight:bold">For Traders of service</b></p>
<p>Becoming a member is free, there will be a subscription for 1m, 3m, 6m, or 12m depending on what they want to be allowed to list their expertise. They will be entitled for a free one month trial. A commission fee will be charge if a transaction is completed. Subscription plans are 1 month, 3 months, 6months and 12 months. And subscriptions for MiraImpex go membership or MiraImpex gold membership. Gold members will be giving priority by having their service pop up whenever a customer search for the service.</p>
                        </div>
                      </div>
                  
                  </div>
             
            </div>
        </div>
    </div>
  </div>
</section>

<script type="text/javascript">
$(document).ready(function(){
  if(localStorage.getItem("miraimpex_token") === null) 
  {
    window.location.replace($("#base_url").val());
  }else{
    var total_info = JSON.parse(localStorage.getItem("miraimpex_total_info"));

    if (total_info.supplier_YN == "0") 
    {
      window.location.replace($("#base_url").val());
    }else{
      //getnotificationlists();
    } 
    
  }
});

function getnotificationlists() {
  $("#quotation_mess").empty();
  $("#quotation_mess").show();
  $('#quotation_mess').html('<div class=""><h2 style="text-align:center;">No notification found.</h2></div>'); 
}
</script>