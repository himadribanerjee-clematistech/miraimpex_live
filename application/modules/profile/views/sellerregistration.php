<script src="<?php echo base_url();?>assets/custom_js/city_state.js?version=<?php echo time(); ?>" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/custom_js/jquery.mask.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/custom_js/jquery.mask.min.js" type="text/javascript"></script>

<section class="my_profile">
  <div class="container">
    <div class="box_wrap">
      <div class="row">
      <?php
	          include_once "commonnav.php";
        ?>
      </div>
    </div>
  </div>
</section>


<section class="myrfq_page my_enquery seller_registration">
  <div class="container">
    <div class="box_wrap">
      <div class="row">
         <div class="col-sm-12 p-no">
            <div class="update_pro_info back_color">
              <h4>Seller Registration Form</h4>          
            </div>
          </div>            
          <div class="col-md-7 col-md-offset-2 form_box">
          <form action="javascript:void(0);" method="post" name="updatesellerProfileForm" id="updatesellerProfileForm" enctype="multipart/form-data">
              <div class="row">
                <div class="col-sm-4">
                <label class="lbl_title"><span>*</span>Business Name :</label>
               </div>
               <div class="col-sm-8">
                  <input type="text" class="txt_file" placeholder="Name" name="business_name" id="business_name" autocomplete="off">
               </div>
              </div>

              <div class="row">
                <div class="col-sm-4">
                <label class="lbl_title"><span>*</span>Region :</label>
               </div>
               <div class="col-sm-8">
               <select name="registrationRegion" class="custom-select" required id="registrationRegion"  onchange="set_country(this,registrationCountry,registrationState,'registrationCountry');">
								<option value=""> -- Select Region -- </option>												
								</select>
               </div>
              </div>

              <div class="row">
                <div class="col-sm-4">
                <label class="lbl_title"><span>*</span>Country :</label>
               </div>
               <div class="col-sm-8">
               <select name="country" disabled='disabled' class="txt_file" required id="registrationCountry" onchange="javascript:set_city_state(this,registrationState,'registrationState');"></select>
               </div>
              </div>

              <div class="row">
                <div class="col-sm-4">
                <label class="lbl_title"><span>*</span>State :</label>
               </div>
               <div class="col-sm-8">
               <select name="state" disabled='disabled' class="txt_file" required id="registrationState"></select>
               </div>
              </div>
              <div class="row">
                <div class="col-sm-4">
                <label class="lbl_title"><span>*</span>City :</label>
               </div>
               <div class="col-sm-8">
                  <input type="text" class="txt_file" placeholder="City" name="city" id="city" autocomplete="off">
               </div>
              </div>
              
              <div class="row">
                <div class="col-sm-4">
                <label class="lbl_title"><span>*</span>Address :</label>
               </div>
               <div class="col-sm-8">
                  <textarea class="txt_file" placeholder="Address.../" name="address" id="address" rows="3" autocomplete="off"></textarea>
               </div>
              </div>

              <div class="row">
                <div class="col-sm-4">
                <label class="lbl_title"><span>*</span>Contact Person :</label>
               </div>
               <div class="col-sm-8">
                  <input type="text" class="txt_file" placeholder="Contact name" name="contact_name" id="contact_name" autocomplete="off">
               </div>
              </div>

              <div class="row">
                <div class="col-sm-4">
                <label class="lbl_title"><span>*</span>Contact Number :</label>
               </div>
               <div class="col-sm-8">
                  <input type="text" class="txt_file input-mask-phone" placeholder="Contact number" name="contact_number" id="contact_number" autocomplete="off">
               </div>
              </div>

              <div class="row">
                <div class="col-sm-4">
                <label class="lbl_title"><span>*</span>Seller Category :</label>
               </div>
               <div class="col-sm-8">
               <select name="sellerCategory" class="txt_file" required id="supplierCategory">
												<option value="1">Manufactures</option>
												<option value="2">Importers & Exporters</option>
												<option value="3">Commission agents</option>
												<option value="4" selected>Local traders</option>
												<option value="5">Local producers</option>
												<option value="6">Distributors</option>
												<option value="7">Wholesalers</option>
												<option value="8">Service providers</option>												
													</select>
               </div>
              </div>

              <div class="row">
                <div class="col-sm-4">
                <label class="lbl_title"><span>*</span>Currency :</label>
               </div>
               <div class="col-sm-8">
               <select name="store_currency" class="txt_file" required id="supplier_store_currency">
									<option value="USD" selected="selected">US Dollar</option>
									<option value="EUR">Euro</option>												
							</select>
               </div>
              </div>

              <div class="row">
                <div class="col-sm-4">
                <label class="lbl_title"><span>*</span>Company Certificates :</label>
               </div>
               <div class="col-sm-8">
               <input type="file" class="txt_file" name="certificate[]" id="gallery-certificate-add" accept="image/x-png,image/gif,image/jpeg">
              <div id="testdvcertificate" class="gallery"></div>  
               </div>
              </div>

              <div class="row">
                <div class="col-sm-4">
                <label class="lbl_title"><span>*</span>Company Documents :</label>
               </div>
               <div class="col-sm-8">
               <input type="file" class="txt_file" name="document[]" id="gallery-document-add">
               <div id="testdvdoc" class="gallery"></div> 
               </div>
              </div>

              <div class="row">
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                <a href="javascript:void(0);"></a><input type="submit" class="btn_subit" value="Upgrade to Seller" id="updatesellerProfileSubmit">
                  
                </div>
              </div>

              
            </form>
          </div>
        </div>
    </div>
  </div>
</section>
<script src="<?php echo base_url(); ?>assets/custom_js/seller_registration.js?version=<?php echo time(); ?>"></script>

