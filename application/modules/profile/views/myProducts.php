<section class="my_profile">
  <div class="container">
    <div class="box_wrap">
      <div class="row">
      <?php
	          include_once "commonnav.php";
        ?>
      </div>
    </div>
  </div>
</section>
<section class="update_profile buyer_profile prod">
  <div class="container">
    <div class="box_wrap">
      <div class="row">
        
      
      <div class="col-sm-12 p-no">
        <div class="update_pro_info back_color">
          <h4>My Product</h4>          
        </div>
      </div>

      <div class="col-md-12">
          <div class="view_edit">
            <a href="<?php echo base_url();?>seller/item/addItem"><i class="fa fa-plus" aria-hidden="true"></i> Add Item</a>
          </div>
        </div>

        <div class="col-sm-12 Product_list" id="productList">
		  
		  
	  </div>
      </div>
      </div>
  </div>
</section>
<script>
<?php
$row=array();
$row['are_you_sure_delete']=$this->lang->line('are_you_sure_delete');
?>	
var error_message=JSON.parse('{\"are_you_sure_delete\":\"Are you sure to remove?\"}');
</script>	
<script src="<?php echo base_url();?>assets/custom_js/seller/sellerProduct.js?<?php echo rand();?>"></script>