<section class="my_profile">
  <div class="container">
    <div class="box_wrap">
      <div class="row">
      <?php
	          include_once "commonnav.php";
        ?>
      </div>
    </div>
  </div>
</section>
<section class="Change_password">
  <div class="container">
    <div class="box_wrap">
      <div class="row">
      
      <div class=" col-md-10 col-md-offset-2">
        <div class="update_pro_info">
        <form action="javascript:void(0);" method="post" name="updatePasswordForm" id="updatePasswordForm">

          <p class="info_change_pass"><i class="fa fa-info-circle" aria-hidden="true"></i> Please make sure that you will receive a 4 digit OTP code in your registered mobile to change Password.</p>
          <div class="change_pass_form">
            <div class="change_pass_form_info">
				<p>Registered Email :</p>
				<p class="verifi_email" id="buyerEmail"> example@example.com <a href="#">[Verified]</a></p>
            </div>
            <div class="change_pass_form_info">
            	<p>Old Password :</p>
            	<p><input type="password" name="old_password" id="passchange_old_password" placeholder="Password"></p>
            </div>
            <div class="change_pass_form_info">
            	<div><a href="javascript:void(0);" onclick="javascript:sendOtpForPasswordChange();"><button type="button" id="buttonOTP" class="btn_otp">Request Free Verification Code</button></a></div>
            </div>
            <div class="cover_verifi_section">
              <div class="change_pass_form_info">
              <p>Verification Code :</p>
              <p><input type="text" name="otp" id="passchange_otp" placeholder="4-digit number"></p>
            </div>
            <div class="change_pass_form_info">
              <p>New Password  :</p>
              <p><input type="password" name="new_password" id="passchange_new_password" placeholder="Password"></p>
            </div>
            <div class="change_pass_form_info">
              <p>Confirm  Password  :</p>
              <p><input type="password" name="confirm_password" id="passchange_confirm_password" placeholder="Confirm Password"></p>
            </div>
          
            <div class="change_pass_form_info">
              <div><button class="btn_Submit" type="Submit" id="changePasswordSubmit"> Submit </button></div>
            </div>
            </div>

          </div>
			</form>

        </div>
      </div>
      </div>
      </div>
  </div>
</section>
<script src="<?php echo base_url(); ?>assets/custom_js/change_password.js?version=<?php echo time(); ?>"></script>