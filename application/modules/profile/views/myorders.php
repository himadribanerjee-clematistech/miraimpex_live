<section class="my_profile">
  <div class="container">
    <div class="box_wrap">
      <div class="row">
      <?php
	          include_once "commonnav.php";
        ?>
      </div>
    </div>
  </div>
</section>


<section class="myrfq_page my_enquery my_order">
  <div class="container">
    <div class="box_wrap">
      <div class="row">
         <div class="col-sm-12 p-no">
            <div class="update_pro_info back_color">
              <h4>MY ORDERS</h4>          
            </div>
          </div>
          <div class="col-md-12">
            <div class="view_edit">
              <a href="javascript:void(0);" id="countTotal"> Total 0 order found.</a>
            </div>
          </div>
        </div>        
        <div class="row" id="messageList">
                       
        </div>
            
        
    </div>
  </div>
</section>
<script src="<?php echo base_url(); ?>assets/custom_js/myorders.js?version=<?php echo time(); ?>"></script>