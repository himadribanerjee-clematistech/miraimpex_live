<script src="<?php echo base_url(); ?>assets/custom_js/profile.js?version=<?php echo time(); ?>"></script>
<section class="my_profile">
  <div class="container">
    <div class="box_wrap">
      <div class="row">
        <?php
	          include_once "commonnav.php";
        ?>
      </div>
    </div>
  </div>
</section>
<section class="update_profile buyer_profile upgrade_supplier">
  <div class="container">
    <div class="box_wrap">
      <div class="row">
        
      <div class="col-lg-2 col-md-3 col-sm-3 pic_sec text-center border-right">
        <div class="update_pro_img">
          <img id="update_pro_img" src="<?php echo base_url(); ?>assets/images/user.png" alt="">          
        </div>
        <div class="upload_photox" ><a href="<?php echo base_url();?>profile/changeprofilepicture">Change Photo</a></div>
        <div class="upload_photox" ><a href="<?php echo base_url();?>profile/emailchange">Change Email</a></div>
        <div class="upload_photox" ><a href="<?php echo base_url();?>profile/changepassword">Change Password</a></div>
      </div>
      <div class="col-lg-10 col-md-9 col-sm-9 info_sec">
        <div class="update_pro_info">
          <h4 id="buyerName"></h4>
          <p id="buyerCountry"></p>
          <div class="row">
            <div class="col-sm-6">
              <p><span><b>Linked Mobile : </b></span><span id="buyerMobile"></span></p>
              <p><span><b>E-mail : </b></span><span id="buyerEmail"></span></p>  
              <p><span><b>Account Type : </b></span><span id="buyerAccountType"></span></p>            
            </div>
            <div class="col-sm-6 border-left">            
            <p id="supplierType"><span><b>Supplier Type : </b></span><span id="supplierAccountType"></span></p>
            <p id="subscriptionType"><span><b>Subscription Type : </b></span><span id="suppliersubscriptionType"></span></p>
              <p>Joined Miraimpex on <span id="joindate">2020 </span></p>              
            </div>
          </div>
        </div>
      </div>
      </div>
      </div>
  </div>
</section>

<section class="update_profile buyer_profile buyer_contact buyerInfoPanel" id="buyerInfoPanel">
  <div class="container">
    <div class="box_wrap">
      <div class="row">
        
      
      <div class="col-sm-12 p-no">
        <div class="update_pro_info back_color">
          <h4>Buyer Information</h4>          
        </div>
      </div>
      <div class="col-md-12 back-color">
          <div class="view_edit">
          <a href="javascript:void(0);" data-toggle="modal" data-target="#edit_buyerprofile_modal"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
          </div>
        </div>
      <div class="col-sm-12">
        <div class="update_pro_info">
          <div class="row">
            <div class="col-sm-6">
              <div class="contact_info">
                 <p>Name :</p>
                <p id="buyerfullName"></p>
               </div>
               <div class="contact_info">
                <p>City : </p>
                <p id="buyerCity"></p>
               </div>               
              
            </div>
            <div class="col-sm-6 ">
              <div class="contact_info">
                <p>Address : </p>
                 <p id="buyerAddress"></p>
                
               </div>
               <div class="contact_info">
                <p>State : </p>
                <p id="buyerState"></p>
               </div>
               <div class="contact_info">
                <p>Country : </p>
                <p id="buyerCountry"></p>
               </div>
               
            </div>
            <div class="col-sm-6 ">
              <div class="contact_info">
                <p>Language : </p>
                 <p id="buyerLanguage"></p>
                
               </div>
               <div class="contact_info">
                <!-- <p>State : </p>
                <p id="buyerState"></p> -->
               </div>
               
            </div>
          </div>
        </div>
      </div>
      </div>
      </div>
  </div>

  
</section>


<section class="update_profile buyer_profile buyer_contact sellerInfoPanel" id="sellerInfoPanel">
  <div class="container">
    <div class="box_wrap">
      <div class="row">
        
      
      <div class="col-sm-12 p-no">
        <div class="update_pro_info back_color">
          <h4>Supplier Information</h4>          
        </div>
      </div>
      <div class="col-md-12">
          <div class="view_edit">
            <a href="javascript:void(0);" data-toggle="modal" data-target="#edit_sellerprofile_modal"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>
          </div>
        </div>
      <div class="col-sm-12">
        <div class="update_pro_info">
          <div class="row">
            <div class="col-sm-6">
              <div class="contact_info">
                 <p>Business Name : </p>
                 <p id="sellerBusinessname"></p>
               </div>
               <div class="contact_info">
                <p>City : </p>
                <p id="sellerCity"></p>
               </div>
               <div class="contact_info">
                <p>Contact Name : </p>
                <p id="sellerContactname"></p>
               </div>
               <div class="contact_info">
                <p>Store Currency : </p>
                <p id="sellerStoreCurrency"></p>
               </div>
               
              
            </div>
            <div class="col-sm-6 ">
              <div class="contact_info">
                <p>Address : </p>
                <p id="sellerAddress"></p>
               </div>
               <div class="contact_info">
                <p>State : </p>
                <p id="sellerState"></p>
               </div>
               <div class="contact_info">
                <p>Country : </p>
                <p id="sellerCountry"></p>
               </div>
               
               <div class="contact_info">
                <p>Contact Number : </p>
                <p id="sellerContactNo"></p>
               </div>
            </div>
          </div>
        </div>
      </div>
  
      </div>
      </div>
  </div>
</section>

<section class="update_profile buyer_profile buyer_contact sellerInfoPanel" id="sellerImagePanel">
  <div class="container">
    <div class="box_wrap">
      <div class="row">
        
      
      <div class="col-sm-12 p-no">
        <div class="update_pro_info back_color">
          <h4>Seller Images <span style="font-size:10px;">(Note: This images will be displayed as gallery in seller details page.)</span>  </h4> 
                 
        </div>
      </div>
      <div class="col-md-12 p-no">
          <div class="view_edit">
            <a style="color:green;" href="javascript:void(0);" data-toggle="modal" data-target="#add_sellerimage_modal"><i class="fa fa-plus" aria-hidden="true"></i> Add</a>
          </div>
        </div>
  
       <div class="col-sm-12">
         <div class="seller_img_section">
          <ul id="sellerImgUl">
            
          </ul>
         </div>
       </div>
      </div>
      </div>
  </div>
</section>

<section class="update_profile buyer_profile buyer_contact sellerInfoPanel" id="sellerCertificatePanel">
  <div class="container">
    <div class="box_wrap">
      <div class="row">
        
      
      <div class="col-sm-12 p-no">
        <div class="update_pro_info back_color">
          <h4>Company Certificates <span style="font-size:10px;">(Note: This images will be displayed as Company certificates in seller details page.)</span></h4>          
        </div>
      </div>
      <div class="col-md-12 p-no">
          <div class="view_edit">
            <a style="color:green;" href="javascript:void(0);" data-toggle="modal" data-target="#add_sellercertificate_modal"><i class="fa fa-plus" aria-hidden="true"></i> Add</a>
          </div>
        </div>
  
       <div class="col-sm-12">
         <div class="seller_img_section">
          <ul id="sellerCertificateImgUl">
            
          </ul>
         </div>
       </div>
      </div>
      </div>
  </div>
</section>

<section class="update_profile buyer_profile buyer_contact sellerInfoPanel" id="sellerDocumentPanel">
  <div class="container">
    <div class="box_wrap">
      <div class="row">
        
      
      <div class="col-sm-12 p-no">
        <div class="update_pro_info back_color">
          <h4>Company Documents <span style="font-size:10px;">(Note: This documents will be visible only to Admin.)</span></h4>          
        </div>
      </div>
      <div class="col-md-12 p-no">
          <div class="view_edit">
            <a style="color:green;" href="javascript:void(0);" data-toggle="modal" data-target="#add_sellerdocument_modal"><i class="fa fa-plus" aria-hidden="true"></i> Add</a>
          </div>
        </div>
       <div class="col-sm-12">
         <div class="seller_document_section">
          <ul id="sellerDocUl">
            
          </ul>
         </div>
       </div>
      </div>
      </div>
  </div>
</section>


<div class="modal fade profile_model_design" id="edit_buyerprofile_modal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Update Buyer Profile</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body buyer_profile_edit">
        <form action="javascript:void(0);" method="post" name="updatebuyerProfileForm" id="updatebuyerProfileForm">
          <div class="row">
          <div class="col-md-6">
              <div class="form-group row ">
                <label for="inputPassword" class="col-md-12 col-form-label">Name :</label>
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <input type="text" name="business_name" id="upd_buyerfullName" class="form-control" placeholder="Name">
                        </div>
                        
                    </div>                               
                </div>
            </div>
          
                
              <div class="form-group row">
                  <label for="inputPassword" class="col-md-12 col-form-label">City :</label>
                  <div class="col-md-12">
                      <div class="row">
                          <div class="col-md-12">
                              <input type="text" name="city" id="upd_buyerCity" class="form-control" placeholder="City ">
                          </div>
                          
                      </div>                               
                  </div>
              </div> 
             
                      
            <div class="form-group row">
                <label for="inputPassword" class="col-sm-12 col-form-label">State:</label>
                <div class="col-sm-12">
                  <input type="text" name="state" id="upd_buyerState" class="form-control" placeholder="State ">
                </div>
            </div>
          </div>
          <div class="col-md-6">
          <div class="form-group row Language">
                <label for="inputPassword" class="col-sm-12 col-form-label">Language:</label>
                <div class="col-sm-12">
                  <select name="cust_language" class="form-control" id="upd_buyerLanguage">
                    <option value="en">English</option>
                    <option value="fr">French</option>
                  </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="inputPassword" class="col-sm-12 col-form-label">Address:</label>
                <div class="col-sm-12">
                    <input type="text" id="upd_buyerAddress" name="address" class="form-control" placeholder="Address"> 
                   <input type="hidden" id="buyerprofilebuyeraddLat" name="lat" value=""> 
                  <input type="hidden" id="buyerprofilebuyeraddLang" name="long" value=""> 
                  <input type="hidden" id="buyerprofilebuyerlocation_city" name="location_city" value="">  
                  <span id="waitMsg" style="font-size: 12px;color: #29863d;padding-left:5px;"></span>
                </div>
            </div>
            
          </div>
      </div>

       
        <div class="form-group row text-center btn_cover_design" id="buyerSubBut">
          <button type="submit" class="btn_submit" id="updatebuyerProfileSubmit">Submit </button>
        </div>
                            
        </form>
      </div>
      
    </div>
  </div>
</div>

<div class="modal fade profile_model_design " id="edit_sellerprofile_modal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Update Supplier Profile</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body buyer_profile_edit">
        <form action="javascript:void(0);" method="post" name="updatesellerProfileForm" id="updatesellerProfileForm">

        <div class="row">
          <div class="col-md-6">
        <div class="form-group row sellerInfoPanel">
            <label for="inputPassword" class="col-sm-12 col-form-label p-no_left">Business Name:</label>
            <div class="col-sm-12 p-no_right">
              <input type="text" name="business_name" id="upd_sellerBusinessname" class="form-control" placeholder="Business Name">
            </div>
        </div>
        
        <div class="form-group row sellerInfoPanel">
            <label for="inputPassword" class="col-sm-12 col-form-label p-no_left">Business City:</label>
            <div class="col-sm-12 p-no_right">
              <input type="text" name="city" id="upd_sellerCity" class="form-control" placeholder="City">
            </div>
        </div>
        <div class="form-group row sellerInfoPanel">
            <label for="inputPassword" class="col-sm-12 col-form-label p-no_left">Business State:</label>
            <div class="col-sm-12 p-no_right">
              <input type="text" name="state" id="upd_sellerState" class="form-control" placeholder="State">
            </div>
        </div>
        <div class="form-group row sellerInfoPanel">
            <label for="inputPassword" class="col-sm-12 col-form-label p-no_left">Contact Name:</label>
            <div class="col-sm-12 p-no_right">
              <input type="text" name="contact_name" id="upd_sellerContactname" class="form-control" placeholder="Contact Name">
            </div>
        </div>
        
      </div>
        <div class="col-md-6">        
        <div class="form-group row sellerInfoPanel">
            <label for="inputPassword" class="col-sm-12 col-form-label p-no_left">Contact Number:</label>
            <div class="col-sm-12 ">
              <input type="text" name="contact_number" id="upd_sellerContactNo" class="form-control input-mask-phone" placeholder="Contact Number">
            </div>
        </div>
        <div class="form-group row sellerInfoPanel">
            <label for="inputPassword" class="col-sm-12 col-form-label p-no_left">Business Address:</label>
            <div class="col-sm-12">
              <input type="text" id="upd_sellerAddress" name="address" class="form-control" placeholder="Address"> 
             <input type="hidden" id="sellerprofileselleraddLat" name="lat" value=""> 
            <input type="hidden" id="sellerprofileselleraddLang" name="long" value="">
            </div>
        </div>
        <div class="form-group row sellerInfoPanel Language">
                <label for="inputPassword" class="col-sm-12 col-form-label">Currency:</label>
                <div class="col-sm-12">
                  <select name="store_currency" class="form-control" id="upd_sellerStoreCurrency">
                  <option value="USD">US Dollar</option>
									<option value="EUR">Euro</option>
                  </select>
                </div>
            </div>
      </div>
         </div>
        <div class="form-group text-center btn_cover_design">
          <button type="submit" class="btn_submit" id="updatesellerProfileSubmit">Submit </button>
        </div>
                            
        </form>
      </div>
      
    </div>
  </div>
</div>


<div class="modal fade profile_model_design" id="add_sellerimage_modal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Add Supplier Image</h4>

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body buyer_profile_edit">
        <form action="javascript:void(0);" method="post" name="addSellerImageForm" id="addSellerImageForm"  enctype="multipart/form-data">
          <div class="row">
          <div class="col-md-12">
            <span class="Upload_type">[Only .png/.jpg files are allowed]</span>
          <input type="file" name="business_logo[]" id="gallery-photo-add" accept="image/x-png,image/gif,image/jpeg">

          <div id="testdv" class="gallery"></div>
                
               
             
                      
            
          </div>          
      </div>

       
        <div class="form-group text-center btn_cover_design">
          <button type="submit" class="btn_submit" id="addSellerImageSubmit">Upload </button>
        </div>
                            
        </form>
      </div>
      
    </div>
  </div>
</div>


<div class="modal fade profile_model_design" id="add_sellercertificate_modal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Add Company Certificate</h4>

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body buyer_profile_edit">
        <form action="javascript:void(0);" method="post" name="addSellerCertificateForm" id="addSellerCertificateForm"  enctype="multipart/form-data">
          <div class="row">
          <div class="col-md-12">
            <span class="Upload_type">[Only .png/.jpg files are allowed]</span>
          <input type="file" name="certificate[]" id="gallery-certificate-add" accept="image/x-png,image/gif,image/jpeg">
          <div id="testdvcertificate" class="gallery"></div>            
          </div>          
      </div>
       
        <div class="form-group text-center btn_cover_design">
          <button type="submit" class="btn_submit" id="addSellerCertificateSubmit">Upload </button>
        </div>
                            
        </form>
      </div>
      
    </div>
  </div>
</div>



<div class="modal fade profile_model_design" id="add_sellerdocument_modal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Add Company Documents</h4>

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body buyer_profile_edit">
        <form action="javascript:void(0);" method="post" name="addSellerDocumentForm" id="addSellerDocumentForm"  enctype="multipart/form-data">
          <div class="row">
          <div class="col-md-12 ">
          <input type="file" name="document[]" id="gallery-document-add">

          <div id="testdvdoc" class="gallery"></div>
                
               
             
                      
            
          </div>          
      </div>

       
        <div class="form-group text-center btn_cover_design">
          <button type="submit" class="btn_submit" id="addSellerDocumentSubmit">Upload </button>
        </div>
                            
        </form>
      </div>
      
    </div>
  </div>
</div>

