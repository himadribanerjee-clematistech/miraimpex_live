<section class="my_profile">
  <div class="container">
    <div class="box_wrap">
      <div class="row">
      <?php
	          include_once "commonnav.php";
        ?>
      </div>
    </div>
  </div>
</section>

<section class="update_profile upload_photo_section upgrade_supplier">
  <div class="container">
    <div class="box_wrap">
      <div class="row">
        <div class="col-md-12">
          <div class="update_pro_info back_color">
             <h4>Upload My Photo</h4>          
          </div>
        </div>
        <div class="col-md-12">
          <div class="photo_notification">
            <h4><i class="fa fa-info-circle" aria-hidden="true"></i> Your Photo will be displayed within 24 hours.</h4>
          </div>
        </div>
        <div class="col-md-8">
          <div class="row border_all" id="drop-container">
            <div class="col-sm-3  text-right">
              <div class="update_pro_img">
                <img id="update_pro_img" src="<?php echo base_url(); ?>assets/images/user.png" alt="">
                
              </div>
              <div class="upload_photo"><button type="button" onclick="javascript:triggerClick();">Upload Photo</button></div>
            </div>
            <div class="col-sm-9">
              <form action="javascript:void(0);" method="post" id="uploadProfilePicForm" enctype="multipart/form-data">
                <input type="file" name="image" id="imgInp" accept="image/*" style="visibility: hidden;">
                <img id="blah" class="d-none" src="" style="width:400px; heigth:400px;" alt="your image">
              </form>
              <div class="update_pro_info">
                <p>Click the Button onto the dotted box to upload</p>
                <p>Upload JPG  format, sized no larger than 3MB</p>
              </div>
              <div class="upload_photo text-left d-none" id="saveBut"><button type="button" id="ssaveBut" onclick="javascript:uploadProfilePic();">Save</button></div>
            </div>
          </div> 

       </div>

       <div class="col-md-4">
          <div class="upload_photo_view border_all">
            <div><h5>Uploading Rules</h5></div>
            <div class="uploading_rules_img">
              <ul>
                <li><img src="<?php echo base_url(); ?>assets/images/single-man.jpg" alt=""></li>
                <li><img src="<?php echo base_url(); ?>assets/images/single-man2.jpg" alt=""></li>
                <li><img src="<?php echo base_url(); ?>assets/images/single-man3.jpg" alt=""></li>
              </ul>
            </div>
            <div class="uploading_rules_info">
              <ul>
                <li><p>Please upload a photo that matches the gender, age and status details in your personal profile</p></li>
                <li><p>Use a photo is appropriate for a businesssetting. Group photos are not allowed. </p></li>
                <li><p>Photos violating the rules stated in the Terms of User will be removed without notice.</p></li>
              </ul>
            </div>
          </div>
       </div>





      </div>
      </div>
  </div>
</section>
<script src="<?php echo base_url(); ?>assets/custom_js/change_profilepicture.js?version=<?php echo time(); ?>"></script>