<section class="my_profile">
  <div class="container">
    <div class="box_wrap">
      <div class="row">
      <?php
	          include_once "commonnav.php";
        ?>
      </div>
    </div>
  </div>
</section>

<section class="Change_password Change_email">
  <div class="container">
    <div class="box_wrap">
      <div class="row">
      
      <div class=" col-md-10 col-md-offset-2">

		<div class="traking">
                  <ul>
                    <li class="" id="changeEmailS1"><span>1</span><p>Verify</p></li>
                    <li class="" id="changeEmailS2"><span>2</span><p>Change Email Address</p></li>
                    <li class="" id="changeEmailS3"><span><i class="fa fa-check" aria-hidden="true"></i></span><p>Done</p></li>
                  </ul>
                </div>


        <div class="update_pro_info">
        <form action="javascript:void(0);" method="post" name="updateEmailForm" id="updateEmailForm">

          <p class="info_change_pass"><i class="fa fa-info-circle" aria-hidden="true"></i> Please make sure that your new email address works.</p>
          <div class="change_pass_form">
            <div class="change_pass_form_info">
				<p>Registered Email :</p>
				<p class="verifi_email" id="buyerEmail"> example@example.com <a href="javascript:void(0);">[Verified]</a></p>
            </div>
            
            <div class="change_pass_form_info">
            	<div><a href="javascript:void(0);" onclick="javascript:sendOtpForEmailChange();"><button class="btn_otp" type="button" id="buttonOTP"> Request Mobile OTP Verification Code</button></a></div>
            </div>
            <div class="cover_verifi_section">
			<div class="change_pass_form_info">
            	<p>Verification Code :</p>
            	<p><input type="text" name="otp" id="changeEmailOtp" placeholder="4-digit number"></p>
            </div>
            <div class="change_pass_form_info">
            	<p>New Email :</p>
            	<p><input type="text" name="email" id="changeEmailNewEmail" placeholder="New Email"></p>
            </div>
          
            <div class="change_pass_form_info">
            	<div><button class="btn_Submit" type="Submit" id="changeEmailSubmit">Update Email</button></div>
            </div>
			</div>

        </div>
			</form>

        </div>


        <div class="inquery_info">
                      <h4>OTP verification code not received</h4>
                      <ol>
                        <li><p>Your Mobile OTP verification code may take up to 10 minutes to arrive (depending on your email service provider), please do not repeat clicking.</p></li>
                        <li><p>Please check if your mailbox works or if it goes to trash/spam folder or your mail inbox is full.</p></li>
                        <li><p>Network anomalies may cause loss of messages, please re-submit request or try again later with different browsers or with  browser cookies cleared.</p></li>
                        <li><p>Check with your email operator to see if verification code email has been blocked.</p></li>
                      </ol>
                    </div>
      </div>
      </div>
      </div>
  </div>
</section>
<script src="<?php echo base_url(); ?>assets/custom_js/change_email.js?version=<?php echo time(); ?>"></script>



