<section class="my_profile">
  <div class="container">
    <div class="box_wrap">
      <div class="row">
      <?php
	          include_once "commonnav.php";
        ?>
      </div>
    </div>
  </div>
</section>


<section class="payment_page order_details_page">
  <div class="container">
    <div class="row">
      <div class="col-sm-8 left_menu">
        <div class="group_pay">
          <div class="pay_heading back_color">
                <a href="javascript:void(0)" class="cl_activity active">Activity</a>
                <a href="javascript:void(0)" class="cl_details">Details</a>      
          </div>
          <div class="order_details_info" id="order_details">
            <div class="head">
              <div><h4>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rerum natus facilis esse</h4>
                <p>Purchased from <a href="#">Carmonaarticles</a>
                    <span>Delivery due date <b>23-Feb-2020, 12:00 PM</b></span>
                </p>
              </div>
              <div class="price">
                <h5>Total Price</h5>
                <h4>527.90</h4>
              </div>
            </div>

            <div class="order_det_info">
              <div class="pay_title"><h5>Order Number <b>#001122</b></h5></div>
              <div class="pay_value"><a href="#">View Invoice</a></div>
            </div>
            <div class="order_det_info">
              <div class="pay_title"><h5><b>Discription</b></h5></div>
              <div class="pay_value"></div>
            </div>
            <div class="order_det_info">
              <div class="pay_title"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p></div>
              <div class="pay_value"></div>
            </div>
            <div class="order_other_info">
              <div><h5><i class="fa fa-file"></i> Your Order <span>21-Feb-2020, 06:30 PM</span></h5> <p>Paid with Credit Card</p></div>

              <table>
                <tbody>
                  <tr>
                    <td>Item</td>
                    <td>Qty</td>
                    <td>Duration </td>
                    <td>Price</td>
                  </tr>
                  <tr>
                    <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</td>
                    <td>1</td>
                    <td>2 days</td>
                    <td>300.50</td>
                  </tr>
                  <tr>
                    <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</td>
                    <td>1</td>
                    <td>2 days</td>
                    <td>300.50</td>
                  </tr>
                  <tr>
                    <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</td>
                    <td>1</td>
                    <td>2 days</td>
                    <td>300.50</td>
                  </tr>
                  <tr>
                    <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</td>
                    <td>1</td>
                    <td>2 days</td>
                    <td>300.50</td>
                  </tr>

                </tbody>
              </table>
            </div>
          </div>
          <div class="order_details_info" id="Activity">
            <div class="head">
              <div><h4>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rerum natus facilis esse</h4>
                <p>Purchased from <a href="#">Carmonaarticles</a>
                    <span>Delivery due date <b>23-Feb-2020, 12:00 PM</b></span>
                </p>
              </div>
              <div class="price">
                <h5>Total Price</h5>
                <h4>527.90</h4>
              </div>
            </div>

            <div class="order_det_info">
              <div class="pay_title"><h5>Order Number <b>#001122</b></h5></div>
              <div class="pay_value"><a href="#">View Invoice</a></div>
            </div>
            <div class="order_det_info">
              <div class="pay_title"><h5><b>Discription</b></h5></div>
              <div class="pay_value"></div>
            </div>
            <div class="order_det_info">
              <div class="pay_title"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p></div>
              <div class="pay_value"></div>
            </div>
            <div class="order_img_sect">
              <div><h4>Demo Heading Here</h4></div>
              <div class="order_img">
                  <div><a href="#"><img src="<?php echo base_url();?>assets/images/product/product2.jpg" alt=""></a></div>
                  <div><a href="#"><img src="<?php echo base_url();?>assets/images/product/product2.jpg" alt=""></a></div>
                  <div><a href="#"><img src="<?php echo base_url();?>assets/images/product/product2.jpg" alt=""></a></div>
                  <div><a href="#"><img src="<?php echo base_url();?>assets/images/product/product2.jpg" alt=""></a></div>
              
               </div>
            </div>
            
           
          </div>
              
        </div>
      </div>
      <div class="col-sm-4 left_menu">
        <div class="group_pay">
          <div class="right_pay_head">
            <div class="pay_img"><a href="#"><img src="<?php echo base_url();?>assets/images/product/product2.jpg" alt=""></a></div>
            <div class="pay_cnt"><a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</a></div>
          </div>
          <div class="payment_page_info">
            <p class="pay_title">Status</p>
            <p class="pay_value"><a href="#">in progress</a></p>
          </div>
          <div class="payment_page_info">
            <p class="pay_title">Enter promo code</p>
          </div>
          <div class="payment_page_info">
            <p class="pay_title">Service fee</p>
            <p class="pay_value">150</p>
          </div>
          <div class="payment_page_info">
            <h5 class="pay_title"><b>Total</b></h5>
            <h5 class="pay_value">1,054</h5>
          </div>
          <div class="payment_page_info">
            <h5 class="pay_title">Order Number</h5>
            <h5 class="pay_value">012235</h5>
          </div>
          
          <div class="Change_email order_details_traking">
            <div class="back_color"><h4>Order Progress</h4></div>
            <div class="traking">
              <ul>
                <li class="active" id="changeEmailS1"><span>1</span><p>Order placed</p></li>
                <li class="active" id="changeEmailS2"><span>2</span><p>Order in progress</p></li>
                <li class="" id="changeEmailS2"><span>3</span><p>Review delivery</p></li>
                <li class="" id="changeEmailS3"><span><i class="fa fa-check" aria-hidden="true"></i></span><p>Complete Order</p></li>
              </ul>
            </div>
          </div>
          <div>
            <button type="button" class="btn_confirm">Confirm & Pay</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>








<script type="text/javascript">
$(document).ready(function(){
  if(localStorage.getItem("miraimpex_token") === null) 
  {
    window.location.replace($("#base_url").val());
  }else{
    getorderlists();
  }
});

function getorderlists() {
  $("#quotation_mess").empty();
  $("#quotation_mess").show();
  $('#quotation_mess').html('<div class=""><h2 style="text-align:center;">No order found.</h2></div>'); 
}
  $(".cl_activity").click(function(){
     $(".cl_activity").addClass("active");
     $(".cl_details").removeClass("active");
    $("#Activity").show();
    $("#order_details").hide();
  });
  $(".cl_details").click(function(){
    $(".cl_details").addClass("active");
     $(".cl_activity").removeClass("active");
    $("#Activity").hide();
    $("#order_details").show();
  });
</script>