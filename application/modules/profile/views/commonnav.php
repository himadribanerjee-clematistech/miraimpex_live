<div class="col-md-12">
          <div class="accout_menu">
              <a <?php if($view_file == 'myProfile'){?>  class="active" <?php  } ?> href="<?php echo base_url();?>profile"><i class="fa fa-user-circle-o"></i> My Profile</a>
		      <a id="mySellerItemsNav" class="<?php echo (($view_file == 'myProducts') || ($view_file == 'ItemAdd') || ($view_file == 'ItemEdit')) ? 'active' : $view_file; ?>" href="<?php echo base_url();?>profile/myproducts"><i class="fa fa-list"></i> My Products</a>
		      <!-- <a id="mySellerServicesNav" <?php if($view_file == 'myServices'){?>  class="active" <?php  } ?> href="<?php echo base_url();?>profile/myservices"><i class="fa fa-list"></i> My Services</a> -->
          <a <?php if($view_file == 'myRfq'){?>  class="active" <?php  } ?> href="<?php echo base_url();?>profile/myrfqs"><i class="fa fa-list"></i> My RFQs</a>
          <a <?php if($view_file == 'myEnquiry'){?>  class="active" <?php  } ?> href="<?php echo base_url();?>profile/myenquiries"><i class="fa fa-list"></i> My Enquiry</a>
          <a <?php if($view_file == 'myNotification'){?>  class="active" <?php  } ?> href="<?php echo base_url();?>profile/notifications"><i class="fa fa-list"></i> Notifications</a>
          <a <?php if($view_file == 'myorders' || $view_file == 'orderdetails'){?>  class="active" <?php  } ?> href="<?php echo base_url();?>profile/myorders"><i class="fa fa-list"></i> My Orders</a> 
          <a id="mySellerSubscriptionNav" <?php if($view_file == 'mysubscription'){?>  class="active" <?php  } ?> href="<?php echo base_url();?>profile/mysubscription"><i class="fa fa-list"></i> My Subscription</a> 
		       <a class="positionright" id="upgrade_to_supplier_link" <?php if($view_file == 'sellerregistration'){?>  class="active" <?php  } ?> href="<?php echo base_url();?>profile/sellerregistration"><i class="fa fa-list"></i> Upgrade to Seller</a>
          </div>
         
        </div>


		<div id="email_verify_otp_modal" class="modal login_modal" aria-labelledby="myModalLabel" style="display: none;" aria-hidden="true">  
  <form action="javascript:void(0);" method="post" name="email_verify_otp_modal_Form" id="email_verify_otp_modal_Form">     
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header text-center">
          <button type="button" onclick="document.getElementById('email_verify_otp_modal').style.display='none'" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
          <h4 class="modal-title w-100 font-weight-bold">Verify Email</h4>
        </div>
        <div class="modal-body">
          <div class="md-form col-md-12" style="padding-top: 10px;">
             <label for="uname"><b>OTP</b></label>
             <input type="text" class="form-control" name="customer_otp" id="verifyemail_customer_otp" value="" placeholder="Enter OTP from your email">
             <input type="hidden" class="form-control" name="email" id="verifyemail_customer_email" value="" placeholder="customer email">
             <input type="hidden" class="form-control" name="customer_id" id="verifyemail_customer_id" value="" placeholder="customer id">
            </div>
          <div class="md-form optPanel" style="padding-bottom: 10px; display: inline-block;">
            <a class="join" href="javascript:void(0);" style="color: #ff7519;font-size: 14px;font-weight: 400;"  onclick="javascript:verifyEmail();" class="text-right">Resend OTP</a>
          </div>  
                    
          <div class="md-form " style="padding-top: 10px;">
            <input type="submit" class="btn btn-org" id="email_verify_otp_modal_Submit" value="Submit" style="width:auto;">

          </div>
        </div>
      </div>
    </div>   
  </form>
</div>		