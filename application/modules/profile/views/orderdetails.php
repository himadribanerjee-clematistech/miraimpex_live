<section class="my_profile">
  <div class="container">
    <div class="box_wrap">
      <div class="row">
      <?php
	          include_once "commonnav.php";
        ?>
      </div>
    </div>
  </div>
</section>


<section class="payment_page order_details_page">
  <div class="container">
    <div class="row">
      <div class="col-sm-8 left_menu">
        <div class="group_pay">
          <div class="pay_heading back_color">
                <a href="javascript:void(0)" class="cl_activity active">Activity</a>
                <a href="javascript:void(0)" class="cl_details">Details</a>      
          </div>
          <div class="order_details_info" id="order_details">
            
          <div class="order_other_info">
              <div><h5><i class="fa fa-file"></i> <span id="orderType" style="font-size:16px;color:#000;">RFQ Details</span> <span id="rfqCreateDate">21-Feb-2020, 06:30 PM</span></h5></div>
            <div id="rfqDetailsInfo"></div>  
          </div>    

            <div class="head rfqsection">
              <div><p>Purchased from<a href="javascript:void(0);" id="purchased_from">Carmonaarticles</a> Purchase Date: <span id="purchase_date"></span></p>
              </div>
              <div class="price">
                <h4>Total Price: <span class="proposal_amount"></span></h4>
              </div>
            </div>

            <div class="order_det_info">
              <div class="pay_title"><h5>Order Number <b><?php echo $order_id; ?></b></h5></div>
              <div class="pay_value"><a href="javascript:void(0);" id="viewinvoice">View Invoice</a></div>
            </div>

            <div class="order_other_info">
              <div><h5><i class="fa fa-user-circle-o"></i> Buyer Info</h5></div>
            <div><p id="buyerContName"></p></div><div><p id="buyerContNo"></p></div><div><p id="buyerEmail"></p></div><div><p id="buyercontactnumber1"></p></div><div><p id="buyercontactnumber2"></p></div>   
          </div>

            <div class="order_other_info" >
              <div><h5><i class="fa fa-user-circle-o"></i> Supplier Info</h5></div>
            <div><p id="compName"></p></div><div><p id="compContName"></p></div><div><p id="compContNo"></p></div><div><p id="compEmail"></p></div> 
          </div>

          

            
          </div>
          <div class="order_details_info" id="Activity">
          <div class="change_option d-none col-sm-12" style="margin-bottom:30px;" id="changeOrderStatusPanel">
            <div class="col-sm-6">
              <label>Change Order Status</label>
              <select name="orderStatus" id="changeOrderStatus" onchange="javascript:changeOrderStatus();">
              <option value="0">Order Placed</option>
                <option value="1">Order Accepted</option>
                <option value="2">Order Shipped</option>
                <option value="3">Order Delivered</option>
                <option value="5">Cancel Order</option>
              </select>
            </div>
            <div class="col-sm-6">
            <label>Payment received?</label>
              <select name="paymentStatus" id="changePaymentStatus" onchange="javascript:changePaymentStatus();">
              <option value="0">No</option>
                <option value="1">Yes</option>
              </select>
            </div>            
          </div>
          
          <div class="Change_email order_details_traking" style="clear:both">            
            <div class="traking">
              <ul>
                <li class="active" id="orderStat0"><span>1</span><p id="orderPlacedDate">Order placed</p></li>
                <li id="orderStat1"><span>2</span><p>Order Accepted</p></li>
                <li  id="orderStat2"><span>3</span><p>Order Shipped</p></li>
                <li  id="orderStat3"><span>4</span><p>Order Delivered</p></li>
                <li class="" id="orderStat4"><span><i class="fa fa-check" aria-hidden="true"></i></span><p>Order Closed</p></li>
              </ul>
            </div>
          </div>
          
          <div class="complete_order d-none" id="cancelOrderPanel">
            <a href="javascript:void(0);" onclick="javascript:changeOrderStatusboth('5');"><button style="background: red; color: white; border-color: red;"> Cancel Order</button></a>
          </div>

          <div class="complete_order d-none" id="completeOrderPanel">
            <a href="javascript:void(0);" onclick="javascript:changeOrderStatusboth('4');"><button> Yes, I have received the package & I am satisfied !</button></a>
          </div>
                       
           
          </div>
              
        </div>
      </div>
      <div class="col-sm-4 left_menu">
        <div class="group_pay">
          <div class="right_pay_head rfqsection">
            <div class="pay_img"><a href="javascript:void(0);" id="proposal_doc_link" target="_blank" title="view proposal details"><img src="<?php echo base_url();?>assets/images/icon/proposal-icon.png" alt=""></a></div>
            <div class="pay_cnt"><a href="javascript:void(0);" id="proposal_title" target="_blank" title="view proposal details">Lorem ipsum dolor sit amet, consectetur adipisicing elit.</a></div>
          </div>
          <div class="payment_page_info rfqsection">
            <p class="pay_title" id="proposal_desc"></p>
          </div>
          <div class="payment_page_info">
            <p class="pay_title">Transaction#</p>
            <p class="pay_value" id="transactionId"></p>
          </div>          
          <div class="payment_page_info">
            <p class="pay_title">Payment Mode</p>
            <p class="pay_value" id="paymentMode"></p>
          </div>
          <div class="payment_page_info">
            <p class="pay_title">Payment Status</p>
            <p class="pay_value" id="paymentStatus"></p>
          </div>
          <div class="payment_page_info">
            <p class="pay_title">Date</p>
            <p class="pay_value" id="proposal_date"></p>
          </div>
          <div class="payment_page_info">
            <h5 class="pay_title"><b>Total</b></h5>
            <h5 class="pay_value proposal_amount" id="proposal_amount">$0.00</h5>
          </div>     
          
          <div class="payment_page_info" id="approxGMD">
          </div> 
          
        </div>
        <div class="group_pay credit-card-box" id="bankpay" style="display:none;"><div>
          <div class="row display-tr" >
              <h3 class="panel-title display-td " >Bank Account Details</h3>            
          </div>        
              <div class="row">
                  <div class="col-xs-12">                      
                      <h5>Bank name : <strong> Ecobank </strong> </h5>
                      <h5>Account name : <strong> Mira Impex </strong> </h5>
                      <h5>Account Number : <strong> 6261006673</strong> </h5>
                      <h5>BBAN : <strong> 008203626100667301</strong> </h5>
                  <br>                      
                  </div>
              </div>
            </div></div>
        

      </div>
    </div>
  </div>
</section>
<input type="hidden" name="order_id" id="order_id" value="<?php echo $order_id; ?>">
<script src="<?php echo base_url(); ?>assets/custom_js/order_details.js?version=<?php echo time(); ?>"></script>