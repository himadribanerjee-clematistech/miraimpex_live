<!DOCTYPE html>
<html>
<head>
	<title>:: Register</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

	<script src="<?php echo base_url();?>assets/custom_js/city_state.js"></script>

	<script src="<?php echo base_url(); ?>assets/custom_js/jquery.mask.js"></script>
<script src="<?php echo base_url(); ?>assets/custom_js/jquery.mask.min.js"></script>

	<style type="text/css">
		.form-control, .custom-select {
			border-radius: 0;
		}
	</style>
</head>
<body>
<?php

$stage = $step;
$verifiedEmail = $registerEmail;

?>
	<div class="container">

<?php
 if($error == 1)
 {
?>
<div class="col-lg-12">Error: <?php echo $errorMsg; ?></div>
<?php
 }
?>
		<div class="modal-dialog">
			<div class="modal-content">				

				<!-- Modal body -->
				<div class="modal-body">

					<input type="hidden" name="step" id="step" value="<?php echo ($stage > 0) ? $stage : ''; ?>">
					<input type="hidden" name="param_mail" id="param_mail" value="<?php echo $verifiedEmail; ?>">


					<form action="" method="POST" id="emailForm">
						<div class="row" id="emailFormDiv">
							<div class="col-lg-12">
								<div class="form-group">
									<div class="row">
										<div class="col-lg-4">
											<label>E-mail Address</label>
										</div>
										<div class="col-lg-8">
											<input type="text" name="email" id="email" required="required" class="form-control" placeholder="Please enter your email address">
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="row">
										<div class="col-lg-4">
											<label>Verification</label>
										</div>
										<div class="col-lg-8">
											<input type="text" name="vcode" id="vcode" required="required" class="form-control">
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="row">
										<div class="col-lg-4"></div>
										<div class="col-lg-8">
											<div class="custom-control custom-checkbox">
												<input type="checkbox" class="custom-control-input" id="accept" name="accept" value="1" required="required">
												<label class="custom-control-label" for="accept" style="font-size: 12px;">Upon creating my account, I agree to: <br> - The <a href="">Mira Implex User Agreement</a> <br> - Receive emails related to Mira Implex membership and services.</label>
											</div>
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="row">
										<div class="col-lg-12">
											<center>
												<input type="submit" class="btn btn-outline-info" id="next_reg" name="next_reg" style="border-radius: 0; width: 40%;" value="Next">
											</center>
										</div>
									</div>
								</div>						
							</div>
						</div>
					</form>


					<div class="row" id="succMsgDiv" style="padding: 20px">
						<div class="col-lg-12">
							<div class="form-group">
								<h6>
									A confirmation email has been sent to your mailbox <span id="email_sent">example@example.com</span>
									<br>
									<small>Please check your email box and continue your registration within 24 hours. or click the link: <span id="clink_"></span></small>
								</h6>

								<p style="font-size: 16px; padding-top: 3%">
									<a href="">Having problem receiving email?</a>
								</p>
							</div>
						</div>
					</div>


					<?php
						
					  if($stage == 3)
					  {
					  
					?>
						<div class="row" id="regFormDiv">
						<form action="" method="POST" id="regForm">
							<div class="col-lg-12">
								<div class="form-group">
									<div class="row">
										<div class="col-lg-4">
											<label><strong>Username</strong></label>
										</div>
										<div class="col-lg-8">
											<span id="email_">example@example.com</span>
											<input type="hidden" name="email" id="email_g" value="<?php echo $registerEmail; ?>">
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="row">
										<div class="col-lg-4">
											<label>Password</label>
										</div>
										<div class="col-lg-8">
											<input type="password" name="password" id="password" required="required" class="form-control" placeholder="Please enter your password">
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="row">
										<div class="col-lg-4">
											<label>Confirm Password</label>
										</div>
										<div class="col-lg-8">
											<input type="password" name="conf_pass" id="conf_pass" required="required" class="form-control" placeholder="Please retype your entered password">
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="row">
										<div class="col-lg-4">
											<label>Region</label>
										</div>
										<div class="col-lg-8">
											<div class="form-group">
												<select name="registrationRegion" class="custom-select" required id="registrationRegion"  onchange="set_country(this,registrationCountry,registrationState,'registrationCountry');">
												<option value=""> -- Select Region -- </option>												
												</select>
											</div>
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="row">
										<div class="col-lg-4">
											<label>Country</label>
										</div>
										<div class="col-lg-8">
											<div class="form-group">
												<select name="registrationCountry" disabled='disabled' class="custom-select" required id="registrationCountry" onchange="javascript:set_city_state(this,registrationState,'registrationState');">
																	
												</select>
											</div>
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="row">
										<div class="col-lg-4">
											<label>State</label>
										</div>
										<div class="col-lg-8">
											<div class="form-group">
												<select name="registrationState" disabled='disabled' class="custom-select" required id="registrationState">
																										
												</select>
											</div>
											
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="row">
										<div class="col-lg-4">
											<label>I am a</label>
										</div>
										<div class="col-lg-8">
											<div class="custom-control custom-radio custom-control-inline">
												<input type="radio" class="custom-control-input" id="type_1" name="type" value="supplier">
												<label class="custom-control-label" for="type_1">Supplier</label>
											</div>

											<div class="custom-control custom-radio custom-control-inline">
												<input type="radio" class="custom-control-input" id="type_2" name="type" value="buyer">
												<label class="custom-control-label" for="type_2">Buyer</label>
											</div>

											<div class="custom-control custom-radio custom-control-inline">
												<input type="radio" class="custom-control-input" id="type_3" name="type" value="both" checked>
												<label class="custom-control-label" for="type_3">Both</label>
											</div>
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="row">
										<div class="col-lg-12">
											<div class="row">
												<div class="col-lg-4">
													<label>Full Name</label>
												</div>
												<div class="col-lg-8">
													<div class="row">
														<div class="col-lg-6 pr-1">
															<input type="text" name="fname" id="fname" class="form-control" placeholder="First Name" required>
														</div>
														<div class="col-lg-6 pl-1">
															<input type="text" name="lname" id="lname" class="form-control" placeholder="Last Name" required>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="row">
										<div class="col-lg-4">
											<label>Company Name</label>
										</div>
										<div class="col-lg-8">
											<input type="text" name="company" id="company" required="required" class="form-control" placeholder="Must be a legally registered company">
										</div>
									</div>
								</div>

								<div class="form-group">
									<div class="row">
										<div class="col-lg-12">
											<div class="row">
												<div class="col-lg-4">
													<label>Tel</label>
												</div>
												<div class="col-lg-8">
													<div class="row">
														<div class="col-lg-2 pr-1">
															<input type="text" class="form-control c_code_" readonly style="padding: 9px;">
															<input type="hidden" name="c_code" id="c_code">
														</div>

														<div class="col-lg-10 pl-1 pr-1">
															<input type="text" name="phone" id="c_num_1" class="form-control input-mask-phone" required>
														</div>

													</div>
												</div>
											</div>
										</div>
									</div>
								</div>


								<div class="form-group">
									<div class="row">
										<div class="col-lg-12">
											<center>
												<input type="submit" class="btn btn-outline-primary" id="submit" name="submit" style="border-radius: 0; width: 40%;" value="Confirm">
											</center>
										</div>
									</div>
								</div>						
							</div>
							</form>
						</div>
					<?php
					}
					?>
					
						

				</div>

			</div>
		</div>
	</div>

	<input type="hidden" name="base_url" id="base_url" value="<?php echo base_url(); ?>">






	<script type="text/javascript">

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}
		
		$(document).ready(function(){
			var base_url = $('#base_url').val();
			var step = parseInt($('#step').val());
			var param_mail = $('#param_mail').val();

			$('#emailFormDiv').show();
			$('#succMsgDiv').hide();
			$('#regFormDiv').hide();

			if (step == '3') {
				$('#emailFormDiv').hide();
				$('#succMsgDiv').hide();
				$('#regFormDiv').show();
				$('#email_').html(param_mail);
				$('#email_g').val(param_mail);
			}

			$('#emailForm').submit(function(e){
				e.preventDefault();
				toastr.clear();

				var email = $('#email').val(); 
				var now = Date.now();

				if($.trim(email)=='' ){
					toastr.error('Please Enter Email Address.');
					$("#email").focus();
					return false;
				}else if(!validateEmail($.trim(email)))
				{
					toastr.error('Invalid Email Address.');
					$("#email").focus();
					return false;
					
				}else {
					$.ajax({
						url: base_url + 'register/check_mail',
						type: 'POST',
						data: { 'email': email },
						dataType: 'JSON',
						success: function(response){
							//console.log(response);
							if(response.status == 1){
								toastr.success(response.message);
								$('#emailFormDiv').hide();
								$('#succMsgDiv').show();
								$('#clink_').html('<a href="' + base_url + 'register?step=2&email=' + email + '&time=' + response.sendTime + '">' + base_url + 'register?step=2&email=' + email + '&time=' + response.sendTime + '</a>');
							}else{
								 toastr.error(response.message);
								 $('#emailFormDiv').show();
								$('#succMsgDiv').hide();
							}	
						},
						error: function(response){
							console.log(response);
						}
					});
				}				
			});

			/* $('#country').change(function(){
				var country = $('#country').val();
				
				if (country == 'Africa') {
					$('.c_code_').val('27');
					$('#c_code').val('27');

				}
				else if (country == 'India') {					

					$('.c_code_').val('91');
					$('#c_code').val('91');
				}

				
			}); */

			$('#regForm').submit(function(e){
				e.preventDefault();
				toastr.clear();				

				var pass = $('#password').val(); 
				var con_pass = $('#conf_pass').val(); 

				if (pass != con_pass) {
					toastr.error('Your Entered Password and Confirm Password Value doesn\'t Matched!');
				}
				else {
					$("#submit").attr("disabled", true);
					var mobileNo = $.trim($('#regForm').find('input[id="c_num_1"]').val());
					var new_mobileNo = mobileNo.replace(/-|\s/g, "");
					$('#c_num_1').val(new_mobileNo);

					var formData = $('#regForm').serializeArray();

					$.ajax({
						url: base_url + 'register/registration',
						type: 'POST',
						data: formData,
						dataType: 'JSON',
						success: function(response){
							//console.log(response);
							 if(response.status == 200){
								 toastr.success(response.message);								
							 }else{
					 	 		toastr.error(response.message);
							 }	
							 $("#submit").attr("disabled", false);
						},
						error: function(response){
							toastr.error(response.message);
							$("#submit").attr("disabled", false);
						}
					});
				}

				//console.log(formData)
			});
		});

	</script>



<?php
   if($stage == 3)
    {
?>


<script type="text/javascript">

    $(document).ready(function(){
        for (region in countries)
        {
            $('#registrationRegion').append('<option value="' + region + '">' + region + '</option>');
		}
		
		var SPMaskBehavior = function (val) {
        return val.replace(/\D/g, '').length === 11 ? '00 00000-0000' : '00 0000-00009';
      },
      spOptions = {
        onKeyPress: function(val, e, field, options) {
          field.mask(SPMaskBehavior.apply({}, arguments), options);
        }
      };
      
	  $('.input-mask-phone').mask(SPMaskBehavior, spOptions);
	  
	  $("#c_num_1").keypress(function (e) {
    //if the letter is not digit then display error and don't type anything
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
      //display error message
      toastr.clear();
      toastr.error('Digits only!');
      return false;
    }
  });
        
    });
</script>

<?php
    }
?>

</body>
</html>