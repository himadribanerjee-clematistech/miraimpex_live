<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {
	protected $salt='HA{t~xOC;07Dp,Q+$y02F??:}/?LMj_';
	function __construct() {
		parent::__construct();
		ERROR_REPORTING('E_ALL^E_NOTICE');
		$this->load->model('Register_Model', 'rm');
	}

	public function index(){
		$currentURL = current_url();
        $params   = $_SERVER['QUERY_STRING'];
        parse_str($params, $params_array);
		
		//print_r($params_array);
		//die;
		$registerEmail = $params_array['email'];
		$registerStep = $params_array['step'];
		$registerTime = $params_array['time'];
		$nowTime = time();

		if(isset($registerEmail) && isset($registerStep) && isset($registerTime))
		{
			$check_duplicate_email = $this->rm->doEmailCheck($registerEmail);	
			
			$diff = $nowTime - $registerTime;                            
			$fullDays    = floor($diff/(60*60*24));   
			$fullHours   = floor(($diff-($fullDays*60*60*24))/(60*60));   
			$fullMinutes = floor(($diff-($fullDays*60*60*24)-($fullHours*60*60))/60);      
			//echo "Difference is $fullDays days, $fullHours hours and $fullMinutes minutes.";
			//die;
			if($fullDays <= 1)
			{
				if($check_duplicate_email)
				{
					$data['error'] = 1;
					$data['step'] = 1;
					$data['errorMsg'] = 'Email id already registered.';
				}elseif(!$check_duplicate_email && $registerEmail!='')
				{
					$data['step'] = 3;
					$data['registerEmail'] = $registerEmail;
					$data['time'] = $registerTime;
				}

			}else{
				$data['step'] = 1;
				$data['error'] = 1;
				$data['proceed'] = 'N';
				$data['errorMsg'] = 'Sorry! This activation link seems a bit older. You have to verify your email id within 24 hours. Please try with a new registration.';
			}

		}
			

        $data['uriParams'] = $params_array;

		//$this->load->view('register', $data);

		$data['view_file'] = 'register';
		view($data);

	}

	public function check_mail(){
		$response=["status" => '0', "message" => '', "field"=>''];
		try {
			$data=$_POST;

			if(isset($data["email"]) && trim($data["email"])==''){
                $response=["status"=>"0","message"=>"Please Enter Email Address.","field"=>'email'];
            }
            else {
            	$check_duplicate_email = $this->rm->doEmailCheck($data["email"]);

                if($check_duplicate_email) {
                    $response=["status"=>"0","message"=>"This Email Id already been activated. Please try with different Credentials."];
                }
                else {
                	$response=["status"=>"1","message"=>"Please Check Your Mailbox.",'sendTime'=>time()];
                }
            }

		} catch (Exception $e) {
			$response=["status" => '0', "message" => '', "error" => $e];
		}
		echo json_encode($response);
	}


	public function sendregistrationemailtoclient(){
		$this->load->library('email');
		$this->email->initialize(array(
			'protocol' => 'smtp',
			'smtp_host' => 'smtp.sendgrid.net',
			'smtp_user' => 'Miraimpex',
			'smtp_pass' => 'Amira2013',
			'mailtype' => 'html',
			'charset' => 'iso-8859-1',
			'smtp_port' => 587,
			'crlf' => "\r\n",
			'newline' => "\r\n"
		  ));

		$response=["status" => '0', "message" => '', "field"=>''];
		try {
			$data=$_POST;

			if(isset($data["email"]) && trim($data["email"])==''){
                $response=["status"=>"0","message"=>"Please Enter Email Address.","field"=>'email'];
            }
            else {
				// Mail Format

			$activationLink = 'http://miraimpextrade.com/register?step=2&email='.$data["email"].'&time='.time();
		
		
			 $to = $data["email"];
			/* $subject = 'Miraimpextrade.com: Verify Email';
			$from = 'admin@miraimpextrade.com';
			$headers = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$headers .= 'From: ' . $from . "\r\n". 'Reply-To: ' . $from . "\r\n" . 'X-Mailer: PHP/' . phpversion(); */

			$message = '<!DOCTYPE html
		  PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	  <html xmlns="http://www.w3.org/1999/xhtml">
	  
	  <head>
		  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		  <title>Verify your email address</title>
		  <style type="text/css" rel="stylesheet" media="all">
			  /* Base ------------------------------ */
			  *:not(br):not(tr):not(html) {
				  font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;
				  -webkit-box-sizing: border-box;
				  box-sizing: border-box;
			  }
	  
			  body {
				  width: 100% !important;
				  height: 100%;
				  margin: 0;
				  line-height: 1.4;
				  background-color: #F5F7F9;
				  color: #839197;
				  -webkit-text-size-adjust: none;
			  }
	  
			  a {
				  color: #414EF9;
			  }
	  
			  /* Layout ------------------------------ */
			  .email-wrapper {
				  width: 100%;
				  margin: 0;
				  padding: 0;
				  background-color: #F5F7F9;
			  }
	  
			  .email-content {
				  width: 100%;
				  margin: 0;
				  padding: 0;
			  }
	  
			  /* Masthead ----------------------- */
			  .email-masthead {
				  padding: 25px 0;
				  text-align: center;
			  }
	  
			  .email-masthead_logo {
				  max-width: 400px;
				  border: 0;
			  }
	  
			  .email-masthead_name {
				  font-size: 16px;
				  font-weight: bold;
				  color: #839197;
				  text-decoration: none;
				  text-shadow: 0 1px 0 white;
			  }
	  
			  /* Body ------------------------------ */
			  .email-body {
				  width: 100%;
				  margin: 0;
				  padding: 0;
				  border-top: 1px solid #E7EAEC;
				  border-bottom: 1px solid #E7EAEC;
				  background-color: #FFFFFF;
			  }
	  
			  .email-body_inner {
				  width: 570px;
				  margin: 0 auto;
				  padding: 0;
			  }
	  
			  .email-footer {
				  width: 570px;
				  margin: 0 auto;
				  padding: 0;
				  text-align: center;
			  }
	  
			  .email-footer p {
				  color: #839197;
			  }
	  
			  .body-action {
				  width: 100%;
				  margin: 30px auto;
				  padding: 0;
				  text-align: center;
			  }
	  
			  .body-sub {
				  margin-top: 25px;
				  padding-top: 25px;
				  border-top: 1px solid #E7EAEC;
			  }
	  
			  .content-cell {
				  padding: 35px;
			  }
	  
			  .align-right {
				  text-align: right;
			  }
	  
			  /* Type ------------------------------ */
			  h1 {
				  margin-top: 0;
				  color: #292E31;
				  font-size: 19px;
				  font-weight: bold;
				  text-align: left;
			  }
	  
			  h2 {
				  margin-top: 0;
				  color: #292E31;
				  font-size: 16px;
				  font-weight: bold;
				  text-align: left;
			  }
	  
			  h3 {
				  margin-top: 0;
				  color: #292E31;
				  font-size: 14px;
				  font-weight: bold;
				  text-align: left;
			  }
	  
			  p {
				  margin-top: 0;
				  color: #839197;
				  font-size: 16px;
				  line-height: 1.5em;
				  text-align: left;
			  }
	  
			  p.sub {
				  font-size: 12px;
			  }
	  
			  p.center {
				  text-align: center;
			  }
	  
			  /* Buttons ------------------------------ */
			  .button {
				  display: inline-block;
				  width: 200px;
				  background-color: #414EF9;
				  border-radius: 3px;
				  color: #ffffff;
				  font-size: 15px;
				  line-height: 45px;
				  text-align: center;
				  text-decoration: none;
				  -webkit-text-size-adjust: none;
				  mso-hide: all;
			  }
	  
			  .button--green {
				  background-color: #28DB67;
			  }
	  
			  .button--red {
				  background-color: #FF3665;
			  }
	  
			  .button--blue {
				  background-color: #414EF9;
			  }
	  
			  /*Media Queries ------------------------------ */
			  @media only screen and (max-width: 600px) {
	  
				  .email-body_inner,
				  .email-footer {
					  width: 100% !important;
				  }
			  }
	  
			  @media only screen and (max-width: 500px) {
				  .button {
					  width: 100% !important;
				  }
			  }
		  </style>
	  </head>
	  
	  <body>
		  <table class="email-wrapper" width="100%" cellpadding="0" cellspacing="0">
			  <tr>
				  <td align="center">
					  <table class="email-content" width="100%" cellpadding="0" cellspacing="0">
						  <!-- Logo -->
						  <tr>
							  <td class="email-masthead">
								  <img class="email-masthead_name"
									  src="http://www.miraimpextrade.com/assets/images/logo_local_supplier.png" style="width: 170px;">
							  </td>
						  </tr>
						  <!-- Email Body -->
						  <tr>
							  <td class="email-body" width="100%">
								  <table class="email-body_inner" align="center" width="570" cellpadding="0" cellspacing="0">
									  <!-- Body content -->
									  <tr>
										  <td class="content-cell">
											  <h1>Verify your email address</h1>
											  <p>Thanks for signing up for Mira Impex! We\'re excited to have you as an early
												  user.
											  </p>
											  <!-- Action -->
											  <table class="body-action" align="center" width="100%" cellpadding="0"
												  cellspacing="0">
												  <tr>
													  <td align="center">
														  <div>
															  <!--[if mso]><v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="{{action_url}}" style="height:45px;v-text-anchor:middle;width:200px;" arcsize="7%" stroke="f" fill="t">
								  <v:fill type="tile" color="#414EF9" />
								  <w:anchorlock/>
								  <center style="color:#ffffff;font-family:sans-serif;font-size:15px;">Verify Email</center>
								</v:roundrect><![endif]-->
															  <a href="'.$activationLink.'" class="button button--blue" style="color:#ffffff">Verify
																  Email</a>
														  </div>
													  </td>
												  </tr>
											  </table>
											  <p>Thanks,<br>The Mira Impex Team</p>
											  <!-- Sub copy -->
											  <table class="body-sub">
												  <tr>
													  <td>
														  <p class="sub">If you\'re having trouble clicking the button, copy
															  and paste the URL below into your web browser. This activation link is vaild for 24 hours.
														  </p>
														  <p class="sub"><a href="'.$activationLink.'">'.$activationLink.'</a></p>
													  </td>
												  </tr>
											  </table>
										  </td>
									  </tr>
								  </table>
							  </td>
						  </tr>
						  <tr>
							  <td>
								  <table class="email-footer" align="center" width="570" cellpadding="0" cellspacing="0">
									  <tr>
										  <td class="content-cell">
											  <p class="sub center">
												  Mira Impex.
												  <br>NO 1 Gacem Road, Jimpex, Kanifing Industrial Area, PO Box 4298 Bakau
												  <br>The Gambia.
												  <br>+220 7117787
											  </p>
										  </td>
									  </tr>
								  </table>
							  </td>
						  </tr>
					  </table>
				  </td>
			  </tr>
		  </table>
	  </body>	  
	  </html>';

	  $this->email->from('info@miraimpextrade.com', 'Mira Impex');
	  $this->email->to($to);
	  //$this->email->cc('another@another-example.com');
	  //$this->email->bcc('them@their-example.com');
	  $this->email->subject('Mira Impex - Just one more step before you register with us!');
	  $this->email->message($message);
	  $this->email->send();

		$response=["status"=>"1","message"=>"Please Check Your Mailbox.","target"=>$to,'sendTime'=>time()];
			
            }

		} catch (Exception $e) {
			$response=["status" => '0', "message" => '', "error" => $e];
		}
		echo json_encode($response);
	}

	public function registration() {
		
		try {
			$data=$_POST;
			//print_r($data);
			//die;
			$error = 0;
			if(!isset($data["email"]) || trim($data["email"])==''){ 
				$error = 1;
				$response=['status' => 400,'msg' => 'Please Enter Email','field'=>'email'];
			 }elseif(isset($data["email"]) && trim($data["email"])!='')
			 {
				
				if(isset($data["email"]) && trim($data["email"])!='' && !preg_match('/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix', $data["email"])){
					$error = 1;
				$response=['status' => 400,'msg' => 'Please Enter Valid Email','field'=>'email'];
				 }else{					
					  $check_duplicate_cust_email = $this->rm->doEmailCheck($data["email"]);
					  if($check_duplicate_cust_email){
						$error = 1;
						$response=['status' => 400,'msg' => ' Email Id Already Registered','field'=>'email'];
					  }
				   
				 }

			 }elseif(!isset($data["password"]) || trim($data["password"])==''){ 
				$error = 1;
				$response=['status' => 400,'msg' => 'Please Enter Password','field'=>'password'];
			 }elseif(!isset($data["conf_pass"]) || trim($data["conf_pass"])==''){ 
				$error = 1;
				$response=['status' => 400,'msg' => 'Please Enter Password','field'=>'conf_pass'];
			 }elseif(trim($data["conf_pass"]) != $data["password"]){ 
				$error = 1;
				$response=['status' => 400,'msg' => 'Password Mismatch','field'=>'conf_pass'];
			 }elseif(!isset($data["registrationRegion"]) || trim($data["registrationRegion"])==''){ 
				$error = 1;
				$response=['status' => 400,'msg' => 'Please Select Region','field'=>'registrationRegion'];
			 }elseif(!isset($data["registrationCountry"]) || trim($data["registrationCountry"])==''){ 
				$error = 1;
				$response=['status' => 400,'msg' => 'Please Select Country','field'=>'registrationCountry'];
			 }elseif(!isset($data["registrationState"]) || trim($data["registrationState"])==''){ 
				$error = 1;
				$response=['status' => 400,'msg' => 'Please Select State','field'=>'registrationState'];
			 }elseif(!isset($data["fname"]) || trim($data["fname"])==''){ 
				$error = 1;
				$response=['status' => 400,'msg' => 'Please Enter Firstname','field'=>'fname'];
			 }elseif(!isset($data["lname"]) || trim($data["lname"])==''){ 
				$error = 1;
				$response=['status' => 400,'msg' => 'Please Enter Lastname','field'=>'lname'];
			 }elseif(!isset($data["phone"]) || trim($data["phone"])==''){ 
				$error = 1;
				$response=['status' => 400,'msg' => 'Please Enter Phone','field'=>'phone'];
			 }
				
			$check_duplicate_cust_phone = $this->rm->doPhoneCheck((string)((int)($data["phone"])));
			if($check_duplicate_cust_phone == 1)
			{
				$error = 1;
				$response = ['status' => 400,'msg'=>'Phone number already exists',"phone"=>$data["phone"]];
				
			}

			$check_duplicate_cust_email = $this->rm->doEmailCheck($data["email"]);
			if($check_duplicate_cust_email){
			  $error = 1;
			  $response=['status' => 400,'msg' => ' Email Id Already Registered','field'=>'email'];
			}
				
			 

			 if($error == 0)
			 {								
					
				$customerdata=[];
				$customerdata["username"] = $data["email"];
				$customerdata["name"] = $data["fname"].' '.$data["lname"];
				$customerdata["email"] = $data["email"];				
				$customerdata["phone"] = (string)((int)($data["phone"]));
				$customerdata["location_key"] = $data["c_code"];
				$customerdata["password"] = md5(crypt($data["password"],$this->salt));
				if($data["type"] == 'buyer')
				{
					$customerdata["supplier_YN"] = 0;
					$customerdata["sellerCategory"] = 0;
				}elseif($data["type"] == 'supplier')
				{
					$customerdata["supplier_YN"] = 1;
					$customerdata["sellerCategory"] = $data["supplierCategory"];
				}elseif($data["type"] == 'both')
				{
					$customerdata["supplier_YN"] = 2;
					$customerdata["sellerCategory"] = $data["supplierCategory"];
				}
				

				$customerdata["phone_otp"] = '';
				$customerdata["active"] = 1;
				$customerdata["phone_verify"] = 1;
				//echo '<pre>';
				//print_r($customerdata);
				//die;
				$response = ['status' => $status,'msg'=>'Registration Successful.'];
				$insertCustomer = $this->rm->doRegisterCustomer($customerdata);

				if($data['company'] == '')
				{
					$data['company'] = $data["email"];
				}
				if($insertCustomer > 0)
				{
					$customeraddressdata = [];
					$customeraddressdata['cust_id'] = $insertCustomer;
					$customeraddressdata['country'] = $data['registrationCountry'];
					$customeraddressdata['state'] = $data['registrationState'];
					$customeraddressdata['business_name'] = $data['company'];
					$insertCustomeraddress = $this->rm->doRegisterCustomerAddress($customeraddressdata);
					//die;
					if($customerdata["supplier_YN"] > 0)
					{
						$sellerdata = [];
						$sellerdata['cust_id'] = $insertCustomer;
						$sellerdata['business_name'] = $data['company'];
						$sellerdata['address'] = $data['address'];
						$sellerdata['country'] = $data['registrationCountry'];
						$sellerdata['state'] = $data['registrationState'];
						$sellerdata['contact_name'] = $data["fname"].' '.$data["lname"];						
						$sellerdata['contact_number'] = (string)((int)($data["phone"]));

						$insertSeller = $this->rm->doRegisterSellerAddress($sellerdata);

					}
					

					if($insertCustomer > 0)
					{
						$response = ['status' => 200,'msg'=>'Registration Successful.'];
					}else{
						$response=['status' => 400,'msg' => 'Something went wrong in address'];
					}				
						
						
				}else{
					$response=['status' => 400,'msg' => 'Something went wrong in customer'];
				}
			 }



		} catch (Exception $e) {
			
			$response = ['msg' => 'Invalid Request!','field'=>'name'];
		}

		echo json_encode($response);
		
	}
}