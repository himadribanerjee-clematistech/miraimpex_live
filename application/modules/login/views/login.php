
<section class="login_page">
	<div class="container">
		<div class="row">
			<div class="col-sm-6 col-sm-offset-3">
				<div class="login_cnt">
					<div class="login_header">
						<h4>Sign in</h4>
					</div>
					<form action="javascript:void(0);">
						<div class="login_form_design">
							<div><i class="fa fa-user"></i>
								<input type="text" class="txt_log" placeholder="Email/Phone" id="uname" name="uname">
							</div>
							<div>
								<i class="fa fa-key"></i>
								<input type="Password" class="txt_log" placeholder="Password" id="password" name="password">
							</div>
							<div>
								<a href="<?php echo base_url();?>register">Join Free</a>
							</div>
							<div><button type="button" class="btn_login" id="loginsubmit" onclick="javascript:doLogin();">Login</button></div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>

<script type="text/javascript">
$(document).ready(function () {
	if (localStorage.getItem("miraimpex_token") === null) {
		$('#top-bar').remove();
		$('#header').remove();
} else {
	//alert($("#base_url").val());
    window.location.replace($("#base_url").val());
  }
});
</script>