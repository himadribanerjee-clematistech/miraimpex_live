<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	protected $salt='HA{t~xOC;07Dp,Q+$y02F??:}/?LMj_';
	function __construct() {
		parent::__construct();
		ERROR_REPORTING('E_ALL^E_NOTICE');
	}

	public function index(){
		$params   = $_SERVER['QUERY_STRING'];
		parse_str($params, $params_array);
		
		if(is_array($params_array) && count($params_array)>0)
		{
			$i=0;
			foreach($params_array as $k=>$v)
			{
				if($k != '' && $v != '')
				{
					if($k == 'tracelog')
					{
						$redirect = $v;
					}else{
						if($i == 0)
						{
							$redirect .= '?'.$k.'='.$v;
						}else{
							$redirect .= '&'.$k.'='.$v;
						}
						
						$i++;
					}
				}
				
			}
			$data['redirect'] = $redirect;
		}
		
		$data['uriParams'] = $params_array;
		
		$data['view_file'] = 'login';
        view($data);
	}

	
}