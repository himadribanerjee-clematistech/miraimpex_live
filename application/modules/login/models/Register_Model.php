<?php

class Register_Model extends CI_Model {

	protected $customerTable = 't_customer';
	
	function __construct() {
		parent::__construct();
	}

	function doEmailCheck($mail) {
		try {
			$query = $this->db->select("email")->from($this->customerTable)->where('email', $mail)->get();

			if($query->num_rows()>0) {
        		return True;
        	}
        	else {
        		return false;
        	}

		} catch (Exception $e) {
			throw new Exception();
		}
	}

	function doPhoneCheck($phone) {
		try {
			$query = $this->db->select("id")->from($this->customerTable)->where('phone', $phone)->get();		  

			if($query->num_rows()>0) {
        		return true;
        	}
        	else {
        		return false;
        	}

		} catch (Exception $e) {
			throw new Exception();
		}
	}

	public function doRegisterCustomer($data){
    	try {
    	 	$this->db->db_debug = FALSE;
    	 	 $this->db->insert($this->customerTable, $data);
	        
	        $db_error = $this->db->error();
	       if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }
	        return  $this->db->insert_id();
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }
    	
	}
	
	public function doRegisterCustomerAddress($data){
    	try {
    	 	$this->db->db_debug = FALSE;
    	 	 $this->db->insert('t_buyer_profile', $data);
	        
	        $db_error = $this->db->error();
	       if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }
	        return  $this->db->insert_id();
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }
    	
    }
}