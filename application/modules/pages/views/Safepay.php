<section class="logistics_pase safe_pay" style="background-image: url(<?php echo base_url();?>assets/images/logo/payment_back.jpg);"> 
    <div class="container">
		<div class="row">
			<div class="col-sm-6">
				<div class="logo_logistic">
					<a href="<?php echo base_url();?>pages/logistics">
                        <img src="<?php echo base_url();?>assets/images/save_paay.png">
                    </a>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="menu_log text-right">
				   <ul>
				   	<li><a href="<?php echo base_url();?>">Home</a></li>
				   	<li><a href="<?php echo base_url();?>pages/contact">Contact Us</a></li>
				   	<li><p>1234567</p></li>
				   </ul>
			    </div>
			</div>
			
		</div>
		<div class="row">
			<div class="col-sm-6">
				<div class="banner_info">
					<h2>Payment Becomes Easy & Reliable</h2>
					<h4>Na Delay! Na Dar! Payment Befikar!</h4>
					<a href="#">Sign Up Now</a>
				</div>
			</div>
			
		</div>
	    
    </div> 

</section>


<div class="banner-block clearfix how_it_work">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
            	<div class="work_heading">
            		<h3>HOW IT WORKS</h3>
            	</div>
            </div>
            <div class="col-sm-6">
                <div class="work_group">
                    <div class="heading back_color">
                        <h4>Seller Information</h4>
                    </div>

                    <div class="work_info">
                        <div class="icon"><img src="<?php echo base_url();?>assets/images/all_icon/seller1.png"></div>
                        <div class="work_info_cnt"><p>Buyer transfers payment to Miraimpex</p></div>
                    </div>
                    <div class="work_info">
                        <div class="icon"><img src="<?php echo base_url();?>assets/images/all_icon/seller2.png"></div>
                        <div class="work_info_cnt"><p>Supplier delivers the product</p></div>
                    </div>
                    <div class="work_info">
                        <div class="icon"><img src="<?php echo base_url();?>assets/images/all_icon/seller3.png"></div>
                        <div class="work_info_cnt"><p>Buyer confirms the shipment</p></div>
                    </div>
                    <div class="work_info">
                        <div class="icon"><img src="<?php echo base_url();?>assets/images/all_icon/seller4.png"></div>
                        <div class="work_info_cnt"><p>Miraimpex transfers payment to seller within 24 hours</p></div>
                    </div>

                </div>
                
            </div>
            <div class="col-sm-6">
                <div class="work_group">
                    <div class="heading back_color">
                        <h4>Buyer Information</h4>
                    </div>

                    <div class="work_info">
                        <div class="icon"><img src="<?php echo base_url();?>assets/images/all_icon/seller1.png"></div>
                        <div class="work_info_cnt"><p>Make payment to Miraimpex</p></div>
                    </div>
                    <div class="work_info">
                        <div class="icon"><img src="<?php echo base_url();?>assets/images/all_icon/seller2.png"></div>
                        <div class="work_info_cnt"><p>Receive the product from seller</p></div>
                    </div>
                    <div class="work_info">
                        <div class="icon"><img src="<?php echo base_url();?>assets/images/all_icon/seller3.png"></div>
                        <div class="work_info_cnt"><p>Confirm the shipment</p></div>
                    </div>
                    <div class="work_info">
                        <div class="icon"><img src="<?php echo base_url();?>assets/images/all_icon/seller4.png"></div>
                        <div class="work_info_cnt"><p>Payment transferred to the seller</p></div>
                    </div>

                </div>
                
            </div>
        </div>
    </div>
</div>





<div class="payment_plan" style="background-image: url(<?php echo base_url();?>assets/images/paysub_banner.jpg);">
    <div class="container">
        <div class="row">
        	<div class="col-md-6 col-md-offset-6">
            	<div class="payment_plan_info">
            		<h4>WHY TO SELECT MIRAIMPEX PAYMENT PROTECTION PLAN?</h4>
                    <ul class="payment_plan_info_item">
                        <li><p><i class="fa fa-check"></i> It is a free service</p></li>
                        <li><p><i class="fa fa-check"></i> Safe & Secure Platform</p></li>
                        <li><p><i class="fa fa-check"></i> Instant Support</p></li>
                        <li><p><i class="fa fa-check"></i> No Setup & Operational Cost.</p></li>
                    </ul>
                    <ul class="payment_plan_info_card">
                        <li><img src="<?php echo base_url();?>assets/images/pay/visa.png"></li>
                        <li><img src="<?php echo base_url();?>assets/images/pay/credit-card.png"></li>
                        <li><img src="<?php echo base_url();?>assets/images/pay/discover.png"></li>
                        <li><img src="<?php echo base_url();?>assets/images/pay/maestro.png"></li>
                        <li><img src="<?php echo base_url();?>assets/images/pay/rupay.png"></li>
                        <li><img src="<?php echo base_url();?>assets/images/pay/worldpay.png"></li>
                    </ul>
            	</div>
            </div>
            
        </div>
    </div>
</div>



<div class="FAQ_sectin">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="header back_color">
                    <h4>FAQ’S</h4>
                </div>
            </div>
            <div class="col-md-12">
                <div class="FAQ_sectin_info">
                    <ul>
                        <li>
                            <!-- <div class="icon_faq"><i class="fa fa-search"></i></div> -->
                            <div class="faq_cnt">
                                <h5>What exactly does miraimpex Payment Protection offer?</h5>
                                <p>miraimpex Payment Protection provides a method of secure & trusted transaction. It offers a great buying and selling experience to the users.</p>
                            </div>
                        </li>
                        <li>
                            <!-- <div class="icon_faq"><i class="fa fa-search"></i></div> -->
                            <div class="faq_cnt">
                                <h5>What is the need of sellers' bank account detail?</h5>
                                <p>Bank account information of sellers is needed to make direct payments into their account. It will be done after buyer acceptance for the product delivered.</p>
                            </div>
                        </li>
                        <li>
                            <div class="faq_cnt">
                                <h5>Is there any charge to use miraimpex Payment Protection?</h5>
                                <p>No, there is no charge. The service is free of cost.</p>
                            </div>
                        </li>
                        <li>
                            <div class="faq_cnt">
                                <h5>What kinds of payment methods are there?</h5>
                                <p>A buyer can pay through Debit Card, Credit Card, Net Banking.</p>
                            </div>
                        </li>
                    </ul>
                    <div class="text-center">
                        <a href="#" class="read_more">Read More <i class="fa fa-angle-double-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>





<div class="know_more">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
            	<div class="know_more_info">
            		<h4>To know more about MIPay</h4>
            		<p><i class="fa fa-phone"></i>00123456789</p>
            		<p> <i class="fa fa-envelope"></i>mipay@miraimpex.com</p>
            	</div>
            </div>
         </div>
    </div>
</div>



<!-- --------------------footer-------------- -->
<div class="logistics_footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
            	<a href="#"><img src="<?php echo base_url();?>assets/images/logo.png"></a>
            </div>
            <div class=" col-sm-6 sl-block text-right">
                <h4>Follow us</h4>
                <div>
                    <a href="#"><img src="<?php echo base_url();?>assets/images/i-1.png" alt=""/></a>
                    <a href="#"><img src="<?php echo base_url();?>assets/images/i-2.png" alt=""/></a>
                    <a href="#"><img src="<?php echo base_url();?>assets/images/i-3.png" alt=""/></a>
                    <a href="#"><img src="<?php echo base_url();?>assets/images/i-4.png" alt=""/></a>
                    <a href="#"><img src="<?php echo base_url();?>assets/images/i-5.png" alt=""/></a>
                </div>  
                <div class="sub_menu_logis">
                	<a href="#">About MI Logistics</a>
                	<a href="#">Contact us</a>
                	<a href="#">User Terms and Privacy Notice</a>
                </div>
            </div>
            <div class="col-sm-12 text-cenrter">
            	<div class="copy"><p>© 2020 mira impex. All Rights Reserved. Designed by <a href="#">clematistech.com</a></p></div>
                 
             </div>
        </div>
    </div>
</div>