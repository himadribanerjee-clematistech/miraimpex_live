<section class="logistics_pase Contact_page" > 
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="logo_logistic">
                   <a href="#"><img src="<?php echo base_url();?>assets/images/logistics.png">
                    </a>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="menu_log text-right">
                   <ul>
                    <li><a href="<?php echo base_url();?>">Home</a></li>
                    <li><p>Contact Us</p></li>
                   </ul>
                </div>
            </div>
            
        </div>
        
        
    </div> 

</section>

<!-- ---------------------contact form------------ -->



<div class="logistics_contact_sect">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="cont_cnt">
                    <div><h4 class="heading"><span>Write to us</span></h4></div>
                    <form action="javascript:void(0);" method="post" name="addSellerDocumentForm" id="addSellerDocumentForm"  enctype="multipart/form-data">
                        <div class="form_design">
                            <select class="txt-file" id="purpose" name="purpose">
                                <option value="General">Select Purpose (Sales, Feedback, etc)</option>
                                <option value="General">General</option>
                                <option value="Buy Requirement">Buy Requirement</option>
                            </select>
                        </div>
                        <div class="form_design">
                            <input type="text" class="txt-file" placeholder="Name" name="name" id="name">
                        </div>
                        <div class="form_design">
                            <input type="text" class="txt-file" placeholder="Subject" name="subject" id="subject">
                        </div>
                        <div class="form_design">
                            <input type="text" class="txt-file" placeholder="Email" name="email" id="email">
                        </div>
                        <div class="form_design">
                            <input type="text" class="txt-file" placeholder="Phone" name="phone" id="phone">
                        </div>
                        <div class="form_design">
                            <input type="text" class="txt-file" placeholder="Company Name" name="company" id="company">
                        </div>
                        <div class="form_design">
                            <textarea class="txt-file" rows="4" placeholder="Message..." name="message" id="message"></textarea>
                        </div>
                        <div class="form_design">
                            <button type="Submit" class="btn_sent_mail" id="quoteButton">Send Email</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="cont_cnt">
                    <div><h4 class="heading"><span>Our Locations</span></h4></div>
                   <div class="address_info">
                       <h4>Corporate Office</h4>
                       <p>NO 1 Gacem Road</p>
                       <p>Jimpex,</p>
                       <p>Kanifing Industrial Area,</p>
                       <p>PO Box 4298 Bakau</p>
                       <p>The Gambia.</p>
                       <p> <b><i class="fa fa-phone"></i> +220 7117787</b></p>
                       <div class="send_mail"><a href="#">Send Email</a></div>
                   </div>

                   <div class="address_info">
                       <h4>Correspondent Address:</h4>
                       <p>217 W 31st st S</p>
                       <p>Wichita, KS 67217</p>
                       <p>U.S.A</p>
                       <p> <b><i class="fa fa-phone"></i> +1 386 383 9732</b></p>
                       <div class="send_mail"><a href="#">Send Email</a></div>
                   </div>

                </div>
            </div>
        </div>
    </div>
</div>












<!-- ----------------------footer----------- -->

<div class="logistics_footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
            	<a href="<?php echo base_url();?>"><img src="<?php echo base_url();?>assets/images/logo.png"></a>
            </div>
            <div class=" col-sm-6 sl-block text-right">
                <h4>Follow us</h4>
                <div>
                    <a href="#"><img src="<?php echo base_url();?>assets/images/i-1.png" alt=""/></a>
                    <a href="#"><img src="<?php echo base_url();?>assets/images/i-2.png" alt=""/></a>
                    <a href="#"><img src="<?php echo base_url();?>assets/images/i-3.png" alt=""/></a>
                    <a href="#"><img src="<?php echo base_url();?>assets/images/i-4.png" alt=""/></a>
                    <a href="#"><img src="<?php echo base_url();?>assets/images/i-5.png" alt=""/></a>
                </div>  
                <div class="sub_menu_logis">
                	<a href="#">About MI Logistics</a>
                	<a href="#">Contact us</a>
                	<a href="#">User Terms and Privacy Notice</a>
                </div>
            </div>
            <div class="col-sm-12 text-cenrter">
            	<div class="copy"><p>© 2020 mira impex. All Rights Reserved. Designed by <a href="#">clematistech.com</a></p></div>
                 
             </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/custom_js/contact_us.js?version=<?php echo time(); ?>"></script>