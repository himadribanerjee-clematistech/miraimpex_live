<section class="logistics_pase custom-logistic_pase" style="background-image: url(<?php echo base_url();?>assets/images/LOGISTIC2.jpg);"> 
    <div class="container">
		<div class="row">
			<div class="col-sm-6 col-xs-4">
				<div class="logo_logistic">
					<a href="#"><img src="<?php echo base_url();?>assets/images/logistics.png">
                    </a>
				</div>
			</div>
			<div class="col-sm-6 col-xs-8">
				<div class="menu_log text-right">
				   <ul>
				   	<li><a href="<?php echo base_url();?>">Home</a></li>
				   	<li><a href="<?php echo base_url();?>pages/contact">Contact Us</a></li>
				   	<li><p>+220 7117787</p></li>
				   </ul>
			    </div>
			</div>
			
		</div>
		<div class="row">
			<div class="col-sm-6 ">
				<div class="banner_info">
					<h2>Connecting Logistics to your business.</h2>
					<p>MI Logistics aims to fulfill your logistics requirements so that you can invest more time in your valuable business.</p>
					<a href="javascript:void(0);" data-toggle="modal" data-target="#add_sellerdocument_modal">Get an instant online quote</a>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="banner_img">
				   <!-- <img src="<?php echo base_url();?>assets/images/banner_slider.png">  -->
				</div>
			</div>
		</div>
	    
    </div> 

</section>

<div class="modal fade profile_model_design" id="add_sellerdocument_modal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Online Quotation</h4>

        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body buyer_profile_edit">
        <form action="javascript:void(0);" method="post" name="addSellerDocumentForm" id="addSellerDocumentForm"  enctype="multipart/form-data">
          <div class="row">
          <div class="col-md-6">
			<div class="form-group row sellerInfoPanel">
				<label for="inputPassword" class="col-sm-12 col-form-label p-no_left">Name:</label>
				<div class="col-sm-12 p-no_right">
					<input type="text" name="name" id="name" class="form-control" placeholder="Name">
				</div>
			</div>
		  </div>      
		  
		  <div class="col-md-6">
			<div class="form-group row sellerInfoPanel">
				<label for="inputPassword" class="col-sm-12 col-form-label p-no_left">Email:</label>
				<div class="col-sm-12 p-no_right">
					<input type="email" name="email" id="email" class="form-control" placeholder="Email">
				</div>
			</div>
		  </div> 

		  <div class="col-md-6">
			<div class="form-group row sellerInfoPanel">
				<label for="inputPassword" class="col-sm-12 col-form-label p-no_left">Phone:</label>
				<div class="col-sm-12 p-no_right">
					<input type="phone" name="phone" id="phone" class="form-control" placeholder="Phone">
				</div>
			</div>
		  </div> 

		  <div class="col-md-6">
			<div class="form-group row sellerInfoPanel">
				<label for="inputPassword" class="col-sm-12 col-form-label p-no_left">Description:</label>
				<div class="col-sm-12 p-no_right">
					<textarea name="description" id="description" class="form-control" rows="5"></textarea>
				</div>
			</div>
		  </div> 
		  
      </div>

       
        <div class="form-group text-center btn_cover_design">
          <button type="submit" class="btn_submit" id="quoteButton" >Submit </button>
        </div>
                            
        </form>
      </div>
      
    </div>
  </div>
</div>

<!-- <div class="banner-block clearfix logis_SMEs_section" style="background-image: url(<?php echo base_url();?>assets/images/banner/banner-img-3.jpg);"> -->
<div class="banner-block clearfix logis_SMEs_section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
            	<div class="SMEs_info">
            		<h2>WHAT IS IN MI FOR SMEs?</h2>
            		<P>Miraimpex has been consistently connecting Buyers and Suppliers enhancing their business for the past 23 years. To strengthen the business further Miraimpex has partnered with ReturnTrucks which provides a platform to SMEs to facilitate shipment of goods in an efficient and timely manner. Customers can post their load and find matching Express delivery service provider. Customers can check the prices for each of the delivery service providers like Gati, VRL, Safexpress on this platform and get the best deals for their shipments. With a facility of tracking your order with GPS technology and a 24/7 support team, we thrive to provide a hassle-free shipping experience to our customers.</P>
            	</div>
            </div>
        </div>
    </div>
</div>


<div class="delivery_made">
    <div class="container">
        <div class="row">
        	<div class="col-md-12">
            	<div class="delivery_made_info">
            		<h4>DELIVERY MADE EASY</h4>
            	</div>
            </div>
            <div class="col-md-12">
            	<div class="delivery_made_info">
            		<ul>
            			<li>
            				<div>
            					<img src="<?php echo base_url();?>assets/images/all_icon/ship.png">
            					<p>Post Your Shipment</p>
            				</div>
            			</li>
            			<li>
            				<div>
            					<img src="<?php echo base_url();?>assets/images/all_icon/Quote.png">
            					<p>Choose The Best Transporter Quote</p>
            				</div>
            			</li>
            			<li>
            				<div>
            					<img src="<?php echo base_url();?>assets/images/all_icon/Pickup.png">
            					<p>Ready Shipment For Pickup</p>
            				</div>
            			</li>
            			<li>
            				<div>
            					<img src="<?php echo base_url();?>assets/images/all_icon/payment.png">
            					<p>Make Payment To Transporter</p>
            				</div>
            			</li>
            			<li>
            				<div>
            					<img src="<?php echo base_url();?>assets/images/all_icon/delivery.png">
            					<p>Shipment Delivered</p>
            				</div>
            			</li>

            		</ul>
            	</div>
            </div>
        </div>
    </div>
</div>

<div class="banner-block clearfix logis_SMEs_section logis_coustomer_section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
            	<div class="SMEs_info">
            		<h2>BENEFITS FOR CUSTOMERS</h2>
            		
            	</div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3 col-xs-6">
            	<div class="customer_info">
            		<div class="custo_icon">
            			<img src="<?php echo base_url();?>assets/images/all_icon/money-bag(2).png">
            		</div>
            		<div class="content">
            			<h4>Get the best quotes for your order</h4>
            		</div>
            	</div>
            </div>
            <div class="col-sm-3 col-xs-6">
            	<div class="customer_info">
            		<div class="custo_icon">
            			<img src="<?php echo base_url();?>assets/images/all_icon/medal(1).png">
            		</div>
            		<div class="content">
            			<h4>Leading logistics providers: Gati, VRL</h4>
            		</div>
            	</div>
            </div>
            <div class="col-sm-3 col-xs-6">
            	<div class="customer_info">
            		<div class="custo_icon">
            			<img src="<?php echo base_url();?>assets/images/all_icon/travel.png">
            		</div>
            		<div class="content">
            			<h4>Ship Anywhere: Extensive nationwide network</h4>
            		</div>
            	</div>
            </div>
            <div class="col-sm-3 col-xs-6">
            	<div class="customer_info">
            		<div class="custo_icon">
            			<img src="<?php echo base_url();?>assets/images/all_icon/truck.png">
            		</div>
            		<div class="content">
            			<h4>Door pickup and door delivery facility</h4>
            		</div>
            	</div>
            </div>
            <div class="col-sm-3 col-xs-6">
            	<div class="customer_info">
            		<div class="custo_icon">
            			<img src="<?php echo base_url();?>assets/images/all_icon/logistics-delivery.png">
            		</div>
            		<div class="content">
            			<h4>Lesser transit time and on-time delivery</h4>
            		</div>
            	</div>
            </div>
            <div class="col-sm-3 col-xs-6">
            	<div class="customer_info">
            		<div class="custo_icon">
            			<img src="<?php echo base_url();?>assets/images/all_icon/track.png">
            		</div>
            		<div class="content">
            			<h4>Online Live Tracking facility</h4>
            		</div>
            	</div>
            </div>
            <div class="col-sm-3 col-xs-6">
            	<div class="customer_info">
            		<div class="custo_icon">
            			<img src="<?php echo base_url();?>assets/images/all_icon/support.png">
            		</div>
            		<div class="content">
            			<h4>24/7 support team</h4>
            		</div>
            	</div>
            </div>
            <div class="col-sm-3 col-xs-6">
            	<div class="customer_info">
            		<div class="custo_icon">
            			<img src="<?php echo base_url();?>assets/images/all_icon/calendar.png">
            		</div>
            		<div class="content">
            			<h4>24*7*365 days operations</h4>
            		</div>
            	</div>
            </div>
        </div>
    </div>
</div>

<div class="know_more">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
            	<div class="know_more_info">
            		<h4>To Know more about MI Logistics</h4>
            		<p><i class="fa fa-phone"></i>00123456789</p>
            		<p> <i class="fa fa-envelope"></i>logistics@miraimpex.com</p>
            	</div>
            </div>
         </div>
    </div>
</div>



<!-- --------------------footer-------------- -->
<div class="logistics_footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
            	<a href="#"><img src="<?php echo base_url();?>assets/images/logo.png"></a>
            </div>
            <div class=" col-sm-6 sl-block text-right">
                <h4>Follow us</h4>
                <div>
                    <a href="#"><img src="<?php echo base_url();?>assets/images/i-1.png" alt=""/></a>
                    <a href="#"><img src="<?php echo base_url();?>assets/images/i-2.png" alt=""/></a>
                    <a href="#"><img src="<?php echo base_url();?>assets/images/i-3.png" alt=""/></a>
                    <a href="#"><img src="<?php echo base_url();?>assets/images/i-4.png" alt=""/></a>
                    <a href="#"><img src="<?php echo base_url();?>assets/images/i-5.png" alt=""/></a>
                </div>  
                <div class="sub_menu_logis">
                	<a href="#">About MI Logistics</a>
                	<a href="#">Contact us</a>
                	<a href="#">User Terms and Privacy Notice</a>
                </div>
            </div>
            <div class="col-sm-12 text-cenrter">
            	<div class="copy"><p>© 2020 mira impex. All Rights Reserved. Designed by <a href="#">clematistech.com</a></p></div>
                 
             </div>
        </div>
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/custom_js/logistics.js?version=<?php echo time(); ?>"></script>