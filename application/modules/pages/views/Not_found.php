<section class="not_found_page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="not_found_info">
					<img src="<?php echo base_url();?>assets/images/not_found.jpg">
					<h2>Oops! We can't find the page you're looking for</h2>
					<a href="#">Go to Home Page</a>
				</div>
			</div>

		</div>
	</div>
	
</section>