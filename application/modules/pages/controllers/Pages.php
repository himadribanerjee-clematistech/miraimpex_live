<?php

/**
 * 
 */
class Pages extends CI_Controller {
	
	function __construct() {
		parent::__construct();
	}

	public function index($value='') {
		echo "Welcome To Pages";
	}

	public function logistics($value='') {
		$data['view_file'] = 'Logistics';
		customview($data);
		
	}

	public function sendemaillogistics(){
		$this->load->library('email');
		$this->email->initialize(array(
			'protocol' => 'smtp',
			'smtp_host' => 'smtp.sendgrid.net',
			'smtp_user' => 'Miraimpex',
			'smtp_pass' => 'Amira2013',
			'mailtype' => 'html',
			'charset' => 'iso-8859-1',
			'smtp_port' => 587,
			'crlf' => "\r\n",
			'newline' => "\r\n"
		  ));

		$response=["status" => '0', "message" => '', "field"=>''];
		try {
			$data=$_POST;

			if(isset($data["email"]) && trim($data["email"])==''){
                $response=["status"=>"0","message"=>"Please Enter Email Address.","field"=>'email'];
            }
            else {

				$insertArr['Name'] = $data['name'];
				$insertArr['Email'] = $data['email'];
				$insertArr['Phone'] = $data['phone'];
				$insertArr['Message'] = $data['description'];
				

				$newarr['details'] = json_encode($insertArr);
				$newarr['type'] = 1;

				try {
					$this->db->db_debug = true;
					$res = $this->db->insert('customer_enquiry', $newarr);
	   
					$db_error = $this->db->error();
					  if (!empty($db_error) && $db_error['code']>0) {
					   throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
					   return false; // unreachable retrun statement !!!
				   }
				   //return  $this->db->insert_id();
			   } catch (Exception $e) {
				   throw new Exception();
				   
			   }

				// Mail Format		
		
			 $from = $data["email"];
			/* $subject = 'Miraimpextrade.com: Verify Email';
			$from = 'admin@miraimpextrade.com';
			$headers = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$headers .= 'From: ' . $from . "\r\n". 'Reply-To: ' . $from . "\r\n" . 'X-Mailer: PHP/' . phpversion(); */

			$message = '<!DOCTYPE html
		  PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	  <html xmlns="http://www.w3.org/1999/xhtml">
	  
	  <head>
		  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		  <title>Verify your email address</title>
		  <style type="text/css" rel="stylesheet" media="all">
			  /* Base ------------------------------ */
			  *:not(br):not(tr):not(html) {
				  font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;
				  -webkit-box-sizing: border-box;
				  box-sizing: border-box;
			  }
	  
			  body {
				  width: 100% !important;
				  height: 100%;
				  margin: 0;
				  line-height: 1.4;
				  background-color: #F5F7F9;
				  color: #839197;
				  -webkit-text-size-adjust: none;
			  }
	  
			  a {
				  color: #414EF9;
			  }
	  
			  /* Layout ------------------------------ */
			  .email-wrapper {
				  width: 100%;
				  margin: 0;
				  padding: 0;
				  background-color: #F5F7F9;
			  }
	  
			  .email-content {
				  width: 100%;
				  margin: 0;
				  padding: 0;
			  }
	  
			  /* Masthead ----------------------- */
			  .email-masthead {
				  padding: 25px 0;
				  text-align: center;
			  }
	  
			  .email-masthead_logo {
				  max-width: 400px;
				  border: 0;
			  }
	  
			  .email-masthead_name {
				  font-size: 16px;
				  font-weight: bold;
				  color: #839197;
				  text-decoration: none;
				  text-shadow: 0 1px 0 white;
			  }
	  
			  /* Body ------------------------------ */
			  .email-body {
				  width: 100%;
				  margin: 0;
				  padding: 0;
				  border-top: 1px solid #E7EAEC;
				  border-bottom: 1px solid #E7EAEC;
				  background-color: #FFFFFF;
			  }
	  
			  .email-body_inner {
				  width: 570px;
				  margin: 0 auto;
				  padding: 0;
			  }
	  
			  .email-footer {
				  width: 570px;
				  margin: 0 auto;
				  padding: 0;
				  text-align: center;
			  }
	  
			  .email-footer p {
				  color: #839197;
			  }
	  
			  .body-action {
				  width: 100%;
				  margin: 30px auto;
				  padding: 0;
				  text-align: center;
			  }
	  
			  .body-sub {
				  margin-top: 25px;
				  padding-top: 25px;
				  border-top: 1px solid #E7EAEC;
			  }
	  
			  .content-cell {
				  padding: 35px;
			  }
	  
			  .align-right {
				  text-align: right;
			  }
	  
			  /* Type ------------------------------ */
			  h1 {
				  margin-top: 0;
				  color: #292E31;
				  font-size: 19px;
				  font-weight: bold;
				  text-align: left;
			  }
	  
			  h2 {
				  margin-top: 0;
				  color: #292E31;
				  font-size: 16px;
				  font-weight: bold;
				  text-align: left;
			  }
	  
			  h3 {
				  margin-top: 0;
				  color: #292E31;
				  font-size: 14px;
				  font-weight: bold;
				  text-align: left;
			  }
	  
			  p {
				  margin-top: 0;
				  color: #839197;
				  font-size: 16px;
				  line-height: 1.5em;
				  text-align: left;
			  }
	  
			  p.sub {
				  font-size: 12px;
			  }
	  
			  p.center {
				  text-align: center;
			  }
	  
			  /* Buttons ------------------------------ */
			  .button {
				  display: inline-block;
				  width: 200px;
				  background-color: #414EF9;
				  border-radius: 3px;
				  color: #ffffff;
				  font-size: 15px;
				  line-height: 45px;
				  text-align: center;
				  text-decoration: none;
				  -webkit-text-size-adjust: none;
				  mso-hide: all;
			  }
	  
			  .button--green {
				  background-color: #28DB67;
			  }
	  
			  .button--red {
				  background-color: #FF3665;
			  }
	  
			  .button--blue {
				  background-color: #414EF9;
			  }
	  
			  /*Media Queries ------------------------------ */
			  @media only screen and (max-width: 600px) {
	  
				  .email-body_inner,
				  .email-footer {
					  width: 100% !important;
				  }
			  }
	  
			  @media only screen and (max-width: 500px) {
				  .button {
					  width: 100% !important;
				  }
			  }
		  </style>
	  </head>
	  
	  <body>
		  <table class="email-wrapper" width="100%" cellpadding="0" cellspacing="0">
			  <tr>
				  <td align="center">
					  <table class="email-content" width="100%" cellpadding="0" cellspacing="0">
						  <!-- Logo -->
						  <tr>
							  <td class="email-masthead">
								  <img class="email-masthead_name"
									  src="http://www.miraimpextrade.com/assets/images/logo_local_supplier.png" style="width: 170px;">
							  </td>
						  </tr>
						  <!-- Email Body -->
						  <tr>
							  <td class="email-body" width="100%">
								  <table class="email-body_inner" align="center" width="570" cellpadding="0" cellspacing="0">
									  <!-- Body content -->
									  <tr>
										  <td class="content-cell">
											  <h1>New Logistics Enquiry</h1>
											  <!-- Action -->
											  <table class="body-action" align="center" width="100%" cellpadding="0"
												  cellspacing="0">
												  <tr>
													  <td align="center">
														  <div>
														  <p><span style="font-weight:bold;">Name: </span> '.$data["name"].'<br />
														  <span style="font-weight:bold;">Email: </span> '.$data["email"].'<br />
														  <span style="font-weight:bold;">Phone: </span> '.$data["phone"].'<br />
														  <span style="font-weight:bold;">Message: </span> '.$data["description"].'<br />
													  </p>
														  </div>
													  </td>
												  </tr>
											  </table>
											  <p>Thanks,<br>The Mira Impex Team</p>
										  </td>
									  </tr>
								  </table>
							  </td>
						  </tr>
						  <tr>
							  <td>
								  <table class="email-footer" align="center" width="570" cellpadding="0" cellspacing="0">
									  <tr>
										  <td class="content-cell">
											  <p class="sub center">
												  Mira Impex.
												  <br>NO 1 Gacem Road, Jimpex, Kanifing Industrial Area, PO Box 4298 Bakau
												  <br>The Gambia.
												  <br>+220 7117787
											  </p>
										  </td>
									  </tr>
								  </table>
							  </td>
						  </tr>
					  </table>
				  </td>
			  </tr>
		  </table>
	  </body>	  
	  </html>';

	  $this->email->from($from, $data["name"]);
	  $this->email->to('info@miraimpextrade.com', 'Mira Impex');
	  $this->email->subject('New Logistics Enquiry from '.$data["name"]);
	  $this->email->message($message);
	  $this->email->send();

		$response=["status"=>"1","message"=>"Thank you for your enquiry. We will get back to you soon."];
			
            }

		} catch (Exception $e) {
			$response=["status" => '0', "message" => '', "error" => $e];
		}
		echo json_encode($response);
	}

	public function sendemailcontact(){
		$this->load->library('email');
		$this->email->initialize(array(
			'protocol' => 'smtp',
			'smtp_host' => 'smtp.sendgrid.net',
			'smtp_user' => 'Miraimpex',
			'smtp_pass' => 'Amira2013',
			'mailtype' => 'html',
			'charset' => 'iso-8859-1',
			'smtp_port' => 587,
			'crlf' => "\r\n",
			'newline' => "\r\n"
		  ));

		$response=["status" => '0', "message" => '', "field"=>''];
		try {
			$data=$_POST;

			if(isset($data["email"]) && trim($data["email"])==''){
                $response=["status"=>"0","message"=>"Please Enter Email Address.","field"=>'email'];
            }
            else {
				$insertArr['Purpose'] = $data['purpose'];
				$insertArr['Name'] = $data['name'];
				$insertArr['Subject'] = $data['subject'];
				$insertArr['Email'] = $data['email'];
				$insertArr['Phone'] = $data['phone'];
				$insertArr['Company'] = $data['company'];
				$insertArr['Message'] = $data['message'];

				$newarr['details'] = json_encode($insertArr);
				$newarr['type'] = 2;

				try {
					$this->db->db_debug = true;
					$res = $this->db->insert('customer_enquiry', $newarr);
	   
					$db_error = $this->db->error();
					  if (!empty($db_error) && $db_error['code']>0) {
					   throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
					   return false; // unreachable retrun statement !!!
				   }
				   //return  $this->db->insert_id();
			   } catch (Exception $e) {
				   throw new Exception();
				   
			   }

				// Mail Format		
		
			 $from = $data["email"];

			$message = '<!DOCTYPE html
		  PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	  <html xmlns="http://www.w3.org/1999/xhtml">
	  
	  <head>
		  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		  <title>Verify your email address</title>
		  <style type="text/css" rel="stylesheet" media="all">
			  /* Base ------------------------------ */
			  *:not(br):not(tr):not(html) {
				  font-family: Arial, \'Helvetica Neue\', Helvetica, sans-serif;
				  -webkit-box-sizing: border-box;
				  box-sizing: border-box;
			  }
	  
			  body {
				  width: 100% !important;
				  height: 100%;
				  margin: 0;
				  line-height: 1.4;
				  background-color: #F5F7F9;
				  color: #839197;
				  -webkit-text-size-adjust: none;
			  }
	  
			  a {
				  color: #414EF9;
			  }
	  
			  /* Layout ------------------------------ */
			  .email-wrapper {
				  width: 100%;
				  margin: 0;
				  padding: 0;
				  background-color: #F5F7F9;
			  }
	  
			  .email-content {
				  width: 100%;
				  margin: 0;
				  padding: 0;
			  }
	  
			  /* Masthead ----------------------- */
			  .email-masthead {
				  padding: 25px 0;
				  text-align: center;
			  }
	  
			  .email-masthead_logo {
				  max-width: 400px;
				  border: 0;
			  }
	  
			  .email-masthead_name {
				  font-size: 16px;
				  font-weight: bold;
				  color: #839197;
				  text-decoration: none;
				  text-shadow: 0 1px 0 white;
			  }
	  
			  /* Body ------------------------------ */
			  .email-body {
				  width: 100%;
				  margin: 0;
				  padding: 0;
				  border-top: 1px solid #E7EAEC;
				  border-bottom: 1px solid #E7EAEC;
				  background-color: #FFFFFF;
			  }
	  
			  .email-body_inner {
				  width: 570px;
				  margin: 0 auto;
				  padding: 0;
			  }
	  
			  .email-footer {
				  width: 570px;
				  margin: 0 auto;
				  padding: 0;
				  text-align: center;
			  }
	  
			  .email-footer p {
				  color: #839197;
			  }
	  
			  .body-action {
				  width: 100%;
				  margin: 30px auto;
				  padding: 0;
				  text-align: center;
			  }
	  
			  .body-sub {
				  margin-top: 25px;
				  padding-top: 25px;
				  border-top: 1px solid #E7EAEC;
			  }
	  
			  .content-cell {
				  padding: 35px;
			  }
	  
			  .align-right {
				  text-align: right;
			  }
	  
			  /* Type ------------------------------ */
			  h1 {
				  margin-top: 0;
				  color: #292E31;
				  font-size: 19px;
				  font-weight: bold;
				  text-align: left;
			  }
	  
			  h2 {
				  margin-top: 0;
				  color: #292E31;
				  font-size: 16px;
				  font-weight: bold;
				  text-align: left;
			  }
	  
			  h3 {
				  margin-top: 0;
				  color: #292E31;
				  font-size: 14px;
				  font-weight: bold;
				  text-align: left;
			  }
	  
			  p {
				  margin-top: 0;
				  color: #839197;
				  font-size: 16px;
				  line-height: 1.5em;
				  text-align: left;
			  }
	  
			  p.sub {
				  font-size: 12px;
			  }
	  
			  p.center {
				  text-align: center;
			  }
	  
			  /* Buttons ------------------------------ */
			  .button {
				  display: inline-block;
				  width: 200px;
				  background-color: #414EF9;
				  border-radius: 3px;
				  color: #ffffff;
				  font-size: 15px;
				  line-height: 45px;
				  text-align: center;
				  text-decoration: none;
				  -webkit-text-size-adjust: none;
				  mso-hide: all;
			  }
	  
			  .button--green {
				  background-color: #28DB67;
			  }
	  
			  .button--red {
				  background-color: #FF3665;
			  }
	  
			  .button--blue {
				  background-color: #414EF9;
			  }
	  
			  /*Media Queries ------------------------------ */
			  @media only screen and (max-width: 600px) {
	  
				  .email-body_inner,
				  .email-footer {
					  width: 100% !important;
				  }
			  }
	  
			  @media only screen and (max-width: 500px) {
				  .button {
					  width: 100% !important;
				  }
			  }
		  </style>
	  </head>
	  
	  <body>
		  <table class="email-wrapper" width="100%" cellpadding="0" cellspacing="0">
			  <tr>
				  <td align="center">
					  <table class="email-content" width="100%" cellpadding="0" cellspacing="0">
						  <!-- Logo -->
						  <tr>
							  <td class="email-masthead">
								  <img class="email-masthead_name"
									  src="http://www.miraimpextrade.com/assets/images/logo_local_supplier.png" style="width: 170px;">
							  </td>
						  </tr>
						  <!-- Email Body -->
						  <tr>
							  <td class="email-body" width="100%">
								  <table class="email-body_inner" align="center" width="570" cellpadding="0" cellspacing="0">
									  <!-- Body content -->
									  <tr>
										  <td class="content-cell">
											  <h1>New Contact Enquiry</h1>
											  <!-- Action -->
											  <table class="body-action" align="center" width="100%" cellpadding="0"
												  cellspacing="0">
												  <tr>
													  <td align="center">
														  <div>
														  <p><span style="font-weight:bold;">Name: </span> '.$data["name"].'<br />
														  <span style="font-weight:bold;">Subject: </span> '.$data["subject"].'<br />
														  <span style="font-weight:bold;">Email: </span> '.$data["email"].'<br />
														  <span style="font-weight:bold;">Phone: </span> '.$data["phone"].'<br />
														  <span style="font-weight:bold;">Company: </span> '.$data["company"].'<br />
														  <span style="font-weight:bold;">Message: </span> '.$data["message"].'<br />
													  </p>
														  </div>
													  </td>
												  </tr>
											  </table>
											  <p>Thanks,<br>The Mira Impex Team</p>
										  </td>
									  </tr>
								  </table>
							  </td>
						  </tr>
						  <tr>
							  <td>
								  <table class="email-footer" align="center" width="570" cellpadding="0" cellspacing="0">
									  <tr>
										  <td class="content-cell">
											  <p class="sub center">
												  Mira Impex.
												  <br>NO 1 Gacem Road, Jimpex, Kanifing Industrial Area, PO Box 4298 Bakau
												  <br>The Gambia.
												  <br>+220 7117787
											  </p>
										  </td>
									  </tr>
								  </table>
							  </td>
						  </tr>
					  </table>
				  </td>
			  </tr>
		  </table>
	  </body>	  
	  </html>';

	  $this->email->from($from, $data["name"]);
	  $this->email->to('info@miraimpextrade.com', 'Mira Impex');
	  $this->email->subject('New Logistics Enquiry from '.$data["name"]);
	  $this->email->message($message);
	  $this->email->send();

		$response=["status"=>"1","message"=>"Thank you for your enquiry. We will get back to you soon."];
			
            }

		} catch (Exception $e) {
			$response=["status" => '0', "message" => '', "error" => $e];
		}
		echo json_encode($response);
	}

	public function safePay($value='') {
		$data['view_file'] = 'Safepay';
		customview($data);
		
	}

	public function forum($value='') {
		$data['view_file'] = 'Forum';
        customview($data);
	}

	public function enquiry($value='') {
		$data['view_file'] = 'Enquiry';
        view($data);
	}

	public function contact($value='') {
		$data['view_file'] = 'Contact_us';
        customview($data);
	}

	public function not_found($value='') {
		$data['view_file'] = 'Not_found';
        view($data);
	}
}