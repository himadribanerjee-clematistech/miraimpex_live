<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Contactsupplier_Model extends CI_Model{
    protected $itemTable = 'item';
	protected $localsupplierTable ='local_supplier';
    protected $customerTable = 'customer';
    protected $uomTable = 'uom';
    protected $categoryTable = 'category';
	public function __construct() {
        parent::__construct();
    }
    public function getItemByItemId($data) {
        try {
            
            $this->db->db_debug = true;
            $this->db->select('itm.id as item_id, itm.supp_id as seller_id, itm.local_sup_id, itm.item_name_en, itm.item_name_fr, itm.description_en, itm.description_fr, itm.category_id, itm.category_json, itm.price_min, itm.price_max, itm.currency, itm.lead_time, itm.moq, itm.attribute, itm.variance, itm.thumbnail, itm.packaging, itm.item_image,itm.moq_uom_id, itm.active, itm.is_approved,uom.name_en as uom_name_en,uom.name_fr as uom_name_fr,sp.business_name, sp.state, sp.country, sp.contact_name, cust.name,cust.phone,cust.email, cat.category_name_en,cat.category_name_fr,cat.type');
            $this->db->from($this->itemTable." itm");
            $this->db->join($this->customerTable." cust","cust.id=itm.supp_id");
            $this->db->join('t_seller_profile'." sp","sp.cust_id=itm.supp_id");
              $this->db->join($this->uomTable." uom","uom.id=itm.moq_uom_id");
              $this->db->join($this->categoryTable." cat","cat.id=itm.category_id");
              if(isset($data["id"]) && !empty($data["id"])){
                $this->db->where("itm.id",$data["id"]);
            }  
            
            $this->db->limit(1,0);
            
            $query = $this->db->get();

            $db_error = $this->db->error();

            if (!empty($db_error) && $db_error['code']>0) {
                throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
                return false; // unreachable retrun statement !!!
            }else{
               //echo $this->db->last_query();die();
                $itemdata = $query->result_array();
                
                // Variance table
                $this->db->db_debug = true;
                if($data["lang"]=='en')
                {
                    $this->db->select("v.id,v.name_en as name,v.type,v.status");
                }elseif($data["lang"]=='fr')
                {
                    $this->db->select("v.id,v.name_fr as name,v.type,v.status");
                }else{
                    $this->db->select("v.id,v.name_en as name,v.type,v.status");
                }
                    
                $this->db->from('variance v' );
                $this->db->where("v.status",1);
                $query=$this->db->get();
                $db_error = $this->db->error();
                $listVariance = $query->result_array();

                $results=[];
              foreach($itemdata as $val){
                $result=[];
                
                $result["item_id"]=base64_encode($val["item_id"]);
                 $result["seller_id"]=base64_encode($val["seller_id"]);
                  $result["local_sup_id"]=base64_encode($val["local_sup_id"]);
                  $result["category_id"]=base64_encode($val["category_id"]);
                   $result["uom_id"]=base64_encode($val["uom_id"]);
                  if(isset($val["category_json"])){
                    $val["category_json"]=json_decode($val["category_json"],true);
                    foreach($val["category_json"] as $val1){
                       $category_json[]=base64_encode($val1);
                    }
                    $result["category_id"]=json_encode($category_json);
                   }
                   
                  if($data["lang"]=='en'){
                     $result["item_name"]=$val["item_name_en"];
                      $result["description"]=$val["description_en"];
                      $result["lead_time"]=$val["lead_time_en"];
                      $result["uom_name"]=$val["uom_name_en"];
                      $result["terms"]=$val["terms_en"];
                     
                  }else{
                     $result["item_name"]=$val["item_name_fr"];
                     $result["description"]=$val["description_fr"];
                      $result["lead_time"]=$val["lead_time_fr"];
                      $result["uom_name"]=$val["uom_name_fr"];
                      $result["terms"]=$val["terms_fr"];
                     
                  }
                $result["price_min"]=$val["price_min"];
                $result["price_max"]=$val["price_max"];
                $result["moq"]=$val["moq"];
                $result["currency"]=$val["currency"];
                if($val["variance"] != '')
                {
                    $attribute_variance_arr = json_decode($val["variance"],true);
                    
                    if(is_array($attribute_variance_arr) && count($attribute_variance_arr)>0)
                    {
                        foreach($attribute_variance_arr as $hk=>$hv)
                        {
                            //$listVariance
                            
                            $varianceName = $listVariance[$hk]['name'];

                            foreach($hv as $hkk=>$hvv)
                            {
                                
                            foreach($hvv as $hkkk=>$hvvv)
                            {
                                $new_attribute_variance_arr[$varianceName][] = $hvvv;
                                
                            }
                                
                            }

                            
                        }
                    }
                }
                
                $result["variance"]=json_encode($new_attribute_variance_arr);



                if($val["attribute"] != '')
                {
                    $attribute_define_arr = json_decode($val["attribute"],true);
                    
                    if(is_array($attribute_define_arr) && count($attribute_define_arr)>0)
                    {
                        foreach($attribute_define_arr as $hk=>$hv)
                        {                            
                            
                            foreach($hv as $hkk=>$hvv)
                            {
                                
                                $new_attribute_define_arr[$hkk] = $hvv;
                                
                            }

                            
                        }
                    }
                }                
                $result["attribute"]=json_encode($new_attribute_define_arr);


                $result["thumbnail"]=$val["thumbnail"];
                $result["item_image"]=$val["item_image"];
                $result["active"]=$val["active"];
                $result["is_approved"]=$val["is_approved"];
                $result["category_name_en"]=$val["category_name_en"];
                $result["category_name_fr"]=$val["category_name_fr"];
                $result["type"]=$val["type"];
                $result["supplier_name"]=$val["name"];
                $result["supplier_phone"]=$val["phone"];
                $result["supplier_email"]=$val["email"];
                $result["supplier_business_name"]=$val["business_name"];
                $result["supplier_contact_name"]=$val["contact_name"];
                $result["supplier_state"]=$val["state"];
                $result["supplier_country"]=$val["country"];
                $results[]=$result;

              }

              return $results;


            }
         } catch (Exception $e) {
            throw new Exception();
            return;
        }    
    }

    public function getSellerById($data) {
        try {
            $this->db->db_debug = true;

                $this->db->select('sp.cust_id as seller_id, sp.business_name as supplier_business_name, sp.state as supplier_state, sp.country as supplier_country, sp.contact_name as supplier_contact_name');
                $this->db->from('t_seller_profile'." sp");
                  if(isset($data["id"]) && !empty($data["id"])){
                    $this->db->where("sp.cust_id",$data["id"]);
                }  
                
                $this->db->limit(1,0);
                
                $query = $this->db->get();
    
                $db_error = $this->db->error();
    
                if (!empty($db_error) && $db_error['code']>0) {
                    throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
                    return false; // unreachable retrun statement !!!
                }else{
                    $itemdata = $query->result_array();
                    
                    foreach($itemdata as $k=>$val){
                                        
                        $itemdata[$k]["seller_id"]=base64_encode($val["seller_id"]);
                    }
                }
    
                return $itemdata;
    
        }catch (Exception $e) {
                throw new Exception();
                return;
            } 
        }
}