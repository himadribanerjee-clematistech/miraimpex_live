<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contactsupplier extends CI_Controller {

	protected $token_id='';

    public function __construct() {
        parent::__construct(); 
        error_reporting('E_ALL^E_NOTICE');
    }


    public function index() {
        $this->load->helper('url');
        $currentURL = current_url();
        $params   = $_SERVER['QUERY_STRING'];
        parse_str($params, $params_array);
        
        $sellerId = $params_array['seller'];
        $itemId = base64_decode($params_array['item']);
        $data['lang'] = $this->session->userdata('lang');
        
        if($itemId>0)
        {
            $this->load->model("contactsupplier_model","contactsupplier");
            $itemdata= $this->contactsupplier->getItemByItemId(["id"=>$itemId,"lang"=>$data['lang']]);
        }else{
            $this->load->model("contactsupplier_model","contactsupplier");
            $itemdata= $this->contactsupplier->getSellerById(["id"=>base64_decode($sellerId),"lang"=>$data['lang']]);
        }
        
        // echo '<pre>';
        // print_r($itemdata);
        // die;
        $data['uriParams'] = $params_array;
        $data['itemdata'] = $itemdata[0];

        $data['view_file'] = 'Contactsupplier';
        view($data);
    } 

       

   


    
    

    
}