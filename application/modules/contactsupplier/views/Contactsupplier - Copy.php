<section class="contact_supplier">
	<div class="container">
		<div class="row">
			<div class="col-sm-10 col-sm-offset-1 group_contact">
				<form>
					<div class="panel-group" id="accordion">
						  <div class="panel panel-default">
						    <div class="panel-heading">
						      <h4 class="panel-title">
						        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
						          <i class="fa fa-user"></i>Felix Zhang Hexin Jucai (Wuhan) Digital Design And Production Co., Ltd 
						        </a>
						      </h4>
						    </div>
						    <div id="collapseOne" class="panel-collapse collapse in">
						      <div class="panel-body">
						      	   <div class="panel_info">
							          <table class="table">
									  <thead class="table-head">
									    <tr class="row">
									      <th class="cell">Product information</th>
									      <th class="cell">Quantity</th>
									      <th class="cell">Unit</th>
									    </tr>
									  </thead>
									  <tbody class="table-body">
									    <tr class="row">
									      <td class="cell">
									      	<img src="http://192.168.2.95/miraimpex/admin/uploads/category/1579850985.jpeg" alt="">
									      	<div><a href="#">China Manufacture Wholesale Mens Blank 100% cotton Short Sleeve tshirts High Quality Plain Custom Logo Printed Black t shirts</a></div>
									      </td>
									      <td class="cell"><input type="number" value="1"></td>
									      <td class="cell">
									      	<select>
									      		<option>Unit/Units</option>
									      		<option>Strand/Strands</option>
												<option>Ton/Tons</option>
												<option>Tonne/Tonnes</option>
												<option>Tray/Trays</option>
									      	</select>
									      </td>
									    </tr>
									    
									    
									  </tbody>
									  </table>
								   </div>
						      </div>
						    </div>
						  </div>
						  <div class="form_design">
						   	  <label><span>*</span> Please enter email address:</label>
						   	  <input type="mail" placeholder="Email" name="">
						   </div>
						  <div class="form_design">
						  	<label><span>*</span> Message: </label>
						  	<textarea placeholder="Message"></textarea>
						  </div>
						   <div class="form_design">
						   	<button type="button" class="btn_attachment"><i class="fa fa-plus"></i> Add attachment</button>
						   </div>
						   
						   <div class="form_design">
						   	<input class="btn btn_send_rfq" type="Submit" value="Send Inquiry Now" name="">
						   </div>
						  
					</div>
				</form>
			</div>
		</div>
	</div>
</section>

