<script src="<?php echo base_url();?>assets/custom_js/contactsupplier.js?version=<?php echo time(); ?>" type="text/javascript"></script>
<section class="contact_supplier">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 group_contact">
				<div class="product"><h4 class="heading"><span>Contact Supplier</span></h4></div>
				
				<form action="javascript:void(0);" name="contactSupplierForm" id="contactSupplierForm" enctype="multipart/form-data" onsubmit="javascript:sendQuotation();">
				<input type="hidden" name="itemAttribute" id="itemAttribute" value="">
				<input type="hidden" name="seller_item_id" id="seller_item_id" value="<?php echo $itemdata['item_id']; ?>">
				<input type="hidden" name="type" id="type" value="<?php echo $itemdata['type']; ?>">
				<input type="hidden" name="to_id" id="to_id" value="<?php echo $itemdata['seller_id']; ?>">
				<input type="hidden" name="from_id" id="from_id" value="">

					<div class="panel-group" id="accordion">
						  <div class="panel panel-default">
						    <div class="panel-heading">
						      <h4 class="panel-title">
						        <a href="<?php echo base_url();?>supplier/details/<?php echo $itemdata['seller_id']; ?>" title="View supplier details">
						          <i class="fa fa-user"></i><?php echo $itemdata['supplier_business_name']; ?>
						        </a>
						      </h4>
						    </div>
							<?php
							if($itemdata['item_id'] != '')
							{

							
							?>
						    <div id="collapseOne" class="panel-collapse collapse in">
						      <div class="panel-body">
						      	   <div class="panel_info">
							          <table class="table">
									  <thead class="table-head">
									    <tr class="row">
									      <th class="cell">Product information</th>
									      <th class="cell">MOQ</th>
									      <th class="cell">Unit</th>
									    </tr>
									  </thead>
									  <tbody class="table-body">
									    <tr class="row">
									      <td class="cell">
										  <?php
										  	
											  if(isset($itemdata["thumbnail"]))
											 {
												 
										?>
											<div class="cont_supp_img"><img src="<?php echo base_url();?>admin/uploads/selleritem/<?php echo $itemdata["thumbnail"]; ?>" alt="">	</div>			
										<?php
													 
											 }else{
											?>
											<div class="cont_supp_img">
											<img src="<?php echo base_url();?>admin/assets/images/no-item-image.png" alt=""></div>				
										<?php	 
											 } 
										  ?>
									      	
									      	<div class="all_cont_info">
									      		<div><a href="<?php echo base_url();?>product/details/<?php echo $itemdata['item_id']; ?>" title="View item details"><?php echo $itemdata['item_name']; ?></a></div>
									      		<?php
													if($itemdata['currency'] == 'Dollar')
													{
														$currencySymbol = '$';
													}elseif($itemdata['currency'] == 'Euro')
													{
														$currencySymbol = '€';
													}else{
														$currencySymbol = '$';
													}
												?> 

												<div>Price: <?php echo $currencySymbol.' '.$itemdata['price_min'].'-'.$itemdata['price_max']; ?></div>
									      	
											  
											<div>
											<?php
										  	if(isset($itemdata["variance"]) && $itemdata["variance"] != ''){
												  
												  function htmlcolorcode($color){
													  $colors = array("aliceblue"=>"#f0f8ff", "antiquewhite"=>"#faebd7", "aqua"=>"#00ffff", "aquamarine"=>"#7fffd4", "azure"=>"#f0ffff", "beige"=>"#f5f5dc", "bisque"=>"#ffe4c4", "black"=>"#000000", "blanchedalmond"=>"#ffebcd", "blue"=>"#0000ff", "blueviolet"=>"#8a2be2", "brown"=>"#a52a2a", "burlywood"=>"#deb887", "cadetblue"=>"#5f9ea0", "chartreuse"=>"#7fff00", "chocolate"=>"#d2691e", "coral"=>"#ff7f50", "cornflowerblue"=>"#6495ed", "cornsilk"=>"#fff8dc", "crimson"=>"#dc143c", "cyan"=>"#00ffff", "darkblue"=>"#00008b", "darkcyan"=>"#008b8b", "darkgoldenrod"=>"#b8860b", "darkgray"=>"#a9a9a9", "darkgreen"=>"#006400", "darkkhaki"=>"#bdb76b", "darkmagenta"=>"#8b008b", "darkolivegreen"=>"#556b2f", "darkorange"=>"#ff8c00", "darkorchid"=>"#9932cc", "darkred"=>"#8b0000", "darksalmon"=>"#e9967a", "darkseagreen"=>"#8fbc8f", "darkslateblue"=>"#483d8b", "darkslategray"=>"#2f4f4f", "darkturquoise"=>"#00ced1", "darkviolet"=>"#9400d3", "deeppink"=>"#ff1493", "deepskyblue"=>"#00bfff", "dimgray"=>"#696969", "dodgerblue"=>"#1e90ff", "firebrick"=>"#b22222", "floralwhite"=>"#fffaf0", "forestgreen"=>"#228b22", "fuchsia"=>"#ff00ff", "gainsboro"=>"#dcdcdc", "ghostwhite"=>"#f8f8ff", "gold"=>"#ffd700", "goldenrod"=>"#daa520", "gray"=>"#808080", "green"=>"#008000", "greenyellow"=>"#adff2f",
													  "honeydew"=>"#f0fff0", 
													  "hotpink"=>"#ff69b4",
													   "indianred"=>"#cd5c5c", "indigo"=>"#4b0082", "ivory"=>"#fffff0", "khaki"=>"#f0e68c", "lavender"=>"#e6e6fa", "lavenderblush"=>"#fff0f5", "lawngreen"=>"#7cfc00", "lemonchiffon"=>"#fffacd", "lightblue"=>"#add8e6", "lightcoral"=>"#f08080", "lightcyan"=>"#e0ffff", "lightgoldenrodyellow"=>"#fafad2", "lightgrey"=>"#d3d3d3", "lightgreen"=>"#90ee90", "lightpink"=>"#ffb6c1", "lightsalmon"=>"#ffa07a", "lightseagreen"=>"#20b2aa", "lightskyblue"=>"#87cefa", "lightslategray"=>"#778899", "lightsteelblue"=>"#b0c4de", "lightyellow"=>"#ffffe0", "lime"=>"#00ff00", "limegreen"=>"#32cd32", "linen"=>"#faf0e6", "magenta"=>"#ff00ff", "maroon"=>"#800000", "mediumaquamarine"=>"#66cdaa", "mediumblue"=>"#0000cd", "mediumorchid"=>"#ba55d3", "mediumpurple"=>"#9370d8", "mediumseagreen"=>"#3cb371", "mediumslateblue"=>"#7b68ee", "mediumspringgreen"=>"#00fa9a", "mediumturquoise"=>"#48d1cc", "mediumvioletred"=>"#c71585", "midnightblue"=>"#191970", "mintcream"=>"#f5fffa", "mistyrose"=>"#ffe4e1", "moccasin"=>"#ffe4b5", "navajowhite"=>"#ffdead", "navy"=>"#000080", "oldlace"=>"#fdf5e6", "olive"=>"#808000", "olivedrab"=>"#6b8e23", "orange"=>"#ffa500", "orangered"=>"#ff4500", "orchid"=>"#da70d6", "palegoldenrod"=>"#eee8aa",
													  "palegreen"=>"#98fb98", "paleturquoise"=>"#afeeee", "palevioletred"=>"#d87093", "papayawhip"=>"#ffefd5", "peachpuff"=>"#ffdab9", "peru"=>"#cd853f", "pink"=>"#ffc0cb", "plum"=>"#dda0dd", "powderblue"=>"#b0e0e6", "purple"=>"#800080", "rebeccapurple"=>"#663399", "red"=>"#ff0000", "rosybrown"=>"#bc8f8f", "royalblue"=>"#4169e1", "saddlebrown"=>"#8b4513", "salmon"=>"#fa8072", "sandybrown"=>"#f4a460", "seagreen"=>"#2e8b57", "seashell"=>"#fff5ee", "sienna"=>"#a0522d", "silver"=>"#c0c0c0", "skyblue"=>"#87ceeb", "slateblue"=>"#6a5acd", "slategray"=>"#708090", "snow"=>"#fffafa", "springgreen"=>"#00ff7f", "steelblue"=>"#4682b4", "tan"=>"#d2b48c", "teal"=>"#008080", "thistle"=>"#d8bfd8", "tomato"=>"#ff6347", "turquoise"=>"#40e0d0", "violet"=>"#ee82ee", "wheat"=>"#f5deb3", "white"=>"#ffffff", "whitesmoke"=>"#f5f5f5", "yellow"=>"#ffff00", "yellowgreen"=>"#9acd32");

													  if($colors[strtolower($color)] != ''){
														  return $colors[strtolower($color)];
													  }else{
														  return false;
													  }
												  }


												$itemdata["variance"]=json_decode($itemdata["variance"],true);
												?>
												<div class="product-info viewCate_attri"><div class="attribute clearfix">
												<?php
													foreach($itemdata['variance'] as $k=>$v)
													{
												?>
													<div><fieldset class="attribute_fieldset"><label class="attribute_label"><?php echo $k; ?></label><div class="attribute_list"><ul class="attribute_size">
													<?php
														foreach($v as $vv)
														{

															$hexcolorcode = htmlcolorcode($vv);

															if($hexcolorcode)
															{
														?>
															<li class="variance" style="background-color:<?php echo $hexcolorcode; ?>; width:25px; height:25px;"><a href="javascript:void(0);" title="<?php echo $vv; ?>" relparent="<?php echo $k; ?>" relval="<?php echo $vv; ?>"></a></li>
														<?php		

															}else{
														?>
															<li class="variance"><a href="javascript:void(0);" relparent="<?php echo $k; ?>" relval="<?php echo $vv; ?>"><?php echo $vv; ?></a></li>
														<?php		
															}
													?>
															
													<?php		
														}
													?>
													</ul></div></fieldset></div>
												<?php	   
													}
												?>
												</div></div>					
												<?php												

											   }
											?>
											</div>
										</div>
									      </td>
									      <td class="cell"><input type="number" name="quantity" id="quantity" value="<?php echo $itemdata['moq']; ?>" disabled readonly="true"></td>
									      <td class="cell">
									      	<select name="uom_id" id="uom_id">
									      		<option value="<?php echo $itemdata['uom_id']; ?>"><?php echo $itemdata['uom_name']; ?></option>
									      	</select>
									      </td>
									    </tr>
									    
									    
									  </tbody>
									  </table>
								   </div>
						      </div>
						    </div>
							<?php
							}
							?>
						  </div>
						  <div class="form_design">
						   	  <label> Registered email address:</label>
						   	  <input type="mail" placeholder="Email" name="from_email" id="from_email" disabled readonly="true">
						   </div>
						  <div class="form_design">
						  	<label><span>*</span> Message: </label>
							  <textarea class="form-control" id="message" name="message" rows="4" onkeyup="javascript:countChar(this);"></textarea>
                                    <span id="charNum" class="blue">Your message must be between 20-8000 characters</span>
						  </div>
						  <div class="form_design">
						   	  <label>Upload attachments:</label>
						   	  <input type="file" name="image[]" id="enq-image-add" accept="image/x-png,image/gif,image/jpeg">
						   </div>
						   <div class="form_design">	
						   <div id="rfqimgdv" class="gallery"></div>					   
						   </div>
						   <div class="form_design">
						   <label for="" class="c_box"><input type="checkbox" name="" id="check2"> I agree to share my Business Card to the supplier.</label>
						   </div>
						   
						   <div class="form_design">
						   	<input class="btn btn_send_rfq" type="Submit" value="Send Inquiry Now" name="">
						   </div>
						  
					</div>
				</form>
			</div>
		</div>
	</div>
</section>