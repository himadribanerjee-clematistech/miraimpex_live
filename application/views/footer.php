<div class="wrapper-footer clearfix">
            	<div class="container">
                	<div class="row">
                    	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-2 es-link">
                          <h4>ABOUT US</h4>
                          <ul class="clearfix">
                              <li><a href="#">About Our Company</a></li>
                                <li><a href="#">Jobs & Careers</a></li>
                                <li><a href="#">Success Stories</a></li>
                                <li><a href="#">Contact Us</a></li>
                                <li><a href="#">partner With Us</a></li>
                            </ul>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-2 es-link">
                        	<h4>OUT SERVICES</h4>
                        	<ul class="clearfix">
                            	<li><a href="#">Advertise With Us</a></li>
                                <li><a href="#">Book Domains</a></li>
                                <li><a href="#">Weekly Newsletter</a></li>
                                <li><a href="#">Order Credit Report</a></li>
                                <li><a href="#">MIPay</a></li>
                            </ul>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-2 es-link">
                        	<h4>BUYERS</h4>
                        	<ul class="clearfix">
                            	<li><a href="#">Browse Suppliers</a></li>
                                <li><a href="#">Post Your Requirement</a></li>
                                <li><a href="#">Subscribe sell Miraimpex Alerts</a></li>
                            </ul>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-2 es-link">
                          <h4>SELLERS</h4>
                          <ul class="clearfix">
                              <li><a href="#">Display New products</a></li>
                                <li><a href="#">Get Freight Quotes</a></li>
                                <li><a href="#">Get Packers & Movers</a></li>
                                <li><a href="#">Subscribe Buy Trade Alerts</a></li>
                                <li><a href="#">Buy Miraimpex Leads</a></li>
                            </ul>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-2 es-link">
                          <h4>DIRECTORY</h4>
                          <ul class="clearfix">
                              <li><a href="#">Manufacturers</a></li>
                                <li><a href="#">Business Services</a></li>
                                <li><a href="#">Services Providers</a></li>
                                <li><a href="#">China Suppliers</a></li>
                                <li><a href="#">Featured Products</a></li>
                            </ul>
                        </div>
                    </div>
                    
                    <div class="spacer15"></div><!-- Spacer -->
                    <div class="newsletter-main">
                        <div class="row">
                            <div class=" col-sm-6 sl-block">
                                <h4>Follow us</h4>
                                <div>
                                    <a href="#"><img src="<?php echo base_url();?>assets/images/i-1.png" alt=""/></a>
                                    <a href="#"><img src="<?php echo base_url();?>assets/images/i-2.png" alt=""/></a>
                                    <a href="#"><img src="<?php echo base_url();?>assets/images/i-3.png" alt=""/></a>
                                    <a href="#"><img src="<?php echo base_url();?>assets/images/i-4.png" alt=""/></a>
                                    <a href="#"><img src="<?php echo base_url();?>assets/images/i-5.png" alt=""/></a>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="newsletter download">
                                    <a href="#">
                                      <img src="<?php echo base_url();?>assets/images/download1.jpg" alt="">
                                      <img src="<?php echo base_url();?>assets/images/download2.jpg" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="wrapper-copy">
                    <div class="container">
                        <div class="copy">
                        	<div class="row">
                                <div class="col-sm-12 text-cenrter">
                                    <p>© 2020 mira impex. All Rights Reserved. Designed by <a href="#">clematistech.com</a></p>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-------------- End footer -------------->
        <!-- </div> -->
        <!-------------- End wrapper main -------------->
    </div>
    <!-------------- End wrapper -------------->
    <input type="hidden" name="viewLang" id="viewLang" value="<?php echo $this->session->userdata('lang'); ?>">
    
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCCWnp0Wk-h48SAo3NepYcz_GLjEPSTS2M&libraries=places"></script>
<script>

            
var autocomplete;

var autocomplete1;
var autocomplete2;

function buyerprofilebuyerinitializeGoogleAddress() {
  autocomplete1 = new google.maps.places.Autocomplete(
    (document.getElementById('upd_buyerAddress')), {
    types: ['geocode']
  }
  );
  // When the user selects an address from the dropdown, populate the address
  // fields in the form.
  autocomplete1.addListener('place_changed', buyerprofilebuyerfillInAddress);
}

function buyerprofilebuyerfillInAddress() {
  // Get the place details from the autocomplete object.
  
  //$("#buyerSubBut").addClass('d-none');
  //$('#waitMsg').html('Fetching data of this address. Please wait.');
  var place = autocomplete1.getPlace();
  var latitute = place.geometry.location.lat();
  var longitute = place.geometry.location.lng();
  document.getElementById('buyerprofilebuyeraddLat').value = latitute;
  document.getElementById('buyerprofilebuyeraddLang').value = longitute;


}


function buyerprofilesellerinitializeGoogleAddress() {
  autocomplete2 = new google.maps.places.Autocomplete(
    (document.getElementById('upd_sellerAddress')), {
    types: ['geocode']
  }
  );
  // When the user selects an address from the dropdown, populate the address
  // fields in the form.
  autocomplete2.addListener('place_changed', buyerprofilesellerfillInAddress);
}

function buyerprofilesellerfillInAddress() {
  // Get the place details from the autocomplete object.
  var place = autocomplete2.getPlace();
  var latitute = place.geometry.location.lat();
  var longitute = place.geometry.location.lng();
  document.getElementById('sellerprofileselleraddLat').value = latitute;
  document.getElementById('sellerprofileselleraddLang').value = longitute;
  //console.log(latitute+'='+longitute);
}


function initializeRfqLocationGoogleAddress() {
    autocomplete = new google.maps.places.Autocomplete(
      (document.getElementById('rfqLocationAddress')), {
          types: ['geocode']
      }
  );
  // When the user selects an address from the dropdown, populate the address
  // fields in the form.
  autocomplete.addListener('place_changed', fillInAddress);
}

function fillInAddress() {
  // Get the place details from the autocomplete object.
  var place = autocomplete.getPlace();
  var latitute = place.geometry.location.lat();
  var longitute = place.geometry.location.lng();
  document.getElementById('rfqLocationLat').value = latitute;
  document.getElementById('rfqLocationLang').value = longitute;
  //console.log(latitute+'='+longitute);
}

</script>





