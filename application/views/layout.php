<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html dir="ltr" lang="en-US">

<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="EShop" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<title>Mira Impex: Enabling trade with and within Africa</title>
    <link rel="icon" href="<?php echo base_url();?>assets/images/faveicon.png" sizes="20x20" type="image/png">
    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url();?>assets/css/owl.carousel.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url();?>assets/css/overright.css" rel="stylesheet" type="text/css"/>
    
    <link href="<?php echo base_url();?>assets/css/colors.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url();?>assets/css/responsive.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url();?>assets/css/toastr.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>assets/css/custom.css" rel="stylesheet" type="text/css"/>


    <!-------------- Java script -------------->
    <script src="<?php echo base_url();?>assets/js/jquery.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/js/owl.carousel.js" type="text/javascript"></script>  
    <script src="<?php echo base_url();?>assets/js/toastr.min.js" type="text/javascript"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
    <script src="<?php echo base_url();?>assets/custom_js/common.js?version=<?php echo time(); ?>" type="text/javascript"></script>
</head>
<body class="blue">
    <a id="top-to-bottom"></a>
	
	<div class="loading hide "><img src="<?php echo base_url();?>assets/images/lg.ring-loading-gif.gif" alt=""></div>
	
	<!-- header started -->
  	<?php  echo (isset($header))?$header:'';?>
  	<!-- header ended -->


  	<?php  echo (isset($content))?$content:'';?>  


	<!--   footer start -->
	<?php  echo (isset($footer))?$footer:'';?>
	<!-- footer end -->

    <input type="hidden" name="base_url" id="base_url" value="<?php echo base_url(); ?>">
    <input type="hidden" name="tracelogUrl" id="tracelogUrl" value="<?php if(isset($redirect)) echo $redirect; ?>">
</body>

</html>
