<style type="text/css">
.newCartItemTotal {
    position: absolute;
    top: 12px;
    right: 3px;
    border: 1px solid #fff;
    height: 20px;
    width: 20px;
    padding: 2px 0px 0 0px;
    text-align: center;
    font-size: 11px;
    color: #fff;
    background-color: green;
    border-radius: 50%;
    line-height: 15px;
}
</style>
<!--------------Start wrapper-------------->
  <div id="wrapper" class="clearfix">
      <!--------------Top bar-------------->
      <div id="top-bar">
          
            <div class="container clearfix">
              <!--Social icons-->
              <div class="top-social-icons">
                  <ul class="clearfix">
                      <!-- <li><a class="i-facbook">Registered Users : 52,67,440 </a></li> -->
                        
                    </ul>
                </div>
                <!--End social icon-->
                
                <!-- Top link -->
                <div class="top-links clearfix fright">
                  <ul>
                    <li class="dropdown">
                      <a href="#">FOR SERVICES PROVIDERS <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                      <ul class="dropdown-menu">
                      <li><a href="<?php echo base_url();?>seller/item/addItem">Enlist New Service</a></li>
                      </ul>
                    </li>
                    <li class="dropdown">
                      <a href="#">FOR BUYERS <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                      <ul class="dropdown-menu">
                          <li><a class="linkrfq" href="<?php echo base_url();?>requestforquotations">Post Buy Requirement</a></li>
                          <li><a href="<?php echo base_url();?>category/all">Search Suppliers</a></li>
                      </ul>
                    </li>

                    <li class="dropdown">
                      <a href="#"> FOR SUPPLIER <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                      <ul class="dropdown-menu">
                          <li><a href="<?php echo base_url();?>seller/item/addItem">Display New Products</a></li>
                          <li><a href="<?php echo base_url();?>pages/logistics">MI Logistics</a></li>
                          <li><a href="<?php echo base_url();?>pages/safePay">MI SafePay</a></li>
                          <li><a href="<?php echo base_url();?>localsupplier">Local Suppliers</a></li>
                      </ul>
                    </li>
                    <li class="dropdown">
                      <a href="#">FORUM DIRECTORY <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                      <ul class="dropdown-menu">
                          <li><a href="<?php echo base_url();?>pages/forum">Directory</a></li>
                      </ul>
                    </li>
                        <li class="dropdown">
                            <?php
                              $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                              if(strpos($actual_link,'?')>0)
                              $actual_link.='&';
                              else
                              $actual_link.='?';	
                              
                              $actual_link=str_replace(array('lang=en','lang=fr'),"",$actual_link);
                              $actual_link=trim($actual_link,'&');
                              if($this->session->userdata('lang')=='en'){	
                              ?>	
                                          <a href="<?=$actual_link;?>&lang=en">English<img src="<?php echo base_url();?>assets/images/flags/england.jpg" alt="England"></a>
                              <?php }
                              else if($this->session->userdata('lang')=='fr'){
                              ?>	
                              <a href="<?=$actual_link;?>&lang=fr">French<img src="<?php echo base_url();?>assets/images/flags/france.jpg" alt="France"></a>
                              <?php
                              }?>
                            
                            <ul class="dropdown-menu">
                              <li>
                                <?php	  
                                if($this->session->userdata('lang')=='fr'){	
                                  ?>	
                                  <a href="<?=$actual_link;?>&lang=en">English<img src="<?php echo base_url();?>assets/images/flags/england.jpg" alt="England"></a>
                                  <?php }
                                  else if($this->session->userdata('lang')=='en'){
                                  ?>	
                                  <a href="<?=$actual_link;?>&lang=fr">French<img src="<?php echo base_url();?>assets/images/flags/france.jpg" alt="France"></a>
                                  <?php
                                }?>	 
							                </li>
                            </ul>
                          </li>
                        <li class="dropdown">
                          <a class="dropdown-toggle signInButtonLink" data-toggle="" href="<?php echo base_url();?>login" id="signInButtonLink"><i class="fa fa-user"></i><span id="unamepanel" class="unamepanel">Sign in</span></a>
                          <ul class="dropdown-menu Account_ul_sec" id="loggedInUserDD" style="display:none">
                              <li><img id="profileImgPanel" class="user_img" src="<?php echo base_url();?>assets/images/user_img.jpg" alt=""><span class="welcome">Welcome To Miraimpex</span></li>
                              <li><a href="<?php echo base_url();?>profile">My Profile</a></li>
                              <li><a href="<?php echo base_url();?>profile/myrfqs">My RFQs</a></li>
                              <li><a href="<?php echo base_url();?>profile/myenquiries">My Enquiries</a></li>
                              <li><a href="<?php echo base_url();?>profile/notifications">Notifications</a></li>
                              <li><a href="<?php echo base_url();?>messagecenter">Message Center</a></li>
                                <!-- <li><a href="#">My Wishlist</a></li>
                                <li><a href="#">Checkout</a></li> -->
                                <li><a rel="<?php echo $_SERVER['REQUEST_URI']; ?>"  href="javascript:void(0);" onclick="javascript:logoutCustomer(this);" >Logout</a></li>
                                
                               
                            </ul>
                        </li>
                        <li class="dropdown">
                          <a href="#"><i class="fa fa-shopping-cart"></i>Cart <span class="newCartItemTotal d-none" id="newCartItemTotal">0</span> <i class="fa fa-angle-down" aria-hidden="true"></i></a>                          
                          <ul class="dropdown-menu help_ul shopping-cart-items floating-cart" id="" style="width:300px;">
                          <div class="item-list crttt" id="addtocartUL">

                          </div>
                          <li style="margin-top: 10px;" id="proceedCheckoutButton" class="chckout">                                     
                              
                          </li>
                        </ul>
                        
                        </li>
                    </ul> 
                </div>
                <!-- End top link -->
                  
            </div>
            
        </div>
        <!--------------End top bar-------------->

                <!-- top navbar for mobile -->

<nav class="navbar navbar-default navbar-for-mobile">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    <div class="top-links clearfix fright">
                  <ul>
                    <li class="dropdown">
                      <a href="#">FOR SERVICES PROVIDERS <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                      <ul class="dropdown-menu">
                      <li><a href="<?php echo base_url();?>seller/item/addItem">Enlist New Service</a></li>
                      </ul>
                    </li>
                    <li class="dropdown">
                      <a href="#">FOR BUYERS <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                      <ul class="dropdown-menu">
                          <li><a class="linkrfq" href="<?php echo base_url();?>requestforquotations">Post Buy Requirement</a></li>
                          <li><a href="<?php echo base_url();?>category/all">Search Suppliers</a></li>
                      </ul>
                    </li>

                    <li class="dropdown">
                      <a href="#"> FOR SUPPLIER <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                      <ul class="dropdown-menu">
                          <li><a href="<?php echo base_url();?>seller/item/addItem">Display New Products</a></li>
                          <li><a href="<?php echo base_url();?>pages/logistics">MI Logistics</a></li>
                          <li><a href="<?php echo base_url();?>pages/safePay">MI SafePay</a></li>
                          <li><a href="<?php echo base_url();?>localsupplier">Local Suppliers</a></li>
                      </ul>
                    </li>
                    <li class="dropdown">
                      <a href="#">FORUM DIRECTORY <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                      <ul class="dropdown-menu">
                          <li><a href="<?php echo base_url();?>pages/forum">Directory</a></li>
                      </ul>
                    </li>
                        <li class="dropdown">
                            <?php
                              $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                              if(strpos($actual_link,'?')>0)
                              $actual_link.='&';
                              else
                              $actual_link.='?';  
                              
                              $actual_link=str_replace(array('lang=en','lang=fr'),"",$actual_link);
                              $actual_link=trim($actual_link,'&');
                              if($this->session->userdata('lang')=='en'){ 
                              ?>  
                                          <a href="<?=$actual_link;?>&lang=en">English<img src="<?php echo base_url();?>assets/images/flags/england.jpg" alt="England"></a>
                              <?php }
                              else if($this->session->userdata('lang')=='fr'){
                              ?>  
                              <a href="<?=$actual_link;?>&lang=fr">French<img src="<?php echo base_url();?>assets/images/flags/france.jpg" alt="France"></a>
                              <?php
                              }?>
                            
                            <ul class="dropdown-menu">
                              <li>
                                <?php   
                                if($this->session->userdata('lang')=='fr'){ 
                                  ?>  
                                  <a href="<?=$actual_link;?>&lang=en">English<img src="<?php echo base_url();?>assets/images/flags/england.jpg" alt="England"></a>
                                  <?php }
                                  else if($this->session->userdata('lang')=='en'){
                                  ?>  
                                  <a href="<?=$actual_link;?>&lang=fr">French<img src="<?php echo base_url();?>assets/images/flags/france.jpg" alt="France"></a>
                                  <?php
                                }?>  
                              </li>
                            </ul>
                          </li>
                        <li class="dropdown">
                          <a class="dropdown-toggle signInButtonLink" data-toggle="" href="<?php echo base_url();?>login" id="signInButtonLink"><i class="fa fa-user"></i><span id="unamepanel" class="unamepanel">Sign in</span></a>
                          <ul class="dropdown-menu Account_ul_sec" id="loggedInUserDD" style="display:none">
                              <li><img id="profileImgPanel" class="user_img" src="<?php echo base_url();?>assets/images/user_img.jpg" alt=""><span class="welcome">Welcome To Miraimpex</span></li>
                              <li><a href="<?php echo base_url();?>profile">My Profile</a></li>
                              <li><a href="<?php echo base_url();?>profile/myrfqs">My RFQs</a></li>
                              <li><a href="<?php echo base_url();?>profile/myenquiries">My Enquiries</a></li>
                              <li><a href="<?php echo base_url();?>profile/notifications">Notifications</a></li>
                              <li><a href="<?php echo base_url();?>messagecenter">Message Center</a></li>
                                <!-- <li><a href="#">My Wishlist</a></li>
                                <li><a href="#">Checkout</a></li> -->
                                <li><a rel="<?php echo $_SERVER['REQUEST_URI']; ?>"  href="javascript:void(0);" onclick="javascript:logoutCustomer(this);" >Logout</a></li>
                                
                               
                            </ul>
                        </li>
                        <li class="dropdown">
                          <a href="#" class="crt"><i class="fa fa-shopping-cart"></i>Cart <span class="newCartItemTotal d-none" id="newCartItemTotal">0</span> <i class="fa fa-angle-down" aria-hidden="true"></i></a>                          
                          <ul class="dropdown-menu help_ul shopping-cart-items floating-cart" id="" style="width:300px;">
                          <div class="item-list crttt" id="addtocartUL">

                          </div>
                          <li style="margin-top: 10px;" id="proceedCheckoutButton" class="chckout">                                     
                              
                          </li>
                        </ul>
                        
                        </li>
                    </ul> 
                </div>
                <!-- End top link -->
    </div>
    <!-- /.navbar-collapse -->
  </div>
  <!-- /.container-fluid -->
</nav>
        <!--------------Header-------------->
        <header id="header">
            <div class="header-wrapper">
              <div class="container clearfix">
                  <!-- logo -->
                  <div class="logo">
                      <div class="clearfix">
                          <a href="<?php echo base_url();?>">
                              <img src="<?php echo base_url();?>assets/images/logo.png" alt="logo"/>
                            </a>
                           
                        </div>
                        <!--  <p class="logo_dwn">Enabling trade with and within Africa</p> -->
                    </div>
                    <!--End logo -->
                        <div class="right-wrapper">
                          <!--<div class="wb-block">
                              <span>Welcome to EShop</span>
                            </div>-->
                          <div class="search"><!--Search-->
                                <form action="" class="search-form" id="searchForm">
                                    <select class="select_pro_supp" name="type" id="type">
                                      <option value="1">Products</option>
                                      <option value="2">Suppliers</option>
                                    </select>
                                    <input type="search" name="searchq" id="searchq" class="s" placeholder="Search entry site here...">
                                    <button type="button"> <i class="fa fa-search"></i></button>


                                    <div class="search_list_view hide">

                                      <div class="list_search"><a href="#">
                                          <div class="search_img default">
                                            <img src="<?php echo base_url();?>assets/images/all_icon/search.png" alt="search icon"/></div>
                                          <div class="search_cnt">
                                            <h5>Tshirts men</h5>
                                            <h5>in Men's T-shirts</h5>
                                          </div></a>
                                      </div>

                                      <div class="list_search"><a href="#">
                                          <div class="search_img"><img src="<?php echo base_url();?>assets/images/product/product5.png" alt="search icon"/></div>
                                          <div class="search_cnt">
                                            <h5>Tshirts men</h5>
                                            <h5>in Men's T-shirts</h5>
                                          </div></a>
                                      </div>

                                      <div class="list_search"><a href="#">
                                          <div class="search_img default"><img src="<?php echo base_url();?>assets/images/all_icon/search.png" alt="search icon"/></div>
                                          <div class="search_cnt">
                                            <h5>Tshirts men</h5>
                                            <h5>in Men's T-shirts</h5>
                                          </div></a>
                                      </div>
                                      
                                    </div>  




                                </form>
                            </div><!--End Search-->
                            <div class="wb-block">
                            <a class="linkrfq" href="<?php echo base_url();?>requestforquotations"><button type="button" class="Requirement">Post Buy Requirement</button></a>
                            </div>
                            <div class="cart download"><!--Start Cart-->
                            <a href="">
                              <img src="<?php echo base_url();?>assets/images/download1.png" alt="">
                              <img src="<?php echo base_url();?>assets/images/download2.png" alt="">
                              <p>Download App</p>
                            </a>
                            </div><!--End Cart-->
                        </div>
                    <p class="logo_dwn">Enabling trade with and within Africa</p>
                </div>
            </div>
            
        </header>
        <!--------------End header-------------->

        <script type="text/javascript">
          
          $(document).ready(function(){
            $('#searchq').keyup(function(){
              if ($('#searchq').val() > 3) {

              }
              else {
                
              }
            });

            $('.crt').click(function(e){
              e.preventDefault();
            });
          });

        </script>