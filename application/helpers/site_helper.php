<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}


function  languageConvertor($str,$lan){
	
$url="https://translate.yandex.net/api/v1.5/tr.json/translate";
$key="trnsl.1.1.20200128T142649Z.59236dc315d7fc28.9a6aa8e0e2a0a118247a5b3d53ebf69c29ef8906";	

$params = array(
   "key" => $key,
   "text" => $str,
   "lang" =>$lan  //lang : fr en 
);	

$translateStr='';	
$output=httpPost($url,$params);	
$output=(array)json_decode($output, false);
if($output['code']=='200'){
$translateStr=$output['text'][0];	
}
  return $translateStr;
  
}


function httpPost($url,$params)
{
  $postData = '';
   //create name value pairs seperated by &
   foreach($params as $k => $v) 
   { 
      $postData .= $k . '='.$v.'&'; 
   }
   $postData = rtrim($postData, '&');
 
    $ch = curl_init();  
 
    curl_setopt($ch,CURLOPT_URL,$url);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
    curl_setopt($ch,CURLOPT_HEADER, false); 
    curl_setopt($ch, CURLOPT_POST, count($postData));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);    
 
    $output=curl_exec($ch);
 
    curl_close($ch);
    return $output;
 
}

function escapeString($val) {
    $db = get_instance()->db->conn_id; 
    $val = mysqli_real_escape_string($db, $val);
    return $val;
}