<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LanguageLoader
    {
	
	   private $CI;
		public function __construct()
		{
			$this->CI =& get_instance();

		}
	
	
       function initialize() {


		    if(!$this->CI->session->userdata('lang'))
			   $this->CI->session->set_userdata('lang','en');	
		   
			if(isset($_GET['lang']))
			  $this->CI->session->set_userdata('lang',$_GET['lang']);	
            
		   	  $this->CI->lang->load('site', $this->CI->session->userdata('lang')); 
		   
       
       }
    }
?>