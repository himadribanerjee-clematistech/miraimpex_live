/*function loadFile(){
                                
    var imgfilename = document.getElementById('chImg');

    imgfilename.src = URL.createObjectURL(event.target.files[0]);
};*/

function addCustomer() {
	var base_url = $("#base_url").val();
	var pattern = /^-?([1-8]?[1-9]|[1-9]0)\.{1}\d{1,6}$/i;

	var name = $("#name").val();
	var username = $("#username").val();
	var email = $("#email").val();
	var phone = $("#phone").val();
	var business_name = $("#business_name").val();
	var address = $("#address").val();
	var state = $("#state").val();
	var city = $("#city").val();
	var lat = $("#lat").val();
	var long = $("#long").val();
	var business_logo = $("#business_logo").val();
	//var agent_YN = $('#agent_YN').val();

	toastr.clear();

	if ($.trim(name) == '') {
		toastr.error('Please Enter Customer Name.');
		$("#name").focus();
		return false;
	}
	else if ($.trim(username) == '') {
		toastr.error('Please Enter Customer User Name.');
		$("#username").focus();
		return false;
	}
	else if ($.trim(email) == '') {
		toastr.error('Please Enter Customer Email Address.');
		$("#email").focus();
		return false;
	}
	else if ($.trim(phone) == '') {
		toastr.error('Please Enter Customer Contact.');
		$("#phone").focus();
		return false;
	}
	else if ($.trim(business_name) == '') {
		toastr.error('Please Enter Bussiness Name.');
		$("#business_name").focus();
		return false;
	}
	else if ($.trim(address) == '') {
		toastr.error('Please Enter Address.');
		$("#address").focus();
		return false;
	}
	else if ($.trim(state) == '') {
		toastr.error('Please Enter State.');
		$("#state").focus();
		return false;
	}
	else if ($.trim(city) == '') {
		toastr.error('Please Enter City.');
		$("#city").focus();
		return false;
	}
	else if ($.trim(lat) == '') {
		toastr.error('Please Enter Latitude.');
		$("#lat").focus();
		return false;
	}
	else if (!pattern.test(lat)) {
		toastr.error('Please Enter Valid Latitude.');
		$("#lat").focus();
		return false;
	}
	else if ($.trim(long) == '') {
		toastr.error('Please Enter Longitude.');
		$("#long").focus();
		return false;
	}
	else if (!pattern.test(long)) {
		toastr.error('Please Enter Valid Longitude.');
		$("#long").focus();
		return false;
	}
	else if ($.trim(business_logo) == '') {
		toastr.error('Please Select Logo.');
		$("#business_logo").focus();
		return false;
	}
	else {

		var formData = new FormData();
		formData.append('business_logo', $('#business_logo')[0].files[0]);
		formData.append('business_name', business_name);
		formData.append('address', address);
		formData.append('state', state);
		formData.append('city', city);
		formData.append('lat', lat);
		formData.append('long', long);

		formData.append('name', name);
		formData.append('username', username);
		formData.append('email', email);
		formData.append('phone', phone);
		//formData.append('agent_YN', agent_YN);


		$('.loading').removeClass("hidden");
		$('#add').attr("disabled", true);
		$.ajax({
			url: base_url + "customer/doAddCust",
			type: "post",
			data: formData,
			processData: false,  // tell jQuery not to process the data
			contentType: false,
			success: function (result) {
				$('.loading').addClass("hidden");
				result = JSON.parse(result);
				if (result.status == 0) {
					toastr.success(result["message"]);
					$('#add').attr("disabled", false);
					setTimeout(
						function () {
							window.location = base_url + 'customer';

						}, 1000
					);
				} else {
					toastr.error(result["message"]);
					$("#" + result.field).focus();
				}
			}
		});
	}
}


function doResendPass(id) {
	var base_url = $("#base_url").val();

	$.ajax({
		url: base_url + "customer/doResendPass",
		data: { id: id },
		method: "post",
		success: function (result) {
			$('.loading').addClass("hidden");
			result = JSON.parse(result);
			if (result.status == 0) {
				toastr.success(result["message"]);
				setTimeout(
					function () {
						window.location = base_url + 'customer';

					}, 1000
				);
			} else {
				toastr.error(result["message"]);
			}
		}
	});
}


function doCustEdit(id) {
	var base_url = $("#base_url").val();
	window.location = base_url + "customer/doCustEdit/" + id;
}

function updateCustomer() {
	var base_url = $("#base_url").val();
	var name = $("#name").val();
	var username = $("#username").val();
	var email = $("#email").val();
	var phone = $("#phone").val();

	toastr.clear();

	if ($.trim(name) == '') {
		toastr.error('Please Enter Customer Name.');
		$("#name").focus();
		return false;
	}
	else if ($.trim(username) == '') {
		toastr.error('Please Enter Customer User Name.');
		$("#username").focus();
		return false;
	}
	else if ($.trim(email) == '') {
		toastr.error('Please Enter Customer Email Address.');
		$("#email").focus();
		return false;
	}
	else if ($.trim(phone) == '') {
		toastr.error('Please Enter Customer Contact.');
		$("#phone").focus();
		return false;
	}
	else {
		$('.loading').removeClass("hidden");
		$.ajax({
			url: base_url + "customer/doUpdateCust",
			data: $("#editCustfrm").serializeArray(),
			method: "post",
			success: function (result) {
				$('.loading').addClass("hidden");
				result = JSON.parse(result);
				if (result.status == 0) {
					toastr.success(result["message"]);
					setTimeout(
						function () {
							window.location = base_url + 'customer';

						}, 1000
					);
				} else {
					toastr.error(result["message"]);
					$("#" + result.field).focus();
				}
			}
		});
	}
}




function doCustStatusChange(value, id) {
	toastr.clear();
	var base_url = $("#base_url").val();
	if (confirm("Do you want to change status of this Customer?")) {
		$('.loading').removeClass("hidden");
		$.ajax({
			url: base_url + "customer/doCustStatusChange",
			data: "id=" + id + '&active=' + value,
			method: "post",
			success: function (result) {
				$('.loading').addClass("hidden");
				result = JSON.parse(result);
				if (result.status == 0) {
					toastr.success(result["message"]);
					/*setTimeout(
				  function() 
				  {
					window.location=base_url+'customer';
			 	
				  }, 1000);*/
				} else {
					toastr.error(result["message"]);
				}

			}
		});
	}
}
