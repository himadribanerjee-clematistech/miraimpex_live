

$(document).ready(function(){
	$('#date').datepicker({
		language: 'en',
		autoClose: true,
		dateFormat: 'dd-mm-yyyy',
	});
    CKEDITOR.replace('description');    
});

function editNews(id) {
	var base_url=$("#base_url").val();
	window.location=base_url+"news/doEdit/"+id;
}


function doStatusChange(value, id){
	toastr.clear();
	var base_url=$("#base_url").val();
	if(confirm("Do you want to change status of this News?")){
		$('.loading').removeClass("hidden");
		$.ajax({
			url: base_url+"news/doStatusChange",
			data: {id: id, status: value},
			method:"post",
			success: function(result){
			 	$('.loading').addClass("hidden");
			 	result=JSON.parse(result);
		 		if(result.status==0){
		 			toastr.success(result["message"]);
		 			/*setTimeout(
					  function() 
					  {
					    window.location=base_url+'news';
				 	
					  }, 1000);*/
		 		}else{
			 		toastr.error(result["message"]);						
			 	}
	    	
	  		}
	 	 });
	}
}