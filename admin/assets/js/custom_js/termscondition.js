$(document).ready(function(){
	CKEDITOR.replace('details');
});


function addtermsconditionDetails(){
	var base_url=$("#base_url").val();

	var code=$("#code").val();
	var name=$("#name").val();
	var details = CKEDITOR.instances['details'].getData();
	
	toastr.clear();

	if($.trim(name)=='' ){
		toastr.error('Please Enter Terms & Condition Name.');
		$("#name").focus();
		return false;
	}
	else if($.trim(details)=='' ){
		toastr.error('Please Enter Terms & Condition Details.');
		$("#details").focus();
		return false;
	}
	else{
		$('.loading').removeClass("hidden");

		$.ajax({
			url: base_url+"termsncondition/doAddTermsCondition",
			data: {
				'code': code,
				'name': name,
				'details': details
			},
			method:"post",
			success: function(result){
				$('.loading').addClass("hidden");
				result=JSON.parse(result);
				if(result.status==0){
					toastr.success(result["message"]);
					setTimeout(
					  function() 
					  {
					    window.location=base_url+'termsncondition';
				 	
					  }, 1000
					);
				}else{
		 			toastr.error(result["message"]);	
		 			$("#name").focus();			
				}	    	
	  		}
	 	});
	}
}