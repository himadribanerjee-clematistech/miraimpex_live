$(document).ready(function(){
    /*$('#from_date').datepicker({
        language: 'en',
        autoClose: true,
        dateFormat: 'dd-mm-yyyy',
    });*/
    $('#to_date').datepicker({
        language: 'en',
        autoClose: true,
        dateFormat: 'dd-mm-yyyy',
    });

    $('.chat').click(function(){
        var base_url = $('#base_url').val();
        var enq = $(this).data('enq-id');
        var to = $(this).data('to-customer-id');
        var from = $(this).data('from-customer-id');
        var name = $(this).data('name');
        
        $.ajax({
            url: base_url + 'serviceenquiry/Chat',
            type: 'POST',
            data: {enquiry_id: enq, to_customer_id: to, from_customer_id: from},
            dataType: 'JSON',
            success: function(response) {      
                console.log(response.data);

                $('#bussiness').html(name);
                $('#chatBoxDetails').empty();
                var i=0;
                var from_id ='';
                $.each(response.data, function(key, value) {
                    if(i==0){
                        from_id = value.from_customer_id; // For Left ChatBox
                    }
                    i++;                   
                    
                    if (value.from_customer_id == from_id) {

                        var replyMsg = $('#replyMsg').clone();

                        if (value.from_dp == null) {
                            replyMsg.find('#replyIcon').attr('src', base_url + 'assets/images/no-user-image.png').end();
                        }
                        else {
                            replyMsg.find('#replyIcon').attr('src', base_url + 'uploads/bussiness_logo/' + value.from_dp).end();
                        }

                        
                        replyMsg.find('#replyName').html(value.requested).end();
                        replyMsg.find('#replyDate').html(value.date).end();
                        replyMsg.find('#replyText').html(value.message).end();

                        if (value.is_seen != '0') {
                            replyMsg.find('#reply_seen').html('Seen').css('font-size', 10).end();
                        }
                        

                        /*if (value.image != null) {
                            $('#replyImage').removeClass('d-none');

                            $.each(value.images, function(key, val){
                                replyMsg.find('#replyImg').attr('src', base_url + 'uploads/rfq/' + val.images).end();
                            });                            
                        }*/

                        $('#chatBoxDetails').append(replyMsg);
                    } else{
                        var myMsg = $('#myMsg').clone();

                        if (value.to_dp == null) {
                            myMsg.find('#myIcon').attr('src', base_url + 'assets/images/no-user-image.png').end();
                        }
                        else {
                            myMsg.find('#myIcon').attr('src', base_url + 'uploads/bussiness_logo/' + value.to_dp).end();
                        }

                        myMsg.find('#myName').html(value.requested).end();
                        myMsg.find('#myDate').html(value.date).end();
                        myMsg.find('#myText').html(value.message).end();

                        if (value.is_seen != '0') {
                            replyMsg.find('#my_seen').html('Seen').css('font-size', 10).end();
                        }

                        /*if (value.images != null) {
                            $('#myImage').removeClass('d-none');

                            $.each(value.images, function(key, val){
                                myMsg.find('#myImg').attr('src', base_url + 'uploads/rfq/' + val.images).end();
                            });                            
                        }*/

                        $('#chatBoxDetails').append(myMsg);
                    }                 
                });              
                
                $('#chatviewModal').modal('show');
            }
        });
    });
});


function openModal(id) {
    var base_url = $('#base_url').val();
    $.ajax({
        url: base_url + 'serviceenquiry/Quote',
        type: 'POST',
        data: {id: id},
        dataType: 'JSON',
        success: function(response) {
            //console.log(response.data);

            $('#from_cust').html(response.data.from_cust);
            $('#to_cust').html(response.data.to_cust);
            $('#quantity').html(response.data.quantity + ' ' + response.data.uom);
            $('#date').html(response.data.date);
            $('#message').html(response.data.message);

            $('#viewModal').modal('show');
        }
    });
}