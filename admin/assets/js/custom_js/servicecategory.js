
function addServiceCatagory(){
	var base_url=$("#base_url").val();
	var service_category_name=$("#service_category_name").val();
	var image=$("#image").val();
	var uom_id = $("select[name='uom_id[]']").map(function(){return $(this).val();}).get();

	 toastr.clear();
	if($.trim(service_category_name)=='' ){
		toastr.error('Please Enter Service Category Name.');
		$("#service_category_name").focus();
		return false;
	}else if($.trim(image)=='' ){
		toastr.error('Please Select Service Category Image.');
		$("#image").focus();
		return false;
	}else{
		var formData = new FormData();
		formData.append('image', $('#image')[0].files[0]);
		formData.append('service_category_name', service_category_name);
		formData.append('uom_id', uom_id);
		///////////////
		$('.loading').removeClass("hidden");
		$.ajax({
		       url :base_url+"servicecategory/doAddServiceCat",
		       type : 'POST',
		       data : formData,
		       processData: false,  // tell jQuery not to process the data
		       contentType: false,  // tell jQuery not to set contentType
		       success : function(result) {
		           $('.loading').addClass("hidden");
			 	result=JSON.parse(result);
			 		if(result.status==0){
			 			toastr.success(result["message"]);
			 			setTimeout(
						  function() 
						  {
						    window.location=base_url+'servicecategory';
					 	
						  }, 1000);
			 		}else{
				 		toastr.error(result["message"]);
				 		if(result["field"]=='to_login'){
				 			window.location=base_url;
				 		}else{
				 			$("#"+result["field"]).focus();
				 		}
						
				 	}
		       }
		});
	}
}


function doServiceCatEdit(id){
	var base_url=$("#base_url").val();
	window.location=base_url+"servicecategory/doServiceCatEdit/"+id;
}


function UpdateServiceCatagory(){
	var base_url=$("#base_url").val();
	var service_category_name=$("#service_category_name").val();
	var id=$("#id").val();
	var image=$("#image").val();
	var img_old=$("#img_old").val();
	var uom_id = $("select[name='uom_id[]']").map(function(){return $(this).val();}).get();

	 toastr.clear();
	if($.trim(service_category_name)=='' ){
		toastr.error('Please Enter Service Category Name.');
		$("#service_category_name").focus();
		return false;
	}else if($.trim(image)=='' &&  $.trim(img_old)==''){
		toastr.error('Please Select Service Category Image.');
		$("#image").focus();
		return false;
	}else{
		var formData = new FormData();
		formData.append('image', $('#image')[0].files[0]);
		formData.append('service_category_name', service_category_name);
		formData.append('id', id);
		formData.append('uom_id', uom_id);
		///////////////
		$('.loading').removeClass("hidden");
		$.ajax({
		       url :base_url+"servicecategory/UpdateServiceCatagory",
		       type : 'POST',
		       data : formData,
		       processData: false,  // tell jQuery not to process the data
		       contentType: false,  // tell jQuery not to set contentType
		       success : function(result) {
		           $('.loading').addClass("hidden");
			 		result=JSON.parse(result);
			 		if(result.status==0){
			 			toastr.success(result["message"]);
			 			setTimeout(
						  function() 
						  {
						    window.location=base_url+'servicecategory';
					 	
						  }, 1000);
			 		}else{
				 		toastr.error(result["message"]);
				 		if(result["field"]=='to_login'){
				 			window.location=base_url;
				 		}else{
				 			$("#"+result["field"]).focus();
				 		}
						
				 	}
		       }
		});
	}
}


function doServiceCategoryStatusChange(value,id){
	toastr.clear();
	var base_url=$("#base_url").val();
	if(confirm("Do you want to change status of this Service?")){
		$('.loading').removeClass("hidden");
		$.ajax({
			url: base_url+"servicecategory/doServiceStatusChange",
			data:"id="+id+'&active='+value,
			method:"post",
			success: function(result){
			 	$('.loading').addClass("hidden");
			 	result=JSON.parse(result);
		 		if(result.status==0){
		 			toastr.success(result["message"]);
		 			/*setTimeout(
					  function() 
					  {
					    window.location=base_url+'servicecategory'+location.search;
				 	
					  }, 1000);*/
		 		}else{
			 		toastr.error(result["message"]);						
			 	}
	    	
	  		}
	 	 });
	}
}


/*
function doCatDelete(id) {
	toastr.clear();
	var base_url=$("#base_url").val();
	if(confirm("Do you want to delete this Service Category?")){
		$('.loading').removeClass("hidden");
		$.ajax({
			url: base_url+"Service Category/doCatDelete",
			data:"id="+id,
			method:"post",
			success: function(result){
			 	$('.loading').addClass("hidden");
			 	result=JSON.parse(result);
		 		if(result.status==0){
		 			toastr.success(result["message"]);
		 			setTimeout(
					  function() 
					  {
					    window.location=base_url+'Service Category';
				 	
					  }, 1000);
		 		}else{
			 		toastr.error(result["message"]);						
			 	}
	    	
	  		}
	 	 });
	}
}
*/