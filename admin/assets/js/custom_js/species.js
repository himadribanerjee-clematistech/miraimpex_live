function dospeciesEdit(id){
	var base_url=$("#base_url").val();
	window.location=base_url+"species/dospeciesEdit/"+id;
}

function dogetItemByCatSubCat(id){
	var category=$("#category").val();
	var base_url=$("#base_url").val();
	$.ajax({
			url: base_url+"item/dogetItemByCatSubCat",
			data:"subcate="+id+"&category="+category,
			method:"post",
			success: function(result){
			 	$('.loading').addClass("hidden");
			 	result=JSON.parse(result);
		 		if(result.status==0){
		 			var data=result.result;
		 			$("#item_id").html("")
		 			 $("#item_id") .append('<option value="">-- Select Item--</option>')
		 			if(data.length>0){
		 				$.each(result.result, function( index, value ) {
						  $("#item_id") .append('<option value="'+value.id+'">'+value.item_name+'</option>')
						});
		 			}
		 			
		 		}else{
			 		toastr.error(result["message"]);						
			 	}
	    	
	  		}
	 	 });
}
function dogetSubCatByCat(id){
	var base_url=$("#base_url").val();
	$.ajax({
			url: base_url+"subcategory/getSubCategoryListByCatId",
			data:"id="+id,
			method:"post",
			success: function(result){
			 	$('.loading').addClass("hidden");
			 	dogetItemByCatSubCat('');
			 	result=JSON.parse(result);
		 		if(result.status==0){
		 			var data=result.result;
		 			$("#subcategory").html("")
		 			 $("#subcategory") .append('<option value="">-- Select Sub Category--</option>')
		 			if(data.length>0){
		 				$.each(result.result, function( index, value ) {
						  $("#subcategory") .append('<option value="'+value.id+'">'+value.category_name+'</option>')
						});
		 			}
		 			
		 		}else{
			 		toastr.error(result["message"]);						
			 	}
	    	
	  		}
	 	 });

}
function addSpeciesDetails(){
	toastr.clear();
	var base_url=$("#base_url").val();
	var species_name=$("#species_name").val();
	var category=$("#category").val();
	var subcategory=$("#subcategory").val();
	var species_image=$("#species_image").val();
	var item_id=$("#item_id").val();
	if($.trim(species_name)=='' ){
		toastr.error('Please Enter Species Name.');
		$("#species_name").focus();
		return false;
	}else if($.trim(category)=='' ){
		toastr.error('Please Select Category.');
		$("#category").focus();
		return false;
	}else if($.trim(item_id)=='' ){
		toastr.error('Please Select Item.');
		$("#item_id").focus();
		return false;
	}else{
		var formData = new FormData();
		formData.append('species_image', $('#species_image')[0].files[0]);
		formData.append('species_name', species_name);
		formData.append('category', category);
		formData.append('subcategory', subcategory);
		formData.append('item_id', item_id);
		$('.loading').removeClass("hidden");
		$.ajax({
		       url :base_url+"species/doAddSpecies",
		       type : 'POST',
		       data : formData,
		       processData: false,  // tell jQuery not to process the data
		       contentType: false,  // tell jQuery not to set contentType
		       success : function(result) {
		           $('.loading').addClass("hidden");
			 	result=JSON.parse(result);
			 		if(result.status==0){
			 			toastr.success(result["message"]);
			 			setTimeout(
						  function() 
						  {
						    window.location=base_url+'species';
					 	
						  }, 1000);
			 		}else{
				 		toastr.error(result["message"]);
				 		if(result["field"]=='to_login'){
				 			window.location=base_url;
				 		}else{
				 			$("#"+result["field"]).focus();
				 		}
						
				 	}
		       }
		});
	}
}
function updateSpeciesDetails(){
	toastr.clear();
	var base_url=$("#base_url").val();
	var species_name=$("#species_name").val();
	var category=$("#category").val();
	var subcategory=$("#subcategory").val();
	var species_image=$("#species_image").val();
	var item_id=$("#item_id").val();
	var id=$("#id").val();
	if($.trim(species_name)=='' ){
		toastr.error('Please Enter Species Name.');
		$("#species_name").focus();
		return false;
	}else if($.trim(category)=='' ){
		toastr.error('Please Select Category.');
		$("#category").focus();
		return false;
	}else if($.trim(item_id)=='' ){
		toastr.error('Please Select Item.');
		$("#item_id").focus();
		return false;
	}else{
		var formData = new FormData();
		formData.append('species_image', $('#species_image')[0].files[0]);
		formData.append('species_name', species_name);
		formData.append('category', category);
		formData.append('subcategory', subcategory);
		formData.append('item_id', item_id);
		
		formData.append('id', id);
		$('.loading').removeClass("hidden");
		$.ajax({
		       url :base_url+"species/doUpdateSpecies",
		       type : 'POST',
		       data : formData,
		       processData: false,  // tell jQuery not to process the data
		       contentType: false,  // tell jQuery not to set contentType
		       success : function(result) {
		           $('.loading').addClass("hidden");
			 	result=JSON.parse(result);
			 		if(result.status==0){
			 			toastr.success(result["message"]);
			 			setTimeout(
						  function() 
						  {
						    window.location=base_url+'species';
					 	
						  }, 1000);
			 		}else{
				 		toastr.error(result["message"]);
				 		if(result["field"]=='to_login'){
				 			window.location=base_url;
				 		}else{
				 			$("#"+result["field"]).focus();
				 		}
						
				 	}
		       }
		});
	}
}
function doSpeciesStatusChange(value,id){
	toastr.clear();
	var base_url=$("#base_url").val();
	if(confirm("Do you want to change status of this Species?")){
		$('.loading').removeClass("hidden");
		$.ajax({
			url: base_url+"species/dospeciesStatusChange",
			data:"id="+id+'&active='+value,
			method:"post",
			success: function(result){
			 	$('.loading').addClass("hidden");
			 	result=JSON.parse(result);
		 		if(result.status==0){
		 			toastr.success(result["message"]);
		 			/*setTimeout(
					  function() 
					  {
					    window.location=base_url+'species'+location.search;
				 	
					  }, 1000);*/
		 		}else{
			 		toastr.error(result["message"]);						
			 	}
	    	
	  		}
	 	 });
	}
}
function dospeciesDelete(id) {
	toastr.clear();
	var base_url=$("#base_url").val();
	if(confirm("Do you want to delete this Species?")){
		$('.loading').removeClass("hidden");
		$.ajax({
			url: base_url+"species/dospeciesDelete",
			data:"id="+id,
			method:"post",
			success: function(result){
			 	$('.loading').addClass("hidden");
			 	result=JSON.parse(result);
		 		if(result.status==0){
		 			toastr.success(result["message"]);
		 			setTimeout(
					  function() 
					  {
					    window.location=base_url+'species';
				 	
					  }, 1000);
		 		}else{
			 		toastr.error(result["message"]);						
			 	}
	    	
	  		}
	 	 });
	}
}