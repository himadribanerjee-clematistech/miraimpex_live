function loadFile(){
                                
    var imgfilename = document.getElementById('chImg');

    imgfilename.src = URL.createObjectURL(event.target.files[0]);
};


function addSettings() {
	var base_url=$("#base_url").val();

	var numCheck = /^[1-9]\d*$/;

	var id = $("#id").val();
	var company_name = $("#company_name").val();
	var company_logo = $("#company_logo").val();
	var company_logo_old = $("#company_logo_old").val();
	var company_mobile = $("#company_mobile").val();
	var company_fax = $("#company_fax").val();
	var company_address = $("#company_address").val();
	var per_page = $("#per_page").val();

	var notification_time = $("#notification_time").val();
	var news_time = $("#news_time").val();
	var information_time = $("#information_time").val();
	var rfq_closing = $("#rfq_closing").val();

	toastr.clear();

	if($.trim(company_name)=='' ){
		toastr.error('Please Enter Company Name.');
		$("#company_name").focus();
		return false;
	}
	else if($.trim(company_logo)=='' &&  $.trim(company_logo_old)==''){
		toastr.error('Please Select Company Logo.');
		$("#company_logo").focus();
		return false;
	}
	else if($.trim(company_mobile)=='' ){
		toastr.error('Please Enter Company Mobile.');
		$("#company_mobile").focus();
		return false;
	}
	else if($.trim(company_fax)=='' ){
		toastr.error('Please Enter Company Fax.');
		$("#company_fax").focus();
		return false;
	}
	else if($.trim(company_address)=='' ){
		toastr.error('Please Enter Company Address.');
		$("#company_address").focus();
		return false;
	}
	else if($.trim(per_page)=='' ){
		toastr.error('Please Enter Pagination Details.');
		$("#per_page").focus();
		return false;
	}
	else if(!per_page.match(numCheck)){
		toastr.error('Please Provide Valid Pagination Number.');
		$("#per_page").focus();
		return false;
	}
	else if($.trim(notification_time)=='' ){
		toastr.error('Please Enter Proper Notification Lead Time.');
		$("#notification_time").focus();
		return false;
	}
	else if($.trim(news_time)=='' ){
		toastr.error('Please Enter Proper News Lead Time.');
		$("#news_time").focus();
		return false;
	}
	else if($.trim(information_time)=='' ){
		toastr.error('Please Enter Proper Information Lead Time.');
		$("#information_time").focus();
		return false;
	}
	else if($.trim(rfq_closing)=='' ){
		toastr.error('Please Enter Proper RFQ Default Closing Time.');
		$("#rfq_closing").focus();
		return false;
	}
	else {
		var formData = new FormData();
		
		formData.append('id', id);
		formData.append('company_name', company_name);
		formData.append('company_logo', $('#company_logo')[0].files[0]);
		formData.append('company_mobile', company_mobile);
		formData.append('company_fax', company_fax);
		formData.append('company_address', company_address);
		formData.append('per_page', per_page);
		
		formData.append('notification_time', notification_time);
		formData.append('news_time', news_time);
		formData.append('information_time', information_time);
		formData.append('rfq_closing', rfq_closing);

		$('.loading').removeClass("hidden");

		$.ajax({
			url :base_url+"settings/doAddSettings",
	       	type : 'POST',
	       	data : formData,
	       	processData: false,  // tell jQuery not to process the data
	       	contentType: false,  // tell jQuery not to set contentType
	       	success : function(result) {
		        $('.loading').addClass("hidden");
			 	result=JSON.parse(result);
		 		if(result.status==0){
		 			toastr.success(result["message"]);
		 			setTimeout(
					  function() 
					  {
					    window.location=base_url+'settings';
				 	
					  }, 1000);
		 		}else{
			 		toastr.error(result["message"]);
			 		if(result["field"]=='to_login'){
			 			window.location=base_url;
			 		}else{
			 			$("#"+result["field"]).focus();
			 		}
					
			 	}
		    }
		});
	}

}