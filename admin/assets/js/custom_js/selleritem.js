 
function loadFile(){
    
    var imgfilename = document.getElementById('chImg');

    imgfilename.src = URL.createObjectURL(event.target.files[0]);
};
                                   

function getSpecies(id){
	var base_url=$("#base_url").val();
	$.ajax({
		url: base_url+"selleritem/getSpecies",
		data:{id : id},
		method:"post",
		success: function(result){
		 	$('.loading').addClass("hidden");
		 	result=JSON.parse(result);
	 		if(result.status==0){
	 			var data=result.result;
	 			$("#species_id").html("")
	 			$("#species_id") .append('<option value="">-- select --</option>')
	 			if(data.length>0){
	 				$.each(result.result, function( index, value ) {
					  $("#species_id") .append('<option value="'+value.id+'">'+value.species_name+'</option>')
					});
	 			}
	 			
	 		}else{
		 		toastr.error(result["message"]);						
		 	}
    	
  		}
	});

}

function addItemDetails(){
	toastr.clear();
	var base_url=$("#base_url").val();

	var pattern_number = /^[+-]?([0-9]*[.])?[0-9]+$/i;
	var pattern_number_2 = /^\d*[0-9]\d*$/i

	var customer_id=$("#customer_id").val();
	var item_id=$("#item_id").val();
	var species_id=$("#species_id").val();
	var item_description=$("#item_description").val();
	var special_information=$("#special_information").val();
	var packing_information=$("#packing_information").val();
	var shipping_information=$("#shipping_information").val();
	var item_image=$("#item_image").val();
	var min_quantity=$("#min_quantity").val();
	var min_price=$("#min_price").val();
	var max_price=$("#max_price").val();
	var organic_YN=$("#organic_YN").val();
	if($.trim(customer_id)=='' ){
		toastr.error('Please Select Customer.');
		$("#customer_id").focus();
		return false;
	}else if($.trim(item_id)=='' ){
		toastr.error('Please Select Item.');
		$("#item_id").focus();
		return false;
	}else if($.trim(species_id)=='' ){
		toastr.error('Please Select Species.');
		$("#species_id").focus();
		return false;
	}else if($.trim(min_quantity)=='' ){
		toastr.error('Please Enter Quantity.');
		$("#min_quantity").focus();
		return false;
	}else if(!pattern_number_2.test(min_quantity)  ){
		toastr.error('Please Enter  Valid Quantity.');
		$("#min_quantity").focus();
		return false;
	}else if($.trim(item_description)=='' ){
		toastr.error('Please Enter Item Description.');
		$("#item_description").focus();
		return false;
	}else if($.trim(special_information)=='' ){
		toastr.error('Please Enter Special Information.');
		$("#special_information").focus();
		return false;
	}else if($.trim(packing_information)=='' ){
		toastr.error('Please Enter Packing Information.');
		$("#packing_information").focus();
		return false;
	}else if($.trim(shipping_information)=='' ){
		toastr.error('Please Enter Shipping Information.');
		$("#shipping_information").focus();
		return false;
	}else if($.trim(min_price)=='' ){
		toastr.error('Please Enter Minimum Price.');
		$("#min_price").focus();
		return false;
	}else if(!pattern_number.test(min_price)) {
		toastr.error('Please Enter Valid Price.');
		$("#min_price").focus();
		return false;
	}else if($.trim(max_price)=='' ){
		toastr.error('Please Enter Maximum Price.');
		$("#max_price").focus();
		return false;
	}else if(!pattern_number.test(max_price)) {
		toastr.error('Please Enter Valid Price.');
		$("#max_price").focus();
		return false;
	}else if($.trim(item_image)=='' ){
		toastr.error('Please Select Item Image.');
		$("#item_image").focus();
		return false;
	}else{
		var formData = new FormData();

		formData.append('item_image', $('#item_image')[0].files[0]);
		formData.append('customer_id', customer_id);
		formData.append('item_id', item_id);
		formData.append('species_id', species_id);
		formData.append('item_description', item_description);
		formData.append('special_information', special_information);
		formData.append('packing_information', packing_information);
		formData.append('shipping_information', shipping_information);
		formData.append('min_quantity', min_quantity);
		formData.append('min_price', min_price);
		formData.append('max_price', max_price);
		formData.append('organic_YN', organic_YN);

		$('.loading').removeClass("hidden");
		$.ajax({
	       	url :base_url+"selleritem/doAddItem",
	       	type : 'POST',
	       	data : formData,
	       	processData: false,  // tell jQuery not to process the data
	       	contentType: false,  // tell jQuery not to set contentType
	       	success : function(result) {
	        	$('.loading').addClass("hidden");
		 		result=JSON.parse(result);
		 		if(result.status==0){
		 			toastr.success(result["message"]);
		 			setTimeout(
					  function() 
					  {
					    window.location=base_url+'selleritem';
				 	
					  }, 1000);
		 		}else{
			 		toastr.error(result["message"]);
			 		if(result["field"]=='to_login'){
			 			window.location=base_url;
			 		}else{
			 			$("#"+result["field"]).focus();
			 		}
					
			 	}
	       	}
		});
	}
}


function doitemEdit(id) {
	var base_url = $("#base_url").val();
	window.location = base_url + "selleritem/doitemEdit/" + id;
}


function updateItem() {
	toastr.clear();
	var base_url=$("#base_url").val();

	var pattern_number = /^[+-]?([0-9]*[.])?[0-9]+$/i;
	var pattern_number_2 = /^\d*[0-9]\d*$/i


	var id=$("#id").val();
	var customer_id=$("#customer_id").val();
	var item_id=$("#item_id").val();
	var species_id=$("#species_id").val();
	var item_description=$("#item_description").val();
	var special_information=$("#special_information").val();
	var packing_information=$("#packing_information").val();
	var shipping_information=$("#shipping_information").val();
	var item_image=$("#item_image").val();
	var old_image=$("#img_old").val();
	var min_quantity=$("#min_quantity").val();
	var min_price=$("#min_price").val();
	var max_price=$("#max_price").val();
	var organic_YN=$("#organic_YN").val();
	if($.trim(customer_id)=='' ){
		toastr.error('Please Select Customer.');
		$("#customer_id").focus();
		return false;
	}else if($.trim(item_id)=='' ){
		toastr.error('Please Select Item.');
		$("#item_id").focus();
		return false;
	}else if($.trim(species_id)=='' ){
		toastr.error('Please Select Species.');
		$("#species_id").focus();
		return false;
	}else if($.trim(min_quantity)=='' ){
		toastr.error('Please Enter Quantity.');
		$("#min_quantity").focus();
		return false;
	}else if(!pattern_number_2.test(min_quantity)  ){
		toastr.error('Please Enter  Valid Quantity.');
		$("#min_quantity").focus();
		return false;
	}else if($.trim(item_description)=='' ){
		toastr.error('Please Enter Item Description.');
		$("#item_description").focus();
		return false;
	}else if($.trim(special_information)=='' ){
		toastr.error('Please Enter Special Information.');
		$("#special_information").focus();
		return false;
	}else if($.trim(packing_information)=='' ){
		toastr.error('Please Enter Packing Information.');
		$("#packing_information").focus();
		return false;
	}else if($.trim(shipping_information)=='' ){
		toastr.error('Please Enter Shipping Information.');
		$("#shipping_information").focus();
		return false;
	}else if($.trim(min_price)=='' ){
		toastr.error('Please Enter Minimum Price.');
		$("#min_price").focus();
		return false;
	}else if(!pattern_number.test(min_price)) {
		toastr.error('Please Enter Valid Price.');
		$("#min_price").focus();
		return false;
	}else if($.trim(max_price)=='' ){
		toastr.error('Please Enter Maximum Price.');
		$("#max_price").focus();
		return false;
	}else if(!pattern_number.test(max_price)) {
		toastr.error('Please Enter Valid Price.');
		$("#max_price").focus();
		return false;
	}else if($.trim(item_image)=='' && $.trim(old_image) == ''){
		toastr.error('Please Select Item Image.....');
		$("#item_image").focus();
		return false;
	}else{
		var formData = new FormData();

		formData.append('item_image', $('#item_image')[0].files[0]);
		formData.append('customer_id', customer_id);
		formData.append('item_id', item_id);
		formData.append('species_id', species_id);
		formData.append('item_description', item_description);
		formData.append('special_information', special_information);
		formData.append('packing_information', packing_information);
		formData.append('shipping_information', shipping_information);
		formData.append('min_quantity', min_quantity);
		formData.append('min_price', min_price);
		formData.append('max_price', max_price);
		formData.append('organic_YN', organic_YN);

		formData.append('old_image', old_image);
		formData.append('id', id);

		$('.loading').removeClass("hidden");
		$.ajax({
	       	url :base_url+"selleritem/doUpdateItem",
	       	type : 'POST',
	       	data : formData,
	       	processData: false,  // tell jQuery not to process the data
	       	contentType: false,  // tell jQuery not to set contentType
	       	success : function(result) {
	        	$('.loading').addClass("hidden");
		 		result=JSON.parse(result);
		 		if(result.status==0){
		 			toastr.success(result["message"]);
		 			setTimeout(
					  function() 
					  {
					    window.location=base_url+'selleritem';
				 	
					  }, 1000);
		 		}else{
			 		toastr.error(result["message"]);
			 		if(result["field"]=='to_login'){
			 			window.location=base_url;
			 		}else{
			 			$("#"+result["field"]).focus();
			 		}
					
			 	}
	       	}
		});
	}
}


function doItemStatusChange(value,id){
	toastr.clear();
	var base_url=$("#base_url").val();
	if(confirm("Do you want to change status of this Data?")){
		
		$('.loading').removeClass("hidden");
		$.ajax({
			url: base_url+"selleritem/doStatusChange",
			data: {id: id, status: value},
			method:"post",
			success: function(result){
			 	$('.loading').addClass("hidden");
			 	result=JSON.parse(result);
		 		if(result.status==0){
		 			toastr.success(result["message"]);
		 			/*setTimeout(
					  function() 
					  {
					    window.location=base_url+'selleritem';
				 	
					  }, 1000);*/
		 		}else{
			 		toastr.error(result["message"]);						
			 	}
	    	
	  		}
	 	 });
	}
}