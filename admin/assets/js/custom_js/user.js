function addUserDetails(){
	var base_url=$("#base_url").val();
	var user_name=$("#user_name").val();
	var Name=$("#Name").val();
	var password=$("#password").val();
	var cpassword=$("#cpassword").val();
	var email=$("#email").val();
	var mobile=$("#mobile").val();
	var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i
	 toastr.clear();
		if($.trim(user_name)=='' ){
		toastr.error('Please Enter User Name.');
		$("#user_name").focus();
		return false;
	}else if($.trim(Name)=='' ){
		toastr.error('Please Enter Name.');
		$("#Name").focus();
		return false;
	}else if($.trim(email)=='' ){
		toastr.error('Please Enter Email Id.');
		$("#email").focus();
		return false;
	}else if(!pattern.test(email) ){
		toastr.error('Please Enter Valid Email Id.');
		$("#email").focus();
		return false;
	}else if($.trim(password)=='' ){
		toastr.error('Please Enter Password.');
		$("#password").focus();
		return false;
	}else if($.trim(cpassword)=='' ){
		toastr.error('Please Enter Confirm Password.');
		$("#cpassword").focus();
		return false;
	}else if($.trim(cpassword)!=$.trim(password) ){
		toastr.error('Password Mismatch.');
		$("#cpassword").focus();
		return false;
	}else if($.trim(mobile)=='' ){
		toastr.error('Please Enter Mobile No.');
		$("#mobile").focus();
		return false;
	}else{
		$('.loading').removeClass("hidden");
		$.ajax({
			url: base_url+"user/doAddUser",
			data:$("#adduserfrm").serializeArray(),
			method:"post",
			 success: function(result){
			 	$('.loading').addClass("hidden");
			 	result=JSON.parse(result);
			 		if(result.status==0){
			 			toastr.success(result["message"]);
			 			setTimeout(
						  function() 
						  {
						    window.location=base_url+'user';
					 	
						  }, 1000);
			 		}else{
				 		toastr.error(result["message"]);
				 		if(result["field"]=='to_login'){
				 			window.location=base_url;
				 		}else{
				 			$("#"+result["field"]).focus();
				 		}
						
				 	}
	    	
	  		}
	 	 });
	}
}


function doUserEdit(id){
	var base_url=$("#base_url").val();
	window.location=base_url+"user/doUserEdit/"+id;
}
function UpdateUserDetails(){
		var base_url=$("#base_url").val();
	var user_name=$("#user_name").val();
	var Name=$("#Name").val();
	var password=$("#password").val();
	var cpassword=$("#cpassword").val();
	var email=$("#email").val();
	var mobile=$("#mobile").val();
	var actve=$("#actve").val();
	var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i
	 toastr.clear();
	if($.trim(user_name)=='' ){
		toastr.error('Please Enter User Name.');
		$("#user_name").focus();
		return false;
	}else if($.trim(Name)=='' ){
		toastr.error('Please Enter Name.');
		$("#Name").focus();
		return false;
	}else if($.trim(email)=='' ){
		toastr.error('Please Enter Email Id.');
		$("#email").focus();
		return false;
	}else if(!pattern.test(email) ){
		toastr.error('Please Enter Valid Email Id.');
		$("#email").focus();
		return false;
	}else if($.trim(mobile)=='' ){
		toastr.error('Please Enter Mobile No.');
		$("#mobile").closest("div").addClass("has-error");
		$("#mobile").focus();
		return false;
	}else if($.trim(actve)=='' ){
		toastr.error('Please Select Status.');
		$("#actve").focus();
		return false;
	}else{
		$('.loading').removeClass("hidden");
		$.ajax({
			url: base_url+"user/UpdateUserDetails",
			data:$("#adduserfrm").serializeArray(),
			method:"post",
			 success: function(result){
			 	$('.loading').addClass("hidden");
			 	result=JSON.parse(result);
			 		if(result.status==0){
			 			toastr.success(result["message"]);
			 			setTimeout(
						  function() 
						  {
						    window.location=base_url+'user';
					 	
						  }, 1000);
			 		}else{
				 		toastr.error(result["message"]);
				 		if(result["field"]=='to_login'){
				 			window.location=base_url;
				 		}else{
				 			$("#"+result["field"]).focus();
				 		}
						
				 	}
	    	
	  		}
	 	 });
	}
}

function doUserStatusChange(value,id){
	toastr.clear();
	var base_url=$("#base_url").val();
	if(confirm("Do you want to change status of this user?")){
		$('.loading').removeClass("hidden");
		$.ajax({
			url: base_url+"user/douserStatusChange",
			data:"id="+id+'&actve='+value,
			method:"post",
			success: function(result){
			 	$('.loading').addClass("hidden");
			 	result=JSON.parse(result);
		 		if(result.status==0){
		 			toastr.success(result["message"]);
		 			/*setTimeout(
					  function() 
					  {
					    window.location=base_url+'user'+location.search;
				 	
					  }, 1000);*/
		 		}else{
			 		toastr.error(result["message"]);						
			 	}
	    	
	  		}
	 	 });
	}
}
function doUserDelete(id){
	 toastr.clear();
	var base_url=$("#base_url").val();
	if(confirm("Do you want to delete this user?")){
		$('.loading').removeClass("hidden");
		$.ajax({
			url: base_url+"user/doUserDelete",
			data:"id="+id,
			method:"post",
			 success: function(result){
			 	$('.loading').addClass("hidden");
			 	result=JSON.parse(result);
			 		if(result.status==0){
			 			toastr.success(result["message"]);
			 			setTimeout(
						  function() 
						  {
						    window.location=base_url+'user';
					 	
						  }, 1000);
			 		}else{
				 		toastr.error(result["message"]);
				 		if(result["field"]=='to_login'){
				 			window.location=base_url;
				 		}
						
				 	}
	    	
	  		}
	 	 });
	}
}

function UpdateProfileDetails(){
	var base_url=$("#base_url").val();
	var user_name=$("#user_name").val();
	var Name=$("#Name").val();
	var email=$("#email").val();
	var mobile=$("#mobile").val();
	var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i
	 toastr.clear();
	if($.trim(user_name)=='' ){
		toastr.error('Please Enter User Name.');
		$("#user_name").focus();
		return false;
	}else if($.trim(Name)=='' ){
		toastr.error('Please Enter Name.');
		$("#Name").focus();
		return false;
	}else if($.trim(email)=='' ){
		toastr.error('Please Enter Email Id.');
		$("#email").focus();
		return false;
	}else if(!pattern.test(email) ){
		toastr.error('Please Enter Valid Email Id.');
		$("#email").focus();
		return false;
	}else if($.trim(mobile)=='' ){
		toastr.error('Please Enter Mobile No.');
		$("#mobile").focus();
		return false;
	}else{
		$('.loading').removeClass("hidden");
		$.ajax({
			url: base_url+"user/UpdateProfileDetails",
			data:$("#adduserfrm").serializeArray(),
			method:"post",
			 success: function(result){
			 	$('.loading').addClass("hidden");
			 	result=JSON.parse(result);
			 		if(result.status==0){
			 			toastr.success(result["message"]);
			 			setTimeout(
						  function() 
						  {
						    window.location=base_url+'editProfile';
					 	
						  }, 1000);
			 		}else{
				 		toastr.error(result["message"]);
				 		if(result["field"]=='to_login'){
				 			window.location=base_url;
				 		}else{
				 			$("#"+result["field"]).focus();
				 		}
						
				 	}
	    	
	  		}
	 	 });
	}
}
 function doChangePassword(){
	var base_url=$("#base_url").val();
	var old_password=$("#old_password").val();
	var new_password=$("#new_password").val();
	var conf_password=$("#conf_password").val();
	if($.trim(old_password)=='' ){
		toastr.error('Please Enter Current Password.');
		$("#old_password").focus();
		return false;
	}else if($.trim(new_password)=='' ){
		toastr.error('Please Enter New Password.');
		$("#new_password").focus();
		return false;
	}else if($.trim(conf_password)=='' ){
		toastr.error('Please Enter Confirm  New Password.');
		$("#conf_password").focus();
		return false;
	}else if($.trim(conf_password)!=$.trim(new_password) ){
		toastr.error(' New Password Mismatch.');
		$("#conf_password").focus();
		return false;
	}else{
		$('.loading').removeClass("hidden");
		$.ajax({
			url: base_url+"user/doChangePassword",
			data:$("#adduserfrm").serializeArray(),
			method:"post",
			 success: function(result){
			 	$('.loading').addClass("hidden");
			 	result=JSON.parse(result);
			 		if(result.status==0){
			 			toastr.success(result["message"]);
			 			setTimeout(
						  function() 
						  {
						    window.location=base_url+'changePassword';
					 	
						  }, 1000);
			 		}else{
				 		toastr.error(result["message"]);
				 		if(result["field"]=='to_login'){
				 			window.location=base_url;
				 		}else{
				 			$("#"+result["field"]).focus();
				 		}
						
				 	}
	    	
	  		}
	 	 });
	}

}

 var elems = Array.prototype.slice.call(document.querySelectorAll('.switch-btn'));
        $('.switch-btn').each(function() {
            new Switchery($(this)[0], $(this).data());
        });
        
        $(document).ready(function() {

        	
            //set initial state.
            $('.custom-check').val($(this).is(':checked'));
        
            $('.custom-check').change(function() {
                if($(this).is(":checked")) {
                    var returnVal = confirm("Are you sure?");
                    if (returnVal== true) {
                        alert('checked-block'+returnVal)
                        $(this).attr("checked", true);
                        var home_dis_value= $(this).attr("data-setvalue");
                    }else{
                       $(this).attr("checked", false);  
                    }
                }else{
                    var returnVal = confirm("Are you sure?");
                    if (returnVal== true) {
                        alert('un-checked-block'+returnVal)
                        $(this).attr("checked", false);
                        var home_dis_value= $(this).attr("data-setvalue");
                    }else{
                       $(this).attr("checked", true); 
                    }
                }
                //$('.custom-check').val($(this).is(':checked'));        
            });
        });