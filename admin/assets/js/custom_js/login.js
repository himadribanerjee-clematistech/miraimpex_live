  $(document).ready(function () {
             
  });
  function doforgetValidate(){
  	var base_url=$("#base_url").val();
  	toastr.clear();
  	var forget_input=$("#forget_input").val();
  	$(".form-control").closest("div").removeClass("has-error");
  	var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i
  	if($.trim(forget_input)=='' ){
		toastr.error('Please Enter Email Id.');
		$("#forget_input").focus();
		return false;
	}else if(!pattern.test(forget_input) ){
		toastr.error('Please Enter Valid Email Id.');
		$("#forget_input").focus();
		return false;
	}else{
		$('.loading').removeClass("d-none");
		$.ajax({
			url: base_url+"login/doforgetValidate",
			data:"forget_input="+forget_input,
			method:"post",
			 success: function(result){
			 	$('.loading').addClass("d-none");
			 	result=JSON.parse(result);
			 		if(result.status==0){
			 			toastr.success(result["message"]);
			 			$("#login").addClass("d-none");
  						$("#forget_pass").addClass("d-none");
  						$("#password_set").addClass("d-none");
  						
			 			$("#otp_pass").removeClass("d-none");
			 		}else{
				 		toastr.error(result["message"]);
						$("#forget_pass").removeClass("d-none");
						$("#otp_pass").addClass("d-none");
						$("#login").addClass("d-none");
						$("#password_set").addClass("d-none");
						$("#"+result["field"]).focus();
				 	}
	    	
	  		}
	 	 });
	}
  }
  function doPassWordSet(){
  		var base_url=$("#base_url").val();
  		toastr.clear();
  		var new_password=$("#new_password").val();
		var conf_password=$("#conf_password").val();
		var token=$("#token").val();
		if($.trim(new_password)=='' ){
			toastr.error('Please Enter New Password.');
			$("#new_password").focus();
			return false;
		}else if($.trim(conf_password)=='' ){
			toastr.error('Please Enter Confirm  New Password.');
			$("#conf_password").focus();
			return false;
		}else if($.trim(conf_password)!=$.trim(new_password) ){
			toastr.error(' New Password Mismatch.');
			$("#conf_password").focus();
			return false;
		}else{
			$('.loading').removeClass("d-none");
			$.ajax({
				url: base_url+"login/doChangePassword",
				data:'new_password='+new_password+"&conf_password="+conf_password+"&token="+token,
				method:"post",
				 success: function(result){
				 	$('.loading').addClass("d-none");
				 	result=JSON.parse(result);
				 		if(result.status==0){
				 			toastr.success(result["message"]);
				 			setTimeout(
							  function() 
							  {
							    window.location=base_url;
						 	
							  }, 1000);
				 		}else{
					 		toastr.error(result["message"]);
					 		$("#"+result["field"]).focus();
							
					 	}
		    	
		  		}
		 	 });
		}
  }
  function doOTPValidate(){
  	var base_url=$("#base_url").val();
  	toastr.clear();
  	var otp_input=$("#otp_input").val();
  	var forget_input=$("#forget_input").val();
  	if($.trim(otp_input)==''){
		toastr.error('Please Enter OTP.');
		$("#otp_input").closest("div").addClass("has-error");
		$("#otp_input").focus();
		return false;
	}else {
		$('.loading').removeClass("d-none");
		$.ajax({
			url: base_url+"login/doOTPValidate",
			data:"otp_input="+otp_input+"&forget_input="+forget_input,
			method:"post",
			 success: function(result){
			 	$('.loading').addClass("d-none");
			 	result=JSON.parse(result);
			 		if(result.status==0){
			 			toastr.success(result["message"]);
			 			$("#password_set").removeClass("d-none");
			 			$("#forget_pass").addClass("d-none");
						$("#otp_pass").addClass("d-none");
						$("#login").addClass("d-none");
						$("#forget_input").val("");
						$("#otp_input").val("");
						$("#token").val(result["token"]);
			 		}else{
				 		toastr.error(result["message"]);
						$("#"+result["field"]).focus();
						$("#forget_pass").addClass("d-none");
						$("#otp_pass").removeClass("d-none");
						$("#password_set").addClass("d-none");
						$("#login").addClass("d-none");
				 	}
	    	
	  		}
	 	 });
	}
	
  }
  function doLoginValidate(){
  	var user_name=$("#user_name").val();
  	var password=$("#password").val();
  	var base_url=$("#base_url").val();
  	toastr.clear();
	if($.trim(user_name)==''){
		toastr.error('Please Enter User Name.');
		$("#user_name").closest("div").addClass("has-error");
		$("#user_name").focus();
		return false;
	}else if($.trim(password)==''){
		toastr.error('Please Enter password.');
		$("#password").closest("div").addClass("has-error");
		$("#password").focus();
		return false;
	}else{
		$('.loading').removeClass("d-none");
		$.ajax({
			url: base_url+"login/doLoginValidate",
			data:$("#loginfrm").serializeArray(),
			method:"post",
			 success: function(result){
			 	$('.loading').addClass("d-none");
			 	result=JSON.parse(result);
			 		if(result.status==0){
			 			toastr.success(result["message"]);
			 			setTimeout(
						  function() 
						  {
						    window.location=base_url+"dashboard";
					 	
						  }, 1000);
			 		}else{
				 		toastr.error(result["message"]);
						$("#"+result["field"]).focus();
				 	}
	    	
	  		}
	 	 });
	}
  }
  function openforgetpassword(){
  	$("#login").addClass("d-none");
  	$("#forget_pass").removeClass("d-none");
  	$("#otp_pass").addClass("d-none");
  	$("#password_set").addClass("d-none");
  }
  function openlogin(){
  	$("#forget_pass").addClass("d-none");
  	$("#login").removeClass("d-none");
  	$("#otp_pass").addClass("d-none");
  	$("#password_set").addClass("d-none");
  }