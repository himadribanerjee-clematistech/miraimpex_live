$(document).ready(function() {
    $('#cust_id').select2();
    $('#supp_id').select2();
});



$('.response').click(function(){
    var base_url = $('#base_url').val();
    var iid = $(this).data('id'); 

    $.ajax({
        url: base_url + 'lclsplr/Details',
        type: 'POST',
        data: {id: iid},
        dataType: 'JSON',
        success: function(response) {
            console.log(response.data);
            
            if (response.data.description == null) {
                $('#details').html('');
                $('.detail_row').hide();
            }
            else {
                $('#details').html(response.data.description);
                $('.detail_row').show();
            }
            
            $('#supplier').html(response.data.supplier_name);
            $('#supplier_id').html(response.data.id);
            $('#customer').html(response.data.name);
            $('#created').html(response.data.created);

            $('#viewModal').modal('show');
        }
    });


});



function doStatusChange(value,id){
    toastr.clear();
    var base_url=$("#base_url").val();
    if(confirm("Do you want to change status of this Item?")){
        $('.loading').removeClass("hidden");
        $.ajax({
            url: base_url+"lclsplr/doStatusChange",
            data:"id="+id+'&active='+value,
            method:"post",
            success: function(result){
                $('.loading').addClass("hidden");
                result=JSON.parse(result);
                if(result.status==0){
                    toastr.success(result["message"]);
                    /*setTimeout(
                      function() 
                      {
                        window.location=base_url+'item'+location.search;
                    
                      }, 1000);*/
                }else{
                    toastr.error(result["message"]);                        
                }
            
            }
         });
    }
}