$(document).ready(function(){

    $('.view').click(function(e){
        var iid = $(this).attr('data-id');

        $.ajax({
            url: 'provideritem/getById',
            type: 'POST',
            data: { id: iid },
            dataType: 'JSON',
            success: function(response){
                //console.log(response);

                $('.item_name').html('');
                $('.provider').html('');
                $('.amount').html('');
                $('.uom').html('');
                $('.description').html('');
                $('.special_instruction').html('');
                $('.terms_condition').html('');
                $('.image').html('');
                
                var amount = '&#8358; ' + response.min_rate + ' to &#8358; ' + response.max_rate;

                $('.item_name').html(response.item);
                $('.provider').html(response.provider);
                $('.amount').html(amount);
                $('.uom').html(response.uom);
                $('.description').html(response.description);
                $('.special_instruction').html(response.special_instruction);
                $('.terms_condition').html(response.terms_condition);

                if (response.image.length > 0) {
                    $('#row').show();

                    $.each(response.image, function(key, value) {
                        $('.image').append('<li style="padding-left: 5px;"><img style="width: 110px; height: 110px;" src="uploads/selleritem/' + value.image + '" /></li>');                      
                    });
                }
                else {
                    $('#row').hide();
                }                                  
                

                $('#shwModal').modal('show');
            }
        });
    });

});