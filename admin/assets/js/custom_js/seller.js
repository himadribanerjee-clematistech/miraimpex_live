function doSellerEdit(id) {
	var base_url = $("#base_url").val();
	window.location = base_url + "seller/doSellerEdit/" + id;
}

function doLclSuppStatusChange(value, id) {
	toastr.clear();
	var base_url = $("#base_url").val();
	if (confirm("Do you want to change Local Supplier Status of this Customer?")) {
		$('.loading').removeClass("hidden");
		$.ajax({
			url: base_url + "seller/doLclSuppStatusChange",
			data: "id=" + id + '&displayLocal=' + value,
			method: "post",
			success: function (result) {
				$('.loading').addClass("hidden");
				result = JSON.parse(result);
				if (result.status == 0) {
					toastr.success(result["message"]);
					/*setTimeout(
				  function() 
				  {
					window.location=base_url+'customer';
			 	
				  }, 1000);*/
				} else {
					toastr.error(result["message"]);
				}

			}
		});
	}
}

/*function editSeller(){
	var base_url=$("#base_url").val();

	var pattern_lat_long =/^-?([1-8]?[1-9]|[1-9]0)\.{1}\d{1,6}$/i;
	var pattern_number = /^[0-9]*$/;

	var id=$("#id").val();
	var cust_id=$("#customer_id").val();
	var business_name=$("#business_name").val();
	var address=$("#address").val();
	var state=$("#state").val();
	var city=$("#city").val();
	var lat=$("#lat").val();
	var long=$("#long").val();
	var contact_name=$("#contact_name").val();
	var contact_number=$("#contact_number").val();
	var business_logo=$("#business_logo").val();

	//console.log(img_old);

	if($.trim(cust_id)=='' ){
		toastr.error('Please Choose Customer Name.');
		$("#cust_id").focus();
		return false;
	}
	else if($.trim(business_name)=='' ){
		toastr.error('Please Enter Bussiness Name.');
		$("#business_name").focus();
		return false;
	}
	else if($.trim(address)=='' ){
		toastr.error('Please Enter Address.');
		$("#address").focus();
		return false;
	}
	else if($.trim(state)=='' ){
		toastr.error('Please Enter State.');
		$("#state").focus();
		return false;
	}
	else if($.trim(city)=='' ){
		toastr.error('Please Enter City.');
		$("#city").focus();
		return false;
	}
	else if($.trim(lat)=='' ){
		toastr.error('Please Enter Latitude.');
		$("#lat").focus();
		return false;
	}
	else if (!pattern_lat_long.test(lat)) {
		toastr.error('Please Enter Valid Latitude.');
		$("#lat").focus();
		return false;
	}
	else if($.trim(long)=='' ){
		toastr.error('Please Enter Longitude.');
		$("#long").focus();
		return false;
	}
	else if(!pattern_lat_long.test(long)) {
		toastr.error('Please Enter Valid Longitude.');
		$("#long").focus();
		return false;
	}
	else if($.trim(contact_name)=='' ){
		toastr.error('Please Enter Contact Name.');
		$("#contact_name").focus();
		return false;
	}
	else if($.trim(contact_number)=='' ){
		toastr.error('Please Enter Contact Number.');
		$("#contact_number").focus();
		return false;
	}
	else if(!pattern_number.test(contact_number)) {
		toastr.error('Please Enter Valid Contact Number.');
		$("#contact_number").focus();
		return false;
	}
	else{

		var formData = new FormData();

		if (business_logo != '') {
			var files = $('#business_logo')[0].files;		
		
			for (var i = 0; i < files.length; i++) {
				formData.append('business_logo[]', files[i]);
			}
		}

		formData.append('cust_id', cust_id);
		formData.append('business_name', business_name);
		formData.append('address', address);
		formData.append('state', state);
		formData.append('city', city);
		formData.append('lat', lat);
		formData.append('long', long);
		formData.append('contact_name', contact_name);
		formData.append('contact_number', contact_number);

		formData.append('id', id);

		$('.loading').removeClass("hidden");
		$.ajax({
			url :base_url+"seller/doUpdate",
		    type : 'POST',
		    data : formData,
		    processData: false,  // tell jQuery not to process the data
		    contentType: false,  // tell jQuery not to set contentType
		    success : function(result) {
		        $('.loading').addClass("hidden");
			 	result=JSON.parse(result);
		 		if(result.status==0){			 			
		 			if(result["not_inseted"]!=''){
		 				var input = $("#business_logo");
        				input.replaceWith(input.val('').clone(true));
        				$("#imgShow").html("");
		 				toastr.error(result["not_inseted"]+" image upload failed .Please upload valid image file.");
		 			}else{
		 				toastr.success(result["message"]);
		 				setTimeout(
						  function() 
						  {
						    window.location=base_url+'seller';
					 	
						  }, 1000);
		 			}
		 			
		 		}else{
		 			if(result["not_inseted"]!=''){
		 				var input = $("#business_logo");
        				input.replaceWith(input.val('').clone(true));
        				$("#imgShow").html("");
		 				toastr.error(result["not_inseted"]+" image upload failed .Please upload valid image file.");
		 			}else{
		 				toastr.error(result["message"]);
				 		if(result["field"]=='to_login'){
				 			window.location=base_url;
				 		}else{
				 			$("#"+result["field"]).focus();
				 		}
		 			}
			 		
					
			 	}
			 	//console.log(result);
		    }
		});
	}
}*/



function doSellerStatusChange(value, id) {
	toastr.clear();
	var base_url = $("#base_url").val();
	if (confirm("Do you want to change status of this Profile?")) {
		$('.loading').removeClass("hidden");
		$.ajax({
			url: base_url + "seller/doSellerStatusChange",
			data: { id: id, status: value },
			method: "post",
			success: function (result) {
				$('.loading').addClass("hidden");
				result = JSON.parse(result);
				if (result.status == 0) {
					toastr.success(result["message"]);
					/*setTimeout(
				  function() 
				  {
					window.location=base_url+'seller';
			 	
				  }, 1000);*/
				} else {
					toastr.error(result["message"]);
				}

			}
		});
	}
}
