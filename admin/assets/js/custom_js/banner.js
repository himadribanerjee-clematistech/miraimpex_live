function loadFile(){
                                
    var imgfilename = document.getElementById('chImg');

    imgfilename.src = URL.createObjectURL(event.target.files[0]);
};


function editBanner(id) {
	var base_url=$("#base_url").val();
	window.location=base_url+"banner/doEdit/"+id;
}


function updateBanner(){
	var base_url=$("#base_url").val();

	var id=$("#id").val();
	var image=$("#image").val();
	var img_old=$("#img_old").val();

	toastr.clear();
	
	if($.trim(image)=='' && $.trim(img_old)=='' ){
		toastr.error('Please Select Banner Image.');
		$("#image").focus();
		return false;
	}else{

		var formData = new FormData();
		formData.append('image', $('#image')[0].files[0]);
		formData.append('id', id);

		$('.loading').removeClass("hidden");
		$.ajax({
			url : base_url+"banner/doUpdate",
			type : 'POST',
			data : formData,
			processData: false,  // tell jQuery not to process the data
			contentType: false,  // tell jQuery not to set contentType
			success : function(result) {
		        $('.loading').addClass("hidden");
			 	result=JSON.parse(result);
			 		if(result.status==0){
			 			
			 			toastr.success(result["message"]);
			 			setTimeout(
						  function() 
						  {
						    window.location=base_url+'banner';
					 	
						  }, 1000);
			 		}else{
				 		toastr.error(result["message"]);
				 		if(result["field"]=='to_login'){
				 			window.location=base_url;
				 		}else{
				 			$("#"+result["field"]).focus();
				 		}
						
				 	}
		       }
		});
	}
}



function doStatusChange(value, id){
	toastr.clear();
	var base_url=$("#base_url").val();
	if(confirm("Do you want to change status of this Banner?")){
		$('.loading').removeClass("hidden");
		$.ajax({
			url: base_url+"banner/doStatusChange",
			data: {id: id, status: value},
			method:"post",
			success: function(result){
			 	$('.loading').addClass("hidden");
			 	result=JSON.parse(result);
		 		if(result.status==0){
		 			toastr.success(result["message"]);
		 			/*setTimeout(
					  function() 
					  {
					    window.location=base_url+'banner';
				 	
					  }, 1000);*/
		 		}else{
			 		toastr.error(result["message"]);						
			 	}
	    	
	  		}
	 	 });
	}
}