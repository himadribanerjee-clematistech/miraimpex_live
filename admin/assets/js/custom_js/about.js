$(document).ready(function(){
	CKEDITOR.replace('details');
});

function addAboutDetails(){
	var base_url=$("#base_url").val();

	var code=$("#code").val();
	var name=$("#name").val();
	var details = CKEDITOR.instances['details'].getData();
	
	toastr.clear();

	if($.trim(name)=='' ){
		toastr.error('Please Enter About Title.');
		$("#name").focus();
		return false;
	}
	else if($.trim(details)=='' ){
		toastr.error('Please Enter About Details.');
		$("#details").focus();
		return false;
	}
	else{
		$('.loading').removeClass("hidden");

		$.ajax({
			url: base_url+"about/doAddAbout",
			data: {
				'code': code,
				'name': name,
				'details': details
			},
			method:"post",
			success: function(result){
				$('.loading').addClass("hidden");
				result=JSON.parse(result);
				if(result.status==0){
					toastr.success(result["message"]);
					setTimeout(
					  function() 
					  {
					    window.location=base_url+'about';
				 	
					  }, 1000
					);
				}else{
		 			toastr.error(result["message"]);	
		 			$("#name").focus();			
				}	    	
	  		}
	 	});
	}
}