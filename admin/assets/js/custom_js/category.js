


function loadFile(){
                                    
    var imgfilename = document.getElementById('chImg');

    imgfilename.src = URL.createObjectURL(event.target.files[0]);
};


function addCatagoryDetails(){
	var base_url=$("#base_url").val();
	var cat_name=$("#cat_name").val();

	var uom_id = $("select[name='uom_id[]']").map(function(){return $(this).val();}).get();
	
	var image=$("#image").val();
	
	toastr.clear();
	
	if($.trim(cat_name)=='' ){
		toastr.error('Please Enter Category Name.');
		$("#cat_name").focus();
		return false;
	}else if($.trim(image)=='' ){
		toastr.error('Please Select Category Image.');
		$("#image").focus();
		return false;
	}else{
		var formData = new FormData();
		formData.append('image', $('#image')[0].files[0]);
		formData.append('cat_name', cat_name);
		formData.append('uom_id', uom_id);
		///////////////
		$('.loading').removeClass("hidden");
		$.ajax({
		       url :base_url+"category/doAddCat",
		       type : 'POST',
		       data : formData,
		       processData: false,  // tell jQuery not to process the data
		       contentType: false,  // tell jQuery not to set contentType
		       success : function(result) {
		           $('.loading').addClass("hidden");
			 	result=JSON.parse(result);
			 		if(result.status==0){
			 			toastr.success(result["message"]);
			 			setTimeout(
						  function() 
						  {
						    window.location=base_url+'category';
					 	
						  }, 1000);
			 		}else{
				 		toastr.error(result["message"]);
				 		if(result["field"]=='to_login'){
				 			window.location=base_url;
				 		}else{
				 			$("#"+result["field"]).focus();
				 		}
						
				 	}
		       }
		});
	}
}

function UpdateCatagoryDetails(){
	var base_url=$("#base_url").val();
	var cat_name=$("#cat_name").val();
	var id=$("#id").val();
	var image=$("#image").val();
	var img_old=$("#img_old").val();
	var is_approved = $('#is_approved').val();
	var approved_by = $('#approved_by').val();

	var uom_id = $("select[name='uom_id[]']").map(function(){return $(this).val();}).get();

	toastr.clear();
	if($.trim(cat_name)=='' ){
		toastr.error('Please Enter Category Name.');
		$("#cat_name").focus();
		return false;
	}else if($.trim(image)=='' &&  $.trim(img_old)==''){
		toastr.error('Please Select Category Image.');
		$("#image").focus();
		return false;
	}else{
		var formData = new FormData();
		formData.append('image', $('#image')[0].files[0]);
		formData.append('cat_name', cat_name);
		formData.append('id', id);
		formData.append('is_approved', is_approved);
		formData.append('approved_by', approved_by);
		formData.append('uom_id', uom_id);
		///////////////
		$('.loading').removeClass("hidden");
		$.ajax({
		       url :base_url+"category/UpdateCatDetails",
		       type : 'POST',
		       data : formData,
		       processData: false,  // tell jQuery not to process the data
		       contentType: false,  // tell jQuery not to set contentType
		       success : function(result) {
		           $('.loading').addClass("hidden");
			 		result=JSON.parse(result);
			 		if(result.status==0){
			 			toastr.success(result["message"]);
			 			setTimeout(
						  function() 
						  {
						    window.location=base_url+'category';
					 	
						  }, 1000);
			 		}else{
				 		toastr.error(result["message"]);
				 		if(result["field"]=='to_login'){
				 			window.location=base_url;
				 		}else{
				 			$("#"+result["field"]).focus();
				 		}
						
				 	}
		       }
		});
	}
}	
function doCatEdit(id){
	var base_url=$("#base_url").val();
	window.location=base_url+"category/doCatEdit/"+id;
}

function doCatStatusChange(value,id){
	toastr.clear();
	var base_url=$("#base_url").val();
	if(confirm("Do you want to change status of this Category?")){
		$('.loading').removeClass("hidden");
		$.ajax({
			url: base_url+"category/doCatStatusChange",
			data:"id="+id+'&actve='+value,
			method:"post",
			success: function(result){
			 	$('.loading').addClass("hidden");
			 	result=JSON.parse(result);
		 		if(result.status==0){
		 			toastr.success(result["message"]);
		 			/*setTimeout(
					  function() 
					  {
					    window.location=base_url+'category'+location.search;
				 	
					  }, 1000);*/
		 		}else{
			 		toastr.error(result["message"]);						
			 	}
	    	
	  		}
	 	 });
	}
}


function doApproveChange(value,id){
	toastr.clear();
	var base_url=$("#base_url").val();
	if(confirm("Do you want to change Approval status of this Category?")){
		$('.loading').removeClass("hidden");
		$.ajax({
			url: base_url+"category/doApproveChange",
			data:"id="+id+'&is_approved='+value,
			method:"post",
			success: function(result){
			 	$('.loading').addClass("hidden");
			 	result=JSON.parse(result);
		 		if(result.status==0){
		 			toastr.success(result["message"]);
		 			/*setTimeout(
					  function() 
					  {
					    window.location=base_url+'category'+location.search;
				 	
					  }, 1000);*/
		 		}else{
			 		toastr.error(result["message"]);						
			 	}
	    	
	  		}
	 	 });
	}
}


function doCatDelete(id) {
	toastr.clear();
	var base_url=$("#base_url").val();
	if(confirm("Do you want to delete this Category?")){
		$('.loading').removeClass("hidden");
		$.ajax({
			url: base_url+"category/doCatDelete",
			data:"id="+id,
			method:"post",
			success: function(result){
			 	$('.loading').addClass("hidden");
			 	result=JSON.parse(result);
		 		if(result.status==0){
		 			toastr.success(result["message"]);
		 			setTimeout(
					  function() 
					  {
					    window.location=base_url+'category';
				 	
					  }, 1000);
		 		}else{
			 		toastr.error(result["message"]);						
			 	}
	    	
	  		}
	 	 });
	}
}