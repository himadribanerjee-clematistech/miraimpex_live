function doStatusChange(value, id){ 
    toastr.clear();
    var base_url = $("#base_url").val();
    if(confirm("Do you want to change status of this Profile ? ")){
        $('.loading').removeClass("hidden");

        $.ajax({
            url: base_url+"reportincident/doStatusChange",
            data: {id: id, status: value},
            method:"post",
            success: function(result){
                $('.loading').addClass("hidden");
                result=JSON.parse(result);
                if(result.status==0){
                    toastr.success(result["message"]);
                }else{
                    toastr.error(result["message"]);                        
                }            
            }
        });
    }
} 



$('.accusedFor').click(function(){
    toastr.clear();
    var base_url = $('#base_url').val();

    var type = $(this).data('type');
    var reference = $(this).data('ref-id');

    $.ajax({
        url: base_url + 'reportincident/accusedfor',
        type: 'POST',
        data: { type: type, reference_id: reference },
        dataType: 'JSON',
        success: function(response) {      
            //console.log(response);

            if (response.type == 'rfq') {
                $('#type_rfq').html('RFQ');
        
                $('#quantity_rfq').html(response.data.quantity);
                $('#message_rfq').html(response.data.message);
                $('#other_requerment').html(response.data.other_requerment);

                if (response.data.expiry_date == '') {
                    $('#expiry_date_col').hide();
                }
                else {
                    $('#expiry_date_col').show();
                    $('#expiry_date').html(response.data.expiry_date);
                }

        
                $('#item_name_rfq').html(response.data.item_name);
                $('#species_name').html(response.data.species_name);
                $('#UOM').html(response.data.UOM);
                $('#customer').html(response.data.customer);
                $('#category').html(response.data.category);

                if (response.data.subcategory != null) {
                    $('#subcategory').html(response.data.subcategory);
                }
                else {
                    $('#subcategory').html('NA');
                }

                $('#imgCol').html('');

                $.each(response.data.image, function(key, val) {

                    if (val.img_id != null) {
                        $('#imgRow').show();

                        $('#imgCol').append("<div style='display: inline-block; padding-right: 7px;' > <img src='" + base_url + "uploads/rfq/" + val.image + "' style='border: 2px dotted #ccc; border-radius: 2px; height: 130px; width: 120px'/> </div>");

                    }
                    else {
                        $('#imgRow').hide();
                    }

                });

                $('#rfqModal').modal('show');
            }
            else if (response.type == 'enq') { //enq
                $('#type_enq').html('Enquiry'); 

                if (response.data.item_name == null) {
                    $('.item').hide();
                }
                else {
                    $('.item').show();
                    $('#item_name_enq').html(response.data.item_name);
                }

                $('#from_cust').html(response.data.from_cust);
                $('#to_cust').html(response.data.to_cust);
                $('#quantity_enq').html(response.data.quantity);
                $('#date').html(response.data.date);
                $('#message_enq').html(response.data.message);

                $('#enqModal').modal('show');
            }
            else {
                toastr.error('Sorry! No RFQ or Enquiry has found!');
            }
        }
    });


});

$('.viewReport').click(function(){
    var base_url = $('#base_url').val();

    var to_id = $(this).data('to-id');
    var from_id = $(this).data('from-id');
    
    $.ajax({
        url: base_url + 'reportincident/accusedreport',
        type: 'POST',
        data: { to_id: to_id, from_id: from_id },
        dataType: 'JSON',
        success: function(response) {      
            //console.log(response);

            $('#accused_by').html(response.data.accused_by);
            $('#created_date').html(response.data.created_date);
            $('#message_report').html(response.data.message);

            if (response.data.images != null) {
                $('#imgReportRow').show();
                $('#imgReportCol').empty('');

                var images = JSON.parse(response.data.images);               

                $.each(images, function(key, val) {

                    $('#imgReportCol').append("<div style='display: inline-block; padding-right: 7px;' > <img src='" + base_url + "uploads/reportIncident/" + val.image + "' style='border: 2px dotted #ccc; border-radius: 2px; height: 130px; width: 120px'/> </div>");

                });
            }
            else {
                $('#imgReportRow').hide();
            }

            $('#reportModal').modal('show');
            
        }
    });


});