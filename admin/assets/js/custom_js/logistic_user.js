function addLogisticUserDetails(){
	var base_url=$("#base_url").val();

	var name=$("#name").val();
	var email=$("#email").val();
	var mobile=$("#mobile").val();

	var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i
	toastr.clear();
		
	if($.trim(name)=='' ){
		toastr.error('Please Enter Name.');
		$("#Name").focus();
		return false;
	}else if($.trim(email)=='' ){
		toastr.error('Please Enter Email Id.');
		$("#email").focus();
		return false;
	}else if(!pattern.test(email) ){
		toastr.error('Please Enter Valid Email Id.');
		$("#email").focus();
		return false;
	}else if($.trim(mobile)=='' ){
		toastr.error('Please Enter Mobile No.');
		$("#mobile").focus();
		return false;
	}else{
		$('.loading').removeClass("hidden");
		$.ajax({
			url: base_url+"logistic_user/doAddUser",
			data:$("#addlogisticuserfrm").serializeArray(),
			method:"post",
			 success: function(result){
			 	$('.loading').addClass("hidden");
			 	result=JSON.parse(result);
			 		if(result.status==0){
			 			toastr.success(result["message"]);
			 			setTimeout(
						  function() 
						  {
						    window.location=base_url+'logistic-user';
					 	
						  }, 1000);
			 		}else{
				 		toastr.error(result["message"]);
				 		if(result["field"]=='to_login'){
				 			window.location=base_url;
				 		}else{
				 			$("#"+result["field"]).focus();
				 		}
						
				 	}
	    	
	  		}
	 	 });
	}
}


function doLogisticUserEdit(id){
	var base_url=$("#base_url").val();
	window.location=base_url+"logistic_user/doUserEdit/"+id;
}

function UpdateLogisticUserDetails(){
	var base_url=$("#base_url").val();

	var name=$("#name").val();
	var email=$("#email").val();
	var mobile=$("#mobile").val();

	var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i
	toastr.clear();
		
	if($.trim(name)=='' ){
		toastr.error('Please Enter Name.');
		$("#Name").focus();
		return false;
	}else if($.trim(email)=='' ){
		toastr.error('Please Enter Email Id.');
		$("#email").focus();
		return false;
	}else if(!pattern.test(email) ){
		toastr.error('Please Enter Valid Email Id.');
		$("#email").focus();
		return false;
	}else if($.trim(mobile)=='' ){
		toastr.error('Please Enter Mobile No.');
		$("#mobile").focus();
		return false;
	}
	else{
		$('.loading').removeClass("hidden");
		$.ajax({
			url: base_url+"logistic_user/UpdateUserDetails",
			data:$("#edituserfrm").serializeArray(),
			method:"post",
			 success: function(result){
			 	$('.loading').addClass("hidden");
			 	result=JSON.parse(result);
			 		if(result.status==0){
			 			toastr.success(result["message"]);
			 			setTimeout(
						  function() 
						  {
						    window.location=base_url+'logistic-user';
					 	
						  }, 1000);
			 		}else{
				 		toastr.error(result["message"]);
				 		if(result["field"]=='to_login'){
				 			window.location=base_url;
				 		}else{
				 			$("#"+result["field"]).focus();
				 		}
						
				 	}
	    	
	  		}
	 	 });
	}
}

function doLogisticUserStatusChange(value,id){
	toastr.clear();
	var base_url=$("#base_url").val();
	if(confirm("Do you want to change status of this user?")){
		$('.loading').removeClass("hidden");
		$.ajax({
			url: base_url+"logistic_user/douserStatusChange",
			data:"id="+id+'&status='+value,
			method:"post",
			success: function(result){
			 	$('.loading').addClass("hidden");
			 	result=JSON.parse(result);
		 		if(result.status==0){
		 			toastr.success(result["message"]);
		 		}else{
			 		toastr.error(result["message"]);						
			 	}	    	
	  		}
	 	 });
	}
}