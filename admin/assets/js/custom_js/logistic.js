$(document).ready(function() {});



$('.details').click(function(){
    var base_url = $('#base_url').val();
    var iid = $(this).data('id'); 

    // console.log(iid);

    $.ajax({
        url: base_url + 'logistic/Details',
        type: 'POST',
        data: {id: iid},
        dataType: 'JSON',
        success: function(response) {
            // console.log(response.data);
            
            $('#partner').html(response.data.name);
            $('#email').html(response.data.email);
            $('#date').html(response.data.date);
            $('#port_of_origin').html(response.data.port_of_origin);
            $('#port_of_distribution').html(response.data.port_of_distribution);
            $('#item').html(response.data.item);
            $('#qty').html(response.data.qty);
            $('#message').html(response.data.message);

            $('#viewModal').modal('show');
        }
    });


});



/*function doStatusChange(value,id){
    toastr.clear();
    var base_url=$("#base_url").val();
    if(confirm("Do you want to change status of this Item?")){
        $('.loading').removeClass("hidden");
        $.ajax({
            url: base_url+"lclsplr/doStatusChange",
            data:"id="+id+'&active='+value,
            method:"post",
            success: function(result){
                $('.loading').addClass("hidden");
                result=JSON.parse(result);
                if(result.status==0){
                    toastr.success(result["message"]);
                }else{
                    toastr.error(result["message"]);                        
                }
            
            }
         });
    }
}*/