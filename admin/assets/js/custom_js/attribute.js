function addAttribute() {
	var base_url=$("#base_url").val();

	var name_en = $("#name_en").val();
	var name_fr = $("#name_fr").val();
	var type = $("#type").val();
	var active = $("#active").val();	

	toastr.clear();

	if($.trim(name_en)=='' ){
		toastr.error('Please Enter Name.');
		$("#name_en").focus();
		return false;
	}
	else if($.trim(name_fr)=='' ){
		toastr.error('Please Enter Name in French.');
		$("#name_fr").focus();
		return false;
	}
	else if($.trim(type)=='' ){
		toastr.error('Please Enter Type.');
		$("#type").focus();
		return false;
	}
	else {
		var formData = $('#addattributefrm').serializeArray();

		$('.loading').removeClass("hidden");

		$.ajax({
			url :base_url+"attribute/doAddAttribute",
	       	type : 'POST',
	       	data : formData,
	       	success : function(result) {
				$('.loading').addClass("hidden");
				result=JSON.parse(result);
				if(result.status==0){
					toastr.success(result["message"]);
					setTimeout(
				  		function() {
				    		window.location=base_url+'attribute';
				    	}, 1000);
				}else{
					toastr.error(result["message"]);
					if(result["field"]=='to_login'){
						window.location=base_url;
					}else{
						$("#"+result["field"]).focus();
					}
				}
		    }
		});
	}

}

function doAttrEdit(id){
	var base_url=$("#base_url").val();
	window.location=base_url+"attribute/doAttributeEdit/"+id;
}

function updateAttribute(){
	var base_url=$("#base_url").val();

	var id = $("#id").val();
	var name_en = $("#name_en").val();
	var name_fr = $("#name_fr").val();
	var type = $("#type").val();
	var active = $("#active").val();	

	toastr.clear();

	if($.trim(name_en)=='' ){
		toastr.error('Please Enter Name.');
		$("#name_en").focus();
		return false;
	}
	else if($.trim(name_fr)=='' ){
		toastr.error('Please Enter Name in French.');
		$("#name_fr").focus();
		return false;
	}
	else if($.trim(type)=='' ){
		toastr.error('Please Enter Type.');
		$("#type").focus();
		return false;
	}
	else{
		$('.loading').removeClass("hidden");
		$.ajax({
			url: base_url+"attribute/updateAttribute",
			data:$("#editattributefrm").serializeArray(),
			method:"post",
			success: function(result){
				$('.loading').addClass("hidden");
				result=JSON.parse(result);
				if(result.status==0){
					toastr.success(result["message"]);
					setTimeout(
					  function() 
					  {
					    window.location=base_url+'attribute';
				 	
					  }, 1000
					);
				}else{
		 			toastr.error(result["message"]);	
		 			$("#attribute_name").focus();			
				}	
	  		}
	 	});
	}
}


function doAttrStatusChange(value,id){
	toastr.clear();
	var base_url=$("#base_url").val();
	if(confirm("Do you want to change status of this Attribute?")){
		$('.loading').removeClass("hidden");
		$.ajax({
			url: base_url+"attribute/doAttrStatusChange",
			data:"id="+id+'&active='+value,
			method:"post",
			success: function(result){
			 	$('.loading').addClass("hidden");
			 	result=JSON.parse(result);
		 		if(result.status==0){
		 			toastr.success(result["message"]);
		 			/*setTimeout(
					  function() 
					  {
					    window.location=base_url+'attribute';
				 	
					  }, 1000);*/
		 		}else{
			 		toastr.error(result["message"]);						
			 	}
	    	
	  		}
	 	 });
	}
}


function doAttrTypeChange(value,id){
	toastr.clear();
	var base_url=$("#base_url").val();
	if(confirm("Do you want to change type of this Attribute?")){
		$('.loading').removeClass("hidden");
		$.ajax({
			url: base_url+"attribute/doAttrTypeChange",
			data:"id="+id+'&type='+value,
			method:"post",
			success: function(result){
			 	$('.loading').addClass("hidden");
			 	result=JSON.parse(result);
		 		if(result.status==0){
		 			toastr.success(result["message"]);
		 			/*setTimeout(
					  function() 
					  {
					    window.location=base_url+'attribute';
				 	
					  }, 1000);*/
		 		}else{
			 		toastr.error(result["message"]);						
			 	}
	    	
	  		}
	 	 });
	}
}