function addVariance() {
	var base_url=$("#base_url").val();

	var name_en = $("#name_en").val();
	var name_fr = $("#name_fr").val();
	var type = $("#type").val();
	var active = $("#active").val();	

	toastr.clear();

	if($.trim(name_en)=='' ){
		toastr.error('Please Enter Name.');
		$("#name_en").focus();
		return false;
	}
	else if($.trim(name_fr)=='' ){
		toastr.error('Please Enter Name in French.');
		$("#name_fr").focus();
		return false;
	}
	else if($.trim(type)=='' ){
		toastr.error('Please Enter Type.');
		$("#type").focus();
		return false;
	}
	else {
		var formData = $('#addvariancefrm').serializeArray();

		$('.loading').removeClass("hidden");

		$.ajax({
			url :base_url+"variance/doAddVariance",
	       	type : 'POST',
	       	data : formData,
	       	success : function(result) {
				$('.loading').addClass("hidden");
				result=JSON.parse(result);
				if(result.status==0){
					toastr.success(result["message"]);
					setTimeout(
				  		function() {
				    		window.location=base_url+'variance';
				    	}, 1000);
				}else{
					toastr.error(result["message"]);
					if(result["field"]=='to_login'){
						window.location=base_url;
					}else{
						$("#"+result["field"]).focus();
					}
				}
		    }
		});
	}

}

function doVarEdit(id){
	var base_url=$("#base_url").val();
	window.location=base_url+"variance/doVarianceEdit/"+id;
}

function updateVar(){
	var base_url=$("#base_url").val();

	var id = $("#id").val();
	var name_en = $("#name_en").val();
	var name_fr = $("#name_fr").val();
	var type = $("#type").val();
	var active = $("#active").val();	

	toastr.clear();

	if($.trim(name_en)=='' ){
		toastr.error('Please Enter Name.');
		$("#name_en").focus();
		return false;
	}
	else if($.trim(name_fr)=='' ){
		toastr.error('Please Enter Name in French.');
		$("#name_fr").focus();
		return false;
	}
	else if($.trim(type)=='' ){
		toastr.error('Please Enter Type.');
		$("#type").focus();
		return false;
	}
	else{
		$('.loading').removeClass("hidden");
		$.ajax({
			url: base_url+"variance/updateVar",
			data:$("#editvarfrm").serializeArray(),
			method:"post",
			success: function(result){
				$('.loading').addClass("hidden");
				result=JSON.parse(result);
				if(result.status==0){
					toastr.success(result["message"]);
					setTimeout(
					  function() 
					  {
					    window.location=base_url+'variance';
				 	
					  }, 1000
					);
				}else{
		 			toastr.error(result["message"]);	
		 			$("#attribute_name").focus();			
				}	
	  		}
	 	});
	}
}


function doVarStatusChange(value,id){
	toastr.clear();
	var base_url=$("#base_url").val();
	if(confirm("Do you want to change status of this Variance?")){
		$('.loading').removeClass("hidden");
		$.ajax({
			url: base_url+"variance/doVarStatusChange",
			data:"id="+id+'&active='+value,
			method:"post",
			success: function(result){
			 	$('.loading').addClass("hidden");
			 	result=JSON.parse(result);
		 		if(result.status==0){
		 			toastr.success(result["message"]);
		 			/*setTimeout(
					  function() 
					  {
					    window.location=base_url+'variance';
				 	
					  }, 1000);*/
		 		}else{
			 		toastr.error(result["message"]);						
			 	}
	    	
	  		}
	 	 });
	}
}


function doVarTypeChange(value,id){
	toastr.clear();
	var base_url=$("#base_url").val();
	if(confirm("Do you want to change type of this Variance?")){
		$('.loading').removeClass("hidden");
		$.ajax({
			url: base_url+"variance/doVarTypeChange",
			data:"id="+id+'&type='+value,
			method:"post",
			success: function(result){
			 	$('.loading').addClass("hidden");
			 	result=JSON.parse(result);
		 		if(result.status==0){
		 			toastr.success(result["message"]);
		 			/*setTimeout(
					  function() 
					  {
					    window.location=base_url+'variance';
				 	
					  }, 1000);*/
		 		}else{
			 		toastr.error(result["message"]);						
			 	}
	    	
	  		}
	 	 });
	}
}