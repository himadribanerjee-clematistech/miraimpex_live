function addserviceuomDetails(){
	var base_url=$("#base_url").val();
	var uom_name=$("#uom_name").val();
	
	toastr.clear();

	if($.trim(uom_name)=='' ){
		toastr.error('Please Enter Unit of Measurement Name.');
		$("#uom_name").focus();
		return false;
	}
	else{
		$('.loading').removeClass("hidden");
		$.ajax({
			url: base_url+"serviceuom/doAddUom",
			data:$("#adduomfrm").serializeArray(),
			method:"post",
			success: function(result){
				$('.loading').addClass("hidden");
				result=JSON.parse(result);
				if(result.status==0){
					toastr.success(result["message"]);
					setTimeout(
					  function() 
					  {
					    window.location=base_url+'serviceuom';
				 	
					  }, 1000
					);
				}else{
		 			toastr.error(result["message"]);	
		 			$("#uom_name").focus();			
				}	    	
	  		}
	 	});
	}
}

function doUomEdit(id){
	var base_url=$("#base_url").val();
	window.location=base_url+"serviceuom/doUomEdit/"+id;
}

function UpdateServiceuomDetails(){
	var base_url=$("#base_url").val();
	var uom_name=$("#uom_name").val();
	
	toastr.clear();

	if($.trim(uom_name)=='' ){
		toastr.error('Please Enter Unit of Measurement Name.');
		$("#uom_name").focus();
		return false;
	}
	else{
		$('.loading').removeClass("hidden");
		$.ajax({
			url: base_url+"serviceuom/UpdateuomDetails",
			data:$("#edituomfrm").serializeArray(),
			method:"post",
			success: function(result){
				$('.loading').addClass("hidden");
				result=JSON.parse(result);
				if(result.status==0){
					toastr.success(result["message"]);
					setTimeout(
					  function() 
					  {
					    window.location=base_url+'serviceuom';
				 	
					  }, 1000
					);
				}else{
		 			toastr.error(result["message"]);	
		 			$("#uom_name").focus();			
				}	    	
	  		}
	 	});
	}
}


function doUomStatusChange(value,id){
	toastr.clear();
	var base_url=$("#base_url").val();
	if(confirm("Do you want to change status of this Unit of Measurement?")){
		$('.loading').removeClass("hidden");
		$.ajax({
			url: base_url+"serviceuom/doUomStatusChange",
			data:"id="+id+'&active='+value,
			method:"post",
			success: function(result){
			 	$('.loading').addClass("hidden");
			 	result=JSON.parse(result);
		 		if(result.status==0){
		 			toastr.success(result["message"]);
		 			/*setTimeout(
					  function() 
					  {
					    window.location=base_url+'uom';
				 	
					  }, 1000);*/
		 		}else{
			 		toastr.error(result["message"]);						
			 	}
	    	
	  		}
	 	 });
	}
}