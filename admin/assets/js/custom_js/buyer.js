function loadFile(){
                                
    var imgfilename = document.getElementById('chImg');

    imgfilename.src = URL.createObjectURL(event.target.files[0]);
};


function addBuyer(){
	toastr.clear();
	var base_url=$("#base_url").val();

	var pattern =/^-?([1-8]?[1-9]|[1-9]0)\.{1}\d{1,6}$/i;

	var cust_id=$("#cust_id").val();
	var business_name=$("#business_name").val();
	var address=$("#address").val();
	var state=$("#state").val();
	var city=$("#city").val();
	var lat=$("#lat").val();
	var long=$("#long").val();
	var business_logo=$("#business_logo").val();

	if($.trim(cust_id)=='' ){
		toastr.error('Please Choose Customer Name.');
		$("#cust_id").focus();
		return false;
	}
	else if($.trim(business_name)=='' ){
		toastr.error('Please Enter Bussiness Name.');
		$("#business_name").focus();
		return false;
	}
	else if($.trim(address)=='' ){
		toastr.error('Please Enter Address.');
		$("#address").focus();
		return false;
	}
	else if($.trim(state)=='' ){
		toastr.error('Please Enter State.');
		$("#state").focus();
		return false;
	}
	else if($.trim(city)=='' ){
		toastr.error('Please Enter City.');
		$("#city").focus();
		return false;
	}
	else if($.trim(lat)=='' ){
		toastr.error('Please Enter Latitude.');
		$("#lat").focus();
		return false;
	}
	else if (!pattern.test(lat)) {
		toastr.error('Please Enter Valid Latitude.');
		$("#lat").focus();
		return false;
	}
	else if($.trim(long)=='' ){
		toastr.error('Please Enter Longitude.');
		$("#long").focus();
		return false;
	}
	 else if(!pattern.test(long)) {
		toastr.error('Please Enter Valid Longitude.');
		$("#long").focus();
		return false;
	}
	else if($.trim(business_logo)=='' ){
		toastr.error('Please Select Logo.');
		$("#business_logo").focus();
		return false;
	}
	else{
		var formData = new FormData();
		formData.append('business_logo', $('#business_logo')[0].files[0]);
		formData.append('cust_id', cust_id);
		formData.append('business_name', business_name);
		formData.append('address', address);
		formData.append('state', state);
		formData.append('city', city);
		formData.append('lat', lat);
		formData.append('long', long);

		$('.loading').removeClass("hidden");
		$.ajax({
			url :base_url+"buyer/doAddNew",
		    type : 'POST',
		    data : formData,
		    processData: false,  // tell jQuery not to process the data
		    contentType: false,  // tell jQuery not to set contentType
		    success : function(result) {
		        $('.loading').addClass("hidden");
			 	result=JSON.parse(result);
			 		if(result.status==0){
			 			toastr.success(result["message"]);
			 			setTimeout(
						  function() 
						  {
						    window.location=base_url+'buyer';
					 	
						  }, 1000);
			 		}else{
				 		toastr.error(result["message"]);
				 		if(result["field"]=='to_login'){
				 			window.location=base_url;
				 		}else{
				 			$("#"+result["field"]).focus();
				 		}
						
				 	}
		       }
		});
	}
}


function doBuyerEdit(id){
	var base_url=$("#base_url").val();
	window.location=base_url+"buyer/doBuyerEdit/"+id;
}

function editBuyer(){
	var base_url=$("#base_url").val();

	var pattern =/^-?([1-8]?[1-9]|[1-9]0)\.{1}\d{1,6}$/i;

	var id=$("#id").val();
	var cust_id=$("#cust_id").val();
	var business_name=$("#business_name").val();
	var address=$("#address").val();
	var state=$("#state").val();
	var city=$("#city").val();
	var lat=$("#lat").val();
	var long=$("#long").val();
	var business_logo=$("#business_logo").val();
	var img_old=$("#img_old").val();

	if($.trim(cust_id)=='' ){
		toastr.error('Please Choose Customer Name.');
		$("#cust_id").focus();
		return false;
	}
	else if($.trim(business_name)=='' ){
		toastr.error('Please Enter Bussiness Name.');
		$("#business_name").focus();
		return false;
	}
	else if($.trim(address)=='' ){
		toastr.error('Please Enter Address.');
		$("#address").focus();
		return false;
	}
	else if($.trim(state)=='' ){
		toastr.error('Please Enter State.');
		$("#state").focus();
		return false;
	}
	else if($.trim(city)=='' ){
		toastr.error('Please Enter City.');
		$("#city").focus();
		return false;
	}
	else if($.trim(lat)=='' ){
		toastr.error('Please Enter Latitude.');
		$("#lat").focus();
		return false;
	}
	else if (!pattern.test(lat)) {
		toastr.error('Please Enter Valid Latitude.');
		$("#lat").focus();
		return false;
	}
	else if($.trim(long)=='' ){
		toastr.error('Please Enter Longitude.');
		$("#long").focus();
		return false;
	}
	 else if(!pattern.test(long)) {
		toastr.error('Please Enter Valid Longitude.');
		$("#long").focus();
		return false;
	}
	else if($.trim(business_logo)=='' && $.trim(img_old)=='' ){
		toastr.error('Please Select Item Image.');
		$("#business_logo").focus();
		return false;
	}
	else{
		var formData = new FormData();
		formData.append('business_logo', $('#business_logo')[0].files[0]);
		formData.append('cust_id', cust_id);
		formData.append('business_name', business_name);
		formData.append('address', address);
		formData.append('state', state);
		formData.append('city', city);
		formData.append('lat', lat);
		formData.append('long', long);

		formData.append('id', id);

		$('.loading').removeClass("hidden");
		$.ajax({
			url :base_url+"buyer/doUpdateBuyer",
		    type : 'POST',
		    data : formData,
		    processData: false,  // tell jQuery not to process the data
		    contentType: false,  // tell jQuery not to set contentType
		    success : function(result) {
		        $('.loading').addClass("hidden");
			 	result=JSON.parse(result);
			 		if(result.status==0){
			 			toastr.success(result["message"]);
			 			setTimeout(
						  function() 
						  {
						    window.location=base_url+'buyer';
					 	
						  }, 1000);
			 		}else{
				 		toastr.error(result["message"]);
				 		if(result["field"]=='to_login'){
				 			window.location=base_url;
				 		}else{
				 			$("#"+result["field"]).focus();
				 		}
						
				 	}
		       }
		});
	}
}


$('.buyer').click(function(){
        var base_url = $('#base_url').val();
        var iid = $(this).data('id');

        $.ajax({
            url: base_url + 'buyer/getById',
            type: 'POST',
            data: {id: iid},
            dataType: 'JSON',
            success: function(response) {
                // console.log(response.data);
                
                $('#cust_name').html(response.data.cust_name);

                if (response.data.address != '') {
                	$('#address').html(response.data.address);
                }
                else {
                	$('#address').html(' Not Found ');
                }
                
                $('#bussiness_name').html(response.data.business_name);
                $('#b_city').html(response.data.city);

                /*if (response.data.lat != '') {
                    $('#lat').html(response.data.lat);
                }
                else {
                    $('#lat').html('NA');
                }

                if (response.data.long != '') {
                    $('#long').html(response.data.long);
                }
                else {
                    $('#long').html('NA');
                }*/
                
                $('#state').html(response.data.state);
                if (response.data.business_logo != '' && response.data.business_logo!=null) {
                    $('#business_logo').attr('src', base_url + 'uploads/bussiness_logo/' + response.data.business_logo);
                }
                else {
                    $('#business_logo').attr('src', base_url + 'assets/images/no-item-image.png');
                }

                $('#viewModal').modal('show');
            }
        });


    });


function doBuyerStatusChange(value, id){ 
	toastr.clear();
	var base_url=$("#base_url").val();
	if(confirm("Do you want to change status of this Profile?")){
		$('.loading').removeClass("hidden");

		$.ajax({
			url: base_url+"buyer/doBuyerStatusChange",
			data: {id: id, status: value},
			method:"post",
			success: function(result){
			 	$('.loading').addClass("hidden");
			 	result=JSON.parse(result);
		 		if(result.status==0){
		 			toastr.success(result["message"]);
		 		}else{
			 		toastr.error(result["message"]);						
			 	}
	    	
	  		}
	 	 });
	}
}
