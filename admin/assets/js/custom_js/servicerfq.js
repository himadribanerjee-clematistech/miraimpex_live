$('.response').click(function(){
    var base_url = $('#base_url').val();
    var iid = $(this).data('id'); 

    $.ajax({
        url: base_url + 'servicerfq/Quote',
        type: 'POST',
        data: {id: iid},
        dataType: 'JSON',
        success: function(response) {
            //console.log(response.data);
            
            if (response.data.business_name == null) {
                $('#bussiness').html(response.data.customer);
            }
            else {
                $('#bussiness').html(response.data.business_name);
            }
            
            $('#quantity').html(response.data.quantity + ' ' + response.data.uom);
            $('#message').html(response.data.message);

            if (response.data.expiry_date == '') {
                $('#expiry_date_col').hide();
            }
            else {
                $('#expiry_date_col').show();
                $('#expiry_date').html(response.data.expiry);
            }

            
            $('#item_name').html(response.data.item_name);
            $('#UOM').html(response.data.UOM);
            $('#customer').html(response.data.customer);
            $('#category').html(response.data.service_category_name);

            if (response.data.other_requerment != '') {
                $('.note').show();
                $('#other_requerment').html(response.data.other_requerment);
            }
            else {
                $('.note').hide();
            }

            if (response.data.status == 1) {
                $('#status').html('Active').css('color', 'green');
            }
            else {
                $('#status').html('In - Active').css('color', 'red');
            }

            $('#imgCol').html('');

            $.each(response.data.image, function(key, val) {

                if (val.img_id != null) {
                    $('#imgRow').show();

                    $('#imgCol').append("<div style='display: inline-block; padding-right: 7px;' > <img src='" + base_url + "uploads/rfq/" + val.image + "' style='border: 2px dotted #ccc; border-radius: 2px; height: 130px; width: 120px'/> </div>");

                }
                else {
                    $('#imgRow').hide();
                }

            });

            $('#viewModal').modal('show');
        }
    });


});


function doStatusChange(value,id){
    toastr.clear();
    var base_url=$("#base_url").val();
    if(confirm("Do you want to change status of this Item?")){
        $('.loading').removeClass("hidden");
        $.ajax({
            url: base_url+"servicerfq/doStatusChange",
            data:"id="+id+'&active='+value,
            method:"post",
            success: function(result){
                $('.loading').addClass("hidden");
                result=JSON.parse(result);
                if(result.status==0){
                    toastr.success(result["message"]);
                    /*setTimeout(
                      function() 
                      {
                        window.location=base_url+'item'+location.search;
                    
                      }, 1000);*/
                }else{
                    toastr.error(result["message"]);                        
                }
            
            }
         });
    }
}