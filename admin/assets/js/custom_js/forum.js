function loadFile(){
                                    
    var imgfilename = document.getElementById('chImg');

    imgfilename.src = URL.createObjectURL(event.target.files[0]);
};

function addForum() {
	var base_url=$("#base_url").val();

	var title_en = $("#title_en").val();
	var title_fr = $("#title_fr").val();
	var description = $("#description").val();
	var type = $("#type").val();	
	var qty = $("#qty").val();	
	var image=$("#img").val();

	// console.log($('#addForumfrm').serializeArray());

	toastr.clear();

	if($.trim(title_en)=='' ){
		toastr.error('Please Enter Name.');
		$("#title_en").focus();
		return false;
	}
	else if($.trim(title_fr)=='' ){
		toastr.error('Please Enter Name in French.');
		$("#title_fr").focus();
		return false;
	}
	else if($.trim(description)=='' ){
		toastr.error('Please Enter Description.');
		$("#description").focus();
		return false;
	}
	else if($.trim(type)=='' ){
		toastr.error('Please Enter Type.');
		$("#type").focus();
		return false;
	}
	else if($.trim(qty)=='' ){
		toastr.error('Please Enter Quantity.');
		$("#qty").focus();
		return false;
	}
	else {
		var formData = new FormData();
		formData.append('image', $('#img')[0].files[0]);
		formData.append('title_en', title_en);
		formData.append('title_fr', title_fr);
		formData.append('description', description);
		formData.append('type', type);
		formData.append('qty', qty);

		$('.loading').removeClass("hidden");
		$.ajax({
	       url :base_url+"forum/doAddforum",
	       type : 'POST',
	       data : formData,
	       processData: false,  // tell jQuery not to process the data
	       contentType: false,  // tell jQuery not to set contentType
	       success : function(result) {
	           $('.loading').addClass("hidden");
		 	result=JSON.parse(result);
		 		if(result.status==0){
		 			toastr.success(result["message"]);
		 			setTimeout(
					  function() 
					  {
					    window.location=base_url+'forum';
				 	
					  }, 1000);
		 		}else{
			 		toastr.error(result["message"]);
			 		if(result["field"]=='to_login'){
			 			window.location=base_url;
			 		}else{
			 			$("#"+result["field"]).focus();
			 		}
					
			 	}
	       }
		});
	}

}

function doForumEdit(id){
	var base_url=$("#base_url").val();
	window.location=base_url+"forum/doforumEdit/"+id;
}

function updateforum(){
	var base_url=$("#base_url").val();

	var id=$("#id").val();
	var title_en = $("#title_en").val();
	var title_fr = $("#title_fr").val();
	var description = $("#description").val();
	var type = $("#type").val();	
	var qty = $("#qty").val();	
	var image=$("#img").val();
	var img_old=$("#img_old").val();

	toastr.clear();

	if($.trim(title_en)=='' ){
		toastr.error('Please Enter Name.');
		$("#title_en").focus();
		return false;
	}
	else if($.trim(title_fr)=='' ){
		toastr.error('Please Enter Name in French.');
		$("#title_fr").focus();
		return false;
	}
	else if($.trim(description)=='' ){
		toastr.error('Please Enter Description.');
		$("#description").focus();
		return false;
	}
	else if($.trim(type)=='' ){
		toastr.error('Please Enter Type.');
		$("#type").focus();
		return false;
	}
	else if($.trim(qty)=='' ){
		toastr.error('Please Enter Quantity.');
		$("#qty").focus();
		return false;
	}
	else if($.trim(image)=='' &&  $.trim(img_old)==''){
		toastr.error('Please Select Forum News Image.');
		$("#img").focus();
		return false;
	}else{
		var formData = new FormData();
		formData.append('image', $('#img')[0].files[0]);
		formData.append('title_en', title_en);
		formData.append('title_fr', title_fr);
		formData.append('description', description);
		formData.append('type', type);
		formData.append('qty', qty);
		formData.append('id', id);

		// console.log(formData);

		///////////////
		$('.loading').removeClass("hidden");
		$.ajax({
			url: base_url+"forum/updateForum",
			data:formData,
			method:"post",
			processData: false,  // tell jQuery not to process the data
	       	contentType: false,  // tell jQuery not to set contentType
			success: function(result){
				$('.loading').addClass("hidden");
				result=JSON.parse(result);
				if(result.status==0){
					toastr.success(result["message"]);
					setTimeout(
					  function() 
					  {
					    window.location=base_url+'forum';
				 	
					  }, 1000
					);
				}else{
		 			toastr.error(result["message"]);	
		 			$("#forum_name").focus();			
				}	
	  		}
	 	});
	}
}

$('.forum').click(function(){
    var base_url = $('#base_url').val();
    var iid = $(this).data('id');

    toastr.clear();

    $.ajax({
        url: base_url + 'forum/getById',
        type: 'POST',
        data: {id: iid},
        dataType: 'JSON',
        success: function(response) {
            // console.log(response);

            if (response.status == '0') {
            	let data = response.data;
            	// console.log(data);

            	$('#image').attr('src', 'uploads/forum/' + data.image);
            	$('#title_en_view').html(data.title_en);
            	$('#type_').html(data.type);
            	$('#qty').html(data.quantity);
            	$('#date').html(data.date);
            	$('#description').html(data.description);

            	$('#forumModal').modal('show');
            }
            else {
            	toastr.error('Some Problem Happened! Contact Admin.');
            }

            
        }
    });
});


function doForumStatusChange(value,id){
	toastr.clear();
	var base_url=$("#base_url").val();
	if(confirm("Do you want to change status of this Forum?")){
		$('.loading').removeClass("hidden");
		$.ajax({
			url: base_url+"forum/doForumStatusChange",
			data:"id="+id+'&status='+value,
			method:"post",
			success: function(result){
			 	$('.loading').addClass("hidden");
			 	result=JSON.parse(result);
		 		if(result.status==0){
		 			toastr.success(result["message"]);
		 			/*setTimeout(
					  function() 
					  {
					    window.location=base_url+'forum';
				 	
					  }, 1000);*/
		 		}else{
			 		toastr.error(result["message"]);						
			 	}
	    	
	  		}
	 	 });
	}
}