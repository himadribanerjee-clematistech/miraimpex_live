function adduomDetails(){
	var base_url=$("#base_url").val();
	var name_en=$("#name_en").val();
	var name_fr=$("#name_fr").val();
	
	toastr.clear();

	if($.trim(name_en)=='' ){
		toastr.error('Please Enter Unit of Measurement Name.');
		$("#name_en").focus();
		return false;
	}
	else if($.trim(name_fr)=='' ){
		toastr.error('Please Enter Unit of Measurement Name in French.');
		$("#name_fr").focus();
		return false;
	}
	else{
		$('.loading').removeClass("hidden");
		$.ajax({
			url: base_url+"uom/doAddUom",
			data:$("#adduomfrm").serializeArray(),
			method:"post",
			success: function(result){
				$('.loading').addClass("hidden");
				result=JSON.parse(result);
				if(result.status==0){
					toastr.success(result["message"]);
					setTimeout(
					  function() 
					  {
					    window.location=base_url+'uom';
				 	
					  }, 1000
					);
				}else{
		 			toastr.error(result["message"]);	
		 			$("#uom_name").focus();			
				}	    	
	  		}
	 	});
	}
}

function doUomEdit(id){
	var base_url=$("#base_url").val();
	window.location=base_url+"uom/doUomEdit/"+id;
}

function UpdateuomDetails(){
	var base_url=$("#base_url").val();
	var name_en=$("#name_en").val();
	var name_fr=$("#name_fr").val();
	
	toastr.clear();

	if($.trim(name_en)=='' ){
		toastr.error('Please Enter Unit of Measurement Name.');
		$("#name_en").focus();
		return false;
	}
	else if($.trim(name_fr)=='' ){
		toastr.error('Please Enter Unit of Measurement Name in French.');
		$("#name_fr").focus();
		return false;
	}
	else{
		$('.loading').removeClass("hidden");
		$.ajax({
			url: base_url+"uom/UpdateuomDetails",
			data:$("#edituomfrm").serializeArray(),
			method:"post",
			success: function(result){
				$('.loading').addClass("hidden");
				result=JSON.parse(result);
				if(result.status==0){
					toastr.success(result["message"]);
					setTimeout(
					  function() 
					  {
					    window.location=base_url+'uom';
				 	
					  }, 1000
					);
				}else{
		 			toastr.error(result["message"]);	
		 			$("#uom_name").focus();			
				}	    	
	  		}
	 	});
	}
}


function doUomStatusChange(value,id){
	toastr.clear();
	var base_url=$("#base_url").val();
	if(confirm("Do you want to change status of this Unit of Measurement?")){
		$('.loading').removeClass("hidden");
		$.ajax({
			url: base_url+"uom/doUomStatusChange",
			data:"id="+id+'&active='+value,
			method:"post",
			success: function(result){
			 	$('.loading').addClass("hidden");
			 	result=JSON.parse(result);
		 		if(result.status==0){
		 			toastr.success(result["message"]);
		 			/*setTimeout(
					  function() 
					  {
					    window.location=base_url+'uom';
				 	
					  }, 1000);*/
		 		}else{
			 		toastr.error(result["message"]);						
			 	}
	    	
	  		}
	 	 });
	}
}


/*function doUomDelete(id) {
	toastr.clear();
	var base_url=$("#base_url").val();
	if(confirm("Do you want to delete this Category?")){
		$('.loading').removeClass("hidden");
		$.ajax({
			url: base_url+"uom/doUomDelete",
			data:"id="+id,
			method:"post",
			success: function(result){
			 	$('.loading').addClass("hidden");
			 	result=JSON.parse(result);
		 		if(result.status==0){
		 			toastr.success(result["message"]);
		 			setTimeout(
					  function() 
					  {
					    window.location=base_url+'uom';
				 	
					  }, 1000);
		 		}else{
			 		toastr.error(result["message"]);						
			 	}
	    	
	  		}
	 	 });
	}
}*/