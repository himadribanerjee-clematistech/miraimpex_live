$(document).ready(function() {
    $('#cust_id').select2();
    // $('#supp_id').select2();
});


$('.response').click(function(){
    var base_url = $('#base_url').val();
    var iid = $(this).data('id'); 

    toastr.clear();

    $.ajax({
        url: base_url + 'rfq/Quote',
        type: 'POST',
        data: {id: iid},
        dataType: 'JSON',
        success: function(response) {
            // console.log(response);

            if (response.status == '0') {
                let data = response.data;
                // console.log(data);

                $('#customer').html(data.customer);
                $('#quantity').html(data.quantity);
                $('#UOM').html(data.uom);
                $('#category').html(data.category);

                if (data.image.length > 0) {
                    $('#imgRow').show();
                    $.each(data.image, function(k, v) {
                        $('#imgCol').append("<div style='display: inline-block; padding-right: 7px;' > <img src='" + base_url + "uploads/rfq/" + v.image + "' style='border: 2px dotted #ccc; border-radius: 2px; height: 130px; width: 120px'/> </div>");
                    });
                }
                else {
                    $('#imgRow').hide();
                }

                $('#viewModal').modal('show');
            }
            else {
                toastr.error('Please Contact Administrator!');           
            }
            
        }
    });


});



function doStatusChange(value,id){
    toastr.clear();
    var base_url=$("#base_url").val();
    if(confirm("Do you want to change status of this Item?")){
        $('.loading').removeClass("hidden");
        $.ajax({
            url: base_url+"servicerfq/doStatusChange",
            data:"id="+id+'&active='+value,
            method:"post",
            success: function(result){
                $('.loading').addClass("hidden");
                result=JSON.parse(result);
                if(result.status==0){
                    toastr.success(result["message"]);
                    /*setTimeout(
                      function() 
                      {
                        window.location=base_url+'item'+location.search;
                    
                      }, 1000);*/
                }else{
                    toastr.error(result["message"]);                        
                }
            
            }
         });
    }
}