function addsubcatDetails(){
	var base_url=$("#base_url").val();
	var sub_cat_name=$("#sub_cat_name").val();
	var id_back=$("#id_back").val();
	
	toastr.clear();

	if($.trim(sub_cat_name)=='' ){
		toastr.error('Please Enter Sub Category Name.');
		$("#sub_cat_name").focus();
		return false;
	}
	else{
		$('.loading').removeClass("hidden");
		$.ajax({
			url: base_url+"subcategory/doAddsubCat",
			data:$("#addsubcatfrm").serializeArray(),
			method:"post",
			success: function(result){
				$('.loading').addClass("hidden");
				result=JSON.parse(result);
				if(result.status==0){
					toastr.success(result["message"]);
					setTimeout(
					  function() 
					  {
					    window.location=base_url+'subcategory/index/'+id_back;
				 	
					  }, 1000
					);
				}else{
		 			toastr.error(result["message"]);	
		 			$("#sub_cat_name").focus();			
				}	    	
	  		}
	 	});
	}
}

function UpdateSubCatDetails(){
	var base_url=$("#base_url").val();
	var sub_cat_name=$("#sub_cat_name").val();
	var id_back=$("#id_back").val();
	
	toastr.clear();

	if($.trim(sub_cat_name)=='' ){
		toastr.error('Please Enter Sub Category Name.');
		$("#sub_cat_name").focus();
		return false;
	}
	else{
		$('.loading').removeClass("hidden");
		$.ajax({
			url: base_url+"subcategory/UpdatesubCatDetails",
			data:$("#editsubcatfrm").serializeArray(),
			method:"post",
			success: function(result){
				$('.loading').addClass("hidden");
				result=JSON.parse(result);
				if(result.status==0){
					toastr.success(result["message"]);
					setTimeout(
					  function() 
					  {
					    window.location=base_url+'subcategory/index/'+id_back;
				 	
					  }, 1000
					);
				}else{
		 			toastr.error(result["message"]);	
		 			$("#sub_cat_name").focus();			
				}	    	
	  		}
	 	});
	}
}

function doSubCatStatusChange(value,id,iid){
	toastr.clear();
	var base_url=$("#base_url").val();
	var id_back= iid;
	if(confirm("Do you want to change status of this Sub Category?")){
		$('.loading').removeClass("hidden");
		$.ajax({
			url: base_url+"subcategory/doSubCatStatusChange",
			data:"id="+id+'&actve='+value,
			method:"post",
			success: function(result){
			 	$('.loading').addClass("hidden");
			 	result=JSON.parse(result);
		 		if(result.status==0){
		 			toastr.success(result["message"]);
		 			/*setTimeout(
					  function() 
					  {
					   // window.location=base_url+'subcategory'+location.search;
				 		window.location=base_url+'subcategory/index/'+id_back+location.search;
					  }, 1000);*/
		 		}else{
			 		toastr.error(result["message"]);						
			 	}
	    	
	  		}
	 	 });
	}
}

function doSubCatDelete(id, iid) {
	toastr.clear();

	var base_url=$("#base_url").val();
	var id_back= iid;

	if(confirm("Do you want to delete this Category?")){
		$('.loading').removeClass("hidden");
		$.ajax({
			url: base_url+"subcategory/doSubCatDelete",
			data:"id="+id,
			method:"post",
			success: function(result){
			 	$('.loading').addClass("hidden");
			 	result=JSON.parse(result);
		 		if(result.status==0){
		 			toastr.success(result["message"]);
		 			setTimeout(
					  function() 
					  {
					     window.location=base_url+'subcategory/index/'+id_back;
				 	
					  }, 1000);
		 		}else{
			 		toastr.error(result["message"]);						
			 	}
	    	
	  		}
	 	 });
	}
}