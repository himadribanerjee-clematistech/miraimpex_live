$('.search-suggestion').hide();
        $('#scrh_box').val('');
        $('#srch_value_passed').val('');
        $(document).on('keyup','#scrh_box',function(){     
            if($('#scrh_box').val().length>=2){
                var keyword = $('#scrh_box').val();
        
                $.ajax({
                    url: '{{ url("") }}' + '/search/get_suggession',
                    type: 'POST',
                    data: {'keyword': keyword ,'_token':"{{ csrf_token() }}"},
                    dataType: 'JSON',
                    beforeSend : function(){
                        //$('.loader').show();   
                
                    },
                    success: function (response) {
                        $('#parent-srchres').html('');
                        $('.search-suggestion').show();
                        if (response.status=='success') {
                            console.log(response.data);
                            if(response.data.length >0){
                                response.data.forEach(function(element) {
                                    $('#parent-srchres').append($('<li>',{}).append($('<a>',{text:element.name+" ("+element.type+")",href:'javascript:void(0);',ref_val:element.type,slug_val:element.slug})));
                                });
                            }
                            else {
                                $('#parent-srchres').append($('<li>',{}).append($('<span>',{text:'No data found'})));
                            }
                     
                        }
                    }
                });
            }
        }) ;

        $('#parent-srchres').on('click','a',function(){
            var ref_val = $(this).attr('ref_val');
            var prod_text = $(this).html();
            var slug_val = $(this).attr('slug_val');
            $('#scrh_box').val(prod_text);
            $('#srch_value_passed').val(prod_text);
            $('#srch_value_passed').attr('ref_val',ref_val);
            $('#srch_value_passed').attr('slug_val',slug_val);
            $('.search-suggestion').hide();
        });

        $(document).on('click','#srchBtn',function(e){
            e.preventDefault();
            var srchkey = $('#srch_value_passed').val();
            var srchtype = $('#srch_value_passed').attr('ref_val');
            var srchslug = $('#srch_value_passed').attr('slug_val');
            var uri = "";
            if(srchtype == 'City'){
                uri = '{{ url("") }}' + '/city/'+srchslug;
            }else if(srchtype == 'Country'){
                uri = '{{ url("") }}' + '/country/'+srchslug;
            }else{

            }
            if(uri !=""){
                window.location.href = uri;
            }
        });