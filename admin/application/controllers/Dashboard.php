<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct() {
    	parent::__construct();
    	$this->token_id=get_cookie('token_id'); 
    	if(trim($this->token_id)==''){
    		redirect("login");
    	}
        $this->load->model("userModel");
        
    }
	public function index()
	{
		try {
           $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            
            if(empty($user_details)){
                    $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                    delete_cookie('token_id'); 
                     delete_cookie('useName'); 
                    echo '<script>
                        alert("Unexpected server error.Please try again some time.");
                            window.location="'.base_url().'login";
                    </script>';
            }
            $user_details=current($user_details);
            $data=[]; 
            $data["menu"]="dashboard";
            $data["useName"]=$user_details["Name"];
            set_cookie('useName',$user_details["Name"],'3600');
           
            $data['view_file'] = 'Dashboard';
            view($data);
           
        } catch (Exception $e) {
            $resposne=["status"=>"1","e"=>$e,"message"=>"Unexpected server error.Please try again some time.","field"=>'user_name'];
         // var_dump($e->getMessage());
        }    
	}

    public function logout(){
        $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
         delete_cookie('token_id'); 
         delete_cookie('useName'); 
         redirect("login");
    }
}
