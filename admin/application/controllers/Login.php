<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct() {
    	parent::__construct();
    	$token_id=get_cookie('token_id'); 
    	if(trim($token_id)!=''){
    		redirect("dashboard");
    	}
    	$this->load->model("userModel");
    	
    }
    
	public function index(){
		$this->load->view("login");
	}

	public function doLoginValidate(){
		try { 
			$data=$this->input->post();
			$resposne=["status"=>"0","message"=>"","field"=>''];
			if(!empty($data)){
				if(!(isset($data["user_name"]) && trim($data["user_name"])!=='')){
					$resposne=["status"=>"1","message"=>"Please Enter User Name.","field"=>'user_name'];
				}else if(!(isset($data["password"]) && trim($data["password"])!=='')){
					$resposne=["status"=>"1","message"=>"Please Enter password.","field"=>'password'];
				}else{

					$user_data=$this->userModel->doLoginCheck($data);
					
					if(count($user_data)==1){
						$token=$this->userModel->generateToken();
						$user_data=current($user_data);
						$this->userModel->updateUser(["token_id"=>$token],$user_data);
						set_cookie('token_id',$token,'3600000'); 
						
						$resposne=["status"=>"0","message"=>"Login Successful"];
					}else{
						$resposne=["status"=>"1","message"=>"Wrong User Name Or Password.","field"=>'user_name'];
					}
				}
			}else{
				$resposne=["status"=>"1","message"=>"Invalid credential","field"=>'user_name'];
			}
		} catch (Exception $e) {
		 	$resposne=["status"=>"1","e"=>$e,"message"=>"Unexpected server error.Please try again some time.","field"=>'user_name'];
		 // var_dump($e->getMessage());
		}
		echo json_encode($resposne);
	}
	public function doOTPValidate(){
		try { 
			$data=$this->input->post();
			$resposne=["status"=>"0","message"=>"","field"=>''];
			if(!empty($data)){
				if(!(isset($data["otp_input"]) && trim($data["otp_input"])!=='')){
					$resposne=["status"=>"1","message"=>"Please Enter OTP.","field"=>'otp_input'];
				}else{
					$user_data=$this->userModel->getUserDetailsbyEmailIdOTP($data["otp_input"],$data["forget_input"]);
					if(count($user_data)==1){
						$user_data=current($user_data);
						
					$this->userModel->updateUser(["email_otp"=>''],["id"=>$user_data["user_id"]]);
						$resposne=["status"=>"0","message"=>"OTP matched","field"=>'otp_input',"token"=>base64_encode($user_data["user_id"])];

					}else{
						$resposne=["status"=>"1","message"=>"Wrong OTP.","field"=>'forget_input'];
					}
				}
			}else{
				$resposne=["status"=>"1","message"=>"Invalid credential","field"=>'user_name'];
			}
		} catch (Exception $e) {
		 	$resposne=["status"=>"1","e"=>$e,"message"=>"Unexpected server error.Please try again some time.","field"=>'user_name'];
		 // var_dump($e->getMessage());
		}
		echo json_encode($resposne);
	}
	public function doforgetValidate(){
		try { 
			$data=$this->input->post();
			$resposne=["status"=>"0","message"=>"","field"=>''];
			if(!empty($data)){
				if(!(isset($data["forget_input"]) && trim($data["forget_input"])!=='')){
					$resposne=["status"=>"1","message"=>"Please Enter Email Id.","field"=>'forget_input'];
				}else{

					$user_data=$this->userModel->getUserDetailsbyEmailId($data["forget_input"]);
					
					if(count($user_data)==1){
						$user_data=current($user_data);
						$opt=generateOTP();
						$to = $data["forget_input"];
						$subject = 'Agro Wholesale Forget password OTP';
						$from = 'info@agrowholesale.com';
						$headers  = 'MIME-Version: 1.0' . "\r\n";
						$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
						$headers .= 'From: '.$from."\r\n".
					    'Reply-To: '.$from."\r\n" .
					    'X-Mailer: PHP/' . phpversion();
					    $message = '<html><body style="background:#FFFFFD;" >';
						$message .= '<h1 style="color:#000;">Hi '.$user_data["Name"].'!</h1>';
						$message .= '<div style="color:#000;font-size:18px;background:#e1ffd3;padding:10px;">OTP for Forget Password:<span style="font-weight:bolder;color:#fff;background:#E35300;padding:2px;margin:10px; ">'.$opt.'</span> </div>';
						$message .= '</body></html>';
						
						mail($to, $subject, $message, $headers);
						$this->userModel->updateUser(["email_otp"=>$opt],["id"=>$user_data["user_id"]]);
						$resposne=["status"=>"0","message"=>"OTP Send to your Registered Mail Id","field"=>'forget_input'];

 

					}else{
						$resposne=["status"=>"1","message"=>"Wrong Email Id.","field"=>'forget_input'];
					}
				}
			}else{
				$resposne=["status"=>"1","message"=>"Invalid credential","field"=>'user_name'];
			}
		} catch (Exception $e) {
		 	$resposne=["status"=>"1","e"=>$e,"message"=>"Unexpected server error.Please try again some time.","field"=>'user_name'];
		 // var_dump($e->getMessage());
		}
		echo json_encode($resposne);

	}

	public function doChangePassword(){
		try {	
				$data=$this->input->post();
				$resposne=["status"=>"0","message"=>"","field"=>''];

				if(trim($data["token"])!=''){
					if(!(isset($data["new_password"]) && trim($data["new_password"])!=='')){
						$resposne=["status"=>"1","message"=>"Please Enter New Password.","field"=>'password'];
					}else if(!(isset($data["conf_password"]) && trim($data["conf_password"])!=='')){
						$resposne=["status"=>"1","message"=>"Please Enter Confirm New Password.","field"=>'cpassword'];
					}else if(trim($data["conf_password"])!=trim($data["new_password"])){
						$resposne=["status"=>"1","message"=>"Password Mismatch.","field"=>'cpassword'];
					}else{
						$id=base64_decode($data["token"]);
						$user_data=$this->userModel->getUserDetailsbyUserId($id);
					
						if(count($user_data)!=1){	
							$resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'old_password'];
						}else{
							$password=$this->userModel->passwordGenerate(trim($data["new_password"]));
							$this->userModel->updateUser(["password"=>$password],["id"=>$id] );
							$resposne=["status"=>"0","message"=>"Password Changed Successful"];
						}
					}
				}else{
					$resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
				}
		} catch (Exception $e) {
				$resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'first_name'];

		}

		echo json_encode($resposne);		
	}
}
