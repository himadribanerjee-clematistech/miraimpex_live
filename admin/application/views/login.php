
<!DOCTYPE html>
<html>         
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Login|MiraimpexTrade</title> 
    </head>
    <body>
      <div class="login-wrap customscroll d-flex align-items-center flex-wrap justify-content-center pd-20">
        <div class="login-box bg-white box-shadow pd-30 border-radius-5">
          <img src="<?php echo base_url()?>assets/images/logo.jpg" alt="login" class="login-img" style="max-width: none;">
          
          <!--LOGIN Module-->
          <div id="login">
          <!-- <a class="text-blue-50"><h2 class="text-center mb-30">Login</h2></a> -->
            <form id="loginfrm" role="form" action="javascript:void(0);" method="post" onsubmit="return doLoginValidate();" >

              <div class="input-group custom input-group-lg">
                <input type="text" name="user_name" value="" id="user_name" class=" form-control" autocomplete="off" placeholder="User Name"   />
                <div class="input-group-append custom">
                  <span class="input-group-text"><i class="fa fa-user" aria-hidden="true"></i></span>
                </div>
              </div>

              <div class="input-group custom input-group-lg">
                <input type="password" name="password" value="" id="password" class=" form-control" placeholder="Password" autocomplete="off" />
                <div class="input-group-append custom">
                  <span class="input-group-text"><i class="fa fa-lock" aria-hidden="true"></i></span>
                </div>
              </div>

              <div class="row">
                <div class="col-sm-6">
                  <div class="input-group">
                    <button type="submit" class="btn btn-outline-primary btn-lg btn-block">
                      Login
                    </button>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="forgot-password padding-top-10">
                    <a href="javascript:void(0);" onclick="openforgetpassword();" >Forgot Password</a>
                  </div>
                </div>
              </div>
            </form>
          </div>
           <!--FORGETPASSWORD Module-->
          <div id="forget_pass" class="d-none"  >
            <a class="text-blue-50"><h2 class="text-center mb-30">Forget Password</h2></a>
            <form id="forgetpassfrm" role="form" action="javascript:void(0);" method="post" onsubmit="return doforgetValidate();" >

              <div class="input-group custom input-group-lg">
                <input type="text" name="forget_input" value="" id="forget_input" class="form-control" autocomplete="off" placeholder="Email Id"   />
                  <div class="input-group-append custom">
                    <span class="input-group-text"><i class="fa fa-envelope" aria-hidden="true"></i></span>
                </div>
              </div>
             <div class="row">
                <div class="col-sm-6">
                  <div class="input-group">
                    <button type="submit" class="btn btn-outline-primary btn-lg btn-block">
                      Submit
                    </button>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="forgot-password padding-top-10">
                    <a href="javascript:void(0);"  onclick="openlogin();"   >Login</a>
                  </div>
                </div>
              </div>
            </form>
          </div>
           <!--OTP Module-->
          <div id="otp_pass"  class="d-none"  >
            <a class="text-blue-50"><h2 class="text-center mb-30">OTP </h2></a>
            <form id="forgetpassfrm" role="form" action="javascript:void(0);" method="post" onsubmit="return doOTPValidate();" >

              <div class="input-group custom input-group-lg">
                <input type="text" name="otp_input" value="" id="otp_input" class=" form-control" autocomplete="off" placeholder="OTP"   />
                  <div class="input-group-append custom">
                    <span class="input-group-text"><i class="fa fa-key " aria-hidden="true"></i></span>
                </div>
              </div>
             <div class="row">
                <div class="col-sm-6">
                  <div class="input-group">
                    <button type="submit" class="btn btn-outline-primary btn-lg btn-block">
                      Submit
                    </button>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="forgot-password padding-top-10">
                    <a href="javascript:void(0);"  onclick="openlogin();"   >Login</a>
                  </div>
                </div>
              </div>
            </form>
          </div>
          <!--Change Password Module-->
          <div id="password_set" class="d-none" >
            <a class="text-blue-50"><h2 class="text-center mb-30">Change Password </h2></a>
            <form id="forgetpassfrm" role="form" action="javascript:void(0);" method="post" onsubmit="return doPassWordSet();" >

              <div class="input-group custom input-group-lg">
               <input type="password" name="new_password" value="" id="new_password" class=" form-control" autocomplete="off" placeholder="New Password"   />
                <input type="hidden" name="token" id="token" value="">
                  <div class="input-group-append custom">
                    <span class="input-group-text"><i class="fa fa-lock " aria-hidden="true"></i></span>
                </div>
              </div>
               <div class="input-group custom input-group-lg">
                  <input type="password" name="conf_password" value="" id="conf_password" class=" form-control" autocomplete="off" placeholder="Confirm Password"   />
                  <div class="input-group-append custom">
                    <span class="input-group-text"><i class="fa fa-lock " aria-hidden="true"></i></span>
                </div>
              </div>
             <div class="row">
                <div class="col-sm-6">
                  <div class="input-group">
                    <button type="submit" class="btn btn-outline-primary btn-lg btn-block"  id="passchangesub" >
                      Submit
                    </button>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="forgot-password padding-top-10">
                    <a href="javascript:void(0);"  onclick="openlogin();"   >Login</a>
                  </div>
                </div>
              </div>
            </form>
          </div>



        </div>
      </div>
       <input type="hidden" name="base_url" id="base_url" value="<?php echo base_url(); ?>">
    </body>
</html>


<!-- Google Font -->
<link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
<!-- CSS -->

<link rel="stylesheet" href="<?php echo base_url()?>assets/deskapp-master/vendors/styles/style.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/deskapp-master/src/plugins/datatables/media/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/deskapp-master/src/plugins/datatables/media/css/dataTables.bootstrap4.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/deskapp-master/src/plugins/datatables/media/css/responsive.dataTables.css">
 <link href="<?php echo base_url();?>assets/css/toastr.css" rel="stylesheet">
   <script src="<?php echo base_url();?>assets/js/jquery-3.3.1.min.js"></script>
 <script src="<?php echo base_url();?>assets/js/toastr.min.js"></script>
 <script src="<?php echo base_url();?>assets/js/custom_js/login.js"></script>
