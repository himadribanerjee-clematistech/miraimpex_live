
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Login|Agro Wholesale</title>
        <link href="<?php echo base_url();?>assets/css/font-awesome.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url();?>assets/css/form_element.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url();?>assets/css/admin.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url();?>assets/css/admin-responsive.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url();?>assets/css/toastr.css" rel="stylesheet">
    </head>

    <body class="log_p">
        <div class="content">
            <div class="s_wraper">              
                <div class="logo_log" style=" height: 80px;  text-align: center; font-size: 40PX;">Agro Wholesale</div>

                <form id="fgtpswdfrm" role="form" action="javascript:void(0);" method="post" onsubmit="return doforgotPassword();" >
                <div class="form login_bg">
                    
                    <div class="form-group">
                        <!-- <label class="login_label" for="email" >Email Address</label> -->
                        <input type="text" name="email" value="" id="email" class=" form-control" autocomplete="off" placeholder="User Email Address"   />
                        <?php ?>
                    </div>

                    <br>

                    <div class="row no_pad">
                        <div class="col-12 no_pad">
                            <center>
                                <span style="color: #000;">
                                    -------------------- or -------------------
                                </span>
                            </center>
                        </div>
                    </div> 

                    <br>

                    <div class="form-group">
                        <!-- <label class="login_label"  for="mobile" >Contact Number</label> -->
                        <input type="text" name="contact" value="" id="contact" class=" form-control" placeholder="Mobile Number" autocomplete="off" />
                       
                    </div>
                  
                    <div class="row no_pad">
                        <div class="col-12 no_pad tar">
                            <input type="submit" name="fgtpswd" value="Submit" id="fgtpswd" class="btn_small bg_bttnclr"  />
                        </div>
                    </div>            
                </div>
                </form>
            </div>
        </div>
        <script src="<?php echo base_url();?>assets/js/jquery-1.11.2.min.js"></script>
         <!-- Bootstrap Core JavaScript -->
        <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>

        <script src="<?php echo base_url();?>assets/js/chosen.jquery.js"></script>
        <script src="<?php echo base_url();?>assets/js/bootstrap-datepicker.js"></script>
        <script src="<?php echo base_url();?>assets/js/jquery.nanoscroller.js"></script>
        <script src="<?php echo base_url();?>assets/js/admin-custom.js"></script>
        <script src="<?php echo base_url();?>assets/js/toastr.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/custom_js/login.js"></script>
       
        <input type="hidden" name="base_url" id="base_url" value="<?php echo base_url(); ?>">
        <div class="loading hidden"  >Loading&#8230;</div>


        

    </body>
    <!-- jquery --> 
</html>
