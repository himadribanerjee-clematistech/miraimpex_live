
<div class="left-side-bar">
    <div class="brand-logo" style="background: none;">
        <a href="<?php echo base_url();?>">
           <img src="<?php echo base_url()?>assets/images/logo.jpg" alt="Agro Wholesale" class="login-img" style="max-width: none;">
        </a>
    </div>
    <div style="" class="d-block d-md-none">
        <h5 style="margin:20px 0 0 30px" class="text-orange-50"></h5>
    </div>
    <div class="menu-block customscroll">
        <div class="sidebar-menu">
            <ul id="accordion-menu">
                
                <li>
                    <a href="<?php echo base_url();?>dashboard" class="dropdown-toggle no-arrow">
                        <span class="fa fa-home"></span><span class="mtext">Dashboard</span>
                    </a>
                </li>

                <li class="dropdown">
                    <a href="javascript:;" class="dropdown-toggle">
                        <span class="fa fa-user"></span><span class="mtext">User</span>
                    </a>
                    <ul class="submenu" style="<?php  echo (isset($menu) && $menu=='user')?'display: block':'display: none'?>">
                        <li><a href="<?php echo base_url('');?>user">User List</a></li>
                        <li><a href="<?php echo base_url('');?>user/addUser">Add User</a></li>
                    </ul>
                </li>

                <li class="dropdown">
                    <a href="javascript:;" class="dropdown-toggle">
                        <span class="fa fa-balance-scale"></span><span class="mtext">Unit Of <br/>Measurement</span>
                    </a>
                    <ul class="submenu" style="<?php  echo (isset($menu) && ( $menu=='uom' || $menu=='serviceuom') )?'display: block':'display: none'?>">
                        <li><a href="<?php echo base_url('uom');?>">Item UOM List</a></li>
                        <!-- <li><a href="<?php //echo base_url('serviceuom');?>">Service UOM List</a></li> -->
                    </ul>
                </li>


                <!-- Added by Kousik Ends -->

                <li class="dropdown">
                    <a href="javascript:;" class="dropdown-toggle">
                        <span class="fa fa-balance-scale"></span><span class="mtext">Types</span>
                    </a>
                    <ul class="submenu" style="<?php  echo (isset($menu) && ( $menu=='uom' || $menu=='serviceuom') )?'display: block':'display: none'?>">
                        <li>
                            <a href="<?php echo base_url('attribute');?>" class="dropdown-toggle no-arrow <?php  echo (isset($menu) && $menu=='attribute')?'active':''?>">
                                <span class="fa fa-ravelry"></span><span class="mtext">Attribute</span>
                            </a>
                        </li>

                        <li>
                            <a href="<?php echo base_url('variance');?>" class="dropdown-toggle no-arrow <?php  echo (isset($menu) && $menu=='variance')?'active':''?>">
                                <span class="fa fa-bar-chart"></span><span class="mtext">Variance</span>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="dropdown">
                    <a href="javascript:;" class="dropdown-toggle">
                        <span class="fa fa-users"></span><span class="mtext">Profiles</span>
                    </a>
                    <ul class="submenu" style="<?php  echo (isset($menu) && ( $menu=='uom' || $menu=='serviceuom') )?'display: block':'display: none'?>">
                        <li>
                            <a href="<?php echo base_url('customer');?>" class="dropdown-toggle no-arrow <?php  echo (isset($menu) && $menu=='customer')?'active':''?>">
                                <span class="fa fa-users"></span><span class="mtext">Members</span>
                            </a>
                        </li>

                         <li>
                            <a href="<?php echo base_url('buyer');?>" class="dropdown-toggle no-arrow <?php  echo (isset($menu) && $menu=='buyer')?'active':''?>">
                                <span class="fa fa-users"></span><span class="mtext">Buyer Profile</span>
                            </a>
                        </li>

                        <li>
                            <a href="<?php echo base_url('seller');?>" class="dropdown-toggle no-arrow <?php  echo (isset($menu) && $menu=='seller')?'active':''?>">
                                <span class="fa fa-users"></span><span class="mtext">Seller Profile</span>
                            </a>
                        </li>
                        
                        <li>
                            <a href="<?php echo base_url('logistic-user');?>" class="dropdown-toggle no-arrow <?php  echo (isset($menu) && $menu=='logistic_user')?'active':''?>">
                                <span class="fa fa-user"></span><span class="mtext">Logistic&nbsp;User</span>
                            </a>
                        </li> 

                        <li>
                            <a href="<?php echo base_url('local_supplier');?>" class="dropdown-toggle no-arrow <?php  echo (isset($menu) && $menu=='lclsplr')?'active':''?>">
                                <span class="fa fa-users"></span><span class="mtext">Local Suppliers</span>
                            </a>
                        </li>

                    </ul>
                </li>                
                   
                
                <li>
                    <a href="<?php echo base_url('enquiry');?>" class="dropdown-toggle no-arrow <?php  echo (isset($menu) && $menu=='enquiry')?'active':''?>">
                        <span class="fa fa-list-alt"></span><span class="mtext">Enquiry</span>
                    </a>
                </li>

                <li>
                    <a href="<?php echo base_url('settings');?>/exchangerate" class="dropdown-toggle no-arrow <?php  echo (isset($menu) && $menu=='settings')?'active':''?>">
                        <span class="fa fa-exchange"></span><span class="mtext">Exchange rate</span>
                    </a>
                </li>

                <li>
                    <a href="<?php echo base_url('orders');?>" class="dropdown-toggle no-arrow <?php  echo (isset($menu) && $menu=='orders')?'active':''?>">
                        <span class="fa fa-shopping-cart"></span><span class="mtext">Orders</span>
                    </a>
                </li>

                <li>
                    <a href="<?php echo base_url('logistic');?>" class="dropdown-toggle no-arrow <?php  echo (isset($menu) && $menu=='logistic')?'active':''?>">
                        <span class="fa fa-truck"></span><span class="mtext">Logistics</span>
                    </a>
                </li>                           

                <li>
                    <a href="<?php echo base_url('forum');?>" class="dropdown-toggle no-arrow <?php  echo (isset($menu) && $menu=='forum')?'active':''?>">
                        <span class="fa fa-forumbee"></span><span class="mtext">Forum</span>
                    </a>
                </li>

                <li>
                    <a href="<?php echo base_url('rfq');?>" class="dropdown-toggle no-arrow <?php  echo (isset($menu) && $menu=='rfq')?'active':''?>">
                        <span class="fa fa-quote-left"></span><span class="mtext">Request For Quotation</span>
                    </a>
                </li>


                <!-- Added by Kousik Ends -->


                <li class="dropdown">
                    <a href="javascript:;" class="dropdown-toggle">
                        <span class="fa fa-list-alt"></span><span class="mtext">Category</span>
                    </a>
                    <ul class="submenu" style="<?php  echo (isset($menu) && $menu=='category')?'display: block':'display: none'?>">
                        <li><a href="<?php echo base_url('category');?>">Category List</a></li>
                        <li><a href="<?php echo base_url('');?>category/add/MA==">Add Main Category</a></li>
                    </ul>
                </li>

                <li class="dropdown">
                    <a href="javascript:;" class="dropdown-toggle">
                        <span class="fa fa-sitemap"></span><span class="mtext">Item</span>
                    </a>
                    <ul class="submenu" style="<?php  echo (isset($menu) && ( $menu=='item' || $menu=='serviceitem' ) )?'display: block':'display: none'?>">
                        <li><a href="<?php echo base_url('item');?>">Item List</a></li>
                        <!-- <li><a href="<?php // echo base_url('serviceitem');?>">Service Item List</a></li> -->
                    </ul>
                </li>


                <li class="dropdown">
                    <a href="javascript:;" class="dropdown-toggle">
                        <span class="fa fa-file-text"></span><span class="mtext">CMS</span>
                    </a>
                    <ul class="submenu" style="<?php  echo (isset($menu) && ($menu=='privacypolicy' || $menu=='termsncondition' || $menu=='about' || $menu=='faq' || $menu=='contact' ) )?'display: block':'display: none'?>">
                        <li><a href="<?php echo base_url('about');?>">About</a></li>
                        <li><a href="<?php echo base_url('faq');?>">FAQ</a></li>
                        <li><a href="<?php echo base_url('contact');?>">Contact</a></li>
                        <li><a href="<?php echo base_url('privacypolicy');?>">Privacy & Policy</a></li>
                        <li><a href="<?php echo base_url('termsncondition');?>">Terms & Condition</a></li>
                    </ul>
                </li>

                <li>
                    <a href="<?php echo base_url('settings');?>" class="dropdown-toggle no-arrow">
                        <span class="fa fa-cogs"></span><span class="mtext">Settings</span>
                    </a>
                </li>             
            </ul>
        </div>
    </div>
</div>