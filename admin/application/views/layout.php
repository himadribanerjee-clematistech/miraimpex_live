<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Miraimpex - The Marketplace</title>
        <link rel="icon" href="<?php echo base_url('assets/images/icon.ico'); ?>">
        
        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
        <!-- CSS -->

        <link href="<?php echo base_url(); ?>assets/deskapp-master/vendors/styles/style.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url(); ?>assets/deskapp-master/src/plugins/datatables/media/css/jquery.dataTables.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url(); ?>assets/deskapp-master/src/plugins/datatables/media/css/dataTables.bootstrap4.css" rel="stylesheet" type="text/css">
         <link href="<?php echo base_url(); ?>assets//deskapp-master/src/plugins/datatables/media/css/responsive.dataTables.css" rel="stylesheet" type="text/css">
          <link href="<?php echo base_url(); ?>assets/css/admin.css" rel="stylesheet" type="text/css">
          <link rel="stylesheet" href="<?php echo base_url()?>assets/deskapp-master/src/plugins/switchery/dist/switchery.css">

          
    
       <!-- JS -->
        <script src="<?php echo base_url(); ?>assets/js/jquery-1.11.2.min.js"></script>
        <link href="<?php echo base_url();?>assets/css/toastr.css" rel="stylesheet">
        <link href="<?php echo base_url();?>assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

        <link rel="stylesheet" href="<?php echo base_url('assets/vendors/select2/dist/css/select2.css')?>">
        <script src="<?php echo base_url('assets/vendors/select2/dist/js/select2.js')?>"></script>

        <style type="text/css">
          .clone {
            display: none;
          }
        </style>

    </head>
    <body>
          <!-- topmenu started -->
                       
             <?php  echo (isset($topmenu))?$topmenu:'';?>
          <!-- topmenu ended -->
         <!--   sidebar started -->
            
              <?php  echo (isset($side_bar))?$side_bar:'';?>
          <!--   sidebar ended -->
          <div class="main-container">
            <div class="pd-ltr-20  height-100-p xs-pd-20-10"> <!-- customscroll customscroll-10-p -->
                
                <?php  echo (isset($content))?$content:'';?>  
               
               
               <!--   footer start -->
                      <?php  echo (isset($footer))?$footer:'';?>
                <!-- footer end -->
            </div>
           </div>
      <input type="hidden" name="base_url" id="base_url" value="<?php echo base_url();?>"/>     
    </body>

    <!-- js -->

<script src="<?php echo base_url(); ?>assets/js/script.min.js"></script>


<script src="<?php echo base_url(); ?>assets/deskapp-master/src/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/deskapp-master/src/plugins/datatables/media/js/dataTables.bootstrap4.js"></script>
<script src="<?php echo base_url(); ?>assets/deskapp-master/src/plugins/datatables/media/js/dataTables.responsive.js"></script>
<script src="<?php echo base_url(); ?>assets/deskapp-master/src/plugins/datatables/media/js/responsive.bootstrap4.js"></script>

    <script>
        $('document').ready(function(){
            $('.data-table').DataTable({
                scrollCollapse: true,
                autoWidth: false,
                  paging: false,
                 searching: false,
                 bInfo: false,
                responsive: true,
            });
            $('.data-table-export').DataTable({
                scrollCollapse: true,
                autoWidth: false,
                responsive: true,
                  paging: false,
                   bInfo: false,
                 searching: false,
                columnDefs: [{
                    targets: "datatable-nosort",
                    orderable: false,
                }],
                "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                "language": {
                    "info": "_START_-_END_ of _TOTAL_ entries",
                    searchPlaceholder: "Search"
                },
                dom: 'Bfrtip',
                buttons: [
                'copy', 'csv', 'pdf', 'print'
                ]
            });
            var table = $('.select-row').DataTable();
            $('.select-row tbody').on('click', 'tr', function () {
                if ($(this).hasClass('selected')) {
                    $(this).removeClass('selected');
                }
                else {
                    table.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                }
            });
            var multipletable = $('.multiple-select-row').DataTable();
            $('.multiple-select-row tbody').on('click', 'tr', function () {
                $(this).toggleClass('selected');
            });
        });
    </script>
        <script src="<?php echo base_url()?>assets/deskapp-master/src/plugins/switchery/dist/switchery.js"></script>

        <script src="<?php echo base_url(); ?>assets/js/tinymce/js/tinymce/tinymce.min.js"></script>
         <script src="<?php echo base_url();?>assets/js/toastr.min.js"></script>
    </html>