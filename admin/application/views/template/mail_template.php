<html>
	<body style="margin:0px;">
		<table style="padding: 20px 25px;background: linear-gradient(to left top, rgb(51, 204, 255) 0%, rgb(255, 153, 204) 100%); width: 100%">
			<tr>
				<td>
					<table style="width: 100%;margin: 0px auto; padding: 0px 25px 15px;text-align: center;font-family: sans-serif;">
						<tr>
							<th><img src="http://192.168.2.95/agroWholesale/assets/img/logo.png" alt=""></th>
						</tr>
					</table>
					<table style="width: 100%;margin: 0px auto; padding:30px 25px 20px;text-align: left;font-family: sans-serif;background-color: #ffffff;">
						<tr>
						    <td style="text-align: center;"><p style="font-size: 20px; color:#333;padding-top: 7px;margin: 0px;">Hello <span style="font-size: 22px; color: #f1a306;padding-left: 5px;"> <?php echo $name; ?> </span></p></td>
						</tr>
					</table>
					<table style="width: 100%;margin: 0px auto; padding:0px 25px 20px;text-align: left;font-family: sans-serif;background-color: #ffffff;">
						<tr>
						    <td style="text-align: left;">
						    	<p style="font-size: 15px; color:#333;line-height: 25px; padding-bottom: 10px;margin: 0px;"><?php echo $message; ?></p>
						    </td>
						</tr>
					</table>
					<table style="width: 100%;margin: 0px auto; padding: 30px 25px 0px;text-align: center;font-family: sans-serif;">
						<tr>
							<th style="width: auto;display: inline-block;">
								<a href="#" style="color: #fff;text-decoration: none;padding: 0px 5px;">
									<img src="http://192.168.2.95/agroWholesale/assets/img/facebook.png" alt="">
								</a>
							</th>
							<th style="width: auto;display: inline-block;">
								<a href="#" style="font-size: 15px;color: #fff;text-decoration: none;padding: 0px 5px;">
									<img src="http://192.168.2.95/agroWholesale/assets/img/twitter.png" alt="">
								</a>
							</th>
							<th style="width: auto;display: inline-block;">
								<a href="#" style="font-size: 15px;color: #fff;text-decoration: none;padding: 0px 5px;">
									<img src="http://192.168.2.95/agroWholesale/assets/img/youtube-logo.png" alt="">
								</a>
							</th>
							<th style="width: auto;display: inline-block;">
								<a href="#" style="font-size: 15px;color: #fff;text-decoration: none;padding: 0px 5px;">
									<img src="http://192.168.2.95/agroWholesale/assets/img/linkedin-logo.png" alt="">
								</a>
							</th>
							<th style="width: auto;display: inline-block;">
								<a href="#" style="font-size: 15px;color: #fff;text-decoration: none;padding: 0px 5px;">
									<img src="http://192.168.2.95/agroWholesale/assets/img/instagram.png" alt="">
								</a>
							</th>
						</tr>
						<tr>
							<th>
								<p style="font-size: 13px;color: #eee;font-weight: 100;padding-top: 0px;margin: 0px;">© 2019 <a href="#" style="font-size: 14px;color: #333;">Akpoti.farm</a>. All rights reserved.</p>
							</th>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</body>
</html>