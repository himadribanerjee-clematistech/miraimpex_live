
<div class="pre-loader"></div>
<div class="header clearfix">
    <div class="header-right">
        <div class="brand-logo" style="background: none;">
            <a href="#">
               <img src="<?php echo base_url()?>assets/images/logo1.png" alt="Agro Wholesale" class="login-img" style="max-width: none; margin: -5px;">
            </a>
        </div>
        
        <div class="menu-icon">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
        </div>
        <div style="float:left;" class="d-none d-md-block">
            <h5 style="margin:20px 0 0 30px" class="text-orange"></h5>   
            
        </div>
            
        <div class="user-info-dropdown">
            <div class="dropdown">
                <a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                    <span class="user-icon"><i class="fa fa-user-o"></i></span>
                    <span class="user-name"></span>
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item <?php  echo (isset($submenu) && ($submenu=='profile' ) )?'active ':'';?> " href="<?php base_url()?>editProfile"><i class="fa fa-user-md" aria-hidden="true"></i> Profile</a>
                    <a class="dropdown-item <?php  echo (isset($submenu) && ($submenu=='changepassword' ) )?'active ':'';?> " href="<?php base_url()?>changePassword"><i class="fa fa-cog" aria-hidden="true"></i> Change Password</a>
                    
                    <a class="dropdown-item" href="<?php base_url()?>dashboard/logout"><i class="fa fa-sign-out" aria-hidden="true"></i> Log Out</a>
                </div>
            </div>
        </div>
       
    </div>
</div>