<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$route['local_supplier'] = 'lclsplr';
$route['logistic-user'] = 'logistic_user';


$route['default_controller'] = 'dashboard';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['changePassword'] = 'user/changePassword';
$route['editProfile'] = 'user/editProfile';
