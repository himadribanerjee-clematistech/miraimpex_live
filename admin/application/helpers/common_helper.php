<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}


if (!function_exists('view')) {

    function view($data = '') {
      	$CI = & get_instance();
		$data["side_bar"]=$CI->load->view("side_bar",$data,TRUE);
		$data["topmenu"]=$CI->load->view("topmenu",$data,TRUE);
		$data["footer"]=$CI->load->view("footer",$data,TRUE);
		if(isset($data["view_file"])){
			$data["content"]=$CI->load->view($data["view_file"],$data,TRUE);
		}
		 $CI->load->view( "layout", $data);
    }

    function email($to, $data){
    	$CI = & get_instance();

    	$subject = 'Akpoti Notification!';
        $from = 'info@agrowholesale.com';

        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= 'From: ' . $from . "\r\n". 'Reply-To: ' . $from . "\r\n" . 'X-Mailer: PHP/' . phpversion();

        $message = $CI->load->view('template/mail_template', $data, TRUE);

        $mail = mail($to, $subject, $message, $headers);

        if ($mail) {
        	return True;
        }
        else {
        	return False;
        }

    }

}

if (!function_exists('generateOTP')) {

    function generateOTP() {
      	$generator = "1357902468"; 
  		$result = ""; 
	  	$n=6;
	    for ($i = 1; $i <= $n; $i++) { 
	        $result .= substr($generator, (rand()%(strlen($generator))), 1); 
	    } 
	  
	    // Return result 
	    return $result; 
    }

}

?>