<?php
if (! defined('BASEPATH')) exit('No direct script access allowed');
 
if (! function_exists('createPageNationLink')) {
     function createPageNationLink($base_url,$total_rows,$per_page){
     	$ci = get_instance();
    	 	$config = [];
	       $config["base_url"] =$base_url;
	       $config["total_rows"] = $total_rows;
			$config["per_page"] = $per_page;
			$config['enable_query_strings'] = TRUE;
		$config['page_query_string'] = TRUE;
		$config['reuse_query_string'] = TRUE;
	       $config["uri_segment"] = 3;
	       $config["full_tag_open"] = '<ul class="pagination">';
			$config["full_tag_close"] = '</ul>';	
			$config["first_link"] = "&laquo;";
			$config["first_tag_open"] = "<li>";
			$config["first_tag_close"] = "</li>";
			$config["last_link"] = "&raquo;";
			$config["last_tag_open"] = "<li>";
			$config["last_tag_close"] = "</li>";
			$config['next_link'] = '&gt;';
			$config['next_tag_open'] = '<li>';
			$config['next_tag_close'] = '<li>';
			$config['prev_link'] = '&lt;';
			$config['prev_tag_open'] = '<li>';
			$config['prev_tag_close'] = '<li>';
			$config['cur_tag_open'] = '<li class="active"><a href="#">';
			$config['cur_tag_close'] = '</a></li>';
			$config['num_tag_open'] = '<li>';
			$config['num_tag_close'] = '</li>';
	       $ci->pagination->initialize($config);
	      return $ci->pagination->create_links();
    }
}

?>