<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class ItemModel extends CI_Model {
	protected $userTable = 'user';
	protected $itemTable = 'item';
	protected $catTable = 'category';
	protected $uomTable = 'uom';
	
	public function __construct() {
        parent::__construct();
    }
    
    public function getitemCount($data){
    	try {
    		$this->db->db_debug = false;
    		$this->db->select("c.category_name_en, i.id, i.item_name_en, i.price_min, i.price_max, i.currency");
    		$this->db->from($this->itemTable . ' i');
    		$this->db->join($this->catTable . ' c', 'i.category_id = c.id');
			$this->db->where("c.active",1);
    		/*$where='(case when c.parent_id=0  then c.active else ( SELECT active FROM t_category  WHERE id = c.parent_id ) end) = 1';
	    		$this->db->where($where); */
			if(isset($data["item_name"]) && $data["item_name"]!=''){ 
	    		$this->db->like("item_name",$data["item_name"]);
	    	}
	    	if(isset($data["subcategory_id"]) && $data["subcategory_id"]!=''){ 
	    		$this->db->where("category_id",$data["subcategory_id"]);

	    	}
	    	if(isset($data["actve"]) && $data["actve"]!=''){ 
	    		$this->db->where("i.active",$data["actve"]);

	    	}
	    	 if(isset($data["category_id"]) && $data["category_id"]!=''){
	    		$where='(case when c.parent_id=0  then c.id else ( SELECT id FROM t_category  WHERE id = c.parent_id ) end) = '.$data["category_id"];
	    		$this->db->where($where);

	    	}
			$query= $this->db->get();
			$returnrows = $query->result_array();
			
	    	//echo $this->db->last_query();die();

	    	$db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
				
	        	return count($returnrows);

	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
    }
    public function getitemList($data,$limit='', $start=0){
    	try {
    		$this->db->db_debug = false;

    		$this->db->select("c.category_name_en, i.id, i.item_name_en, i.price_min, i.price_max, i.currency, i.active, i.showcase, i.on_display");
    		$this->db->from($this->itemTable . ' i');
    		$this->db->join($this->catTable . ' c', 'i.category_id = c.id');
    		$this->db->where("c.active",1);
    		/* $where='(case when c.parent_id=0  then c.active else ( SELECT active FROM t_category  WHERE id = c.parent_id ) end) = 1';
	    		$this->db->where($where); */
	    	if(isset($data["item_name"]) && $data["item_name"]!=''){
	    		$this->db->like("item_name",$data["item_name"]);
	    	}
	    	if(isset($data["actve"]) && $data["actve"]!=''){ 
	    		$this->db->where("i.active",$data["actve"]);

	    	}
	    	if(isset($data["subcategory_id"]) && $data["subcategory_id"]!=''){ 
	    		$this->db->where("category_id",$data["subcategory_id"]);

	    	}
	    	 if(isset($data["category_id"]) && $data["category_id"]!=''){
	    		$where='(case when c.parent_id=0  then c.id else ( SELECT id FROM t_category  WHERE id = c.parent_id ) end) = '.$data["category_id"];
	    		$this->db->where($where);

	    	}
	    	$this->db->order_by('item_name_en', 'ASC'); 
	    	if($limit!=''  && $start>=0){
	    		$this->db->limit($limit, $start);
	    	}
	    	
 
 			$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	 //echo $this->db->last_query();die();
	        	$result=[];
	        	foreach($query->result_array() as $row){
	        		$result[]=$row;
	        	}
	        	return $result;
	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
    }	

    public function getCat() {
    	return $this->db->select('id, category_name')->where('parent_id', 0)->where('active', 1)->from($this->catTable)->get();
    }

    public function getUom() {
    	return $this->db->select('id, name')->from($this->uomTable)->where("active",1)->get();
    }

    public function doitemCheck($data, $id=''){
    	//print_r($data); die();
		try {
		 	$this->db->db_debug = FALSE;
	        $this->db->select("item_name");
	    	$this->db->from($this->itemTable);
	    	if(isset($data) && trim($data)!='' ){
	    		$this->db->where("item_name",$data);
	    		
	    	}

	    	if($id!=''){
	    		$this->db->where("id !=",$id);
	    	}
	    	
	    	$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query() . '<br>';
	        	if($query->num_rows() >0) {
	        		return True;
	        	}
	        	else {
	        		return false;
	        	}
	        }
	        //return TRUE;
	    } catch (Exception $e) {
	       throw new Exception();
	    }	
    }
     
    public function insertitem($data){
     	//print_r($data); die();
    	try {
    	 	$this->db->db_debug = FALSE;
    	 	$res = $this->db->insert($this->itemTable, $data);

    	 	$db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }
	        else {
	        	//echo $this->db->last_query() . '<br>';
	        	if ($res) {
	        		return true;
	        	}
	        	else {
	        		return false;
	        	}
	        }
	        //return  $this->db->insert_id();
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }
    	
    }

    public function getById($id){
    	try {
		 	$this->db->db_debug = false;
	        $this->db->select("i.id, i.item_name, i.item_image, i.item_description,i.local_name,IF (c.parent_id=0 ,c.category_name,(SELECT category_name FROM t_category WHERE id= c.parent_id) ) as category_name,IF (c.parent_id=0 ,c.id,(SELECT id FROM t_category WHERE id= c.parent_id) ) as category_id ,IF (c.parent_id=0 ,'',c.category_name ) as subcat_name ,IF (c.parent_id=0 ,'',c.id ) as subcat_id ");
	    	$this->db->from($this->itemTable . ' i');
    		$this->db->join($this->catTable . ' c', 'i.category_id = c.id');    		
	    	
	    	$this->db->where("i.id", $id);
	    	
	    	$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	$data=[];
	        	foreach($query->result_array() as $row){
	        		 $data[]=$row;
	    	
	        	}
	        	//echo $this->db->last_query();
	        	return $data;
	        }
	        return TRUE;
	    } catch (Exception $e) {
	       throw new Exception();
	    }	
    }

    public function updateItem($data,$where){
    	
    	try {
    	 	$this->db->db_debug = False;
    	 	$this->db->trans_start(FALSE);
	        $this->db->where($where);
	    	$this->db->update($this->itemTable, $data);
	        $this->db->trans_complete();
	        $db_error = $this->db->error();
	            if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	           
	        }
	        return TRUE;
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }
    }

    public function doDeleteitem($where){
    	
    	try {
    	 	$this->db->db_debug = False;
    	 	$res = $this->db->delete($this->itemTable, ['id' => $where]);

	    	$db_error = $this->db->error();
	        if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }
	        else {
	        	if ($res) {
	        		return True;
	        	}
	        	else {
	        		return False;
	        	}
	        }
	        
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }
    }
   	
}

?>