<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class UserModel extends CI_Model {
	protected $userTable = 't_user';
	protected $salt='HA{t~xOB;74Dp,Q+$y3F??:}/?LMj_';
	public function __construct() {
        parent::__construct();
    }
    public function passwordGenerate($password){
    	return md5(crypt($password,$this->salt));

    }

   
   
     public function doPasswordCheck($data){
    	
		 try {
		 	$this->db->db_debug = FALSE;
	        $this->db->select("id");
	    	$this->db->from($this->userTable);
	    	if(isset($data["token_id"]) && isset($data["password"]) ){
	    		$this->db->where("token_id",$data["token_id"]);
	    		$this->db->where("password",md5(crypt($data["password"],$this->salt)));
	    	}
	    	$this->db->where("actve",1);
	    	$this->db->where("is_admin",1);
	    	$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query();
	        	return $query->result_array();
	        }
	        return TRUE;
	    } catch (Exception $e) {
	       throw new Exception();
	    }	
    }

  
   
    public function doLoginCheck($data){
    	
		try {
		 	$this->db->db_debug = FALSE;
	        $this->db->select("id,user_name,mobile,actve");
	    	$this->db->from($this->userTable);
	    	if(isset($data["user_name"]) && isset($data["password"]) ){
	    		$this->db->where("(user_name='".$data["user_name"]."' OR mobile='".$data["user_name"]."' )");
	    		$this->db->where("password",md5(crypt($data["password"],$this->salt)));
	    		$this->db->where("actve",1);
	    		$this->db->where("is_admin",1);
		    	$query=$this->db->get();
		        $db_error = $this->db->error();
		       // echo $this->db->last_query();die();
		       	if (!empty($db_error) && $db_error['code']>0) {
		            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
		           
		        }else{

		        	return $query->result_array();
		        }
	    	}else{
	    		 return [];
	    	}
	    	

	        
	    } catch (Exception $e) {
	       throw new Exception();
	    }	
    }

   	  public function updateUser($data,$where){
    	
    	try {
    	 	$this->db->db_debug = False;
    	 	$this->db->trans_start(FALSE);
	        $this->db->where($where);
	    	$this->db->update($this->userTable, $data);
	        $this->db->trans_complete();
	        $db_error = $this->db->error();
	            if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }
	        return TRUE;
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }
    }
    public function getUserDetail($id,$actve=1){
    	 try {
		 	$this->db->db_debug = false;
	         $this->db->select("u.user_name,u.id as user_id,u.email,u.mobile,u.Name,u.actve");
	    	$this->db->from($this->userTable." u");
	    	
	    	$this->db->where("u.id",$id);
	    	if($actve==1){
	    		$this->db->where("u.actve",1);
	    	}
	    	
	    	
	    	$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	$data=[];
	        	foreach($query->result_array() as $row){
	        		 $data[]=$row;
	    	
	        	}
	        	//echo $this->db->last_query();
	        	return $data;
	        }
	        return TRUE;
	    } catch (Exception $e) {
	       throw new Exception();
	    }	
    }
     public function getUserDetailsbyId($id){
    	 try {
		 	$this->db->db_debug = false;
	        $this->db->select("u.user_name,u.id as user_id,u.email,u.mobile,u.Name,u.actve");
	    	$this->db->from($this->userTable." u");
	    	
	    	$this->db->where("u.token_id",$id);
	    	$this->db->where("u.actve",1);
	    	$this->db->where("u.is_admin",1);
	    	$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	$data=[];
	        	foreach($query->result_array() as $row){
	        		 $data[]=$row;
	    	
	        	}
	        	//echo $this->db->last_query();
	        	return $data;
	        }
	        return TRUE;
	    } catch (Exception $e) {
	       throw new Exception();
	    }	
    }
     public function getUserDetailsbyUserId($id){
    	 try {
		 	$this->db->db_debug = false;
	        $this->db->select("u.user_name,u.id as user_id,u.email,u.mobile,u.Name,u.actve");
	    	$this->db->from($this->userTable." u");
	    	
	    	$this->db->where("u.id",$id);
	    	$this->db->where("u.actve",1);
	    	$this->db->where("u.is_admin",1);
	    	$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	$data=[];
	        	foreach($query->result_array() as $row){
	        		 $data[]=$row;
	    	
	        	}
	        	//echo $this->db->last_query();
	        	return $data;
	        }
	        return TRUE;
	    } catch (Exception $e) {
	       throw new Exception();
	    }	
    }
    public function getUserDetailsbyEmailIdOTP($otp,$email){
    	try {
		 	$this->db->db_debug = false;
	        $this->db->select("u.user_name,u.id as user_id,u.email,u.mobile,u.Name,u.actve");
	    	$this->db->from($this->userTable." u");
	    	
	    	$this->db->where("u.email_otp",$otp);
	    	$this->db->where("u.email",$email);
	    	$this->db->where("u.actve",1);
	    	$this->db->where("u.is_admin",1);
	    	$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	$data=[];
	        	foreach($query->result_array() as $row){
	        		 $data[]=$row;
	    	
	        	}
	        	//echo $this->db->last_query();
	        	return $data;
	        }
	        return TRUE;
	    } catch (Exception $e) {
	       throw new Exception();
	    }	

    }
     public function getUserDetailsbyEmailId($email){
    	 try {
		 	$this->db->db_debug = false;
	        $this->db->select("u.user_name,u.id as user_id,u.email,u.mobile,u.Name,u.actve");
	    	$this->db->from($this->userTable." u");
	    	
	    	$this->db->where("u.email",$email);
	    	$this->db->where("u.actve",1);
	    	$this->db->where("u.is_admin",1);
	    	$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	$data=[];
	        	foreach($query->result_array() as $row){
	        		 $data[]=$row;
	    	
	        	}
	        	//echo $this->db->last_query();
	        	return $data;
	        }
	        return TRUE;
	    } catch (Exception $e) {
	       throw new Exception();
	    }	
    }
    public function getUserCount($data){
    	try {
    		$this->db->db_debug = false;
    		$this->db->select("u.id");
    		$this->db->from($this->userTable." u");
    		if(isset($data["user_name"]) && $data["user_name"]!=''){
	    		$this->db->like("u.user_name",$data["user_name"]);
	    	}
	    	if(isset($data["Name"]) && $data["Name"]!=''){
	    		$this->db->like("u.Name",$data["Name"]);
	    	}
	    	if(isset($data["actve"]) && $data["actve"]!=''){
	    		$this->db->where("u.actve",$data["actve"]);
	    	}
	    	$db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query();die();
	        	return $this->db->count_all_results();

	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
    }
    public function getUserList($data,$limit, $start){
    	try {
    		$this->db->db_debug = false;
    		$this->db->select("u.id,u.user_name,u.email,u.mobile,u.Name,u.actve");
    		$this->db->from($this->userTable." u");
    		if(isset($data["user_name"]) && $data["user_name"]!=''){
	    		$this->db->like("u.user_name",$data["user_name"]);
	    	}
	    	if(isset($data["Name"]) && $data["Name"]!=''){
	    		$this->db->like("u.Name",$data["Name"]);
	    	}
	    	if(isset($data["actve"]) && $data["actve"]!=''){
	    		$this->db->where("u.actve",$data["actve"]);
	    	}
	    	$this->db->order_by('u.user_name', 'ASC'); 
	    	$this->db->limit($limit, $start);

 			$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query();die();
	        	$result=[];
	        	foreach($query->result_array() as $row){
	        		$result[]=$row;
	        	}
	        	return $result;
	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
    }	
    public function doEmailCheck($data,$id=''){
    	
		 try {
		 	$this->db->db_debug = FALSE;
	        $this->db->select("id");
	    	$this->db->from($this->userTable);
	    	if(isset($data["email"]) && trim($data["email"])!='' ){
	    		$this->db->where("email",$data["email"]);
	    		
	    	}

	    	if($id!=''){
	    		$this->db->where("id !=",$id);
	    	}
	    	$this->db->where("actve",1);
	    	$query=$this->db->get();

	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	return $query->result_array();
	        }
	        return TRUE;
	    } catch (Exception $e) {
	       throw new Exception();
	    }	
    }
     public function doMobileCheck($data,$id=''){
    	
		 try {
		 	$this->db->db_debug = FALSE;
	        $this->db->select("id");
	    	$this->db->from($this->userTable);
	    	if(isset($data["mobile"]) && trim($data["mobile"])!='' ){
	    		$this->db->where("mobile",$data["mobile"]);
	    		
	    	}

	    	if($id!=''){
	    		$this->db->where("id !=",$id);
	    	}
	    	$this->db->where("actve",1);
	    	$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	return $query->result_array();
	        }
	        return TRUE;
	    } catch (Exception $e) {
	       throw new Exception();
	    }	
    }
     public function doUserNameCheck($data,$id=''){
    	
		 try {
		 	$this->db->db_debug = FALSE;
	        $this->db->select("id");
	    	$this->db->from($this->userTable);
	    	if(isset($data["user_name"]) &&trim($data["user_name"])!='' ){
	    		$this->db->where("user_name",$data["user_name"]);
	    		
	    	}
	    	if($id!=''){
	    		$this->db->where("id !=",$id);
	    	}
	    	$this->db->where("actve",1);
	    	$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query();
	        	return $query->result_array();
	        }
	        return TRUE;
	    } catch (Exception $e) {
	       throw new Exception();
	    }	
    }
     public function insertUser($data){

    	try {
    	 	$this->db->db_debug = FALSE;
    	 	 $this->db->insert($this->userTable, $data);
	        
	        $db_error = $this->db->error();
	       if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }
	        return  $this->db->insert_id();;
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }
    	
    }
     public function doDeleteUser($data){

    	try {
    	 	$this->db->db_debug = FALSE;
    	 	 $this->db->where($data);
			$this->db->delete("t_user");

	        $db_error = $this->db->error();
	       if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }
	        return  true;
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }
    	
    }
   	public function generateToken(){
    	return md5(crypt(rand().time(),$this->salt));
    }

    

 



    

}

?>