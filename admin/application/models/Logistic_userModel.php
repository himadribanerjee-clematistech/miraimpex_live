<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Logistic_userModel extends CI_Model {
	protected $userTable = 'logistic_user';
	protected $logisticTable = 'logistic';

	public function __construct() {
        parent::__construct();
    }
    
    public function getUserCount($data){
    	try {
    		$this->db->db_debug = false;

    		// SELECT COUNT(l.email) AS tot_mail, lu.id, lu.name, lu.email, lu.contact, lu.created FROM t_logistic_user lu LEFT JOIN t_logistic l ON lu.id = l.logistic_partner_id

    		$this->db->select("COUNT(l.email) AS tot_mail, lu.id, lu.name, lu.email, lu.contact, lu.created, lu.status");
    		$this->db->from($this->userTable . ' lu');
    		$this->db->join($this->logisticTable . ' l', 'ON lu.id = l.logistic_partner_id', 'LEFT');

    		if(isset($data["Name"]) && $data["Name"]!=''){
	    		$this->db->like("lu.name",$data["Name"]);
	    	}
	    	if(isset($data["status"]) && $data["status"]!=''){
	    		$this->db->where("lu.status",$data["status"]);
	    		$this->db->group_by('lu.id HAVING count(lu.id) > 0');
	    	}

	    	

	    	// $query=$this->db->get();
	    	// echo $this->db->last_query();die();

	    	$db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	// echo $this->db->last_query();die();
	        	return $this->db->count_all_results();

	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
    }
    public function getUserList($data,$limit, $start){
    	try {
    		$this->db->db_debug = false;
    		
    		// SELECT COUNT(l.email) AS tot_mail, `lu`.`id`, `lu`.`name`, `lu`.`email`, `lu`.`contact`, `lu`.`created`, `lu`.`status` FROM `t_logistic_user` `lu` LEFT JOIN `t_logistic` `l` ON `lu`.`id` = `l`.`logistic_partner_id` WHERE `lu`.`status` = '1'  GROUP BY `lu`.`id` HAVING count(`lu`.`id`) > 0

    		$this->db->select("COUNT(l.email) AS tot_mail, lu.id, lu.name, lu.email, lu.contact, lu.created, lu.status");
    		$this->db->from($this->userTable . ' lu');
    		$this->db->join($this->logisticTable . ' l', 'ON lu.id = l.logistic_partner_id', 'LEFT');

    		
	    	if(isset($data["Name"]) && $data["Name"]!=''){
	    		$this->db->like("lu.name",$data["Name"]);
	    	}
	    	if(isset($data["status"]) && $data["status"]!=''){
	    		$this->db->where("lu.status",$data["status"]);
	    		$this->db->group_by('lu.id HAVING count(lu.id) > 0');
	    	}

	    	$this->db->order_by('lu.created', 'DESC'); 

	    	if ($limit != '') {
	    		$this->db->limit($limit, $start);
	    	}
	    	

 			$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	// echo $this->db->last_query();die();
	        	$result=[];
	        	foreach($query->result_array() as $row){
	        		$result[]=$row;
	        	}
	        	return $result;
	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
    }	
    public function doEmailCheck($data,$id=''){    	
    	// echo $id; die();
		try {
		 	$this->db->db_debug = FALSE;
	        $this->db->select("id");
	    	$this->db->from($this->userTable);
	    	if(isset($data["email"]) && trim($data["email"])!='' ){
	    		$this->db->where("email",$data["email"]);    		
	    	}

	    	if($id!=''){
	    		$this->db->where("id !=",$id);
	    	}
	    	$this->db->where("status",1);
	    	$query=$this->db->get();

	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	return $query->result_array();
	        }
	        return TRUE;
	    } catch (Exception $e) {
	       throw new Exception();
	    }	
    }
     
    public function doMobileCheck($data,$id=''){    	
		try {
		 	$this->db->db_debug = FALSE;
	        $this->db->select("id");
	    	$this->db->from($this->userTable);
	    	if(isset($data["mobile"]) && trim($data["mobile"])!='' ){
	    		$this->db->where("contact",$data["mobile"]);
	    		
	    	}

	    	if($id!=''){
	    		$this->db->where("id !=",$id);
	    	}
	    	$this->db->where("status",1);
	    	$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	return $query->result_array();
	        }
	        return TRUE;
	    } catch (Exception $e) {
	       throw new Exception();
	    }	
    }
    
    public function insertUser($data){
    	try {
    	 	$this->db->db_debug = FALSE;
    	 	$this->db->insert($this->userTable, $data);
	        
	        $db_error = $this->db->error();
	       if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }
	        return  $this->db->insert_id();;
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }    	
    }

    public function getUserDetail($id,$actve=1){
    	try {
		 	$this->db->db_debug = false;
	         $this->db->select("id as user_id, email, contact, name");
	    	$this->db->from($this->userTable);
	    	
	    	$this->db->where("id",$id);
	    	if($actve==1){
	    		$this->db->where("status",1);
	    	}
	    	
	    	
	    	$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	$data=[];
	        	foreach($query->result_array() as $row){
	        		 $data[]=$row;
	    	
	        	}
	        	//echo $this->db->last_query();
	        	return $data;
	        }
	        return TRUE;
	    } catch (Exception $e) {
	       throw new Exception();
	    }	
    }

    public function updateUser($data,$where){    	
    	try {
    	 	$this->db->db_debug = False;
    	 	$this->db->trans_start(FALSE);
	        $this->db->where($where);
	    	$this->db->update($this->userTable, $data);
	        $this->db->trans_complete();
	        $db_error = $this->db->error();
	            if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }
	        return TRUE;
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }
    }

}

?>