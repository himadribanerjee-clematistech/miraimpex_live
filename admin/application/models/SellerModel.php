<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class SellerModel extends CI_Model {
	protected $custTable = 'customer';
	protected $sellerTable = 'seller_profile';
	protected $sellerImageTable = 'seller_image';
	protected $agentDocuTable = 'agent_document';
	
	public function __construct() {
        parent::__construct(); 
    }

    public function getSellerCount($data){
    	//print_r($data); die();
    	try {
    		$this->db->db_debug = false;

    		// SELECT c.name as seller, c.email, sp.id, sp.business_name, sp.state, sp.city, sp.contact_number, sp.active FROM t_seller_profile sp LEFT JOIN t_customer c ON c.id = sp.cust_id

    		$this->db->select("c.name as seller, c.email, c.displayLocal, sp.id, sp.business_name, sp.city, sp.contact_number, sp.active");
    		$this->db->from($this->sellerTable . ' sp');
    		$this->db->join($this->custTable . ' c', 'ON sp.cust_id = c.id', 'LEFT');

    		if(isset($data["customer_id"]) && $data["customer_id"]!=''){ 
	    		$this->db->where("sp.cust_id",$data["customer_id"]);
	    	}
	    	/*if(isset($data["business_name"]) && $data["business_name"]!=''){ 
	    		$this->db->like("s.business_name",$data["business_name"]);
	    	}*/
	    	if(isset($data["city"]) && $data["city"]!=''){ 
	    		$this->db->like("sp.city",$data["city"]);
	    	}

	    	// $this->db->get();
	    	// echo $this->db->last_query();
	    	
	    	$db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query();
	        	return $this->db->count_all_results();

	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
    }

    public function getSellers($data='',$limit='', $start=''){
    	try {
    		$this->db->db_debug = false;
    		$this->db->select("c.name as seller, c.email, c.displayLocal, c.id, sp.id as seller_id, sp.business_name, sp.city, sp.contact_number, sp.active");
    		$this->db->from($this->sellerTable . ' sp');
    		$this->db->join($this->custTable . ' c', 'ON sp.cust_id = c.id', 'LEFT');

    		if(isset($data["customer_id"]) && $data["customer_id"]!=''){ 
	    		$this->db->where("sp.cust_id",$data["customer_id"]);
	    	}
	    	/*if(isset($data["business_name"]) && $data["business_name"]!=''){ 
	    		$this->db->like("business_name",$data["business_name"]);
	    	}*/
	    	if(isset($data["city"]) && $data["city"]!=''){ 
	    		$this->db->like("s.city",$data["city"]);
	    	}

    		$this->db->order_by('sp.business_name', 'ASC'); 

    		if ($limit != '') {
    			$this->db->limit($limit, $start);
    		}	    	
    		
    		$query=$this->db->get();

	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query();
	        	$result=[];
	        	foreach($query->result_array() as $row){
	        		$result[]=$row;
	        	}
	        	return $result;
	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
    }

    
    public function getCustomer(){
    	try {
    		$this->db->db_debug = false;

    		//SELECT c.id, c.name FROM t_customer c, t_buyer_profile b WHERE c.id = b.cust_id

    		$this->db->select("c.id, c.name");
    		$this->db->from($this->custTable . ' c');
    		$this->db->from($this->sellerTable . ' s');
    		$this->db->where('c.active', 1);
    		$this->db->where('c.id = s.cust_id');
    		
    		$query=$this->db->get();

	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query();
	        	$result=[];
	        	foreach($query->result_array() as $row){
	        		$result[]=$row;
	        	}
	        	return $result;
	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
    }

    public function doSellerCheck($cdata='', $bdata='', $id=''){
    	//print_r($data); die();
		try {
		 	$this->db->db_debug = FALSE;

		 	if(isset($cdata) && trim($cdata)!='' ){
		 		$this->db->select("cust_id");
		 	}

		 	if(isset($bdata) && trim($bdata)!='' ){
	    		$this->db->select("business_name");
	    	}

	        $this->db->from($this->sellerTable);
	    	
	    	if(isset($cdata) && trim($cdata)!='' ){
	    		$this->db->where("cust_id",$cdata);
	    		
	    	}

	    	if(isset($bdata) && trim($bdata)!='' ){
	    		$this->db->where("business_name",$bdata);
	    		
	    	}

	    	if($id!=''){
	    		$this->db->where("id !=",$id);
	    	}
	    	
	    	$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query() . '<br>';
	        	if($query->num_rows() >0) {
	        		return True;
	        	}
	        	else {
	        		return false;
	        	}
	        }
	        //return TRUE;
	    } catch (Exception $e) {
	       throw new Exception();
	    }	
    }	

    public function insertSeller($data) {
    	try {
    	 	$this->db->db_debug = true;
    	 	$res = $this->db->insert($this->sellerTable, $data);

    	 	$db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }
	        else {
	        	//echo $this->db->last_query() . '<br>';
	        	if ($res) {
	        		return  $this->db->insert_id();
	        	}
	        	else {
	        		return false;
	        	}
	        }
	        //return  $this->db->insert_id();
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }
    }

    public function insertSellerImg($data){
    	try{
    		$this->db->db_debug = false;

    		$insert = $this->db->insert($this->sellerImageTable, $data);

    		$db_error = $this->db->error();
    		if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	return $insert?true:false;
	        }
    	}
    	catch(Exception $e) {
    		throw new Exception("Error Processing Request", 1);    		
    	}
    }

    public function getById($id){
    	try {
		 	$this->db->db_debug = false;

		 	//	SELECT c.name as seller, c.email, sp.id, sp.business_name, sp.state, sp.city, sp.contact_number, sp.active, si.image, si.type FROM t_seller_profile sp LEFT JOIN t_customer c ON c.id = sp.cust_id LEFT JOIN t_seller_image si ON si.seller_id = sp.id WHERE sp.id = 2

		 	$this->db->select("c.name as seller, c.email, sp.id, sp.business_name, sp.address, sp.state, sp.city, sp.contact_name, sp.contact_number, sp.active, si.id AS img_id, si.image, si.type");
    		$this->db->from($this->sellerTable . ' sp');
    		$this->db->join($this->custTable . ' c', 'ON sp.cust_id = c.id', 'LEFT');
    		$this->db->join($this->sellerImageTable . ' si', 'ON sp.id = si.seller_id', 'LEFT');

    		$this->db->where("sp.id", $id);
	    	
	    	$query=$this->db->get();

	    	$db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	$data=[];
	        	foreach($query->result_array() as $row){
	        		$simg = $row['id'] . '_img';

	        		if (!isset($data[$simg])) {
	        			$data[$simg]['id'] = $row['id'];
	        			$data[$simg]['seller'] = $row['seller'];
	        			$data[$simg]['business_name'] = $row['business_name'];
	        			$data[$simg]['address'] = $row['address'];
	        			$data[$simg]['state'] = $row['state'];
	        			$data[$simg]['city'] = $row['city'];
	        			$data[$simg]['contact_name'] = $row['contact_name'];
	        			$data[$simg]['contact_number'] = $row['contact_number'];
	        			$data[$simg]['image'] = [];
	        		}

	        		$get_ext = pathinfo($row['image'])['extension'];

	        		$data[$simg]['image'][] = ['img_id' => $row['img_id'], 'image' => $row['image'], 'type' => $row['type'], 'ext' => $get_ext ];

	        	}

	        	//echo $this->db->last_query(); die();
	        	// print_r($data); die();
	        	return array_values($data);
	        }
	    } catch (Exception $e) {
	       throw new Exception();
	    }	
    }

    public function getAgentById($id){
    	try {
		 	$this->db->db_debug = false;

		 	//SELECT ad.id, ad.cust_id, ad.document, ad.document_type, c.name AS customer FROM t_agent_document ad LEFT JOIN t_customer c ON ad.cust_id = c.id WHERE ad.cust_id =2

		 	$this->db->select("ad.id, ad.cust_id, ad.document, ad.document_type, c.name AS customer, c.agent_YN");
    		$this->db->from($this->agentDocuTable . ' ad');
    		$this->db->join($this->custTable . ' c', 'ON ad.cust_id = c.id', 'LEFT');

    		$this->db->where("ad.cust_id", $id);
	    	
	    	$query=$this->db->get();

	    	$db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	$data=[];
	        	foreach($query->result_array() as $row){
	        		$adocu = $row['cust_id'] . '_img';

	        		if (!isset($data[$adocu])) {
	        			$data[$adocu]['id'] = base64_encode($row['id']);
	        			$data[$adocu]['cust_id'] = base64_encode($row['cust_id']);
	        			$data[$adocu]['customer'] = $row['customer'];
	        			$data[$adocu]['agent_YN'] = $row['agent_YN'];
	        			$data[$adocu]['document'] = [];
	        		}

	        		$data[$adocu]['document'][] = ['name' => $row['document'], 'type' => $row['document_type']];
	        	}

	        	//echo $this->db->last_query(); die();
	        	//print_r(array_values($data)); die();
	        	return array_values($data);
	        }
	    } catch (Exception $e) {
	       throw new Exception();
	    }	
    }

    public function updateSeller($data, $where='', $flag=''){
    	try {
    	 	$this->db->db_debug = False;

    	 	if ($flag == 'ban') {
    	 		$res = $this->db->update($this->sellerTable, $data, ['cust_id' => $where]);
    	 	}
    	 	else {
    	 		$res = $this->db->update($this->sellerTable, $data, ['id' => $where]);
    	 	}    	 	

    	 	//echo $this->db->last_query(); die();

    	 	$result = $this->db->select('cust_id')->from($this->sellerTable)->where('id', $where)->get();

    	 	// echo $this->db->last_query(); die();

    	 	$res = current($result->result());
    	 	// print_r(current($res)); die();
    	 	

	    	$db_error = $this->db->error();
	        if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }
	        else {
	        	if ($res) {
	        		return current($res);
	        	}
	        	else {
	        		return False;
	        	}
	        }
	        
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }
    }

    public function updateAgent($data, $where=''){
    	try {
    	 	$this->db->db_debug = False;
    	 	$res = $this->db->update($this->custTable, $data, ['id' => $where]);

    	 	//echo $this->db->last_query(); die();
    	 	

	    	$db_error = $this->db->error();
	        if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }
	        else {
	        	if ($res) {
	        		return True;
	        	}
	        	else {
	        		return False;
	        	}
	        }
	        
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }
    }

    public function getImgById($where) {
    	try {
    	 	$this->db->db_debug = False;

    	 	$this->db->select("image");
    		$this->db->from($this->sellerImageTable);
    		$this->db->where("id", $where);
	    	
	    	$query=$this->db->get();

	    	$db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	$result=[];
	        	foreach($query->result_array() as $row){
	        		$result[]=$row;
	        	}
	        	return $result;
	        }
	        
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }
    }

    public function dltSellerImg($where) {
    	try {
    	 	$this->db->db_debug = False;
    	 	$res = $this->db->delete($this->sellerImageTable, ['id' => $where]);

    	 	$db_error = $this->db->error();

	        if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }
	        else {
	        	if ($res) {
	        		return True;
	        	}
	        	else {
	        		return False;
	        	}
	        }
	        
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }
    }
}

?>