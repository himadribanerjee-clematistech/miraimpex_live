<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class CustomerModel extends CI_Model {
	protected $custTable = 'customer';
	
	public function __construct() {
        parent::__construct();
    }
    
    
    public function getCustCount($data){
    	//print_r($data); die();
    	try {
    		$this->db->db_debug = false;
    		$this->db->select("id, displayLocal, username, name, email, email_verify, phone, phone_verify, supplier_YN, active");
    		$this->db->from($this->custTable);
    		//$this->db->where("username IS NOT NULL");
    		if(isset($data["name"]) && $data["name"]!=''){ 
	    		$this->db->like("name",$data["name"]);
	    	}
	    	if(isset($data["username"]) && $data["username"]!=''){ 
	    		$this->db->like("username",$data["username"]);
	    	}
	    	if(isset($data["email"]) && $data["email"]!=''){ 
	    		$this->db->like("email",$data["email"]);
	    	}
	    	if(isset($data["active"]) && $data["active"]!=''){ 
	    		$this->db->where("active",$data["active"]);
	    	}

	    	//$this->db->get();
	    	
	    	$db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query();die();
	        	return $this->db->count_all_results();

	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
    }
    public function getCustList($data='',$limit='', $start=''){
    	try {
    		$this->db->db_debug = false;
    		$this->db->select("id, displayLocal, username, name, email, email_verify, phone, phone_verify, supplier_YN, active");
    		$this->db->from($this->custTable);
    		//$this->db->where("username IS NOT NULL");
	    	if(isset($data["name"]) && $data["name"]!=''){ 
	    		$this->db->like("name",$data["name"]);
	    	}
	    	if(isset($data["username"]) && $data["username"]!=''){ 
	    		$this->db->like("username",$data["username"]);
	    	}
	    	if(isset($data["email"]) && $data["email"]!=''){ 
	    		$this->db->like("email",$data["email"]);
	    	}
	    	if(isset($data["active"]) && $data["active"]!=''){ 
	    		$this->db->where("active",$data["active"]);
	    	}
	    	
	    	$this->db->order_by('name', 'ASC'); 

	    	if ($limit != '') {
	    		$this->db->limit($limit, $start);
	    	}
	    	
 
 			$query=$this->db->get();
	        $db_error = $this->db->error();
	        // echo $this->db->last_query();die();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	// echo $this->db->last_query();die();
	        	$result=[];
	        	foreach($query->result_array() as $row){
	        		$result[]=$row;
	        	}
	        	return $result;
	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
    }	

    public function doCustCheck($data, $id=''){
    	//print_r($data); die();
		try {
		 	$this->db->db_debug = FALSE;
	        $this->db->select("name");
	    	$this->db->from($this->custTable);
	    	if(isset($data) && trim($data)!='' ){
	    		$this->db->where("name",$data);
	    		
	    	}
	    	//$this->db->where("username IS NOT NULL");
	    	if($id!=''){
	    		$this->db->where("id !=",$id);
	    	}
	    	
	    	$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query() . '<br>';
	        	if($query->num_rows() >0) {
	        		return True;
	        	}
	        	else {
	        		return false;
	        	}
	        }
	        //return TRUE;
	    } catch (Exception $e) {
	       throw new Exception();
	    }	
    }
     
    public function insertCust($data){
     	//print_r($data); die();
    	try {
    	 	$this->db->db_debug = FALSE;
    	 	$res = $this->db->insert($this->custTable, $data);

    	 	$db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }
	        else {
	        	//echo $this->db->last_query() . '<br>';
	        	if ($res) {
	        		return true;
	        	}
	        	else {
	        		return false;
	        	}
	        }
	        //return  $this->db->insert_id();
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }
    	
    }

    public function getById($id){
    	try {
		 	$this->db->db_debug = false;
	        $this->db->select("id, displayLocal, username, name, email, phone, created_date");
	    	$this->db->from($this->custTable);
	    	//$this->db->where("username IS NOT NULL");
	    	$this->db->where("id", $id);
	    	
	    	$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	$data=[];
	        	foreach($query->result_array() as $row){
	        		//print_r($row);
	        		$c_data = $row['id'] . '_c';
	        		
	        		if (!isset($data[$c_data])) {
	        			$data[$c_data]['id'] = $row['id'];
	        			$data[$c_data]['username'] = $row['username'];
	        			$data[$c_data]['name'] = $row['name'];
	        			$data[$c_data]['email'] = $row['email'];
	        			$data[$c_data]['phone'] = $row['phone'];
	        			$data[$c_data]['created_date'] = date('d-m-Y H:i:s', strtotime($row['created_date']));
	        		}
	    	
	        	}
	        	//echo $this->db->last_query();
	        	//print_r($data); die();
	        	return array_values($data);
	        }
	        return TRUE;
	    } catch (Exception $e) {
	       throw new Exception();
	    }	
    }

    public function updateCust($data,$where){
    	
    	try {
    	 	$this->db->db_debug = False;
    	 	$res = $this->db->update($this->custTable, $data, ['id' => $where]);

	    	$db_error = $this->db->error();
	        if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }
	        else {

	        	if ($res) {
	        		return True;
	        	}
	        	else {
	        		return False;
	        	}
	        }
	        
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }
    }

    /*public function doDeleteUom($where){
    	
    	try {
    	 	$this->db->db_debug = False;
    	 	$res = $this->db->delete($this->custTable, ['id' => $where]);

	    	$db_error = $this->db->error();
	        if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }
	        else {
	        	if ($res) {
	        		return True;
	        	}
	        	else {
	        		return False;
	        	}
	        }
	        
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }
    }*/

    public function getEmailId($id){
    	try {
		 	$this->db->db_debug = false;
	        $this->db->select("name, email");
	    	$this->db->from($this->custTable);
	    	//$this->db->where("username IS NOT NULL");
	    	$this->db->where("id", $id);
	    	
	    	$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	$data=[];
	        	foreach($query->result_array() as $row){
	        		$data = $row;
	        	}
	        	//echo $this->db->last_query();
	        	//print_r($data); die();
	        	return $data;
	        }
	        return TRUE;
	    } catch (Exception $e) {
	       throw new Exception();
	    }	
    }
   	
}

?>