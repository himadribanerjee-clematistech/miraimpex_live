<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class SubcategoryModel extends CI_Model {
	protected $userTable = 'user';
	protected $catTable = 'category';
	
	public function __construct() {
        parent::__construct();
    }
    
    public function getUserDetailsbyId($id){
    	 try {
		 	$this->db->db_debug = false;
	        $this->db->select("u.user_name,u.id as user_id,u.email,u.mobile,u.Name,u.actve");
	    	$this->db->from($this->userTable." u");
	    	
	    	$this->db->where("u.token_id",$id);
	    	$this->db->where("u.actve",1);
	    	
	    	$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	$data=[];
	        	foreach($query->result_array() as $row){
	        		 $data[]=$row;
	    	
	        	}
	        	//echo $this->db->last_query();
	        	return $data;
	        }
	        return TRUE;
	    } catch (Exception $e) {
	       throw new Exception();
	    }	
    }
    public function getsubCatCount($data, $id){
    	//print_r($data); die();
    	try {
    		$this->db->db_debug = false;
    		$this->db->select("u.id,u.category_name,u.active");
    		$this->db->from($this->catTable." u");
    		if(isset($data["subcat_name"]) && $data["subcat_name"]!=''){ 
	    		$this->db->like("u.category_name",$data["subcat_name"]);
	    	}
	    	if(isset($data["active"]) && $data["active"]!=''){
	    		$this->db->where("u.active",$data["active"]);
	    	}
	    	$this->db->where('parent_id', $id);

	    	$db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query();die();
	        	return $this->db->count_all_results();

	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
    }

    public function getCatName($id){
    	try {
    		$this->db->db_debug = false;
    		$this->db->select("category_name");
    		$this->db->from($this->catTable);
    		
	    	$this->db->where('id', $id);
	    	
 			$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query();die();
	        	foreach ($query->row() as $t) {
				    return $t;
				}
				
	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
	}

     public function getsubCatList($data,$limit='', $start=0, $id){
    	try {
    		$this->db->db_debug = false;
    		$this->db->select("u.id,u.category_name,u.active");
    		$this->db->from($this->catTable." u");
    		
	    	if(isset($data["subcat_name"]) && $data["subcat_name"]!=''){
	    		$this->db->like("u.category_name",$data["subcat_name"]);
	    	}
	    	if(isset($data["active"]) && $data["active"]!=''){
	    		$this->db->where("u.active",$data["active"]);
	    	}
	    	$this->db->where('parent_id', $id);
	    	$this->db->order_by('u.category_name', 'ASC'); 
	    	if(trim($limit)!='' && trim($start)>=0){
	    		$this->db->limit($limit, $start);
	    	}
	    	
 
 			$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query();die();
	        	$result=[];
	        	foreach($query->result_array() as $row){
	        		$result[]=$row;
	        	}
	        	return $result;
	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
    }	
   	
}
