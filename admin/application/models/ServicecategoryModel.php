<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class ServicecategoryModel extends CI_Model {
	protected $servicecatTable = 'service_category';
	protected $uomservicecatTable = 'service_cat_uom_relation';
	protected $uomTable = 'uom';
	
	public function __construct() {
        parent::__construct();
    }
     
    
    public function getServiceCatCount($data){
    	//print_r($data); die();
    	try {
    		$this->db->db_debug = false;
    		$this->db->select("u.id,u.service_category_name,u.image");
    		$this->db->from($this->servicecatTable." u");
    		if(isset($data["service_category_name"]) && $data["service_category_name"]!='') {
	    		$this->db->like("u.service_category_name",$data["service_category_name"]);
	    	}
	    	if(isset($data["active"]) && $data["active"]!=''){ 
	    		$this->db->where("u.active",$data["active"]);
	    	}
	    	$this->db->where('parent_id', 0);
	    	//$this->db->get();
	    	
	    	$db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query();die();
	        	return $this->db->count_all_results();

	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
    }
    public function getServiceCatList($data,$limit='', $start=0){
    	try {
    		$this->db->db_debug = false;
    		$this->db->select("u.id,u.service_category_name,u.image,u.active");
    		$this->db->from($this->servicecatTable." u");
    		
	    	if(isset($data["service_category_name"]) && $data["service_category_name"]!='') {
	    		$this->db->like("u.service_category_name",$data["service_category_name"]);
	    	}
	    	if(isset($data["active"]) && $data["active"]!=''){ 
	    		$this->db->where("u.active",$data["active"]);
	    	}

	    	$this->db->where('parent_id', 0);
	    	$this->db->order_by('u.service_category_name', 'ASC');
	    	if($limit!='' && $start>=0){
	    		$this->db->limit($limit, $start);
	    	} 
	    	
 
 			$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query();die();
	        	$result=[];
	        	foreach($query->result_array() as $row){
	        		$result[]=$row;
	        	}
	        	return $result;
	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
    }	

    public function doServiceCatCheck($data, $id='', $parent=''){
    	//print_r($data); die();
		try {
		 	$this->db->db_debug = FALSE;
	        $this->db->select("service_category_name");
	    	$this->db->from($this->servicecatTable);
	    	if(isset($data) && trim($data)!='' ){
	    		$this->db->where("service_category_name",$data);
	    		
	    	}

	    	if($id!=''){
	    		$this->db->where("id !=",$id);
	    	}
	    	if ($parent != '') {
	    		$this->db->where('parent_id', $parent);
	    	}
	    	
	    	$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query() . '<br>';
	        	if($query->num_rows() >0) {
	        		return True;
	        	}
	        	else {
	        		return false;
	        	}
	        }
	        //return TRUE;
	    } catch (Exception $e) {
	       throw new Exception();
	    }	
    }

    
    public function insertServiceCat($data){
     	//print_r($data); die();
    	try {
    	 	$this->db->db_debug = FALSE;

    	 	if ($data['parent_id'] != '') {
    			$cat_data = [
	                'service_category_name' => $data['service_category_name'], 
	                'image' => $data['image'],
	                'parent_id' => $data['parent_id'],
	                'slug'	=> $data['slug']
	            ];
    		}
    		else {
    			$cat_data = [
	                'service_category_name' => $data['service_category_name'], 
	                'image' => $data['image'],
	                'slug'	=> $data['slug']
	            ];
    		} 

    		//print_r($cat_data); die();

    	 	$res = $this->db->insert($this->servicecatTable, $cat_data);

    	 	$last_id = $this->db->insert_id();

            if (!empty($data['uom_id'])) {
            	$uom = explode(',' , $data['uom_id']);
	            $count_uom = count($uom);

	            for ($i = 0; $i < $count_uom; $i++) { 
	            	$uom_cat = ['cat_id' => $last_id, 'uom_id' => base64_decode($uom[$i])];

	            	$res2 = $this->db->insert($this->uomservicecatTable, $uom_cat);
	            }
            }

    	 	//echo $this->db->last_query(); die();

    	 	$db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }
	        else {
	        	//echo $this->db->last_query() . '<br>';
	        	if (!empty($data['uom_id'])) {
	        		if ($res2) {
		        		return true;
		        	}
		        	else {
		        		return false;
		        	}
	        	}
	        	else {
	        		if ($res) {
		        		return true;
		        	}
		        	else {
		        		return false;
		        	}
	        	}
	        }
	        //return  $this->db->insert_id();
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }
    	
    }

    public function getById($id){
    	//echo $id; die();
    	try {
		 	$this->db->db_debug = false;

		 	// SELECT sc.service_category_name, sc.image, u.name, scur.uom_id FROM t_service_category sc LEFT JOIN t_service_cat_uom_relation scur ON scur.cat_id = sc.id LEFT JOIN t_uom u ON u.id = scur.uom_id WHERE sc.id = 1

	        $this->db->select("sc.id, sc.service_category_name, sc.image, u.name, scur.uom_id");
	    	$this->db->from($this->servicecatTable . ' sc');
	    	$this->db->join($this->uomservicecatTable . ' scur' , 'ON scur.cat_id = sc.id', 'LEFT');
	    	$this->db->join($this->uomTable . ' u' , 'ON u.id = scur.uom_id', 'LEFT');
	    	
	    	$this->db->where("sc.id", $id);
	    	
	    	$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	$data=[];
	        	foreach($query->result_array() as $row){
	        		$rcat = $row["id"]."_cat";

	        		if(!isset($data[$rcat])){
	        			$data[$rcat]["id"]=$row["id"];
	        			$data[$rcat]["service_category_name"]=$row["service_category_name"];
	        			$data[$rcat]["image"]=$row["image"];
	        			$data[$rcat]["uom"]=[];
	        			
	        		}
	        		$data[$row["id"]."_cat"]["uom"][]=["uom_id"=>$row["uom_id"]];    	
	        	}

	        	//print_r(array_values($data)); die();
	        	//echo $this->db->last_query();
	        	return array_values($data);
	        }
	        return TRUE;
	    } catch (Exception $e) {
	       throw new Exception();
	    }	
    }

    public function updateServiceCat($data,$where){
    	//print_r($data); die();
    	try {
    	 	$this->db->db_debug = False;

    	 	$cat_data = [ 
    	 		'service_category_name' => $data['service_category_name'],  
    	 		'image' => $data['image'],
	            'slug'	=> $data['slug']
    	 	];

    	 	$res = $this->db->update($this->servicecatTable, $cat_data, ['id' => $where]);

    	 	if ($res) {
    	 		if ($data['uom_id'] != '') {
    	 			$uom = explode(',' , $data['uom_id']);
		            $count_uom = count($uom);

		            $this->db->delete($this->uomservicecatTable, ['cat_id' => $where]);

		            for ($i = 0; $i < $count_uom; $i++) { 
		                $uom_cat = ['cat_id' => $where, 'uom_id' => base64_decode($uom[$i])];

		                $res2 = $this->db->insert($this->uomservicecatTable, $uom_cat);
		            }
	            }
    	 	}

	        //echo $this->db->last_query(); die();
	        
	        $db_error = $this->db->error();
	        if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	           
	        }
	        else {
	        	if (!empty($data['uom_id'])) {
	        		if ($res2) {
		        		return true;
		        	}
		        	else {
		        		return false;
		        	}
	        	}
	        	else {
	        		if ($res) {
		        		return true;
		        	}
		        	else {
		        		return false;
		        	}
	        	}
	        }
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }

    	
    }

    /*public function doDeleteCat($where){
    	
    	try {
    	 	$this->db->db_debug = False;
    	 	$res = $this->db->delete($this->servicecatTable, ['id' => $where]);

	    	$db_error = $this->db->error();
	        if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }
	        else {
	        	if ($res) {
	        		return True;
	        	}
	        	else {
	        		return False;
	        	}
	        }
	        
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }
    }*/
   	
}

?>