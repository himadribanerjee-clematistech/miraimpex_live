<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class BuyerModel extends CI_Model {
	protected $custTable = 'customer';
	protected $buyerTable = 'buyer_profile';
	protected $buyerImgTable = 'buyer_image';
	
	public function __construct() {
        parent::__construct(); 
    } 

    public function getBuyerCount($data){
    	//print_r($data); die();
    	try {
    		$this->db->db_debug = false;

    		// SELECT c.name, c.email, bp.business_name, bp.state, bp.city, bp.active FROM t_customer c LEFT JOIN t_buyer_profile bp ON c.id = bp.cust_id

    		$this->db->select("bp.id, c.name as cust_name, c.email, bp.business_name, bp.state, bp.city, bp.active");
    		$this->db->from($this->buyerTable . ' bp');
    		$this->db->join($this->custTable . ' c', 'ON bp.cust_id = c.id', 'LEFT');

    		if(isset($data["cust_id"]) && $data["cust_id"]!=''){ 
	    		$this->db->where("bp.cust_id",$data["cust_id"]);
	    	}
	    	/*if(isset($data["business_name"]) && $data["business_name"]!=''){ 
	    		$this->db->like("business_name",$data["business_name"]);
	    	}*/
	    	if(isset($data["city"]) && $data["city"]!=''){ 
	    		$this->db->like("bp.city",$data["city"]);
	    	}

	    	//$this->db->get();
	    	//echo $this->db->last_query();die();
	    	
	    	$db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query();die();
	        	return $this->db->count_all_results();

	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
    }

    public function getBuyers($data='',$limit='', $start=''){
    	try {
    		$this->db->db_debug = false;
    		$this->db->select("bp.id, c.name as cust_name, c.email, bp.business_name, bp.state, bp.city, bp.active");
    		$this->db->from($this->buyerTable . ' bp');
    		$this->db->join($this->custTable . ' c', 'ON bp.cust_id = c.id', 'LEFT');

    		if(isset($data["cust_id"]) && $data["cust_id"]!=''){ 
	    		$this->db->where("bp.cust_id",$data["cust_id"]);
	    	}
	    	/*if(isset($data["business_name"]) && $data["business_name"]!=''){ 
	    		$this->db->like("business_name",$data["business_name"]);
	    	}*/
	    	if(isset($data["city"]) && $data["city"]!=''){ 
	    		$this->db->like("bp.city",$data["city"]);
	    	}

    		$this->db->order_by('bp.business_name', 'ASC'); 

    		if ($limit != '') {
    			$this->db->limit($limit, $start);
    		}	    	
    		
    		$query=$this->db->get();

	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query(); die();
	        	$result=[];
	        	foreach($query->result_array() as $row){
	        		$result[]=$row;
	        	}
	        	return $result;
	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
    }

    
    public function getCustomer(){
    	try {
    		$this->db->db_debug = false;

    		//SELECT c.id, c.name FROM t_customer c, t_buyer_profile b WHERE c.id = b.cust_id

    		$this->db->select("c.id, c.name");
    		$this->db->from($this->custTable . ' c');
    		$this->db->from($this->buyerTable . ' b');
    		$this->db->where('c.active', 1);
    		$this->db->where('c.id = b.cust_id');
    		
    		$query=$this->db->get();

	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query(); die();
	        	$result=[];
	        	foreach($query->result_array() as $row){
	        		$result[]=$row;
	        	}
	        	return $result;
	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
    }	

    public function doBuyerCheck($cdata='', $bdata='', $id=''){
    	//print_r($data); die();
		try {
		 	$this->db->db_debug = FALSE;

		 	if(isset($cdata) && trim($cdata)!='' ){
		 		$this->db->select("cust_id");
		 	}

		 	if(isset($bdata) && trim($bdata)!='' ){
	    		$this->db->select("business_name");
	    	}

	        $this->db->from($this->buyerTable);
	    	
	    	if(isset($cdata) && trim($cdata)!='' ){
	    		$this->db->where("cust_id",$cdata);
	    		
	    	}

	    	if(isset($bdata) && trim($bdata)!='' ){
	    		$this->db->where("business_name",$bdata);
	    		
	    	}

	    	if($id!=''){
	    		$this->db->where("id !=",$id);
	    	}
	    	
	    	$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query() . '<br>';
	        	if($query->num_rows() >0) {
	        		return True;
	        	}
	        	else {
	        		return false;
	        	}
	        }
	        //return TRUE;
	    } catch (Exception $e) {
	       throw new Exception();
	    }	
    }

    public function insertBuyer($data) {
    	try {
    	 	$this->db->db_debug = true;
    	 	$res = $this->db->insert($this->buyerTable, $data);

    	 	$db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }
	        else {
	        	//echo $this->db->last_query() . '<br>';
	        	if ($res) {
	        		return true;
	        	}
	        	else {
	        		return false;
	        	}
	        }
	        //return  $this->db->insert_id();
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }
    }

    public function getById($id){
    	try {
		 	$this->db->db_debug = false;

		 	// SELECT c.name, bp.business_name, bp.state, bp.city, bp.active, bi.image FROM t_buyer_profile bp LEFT JOIN t_customer c ON c.id = bp.cust_id LEFT JOIN t_buyer_image bi ON bp.id = bi.buyer_id

	     	// $this->db->select("c.name as cust_name, bp.business_name, bp.state, bp.city, bp.address, bp.active, bi.image");
    		// $this->db->from($this->buyerTable . ' bp');
    		// $this->db->join($this->custTable . ' c', 'ON bp.cust_id = c.id', 'LEFT');
    		// $this->db->join($this->buyerImgTable . ' bi', 'ON bp.id = bi.buyer_id', 'LEFT');


    		// SELECT c.name, c.email, bp.business_name, bp.business_logo, bp.state, bp.city, bp.active FROM t_customer c LEFT JOIN t_buyer_profile bp ON c.id = bp.cust_id WHERE bp.id = ''

    		$this->db->select("bp.id, c.name as cust_name, c.email, bp.business_name, bp.address, bp.business_logo, bp.state, bp.city, bp.active");
    		$this->db->from($this->buyerTable . ' bp');
    		$this->db->join($this->custTable . ' c', 'ON bp.cust_id = c.id', 'LEFT');
	    	
	    	$this->db->where("bp.id", $id);
	    	
	    	$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query(); die();
	        	$data=[];
	        	foreach($query->result_array() as $row){
	        		$data[]=$row;	    	
	        	}
	        	
	        	return $data;
	        }
	        return TRUE;
	    } catch (Exception $e) {
	       throw new Exception();
	    }	
    }

    public function updateBuyer($data, $where=''){
    	try {
    	 	$this->db->db_debug = False;
    	 	$res = $this->db->update($this->buyerTable, $data, ['id' => $where]);

    	 	

    	 	$result = $this->db->select('cust_id')->from($this->buyerTable)->where('id', $where)->get();

    	 	// echo $this->db->last_query(); die();

    	 	$res = current($result->result());
    	 	// print_r(current($res)); die();

    	 	

	    	$db_error = $this->db->error();
	        if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }
	        else {
	        	if ($res) {
	        		return current($res);
	        	}
	        	else {
	        		return False;
	        	}
	        }
	        
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }
    }
}

?>