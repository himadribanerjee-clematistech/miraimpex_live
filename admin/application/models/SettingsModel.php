<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class SettingsModel extends CI_Model {
	protected $settingsTable = 'setting';
	
	public function __construct() {
        parent::__construct();
    }
    
    public function getSettings(){
    	try {
    		$this->db->db_debug = false;
    		$this->db->select("id, company_name, company_logo, company_mobile, company_fax, company_address, per_page, notification_time, news_time, information_time, rfq_closing, delivery_charges");
    		$this->db->from($this->settingsTable);
    		
    		$query=$this->db->get();

	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	return $query->row();
	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
	}
	
	public function getExchangerate(){
    	try {
    		$this->db->db_debug = false;
    		$this->db->select("base_currency, to_currency, rate, updated_date");
    		$this->db->from('t_rate_exchange');
    		
    		$query=$this->db->get();

	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	return $query->result_array();
	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
    }

    public function getById($id){
    	try {
		 	$this->db->db_debug = false;
	        $this->db->select("id, company_name, company_logo, company_mobile, company_fax, company_address, per_page, notification_time, news_time, information_time, rfq_closing, delivery_charges");
	    	$this->db->from($this->settingsTable);
	    	
	    	$this->db->where("id", $id);
	    	
	    	$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	$data=[];
	        	foreach($query->result_array() as $row){
	        		$data[]=$row;	    	
	        	}
	        	//echo $this->db->last_query();
	        	return $data;
	        }
	        return TRUE;
	    } catch (Exception $e) {
	       throw new Exception();
	    }	
    }

    public function updateSetting($data, $where=''){
    	try {
    	 	$this->db->db_debug = False;

    	 	if ($where != '') {
    	 		$res = $this->db->update($this->settingsTable, $data, ['id' => $where]);
    	 	}
    	 	else {
    	 		$res = $this->db->insert($this->settingsTable, $data);
    	 	}

    	 	

	    	$db_error = $this->db->error();
	        if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }
	        else {
	        	if ($res) {
	        		return True;
	        	}
	        	else {
	        		return False;
	        	}
	        }
	        
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }
	}
	

	public function updateExchangerate($data, $where=''){
    	try {
			 $this->db->db_debug = False;
			

    	 	if ($data['base_currency'] == 'USD' && $data['rate']>0) {
    	 		$res = $this->db->update('t_rate_exchange', $data, ['base_currency' => $data['base_currency']]);
    	 	}   	 	

    	 	

	    	$db_error = $this->db->error();
	        if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }
	        else {
	        	if ($res) {
	        		return True;
	        	}
	        	else {
	        		return False;
	        	}
	        }
	        
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }
    }
}

?>