<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class AttributeModel extends CI_Model {
	protected $attributeTable = 'attribute';
	
	public function __construct() {
        parent::__construct();
    }
    
    public function getAttributeCount($data){
    	//print_r($data); die();
    	try {
    		$this->db->db_debug = false;
    		$this->db->select("id, name_en, name_fr, type, status");
    		$this->db->from($this->attributeTable);
    		if(isset($data["name_en"]) && $data["name_en"]!=''){ 
	    		$this->db->like("name_en",$data["name_en"]);
	    	}
	    	if(isset($data["type"]) && $data["type"]!=''){ 
	    		$this->db->where("type",$data["type"]);
	    	}
	    	if(isset($data["status"]) && $data["status"]!=''){ 
	    		$this->db->where("status",$data["status"]);
	    	}

	    	//$this->db->get();
	    	
	    	$db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query();die();
	        	return $this->db->count_all_results();

	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
    }
    public function getAttributeList($data='',$limit='', $start=''){
    	try {
    		$this->db->db_debug = false;
    		$this->db->select("id, name_en, name_fr, type, status");
    		$this->db->from($this->attributeTable);
    		if(isset($data["name_en"]) && $data["name_en"]!=''){ 
	    		$this->db->like("name_en",$data["name_en"]);
	    	}
	    	if(isset($data["type"]) && $data["type"]!=''){ 
	    		$this->db->where("type",$data["type"]);
	    	}
	    	if(isset($data["status"]) && $data["status"]!=''){ 
	    		$this->db->where("status",$data["status"]);
	    	}
	    	
	    	$this->db->order_by('name_en', 'ASC'); 

	    	if ($limit != '' ) {
	    		$this->db->limit($limit, $start);
	    	}
	    	
 
 			$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query();die();
	        	$result=[];
	        	foreach($query->result_array() as $row){
	        		$result[]=$row;
	        	}
	        	return $result;
	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
    }	

    public function doAttrCheck($data, $id=''){
    	//print_r($data); die();
		try {
		 	$this->db->db_debug = FALSE;
	        $this->db->select("name_en");
	    	$this->db->from($this->attributeTable);
	    	if(isset($data) && trim($data)!='' ){
	    		$this->db->where("name_en",$data);	    		
	    	}

	    	if($id!=''){
	    		$this->db->where("id !=",$id);
	    	}

	    	$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	// echo $this->db->last_query() . '<br>';
	        	if($query->num_rows() >0) {
	        		return True;
	        	}
	        	else {
	        		return false;
	        	}
	        }
	        //return TRUE;
	    } catch (Exception $e) {
	       throw new Exception();
	    }	
    }

    public function saveAttribute($data, $where=''){
    	try {
    	 	$this->db->db_debug = False;

    	 	if ($where != '') {
    	 		$res = $this->db->update($this->attributeTable, $data, ['id' => $where]);
    	 	}
    	 	else {
    	 		$res = $this->db->insert($this->attributeTable, $data);
    	 	}

    	 	

	    	$db_error = $this->db->error();
	        if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }
	        else {
	        	if ($res) {
	        		return True;
	        	}
	        	else {
	        		return False;
	        	}
	        }
	        
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }
    }

    public function getById($id){
    	try {
		 	$this->db->db_debug = false;
	        $this->db->select("id, name_en, name_fr, type, status");
	    	$this->db->from($this->attributeTable);
	    	
	    	$this->db->where("id", $id);
	    	
	    	$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	$data=[];
	        	foreach($query->result_array() as $row){
	        		 $data[]=$row;
	    	
	        	}
	        	// echo $this->db->last_query();
	        	return $data;
	        }
	        return TRUE;
	    } catch (Exception $e) {
	       throw new Exception();
	    }	
    }
}

?>