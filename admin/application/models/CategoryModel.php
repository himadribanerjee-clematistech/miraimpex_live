<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class CategoryModel extends CI_Model {
	protected $catTable = 'category';
	protected $sellerTable = 'seller_profile';
	protected $custTable = 'customer';
	protected $uomcatTable = 'cat_uom_relation';
	protected $uomTable = 'uom';

	protected $returnArr = [];
	
	public function __construct() {
        parent::__construct();
    }
     
    
    public function getCatCount($data){
    	//print_r($data); die();
    	try {
    		$this->db->db_debug = false;
    		$this->db->select("c.id, c.category_name_en,c.category_name_fr, c.image, c.active, c.request_seller_id, c.is_approved, cust.name");
    		$this->db->from($this->catTable." c");
    		$this->db->join($this->custTable . ' cust' , 'ON c.request_seller_id = cust.id', 'LEFT');
    		$this->db->group_by('c.id');

    		if(isset($data["cat_name"]) && $data["cat_name"]!=''){ //FRESH VEGETABLES
	    		$this->db->like("c.category_name_en",$data["cat_name"]);
	    	}

	    	if(isset($data["request_seller_id"]) && trim($data["request_seller_id"])!=''){
                $this->db->like("c.request_seller_id",$data["request_seller_id"]);
                
            }

            if(isset($data["is_approved"]) && trim($data["is_approved"])!=''){
                $this->db->like("c.is_approved",$data["is_approved"]);
                
            }

	    	if(isset($data["active"]) && $data["active"]!=''){
	    		$this->db->where("c.active",$data["active"]);
	    	}

	    	$this->db->where('c.parent_id', 0);
	    	
	    	$db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query();die();
	        	return $this->db->count_all_results();

	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
    }

    public function getCatList($data, $limit='', $parent_id=0, $start=0){
    	//print_r($data); 
    	try {
    		$this->db->db_debug = false;

    		$this->db->select("c.id, c.category_name_en, c.image, c.active, c.link_json, c.is_approved, c.type, c.parent_id");
    		$this->db->from($this->catTable." c");
    		
    		if(isset($data["cat_name"]) && $data["cat_name"]!=''){
	    		$this->db->like("c.category_name_en",$data["cat_name"]);
	    	}


            if(isset($data["is_approved"]) && trim($data["is_approved"])!=''){
                $this->db->like("c.is_approved",$data["is_approved"]);
                
            }

	    	if(isset($data["active"]) && $data["active"]!=''){
	    		$this->db->where("c.active",$data["active"]);
	    	}else{
				$this->db->where("c.active != 2");
			}

	    	$this->db->where('c.parent_id', $parent_id);
	    	$this->db->order_by('c.category_name_en', 'ASC');

			/*
	    	if($limit!='' && $start>=0 ){
	    		$this->db->limit($limit, $start);
	    	} 
	    	*/
 
 			$query=$this->db->get();
			$db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query();die();
	        	$result=[];
	        	foreach($query->result_array() as $row){					
					if($row['type'] == 1)
					{
						$result['Item Category'][]=$row;
					}elseif($row['type'] == 2)
					{
						$result['Service Category'][]=$row;
					}
	        		
				}
				
	        	return $result;
	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
    }	

	public function doCatCheckFr($data,$id='')
	{
		$this->db->db_debug = FALSE;
		$this->db->select("category_name_fr");
		$this->db->from($this->catTable);
		if(isset($data) && trim($data)!='' ){
			$this->db->where("category_name_fr",$data);
			
		}

		if($id!=''){
			$this->db->where("id !=",$id);
		}
		$query=$this->db->get();	
		$db_error = $this->db->error();
		if (!empty($db_error) && $db_error['code']>0) {
			throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
			return false; // unreachable retrun statement !!!
		}else{
					
			if($query->num_rows() >0) {
				return true;
			}
			else {
				return false;
			}
		}
		
	}

	public function getParentMapping($parent_id=0)
	{
		try {
			$result=[];
			 if($parent_id > 0)
			 {
				$this->db->db_debug = true;
				$this->db->select("c.id,c.category_name_en,c.category_name_fr,c.parent_id");
				$this->db->from('category c' );
				$this->db->where("c.active",1);
				$this->db->where("c.is_approved",1);
				$this->db->where("c.id",$parent_id);
				$query=$this->db->get();
				$db_error = $this->db->error();

				if (!empty($db_error) && $db_error['code']>0) 
				{
					throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
					return false; // unreachable retrun statement !!!
					
				}else{

					$listcat = $query->result_array();
					
					if(is_array($listcat) && count($listcat)>0)
					{
						$i=0;
						while($listcat[$i]){
							$this->returnArr[] = json_encode($listcat[$i]);
							$this->getParentMapping($listcat[$i]['parent_id']);
							$i++;
						}
					}
				}
			 }			

			return array_reverse($this->returnArr);
		}catch (Exception $e) {
	       throw new Exception();
	    }
		
	}

    public function doCatCheck($data,$id=''){
    	//print_r($data); die();
		try {
		 	$this->db->db_debug = FALSE;
	        $this->db->select("category_name_en");
	    	$this->db->from($this->catTable);
	    	if(isset($data) && trim($data)!='' ){
	    		$this->db->where("category_name_en",$data);
	    		
	    	}

	    	if($id!=''){
	    		$this->db->where("id !=",$id);
	    	}
	    	
	    	$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query() . '<br>';
	        	if($query->num_rows() >0) {
	        		return true;
	        	}
	        	else {
	        		return false;
	        	}
	        }
	        //return TRUE;
	    } catch (Exception $e) {
	       throw new Exception();
	    }	
	}
	
	public function checkProductsInParentCat($data){
    	//print_r($data); die();
		try {
		 	$this->db->db_debug = FALSE;
	        $this->db->select("id");
	    	$this->db->from('t_item');
	    	if(isset($data) && trim($data)!='' ){
	    		$this->db->where("category_id",$data);
	    		
	    	}
			$this->db->limit(1, 0);	    	
	    	$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
				//echo $this->db->last_query() . '<br>';
				return $query->num_rows();
				
	        }
	        //return TRUE;
	    } catch (Exception $e) {
	       throw new Exception();
	    }	
    }
     
    public function insertCat($data){
     	//print_r($data); die(); 
    	try {

    		$this->db->db_debug = TRUE;

            $res = $this->db->insert($this->catTable, $data);

            $last_id = $this->db->insert_id();

            if (!empty($data['uom_id'])) {
            	$uom = explode(',' , $data['uom_id']);
	            $count_uom = count($uom);

	            for ($i = 0; $i < $count_uom; $i++) { 
	            	$uom_cat = ['cat_id' => $last_id, 'uom_id' => base64_decode($uom[$i])];

	            	$res2 = $this->db->insert($this->uomcatTable, $uom_cat);
	            }
            }            

            //echo $this->db->last_query(); die();

    	 	$db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }
	        else {
	        	//echo $this->db->last_query() . '<br>';
	        	return  $this->db->insert_id();
	        	
	        }
	        
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }
    	
    }

  	
  	public function getById($id){
    	try {
		 	$this->db->db_debug = false;

		 	$this->db->select("c.id, c.displayLocal, c.category_name_en, c.category_name_fr, c.parent_id, c.type, c.image, c.is_approved, c.active, c.metadata, c.link_json, c.featured, c.display ");
    		$this->db->from($this->catTable." c");
	    	
	    	$this->db->where("c.id", $id);
	    	
	    	$query=$this->db->get();

	    	// echo $this->db->last_query(); die();

	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	return $listcat = $query->result_array();
	        }
	    } catch (Exception $e) {
	       throw new Exception();
	    }	
    }

    public function updateCat($data,$where){
    	//print_r($data); die();
    	try {
    	 	$this->db->db_debug = False;

    	 	
    	 	$res = $this->db->update($this->catTable, $data, ['id' => $where]);

    	 	if ($res) {
    	 		if (!empty($data['uom_id'])) {
	            	$uom = explode(',' , $data['uom_id']);
		            $count_uom = count($uom);

		            $this->db->delete($this->uomcatTable, ['cat_id' => $where]);

		            for ($i = 0; $i < $count_uom; $i++) { 
		                $uom_cat = ['cat_id' => $where, 'uom_id' => base64_decode($uom[$i])];

		                $res2 = $this->db->insert($this->uomcatTable, $uom_cat);
		            }
	            }
    	 	}

    	 	$db_error = $this->db->error();
            if (!empty($db_error) && $db_error['code']>0) {
                throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
                return false; // unreachable retrun statement !!!
            }
            else {
                //echo $this->db->last_query() . '<br>';
                if (!empty($data['uom_id'])) {
	        		if ($res2) {
		        		return true;
		        	}
		        	else {
		        		return false;
		        	}
	        	}
	        	else {
	        		if ($res) {
		        		return true;
		        	}
		        	else {
		        		return false;
		        	}
	        	}
            }
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }

    	
    }

    public function updateCatStat($data,$where){
    	try {
    	 	$this->db->db_debug = False;

    	 	$res = $this->db->update($this->catTable, $data, ['id' => $where]);

    	 	$db_error = $this->db->error();
            if (!empty($db_error) && $db_error['code']>0) {
                throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
                return false; // unreachable retrun statement !!!
            }
            else {
                //echo $this->db->last_query() . '<br>';
                if ($res) {
                    return true;
                }
                else {
                    return false;
                }
            }
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }    	
    }


    public function updateCatApprove($data,$where){
    	/*print_r($data);
    	print_r($where); die();*/
    	try {
    	 	$this->db->db_debug = False;

    	 	$res = $this->db->update($this->catTable, $data, ['id' => $where]);

    	 	$db_error = $this->db->error();
            if (!empty($db_error) && $db_error['code']>0) {
                throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
                return false; // unreachable retrun statement !!!
            }
            else {
                //echo $this->db->last_query() . '<br>';
                if ($res) {
                    return true;
                }
                else {
                    return false;
                }
            }
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }

    	
    }

    public function doDeleteCat($where){
    	
    	try {
    	 	$this->db->db_debug = False;			 
			
			$data['active'] = 2; 
			 $res = $this->db->update($this->catTable, $data, ['id' => $where]);

	    	$db_error = $this->db->error();
	        if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }
	        else {
	        	if ($res) {
	        		return True;
	        	}
	        	else {
	        		return False;
	        	}
	        }
	        
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }
    }
   	
}

?>