<div class="min-height-200px">
    
    <!-- horizontal Basic Forms Start -->
    <div class="pd-20 bg-white border-radius-4 box-shadow mb-30">

        <div class="clearfix mb-10">
            <div class="pull-left">
                <nav aria-label="breadcrumb" role="navigation">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url('logistic_user')?>">Logistic User Details</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Update Logistic User</li>
                    </ol>
                </nav>
            </div>  
            <div class="pull-right">
                <a class="btn btn-outline-primary" href="<?php echo base_url('logistic_user')?>">Back</a>            
            </div> 
        </div>
   


        <form id="edituserfrm" method="post" action="javascript:void(0);" onsubmit="UpdateLogisticUserDetails();">
            <input type="hidden" name="id" id="id" value="<?php echo (isset($user_details) && isset($user_details["user_id"]) )?$user_details["user_id"]:'';?>">
            <div class="form-group">
                <div class="row">                    
                    <div class="col-4">
                        <label for="Name">Name <sup class="mandatory_label">&#8727;</sup></label>
                        <input type="text" name="name" id="name" class="form-control"  value="<?php echo (isset($user_details) && isset($user_details["name"]) )?$user_details["name"]:'';?>" placeholder="Name">
                    </div>
                    
                    <div class="col-4">
                        <label for="email">Email <sup class="mandatory_label">&#8727;</sup></label>
                        <input type="text" name="email" id="email" class="form-control" value="<?php echo (isset($user_details) && isset($user_details["email"]) )?$user_details["email"]:'';?>" placeholder="Email">
                    </div>

                    <div class="col-4">
                        <label for="mobile">Mobile No <sup class="mandatory_label">&#8727;</sup></label>
                        <input type="text" name="mobile" id="mobile" class="form-control" value="<?php echo (isset($user_details) && isset($user_details["contact"]) )?$user_details["contact"]:'';?>" placeholder="Mobile No">
                    </div>
                    
                </div>

            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-6 offset-6 text-right">
                        <input class="btn btn-primary" type="submit" value="Submit">
                    </div>
                </div>
            </div>           
            
         
        </form>
    </div>
    <!-- horizontal Basic Forms End -->

</div>

<?php
    if(isset($custom_js)){
        foreach($custom_js as $val){
            ?>
            <script src="<?php echo base_url();?>assets/js/custom_js/<?php echo $val;?>"></script>
            <?php
        }
    }
    
?>
  