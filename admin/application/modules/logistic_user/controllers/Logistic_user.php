<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class Logistic_user extends CI_Controller {
	protected $token_id='';
	protected $per_page=10;
	public function __construct() {
    	parent::__construct();
    	$this->token_id=get_cookie('token_id'); 
    	if(trim($this->token_id)==''){
    		redirect("login");
    	}
        $this->load->model("userModel");
        $this->load->model("Logistic_userModel");
        $this->load->helper("pagination");
         $this->load->library("pagination");
         $this->load->library('email'); 
		$this->load->model("settingsModel", 'sM');
		$settings_data=$this->sM->getSettings();
		if(isset($settings_data->per_page)){
		$this->per_page=$settings_data->per_page;
		}
        
    }
    public function index()
	{
		// echo "Hell";
		try {
           $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            
            if(empty($user_details)){
                    $this->Logistic_userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                    delete_cookie('token_id'); 
                     delete_cookie('useName'); 
                    echo '<script>
                        alert("Unexpected server error.Please try again some time.");
                            window.location="'.base_url().'login";
                    </script>';
            }
            $user_details=current($user_details);
            $data=[]; 
            $data["menu"]="logistic-user";
            $data["useName"]=$user_details["Name"];
            $data["custom_js"]=['logistic_user.js'];
            $data["user_id"]=$user_details["user_id"];
            set_cookie('useName',$user_details["Name"],'3600');
            //search 
            $search_input=$this->input->get();

            // print_r($search_input);

            $user_search=[];
            
            if(isset($search_input["name"]) && trim($search_input["name"])!=''){
            	$user_search["Name"]=$search_input["name"];
            	
            }
             if(isset($search_input["status"]) && trim($search_input["status"])!=''){
            	$user_search["status"]=$search_input["status"];
            	
            }
            $data["user_search"]=$user_search;
            $page=0;
			if($this->input->get("per_page")){
	       		$page= $this->input->get("per_page");
	       	}

	       	$data["page"]=$page;
	       	
	       	$total_row=$this->Logistic_userModel->getUserCount($user_search);
	        $data["links"] = createPageNationLink(base_url() . "user/index", $total_row,$this->per_page);

	        $start= (int)$page +1;
			$end = ($page+ $this->per_page >=$total_row)? $total_row : (int)$page  + $this->per_page;
			$data['result_count']='';
			if($total_row>0){
				$data['result_count']= "Showing ".$start." - ".$end." of ".$total_row." Results";
			}
			
			$data["user_data"]=$this->Logistic_userModel->getUserList($user_search,$this->per_page, $page);

	       	// print_r($data);die();
            $data['view_file'] = 'Logistic_user';
            view($data);
           
        } catch (Exception $e) {
            $resposne=["status"=>"1","e"=>$e,"message"=>"Unexpected server error.Please try again some time.","field"=>'user_name'];
         // var_dump($e->getMessage());
        }   
	}
	
	public function addUser(){
		try {
			$user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
			if(empty($user_details)){
					$this->Logistic_userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
					delete_cookie('token_id'); 
					 delete_cookie('role_id'); 
					echo '<script>
						alert("Session expired please login again.");
							window.location="'.base_url().'login";
					</script>';
					exit();
			}
			$user_details=current($user_details);
			$data=[]; 
            $data["menu"]="logistic-user";
            $data["useName"]=$user_details["Name"];
            $data["custom_js"]=['logistic_user.js'];
            set_cookie('useName',$user_details["Name"],'3600');
             
            $data['view_file'] = 'add_logistic_user';
            view($data);
		} catch (Exception $e) {
			echo '<script>
				alert("Unexpected server error.Please try again some time.");
							window.location="'.base_url().'user";
			</script>';

		}
	}

	public function doAddUser(){
		try {
			$data=$this->input->post();
			$resposne=["status"=>"0","message"=>"","field"=>''];
			$user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));

			if(trim($this->token_id)!=''){
				if(isset($data["name"]) && trim($data["name"])==''){
					$resposne=["status"=>"1","message"=>"Please Enter Name.","field"=>'Name'];
				}
				else if(isset($data["email"]) && trim($data["email"])==''){
					$resposne=["status"=>"1","message"=>"Please Enter Email id.","field"=>'email'];
				}
				else if(isset($data["mobile"]) && trim($data["mobile"])==''){
					$resposne=["status"=>"1","message"=>"Please Enter Mobile No.","field"=>'mobile'];
				}
				else{

					$check_duplicate_email=$this->Logistic_userModel->doEmailCheck(["email"=>$data["email"]]);
					$check_duplicate_mobile=$this->Logistic_userModel->doMobileCheck(["mobile"=>$data["mobile"]]);

					if(!empty($check_duplicate_email)){
						$resposne=["status"=>"1","message"=>"Duplicate Email id.","field"=>'email'];
					}else if(!empty($check_duplicate_mobile)){
						$resposne=["status"=>"1","message"=>"Duplicate Mobile No.","field"=>'mobile'];
					}
					if($resposne["status"]==0){
						
						$user_data=['email'=>$data["email"],'contact'=>$data["mobile"],'name'=>$data["name"]];
						$id=$this->Logistic_userModel->insertUser($user_data);
						
						if($id>0){
						 $resposne=["status"=>"0","message"=>"User added successfully.","field"=>'user_name'];
						}else{
							
							$resposne=["status"=>"1","message"=>"User added failed.Please try again some time.","field"=>'user_name'];								
						}
					}

				}
			}else{
				$resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
			}
		} catch (Exception $e) {
				$resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'user_name'];

		}

		echo json_encode($resposne);
	}

	public function doUserEdit($id){
		$user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
			
			if(empty($user_details)){
					$this->Logistic_userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
					delete_cookie('token_id'); 
					 delete_cookie('role_id'); 
					echo '<script>
						alert("Session expired please login again.");
							window.location="'.base_url().'login";
					</script>';
					exit();
			}
			$user_details=current($user_details);
			$id=base64_decode($id);
			if(trim($id)==''){
				echo '<script>
						alert("Invalid user.");
							window.location="'.base_url().'user";
					</script>';
					exit();
			}
			$data=[]; 
            $data["menu"]="logistic-user";
            $data["useName"]=$user_details["Name"];
            $data["custom_js"]=['logistic_user.js'];
            set_cookie('useName',$user_details["Name"],'3600');
            $data["user_details"]=current($this->Logistic_userModel->getUserDetail($id,0));
			if(empty($data["user_details"])){
				echo '<script>
						alert("Invalid user.");
							window.location="'.base_url().'user";
					</script>';
					exit();
			}
			$data['view_file'] = 'edit_logistic_user';
			// print_r($data); die();
            view($data);
			
	}
	
	public function UpdateUserDetails(){
		$data=$this->input->post();
		// print_r($data); die();
		$resposne=["status"=>"0","message"=>"","field"=>''];
		try {
			$user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
			if(trim($this->token_id)!=''){
				
				if(isset($data["name"]) && trim($data["name"])==''){
					$resposne=["status"=>"1","message"=>"Please Enter Name.","field"=>'name'];
				}else if(isset($data["email"]) && trim($data["email"])==''){
					$resposne=["status"=>"1","message"=>"Please Enter Email id.","field"=>'email'];
				}else if(isset($data["mobile"]) && trim($data["mobile"])==''){
					$resposne=["status"=>"1","message"=>"Please Enter Mobile No.","field"=>'mobile'];
				}else{
					$check_duplicate_email=$this->Logistic_userModel->doEmailCheck(["email"=>$data["email"]], $data["id"]);
					$check_duplicate_mobile=$this->Logistic_userModel->doMobileCheck(["mobile"=>$data["mobile"]], $data["id"]);

					if(!empty($check_duplicate_email)){
						$resposne=["status"=>"1","message"=>"Duplicate Email id.","field"=>'email'];
					}else if(!empty($check_duplicate_mobile)){
						$resposne=["status"=>"1","message"=>"Duplicate Mobile No.","field"=>'mobile'];
					}

					if($resposne["status"]==0){
						
						$user_data=['email'=>$data["email"],'contact'=>$data["mobile"],'name'=>$data["name"]];

						$id=$this->Logistic_userModel->updateUser($user_data,["id"=>$data["id"]]);

						if($id>0){
						 	$resposne=["status"=>"0","message"=>" user updated successfully.","field"=>'user_name'];
						}else{							
							$resposne=["status"=>"1","message"=>"User updated failed.Please try again some time.","field"=>'user_name'];							
						}
					}
				}
			}else{
				$resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
			}
		}catch (Exception $e) {
				$resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'user_name','error'=>$e];

		}

		echo json_encode($resposne);
	}

	public function douserStatusChange(){
		$data=$this->input->post();
		$data["id"]=base64_decode($data["id"]);
		$resposne=["status"=>"0","message"=>"","field"=>''];
		try {
			$user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
			if(trim($this->token_id)!=''){
				$user_detail=current($this->Logistic_userModel->getUserDetail($data["id"]));
			
					if(empty($user_details)){
						$resposne=["status"=>"1","message"=>"Invalid User.","field"=>'name'];

					}else{
						$id=$this->Logistic_userModel->updateUser(["status"=>$data["status"]],["id"=>$data["id"]]);
						$resposne=["status"=>"0","message"=>"User status change successfully.","field"=>'user_name'];
					}
			}else{
				$resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
			}	
		}catch (Exception $e) {
				$resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'role_id'];

		}

		echo json_encode($resposne);	
	}

}
?>