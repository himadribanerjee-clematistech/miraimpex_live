<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class PaymentModel extends CI_Model {
	protected $userTable = 'user';
	protected $paymentTable = 'order';
	
	public function __construct() {
        parent::__construct();
    }
    
    public function getPaymentCount($data){
    	//print_r($data); die();
    	try {
    		$this->db->db_debug = false;
    		$this->db->select("*");
    		$this->db->from($this->paymentTable);
    		/*if(isset($data["uom_name"]) && $data["uom_name"]!=''){ 
	    		$this->db->like("name",$data["uom_name"]);
	    	}
	    	if(isset($data["status"]) && $data["status"]!=''){ 
	    		$this->db->where("active",$data["status"]);
	    	}*/
	    	
	    	$db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query();die();
	        	return $this->db->count_all_results();

	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
    }
    public function getPaymentList($data='',$limit='', $start=''){
    	try {
    		$this->db->db_debug = false;
    		$this->db->select("*");
    		$this->db->from($this->paymentTable);
    		
	    	/*if(isset($data["uom_name"]) && $data["uom_name"]!=''){
	    		$this->db->like("name",$data["uom_name"]);
	    	}
	    	if(isset($data["status"]) && $data["status"]!=''){ 
	    		$this->db->where("active",$data["status"]);
	    	}*/
	    	
	    	//$this->db->order_by('name', 'ASC'); 

	    	if ($limit != '' ) {
	    		$this->db->limit($limit, $start);
	    	}
	    	
 
 			$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query();die();
	        	$result=[];
	        	foreach($query->result_array() as $row){
	        		$result[]=$row;
	        	}
	        	return $result;
	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
    }	

    public function doUomCheck($data, $id=''){
    	//print_r($data); die();
		try {
		 	$this->db->db_debug = FALSE;
	        $this->db->select("name");
	    	$this->db->from($this->paymentTable);
	    	if(isset($data) && trim($data)!='' ){
	    		$this->db->where("name",$data);
	    		
	    	}

	    	if($id!=''){
	    		$this->db->where("id !=",$id);
	    	}

	    	$this->db->where('type', 1);
	    	
	    	$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query() . '<br>';
	        	if($query->num_rows() >0) {
	        		return True;
	        	}
	        	else {
	        		return false;
	        	}
	        }
	        //return TRUE;
	    } catch (Exception $e) {
	       throw new Exception();
	    }	
    }
     
    public function insertUom($data){
     	//print_r($data); die();
    	try {
    	 	$this->db->db_debug = FALSE;
    	 	$res = $this->db->insert($this->paymentTable, $data);

    	 	$db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }
	        else {
	        	//echo $this->db->last_query() . '<br>';
	        	if ($res) {
	        		return true;
	        	}
	        	else {
	        		return false;
	        	}
	        }
	        //return  $this->db->insert_id();
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }
    	
    }

    public function getById($id){
    	try {
		 	$this->db->db_debug = false;
	        $this->db->select("id, name");
	    	$this->db->from($this->paymentTable);
	    	
	    	$this->db->where("id", $id);

	    	$this->db->where('type', 1);
	    	
	    	$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	$data=[];
	        	foreach($query->result_array() as $row){
	        		 $data[]=$row;
	    	
	        	}
	        	//echo $this->db->last_query();
	        	return $data;
	        }
	        return TRUE;
	    } catch (Exception $e) {
	       throw new Exception();
	    }	
    }

    public function updateUom($data,$where){
    	
    	try {
    	 	$this->db->db_debug = False;
    	 	$res = $this->db->update($this->paymentTable, $data, ['id' => $where]);

    	 	//echo $this->db->last_query(); die();

	    	$db_error = $this->db->error();
	        if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }
	        else {
	        	if ($res) {
	        		return True;
	        	}
	        	else {
	        		return False;
	        	}
	        }
	        
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }
    }

    /*public function doDeleteUom($where){
    	
    	try {
    	 	$this->db->db_debug = False;
    	 	$res = $this->db->delete($this->paymentTable, ['id' => $where]);

	    	$db_error = $this->db->error();
	        if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }
	        else {
	        	if ($res) {
	        		return True;
	        	}
	        	else {
	        		return False;
	        	}
	        }
	        
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }
    }*/
   	
}

?>