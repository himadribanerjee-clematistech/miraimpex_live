<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class ProvideritemModel extends CI_Model {
	protected $custTable = 'customer';
	protected $psiTable = 'provider_service_item';
	protected $siiTable = 'provider_service_item_image';
	protected $siTable = 'service_item';
	protected $uomTable = 'uom';
	
	public function __construct() {
        parent::__construct();
    }
    
    public function getitemCount($data){
    	try {
    		$this->db->db_debug = false;

    		//SELECT psi.provider_service_item_id, psi.min_rate, psi.max_rate, psi.active, c.name AS provider, si.name AS item FROM t_provider_service_item psi JOIN t_customer c ON c.id = psi.provider_id JOIN t_service_item si ON si.service_item_id = psi.service_item_id

    		$this->db->select("psi.provider_service_item_id, psi.min_rate, psi.max_rate, psi.active, c.name AS provider, si.name AS item");
    		$this->db->from($this->psiTable . ' psi');
    		$this->db->join($this->custTable . ' c', 'c.id = psi.provider_id');
    		$this->db->join($this->siTable . ' si', 'si.service_item_id = psi.service_item_id');

			/*if(isset($data["customer_id"]) && $data["customer_id"]!=''){ 
	    		$this->db->like("si.customer_id",$data["customer_id"]);
	    	}
	    	if(isset($data["item_id"]) && $data["item_id"]!=''){ 
	    		$this->db->where("si.item_id",$data["item_id"]);

	    	}
	    	if(isset($data["species_id"]) && $data["species_id"]!=''){ 
	    		$this->db->where("si.species_id",$data["species_id"]);
	    	}*/
	    	
	    	$this->db->get();

	    	$db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	return $this->db->count_all_results();

	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
    }
    
    public function getSellerItem($data,$limit='', $start=''){
       	try {
    		$this->db->db_debug = false;

    		//SELECT psi.provider_service_item_id, psi.min_rate, psi.max_rate, psi.active, c.name AS provider, si.name AS item FROM t_provider_service_item psi JOIN t_customer c ON c.id = psi.provider_id JOIN t_service_item si ON si.service_item_id = psi.service_item_id

    		$this->db->select("psi.provider_service_item_id, psi.min_rate, psi.max_rate, psi.active, c.name AS provider, si.name AS item");
    		$this->db->from($this->psiTable . ' psi');
    		$this->db->join($this->custTable . ' c', 'c.id = psi.provider_id');
    		$this->db->join($this->siTable . ' si', 'si.service_item_id = psi.service_item_id');
    		
    		/*if(isset($data["customer_id"]) && $data["customer_id"]!=''){ 
	    		$this->db->like("si.customer_id",$data["customer_id"]);
	    	}
	    	if(isset($data["item_id"]) && $data["item_id"]!=''){ 
	    		$this->db->where("si.item_id",$data["item_id"]);

	    	}
	    	if(isset($data["species_id"]) && $data["species_id"]!=''){ 
	    		$this->db->where("si.species_id",$data["species_id"]);
	    	}*/

	    	$this->db->order_by('psi.provider_service_item_id', 'DESC'); 
	    	
	    	if($limit!=''  && $start!=''){
	    		$this->db->limit($limit, $start);
	    	}
	    	
 
 			$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query();die();
	        	$result=[];
	        	foreach($query->result_array() as $row){
	        		$result[]=$row;
	        	}
	        	return $result;
	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
    }

    public function getCustomer() {
    	return $this->db->select('id, name')->where('active', 1)->from($this->custTable)->get()->result_array();
    }

    public function getItem() {
    	return $this->db->select('service_item_id, name')->from($this->siTable)->where("status",1)->get()->result_array();
    }

    public function getDataById($id){
    	try {
		 	$this->db->db_debug = false;

		 	// SELECT psi.provider_service_item_id, psi.min_rate, psi.max_rate, c.name AS provider, si.name AS item, u.name, psii.image, psi.description, psi.special_instruction, psi.terms_condition FROM t_provider_service_item psi JOIN t_customer c ON c.id = psi.provider_id JOIN t_service_item si ON si.service_item_id = psi.service_item_id JOIN t_uom u ON u.id = psi.uom_id JOIN t_provider_service_item_image psii ON psii.provider_service_item_id = psi.provider_service_item_id WHERE psi.provider_service_item_id = 1


		 	$this->db->select("psi.provider_service_item_id, psi.min_rate, psi.max_rate, c.name AS provider, si.name AS item, u.name AS uom, psii.image, psi.description, psi.special_instruction, psi.terms_condition");
    		$this->db->from($this->psiTable." psi");
    		$this->db->join($this->custTable . ' c' , 'c.id = psi.provider_id');
    		$this->db->join($this->siTable . ' si' , 'si.service_item_id = psi.service_item_id');
    		$this->db->join($this->uomTable . ' u' , 'u.id = psi.uom_id');
    		$this->db->join($this->siiTable . ' psii' , 'psii.provider_service_item_id = psi.provider_service_item_id');
	    	
	    	$this->db->where("psi.provider_service_item_id", $id);
	    	
	    	$query=$this->db->get();

	    	//echo $this->db->last_query(); die();

	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	$data=[];
	        	foreach($query->result_array() as $row){
	        		$rcat = $row["provider_service_item_id"]."_pi";

	        		if(!isset($data[$rcat])){
	        			$data[$rcat]["id"]=base64_encode($row["provider_service_item_id"]);
	        			$data[$rcat]["item"]=$row["item"];
	        			$data[$rcat]["provider"]=$row["provider"];
	        			$data[$rcat]["description"]=$row["description"];
	        			$data[$rcat]["special_instruction"]=$row["special_instruction"];
	        			$data[$rcat]["terms_condition"]=$row["terms_condition"];
	        			$data[$rcat]["min_rate"]=$row["min_rate"];
	        			$data[$rcat]["max_rate"]=$row["max_rate"];
	        			$data[$rcat]["uom"]=$row["uom"];
	        			$data[$rcat]["image"]=[];
	        			
	        		}
	        		$data[$rcat]["image"][] = ["image" => $row["image"]];	    	
	        	}

	        	//print_r(array_values($data)); die();

	        	//echo $this->db->last_query(); die();
	        	return array_values($data);
	        }
	        return TRUE;
	    } catch (Exception $e) {
	       throw new Exception();
	    }	
    }

    /*public function doCheck($cust, $item, $species, $id=''){
    	//print_r($data); die();
		try {
		 	$this->db->db_debug = FALSE;
	        $this->db->select("id");
	    	$this->db->from($this->siTable);
	    	if(isset($cust) && trim($cust)!='' ){
	    		$this->db->where("customer_id",$cust);
	    		
	    	}
	    	if(isset($item) && trim($item)!='' ){
	    		$this->db->where("item_id",$item);
	    		
	    	}
	    	if(isset($species) && trim($species)!='' ){
	    		$this->db->where("species_id",$species);
	    		
	    	}

	    	if($id!=''){
	    		$this->db->where("id !=",$id);
	    	}
	    	
	    	$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query() . '<br>';
	        	if($query->num_rows() >0) {
	        		return True;
	        	}
	        	else {
	        		return false;
	        	}
	        }
	        //return TRUE;
	    } catch (Exception $e) {
	       throw new Exception();
	    }	
    }
     
    public function insertNew($data){
     	//print_r($data); die();
    	try {
    	 	$this->db->db_debug = FALSE;
    	 	$res = $this->db->insert($this->siTable, $data);

    	 	$db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }
	        else {
	        	//echo $this->db->last_query() . '<br>';
	        	if ($res) {
	        		return true;
	        	}
	        	else {
	        		return false;
	        	}
	        }
	        //return  $this->db->insert_id();
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }
    	
    }

    public function getById($id){
    	try {
		 	$this->db->db_debug = false;
	        $this->db->where("id", $id);	    	
	    	$query=$this->db->get($this->siTable);
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	$data=[];
	        	foreach($query->result_array() as $row){
	        		 $data[]=$row;
	    	
	        	}
	        	//echo $this->db->last_query();
	        	return $data;
	        }
	        return TRUE;
	    } catch (Exception $e) {
	       throw new Exception();
	    }	
    }

    public function updateSellerItem($data,$where){
    	
    	try {
    	 	$this->db->db_debug = False;
    	 	$res = $this->db->update($this->siTable, $data, ['id' => $where]);

	    	$db_error = $this->db->error();
	        if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }
	        else {

	        	if ($res) {
	        		return True;
	        	}
	        	else {
	        		return False;
	        	}
	        }
	        
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }
    }*/
   	
}

?>