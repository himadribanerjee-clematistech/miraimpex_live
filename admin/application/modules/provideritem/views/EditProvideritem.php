<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="clearfix mb-10">
        <div class="pull-left">
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Seller Item</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Update Following Seller Item</li>
                </ol>
            </nav>
        </div>
        <div class="pull-right">
            <a class="btn btn-outline-primary" href="<?php echo base_url()?>selleritem">Back</a>            
        </div>
    </div>

    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                <form id="edititemfrm" method="post" action="javascript:void(0);" autocomplete="off" enctype="multipart/form-data" onsubmit="return updateItem();">

                    <input type="hidden" id="id" value="<?php echo (isset($selleritemDetail) && isset($selleritemDetail["id"]) )?base64_encode($selleritemDetail["id"]):''; ?>" >

                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-3">
                                <label label-for='name'>Customer <span class="mandatory_label">*</span></label>
                                <select name="customer_id" id="customer_id" class="form-control">
                                        <option value=""> -- select -- </option>

                                <?php

                                    foreach ($customer as $cust) {
                                        
                                ?>

                                        <option value="<?php echo base64_encode($cust["id"]); ?>" 
                                            <?php 
                                                echo (isset($selleritemDetail) && isset($selleritemDetail["customer_id"]) && ($selleritemDetail["customer_id"] == $cust["id"]) )? 'selected':'';
                                            ?> > <?php echo $cust["name"]; ?> </option>

                                <?php

                                    }

                                ?>

                                    
                                </select>
                            </div>
                            <div class="col-lg-3">
                                <label label-for='name'>Item <span class="mandatory_label">*</span></label>
                                <select name="item_id" id="item_id" onchange="getSpecies(this.value)" class="form-control">
                                    <option value=""> -- select -- </option>

                                <?php

                                    foreach ($item as $item) {
                                        
                                ?>

                                    <option value="<?php echo base64_encode($item["id"]); ?>"

                                        <?php 
                                            echo (isset($selleritemDetail) && isset($selleritemDetail["item_id"]) && ($selleritemDetail["item_id"] == $item["id"]) )? 'selected':'';
                                        ?>

                                    > <?php echo $item["item_name"]; ?> </option>

                                <?php

                                    }

                                ?>
                                    
                                </select>
                            </div>
                            <div class="col-lg-3">
                                <label label-for='name'>Species <span class="mandatory_label">*</span></label>
                                <select name="species_id" id="species_id" class="form-control">
                                    <option value=""> -- select Species -- </option>
                                <?php

                                    foreach ($species as $sp) {
                                        
                                ?>

                                    <option value="<?php echo base64_encode($sp["id"]); ?>"
                                        <?php 
                                            echo (isset($selleritemDetail) && isset($selleritemDetail["species_id"]) && ($selleritemDetail["species_id"] == $sp["id"]) )? 'selected':'';
                                        ?>

                                    > <?php echo $sp["species_name"]; ?> </option>

                                <?php

                                    }

                                ?>
                                </select>
                            </div>
                            <div class="col-lg-3">
                                <label label-for='name'>Qty <span class="mandatory_label">*</span></label>
                                <input type="text" name="min_quantity" id="min_quantity" class="form-control" value="<?php echo (isset($selleritemDetail) && isset($selleritemDetail["min_quantity"]) )?$selleritemDetail["min_quantity"]:''; ?>">
                            </div>
                        </div>
                    </div>

                    <br>                    

                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-3">
                                <label label-for='name'>Item Description <span class="mandatory_label">*</span></label>
                                <textarea name="item_description" id="item_description" class="form-control" style="height: auto;"><?php echo (isset($selleritemDetail) && isset($selleritemDetail["item_description"]) )?$selleritemDetail["item_description"]:''; ?></textarea>
                            </div>
                            <div class="col-lg-3">
                                <label label-for='name'>Special Information <span class="mandatory_label">*</span></label>
                                <textarea name="special_information" id="special_information" class="form-control" style="height: auto;"><?php echo (isset($selleritemDetail) && isset($selleritemDetail["special_information"]) )?$selleritemDetail["special_information"]:''; ?></textarea>
                            </div>
                            <div class="col-lg-3">
                                <label label-for='name'>Packing Information <span class="mandatory_label">*</span></label>
                                <textarea name="packing_information" id="packing_information" class="form-control" style="height: auto;"><?php echo (isset($selleritemDetail) && isset($selleritemDetail["packing_information"]) )?$selleritemDetail["packing_information"]:''; ?></textarea>
                            </div>
                            <div class="col-lg-3">
                                <label label-for='name'>Shipping Information <span class="mandatory_label">*</span></label>
                                <textarea name="shipping_information" id="shipping_information" class="form-control" style="height: auto;"><?php echo (isset($selleritemDetail) && isset($selleritemDetail["shipping_information"]) )?$selleritemDetail["shipping_information"]:''; ?></textarea>
                            </div>
                        </div>
                    </div>

                    <br>

                    <div class="form-group">
                        <div class="row">                            
                            <div class="col-lg-3">
                                <label label-for='name'>Min. Price <span class="mandatory_label">*</span></label>
                                <input type="text" name="min_price" id="min_price" placeholder="0.00" class="form-control" value="<?php echo (isset($selleritemDetail) && isset($selleritemDetail["min_price"]) )?$selleritemDetail["min_price"]:''; ?>">
                            </div>
                            <div class="col-lg-3">
                                <label label-for='name'>Max. Price <span class="mandatory_label">*</span></label>
                                <input type="text" name="max_price" id="max_price" placeholder="0.00" class="form-control" value="<?php echo (isset($selleritemDetail) && isset($selleritemDetail["max_price"]) )?$selleritemDetail["max_price"]:''; ?>">
                            </div>
                            <div class="col-lg-3">
                                <label label-for='name'>Organic ? <span class="mandatory_label">*</span></label>
                                <select name="organic_YN" id="organic_YN" class="form-control">
                                    <option value="0"
                                    <?php 
                                        echo (isset($selleritemDetail) && isset($selleritemDetail["organic_YN"]) && ($selleritemDetail["organic_YN"] == 0) )? 'selected':'';
                                    ?>
                                    > No </option>
                                    <option value="1"
                                    <?php 
                                        echo (isset($selleritemDetail) && isset($selleritemDetail["organic_YN"]) && ($selleritemDetail["organic_YN"] == 1) )? 'selected':'';
                                    ?>
                                    > Yes </option>
                                </select>
                            </div>
                            <div class="col-lg-3">
                                <label label-for='name'>Image <span class="mandatory_label">*</span></label>

                                <input type="hidden" id="img_old" value="<?php echo (isset($selleritemDetail) && isset($selleritemDetail["item_image"]) )?$selleritemDetail["item_image"]:''; ?>" >

                                <img src="<?php echo (isset($selleritemDetail) && ($selleritemDetail["item_image"] != '') )?base_url('uploads/selleritem/' . $selleritemDetail["item_image"]):base_url('assets/images/no-item-image.png'); ?>" height="150" width="150"  style="border: 2px dotted #ccc; border-radius: 2px;" id="chImg"/>

                                    <br><br><br>

                                <input type="file" name="item_image" id="item_image" onchange="loadFile()">
                                
                            </div>
                        </div>
                    </div>

                    <br>

                    <div class="form-group pt-2">
                        <div class="row">
                            <div class="col-lg-12 text-right">
                                <input type="submit" value="Update" name="submit" class="btn btn-outline-primary">
                            </div>
                        </div>
                    </div>
                  
                </form>
            </div>
        </div>
    </div>

</div> 


<?php

    if(isset($custom_js)){
        foreach($custom_js as $val){
            ?>
            <script src="<?php echo base_url();?>assets/js/custom_js/<?php echo $val;?>"></script>
            <?php
        }
    }
    
?>
  



 
 