
<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="clearfix mb-10">
        <div class="pull-left">
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo base_url('provideritem')?>">Provider Item</a></li>
                    <li class="breadcrumb-item active" aria-current="page">List</li>
                </ol>
            </nav>
        </div>
        <!-- <div class="pull-right">
            <a class="btn btn-outline-primary" href="<?php //echo base_url('selleritem/additem')?>">Add New</a>            
        </div> -->
    </div>

    <div class="container-fluid bg-light"> 
        <div class="row">
            <div class="col-lg-12">

                <form id="catsrch" action="<?php echo base_url('selleritem')?>">
                    <input type="hidden" name="per_page_search" value="<?php echo $page; ?>">

                    <div class="row align-items-center justify-content-center">
                        <div class="col-md-3 pt-3">
                            <div class="form-group ">
                                <select name="customer_id" id="customer_id" class="form-control">
                                    <option value=""> -- select Customer -- </option>

                            <?php

                                foreach ($customer as $cust) {
                                    
                            ?>

                                    <option value="<?php echo base64_encode($cust["id"]); ?>" <?php echo (isset($item_search) && isset($item_search["customer_id"]) && ($item_search["customer_id"] == $cust["id"]) )? 'selected':''; ?> > <?php echo $cust["name"]; ?> </option>

                            <?php

                                }

                            ?>

                        
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3 pt-3">
                            <div class="form-group">
                                <select name="item_id" id="item_id" class="form-control">
                                    <option value=""> -- select Item -- </option>

                            <?php

                                foreach ($item as $item) {
                                    
                            ?>

                                    <option value="<?php echo base64_encode($item["service_item_id"]); ?>" <?php echo (isset($item_search) && isset($item_search["item_id"]) && ($item_search["item_id"] == $item["service_item_id"]) )? 'selected':''; ?> > <?php echo $item["name"]; ?> </option>

                            <?php

                                }

                            ?>

                                
                                </select>
                            </div>
                        </div>
                                               
                        <div class="col-md-3">
                            <a class="btn btn-outline-primary" href="<?php echo base_url('selleritem')?>"><i class="fa fa-refresh" aria-hidden="true"></i> Reset</a> 
                                
                            <input type="submit" name="search" value="Search" class="btn btn-outline-primary">
                        </div>
                    </div>
                    
                </form>
            </div>
        </div>
    </div>

    <div class="row pt-2">
        
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="data-table stripe hover no-wrap" style="font-size: 14px;">
            <thead>
                <tr class="titel_bar">
                    <td class="table-plus datatable-nosort"  width="15%">Provider</td>
                    <td class="table-plus datatable-nosort"  width="15%">Providing</td>
                    <td class="table-plus datatable-nosort"  width="20%">Price</td>
                    <td class="table-plus datatable-nosort text-right" width="10%" >Status</td>
                    <td class="table-plus datatable-nosort"  width="10%" ></td>
                    <!-- <td class="table-plus datatable-nosort"  width="10%" ><center>Action</center></td> -->
                </tr>
            </thead>
            <tbody>

    <?php
        if(!empty($provider_item)){
            foreach($provider_item as $val){
    ?>
                <tr class="">
                    <td class="table-plus" >
                        <?php echo $val["provider"];?>           
                    </td>
                    <td class="table-plus" >
                        <?php echo $val["item"];?>           
                    </td>   
                    <td class="table-plus" >
                        &#8358; <?php echo $val["min_rate"];?> - &#8358; <?php echo $val["max_rate"];?>
                            
                    </td>
                    <td class="table-plus text-right">

                        <?php echo (!empty($val["active"]) && isset($val["active"]) && $val["active"]== 1) ? '<font size="4" color="green">Active</font>' : '<font size="4" color="red">Deleted</font>'; ?>

                    </td>
                    <td>
                        <center>
                            <a class="btn btn-outline-primary view" href="javascript:void(0);" data-id="<?php echo base64_encode($val["provider_service_item_id"]); ?>" data-toggle="modal">
                                <i class="fa fa-eye" aria-hidden="true"></i> Details
                            </a>
                        </center>
                    </td>
                    <!-- <td class="table-plus" style="text-align:center;">

                        <div class="dropdown">
                            <a class="btn btn-outline-primary dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                <i class="fa fa-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="javascript:void(0);" style=" font-size: 15px;" onclick="doitemEdit('<?php //echo base64_encode($val["id"]);?>')" >
                                    <i class="fa fa-edit" aria-hidden="true"></i> 
                                    Edit
                                </a>
                            </div>
                        </div>

                    </td> -->

                </tr>
    <?php
            }
        }else{
    ?>

                <tr class="">
                    <td colspan="7">No Record Found</td>
                </tr>  

    <?php
        }
    ?>
           
                 
            </tbody>
        </table> 
    </div>
</div>




<div class="modal fade admin_modal" id="shwModal">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title"><small>Description of</small> <span class="item_name"></span></h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>Provide By</label>
                            <br>
                            <span class="provider" style="font-weight: 500 !important;"></span>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>Price</label>
                            <br>
                            <span class="amount" style="font-weight: 500 !important;"></span>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>Unit of Measurement</label>
                            <br>
                            <span class="uom" style="font-weight: 500 !important;"></span>
                        </div>
                    </div>
                </div>

                <br>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Description</label>
                            <br>
                            <span class="description" style="font-weight: 500 !important;"></span>
                        </div>
                    </div>
                </div>

                <br>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Special Information</label>
                            <br>
                            <span class="special_instruction" style="font-weight: 500 !important;"></span>
                        </div>
                    </div>
                </div>

                <br>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Terms and Condition</label>
                            <br>
                            <span class="terms_condition" style="font-weight: 500 !important;"></span>
                        </div>
                    </div>
                </div>

                <br>

                <div class="row" id="row"> 
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Images</label>
                            <br>
                            <span id="emImg"><ul class="image" style="list-style-type: none; display: flex;"></ul></span>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>




<?php
	if(isset($custom_js)){
		foreach($custom_js as $val){
			?>
			<script src="<?php echo base_url();?>assets/js/custom_js/<?php echo $val;?>"></script>
			<?php
		}
	}
	
?>
  