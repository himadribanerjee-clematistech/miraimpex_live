<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class Variance extends CI_Controller {
	protected $token_id='';
	protected $per_page=10;
	public function __construct() {
    	parent::__construct();
    	$this->token_id=get_cookie('token_id'); 
    	if(trim($this->token_id)==''){
    		redirect("login");
    	}
        $this->load->model("VarianceModel", 'vM');
        $this->load->model("SettingsModel", 'sM');
        $this->load->model("userModel");
        $this->load->helper("pagination");
        $this->load->library("pagination");

        $settings_data=$this->sM->getSettings();
        if(isset($settings_data->per_page)){
            $this->per_page=$settings_data->per_page;
        }
    }
    public function index()
	{
		try {
           $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            
            if(empty($user_details)){
                    $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                    delete_cookie('token_id'); 
                     delete_cookie('userName'); 
                    echo '<script>
                        alert("Unexpected server error.Please try again some time.");
                            window.location="'.base_url().'login";
                    </script>';
            }

            $user_details=current($user_details);
            $data=[]; 
            $data["menu"]="variance";
            $data["userName"]=$user_details["Name"];
            $data["custom_js"]=['variance.js'];
            
            set_cookie('userName',$user_details["Name"],'3600');

            //search 
            $search_input=$this->input->get();

            $var_search=[];
            
            if(isset($search_input["name_en"]) && trim($search_input["name_en"])!=''){
            	$var_search["name_en"] = $search_input["name_en"];
            }
            if(isset($search_input["type"]) && trim($search_input["type"])!=''){
                $var_search["type"] = $search_input["type"];
            }
            if(isset($search_input["status"]) && trim($search_input["status"])!=''){
                $var_search["status"] = $search_input["status"];
            }
            
            $data["var_search"] = $var_search;
            $page=0;
			if($this->input->get("per_page")){
	       		$page= $this->input->get("per_page");
	       	}

            $data["page"]=$page;

            $total_row=$this->vM->getVarianceCount($var_search);
         
	        $data["links"] = createPageNationLink(base_url('variance/index'), $total_row, $this->per_page);

	        $start= (int)$page +1;
			$end = ($page+ $this->per_page >=$total_row)? $total_row : (int)$page  + $this->per_page;
			$data['result_count']='';
			if($total_row>0){
				$data['result_count']= "Showing ".$start." - ".$end." of ".$total_row." Results";
			}
			
			$data["variance_data"]=$this->vM->getVarianceList($var_search, $this->per_page, $page);

            $data['view_file'] = 'Variance';
            // print_r($data); die();
            view($data);
           
        } catch (Exception $e) {
            $resposne=["status"=>"1","e"=>$e,"message"=>"Unexpected server error.Please try again some time.","field"=>'user_Name'];
        }   
	}
	
    public function addVariance(){
        try {
            $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            if(empty($user_details)){
                    $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                    delete_cookie('token_id'); 
                     delete_cookie('role_id'); 
                    echo '<script>
                        alert("Session expired please login again.");
                            window.location="'.base_url().'login";
                    </script>';
                    exit();
            }
            $user_details=current($user_details);
            $data=[]; 
            $data["menu"]="variance";
            $data["userName"]=$user_details["Name"];
            $data["custom_js"]=['variance.js'];
            set_cookie('userName',$user_details["Name"],'3600');
             $data['view_file'] = 'AddVariance';
            view($data);
        } catch (Exception $e) {
            echo '<script>
                alert("Unexpected server error.Please try again some time.");
                            window.location="'.base_url().'category";
            </script>';

        }
    }

    public function doAddVariance(){
        try {
            $data=$this->input->post();
            $resposne=["status"=>"0","message"=>"", "field"=>''];
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));

            //print_r($data); die();

            if(trim($this->token_id)!=''){
                if(isset($data["name_en"]) && trim($data["name_en"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter Name.","field"=>'name_en'];
                }
                elseif(isset($data["Name_fr"]) && trim($data["Name_fr"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter Name in French.","field"=>'Name_fr'];
                }
                elseif(isset($data["type"]) && trim($data["type"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter Type.","field"=>'type'];
                }
                else{
                    $check_duplicate_Name = $this->vM->doVarCheck($data["name_en"]);

                    if($check_duplicate_Name == 1) {
                        $resposne=["status"=>"1","message"=>"Duplicate Variance Name.","field"=>'name_en'];
                    }
                    else {
                        
                        $formData = [
                            'name_en'      => $data['name_en'], 
                            'name_fr'   => $data['name_fr'], 
                            'type'      => $data['type'], 
                            'status'    => $data['active'],
                        ];
                        $res = $this->vM->saveVariance($formData);

                        if($res){
                            $resposne=["status"=>"0","message"=>"Variance added successfully."];
                        }
                        else{                                
                            $resposne=["status"=>"1","message"=>"Variance added failed.Please try again after some time."];                                
                        }
                    }
                }
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied."];
            }
        } catch (Exception $e) {
            $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time."];

        }

        echo json_encode($resposne);
    }

    public function doVarianceEdit($id){
        $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            
        if(empty($user_details)){
                $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                delete_cookie('token_id'); 
                 delete_cookie('role_id'); 
                echo '<script>
                    alert("Session expired please login again.");
                        window.location="'.base_url().'login";
                </script>';
                exit();
        }
        $user_details=current($user_details);
        $id=base64_decode($id);
        if(trim($id)==''){
            echo '<script>
                    alert("Invalid user.");
                        window.location="'.base_url().'user";
                </script>';
                exit();
        }
        $data=[]; 
        $data["menu"]="variance";
        $data["userName"]=$user_details["Name"];
        $data["custom_js"]=['variance.js'];
        set_cookie('userName',$user_details["Name"],'3600');
        $data['var_detail'] = current($this->vM->getById($id));

        // print_r($data); die();
         
        $data['view_file'] = 'EditVariance';
        view($data);
            
    }
    public function updateVar(){

        try {
            $data=$this->input->post();
            $resposne=["status"=>"0","message"=>"", "field"=>''];
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));

            if(trim($this->token_id)!=''){
                if(isset($data["name_en"]) && trim($data["name_en"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter Name.","field"=>'name_en'];
                }
                elseif(isset($data["Name_fr"]) && trim($data["Name_fr"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter Name in French.","field"=>'Name_fr'];
                }
                elseif(isset($data["type"]) && trim($data["type"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter Type.","field"=>'type'];
                }
                else{
                    $check_duplicate_Name = $this->vM->doVarCheck($data["name_en"], base64_decode($data["id"]));

                    if($check_duplicate_Name == 1) {
                        $resposne=["status"=>"1","message"=>"Duplicate Variance Name.","field"=>'name_en'];
                    }
                    else {
                        
                       $formData = [
                            'name_en'      => $data['name_en'], 
                            'name_fr'   => $data['name_fr'], 
                            'type'      => $data['type'], 
                            'status'    => $data['active'],
                        ];
                        $res = $this->vM->saveVariance($formData, base64_decode($data["id"]));

                        if($res){
                            $resposne=["status"=>"0","message"=>"Variance Updated successfully."];
                        }
                        else{                                
                            $resposne=["status"=>"1","message"=>"Variance Updation failed.Please try again after some time."];                                
                        }
                    }
                }
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied."];
            }
        } catch (Exception $e) {
            $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time."];

        }

        echo json_encode($resposne);
    }

    public function doVarStatusChange(){
        $data=$this->input->post();
        $id=base64_decode($data["id"]);
        $resposne=["status"=>"0","message"=>"","field"=>''];
        try {
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
            if(trim($this->token_id)!=''){
                $res = $this->vM->saveVariance(["status"=>$data["active"]], $id);
              
                if($res){
                    $resposne=["status"=>"0","message"=>"Variance status change successfully."];
                }
                else{                                
                    $resposne=["status"=>"1","message"=>"Variance change failed.Please try again after some time."];                                
                }
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
            }   
        }catch (Exception $e) {
                $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'status'];

        }

        echo json_encode($resposne);
    }

    public function doVarTypeChange(){
        $data=$this->input->post();
        $id=base64_decode($data["id"]);
        $resposne=["status"=>"0","message"=>"","field"=>''];
        try {
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
            if(trim($this->token_id)!=''){
                $res = $this->vM->saveVariance(["type"=>$data["type"]], $id);
              
                if($res){
                    $resposne=["status"=>"0","message"=>"Variance type change successfully."];
                }
                else{                                
                    $resposne=["status"=>"1","message"=>"Variance type change failed.Please try again after some time."];                                
                }
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
            }   
        }catch (Exception $e) {
                $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'status'];

        }

        echo json_encode($resposne);
    }

}