<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="clearfix mb-10">
        <div class="pull-left">
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo base_url('variance')?>">Variance Details</a></li>
                </ol>
            </nav>
        </div>
        <div class="pull-right"> 
            <a class="btn btn-outline-primary" href="<?php echo base_url('variance/addVariance')?>">Add New</a>            
        </div>
    </div>

    <div class="container-fluid bg-light"> 
        <div class="row">
            <div class="col-lg-12">

                <form id="attrsrch" action="<?php echo base_url('variance')?>">
                    <input type="hidden" name="per_page_search" value="<?php echo $page; ?>">
                    <div class="row align-items-center justify-content-center">
                        <div class="col-md-3 pt-4 pb-4">
                            <input type="text" name="name_en" id="name_en" value="<?php echo (isset($var_search["name_en"]))?$var_search["name_en"]:'';?>" placeholder="Name" class="form-control">
                        </div>

                        <div class="col-md-3 pt-4 pb-4">
                            <select id="type" name="type" class="form-control">
                                <option value="">Select Type</option>
                                <option value="1" <?php echo (isset($var_search) && isset($var_search["type"]) &&  $var_search["type"]==1)?'selected':'';?>>Item</option>
                                <option value="2"  <?php echo (isset($var_search) && isset($var_search["type"]) &&  $var_search["type"]==2)?'selected':'';?>>Service</option>
                            </select>
                        </div>

                        <div class="col-md-3 pt-4 pb-4">
                            <select id="status" name="status" class="form-control">
                                <option value="">Select Status</option>
                                <option value="1" <?php echo (isset($var_search) && isset($var_search["status"]) &&  $var_search["status"]==1)?'selected':'';?>>Active</option>
                                <option value="0"  <?php echo (isset($var_search) && isset($var_search["status"]) &&  $var_search["status"]==0)?'selected':'';?>>InActive</option>
                            </select>
                        </div>
                        <div class="col-md-3 pt-4 pb-4">
                            <a class="btn btn-outline-primary" href="<?php echo base_url('variance')?>"><i class="fa fa-refresh" aria-hidden="true"></i> Reset</a> 
                                
                            <input type="submit" name="search" value="Search" class="btn btn-outline-primary">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="row pt-4">
        
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="data-table stripe hover no-wrap" style="font-size: 14px;">
            <thead>
                <tr class="titel_bar">
                    <th class="table-plus datatable-nosort" width="5%">#</th>
                    <th class="table-plus datatable-nosort" width="30%">Name</th>
                    <th class="table-plus datatable-nosort" width="30%">Name in French</th>
                    <th class="table-plus datatable-nosort" width="20%">Type</th>
                    <th class="table-plus datatable-nosort" width="20%">Status</th>
                    <th class="table-plus datatable-nosort" width="20%" ><center>Action</center></th>
                </tr>
            </thead>
            <tbody>

            <?php
            	if(!empty($variance_data)){
            		$i=0;
            		foreach($variance_data as $val){
            			$i++;
            			?>
            			 <tr class="">
							<td ><?php echo $i;?></td>	
							<td >
                                <?php echo $val["name_en"];?>           
                            </td>
                            <td >
                                <?php echo $val["name_fr"];?>           
                            </td>	
                            <td>
                                <select name="type" id="type" onchange="doVarTypeChange(this.value,'<?php echo base64_encode($val["id"]);?>')" class="form-control">
                                    <option value="1" <?php echo (isset($val) && isset($val["type"]) && $val['type'] == 1)?'selected':''; ?>> Item </option>

                                    <option value="2" <?php echo (isset($val) && isset($val["type"]) && $val['type'] == 2)?'selected':''; ?>> Service </option>
                                </select>
                            </td>
                            <td>
                                <select name="status" id="status" onchange="doVarStatusChange(this.value,'<?php echo base64_encode($val["id"]);?>')" class="form-control">
                                    <option value="1" <?php echo (isset($val) && isset($val["status"]) && $val['status'] == 1)?'selected':''; ?>> Active </option>

                                    <option value="0" <?php echo (isset($val) && isset($val["status"]) && $val['status'] == 0)?'selected':''; ?>> InActive </option>
                                </select>
                            </td>
                            <td class="table-plus" style="text-align:center;">

                                <div class="dropdown">
                                    <a class="btn btn-outline-primary dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                        <i class="fa fa-ellipsis-h"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item" href="javascript:void(0);" style=" font-size: 15px;" onclick="doVarEdit('<?php echo base64_encode($val["id"]);?>')" >
                                            <i class="fa fa-edit" aria-hidden="true"></i> 
                                            Edit
                                        </a>
                                    </div>
                                </div>

                            </td>
							

            		 	</tr>
            			<?php
            		}
            	}else{
            		?>
            		 <tr class="">
            		 	 <td colspan="7">No Record Found</td>	

            		 </tr>	
            		<?php
            	}
            ?>
           
                 
            </tbody>
        </table>
    </div>

    <div class="row pt-4">
        <div class="col-sm-12">
            <div style="float: right;"><?php echo $links; ?></div>
        </div>
    </div>
</div>


<?php
	if(isset($custom_js)){
		foreach($custom_js as $val){
			?>
			<script src="<?php echo base_url();?>assets/js/custom_js/<?php echo $val;?>"></script>
			<?php
		}
	}
	
?>
  