<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="clearfix mb-10">
        <div class="pull-left">
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo base_url('variance')?>">Variance Details</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Add New Variance Details</li>
                </ol>
            </nav>
        </div>  
        <div class="pull-right">
            <a class="btn btn-outline-primary" href="<?php echo base_url('variance')?>">Back</a>            
        </div> 
    </div> 

    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">

                <form id="addvariancefrm" method="post" action="javascript:void(0);" onsubmit="addVariance();">

                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-6">
                                <label label-for='name'> Name <span class="mandatory_label">*</span></label>
                                <input type="text" name="name_en" id="name_en" placeholder="Name" value="" class="form-control">
                            </div>

                            <div class="col-lg-6">
                                <label label-for='name'> Name [French] <span class="mandatory_label">*</span></label>
                                <input type="text" name="name_fr" id="name_fr" placeholder="Name" value="" class="form-control">
                            </div>
                        </div>
                    </div>

                    <br>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-3">
                                <label label-for='type'> Type <span class="mandatory_label">*</span></label>
                                <select name="type" class="form-control" id="type">
                                    <option value=""> -- Select -- </option>
                                    <option value="1" >Item</option>
                                    <option value="2" >Service</option>
                                </select>
                            </div>

                            <div class="col-lg-3">
                                <label for="active"> Status </label>
                                <select name="active" class="form-control" id="active">
                                    <option value="1" >Active</option>
                                    <option value="0" >Inactive</option>
                                </select>
                            </div>

                            <div class="col-lg-6 text-right pt-20">
                                <input type="submit" value="Submit" name="submit" class="btn btn-outline-primary">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>




<?php
    if(isset($custom_js)){
        foreach($custom_js as $val){
            ?>
            <script src="<?php echo base_url();?>assets/js/custom_js/<?php echo $val;?>"></script>
            <?php
        }
    }
    
?>