<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="clearfix mb-10">
        <div class="pull-left">
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo base_url('quotation')?>">Quotation</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Details</li>
                </ol>
            </nav>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row" style="padding-bottom: 20px;">
            <div class="col-lg-8">
                <label style="font-weight: 400;">By : </label> <span id="customer"> <?php echo $result_details['customer']; ?> </span>
            </div>

        <?php

            if ($result_details['expiry_date'] != null) {
                
        ?>
        
            <div class="col-lg-4">
                <label style="font-weight: 400;">Expired On : </label> <span id="expiry_date"> <?php echo $result_details['expiry_date']; ?> </span>
            </div>

        <?php

            }

        ?>
            
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="row" style="padding-bottom: 20px;">
                    <div class="col-lg-12">
                        <label style="font-weight: 400;">Quotation : </label>
                        <br>
                        <div id="message"> <?php echo $result_details['message']; ?> </div>
                    </div>
                </div>

            <?php 

                if ($result_details['other_requerment'] != '') {
            
            ?>

                <div class="row" style="padding-bottom: 20px;">
                    <div class="col-lg-12">
                        <label style="font-weight: 400;">Note : </label>
                        <br>
                        <div id="other_requerment"> <?php echo $result_details['other_requerment']; ?> </div>
                    </div>
                </div>

            <?php

                }

            ?>

                
            </div>
        </div>

<?php 

    if (count($result_details['image']) > 0) { 
        if ($result_details['image'][0]['img_id'] != '') {
                
?>

        <div class="row" style="padding-bottom: 20px;" id="imgRow">
            <div class="col-lg-12" id="imgCol">
                
            <?php

                foreach ($result_details['image'] as $img) {
                    
            ?>
            
                <div style='display: inline-block; padding-right: 7px;'>
                    <img src='<?php echo base_url("uploads/rfq/" . $img["image"]); ?>' style='border: 2px dotted #ccc; border-radius: 2px; height: 130px; width: 120px'/> 
                </div>

            <?php

                }

            ?>

            </div>
        </div>

<?php

        }
    }

?>

        
    </div>

    <div class="container-fluid"> 
        <div class="row">      
            <table class="data-table stripe hover nowrap" id="userlst">
                <thead>
                    <tr>
                        <th width="2%"  class="table-plus datatable-nosort" >#</th>
                        <th width="15%"  class="table-plus datatable-nosort">From</th>
                        <th width="15%"  class="table-plus datatable-nosort">Replied By</th>
                        <th width="15%"  class="table-plus datatable-nosort">Replied On</th>
                        <!-- <th width="15%"  class="table-plus datatable-nosort">Message</th> -->
                        <th width="10%"  class="table-plus datatable-nosort text-center" >Action</th>                   
                    </tr>
                </thead>
                <tbody>

        <?php

            if(!empty($response_details)){
                $i=0;
                foreach($response_details as $val){
                    $i++;
        ?>

                    <tr class="">
                        <td class="table-plus" ><?php echo $i;?></td>  
                        <td class="table-plus" >
                            <?php echo $val["requested"];?>      
                        </td>
                        <td class="table-plus" >
                            <?php echo $val["responsed"];?>                                              
                        </td>
                        <td class="table-plus">
                            <?php echo date('d-m-Y', strtotime($val["date"])); ?>                                              
                        </td>
                        <td class="table-plus text-center">
                            <a class="btn btn-outline-primary response" href="#" role="button" data-toggle="modal" data-rfq-id="<?php echo base64_encode($val['rfq_id']) ?>" data-to-customer-id="<?php echo base64_encode($val['to_customer_id']) ?>" data-from-customer-id="<?php echo base64_encode($val['from_customer_id']) ?>" data-name="<?php echo $val["responsed"];?>">
                                <i class="fa fa-eye"></i>
                            </a>
                        </td>
                    </tr>

        <?php
                }
            }
          
        ?>
                    
                </tbody>
            </table>
        </div>
    </div>      
</div>



<!-- Modal -->

<div class="modal fade admin_modal" id="chatviewModal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h4 class="modal-title"> <span id="bussiness"> Bussiness Name </span> </h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                
                <div class="Message_box_control" id="chatBoxDetails"></div>

            </div>

        </div>
    </div>
</div>


<!-- Modal Ends -->


<div style="display: none;" id="clone">
    <div class="Message_box_sec replyMsg" id="replyMsg">
        <div class="Message_box_sec_img">
            <img src="" alt="" id="replyIcon">
        </div>
        <div class="Message_box_sec_info">
            <p class="Message_send_date"><span id="replyName"></span> <span id="replyDate"></span></p>
            <div class="Message_send_cont">
                <p id="replyText"></p>
                <p id="replyImage" class="d-none">
                    <img src="" alt="" id="replyImg">
                </p>
            </div>
        </div>
    </div>

    <div class="Message_box_sec myMsg" id="myMsg">
        <div class="Message_box_sec_img">
            <img src="" alt="" id="myImg">
        </div>
        <div class="Message_box_sec_info">
            <p class="Message_send_date"><span id="myName"></span> <span id="myDate"></span></p>
            <div class="Message_send_cont">
                <p id="myText"></p>
                <p id="myImage" class="d-none">
                    <img src="" alt="" id="myImg">
                </p>
            </div>
        </div>
    </div>
</div>



<script type="text/javascript">
    
    $('.response').click(function(){
        var base_url = $('#base_url').val();
        var rfq = $(this).data('rfq-id');
        var to = $(this).data('to-customer-id');
        var from = $(this).data('from-customer-id');
        var name = $(this).data('name');
        
        $.ajax({
            url: base_url + 'quotation/Chat',
            type: 'POST',
            data: {rfq_id: rfq, to_customer_id: to, from_customer_id: from},
            dataType: 'JSON',
            success: function(response) {      
                console.log(response);

                $('#bussiness').html(name);
                $('#chatBoxDetails').empty();
                var i=0;
                var from_id ='';
                $.each(response.data, function(key, value) {
                    if(i==0){
                        from_id = value.from_customer_id; // For Left ChatBox
                    }
                    i++;                   
                    
                    if (value.from_customer_id == from_id) {

                        var replyMsg = $('#replyMsg').clone();

                        replyMsg.find('#replyIcon').attr('src', base_url + 'uploads/bussiness_logo/' + value.business_logo).end();
                        replyMsg.find('#replyName').html(value.requested).end();
                        replyMsg.find('#replyDate').html(value.date).end();
                        replyMsg.find('#replyText').html(value.message).end();

                        if (value.images != null) {
                            $('#replyImage').removeClass('d-none');

                            $.each(value.images, function(key, val){
                                replyMsg.find('#replyImg').attr('src', base_url + 'uploads/rfq/' + val.images).end();
                            });                            
                        }

                        $('#chatBoxDetails').append(replyMsg);
                    } else{
                        var myMsg = $('#myMsg').clone();

                        myMsg.find('#myImg').attr('src', base_url + 'uploads/bussiness_logo/' + value.business_logo).end();
                        myMsg.find('#myName').html(value.requested).end();
                        myMsg.find('#myDate').html(value.date).end();
                        myMsg.find('#myText').html(value.message).end();

                        if (value.images != null) {
                            $('#myImage').removeClass('d-none');

                            $.each(value.images, function(key, val){
                                myMsg.find('#myImg').attr('src', base_url + 'uploads/rfq/' + val.images).end();
                            });                            
                        }

                        $('#chatBoxDetails').append(myMsg);
                    }                 
                });                
                
                $('#chatviewModal').modal('show');
            }
        });


    });

</script>
