<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class RfqModel extends CI_Model {
	protected $buyerTable = 'buyer_profile'; 
	protected $sellerTable = 'seller_profile'; 
	protected $catTable = 'category';
	protected $itemTable = 'item';
	protected $speciesTable = 'species';
	protected $uomTable = 'uom';
	protected $quoteTable = 'rfq';
	protected $customerTable = 'customer';
	protected $rfqImgTable = 'rfq_image';
	protected $threadTable = 'rfq_thread';
	protected $threadImgTable = 'rfq_thread_image';
	
	public function __construct() {
        parent::__construct();
    }
     
    
    public function getQuoteCount($data){
    	try {
    		$this->db->db_debug = false;

    		// SELECT r.rfq_id, c.name AS customer, cat.category_name_en AS category, r.quantity, r.expiry_date, r.status, u.name_en AS uom FROM t_rfq r LEFT JOIN t_customer c ON r.buyer_id = c.id LEFT JOIN t_category cat ON cat.id = r.cat_id LEFT JOIN t_uom u ON u.id = r.uom_id


    		$this->db->select("r.rfq_id, c.name AS customer, cat.category_name_en AS category, r.quantity, r.expiry_date, r.status, u.name_en AS uom");
    		$this->db->from($this->quoteTable." r");
    		$this->db->join($this->customerTable . ' c' , 'ON r.buyer_id = c.id', 'LEFT');
    		$this->db->join($this->catTable . ' cat' , 'ON cat.id = r.cat_id', 'LEFT');
    		$this->db->join($this->uomTable . ' u' , 'ON u.id = r.uom_id', 'LEFT');
    		
    		if(isset($data["cust_id"]) && $data["cust_id"]!=''){
	    		$this->db->where("r.buyer_id",$data["cust_id"]);
	    	}

	    	if(isset($data["active"]) && $data["active"]!=''){
	    		$this->db->where("r.status",$data["active"]);
	    	}

	    	// $this->db->get();
	    	// echo $this->db->last_query();die();

	    	$db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	// echo $this->db->last_query();die();
	        	return $this->db->count_all_results();

	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
    }

    public function getQuoteList($data, $limit='', $start=0){
    	try {
    		$this->db->db_debug = false;

    		$this->db->select("r.rfq_id, c.name AS customer, cat.category_name_en AS category, r.quantity, r.expiry_date, r.status, u.name_en AS uom");
    		$this->db->from($this->quoteTable." r");
    		$this->db->join($this->customerTable . ' c' , 'ON r.buyer_id = c.id', 'LEFT');
    		$this->db->join($this->catTable . ' cat' , 'ON cat.id = r.cat_id', 'LEFT');
    		$this->db->join($this->uomTable . ' u' , 'ON u.id = r.uom_id', 'LEFT');
    		
    		if(isset($data["cust_id"]) && $data["cust_id"]!=''){
	    		$this->db->where("r.buyer_id",$data["cust_id"]);
	    	}

	    	if(isset($data["active"]) && $data["active"]!=''){
	    		$this->db->where("r.status",$data["active"]);
	    	}

	    	$this->db->order_by('r.created_date', 'DESC');

	    	if($limit!='' && $start>=0 ){
	    		$this->db->limit($limit, $start);
	    	} 	    	
 
 			$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query();die();
	        	$result=[];
	        	foreach($query->result_array() as $row){
	        		$result[]=$row;
	        	}
	        	return $result;
	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
    }	

    
    public function getDetailById($id) {
    	try {
		 	$this->db->db_debug = false;

		 	// SELECT r.rfq_id, c.name AS customer, cat.category_name_en AS category, r.quantity, r.expiry_date, r.other_requerment, r.status, u.name_en AS uom, ri.image FROM t_rfq r LEFT JOIN t_customer c ON r.buyer_id = c.id LEFT JOIN t_category cat ON cat.id = r.cat_id LEFT JOIN t_uom u ON u.id = r.uom_id LEFT JOIN t_rfq_image ri ON ri.req_id = r.rfq_id WHERE r.rfq_id = 1


		 	$this->db->select("r.rfq_id, c.name AS customer, cat.category_name_en AS category, r.quantity, r.expiry_date, r.status, u.name_en AS uom, ri.id, ri.image");
    		
    		$this->db->from($this->quoteTable." r");
    		$this->db->join($this->customerTable . ' c' , 'ON r.buyer_id = c.id', 'LEFT');
    		$this->db->join($this->catTable . ' cat' , 'ON cat.id = r.cat_id', 'LEFT');
    		$this->db->join($this->uomTable . ' u' , 'ON u.id = r.uom_id', 'LEFT');
    		$this->db->join($this->rfqImgTable . ' ri' , 'ON r.rfq_id = ri.req_id', 'LEFT');    		

    		$this->db->where('r.rfq_id', $id);

    		$query=$this->db->get();

    		//echo $this->db->last_query();die();

    		$db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{


	        	$data=[];
	        	foreach($query->result_array() as $row){
					$rfq = $row['rfq_id'] . '_data';

	        		if (!isset($data[$rfq])) {
	        			$data[$rfq]['id'] = $row['rfq_id'];
	        			$data[$rfq]['quantity'] = $row['quantity'];

	        			if ($row['expiry_date'] != NULL) {
	        				$data[$rfq]['expiry_date'] = date('d-m-Y', strtotime($row['expiry_date']));
	        			}
	        			else {
	        				$data[$rfq]['expiry_date'] = '';
	        			}
	        			
	        			$data[$rfq]['uom'] = $row['uom'];
	        			$data[$rfq]['customer'] = $row['customer'];
	        			$data[$rfq]['category'] = $row['category'];
	        			$data[$rfq]['image'] = [];
	        		}

	        		$data[$rfq]['image'][] = ['img_id' => $row['id'], 'image' => $row['image']];
	        	}
	        	
	        	//echo $this->db->last_query();die();
	        	// print_r($data); die();
	        	return array_values($data);
	        }
	        return TRUE;
	    } 
	    catch (Exception $e) {
	       throw new Exception();
	    }	

    }

    public function getResponseDetailById($id) {
    	try {
		 	$this->db->db_debug = false;

		 	// SELECT rt.id, rt.rfq_id, rt.to_customer_id, rt.from_customer_id, c.name AS requested, ct.name AS responsed, MAX(rt.date) AS date FROM t_rfq_thread rt LEFT JOIN t_customer c ON rt.from_customer_id = c.id LEFT JOIN t_customer ct ON rt.to_customer_id = ct.id WHERE rt.rfq_id = 3 GROUP BY rt.rfq_id ORDER BY rt.date DESC
 

		 	$this->db->select("rt.id, rt.rfq_id, rt.to_customer_id, rt.from_customer_id, cf.name AS requested, ct.name AS responsed, MAX(rt.date) AS date");
    		$this->db->from($this->threadTable." rt");

    		$this->db->join($this->customerTable . ' cf' , 'ON rt.from_customer_id = cf.id', 'LEFT');
    		$this->db->join($this->customerTable . ' ct' , 'ON rt.to_customer_id = ct.id', 'LEFT');

    		$this->db->where('rt.rfq_id', $id);
    		$this->db->group_by('rt.rfq_id');
    		$this->db->order_by('rt.date', 'DESC');

    		$query=$this->db->get();

    		//echo $this->db->last_query();die();

    		$db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{

	        	if ($query->num_rows() > 0) {
	        		$result=[];
		        	foreach($query->result_array() as $row){
		        		if ($row['id'] != null) {
		        			$result[] = $row;
		        		}		        		
		        	}
		        	//print_r($result); die();
		        	return $result;
	        	}	        	
	        }
	    } 
	    catch (Exception $e) {
	       throw new Exception();
	    }	

    } 


    public function getChatDetail($rfq_id, $to_customer_id, $from_customer_id) {
    	try {
		 	$this->db->db_debug = false;

		 	// SELECT rt.message, rti.images, c.name AS requested, ct.name AS responsed, rt.date, rt.to_customer_id, rt.from_customer_id, b.business_logo FROM t_rfq_thread rt LEFT JOIN t_rfq_thread_image rti ON rt.rfq_id = rti.rfq_thread_id LEFT JOIN t_customer c ON rt.to_customer_id = c.id LEFT JOIN t_customer ct ON rt.from_customer_id = ct.id LEFT JOIN t_buyer_profile b ON b.cust_id = rt.to_customer_id WHERE rt.rfq_id = 5 AND (rt.to_customer_id = 35 OR rt.to_customer_id = 31) AND (rt.from_customer_id = 31 OR rt.from_customer_id = 35)
 

		 	$this->db->select("rt.message, rti.images, c.name AS requested, ct.name AS responsed, rt.date, rt.to_customer_id, rt.from_customer_id, b.business_logo");
    		$this->db->from($this->threadTable." rt");

    		$this->db->join($this->customerTable . ' c' , 'ON rt.to_customer_id = c.id', 'LEFT');
    		$this->db->join($this->customerTable . ' ct' , 'ON rt.from_customer_id = ct.id', 'LEFT');
    		$this->db->join($this->buyerTable . ' b' , 'ON b.cust_id = rt.to_customer_id', 'LEFT');
    		$this->db->join($this->threadImgTable . ' rti' , 'ON rt.rfq_id = rti.rfq_thread_id', 'LEFT');

    		$this->db->where('rt.rfq_id', $rfq_id);
    		$this->db->where('(rt.to_customer_id = ' . $to_customer_id . ' OR rt.to_customer_id = ' . $from_customer_id . ')');
    		$this->db->where('(rt.from_customer_id = ' . $from_customer_id . ' OR rt.from_customer_id = ' . $to_customer_id . ')');

    		$query=$this->db->get();

    		//echo $this->db->last_query();die();

    		$db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	$result=[];
	        	foreach($query->result_array() as $row){
	        		$result[] = $row;
	        	}
	        	//print_r($result); die();
	        	return $result;
	        }
	        return TRUE;
	    } 
	    catch (Exception $e) {
	       throw new Exception();
	    }	

    }

    public function updateRfq($data, $id) {
    	$res = $this->db->update($this->quoteTable, $data, ['rfq_id' => $id]);

    	return ($res)?true:false;
    }
    
}

?>