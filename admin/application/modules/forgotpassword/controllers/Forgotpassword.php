<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Forgotpassword extends CI_Controller {

    public function __construct() {
        parent::__construct();
        
    }
    
    public function index(){
        $this->load->view("forgotpassword");
    }

    public function doLoginValidate(){
        try { 
            $data=$this->input->post();
            $resposne=["status"=>"0","message"=>"","field"=>''];
            if(!empty($data)){
                if(!(isset($data["user_name"]) && trim($data["user_name"])!=='')){
                    $resposne=["status"=>"1","message"=>"Please Enter User Name.","field"=>'user_name'];
                }else if(!(isset($data["password"]) && trim($data["password"])!=='')){
                    $resposne=["status"=>"1","message"=>"Please Enter password.","field"=>'password'];
                }else{

                    $user_data=$this->userModel->doLoginCheck($data);
                    
                    if(count($user_data)==1){
                        $token=$this->userModel->generateToken();
                        $user_data=current($user_data);
                        $this->userModel->updateUser(["token_id"=>$token],$user_data);
                        set_cookie('token_id',$token,'3600'); 
                        
                        $resposne=["status"=>"0","message"=>"Login Successful"];
                    }else{
                        $resposne=["status"=>"1","message"=>"Wrong User Name Or Password.","field"=>'user_name'];
                    }
                }
            }else{
                $resposne=["status"=>"1","message"=>"Invalid credential","field"=>'user_name'];
            }
        } catch (Exception $e) {
            $resposne=["status"=>"1","e"=>$e,"message"=>"Unexpected server error.Please try again some time.","field"=>'user_name'];
         // var_dump($e->getMessage());
        }
        echo json_encode($resposne);
    }
}
