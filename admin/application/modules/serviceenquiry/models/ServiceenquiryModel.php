<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class ServiceenquiryModel extends CI_Model {
	protected $buyerTable = 'buyer_profile'; 
	//protected $sellerTable = 'seller_profile'; 
	//protected $catTable = 'category';
	protected $itemTable = 'item';
	//protected $speciesTable = 'species';
	protected $uomTable = 'uom';
	protected $enqTable = 'enquiry'; //
	protected $customerTable = 'customer';
	protected $enqImgTable = 'enquiry_image';
	protected $threadTable = 'enquiry_thread';
	protected $threadImgTable = 'enquiry_image';
	protected $siTable = 'seller_item';
	
	public function __construct() {
        parent::__construct();
    }
     
    
    public function getEnqCount($data){
    	//print_r($data); //Array ( [to_date] => 2019-10-10 )
    	try {
    		$this->db->db_debug = false;

    		// SELECT e.id, ct.name AS to_cust, cf.name AS from_cust, e.quantity, e.message, e.date, i.item_name, u.name AS uom FROM t_enquiry e LEFT JOIN t_customer ct ON e.to_id = ct.id LEFT JOIN t_customer cf ON e.from_id = cf.id LEFT JOIN t_seller_item si ON e.seller_item_id = si.id LEFT JOIN t_item i ON si.item_id = i.id LEFT JOIN t_uom u ON u.id = e.uom_id WHERE e.type = 1


    		$this->db->select("e.id, ct.name AS to_cust, cf.name AS from_cust, e.quantity, e.message, e.date, u.name AS uom");
    		$this->db->from($this->enqTable." e");
    		$this->db->join($this->customerTable . ' ct' , 'ON e.to_id = ct.id', 'LEFT');
    		$this->db->join($this->customerTable . ' cf' , 'ON e.from_id = cf.id', 'LEFT');
    		$this->db->join($this->siTable . ' si' , 'ON e.seller_item_id = si.id', 'LEFT');
    		$this->db->join($this->uomTable . ' u' , 'ON u.id = e.uom_id', 'LEFT');
    		
    		if(isset($data["cust_id"]) && $data["cust_id"]!=''){
	    		$this->db->where("e.to_id",$data["cust_id"]);
	    	}

	    	/*if(isset($data["from_date"]) && $data["from_date"]!=''){
	    		$this->db->where('e.date > ' . $data["from_date"]);
	    	}*/

	    	if(isset($data["to_date"]) && $data["to_date"]!=''){
	    		$this->db->where('DATE_FORMAT(e.date, "%Y-%m-%d") = "' . $data["to_date"] . '"');
	    	}

	    	/*if( (isset($data["from_date"]) && $data["from_date"]!='') && (isset($data["to_date"]) && $data["to_date"]!='') ) {
	    		$this->db->where('e.date BETWEEN ' . $data["from_date"] . ' AND ' . $data["to_date"]);
	    	}*/

	    	$this->db->where('e.type', 2);

	    	$this->db->order_by('e.date', 'DESC');

	    	$query=$this->db->get();

	    	$db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query();die();
	        	return $this->db->count_all_results();

	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
    }

    public function getEnqList($data, $limit='', $start=0){
    	try {
    		$this->db->db_debug = false;

    		$this->db->select("e.id, ct.name AS to_cust, cf.name AS from_cust, e.quantity, e.message, e.date, u.name AS uom");
    		$this->db->from($this->enqTable." e");
    		$this->db->join($this->customerTable . ' ct' , 'ON e.to_id = ct.id', 'LEFT');
    		$this->db->join($this->customerTable . ' cf' , 'ON e.from_id = cf.id', 'LEFT');
    		$this->db->join($this->siTable . ' si' , 'ON e.seller_item_id = si.id', 'LEFT');
    		$this->db->join($this->uomTable . ' u' , 'ON u.id = e.uom_id', 'LEFT');
    		
    		if(isset($data["cust_id"]) && $data["cust_id"]!=''){
	    		$this->db->where("e.to_id",$data["cust_id"]);
	    	}

	    	/*if(isset($data["from_date"]) && $data["from_date"]!=''){
	    		$this->db->where('e.date > ' . $data["from_date"]);
	    	}*/

	    	if(isset($data["to_date"]) && $data["to_date"]!=''){
	    		$this->db->where('DATE_FORMAT(e.date, "%Y-%m-%d") = "' . $data["to_date"] . '"');
	    	}

	    	/*if( (isset($data["from_date"]) && $data["from_date"]!='') && (isset($data["to_date"]) && $data["to_date"]!='') ) {
	    		$this->db->where('e.date BETWEEN ' . $data["from_date"] . ' AND ' . $data["to_date"]);
	    	}*/

	    	$this->db->where('e.type', 2);

	    	$this->db->order_by('e.date', 'DESC');

	    	if($limit!='' && $start>=0 ){
	    		$this->db->limit($limit, $start);
	    	} 	    	
 
 			$query=$this->db->get();

 			//echo $this->db->last_query();


	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query();
	        	$result=[];
	        	foreach($query->result_array() as $row){
	        		$result[]=$row;
	        	}
	        	return $result;
	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
    }	

    
    public function getDetailById($id) {
    	try {
		 	$this->db->db_debug = false;

		 	// SELECT e.id, ct.name AS to_cust, cf.name AS from_cust, e.quantity, e.message, u.name AS uom, DATE_FORMAT(e.date, "%d-%m-%Y %H:%i") AS date FROM t_enquiry e LEFT JOIN t_customer ct ON e.to_id = ct.id LEFT JOIN t_customer cf ON e.from_id = cf.id LEFT JOIN t_uom u ON u.id = e.uom_id WHERE e.id = 4 AND e.type = 2


    		$this->db->select('e.id, ct.name AS to_cust, cf.name AS from_cust, e.quantity, e.message, u.name AS uom, DATE_FORMAT(e.date, "%d-%m-%Y %H:%i") AS date');
    		$this->db->from($this->enqTable." e");
    		$this->db->join($this->customerTable . ' ct' , 'ON e.to_id = ct.id', 'LEFT');
    		$this->db->join($this->customerTable . ' cf' , 'ON e.from_id = cf.id', 'LEFT');
    		$this->db->join($this->uomTable . ' u' , 'ON u.id = e.uom_id', 'LEFT');

    		$this->db->where('e.id', $id);
    		$this->db->where('e.type', 2);

    		$query=$this->db->get();

    		//echo $this->db->last_query();die();

    		$db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query();die();
	        	$result=[];
	        	foreach($query->result_array() as $row){
	        		$result[]=$row;
	        	}
	        	return $result;
	        }
	    } 
	    catch (Exception $e) {
	       throw new Exception();
	    }	

    }

    public function getResponseDetailById($id) {
    	try {
		 	$this->db->db_debug = false;

		 	// SELECT et.id, et.enquiry_id, et.to_customer_id, et.from_customer_id, ct.name AS to_cust, cf.name AS from_cust, MAX(et.date) AS date, et.is_seen FROM t_enquiry_thread et LEFT JOIN t_customer ct ON et.to_customer_id = ct.id LEFT JOIN t_customer cf ON et.from_customer_id = cf.id WHERE et.enquiry_id = 4 ORDER By et.date DESC

		 	$this->db->select('et.id, et.enquiry_id, et.to_customer_id, et.from_customer_id, ct.name AS to_cust, cf.name AS from_cust, MAX(et.date) AS date, et.is_seen');
    		$this->db->from($this->threadTable." et");

    		$this->db->join($this->customerTable . ' ct' , 'ON et.to_customer_id = ct.id', 'LEFT');
    		$this->db->join($this->customerTable . ' cf' , 'ON et.from_customer_id = cf.id', 'LEFT');

    		$this->db->where('et.enquiry_id', $id);
    		$this->db->order_by('et.date', 'DESC');

    		$query=$this->db->get();

    		//echo $this->db->last_query();die();

    		$db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{

	        	if ($query->num_rows() > 0) {
	        		$result=[];
		        	foreach($query->result_array() as $row){
		        		if ($row['id'] != null) {
		        			$result[] = $row;
		        		}		        		
		        	}
		        	//print_r($result); die();
		        	return $result;
	        	}	        	
	        }
	        //return TRUE;
	    } 
	    catch (Exception $e) {
	       throw new Exception();
	    }	

    } 


    public function getChatDetail($enquiry_id, $to_customer_id, $from_customer_id) {
    	try {
		 	$this->db->db_debug = false;

		 	// SELECT et.message, ei.image, c.name AS to_cust, ct.name AS from_cust, DATE_FORMAT(et.date, "%d-%m-%Y %H:%i:%s") AS date, bt.business_logo AS to_dp, bf.business_logo AS from_dp, et.to_customer_id, et.from_customer_id, et.is_seen FROM t_enquiry_thread et LEFT JOIN t_buyer_profile bt ON bt.cust_id = et.to_customer_id LEFT JOIN t_buyer_profile bf ON bf.cust_id = et.from_customer_id LEFT JOIN t_enquiry_image ei ON ei.enquiry_thread_id = et.id LEFT JOIN t_customer c ON et.to_customer_id = c.id LEFT JOIN t_customer ct ON et.from_customer_id = ct.id WHERE et.enquiry_id = 4 AND (et.to_customer_id = 1 OR et.to_customer_id = 11) AND (et.from_customer_id = 11 OR et.from_customer_id = 1)s
 

		 	$this->db->select('et.message, ei.image, c.name AS to_cust, ct.name AS from_cust, DATE_FORMAT(et.date, "%d-%m-%Y %H:%i:%s") AS date, bt.business_logo AS to_dp, bf.business_logo AS from_dp, et.to_customer_id, et.from_customer_id, et.is_seen');
    		$this->db->from($this->threadTable." et");
    		$this->db->join($this->threadImgTable . ' ei' , 'ON ei.enquiry_thread_id = et.id', 'LEFT');

    		$this->db->join($this->buyerTable . ' bt' , 'ON bt.cust_id = et.to_customer_id', 'LEFT');
    		$this->db->join($this->buyerTable . ' bf' , 'ON bf.cust_id = et.from_customer_id', 'LEFT');

    		$this->db->join($this->customerTable . ' c' , 'ON et.to_customer_id = c.id', 'LEFT');
    		$this->db->join($this->customerTable . ' ct' , 'ON et.from_customer_id = ct.id', 'LEFT');    		

    		$this->db->where('et.enquiry_id', $enquiry_id);
    		$this->db->where('(et.to_customer_id = ' . $to_customer_id . ' OR et.to_customer_id = ' . $from_customer_id . ')');
    		$this->db->where('(et.from_customer_id = ' . $from_customer_id . ' OR et.from_customer_id = ' . $to_customer_id . ')');

    		$query=$this->db->get();

    		//echo $this->db->last_query();die();

    		$db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	$result=[];
	        	foreach($query->result_array() as $row){
	        		$result[] = $row;
	        	}
	        	//print_r($result); die();
	        	return $result;
	        }
	        return TRUE;
	    } 
	    catch (Exception $e) {
	       throw new Exception();
	    }	

    }
    
}

?>