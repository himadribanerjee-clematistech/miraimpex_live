<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="clearfix mb-10">
        <div class="pull-left">
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo base_url('serviceenquiry')?>">Service Enquiry Details</a></li>
                    <li class="breadcrumb-item active" aria-current="page">List</li>
                </ol>
            </nav>
        </div>
    </div>

    <div class="container-fluid bg-light"> 
        <div class="row">
            <div class="col-lg-12">
  
                <form id="enqsrch" action="<?php echo base_url('serviceenquiry') ?>" autocomplete="off">

                    <input type="hidden" name="per_page_search" value="<?php echo $page; ?>">

                    <div class="row align-items-center justify-content-center">
                        <div class="col-md-3 pt-3">
                            <div class="form-group ">
                                <select id="cust_id" name="cust_id" class="form-control">
                                    <option value="">-- Customer --</option>

                                <?php foreach ($customer as $c) : ?>

                                    <option value="<?php echo $c['id']; ?>" <?php echo (isset($serviceenquiry_search) && isset($serviceenquiry_search["cust_id"]) &&  $serviceenquiry_search["cust_id"]== $c['id']) ? 'selected' : ''; ?> > <?php echo $c['name']; ?> </option>

                                <?php endforeach; ?>

                                </select>  
                            </div>
                        </div>

                        <!-- <div class="col-md-3 pt-3">
                            <div class="form-group">
                                <input type="text" name="from_date" id="from_date" class="form-control" placeholder="From Date">
                            </div>
                        </div> -->

                        <div class="col-md-3 pt-3">
                            <div class="form-group">
                                <input type="text" name="to_date" id="to_date" class="form-control" placeholder="Service Enquiry Date" value="<?php echo (isset($serviceenquiry_search) && isset($serviceenquiry_search["to_date"]) &&  $serviceenquiry_search["to_date"]!= '') ? date('d-m-Y', strtotime($serviceenquiry_search["to_date"])) : ''; ?>">
                            </div>
                        </div>
                        
                        <div class="col-md-3 pt-3">
                            <div class="form-group">
                                <a class="" href="<?php echo base_url('serviceenquiry')?>"><button type="button" class="btn btn-outline-primary "><i class="fa fa-refresh" aria-hidden="true"></i> Reset</button></a> 

                                <input type="submit" name="search" value="Search" class="btn btn-outline-primary">                                
                            </div>                       
                        </div>
                    </div>
                
                </form>

            </div>
        </div>
    </div>

    <div class="row">
        <table class="data-table stripe hover nowrap" id="userlst">
            <thead>
                <tr>
                    <th width="2%"  class="table-plus datatable-nosort" >#</th>
                    <th width="20%"  class="table-plus datatable-nosort">From</th>
                    <th width="15%"  class="table-plus datatable-nosort">To</th>
                    <th width="10%"  class="table-plus datatable-nosort">Quantity</th>
                    <th width="15%"  class="table-plus datatable-nosort">Enquiry Date</th>
                    <th width="5%"  class="table-plus datatable-nosort text-center" >Messages</th>
                    <th width="5%"  class="table-plus datatable-nosort text-center" >Action</th>                   
                </tr>
            </thead>
            <tbody>

            <?php

                if(!empty($enq_data)){
                    $i=0;
                    foreach($enq_data as $val){
                        $i++;
            ?>

                        <tr class="">
                            <td class="table-plus" ><?php echo $i;?></td>  
                            <td class="table-plus" >
                                <?php echo $val["from_cust"];?>      
                            </td>
                            <td class="table-plus" >
                                <?php echo $val["to_cust"];?>                                              
                            </td>
                            <td class="table-plus text-center" >
                                <?php echo $val["quantity"];?>  <?php echo $val["uom"];?>                  
                            </td>
                            <td class="table-plus">
                            <?php

                                if ($val['date'] != NULL) {
                                    echo date('d-m-Y', strtotime($val['date']));
                                    echo "<br>";
                                    echo date('h:i:s A', strtotime($val['date']));
                                }

                            ?>                                              
                            </td>
                            <td class="table-plus text-center">
                                <a class="btn btn-outline-primary" href="<?php echo base_url('serviceenquiry/details/' . base64_encode($val['id'])); ?>">
                                    <i class="fa fa-comments" aria-hidden="true"></i>     
                                </a>
                            </td>                   
                            <td class="table-plus text-center">
                                <!-- <a class="btn btn-outline-primary serviceenquiryModal" href="#" role="button" data-toggle="modal" data-id="<?php //echo base64_encode($val['id']) ?>">
                                    <i class="fa fa-eye"></i> 
                                </a> -->
                                <button type="button" name="serviceenquiry" id="serviceenquiry" role="button" onclick="openModal('<?php echo base64_encode($val['id']) ?>')" class="btn btn-outline-primary">
                                    <i class="fa fa-eye"></i> 
                                </button>
                            </td>
                        </tr>

            <?php
                    }
                }
              
            ?>

             
            </tbody>
        </table>
        <div class="col-sm-12">
            <div style="float: right;"><?php echo $links; ?></div>
        </div>
    </div>
</div>



<!-- Modal -->

<div class="modal fade admin_modal" id="viewModal">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header text-center">
                <!-- <h4 class="modal-title"> <span id="bussiness"> Bussiness Name </span> </h4> -->
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="row" style="padding-bottom: 20px;">
                    <div class="col-lg-6">
                        <label style="font-weight: 400;">From : </label> <span id="from_cust"> From Customer </span>
                    </div>
                    <div class="col-lg-6">
                        <label style="font-weight: 400;">To : </label> <span id="to_cust"> To Customer </span>
                    </div>
                </div>

                <div class="row" style="padding-bottom: 20px;">
                    <div class="col-lg-6">
                        <label style="font-weight: 400;">Quantity : </label> <span id="quantity"> Quantity </span>
                    </div>
                    <div class="col-lg-6">
                        <label style="font-weight: 400;">On : </label> <span id="date"> Date </span>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="row" style="padding-bottom: 20px;">
                            <div class="col-lg-12">
                                <label style="font-weight: 400;">Message : </label>
                                <br>
                                <div id="message">Message</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


<!-- Modal Ends -->

    
<?php
    if(isset($custom_js)){
        foreach($custom_js as $val){
            ?>
            <script src="<?php echo base_url();?>assets/js/custom_js/<?php echo $val;?>"></script>
            <?php
        }
    }
    
?>
