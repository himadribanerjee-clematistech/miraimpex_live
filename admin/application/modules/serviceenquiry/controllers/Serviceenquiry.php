<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class Serviceenquiry extends CI_Controller {
    protected $token_id='';
    
    public function __construct() {
        parent::__construct();
        $this->token_id=get_cookie('token_id'); 
        if(trim($this->token_id)==''){
            redirect("login");
        }

        $this->load->model("userModel");
        $this->load->model("customerModel");

        $this->load->model("serviceenquiryModel");       
        
        $this->load->helper("pagination");
        $this->load->library("pagination");
        $this->load->model("settingsModel", 'sM');
        $settings_data=$this->sM->getSettings();
        if(isset($settings_data->per_page)){
            $this->per_page=$settings_data->per_page;
        }
    }
    public function index() {
        try {
           $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            
            if(empty($user_details)){
                    $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                    delete_cookie('token_id'); 
                     delete_cookie('useName'); 
                    echo '<script>
                        alert("Unexpected server error.Please try again some time.");
                            window.location="'.base_url().'login";
                    </script>';
            }
            $user_details=current($user_details);
            $data=[]; 
            $data["menu"]="serviceenquiry";
            $data["useName"]=$user_details["Name"];
            $data["custom_js"]=['serviceenquiry.js'];
            set_cookie('useName',$user_details["Name"],'3600');
            
            //search 
            $search_input=$this->input->get();

            $serviceenquiry_search=[];
            
            if(isset($search_input["cust_id"]) && trim($search_input["cust_id"])!=''){
                $serviceenquiry_search["cust_id"]=$search_input["cust_id"];                
            }

            /*if(isset($search_input["from_date"]) && trim($search_input["from_date"])!=''){
                $serviceenquiry_search["from_date"]= date('Y-m-d', strtotime($search_input["from_date"]));                
            }*/

            if(isset($search_input["to_date"]) && trim($search_input["to_date"])!=''){
                $serviceenquiry_search["to_date"] = date('Y-m-d', strtotime($search_input["to_date"])); //2019-10-10             
            }

            $data["serviceenquiry_search"] = $serviceenquiry_search;
            $page=0;
            if($this->input->get("per_page")){
                $page= $this->input->get("per_page");
            }

            $data["page"]=$page;

            $total_row=$this->serviceenquiryModel->getEnqCount($serviceenquiry_search);
         
            $data["links"] = createPageNationLink(base_url('serviceenquiry/index'), $total_row, $this->per_page);

            $start= (int)$page +1;
            $end = ($page+ $this->per_page >=$total_row)? $total_row : (int)$page  + $this->per_page;
            $data['result_count']='';
            if($total_row>0){
                $data['result_count']= "Showing ".$start." - ".$end." of ".$total_row." Results";
            }
            
            $data["enq_data"] = $this->serviceenquiryModel->getEnqList($serviceenquiry_search,$this->per_page, $page); 
            $data["customer"] = $this->customerModel->getCustList('','','');

            //print_r($data); die();
            $data['view_file'] = 'Serviceenquiry';
            view($data);
           
        } catch (Exception $e) {
            $resposne=["status"=>"1","e"=>$e,"message"=>"Unexpected server error.Please try again some time.","field"=>'user_name'];
        }   
    }
    

    public function Quote() {
        try{
            $id = base64_decode($this->input->post('id'));

            $resposne=["status"=>"0","message"=>"","field"=>'', 'data'=> ''];

            $user_details = current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
            if(trim($this->token_id)!=''){
                $res = current($this->serviceenquiryModel->getDetailById($id));

                $resposne=["status"=>"0","message"=>"Success.","field"=>'', 'data'=> $res];
            }
            else {
                $resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
            }

        }
        catch(Exception $e) {
            $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'role_id'];
        }

        echo json_encode($resposne);
    }


    public function details($value) {
        $resposne=["status"=>"0","message"=>"","field"=>'', 'data'=> ''];
        try {
            $id = base64_decode($value);

            $user_details = current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));

            $data["custom_js"]=['serviceenquiry.js'];

            if(trim($this->token_id)!=''){
                $data['result_details'] = current($this->serviceenquiryModel->getDetailById($id));
                $data['response_details'] = $this->serviceenquiryModel->getResponseDetailById($id);

                //print_r($data['response_details']); die();

                $data["menu"]="serviceenquiry";
                $data['view_file'] = 'Details';
                view($data);
            }
            else {
                redirect(base_url(), 'refresh');
            }
        }
        catch (Exception $e) {
            $resposne=["status"=>"1","e"=>$e,"message"=>"Unexpected server error.Please try again some time.","field"=>'user_name'];
        }
    }


    public function Chat() {
        try{
            $serviceenquiry_id = base64_decode($this->input->post('enquiry_id'));
            $to_customer_id = base64_decode($this->input->post('to_customer_id'));
            $from_customer_id = base64_decode($this->input->post('from_customer_id'));

            //echo $serviceenquiry_id . ' , ' . $to_customer_id . ' , ' . $from_customer_id; die(); 5 , 3 , 19

            $resposne=["status"=>"0","message"=>"","field"=>'', 'data'=> ''];

            $user_details = current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
            
            if(trim($this->token_id)!=''){
                $res = $this->serviceenquiryModel->getChatDetail($serviceenquiry_id, $to_customer_id, $from_customer_id);
                
                $resposne=["status"=>"0","message"=>"Success.","field"=>'', 'data'=> $res];
            }
            else {
                $resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
            }

        }
        catch(Exception $e) {
            $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'role_id'];
        }

        echo json_encode($resposne);
    }

}
?>