<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="clearfix mb-10">
        <div class="pull-left">
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Unit of Measurement</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Add New Unit of Measurement</li>
                </ol>
            </nav>
        </div> 
        <div class="pull-right">
            <a class="btn btn-outline-primary" href="<?php echo base_url()?>uom">Back</a>            
        </div> 
    </div> 

    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">

                <form id="adduomfrm" method="post" action="javascript:void(0);" onsubmit="adduomDetails();" autocomplete="off">

                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-6">
                                <label label-for='name'>Unit of Measurement Name <span class="mandatory_label">*</span> ( English )</label>
                                <input type="text" name="name_en" id="name_en" value="" placeholder="Unit of Measurement Name" class="form-control">
                            </div>
                            <div class="col-lg-6">
                                <label label-for='name'>Unit of Measurement Name <span class="mandatory_label">*</span> ( French )</label>
                                <input type="text" name="name_fr" id="name_fr" value="" placeholder="Nom de l'unité de mesure" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-12 text-right">
                                <input type="submit" value="Add" name="submit" class="btn btn-outline-primary">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>




<?php
    if(isset($custom_js)){
        foreach($custom_js as $val){
            ?>
            <script src="<?php echo base_url();?>assets/js/custom_js/<?php echo $val;?>"></script>
            <?php
        }
    }
    
?>