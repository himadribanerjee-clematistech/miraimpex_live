
  <style type="text/css">
      table.data_table_view tbody tr:hover {
    background-color: #def2ff;
}
table.data_table_view thead th {
        padding: 5px 10px;
    }
    table.data_table_view tbody th, table.data_table_view tbody td {
        padding: 12px 10px;
    }
    table.data_table_view tbody tr:nth-child(even) {
    background-color: #f1f9ff;
}
  </style>
<!-- Simple Datatable start -->
<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="clearfix mb-20">
        <div class="pull-left">
            <h5 class="text-blue">List of News</h5>
            
        </div>
        <div class="pull-right">
            <a class="btn btn-outline-primary" href="<?php echo base_url('news/addNews')?>">Add News</a>
            
        </div>
    </div>
 

   
    <div class="row">
        <table class="data_table_view" id="userlst" width="100%">
            <thead>
                <tr>
                    <th width="10%"  class="table-plus datatable-nosort" >#</th>
                    <th width="30%"  class="table-plus datatable-nosort">Title</th>
                    <th width="15%"  class="table-plus datatable-nosort" >Visible On</th>
                    <th width="15%"  class="table-plus datatable-nosort" >Visible To</th>
                    <th width="20%"  class="table-plus datatable-nosort" >Status</th>
                    <th width="20%"  class="table-plus datatable-nosort" >Action</th>
                    
                </tr>
            </thead>
            <tbody>
                
    <?php
        if(!empty($news)){
            $i=0;
            foreach($news as $val){
                $i++;
    ?>
            
            <tr class="">
                <td ><?php echo $i;?></td>
                <td style="display: grid;">
                    <p style="overflow: hidden;
                        text-overflow: ellipsis;
                        white-space: nowrap;
                        width: 93%;
                        padding: 10px 0;
                        margin: 0;"> 
                        <?php echo (isset($val) && isset($val["title"]) ) ? $val['title'] : ''; ?>
                    </p>
                </td>
                <td ><?php echo (isset($val) && isset($val["date"]) ) ? date('d-m-Y', strtotime($val['date'])) : ''; ?></td>
                <td style="display: grid;">

                <?php

                    if (isset($val) && isset($val["visible_to"])) {
                        if ($val["visible_to"] == 1) {
                            echo "All";
                        }
                        else if ($val["visible_to"] == 2) {
                            echo "Login User";
                        }
                        else {
                            echo "Agent";
                        }
                    }

                ?>

                </td>
                <td >
                    <select name="status" class="form-control" id="status" onchange="doStatusChange(this.value, '<?php echo base64_encode($val["news_id"]);?>')">
                        <option value="1" <?php echo (isset($val) && isset($val["active"]) && $val['active'] == 1)?'selected':''; ?>> Active </option>

                        <option value="0" <?php echo (isset($val) && isset($val["active"]) && $val['active'] == 0)?'selected':''; ?>> InActive </option>
                    </select>
                </td>
                
                <td >
                     <div class="dropdown">
                        <a class="btn btn-outline-primary dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                            <i class="fa fa-ellipsis-h"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                         
                            <a class="dropdown-item" href="javascript:void(0);" onclick="editNews('<?php echo base64_encode($val["news_id"]);?>')" ><i class="fa fa-pencil"></i> Edit</a>
                            
                        </div>
                    </div>
                   

                  
                </td>
            </tr>
                        
    <?php
            }
        }
    ?>
              

             
            </tbody>
        </table>
        <div class="col-sm-12">
            <div style="float: right;"><?php echo $links; ?></div>
        </div>
    </div>
</div>

    
<?php
    if(isset($custom_js)){
        foreach($custom_js as $val){
            ?>
            <script src="<?php echo base_url();?>assets/js/custom_js/<?php echo $val;?>"></script>
            <?php
        }
    }
    
?>
  