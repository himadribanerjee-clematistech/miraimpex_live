<link rel="stylesheet" href="<?php echo base_url('assets/vendors/jquery-ui/jquery-ui.min.css'); ?>">
<script src="<?php echo base_url('assets/vendors/jquery-ui/jquery-ui.min.js'); ?>"></script>


<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="clearfix mb-10">
        <div class="pull-left">
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo base_url('news')?>">News</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Update News</li>
                </ol>
            </nav>
        </div>  
        <div class="pull-right">
            <a class="btn btn-outline-primary" href="<?php echo base_url('news')?>">Back</a>            
        </div> 
    </div>

    <div class="container-fluid">

        <form id="updateNewsfrm" method="post" action="javascript:void(0);" >

            <input type="hidden" name="id" id="id" value="<?php echo (isset($newsDetail) && isset($newsDetail["id"]) ) ? $newsDetail["id"]:'';?>">

            <div class="row">
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label label-for="title">Title <span class="mandatory_label">*</span></label>
                                <input type="text" name="title" id="title" placeholder="News Title" value="<?php echo (isset($newsDetail) && isset($newsDetail['title']) )?$newsDetail['title']:'';?>" class="form-control">
                            </div>                            
                        </div>
                    </div>

                    <br>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">                                
                                <label label-for="visible">Visible To <span class="mandatory_label">*</span></label>
                                <br>
                                <input type="radio" name="visible_to" id="visible_to" value="1" <?php echo (isset($newsDetail) && isset($newsDetail['visible_to']) && ( $newsDetail['visible_to'] == 1 ) ) ? 'checked' :''; ?> > All
                                &nbsp;&nbsp;&nbsp;
                                <input type="radio" name="visible_to" id="visible_to" value="2" <?php echo (isset($newsDetail) && isset($newsDetail['visible_to']) && ( $newsDetail['visible_to'] == 2 ) ) ? 'checked' :''; ?> > Logged In User
                                &nbsp;&nbsp;&nbsp;
                                <input type="radio" name="visible_to" id="visible_to" value="3" <?php echo (isset($newsDetail) && isset($newsDetail['visible_to']) && ( $newsDetail['visible_to'] == 3 ) ) ? 'checked' :''; ?> > Agent
                            </div>
                        </div>
                    </div>

                    <br>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label label-for="publish">Publish On <span class="mandatory_label">*</span></label>
                                <input type="text" name="date" id="date" class="form-control" value="<?php echo (isset($newsDetail) && isset($newsDetail['date']) ) ? date('d-m-Y', strtotime($newsDetail['date'])) :''; ?>">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label label-for="details">Details <span class="mandatory_label">*</span></label>
                        <textarea name="description" id="description" placeholder="News Details" class="form-control"><?php echo (isset($newsDetail) && isset($newsDetail['description']) ) ? $newsDetail['description'] :''; ?></textarea>
                    </div>
                </div>
            </div>

            <br>

            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="image">News Image <sup class="mandatory_label">&#8727;</sup> (You Can Add Multiple Images) </label>
                        
                        <div id="testdv">
                            <div id="filediv_0" class="testcls">
                                <input type="file" name="image[]" id="image"/>
                            </div>
                        </div>

                <?php

                    if (isset($newsDetail) && isset($newsDetail["image"]) && ($newsDetail['image'][0]['id']) != '') {
                        for ($i = 0; $i < count($newsDetail["image"]); $i++) {

                ?>

                        <div style='display: inline-block; padding: 14px;'>
                            <div id='<?php echo "abcd_" . $i; ?>' class='abcd'>
                                <img id='<?php echo "previewimg" . $i; ?>' src='<?php echo base_url('uploads/news/' . $newsDetail["image"][$i]['image']); ?>' style='width: 139px; height: 89px;' />
                                <div style='bottom: 0; width: 100%; height: 24px; line-height: 9px; background: #3b485a; opacity: .85;' id="Help">
                                    <a class='remove' id="<?php echo base64_encode($newsDetail["image"][$i]['id']); ?>" href='javascript:void(0);' onclick="" style='display: inline-block; width: 100%; height: 24px; background: url(../../assets/images/uploader-icons.png) no-repeat center 3px; text-align: center; background-position: center -37px;'></a>
                                </div>
                            </div>
                        </div>

                <?php

                        }
                    }


                ?>

                    </div>                    
                </div>
            </div>
            
            <div class="form-group">
                <div class="row">
                    <div class="col-12 text-right">
                        <input class="btn btn-primary" type="submit" value="Submit">
                    </div>
                </div>
            </div>
        </form>

    </div>
    <!-- horizontal Basic Forms End -->

</div>

<?php
    if(isset($vendor_js)){
        foreach($vendor_js as $val){
            ?>
            <script src="<?php echo base_url();?>assets/vendors/ckeditor/<?php echo $val;?>"></script>
            <?php
        }
    }  

    if(isset($custom_js)){
        foreach($custom_js as $val){
            ?>
            <script src="<?php echo base_url();?>assets/js/custom_js/<?php echo $val;?>"></script>
            <?php
        }
    }   
?>

<script type="text/javascript">

    var abc = 0;      // Declaring and defining global increment variable.

    $(document).ready(function() {
        
        // Following function will executes on change event of file input to select different file.
        $('body').on('change', '#image', function() {
            if (this.files && this.files[0]) {
                abc += 1; // Incrementing global variable by 1.
                var z = abc - 1;
                $(this).find('#previewimg' + z).remove();

                $('#testdv').after("<div style='display: inline-block; padding: 14px;'><div id='abcd_" + abc + "' class='abcd'><img id='previewimg" + abc + "' src='' style='width: 139px; height: 89px;' /><div style='bottom: 0; width: 100%; height: 24px; line-height: 9px; background: #3b485a; opacity: .85;'><a class='remove' href='javascript:void(0);' onclick='return false;' style='display: inline-block; width: 100%; height: 24px; background: url(../../assets/images/uploader-icons.png) no-repeat center 3px; text-align: center; background-position: center -37px;'></a></div></div></div>");
            
                var reader = new FileReader();
                reader.onload = imageIsLoaded;
                reader.readAsDataURL(this.files[0]);
                $(this).hide();

                $("#abcd_" + abc).click(function(){
                    $(this).parent().remove();
                    var id=$(this).attr("id");
                    var id_arr=id.split("_");
                    var no =(parseInt(id_arr[1])-1);
                    //console.log(abc,id_arr[1],no);
                    if(id_arr.length==2){
                        $("#filediv_" + no).val("").trigger("change");
                        $("#filediv_" + no).remove();
                    }         
                   
                });

                $("#testdv").prepend(
                    $("<div id='filediv_"+abc+"' class ='testcls' />").fadeIn('slow').prepend($("<input/>", {
                        name: 'image[]',
                        type: 'file',
                        id: 'image'
                    }))
                );
            }

            // To Preview Image
            function imageIsLoaded(e) {
                $('#previewimg' + abc).attr('src', e.target.result);
            };
        });        

        $('.remove').click(function(){
            toastr.clear();
            var base_url=$("#base_url").val();
        
            if(confirm("Do you want to Permanently delete this Image?")){
                $('.loading').removeClass("hidden");

                var id = $(this).attr('id');

                var dlt = $(this).parent().parent().attr('id');
                
                $.ajax({
                    url: base_url + "news/doNewsImageDlt",
                    data: {id: id, dltid: dlt},
                    method:"post",
                    success: function(result){
                        //console.log(result);
                        $('.loading').addClass("hidden");
                        result=JSON.parse(result);
                        if(result.status==0){
                            toastr.success(result["message"]);
                            $('#' + result.dltid).remove();
                             
                        }else{
                            toastr.error(result["message"]);                 
                        }               
                    }
                });
            }
        });


        $("#updateNewsfrm").submit(function(e) {
            e.preventDefault();
            toastr.clear();

            var base_url=$("#base_url").val();
            var description = CKEDITOR.instances.description.getData();
            var formData = new FormData(this);
            var newData = formData.append('description1', description);

            $('.loading').removeClass("hidden");
          
            $.ajax({
            url : base_url+"news/doUpdate",
            type : 'POST',
            data : formData,
            processData: false,
            contentType: false,
            success: function(result) {
                //console.log(result);
                $('.loading').addClass("hidden");
                result=JSON.parse(result);
                if(result.status==0){               
                    if(result["not_inseted"]!=''){
                         var input = $("#image");
                        input.replaceWith(input.val('').clone(true));
                        $("#imgShow").html("");
                        toastr.error(result["not_inseted"]+" image upload failed .Please upload valid image file.");
                    }else{
                        toastr.success(result["message"]);
                        setTimeout(
                          function() 
                          {
                            window.location=base_url+'news';
                        
                          }, 1000);
                    }               
                }else{
                    if(result["not_inseted"]!=''){
                        var input = $("#image");
                        input.replaceWith(input.val('').clone(true));
                        $("#imgShow").html("");
                        toastr.error(result["not_inseted"]+" image upload failed .Please upload valid image file.");
                    }else{
                        toastr.error(result["message"]);
                        if(result["field"]=='to_login'){
                            window.location=base_url;
                        }else{
                            $("#"+result["field"]).focus();
                        }
                    }           
                } 
            },
            error: function(e) {
                console.log(result);  
            }    
        });
        });
    });

</script>