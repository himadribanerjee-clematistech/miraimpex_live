<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class News extends CI_Controller {
	protected $token_id='';
	protected $per_page = 10;
	
    public function __construct() {
    	parent::__construct();
    	$this->token_id=get_cookie('token_id'); 
    	if(trim($this->token_id)==''){
    		redirect("login");
    	}
        $this->load->model("newsModel");
        $this->load->model("userModel");
        $this->load->helper("pagination");
        $this->load->library("pagination");
        $this->load->model("settingsModel", 'sM');
        $settings_data=$this->sM->getSettings();
        if(isset($settings_data->per_page)){
        $this->per_page=$settings_data->per_page;
        }
    }

    public function index()
	{
        try {
           $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            
            if(empty($user_details)){
                    $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                    delete_cookie('token_id'); 
                     delete_cookie('useName'); 
                    echo '<script>
                        alert("Unexpected server error.Please try again some time.");
                            window.location="'.base_url().'login";
                    </script>';
            }
            $user_details=current($user_details);
            $data=[]; 
            $data["menu"]="news";
            $data["useName"]=$user_details["Name"];
            $data["custom_js"]=['news.js'];
            
            set_cookie('useName',$user_details["Name"],'3600');

            $page=0;
            if($this->input->get("per_page")){
                $page = $this->input->get("per_page");
            }

            $total_row=$this->newsModel->getNewsCount();
         
            $data["links"] = createPageNationLink(base_url('news/index'), $total_row,$this->per_page);

            $start= (int)$page +1;
            $end = ($page+ $this->per_page >=$total_row)? $total_row : (int)$page  + $this->per_page;
            $data['result_count']='';
            if($total_row>0){
                $data['result_count']= "Showing ".$start." - ".$end." of ".$total_row." Results";
            }
            
            $data["news"]=$this->newsModel->getNews($this->per_page, $page);
			
            $data['view_file'] = 'News';
            view($data);
           
        } catch (Exception $e) {
            $resposne=["status"=>"1","e"=>$e,"message"=>"Unexpected server error.Please try again some time.","field"=>'user_name'];
        }  
	}

    public function addNews() {
        try{
            $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            if(empty($user_details)){
                    $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                    delete_cookie('token_id'); 
                     delete_cookie('role_id'); 
                    echo '<script>
                        alert("Session expired please login again.");
                            window.location="'.base_url().'login";
                    </script>';
                    exit();
            }
            $user_details=current($user_details);
            $data=[]; 
            $data["menu"]="news";
            $data["useName"]=$user_details["Name"];
            $data["vendor_js"]=['ckeditor.js'];
            $data["custom_js"]=['news.js'];
            
            set_cookie('useName',$user_details["Name"],'3600');
            
            $data['view_file'] = 'AddNews';
            view($data);
        } catch (Exception $e) {
            echo '<script>
                alert("Unexpected server error.Please try again some time.");
                            window.location="'.base_url().'news";
            </script>';

        }
    }

    public function doAdd() {    
        try{
            $data = $this->input->post();
            //print_r($data); die();
            $resposne=["status"=>"0","message"=>"","field"=>''];
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));

            if(trim($this->token_id)!=''){  
                if(isset($data["title"]) && trim($data["title"])==''){  
                    $resposne=["status"=>"1","message"=>"Please Insert Title.","field"=>'title','not_inseted'=>''];
                }
                else if(isset($data["visible_to"]) && trim($data["visible_to"])==''){  
                    $resposne=["status"=>"1","message"=>"Please Select Visible To.","field"=>'visible_to','not_inseted'=>''];
                }
                else if(isset($data["date"]) && trim($data["date"])==''){
                    $resposne=["status"=>"1","message"=>"Please Select Date.","field"=>'date','not_inseted'=>''];
                }
                /*else if(isset($data["description"]) && trim($data["description"])==''){
                    $resposne=["status"=>"1","message"=>"Please Insert Description.","field"=>'description','not_inseted'=>''];
                }*/
                else if(isset($data["description1"]) && trim($data["description1"])==''){
                    $resposne=["status"=>"1","message"=>"Please Insert Description.","field"=>'description','not_inseted'=>''];
                }
                else if(empty($_FILES["image"]) && !isset($_FILES["image"]["name"])){
                    $resposne=["status"=>"1","message"=>"Please Select Image.","field"=>'image','not_inseted'=>''];
                }
                else{
                    $news_data = [
                        'title' => $data["title"],
                        'visible_to' => $data["visible_to"],
                        'date' => date('Y-m-d', strtotime($data["date"])) . ' 10:00:00',
                        'description' => $data["description1"]
                    ];

                    $res = $this->newsModel->insertData($news_data);

                    if ($res) {
                        $last_id = $res;

                        $this->load->library('upload');
                        $dataInfo = array();
                        $files = $_FILES['image']['name'];

                        $cpt = count($files);
                        $not_inseted=[];
                        $insert=0;
                        for($i=0; $i<$cpt; $i++) {  
                            if (trim($files[$i]) =='') {
                                continue;
                            }
                            $fileName = time().'-'.$files[$i];
                            $allowedExts = array("gif", "jpeg", "jpg", "png");
                            $image_data=explode(".", $files[$i]);
                           
                            $extension = end($image_data);

                            if ((($_FILES["image"]["type"][$i] == "image/gif")  || ($_FILES["image"]["type"][$i] == "image/jpeg")  || ($_FILES["image"]["type"][$i] == "image/jpg")  || ($_FILES["image"]["type"][$i] == "image/png")) && in_array($extension, $allowedExts)){
                                 

                                // File upload configuration
                                $uploadPath = './uploads/news/';

                                $config = [];
                                $config['upload_path']      = $uploadPath;
                                $config['allowed_types']    = 'gif|jpg|jpeg|png'; 
                                //$config['min_height']       = 296;
                                //$config['min_width']        = 804;
                                $config['overwrite']        = FALSE;
                                $config['file_name']        = $fileName;  

                               // $this->upload->initialize($config);
                             
                                if ( ! move_uploaded_file($_FILES['image']['tmp_name'][$i],  $uploadPath."/".$fileName)){
                                    $resposne=["status"=>"1","message"=>"Please Select jpg/jpeg/png/jpeg Image.",'error'=>'',"field"=>'image'];
                                   
                                }else{

                                    $imgData = [
                                        'news_id'   =>  $last_id,
                                        'image'     =>  $fileName,
                                    ];
                                    
                                    $insert = $this->newsModel->insertImage($imgData);
                                }   

                            }else{
                                $not_inseted[]= $_FILES['image']['name'];
                            }
                        }
                    }                   


                    if($insert>0){
                        $resposne=["status"=>"0","message"=>"News added successfully.","field"=>'image','not_inseted'=>implode(",", $not_inseted)];
                    }else{
                        $resposne=["status"=>"1","message"=>"News addition failed. Please try again some time.","field"=>'image','not_inseted'=>implode(",", $not_inseted)];
                    }
                }
            }
        }
        catch(Exception $e) {
            $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'','not_inseted'=>''];
        }

        echo json_encode($resposne);
    }

    public function doEdit($id) {
        try{
            $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            
            if(empty($user_details)){
                    $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                    delete_cookie('token_id'); 
                     delete_cookie('role_id'); 
                    echo '<script>
                        alert("Session expired please login again.");
                            window.location="'.base_url().'login";
                    </script>';
                    exit();
            }
            $user_details=current($user_details);
            $id=base64_decode($id);
            if(trim($id)==''){
                echo '<script>
                        alert("Invalid user.");
                            window.location="'.base_url().'news";
                    </script>';
                    exit();
            }
            $data=[]; 
            $data["menu"]="news";
            $data["useName"]=$user_details["Name"];
            $data["custom_js"]=['news.js'];
            $data["vendor_js"]=['ckeditor.js'];
            set_cookie('useName',$user_details["Name"],'3600');

            $data['newsDetail'] = current($this->newsModel->getById($id));

            //print_r($data['newsDetail']); die();
            
            $data['view_file'] = 'EditNews';
            view($data);
        }
        catch(Exception $e) {
            echo '<script>
                    alert("Check Again.");
                    window.location="'.base_url().'news";
                </script>';
        }
    }

    public function doNewsImageDlt() {
        $data=$this->input->post();
        $dltid = $data['dltid'];
        $id=base64_decode($data["id"]);
        //echo $dltid; 
        //echo $id; die();
        $resposne=["status"=>"0","message"=>"","field"=>'', 'dltid'=>''];
        try {
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
            if(trim($this->token_id)!=''){

                $img = current($this->newsModel->getImgById($id));

                $res = $this->newsModel->dltNewsImg($id);
              
                if($res){

                    if(file_exists('./uploads/news/' . $img["image"])) {
                        unlink('./uploads/news/' . $img["image"]);
                    }

                    $resposne=["status"=>"0","message"=>"Image Deleted successfully.", "dltid" => $dltid ];
                }
                else{                                
                    $resposne=["status"=>"1","message"=>"Image Deletion failed.Please try again after some time."];                                
                }
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
            }   
        }catch (Exception $e) {
                $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'status'];

        }

        echo json_encode($resposne);
    }


    public function doUpdate() {    
        try{
            $data = $this->input->post();
            //print_r($data); die();
            $resposne=["status"=>"0","message"=>"","field"=>''];
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));

            if(trim($this->token_id)!=''){  
                if(isset($data["id"]) && trim($data["id"])==''){
                     $resposne=["status"=>"1","message"=>"Invalid Request.","field"=>'','not_inseted'=>''];
                }
                else{

                    $id=base64_decode($data["id"]);
                    $news_details=current($this->newsModel->getById($id));

                    if(empty($news_details)){
                        $resposne=["status"=>"1","message"=>"Invalid Request.","field"=>'','not_inseted'=>''];
                    }
                    else{

                        //print_r($news_details); die();

                        if(isset($data["title"]) && trim($data["title"])==''){  
                            $resposne=["status"=>"1","message"=>"Please Insert Title.","field"=>'title','not_inseted'=>''];
                        }
                        else if(isset($data["visible_to"]) && trim($data["visible_to"])==''){  
                            $resposne=["status"=>"1","message"=>"Please Select Visible To.","field"=>'visible_to','not_inseted'=>''];
                        }
                        else if(isset($data["date"]) && trim($data["date"])==''){
                            $resposne=["status"=>"1","message"=>"Please Select Date.","field"=>'date','not_inseted'=>''];
                        }
                        else if(isset($data["description1"]) && trim($data["description1"])==''){
                            $resposne=["status"=>"1","message"=>"Please Insert Description.","field"=>'description','not_inseted'=>''];
                        }
                        else if(empty($_FILES["image"]) && !isset($_FILES["image"]["name"]) && trim($news_details["image"])==''){
                            $resposne=["status"=>"1","message"=>"Please Select Image.","field"=>'image','not_inseted'=>''];
                        }
                        else{
                            
                            $news_data = [
                                'title' => $data["title"],
                                'visible_to' => $data["visible_to"],
                                'date' => date('Y-m-d', strtotime($data["date"])) . ' 10:00:00',
                                'description' => $data["description1"]
                            ];

                            //print_r($news_data); die();

                            $res = $this->newsModel->updateData($news_data, $id);

                            if ($res) {
                                if(!empty($_FILES["business_logo"]['name'][1])){
                                    $this->load->library('upload');
                                    $dataInfo = array();
                                    $files = $_FILES['image']['name'];

                                    //print_r($files[0]); die();

                                    $cpt = count($files);
                                    //echo $cpt; die();
                                    $not_inseted=[];
                                    $insert=0;

                                    for($i=0; $i<$cpt; $i++) {  
                                        if (trim($files[$i]) =='') {
                                            continue;
                                        }
                                        $fileName = time().'-'.$files[$i];

                                        $allowedExts = array("gif", "jpeg", "jpg", "png");
                                        $image_data=explode(".", $files[$i]);
                                       
                                        $extension = end($image_data);

                                        if ((($_FILES["image"]["type"][$i] == "image/gif")  || ($_FILES["image"]["type"][$i] == "image/jpeg")  || ($_FILES["image"]["type"][$i] == "image/jpg")  || ($_FILES["image"]["type"][$i] == "image/png")) && in_array($extension, $allowedExts)){
                                     

                                            // File upload configuration
                                            $uploadPath = './uploads/news/';

                                            $config = [];
                                            $config['upload_path']      = $uploadPath;
                                            $config['allowed_types']    = 'gif|jpg|jpeg|png'; 
                                            //$config['min_height']       = 296;
                                            //$config['min_width']        = 804;
                                            $config['overwrite']        = FALSE;
                                            $config['file_name']        = $fileName;  

                                            // $this->upload->initialize($config);
                                         
                                            if ( ! move_uploaded_file($_FILES['image']['tmp_name'][$i],  $uploadPath.$fileName)){
                                                $resposne=["status"=>"1","message"=>"Please Select jpg/jpeg/png/jpeg Image.",'error'=>'',"field"=>'image'];
                                               
                                            }else{

                                                $imgData = [
                                                    'news_id'   =>  $id,
                                                    'image'     =>  $fileName,
                                                ];

                                                $insert = $this->newsModel->insertImage($imgData);
                                            }   

                                        }else{
                                            $not_inseted[]= $_FILES['image']['name'];
                                        }
                                    }
                                
                                    if($insert){
                                        $resposne=["status"=>"0","message"=>"News Updated successfully.","field"=>'image','not_inseted'=>implode(",", $not_inseted)];
                                    }else{
                                        $resposne=["status"=>"1","message"=>"News updation failed. Please try again some time.","field"=>'image','not_inseted'=>implode(",", $not_inseted)];
                                    }
                                }
                                else {
                                    $resposne=["status"=>"0","message"=>"News Updated successfully.","field"=>'',"not_inseted"=>""];
                                }
                            } 
                            else {
                                $resposne=["status"=>"1","message"=>"News Updation failed. Please try again some time.","field"=>'image','not_inseted'=>implode(",", $not_inseted)];
                            }                   
                        }
                    }
                }
            }
        }
        catch(Exception $e) {
            $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'','not_inseted'=>''];
        }

        echo json_encode($resposne);
    }


    public function doStatusChange(){
        $data=$this->input->post();
        //print_r($data);
        $id=base64_decode($data["id"]);
        $resposne=["status"=>"0","message"=>"","field"=>''];
        try {
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
            if(trim($this->token_id)!=''){
                $res = $this->newsModel->update(["active"=>$data["status"]],$id);
              
                if($res){
                    $resposne=["status"=>"0","message"=>"Banner status changed successfully."];
                }
                else{                                
                    $resposne=["status"=>"1","message"=>"Banner change failed.Please try again after some time."];                                
                }
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
            }   
        }catch (Exception $e) {
                $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'status'];

        }

        echo json_encode($resposne);
    }
}

?>