<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class NewsModel extends CI_Model {
	protected $newsTable = 'news';
	protected $newsImageTable = 'news_image';

	public function __construct() {
        parent::__construct(); 
    }

    public function getNewsCount(){
    	try {
    		$this->db->db_debug = false;
    		$this->db->select("news_id, title, date, visible_to, active");
    		$this->db->from($this->newsTable);
    		
    		//$this->db->get();
	    	
	    	$db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query();die();
	        	return $this->db->count_all_results();

	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
    }

    public function getNews($limit, $start){
    	try {
    		$this->db->db_debug = false;
    		$this->db->select("news_id, title, date, visible_to, active");
    		$this->db->from($this->newsTable);

    		$this->db->order_by('date', 'DESC'); 
	    	$this->db->limit($limit, $start);
    		
    		$query=$this->db->get();

	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query();
	        	$result=[];
	        	foreach($query->result_array() as $row){
	        		$result[]=$row;
	        	}
	        	return $result;
	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
    }

    public function insertData($data){
    	//print_r($data);
    	try{
    		$this->db->db_debug = false;

    		$insert = $this->db->insert($this->newsTable, $data);

    		$db_error = $this->db->error();
    		if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{

	        	//return $insert?true:false;
	        	return $this->db->insert_id();
	        }
    	}
    	catch(Exception $e) {
    		throw new Exception("Error Processing Request", 1);    		
    	}
    }

    public function insertImage($data){
    	//print_r($data);
    	try{
    		$this->db->db_debug = false;

    		$insert = $this->db->insert($this->newsImageTable, $data);

    		//echo $this->db->last_query(); die();

    		$db_error = $this->db->error();
    		if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{

	        	return $insert?true:false;
	        }
    	}
    	catch(Exception $e) {
    		throw new Exception("Error Processing Request", 1);    		
    	}
    }

    public function getById($id){
    	try {
		 	$this->db->db_debug = false;

		 	// SELECT n.news_id, n.title, n.description, n.date, n.visible_to, ni.id, ni.image FROM t_news_image ni LEFT JOIN t_news n ON ni.news_id = n.news_id WHERE n.news_id = 2

		 	$this->db->select("n.news_id, n.title, n.description, n.date, n.visible_to, ni.id, ni.image");
    		$this->db->from($this->newsImageTable." ni");
    		$this->db->join($this->newsTable . ' n' , 'ON ni.news_id = n.news_id', 'LEFT');
    		$this->db->where("n.news_id", $id);	   

	    	$query=$this->db->get();

		 	$db_error = $this->db->error();

	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	$data=[];
	        	foreach($query->result_array() as $row){
	        		$rcat = $row["news_id"]."_ni";

	        		if(!isset($data[$rcat])){
	        			$data[$rcat]["id"]=base64_encode($row["news_id"]);
	        			$data[$rcat]["title"]=$row["title"];
	        			$data[$rcat]["description"]=$row["description"];
	        			$data[$rcat]["date"]=$row["date"];
	        			$data[$rcat]["visible_to"]=$row["visible_to"];
	        			$data[$rcat]["image"]=[];	        			
	        		}
	        		$data[$rcat]["image"][] = ["id" => $row['id'], "image" => $row["image"]]; 	
	        	}

	        	return array_values($data);
	        }
	        return TRUE;
	    } catch (Exception $e) {
	       throw new Exception();
	    }	
    }

    public function getImgById($where) {
    	try {
    	 	$this->db->db_debug = False;

    	 	$this->db->select("image");
    		$this->db->from($this->newsImageTable);
    		$this->db->where("id", $where);
	    	
	    	$query=$this->db->get();

	    	$db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	$result=[];
	        	foreach($query->result_array() as $row){
	        		$result[]=$row;
	        	}
	        	return $result;
	        }
	        
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }
    }

    public function dltNewsImg($where) {
    	try {
    	 	$this->db->db_debug = False;
    	 	$res = $this->db->delete($this->newsImageTable, ['id' => $where]);

    	 	$db_error = $this->db->error();

	        if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }
	        else {
	        	if ($res) {
	        		return True;
	        	}
	        	else {
	        		return False;
	        	}
	        }
	        
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }
    }

    public function updateData($data, $where=''){
    	try {
    	 	$this->db->db_debug = False;
    	 	$res = $this->db->update($this->newsTable, $data, ['news_id' => $where]);

    	 	//echo $this->db->last_query(); die();

    	 	$db_error = $this->db->error();
	        if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }
	        else {
	        	if ($res) {
	        		return True;
	        	}
	        	else {
	        		return False;
	        	}
	        }
	        
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }
    }
}

?>