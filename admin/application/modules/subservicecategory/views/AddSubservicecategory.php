<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="clearfix mb-10">
        <div class="pull-left">
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Sub Service Category</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Add New Sub Service Category of <?php echo $cate_name; ?></li>
                </ol>
            </nav>
        </div> 
        <div class="pull-right">
            <a class="btn btn-outline-primary" href="<?php echo base_url(); ?>subservicecategory/index/<?php echo base64_encode($cid);?>">Back</a>            
        </div>
    </div> 


    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">

                <form id="addsubservicecatfrm" method="post" action="javascript:void(0);" onsubmit="addsubservicecat();" autocomplete="off">
                    <input type="hidden" name="cid" id="cid" value="<?php echo base64_encode($cid);?>">
                    <input type="hidden" name="id_back" id="id_back" value="<?php echo base64_encode($cid);?>">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-6">
                                <label label-for='name'>Sub Service Category Name <span class="mandatory_label">*</span></label>
                                <input type="text" name="sub_cat_name" id="sub_cat_name" value="<?php echo (isset($subcat_detail) && isset($subcat_detail["service_category_name"]) )?$subcat_detail["service_category_name"]:'';?>" placeholder="Sub Service Category Name" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                            <div class="col-lg-12 text-right">
                                <input type="submit" value="Add" name="submit" class="btn btn-outline-primary">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>



<?php
    if(isset($custom_js)){
        foreach($custom_js as $val){
            ?>
            <script src="<?php echo base_url();?>assets/js/custom_js/<?php echo $val;?>"></script>
            <?php
        }
    }
    
?>