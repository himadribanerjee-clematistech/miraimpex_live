<div class="pd-20 bg-white border-radius-4 box-shadow mb-30"> 
    <div class="clearfix mb-10">
        <div class="pull-left">
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Service Category</a></li>
                    <li class="breadcrumb-item active" aria-current="page">List of <?php echo $cate_name; ?></li>
                </ol>
            </nav>
        </div>
        <div class="pull-right">
            <a class="btn btn-outline-primary" href="<?php echo base_url()?>subservicecategory/addNew/<?php echo base64_encode($cid);?>">Add New</a>            
            <a class="btn btn-outline-primary" href="<?php echo base_url('servicecategory')?>">Back</a>            
        </div>
    </div>

    <div class="container-fluid bg-light"> 
        <div class="row">
            <div class="col-lg-12">

                <form id="servicecatsrch" action="<?php echo base_url()?>subservicecategory/index/<?php echo $id;?>">
                    <input type="hidden" name="per_page_search" value="<?php echo $page; ?>">

                    <div class="row align-items-center justify-content-center">
                        <div class="col-md-4 pt-3">
                            <div class="form-group ">
                                <input type="text" name="subcatName" id="subcat_name" value="<?php echo (isset($subcat_search["subcat_name"]))?$subcat_search["subcat_name"]:'';?>" placeholder="Sub Category Name" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-4 pt-3">
                            <div class="form-group">
                                <select name="status" class="form-control">
                                    <option value="">Select Status</option>
                                     <option value="1" <?php echo (isset($subcat_search) && isset($subcat_search["active"]) &&  $subcat_search["active"]==1)?'selected':'';?>>Active</option>
                                     <option value="0"  <?php echo (isset($subcat_search) && isset($subcat_search["active"]) &&  $subcat_search["active"]==0)?'selected':'';?>>InActive</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <a class="btn btn-outline-primary" href="<?php echo base_url(); ?>subservicecategory/index/<?php echo $id;?>"><i class="fa fa-refresh" aria-hidden="true"></i> Reset</a> 
                                
                            <input type="submit" name="search" value="Search" class="btn btn-outline-primary">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="row pt-2">
        
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="data-table stripe hover no-wrap" style="font-size: 14px;">
            <thead>
                <tr class="titel_bar">
                    <th class="table-plus datatable-nosort" width="10%">Sl No.</th>
                    <th class="table-plus datatable-nosort" width="30%">Sub Category Name</th>
                    <th class="table-plus datatable-nosort" width="10%">Status</th>
                    <th class="table-plus datatable-nosort" width="10%" >Action</th>
                </tr>
            </thead>
            <tbody>

            <?php
                if(!empty($subcat_data)){
                    $i=0;
                    foreach($subcat_data as $val){
                        $i++;
            ?>
                        
                        <tr class="">
                            <td class="table-plus"><?php echo $i;?></td>  
                            <td class="table-plus"><?php echo $val["service_category_name"];?></td>   
                            <td class="table-plus">
                                <select  name="status" onchange="doSubSrvcCatStatusChange(this.value,'<?php echo base64_encode($val["id"]);?>', '<?php echo base64_encode($cid);?>');" class="form-control">
                                    <option value="1" <?php echo ($val["active"]==1)?'selected':'';?>>Active</option>
                                     <option value="0"  <?php echo ($val["active"]==0)?'selected':'';?>>InActive</option>
                                </select> 
                            </td>
                            <td class="table-plus" style="text-align:center;">

                                <div class="dropdown">
                                    <a class="btn btn-outline-primary dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                        <i class="fa fa-ellipsis-h"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item" href="<?php echo base_url()?>subservicecategory/dosubSrvcCatEdit/<?php echo base64_encode($val["id"]);?>/<?php echo base64_encode($cid);?>" style=" font-size: 15px;" >
                                            <i class="fa fa-edit" aria-hidden="true"></i> 
                                            Edit
                                        </a>
                                    </div>
                                </div>

                            </td>
                        </tr>
                        
            <?php
                    }
                }else{
                    ?>
                     <tr class="">
                         <td colspan="7">No Record Found</td>   

                     </tr>  
                    <?php
                }
            ?>
           
                 
            </tbody>
    </table>
    <div class="col-sm-12">
            <div style="float: right;"><?php echo $links; ?></div>
        </div>
</div>
<?php
    if(isset($custom_js)){
        foreach($custom_js as $val){
            ?>
            <script src="<?php echo base_url();?>assets/js/custom_js/<?php echo $val;?>"></script>
            <?php
        }
    }
    
?>
  