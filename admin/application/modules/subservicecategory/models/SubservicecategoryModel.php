<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class SubservicecategoryModel extends CI_Model {
	protected $servicecatTable = 'service_category';
	
	public function __construct() {
        parent::__construct();
    }
    
    public function getsubSrvcCatCount($data, $id){
    	//print_r($data); die();
    	try {
    		$this->db->db_debug = false;
    		$this->db->select("u.id,u.service_category_name,u.active");
    		$this->db->from($this->servicecatTable." u");
    		if(isset($data["subcat_name"]) && $data["subcat_name"]!=''){ 
	    		$this->db->like("u.service_category_name",$data["subcat_name"]);
	    	}
	    	if(isset($data["active"]) && $data["active"]!=''){
	    		$this->db->where("u.active",$data["active"]);
	    	}
	    	$this->db->where('parent_id', $id);

	    	$db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query();die();
	        	return $this->db->count_all_results();

	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
    }

    public function getSrvcCatName($id){
    	try {
    		$this->db->db_debug = false;
    		$this->db->select("service_category_name");
    		$this->db->from($this->servicecatTable);
    		
	    	$this->db->where('id', $id);
	    	
 			$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query();die();
	        	foreach ($query->row() as $t) {
				    return $t;
				}
				
	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
	}

    public function getsubSrvcCatList($data,$limit='', $start='', $id){
    	try {
    		$this->db->db_debug = false;
    		$this->db->select("u.id,u.service_category_name,u.active");
    		$this->db->from($this->servicecatTable." u");
    		
	    	if(isset($data["subcat_name"]) && $data["subcat_name"]!=''){
	    		$this->db->like("u.service_category_name",$data["subcat_name"]);
	    	}
	    	if(isset($data["active"]) && $data["active"]!=''){
	    		$this->db->where("u.active",$data["active"]);
	    	}
	    	$this->db->where('parent_id', $id);
	    	$this->db->order_by('u.service_category_name', 'DESC'); 
	    	if(trim($limit)!='' && trim($start)!=''){
	    		$this->db->limit($limit, $start);
	    	}
	    	
 
 			$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query();die();
	        	$result=[];
	        	foreach($query->result_array() as $row){
	        		$result[]=$row;
	        	}
	        	return $result;
	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
    }	
   	
}

?>