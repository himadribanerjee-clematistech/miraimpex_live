<div class="page-header">
        <div class="row">
            <div class="col-md-9 col-sm-12">
                <nav aria-label="breadcrumb" role="navigation">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url()?>category">Category</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Main Category</li>
                    </ol>
                </nav>
            </div>

            <div class="col-md-3 col-sm-12">
            <div class="pull-right">
            <a class="btn btn-outline-primary" href="<?php echo base_url()?>category/add/<?php echo base64_encode($parent_id); ?>">Add Category</a>
            
        </div>
            </div>

        </div>
    </div>
  
<!-- Simple Datatable start -->
<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">    
  
    <?php
        if(is_array($cat_data) && count($cat_data)>0)
        {
    ?>
    <div class="container-fluid bg-light ">
    
    <form id="usersrch" action="<?php echo base_url()?>category">

        <input type="hidden" name="per_page_search" value="<?php echo $page; ?>">

        <div class="row align-items-center justify-content-center">
            <div class="col-md-3 pt-3">
               <div class="form-group ">
                  <input type="text" class="form-control" name="cat_name" id="cat_name" value="<?php echo (isset($cat_search["cat_name"]))?$cat_search["cat_name"]:'';?>" placeholder="Category Name">
               </div>
            </div>


            <div class="col-md-3 pt-3">
                <div class="form-group ">
                    <select id="is_approved" name="is_approved" class="form-control">
                        <option value="">-- Select Approval --</option>
                        <option value="1" <?php echo (isset($cat_search) && isset($cat_search["is_approved"]) &&  $cat_search["is_approved"]== 1) ? 'selected' : ''; ?> > Approved </option>
                        <option value="0" <?php echo (isset($cat_search) && isset($cat_search["is_approved"]) &&  $cat_search["is_approved"]== 0) ? 'selected' : ''; ?> > Not Approved </option>
                    </select>  
                </div>
            </div>

            <div class="col-md-3 pt-3">
               <div class="form-group">
                    <select id="active" name="status" class="form-control">
                        <option value="">-- Select Status --</option>
                        <option value="1" <?php echo (isset($cat_search) && isset($cat_search["active"]) &&  $cat_search["active"]==1)?'selected':'';?>>Active</option>
                        <option value="0"  <?php echo (isset($cat_search) && isset($cat_search["active"]) &&  $cat_search["active"]==0)?'selected':'';?>>InActive</option>
                    </select>
               </div>
            </div>

            <div class="col-md-3 pt-3">
               <div class="form-group">
               <div class="col-12 text-right">
                <div class="form-group">
                    <a class="" href="<?php echo base_url()?>category"><button type="button" class="btn btn-outline-primary "><i class="fa fa-refresh" aria-hidden="true"></i> Reset</button></a> 

                    <input type="submit" name="search" value="Search" class="btn btn-outline-primary">                                
                </div>                       
            </div>
               </div>
            </div>
        </div>
   
    </form>

</div>
    <?php
        }

    ?>          

    

    <div class="row align-items-center justify-content-center">        
        
            <?php
            
              if(is_array($cat_data) && count($cat_data)>0)
              {
                foreach($cat_data as $m=>$n)
                {
            ?>
                     <div class="col-md-12 bg-light" style="margin:5px;">
                <h2><?php echo $m; ?> :</h2>
            </div>
            <?php        
                    foreach($n as $k=>$v)
                {
            ?>
            <div class="col-md-2 pt-3 bg-light" style="margin:5px;">
                    <div class="form-group ">
                    <a href="<?php echo base_url()?>category/view/<?php echo base64_encode($v['id']); ?>" title="View subcategories of <?php echo $v['category_name_en'] ?>">
            <figure><img id="seller_item_image" style="height: 100px;" src="<?php echo (isset($v) && ($v["image"] != '') )?base_url('uploads/category/' . $v["image"]):base_url('assets/images/no-item-image.png'); ?>" alt=""></figure></a>
            <div class="icon myitem_info">
            <a href="<?php echo base_url()?>category/view/<?php echo base64_encode($v['id']); ?>" title="View subcategories of <?php echo $v['category_name_en'] ?>">
                <h5><?php echo $v['category_name_en'] ?></h5>
                </a>    
              <div class="vall"><a href="<?php echo base_url()?>category/edit/<?php echo base64_encode($v['id']); ?>" style="color:#00ff00;">Edit</a>  | <a href="javascript:void(0);" rel="<?php echo base64_encode($v['id']); ?>" onclick="javascript:deleteCategory(this);" style="color:#ff0000;">Delete</a></div>  
            </div>

        </div>
    </div>
            <?php
                }
                }
              }else{
                ?>
                <div class="col-sm-12 bg-light">
                    <h5>No Sub category found.</h5>
                </div>
                <?php
              }
            ?>
        
    </div>
</div>

    
<?php
    if(isset($custom_js)){
        foreach($custom_js as $val){
            ?>
            <script src="<?php echo base_url();?>assets/js/custom_js/<?php echo $val;?>"></script>
            <?php
        }
    }
    
?>

<script type="text/javascript">
function deleteCategory(obj)
{
    var Id = $(obj).attr('rel');
    var result = confirm("This category will be deleted permanently. Want to delete?");
    if (result) 
    {
        var base_url=$("#base_url").val();
        window.location=base_url+'category/delete/'+$(obj).attr('rel');
    }
}
</script>
  