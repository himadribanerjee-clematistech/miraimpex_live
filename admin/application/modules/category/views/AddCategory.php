<div class="min-height-200px">
    <div class="page-header">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="title">
                    
                </div>
                <nav aria-label="breadcrumb" role="navigation">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url()?>category">Add: Category</a></li>
                        <?php
                          if(is_array($mappingArr) && count($mappingArr)>0)
                          {
                              foreach($mappingArr as $k=>$v)
                              {
                        ?>
                                 <li class="breadcrumb-item active" aria-current="page"><a href="<?php echo base_url()?>category/view/<?php echo base64_encode($v['id']); ?>"><?php echo $v['category_name_en']; ?></a></li>   
                        <?php          
                              }
                          }
                        ?>
                    </ol>
                </nav>
            </div>

        </div>
    </div>

    <?php
      if(is_array($error) && count($error)>0)
      {
          if($error['error'] == 1)
          {
        ?>
            <div class="col-md-12 bg-light" style="margin:5px;">
                <p style="color:#ff0000;"><strong>ERROR: </strong><?php echo $error['message']; ?></p>
            </div>
        <?php      
          }else{
            ?>
<div class="col-md-12 bg-light" style="margin:5px;">
<p style="color:#00ff00;"><strong>SUCCESS: </strong><?php echo $error['message']; ?></p>
</div>
            <?php
        }
      }
    ?>
    <!-- horizontal Basic Forms Start -->
    <div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
   


        <form id="uploadForm" name="uploadForm" method="post" action="" autocomplete="off" enctype="multipart/form-data">
          <input type="hidden" name="process" value="ok">
            <div class="form-group">
                <div class="row mb-30">
                    
                    <div class="col-6">
                        <label for="cat_name_en"> Category Name (En)<sup class="mandatory_label">&#8727;</sup></label>
                        <input type="text" name="cat_name_en" class="form-control" id="cat_name_en" value="" placeholder=" Category Name(English)">
                    </div>

                    <div class="col-6">
                        <label for="cat_name_fr"> Category Name (Fr)<sup class="mandatory_label">&#8727;</sup></label>
                        <input type="text" name="cat_name_fr" class="form-control" id="cat_name_fr" value="" placeholder=" Category Name(French)">
                    </div>
                    </div>

                    <div class="row mb-30">
                    <div class="col-6">
                        <label for="cat_type"> Category Type<sup class="mandatory_label">&#8727;</sup></label>
                        <select name="cat_type" class="form-control" id="cat_type">
                            <option value="1" selected>Item</option>
                            <option value="2">Service</option>
                        </select>
                    </div>

                    <div class="col-6">
                        <label for="cat_name"> Unit of Measurement</label>
                        <select name="uom_id[]" class="form-control" id="uom_id" multiple="multiple">
                            <option></option>
                        </select>
                    </div>
                    </div>
                    
                    <div class="row mb-30">
                    <div class="col-6">

<label for="image">Category Image <?php if($parent_id == 0){ ?> <sup class="mandatory_label">&#8727;</sup> <?php } ?></label>
                        <input type="file" name="image" id="image">

                            <br><br>

                            <img src="<?php echo base_url('assets/images/no-item-image.png'); ?>" height="150" width="150"  style="border: 2px dotted #ccc; border-radius: 2px;" id="chImg"/>
                    </div>

                    <div class="col-6">
                        <label for="cat_name"> Available for local suppliers ?</label>
                        <select name="displayLocal" class="form-control" id="displayLocal">
                        <option value="1">Yes</option>
                        <option value="0">No</option>
                        </select>
                    </div>
                    </div>
                
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-lg-12 text-right">
                        <input class="btn btn-primary" type="submit" value="Submit">
                    </div>
                </div>
            </div>

        </form>
    </div>
    <!-- horizontal Basic Forms End -->

</div>



<div class="clone">
    <!-- Value for UOM -->
    <option id="uom_cln_id" value=""></option>

</div>



<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>

<script type="text/javascript">
    $("#uom_id").select2();
</script>


  
<script type="text/javascript">

function filePreview(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#uploadForm + img').remove();
            $('#chImg').attr('src',e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

function returnValidation(){
    toastr.clear();
      var submit = true;

    if($.trim($('#cat_name_en').val()) == '')
    {
        toastr.error('Enter category name!!!');
        submit = false;
        return false;
    }

    <?php
      if($parent_id == 0){
    ?>
    if($('#image')[0].files.length === 0){
        toastr.error('category image is required!');
        submit = false;
        return false;
    }
    <?php
    } 
    ?>
        return submit;
}
    
$(document).ready(function(){
    var base_url = $('#base_url').val();

    $("#image").change(function () {
        filePreview(this);
    });


    $('#uploadForm').submit(function(){
        toastr.clear();
      
    if($.trim($('#cat_name_en').val()) == '')
    {
        toastr.error('Enter category name!!!');        
        return false;
    }

    if($.trim($('#cat_name_fr').val()) == '')
    {
        toastr.error('Enter category name!!!');        
        return false;
    }

    <?php
      if($parent_id == 0){
    ?>
    if($('#image')[0].files.length === 0){
        toastr.error('category image is required!');        
        return false;
    }
    <?php
    } 
    ?>
        return true;
    });
        
  

    });


</script>