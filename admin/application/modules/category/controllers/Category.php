<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class Category extends CI_Controller {
    protected $token_id='';
    protected $per_page=10;

    public function __construct() {
        parent::__construct();
        ERROR_REPORTING('E_ALL^E_NOTICE');
        $this->token_id=get_cookie('token_id'); 
        if(trim($this->token_id)==''){
            redirect("login");
        }
        $this->load->model("categoryModel");
        $this->load->model("customerModel");

        $this->load->model("userModel");
        $this->load->model("uomModel");
        $this->load->helper("pagination");
        $this->load->library("pagination");
        $this->load->model("settingsModel", 'sM');
        $settings_data=$this->sM->getSettings();
        if(isset($settings_data->per_page)){
         $this->per_page=$settings_data->per_page;
        }
    }


    public function index()
    {
        try {
           $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
           
            if(empty($user_details) || $user_details[0]['actve'] != 1){
                    $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                    delete_cookie('token_id'); 
                     delete_cookie('useName'); 
                    echo '<script>
                        alert("Unexpected server error.Please try again some time.");
                            window.location="'.base_url().'login";
                    </script>';
            }
            $user_details=current($user_details);

            $data=[]; 

            $cat_search=[];
            
            if(isset($search_input["cat_name"]) && trim($search_input["cat_name"])!=''){
                $cat_search["cat_name"] = $search_input["cat_name"];

            }
            
            if(isset($search_input["is_approved"]) && trim($search_input["is_approved"])!=''){
                $cat_search["is_approved"]=$search_input["is_approved"];
                
            }

            if(isset($search_input["status"]) && trim($search_input["status"])!=''){
                $cat_search["active"]=$search_input["status"];
                
            }


            $data["cat_search"] = $cat_search;
            $page=0;
            if($this->input->get("per_page")){
                $page= $this->input->get("per_page");
            }

            $data["page"]=$page;

            $parent_id = 0;
            $data["parent_id"]=$parent_id;

            $data["cat_data"]=$this->categoryModel->getCatList($cat_search,$this->per_page, $parent_id, $page); 
            //echo '<pre>';
            //print_r($data["cat_data"]);
            //die;
            
            $data["menu"]="category";
            $data["useName"]=$user_details["Name"];
            $data["custom_js"]=['category.js'];
            set_cookie('useName',$user_details["Name"],'3600');
            //search 
            $search_input=$this->input->get();
            //print_r($search_input); die();

            $data['view_file'] = 'Category';
            view($data);
           
        } catch (Exception $e) {
            $resposne=["status"=>"1","e"=>$e,"message"=>"Unexpected server error.Please try again some time.","field"=>'user_name'];
        }   
    }

    public function view($parent_id) 
    {
        $parent_id = base64_decode($parent_id);
        $get_parent_mapping = $this->categoryModel->getParentMapping($parent_id);

        if(is_array($get_parent_mapping) && count($get_parent_mapping)>0)
        {
            foreach($get_parent_mapping as $k=>$v)
            {
                $parentMappingArr = json_decode($v,true);
                $mappingArr[$k]['id'] = $parentMappingArr['id'];
                $mappingArr[$k]['parent_id'] = $parentMappingArr['parent_id'];
                $mappingArr[$k]['category_name_en'] = $parentMappingArr['category_name_en'];
            }
        }
        /* echo '<pre>';
        print_r($get_parent_mapping);
        die; */
        $data['mappingArr'] = $mappingArr;    
        $data['parent_id'] = $parent_id;   
        
        if($parent_id > 0)
        {
            $checkProductsInParentCat = $this->categoryModel->checkProductsInParentCat($parent_id);

            if($checkProductsInParentCat == 0)
            {
                $data['displayAddCategoryButton'] = true; 
            }else{
                $data['displayAddCategoryButton'] = false; 
            }
        }elseif($parent_id == 0)
        {
            $data['displayAddCategoryButton'] = true; 
        }else{
            $data['displayAddCategoryButton'] = false; 
        }

        $page=0;
       

        $data["page"]=$page;

        $data["parent_id"]=$parent_id;

        $data["cat_data"]=$this->categoryModel->getCatList($cat_search,$this->per_page, $parent_id, $page); 

        $data['view_file'] = 'ViewCategory';
        view($data);
    }

    public function add($parent_id) 
    {        
        try 
        {            
            $parent_id = base64_decode($parent_id);

            $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            
             if(empty($user_details) || $user_details[0]['actve'] != 1){
                     $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                     delete_cookie('token_id'); 
                      delete_cookie('useName'); 
                     echo '<script>
                         alert("Unexpected server error.Please try again some time.");
                             window.location="'.base_url().'login";
                     </script>';
             }else{
                $get_parent_mapping = $this->categoryModel->getParentMapping($parent_id);

                if(is_array($get_parent_mapping) && count($get_parent_mapping)>0)
                {
                    foreach($get_parent_mapping as $k=>$v)
                    {
                        $parentMappingArr = json_decode($v,true);
                        $mappingArr[$k]['id'] = $parentMappingArr['id'];
                        $mappingArr[$k]['parent_id'] = $parentMappingArr['parent_id'];
                        $mappingArr[$k]['category_name_en'] = $parentMappingArr['category_name_en'];
                    }
                }
                /* echo '<pre>';
                print_r($get_parent_mapping);
                die; */
                $data['mappingArr'] = $mappingArr;  

                $user_details=current($user_details);
                $data['parent_id'] = $parent_id;  
                $data['view_file'] = 'AddCategory';

                if($parent_id > 0)
                {
                    $checkProductsInParentCat = $this->categoryModel->checkProductsInParentCat($parent_id);

                    if($checkProductsInParentCat == 0)
                    {
                        $data['displayAddCategoryButton'] = true; 
                    }else{
                        $data['displayAddCategoryButton'] = false; 
                    }
                }elseif($parent_id == 0)
                {
                    $data['displayAddCategoryButton'] = true; 
                }else{
                    $data['displayAddCategoryButton'] = false; 
                }
                
                if(!empty($_POST) && $_POST['process'] == 'ok' && $data['displayAddCategoryButton']){
                    // echo '<pre>';
                    // print_r($_POST);
                    // die;
                    
                    if(isset($_POST["cat_name_en"]) && trim($_POST["cat_name_en"])==''){

                        $resposne=["error"=>"1","message"=>"Please Enter Category Name(English).","field"=>'cat_name_en'];

                    }elseif(isset($_POST["cat_name_fr"]) && trim($_POST["cat_name_fr"])==''){

                        $resposne=["error"=>"1","message"=>"Please Enter Category Name(French).","field"=>'cat_name_fr'];

                    }else if($parent_id == 0 && empty($_FILES["image"]) && trim($_FILES["image"]["name"])==''){

                        $resposne=["error"=>"1","message"=>"Please Select Category Image.","field"=>'image'];

                    }else if($parent_id < 0){

                        $resposne=["error"=>"1","message"=>"Invalid Category.","field"=>'parent_id'];

                    }else{
                        $check_duplicate_catname_en = $this->categoryModel->doCatCheck($_POST["cat_name_en"]);
                        $check_duplicate_catname_fr = $this->categoryModel->doCatCheckFr($_POST["cat_name_fr"]);

                        if($check_duplicate_catname_en )
                        {
                            $resposne=["error"=>"1","message"=>"Category name (English) already exists.","field"=>'cat_name_en']; 
                        }elseif($check_duplicate_catname_fr )
                        {
                            $resposne=["error"=>"1","message"=>"Category name(French) already exists.","field"=>'cat_name_fr']; 
                        }else{

                            $cat_data = [];
                            $cat_data['category_name_en'] = trim($_POST["cat_name_en"]);
                            $cat_data['category_name_fr'] = trim($_POST["cat_name_fr"]);
                            $cat_data['type'] = trim($_POST["cat_type"]);
                            $cat_data['uom_id'] = trim($_POST["uom_id"]);
                            $cat_data['parent_id'] = $parent_id;
                            $cat_data['metadata'] = '';
                            $cat_data['link_json'] = json_encode($get_parent_mapping);
                            $cat_data['displayLocal'] = trim($_POST["displayLocal"]);

                            $file_name=time().$_FILES["image"]["name"];
                            $allowedExts = array("gif", "jpeg", "jpg", "png");
                            $image_data=explode(".", $_FILES["image"]["name"]);
                            $extension =(!empty($image_data))? end($image_data):'';
                            $file_name=time().'.'.$extension;
                            
                            if ((($_FILES["image"]["type"] == "image/gif")  || ($_FILES["image"]["type"] == "image/jpeg")  || ($_FILES["image"]["type"] == "image/jpg")  || ($_FILES["image"]["type"] == "image/png")) && in_array($extension, $allowedExts)){

                                $this->load->library('image_lib');

                                
                                $config['upload_path'] = './uploads/category/';
                                $config['allowed_types'] = 'jpg|png|jpeg|gif';
                                $config['file_name'] =$file_name;                                
                                $this->load->library('upload', $config);

                                if ( ! $this->upload->do_upload('image'))
                                {
                                    $resposne=["error"=>"1","message"=>"Please Select jpg/jpeg/png/jpeg Category Image. Dimension Should Be 500 X 500",'error'=> $this->upload->display_errors(),"field"=>'image'];
                                    //echo $resposne;
                                // echo '<pre>';
                                // print_r($resposne);
                                }else
                                {
                                    $image_data =   $this->upload->data();
                          
                                    $configer =  array(
                                        'image_library'   => 'gd2',
                                        'source_image'    =>  $image_data['full_path'],
                                        'maintain_ratio'  =>  TRUE,
                                        'width'           =>  500,
                                        'height'          =>  500,
                                      );
                                      $this->image_lib->clear();
                                      $this->image_lib->initialize($configer);
                                      $this->image_lib->resize();

                                      $cat_data['image'] = $file_name;
                                }      

                                //echo '<pre>';
                                //print_r($cat_data);
                                //die;

                            }

                            // print_r($cat_data); die();

                            $id = $this->categoryModel->insertCat($cat_data);
                            
                            if($id>0){
                                $resposne=["error"=>"0","message"=>" Category added successfully.","field"=>'user_name'];
                            }else{
                                $resposne=["error"=>"1","message"=>"Category added failed.Please try again some time.","field"=>'cat_name'];
                            }

                        }
                    }
                 }
                 $data['error'] = $resposne;
                view($data);
             }
             
            }catch (Exception $e) {
                $resposne=["status"=>"1","e"=>$e,"message"=>"Unexpected server error.Please try again some time.","field"=>'user_name'];
            }  
        
    }

    

    public function edit($id) 
    {        
        try 
        {
            $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            
             if(empty($user_details) || $user_details[0]['actve'] != 1){
                     $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                     delete_cookie('token_id'); 
                      delete_cookie('useName'); 
                     echo '<script>
                         alert("Unexpected server error.Please try again some time.");
                             window.location="'.base_url().'login";
                     </script>';
             }else{

                $id = base64_decode($id);
                $edit_catData = current($this->categoryModel->getById($id));
                $data['edit_catData'] = $edit_catData; 
                $parent_id = $edit_catData['parent_id'];
                /*echo '<pre>';
                print_r($edit_catData);
                die; */
                $get_parent_mapping = $this->categoryModel->getParentMapping($parent_id);

                if(is_array($get_parent_mapping) && count($get_parent_mapping)>0)
                {
                    foreach($get_parent_mapping as $k=>$v)
                    {
                        $parentMappingArr = json_decode($v,true);
                        $mappingArr[$k]['id'] = $parentMappingArr['id'];
                        $mappingArr[$k]['parent_id'] = $parentMappingArr['parent_id'];
                        $mappingArr[$k]['category_name_en'] = $parentMappingArr['category_name_en'];
                    }
                }
                /* echo '<pre>';
                print_r($get_parent_mapping);
                die; */
                $data['mappingArr'] = $mappingArr;  

                $user_details=current($user_details);
                $data['parent_id'] = $parent_id;  
                $data['view_file'] = 'EditCategory';
        
                if(!empty($_POST) && $_POST['process'] == 'ok'){
                    // echo '<pre>';
                    // print_r($_POST);
                    // die;

                    $editId = base64_decode($_POST["edit_id"]);
                    
                    if(isset($_POST["cat_name_en"]) && trim($_POST["cat_name_en"])==''){

                        $resposne=["error"=>"1","message"=>"Please Enter Category Name(English).","field"=>'cat_name_en'];

                    }elseif(isset($_POST["cat_name_fr"]) && trim($_POST["cat_name_fr"])==''){

                        $resposne=["error"=>"1","message"=>"Please Enter Category Name(French).","field"=>'cat_name_fr'];

                    }else if($parent_id == 0 && empty($_FILES["image"]) && trim($_FILES["image"]["name"])=='' && $_POST["existingImg"]==''){

                        $resposne=["error"=>"1","message"=>"Please Select Category Image.","field"=>'image'];

                    }else if($parent_id < 0){

                        $resposne=["error"=>"1","message"=>"Invalid Category.","field"=>'parent_id'];

                    }else{
                        $check_duplicate_catname_en = $this->categoryModel->doCatCheck($_POST["cat_name_en"],$editId);
                        $check_duplicate_catname_fr = $this->categoryModel->doCatCheckFr($_POST["cat_name_fr"],$editId);

                        if($check_duplicate_catname_en )
                        {
                            $resposne=["error"=>"1","message"=>"Category name (English) already exists.","field"=>'cat_name_en']; 
                        }elseif($check_duplicate_catname_fr )
                        {
                            $resposne=["error"=>"1","message"=>"Category name(French) already exists.","field"=>'cat_name_fr']; 
                        }else{

                            $cat_data = [];
                            $cat_data['category_name_en'] = trim($_POST["cat_name_en"]);
                            $cat_data['category_name_fr'] = trim($_POST["cat_name_fr"]);
                            $cat_data['type'] = trim($_POST["cat_type"]);
                            $cat_data['uom_id'] = trim($_POST["uom_id"]);
                            $cat_data['parent_id'] = $parent_id;
                            $cat_data['metadata'] = '';
                            $cat_data['active'] = trim($_POST["cat_active"]);
                            $cat_data['link_json'] = json_encode($get_parent_mapping);
                            $cat_data['image'] = $_POST["existingImg"];
                            $cat_data['featured'] = $_POST["featured"];
                            $cat_data['display'] = $_POST["display"];
                            $cat_data['displayLocal'] = $_POST["displayLocal"];

                            $file_name=time().$_FILES["image"]["name"];
                            $allowedExts = array("gif", "jpeg", "jpg", "png");
                            $image_data=explode(".", $_FILES["image"]["name"]);
                            $extension =(!empty($image_data))? end($image_data):'';
                            $file_name=time().'.'.$extension;
                            
                            if ((($_FILES["image"]["type"] == "image/gif")  || ($_FILES["image"]["type"] == "image/jpeg")  || ($_FILES["image"]["type"] == "image/jpg")  || ($_FILES["image"]["type"] == "image/png")) && in_array($extension, $allowedExts)){

                                $this->load->library('image_lib');

                                
                                $config['upload_path'] = './uploads/category/';
                                $config['allowed_types'] = 'jpg|png|jpeg|gif';
                                $config['file_name'] =$file_name;                                
                                $this->load->library('upload', $config);

                                if ( ! $this->upload->do_upload('image'))
                                {
                                    $resposne=["error"=>"1","message"=>"Please Select jpg/jpeg/png/jpeg Category Image. Dimension Should Be 500 X 500",'error'=> $this->upload->display_errors(),"field"=>'image'];
                                    //echo $resposne;
                                //echo '<pre>';
                                //print_r($resposne);
                                }else
                                {
                                    $image_data =   $this->upload->data();
                          
                                    $configer =  array(
                                        'image_library'   => 'gd2',
                                        'source_image'    =>  $image_data['full_path'],
                                        'maintain_ratio'  =>  TRUE,
                                        'width'           =>  500,
                                        'height'          =>  500,
                                      );
                                      $this->image_lib->clear();
                                      $this->image_lib->initialize($configer);
                                      $this->image_lib->resize();

                                      $cat_data['image'] = $file_name;
                                }      

                                //echo '<pre>';
                                //print_r($cat_data);
                                //die;

                            }

                            $id = $this->categoryModel->updateCat($cat_data,$editId);
                            
                            if($id>0){
                                $resposne=["error"=>"0","message"=>" Category updated successfully.","field"=>'user_name'];
                                if($parent_id == 0)
                                {
                                    redirect('/category');
                                }else{
                                    redirect('/category/view/'.base64_encode($parent_id)); 
                                }
                                
                            }else{
                                $resposne=["error"=>"1","message"=>"Category added failed.Please try again some time.","field"=>'cat_name'];
                            }

                             

                        }
                    }
                 }

                 $data['error'] = $resposne;
                view($data);
             }
             
            }catch (Exception $e) {
                $resposne=["status"=>"1","e"=>$e,"message"=>"Unexpected server error.Please try again some time.","field"=>'user_name'];
            }  
        
    }
    
      
    public function delete($id) 
    {        
        try 
        {
            $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            
             if(empty($user_details) || $user_details[0]['actve'] != 1){
                     $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                     delete_cookie('token_id'); 
                      delete_cookie('useName'); 
                     echo '<script>
                         alert("Unexpected server error.Please try again some time.");
                             window.location="'.base_url().'login";
                     </script>';
             }else{

                $id = base64_decode($id);
                $edit_catData = current($this->categoryModel->getById($id));                
                $parent_id = $edit_catData['parent_id'];

                $deleteSuccessfull = $this->categoryModel->doDeleteCat($id);

                if($parent_id == 0)
                {
                    redirect('/category');
                }else{
                    redirect('/category/view/'.base64_encode($parent_id)); 
                }

                
                
             }
             
            }catch (Exception $e) {
                $resposne=["status"=>"1","e"=>$e,"message"=>"Unexpected server error.Please try again some time.","field"=>'user_name'];
            }  
        
    }

}
?>