<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class BannerModel extends CI_Model {
	protected $bannerTable = 'banner';

	public function __construct() {
        parent::__construct(); 
    }

    public function getBannerCount(){
    	try {
    		$this->db->db_debug = false;
    		$this->db->select("id, image");
    		$this->db->from($this->bannerTable);
    		
    		//$this->db->get();
	    	
	    	$db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query();die();
	        	return $this->db->count_all_results();

	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
    }

    public function getBanners($limit, $start){
    	try {
    		$this->db->db_debug = false;
    		$this->db->select("id, image, active");
    		$this->db->from($this->bannerTable);

    		$this->db->order_by('id', 'DESC'); 
	    	$this->db->limit($limit, $start);
    		
    		$query=$this->db->get();

	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query();
	        	$result=[];
	        	foreach($query->result_array() as $row){
	        		$result[]=$row;
	        	}
	        	return $result;
	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
    }

    public function insert($data){
    	//print_r($data);
    	try{
    		$this->db->db_debug = false;

    		$insert = $this->db->insert($this->bannerTable, $data);

    		$db_error = $this->db->error();
    		if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	return $insert?true:false;
	        }
    	}
    	catch(Exception $e) {
    		throw new Exception("Error Processing Request", 1);    		
    	}
    }

    public function getById($id){
    	try {
		 	$this->db->db_debug = false;

		 	$this->db->select('id, image, active')->from($this->bannerTable)->where("id", $id);    	
	    	
	    	$query=$this->db->get();

	    	$db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	$data=[];
	        	foreach($query->result_array() as $row){
	        		$data[]=$row;	    	
	        	}
	        	//echo $this->db->last_query();
	        	return $data;
	        }
	        return TRUE;
	    } catch (Exception $e) {
	       throw new Exception();
	    }	
    }

    public function update($data, $where=''){
    	try {
    	 	$this->db->db_debug = False;
    	 	$res = $this->db->update($this->bannerTable, $data, ['id' => $where]);

    	 	//echo $this->db->last_query(); die();

    	 	$db_error = $this->db->error();
	        if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }
	        else {
	        	if ($res) {
	        		return True;
	        	}
	        	else {
	        		return False;
	        	}
	        }
	        
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }
    }
}

?>