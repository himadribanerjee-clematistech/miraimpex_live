<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class Banner extends CI_Controller {
	protected $token_id='';
	protected $per_page = 10;
	
    public function __construct() {
    	parent::__construct();
    	$this->token_id=get_cookie('token_id'); 
    	if(trim($this->token_id)==''){
    		redirect("login");
    	}
        $this->load->model("bannerModel");
        $this->load->model("userModel");
        $this->load->helper("pagination");
        $this->load->library("pagination");
        $this->load->model("settingsModel", 'sM');
        $settings_data=$this->sM->getSettings();
        if(isset($settings_data->per_page)){
        $this->per_page=$settings_data->per_page;
        }
    }

    public function index()
	{
        try {
           $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            
            if(empty($user_details)){
                    $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                    delete_cookie('token_id'); 
                     delete_cookie('useName'); 
                    echo '<script>
                        alert("Unexpected server error.Please try again some time.");
                            window.location="'.base_url().'login";
                    </script>';
            }
            $user_details=current($user_details);
            $data=[]; 
            $data["menu"]="banner";
            $data["useName"]=$user_details["Name"];
            $data["custom_js"]=['banner.js'];
            
            set_cookie('useName',$user_details["Name"],'3600');

            $page=0;
            if($this->input->get("per_page")){
                $page = $this->input->get("per_page");
            }

            $total_row=$this->bannerModel->getBannerCount();
         
            $data["links"] = createPageNationLink(base_url('banner/index'), $total_row,$this->per_page);

            $start= (int)$page +1;
            $end = ($page+ $this->per_page >=$total_row)? $total_row : (int)$page  + $this->per_page;
            $data['result_count']='';
            if($total_row>0){
                $data['result_count']= "Showing ".$start." - ".$end." of ".$total_row." Results";
            }
            
            $data["banner"]=$this->bannerModel->getBanners($this->per_page, $page);
			
            $data['view_file'] = 'Banner';
            view($data);
           
        } catch (Exception $e) {
            $resposne=["status"=>"1","e"=>$e,"message"=>"Unexpected server error.Please try again some time.","field"=>'user_name'];
        }  
	}

    public function addBanner() {
        try{
            $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            if(empty($user_details)){
                    $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                    delete_cookie('token_id'); 
                     delete_cookie('role_id'); 
                    echo '<script>
                        alert("Session expired please login again.");
                            window.location="'.base_url().'login";
                    </script>';
                    exit();
            }
            $user_details=current($user_details);
            $data=[]; 
            $data["menu"]="banner";
            $data["useName"]=$user_details["Name"];
            $data["custom_js"]=['banner.js'];
            $data["file_jquery"] = ['jquery.form.min.js'];
            
            set_cookie('useName',$user_details["Name"],'3600');
            
            $data['view_file'] = 'AddBanner';
            view($data);
        } catch (Exception $e) {
            echo '<script>
                alert("Unexpected server error.Please try again some time.");
                            window.location="'.base_url().'banner";
            </script>';

        }
    }

    public function doAdd() {    
        try{
            $resposne=["status"=>"0","message"=>"","field"=>''];
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));

            if(trim($this->token_id)!=''){
                if(empty($_FILES["image"]) && !isset($_FILES["image"]["name"])){
                    $resposne=["status"=>"1","message"=>"Please Select Image.","field"=>'image','not_inseted'=>''];
                }else{

                    $this->load->library('upload');
                    $dataInfo = array();
                    $files = $_FILES['image']['name'];

                    //print_r($files['image']['name']); die();

                    $cpt = count($files);
                    $not_inseted=[];
                     $insert=0;
                    for($i=0; $i<$cpt; $i++)
                    {  
                        if (trim($files[$i]) =='') {
                            continue;
                        }
                        $fileName = time().'-'.$files[$i];
                        $allowedExts = array("gif", "jpeg", "jpg", "png");
                        $image_data=explode(".", $files[$i]);
                       
                        $extension = end($image_data);

                        if ((($_FILES["image"]["type"][$i] == "image/gif")  || ($_FILES["image"]["type"][$i] == "image/jpeg")  || ($_FILES["image"]["type"][$i] == "image/jpg")  || ($_FILES["image"]["type"][$i] == "image/png")) && in_array($extension, $allowedExts)){
                             

                            // File upload configuration
                            $uploadPath = './uploads/banner/';

                            $config = [];
                            $config['upload_path']      = $uploadPath;
                            $config['allowed_types']    = 'gif|jpg|jpeg|png'; 
                            //$config['min_height']       = 296;
                            //$config['min_width']        = 804;
                            $config['overwrite']        = FALSE;
                            $config['file_name']        = $fileName;  

                           // $this->upload->initialize($config);
                         
                            if ( ! move_uploaded_file($_FILES['image']['tmp_name'][$i],  $uploadPath."/".$fileName)){
                                $resposne=["status"=>"1","message"=>"Please Select jpg/jpeg/png/jpeg Image.",'error'=>'',"field"=>'image'];
                               
                            }else{

                                /*$dataInfo[] = $this->upload->data();

                                if(!empty($dataInfo)){
                                    // Insert files data into the database
                                    $imgData = [
                                        'image'     =>  $dataInfo[$i]['file_name'],
                                    ];
                                    
                                    $insert = $this->bannerModel->insert($imgData);
                                } else {
                                    $not_inseted[]=$dataInfo[$i]['file_name'];
                                }*/
                                $imgData = [
                                    'image'     =>  $fileName,
                                ];
                                
                                $insert = $this->bannerModel->insert($imgData);

                            }   

                        }else{
                            $not_inseted[]= $_FILES['image']['name'];
                        }
                      
                                   
                    }


                    if($insert>0){
                        $resposne=["status"=>"0","message"=>"Banner added successfully.","field"=>'image','not_inseted'=>implode(",", $not_inseted)];
                    }else{
                        $resposne=["status"=>"1","message"=>"Banner added failed.Please try again some time.","field"=>'image','not_inseted'=>implode(",", $not_inseted)];
                    }
                }
            }
        }
        catch(Exception $e) {
            $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'','not_inseted'=>''];
        }

        echo json_encode($resposne);
    }

    public function doEdit($id) {
        try{
            $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            
            if(empty($user_details)){
                    $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                    delete_cookie('token_id'); 
                     delete_cookie('role_id'); 
                    echo '<script>
                        alert("Session expired please login again.");
                            window.location="'.base_url().'login";
                    </script>';
                    exit();
            }
            $user_details=current($user_details);
            $id=base64_decode($id);
            if(trim($id)==''){
                echo '<script>
                        alert("Invalid user.");
                            window.location="'.base_url().'banner";
                    </script>';
                    exit();
            }
            $data=[]; 
            $data["menu"]="banner";
            $data["useName"]=$user_details["Name"];
            $data["custom_js"]=['banner.js'];
            set_cookie('useName',$user_details["Name"],'3600');

            $data['bannerDetail'] = current($this->bannerModel->getById($id));
            
            $data['view_file'] = 'EditBanner';
            view($data);
        }
        catch(Exception $e) {
            echo '<script>
                    alert("Check Again.");
                    window.location="'.base_url().'banner";
                </script>';
        }
    }


    public function doUpdate() {
        try{
            $data=$this->input->post();

            $resposne = ["status"=>"0","message"=>"","field"=>''];

            $user_details = current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));

            if(trim($this->token_id)!=''){
                if(isset($data["id"]) && trim($data["id"])==''){
                    $resposne=["status"=>"1","message"=>"Invalid Request.","field"=>'image'];
                }else{
                    $id = base64_decode($data["id"]);
                    $banner_details = current($this->bannerModel->getById($id));

                    if(empty($banner_details)){
                        $resposne=["status"=>"1","message"=>"Invalid Request.","field"=>'image'];
                    }else{
                        if(empty($_FILES["image"]) && !isset($_FILES["image"]["name"]) && trim($banner_details["image"])==''){
                            $resposne=["status"=>"1","message"=>"Please Select Image.","field"=>'image'];
                        }
                        else{

                            if(!empty($_FILES["image"]) && trim($_FILES["image"]["name"])!='') {

                                $file_name = time().$_FILES["image"]["name"];
                                $allowedExts = array("gif", "jpeg", "jpg", "png");
                                $image_data=explode(".", $_FILES["image"]["name"]);
                               
                                $extension =(!empty($image_data))?$image_data[1]:'';

                                 if ((($_FILES["image"]["type"] == "image/gif")  || ($_FILES["image"]["type"] == "image/jpeg")  || ($_FILES["image"]["type"] == "image/jpg")  || ($_FILES["image"]["type"] == "image/png")) && in_array($extension, $allowedExts)){

                                     $config['upload_path'] = './uploads/banner/';
                                        $config['allowed_types'] = 'jpg|png|jpeg|gif';
                                        //$config['min_height']       = 296;
                                        //$config['min_width']        = 804;
                                        $config['file_name'] = $file_name;
                                                    
                                        $this->load->library('upload', $config);
                                            
                                        if ( ! $this->upload->do_upload('image')){
                                            $resposne=["status"=>"1","message"=> str_ireplace('<p/>', '', $this->upload->display_errors()),'error'=> $this->upload->display_errors(),"field"=>'image'];
                                        }
                                        else{

                                            if(file_exists('./uploads/banner/'.$banner_details["image"])){
                                                unlink('./uploads/banner/'
                                                    .$banner_details["image"]);
                                            }


                                        }
                                 }else{

                                        $resposne=["status"=>"1","message"=>"Please Select jpg/jpeg/png/jpeg Image.",'error'=> '',"field"=>'image'];
                                 }
                             

                               
                            }else{

                                $file_name = $banner_details["image"];
                            }
                            if($resposne["status"]==0){
                                if ($banner_details['active'] == 0) {
                                    $buyerData = [ 'image' =>  $file_name, 'active' => 1];
                                }
                                else {
                                    $buyerData = [ 'image' =>  $file_name];
                                }                            

                                $update = $this->bannerModel->update($buyerData, $id);

                                if($update>0){
                                    $resposne=["status"=>"0","message"=>"Banner Updated successfully.","field"=>'image'];
                                }else{
                                    $resposne=["status"=>"1","message"=>"Banner Updated failed.Please try again some time.","field"=>'image'];
                                }
                            }
                            
                        }
                    }
                }
            }
        }
        catch(Exception $e) {
            $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>''];
        }

        echo json_encode($resposne);
    }


    public function doStatusChange(){
        $data=$this->input->post();
        //print_r($data);
        $id=base64_decode($data["id"]);
        $resposne=["status"=>"0","message"=>"","field"=>''];
        try {
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
            if(trim($this->token_id)!=''){
                $res = $this->bannerModel->update(["active"=>$data["status"]],$id);
              
                if($res){
                    $resposne=["status"=>"0","message"=>"Banner status changed successfully."];
                }
                else{                                
                    $resposne=["status"=>"1","message"=>"Banner change failed.Please try again after some time."];                                
                }
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
            }   
        }catch (Exception $e) {
                $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'status'];

        }

        echo json_encode($resposne);
    }
}

?>