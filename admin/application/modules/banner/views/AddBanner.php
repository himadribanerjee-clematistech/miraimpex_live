<div class="min-height-200px">
    <div class="page-header">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="title">
                    
                </div>
                <nav aria-label="breadcrumb" role="navigation">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url()?>banner">Banner</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Add Banner</li>
                    </ol>
                </nav>
            </div>

        </div>
    </div> 

    <!-- horizontal Basic Forms Start -->
    <div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
        <!-- <form id="addBannerfrm" method="post" action="javascript:void(0);" onsubmit="addBanner();"> -->
        <form id="addBannerfrm" method="post" action="javascript:void(0);">
          
            <div class="form-group">
                <div class="row">
                    
                    <div class="col-12">

                        <label for="image">Banner Images <sup class="mandatory_label">&#8727;</sup></label>
                        <!-- <input type="file" name="image[]" id="image" multiple class="form-control"> -->

                        <div id="testdv">

                            <div id="filediv_0" class="testcls">
                                <input type="file" name="image[]" id="image"/>
                            </div>
                        </div>

                    </div>
                    
                  
                    
                </div>

            </div>
            
            <div class="form-group">
                <div class="col-12 col-md-12 bom_space">
                    <div class="row">
                        <div class="col-12 imgShow"></div>
                    </div>
                </div>  
            </div>
            
            <div class="form-group">
                <div class="row">
                    <div class="col-12 text-right">
                        <input class="btn btn-primary" type="submit" value="Submit">
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- horizontal Basic Forms End -->

</div>


<script type="text/javascript">
    
var abc = 0;      // Declaring and defining global increment variable.

$(document).ready(function() {
    
    // Following function will executes on change event of file input to select different file.
    $('body').on('change', '#image', function() {
        if (this.files && this.files[0]) {
            abc += 1; // Incrementing global variable by 1.
            var z = abc - 1;
            $(this).find('#previewimg' + z).remove();

            $('.imgShow').after("<div style='display: flex; padding: 14px;'><div id='abcd_" + abc + "' class='abcd'><img id='previewimg" + abc + "' src='' style='width: 228px; height: 124px;' /><div style='bottom: 0; width: 100%; height: 24px; line-height: 9px; background: #3b485a; opacity: .85;'><a class='remove' href='javascript:void(0);' onclick='return false;' style='display: inline-block; width: 100%; height: 24px; background: url(../assets/images/uploader-icons.png) no-repeat center 3px; text-align: center; background-position: center -37px;'></a></div></div></div>");
        
            var reader = new FileReader();
            reader.onload = imageIsLoaded;
            reader.readAsDataURL(this.files[0]);
            $(this).hide();

            $("#abcd_" + abc).click(function(){
                $(this).parent().remove();
                var id=$(this).attr("id");
                var id_arr=id.split("_");
                var no =(parseInt(id_arr[1])-1);
                //console.log(abc,id_arr[1],no);
                if(id_arr.length==2){
                    $("#filediv_" + no).val("").trigger("change");
                    $("#filediv_" + no).remove();
                }         
               
            });



            $("#testdv").prepend($("<div id='filediv_"+abc+"' class ='testcls' />").fadeIn('slow').prepend($("<input/>", {
                name: 'image[]',
                type: 'file',
                id: 'image'
            })));
            
        }
    });

    // To Preview Image
    function imageIsLoaded(e) {
        $('#previewimg' + abc).attr('src', e.target.result);
    };


    $("#addBannerfrm").submit(function(e) {
        e.preventDefault();
        toastr.clear();

        var base_url=$("#base_url").val();
        var formData = new FormData(this);

        $('.loading').removeClass("hidden");
      
        $.ajax({
            url : base_url+"banner/doAdd",
            type : 'POST',
            data : formData,
            processData: false,
            contentType: false,
            success: function(result) {
                //console.log(result);
                $('.loading').addClass("hidden");
                result=JSON.parse(result);
                if(result.status==0){               
                    if(result["not_inseted"]!=''){
                         var input = $("#image");
                        input.replaceWith(input.val('').clone(true));
                        $("#imgShow").html("");
                        toastr.error(result["not_inseted"]+" image upload failed .Please upload valid image file.");
                    }else{
                        toastr.success(result["message"]);
                        setTimeout(
                          function() 
                          {
                            window.location=base_url+'banner';
                        
                          }, 1000);
                    }               
                }else{
                    if(result["not_inseted"]!=''){
                         var input = $("#image");
                        input.replaceWith(input.val('').clone(true));
                        $("#imgShow").html("");
                        toastr.error(result["not_inseted"]+" image upload failed .Please upload valid image file.");
                    }else{
                        toastr.error(result["message"]);
                        if(result["field"]=='to_login'){
                            window.location=base_url;
                        }else{
                            $("#"+result["field"]).focus();
                        }
                    }           
                } 
            },
            error: function(e) {
                console.log(result);  
            }    
        });
    });

});    

</script>