<div class="min-height-200px">
    <div class="page-header">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="title">
                    
                </div>
                <nav aria-label="breadcrumb" role="navigation">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url()?>banner">Banner</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Edit Banner</li>
                    </ol>
                </nav>
            </div>

        </div>
    </div>

    <!-- horizontal Basic Forms Start -->
    <div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
   


         <form id="adduserfrm" method="post" action="javascript:void(0);" onsubmit="updateBanner();" enctype="multipart/form-data" >
          
            <div class="form-group">
                <div class="row">
                    
                    <div class="col-4">

                        <label for="image">Banner Images <sup class="mandatory_label">&#8727;</sup></label>
                          <input type="file" name="image" id="image" onchange="loadFile()" class="form-control">
                    </div>
                    
                  
                    
                </div>

            </div>
            
            <div class="form-group">
            <div class="col-12 col-md-12 bom_space">
                <div class="row">
                    <div class="col-12">
                        <input type="hidden" id="img_old" value="<?php echo (isset($bannerDetail) && isset($bannerDetail["image"]) )?$bannerDetail["image"]:''; ?>" >

                        <img src="<?php echo (isset($bannerDetail) && ($bannerDetail["image"] != '') )?base_url('uploads/banner/' . $bannerDetail["image"]):base_url('assets/images/no-item-image.png'); ?>" height="150" width="250"  style="border: 2px dotted #ccc; border-radius: 2px;" id="chImg"/>
                    </div>
                </div>
            </div>  
        </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-6"></div>
                    <div class="col-6">
                        <input class="btn btn-primary" type="submit" value="Submit">
                        <input type="hidden" name="id" id="id" value="<?php echo (isset($bannerDetail) && isset($bannerDetail["id"]) ) ? base64_encode($bannerDetail["id"]):'';?>">
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- horizontal Basic Forms End -->

</div>

<?php
    if(isset($custom_js)){
        foreach($custom_js as $val){
            ?>
            <script src="<?php echo base_url();?>assets/js/custom_js/<?php echo $val;?>"></script>
            <?php
        }
    }
    
?>
  