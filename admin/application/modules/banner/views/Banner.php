
  
<!-- Simple Datatable start -->
<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="clearfix mb-20">
        <div class="pull-left">
            <h5 class="text-blue">List of Banner</h5>
            
        </div>
        <div class="pull-right">
            <a class="btn btn-outline-primary" href="<?php echo base_url()?>banner/addBanner">Add Banner</a>
            
        </div>
    </div>
 

   
    <div class="row">
        <table class="data-table stripe hover nowrap" id="userlst">
            <thead>
                <tr>
                 <th width="10%"  class="table-plus datatable-nosort" >Sl No.</th>
                 <th width="40%"  class="table-plus datatable-nosort">Banner Image</th>
                 <th width="10%"  class="table-plus datatable-nosort" >On Display</th>
                 <th width="20%"  class="table-plus datatable-nosort" >Status</th>
                 <th width="20%"  class="table-plus datatable-nosort" >Action</th>
                    
                </tr>
            </thead>
            <tbody>
                <?php
        if(!empty($banner)){
            $i=0;
            foreach($banner as $val){
                $i++;
    ?>
            
            <tr class="">
                <td class="table-plus"><?php echo $i;?></td>
                <td class="table-plus" >
                    <img src="<?php echo base_url('uploads/banner/' . $val["image"]); ?>" height='80' width='100'>
                </td>
                <td class="table-plus">
                    <center>
                        <?php echo (isset($val) && isset($val["active"]) && $val['active'] == 1) ? '<i class="fa fa-check fa-2x" aria-hidden="true" style="color: green"></i>' : '<i class="fa fa-times fa-2x" aria-hidden="true" style="color: red"></i>'; ?>
                    </center>
                </td>
                <td class="table-plus">
                    <select name="status" class="form-control" id="status" onchange="doStatusChange(this.value, '<?php echo base64_encode($val["id"]);?>')">
                        <option value="1" <?php echo (isset($val) && isset($val["active"]) && $val['active'] == 1)?'selected':''; ?>> Active </option>

                        <option value="0" <?php echo (isset($val) && isset($val["active"]) && $val['active'] == 0)?'selected':''; ?>> InActive </option>
                    </select>
                </td>
                
                <td class="table-plus">
                     <div class="dropdown">
                        <a class="btn btn-outline-primary dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                            <i class="fa fa-ellipsis-h"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                         
                            <a class="dropdown-item" href="javascript:void(0);" " onclick="editBanner('<?php echo base64_encode($val["id"]);?>')" ><i class="fa fa-pencil"></i> Edit</a>
                            
                        </div>
                    </div>
                   

                  
                </td>
            </tr>
                        
    <?php
            }
        }
    ?>
              
<!-- Section for registration dropdown start -->
<script src="<?php echo base_url();?>assets/js/custom_js/city_state.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        for (region in countries)
        {
            $('#registrationRegion').append('<option value="' + region + '">' + region + '</option>');
        }
        
    });
</script>

<div class="col-sm-12">
<select name="registrationRegion" class="form-control" id="registrationRegion" onchange="set_country(this,registrationCountry,registrationState,'registrationCountry')">
    <option value="1"> -- Select Region -- </option>
</select>

<select name="registrationCountry" class="form-control" id="registrationCountry" onchange="set_city_state(this,registrationState,'registrationState')">
    <option value="1"> -- Select Country -- </option>
</select>

<select name="registrationState" class="form-control" id="registrationState">
    <option value="1"> -- Select State -- </option>
</select>

</div>

<!-- Registration dropdown end -->
             
            </tbody>
        </table>
        <div class="col-sm-12">
            <div style="float: right;"><?php echo $links; ?></div>
        </div>
    </div>
</div>

    
<?php
    if(isset($custom_js)){
        foreach($custom_js as $val){
            ?>
            <script src="<?php echo base_url();?>assets/js/custom_js/<?php echo $val;?>"></script>
            <?php
        }
    }
    
?>
  