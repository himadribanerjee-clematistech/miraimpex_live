<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class Attribute extends CI_Controller {
	protected $token_id='';
	protected $per_page=10;
	public function __construct() {
    	parent::__construct();
    	$this->token_id=get_cookie('token_id'); 
    	if(trim($this->token_id)==''){
    		redirect("login");
    	}
        $this->load->model("AttributeModel", 'aM');
        $this->load->model("SettingsModel", 'sM');
        $this->load->model("userModel");
        $this->load->helper("pagination");
        $this->load->library("pagination");

        $settings_data=$this->sM->getSettings();
        if(isset($settings_data->per_page)){
            $this->per_page=$settings_data->per_page;
        }
    }
    public function index()
	{
		try {
           $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            
            if(empty($user_details)){
                    $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                    delete_cookie('token_id'); 
                     delete_cookie('useName'); 
                    echo '<script>
                        alert("Unexpected server error.Please try again some time.");
                            window.location="'.base_url().'login";
                    </script>';
            }

            $user_details=current($user_details);
            $data=[]; 
            $data["menu"]="attribute";
            $data["useName"]=$user_details["Name"];
            $data["custom_js"]=['attribute.js'];
            
            set_cookie('useName',$user_details["Name"],'3600');

            //search 
            $search_input=$this->input->get();

            $attr_search=[];
            
            if(isset($search_input["name_en"]) && trim($search_input["name_en"])!=''){
            	$attr_search["name_en"] = $search_input["name_en"];
            }
            if(isset($search_input["type"]) && trim($search_input["type"])!=''){
                $attr_search["type"] = $search_input["type"];
            }
            if(isset($search_input["status"]) && trim($search_input["status"])!=''){
                $attr_search["status"] = $search_input["status"];
            }
            
            $data["attr_search"] = $attr_search;
            $page=0;
			if($this->input->get("per_page")){
	       		$page= $this->input->get("per_page");
	       	}

            $data["page"]=$page;

            $total_row=$this->aM->getAttributeCount($attr_search);
         
	        $data["links"] = createPageNationLink(base_url('attribute/index'), $total_row, $this->per_page);

	        $start= (int)$page +1;
			$end = ($page+ $this->per_page >=$total_row)? $total_row : (int)$page  + $this->per_page;
			$data['result_count']='';
			if($total_row>0){
				$data['result_count']= "Showing ".$start." - ".$end." of ".$total_row." Results";
			}
			
			$data["uom_data"]=$this->aM->getAttributeList($attr_search, $this->per_page, $page);

            $data['view_file'] = 'Attribute';
            // print_r($data); die();
            view($data);
           
        } catch (Exception $e) {
            $resposne=["status"=>"1","e"=>$e,"message"=>"Unexpected server error.Please try again some time.","field"=>'user_name'];
        }   
	}
	
    public function addAttribute(){
        try {
            $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            if(empty($user_details)){
                    $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                    delete_cookie('token_id'); 
                     delete_cookie('role_id'); 
                    echo '<script>
                        alert("Session expired please login again.");
                            window.location="'.base_url().'login";
                    </script>';
                    exit();
            }
            $user_details=current($user_details);
            $data=[]; 
            $data["menu"]="attribute";
            $data["useName"]=$user_details["Name"];
            $data["custom_js"]=['attribute.js'];
            set_cookie('useName',$user_details["Name"],'3600');
             $data['view_file'] = 'AddAttribute';
            view($data);
        } catch (Exception $e) {
            echo '<script>
                alert("Unexpected server error.Please try again some time.");
                            window.location="'.base_url().'category";
            </script>';

        }
    }

    public function doAddAttribute(){
        try {
            $data=$this->input->post();
            $resposne=["status"=>"0","message"=>"", "field"=>''];
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));

            //print_r($data); die();

            if(trim($this->token_id)!=''){
                if(isset($data["name_en"]) && trim($data["name_en"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter Name.","field"=>'name_en'];
                }
                elseif(isset($data["name_fr"]) && trim($data["name_fr"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter Name in French.","field"=>'name_fr'];
                }
                elseif(isset($data["type"]) && trim($data["type"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter Type.","field"=>'type'];
                }
                else{
                    $check_duplicate_name = $this->aM->doAttrCheck($data["name_en"]);

                    if($check_duplicate_name == 1) {
                        $resposne=["status"=>"1","message"=>"Duplicate Attribute Name.","field"=>'name_en'];
                    }
                    else {
                        
                        $formData = [
                            'name_en'   => $data['name_en'], 
                            'name_fr'   => $data['name_fr'], 
                            'type'      => $data['type'], 
                            'status'    => $data['active'],
                        ];
                        $res = $this->aM->saveAttribute($formData);

                        if($res){
                            $resposne=["status"=>"0","message"=>"Attribute added successfully."];
                        }
                        else{                                
                            $resposne=["status"=>"1","message"=>"Attribute added failed.Please try again after some time."];                                
                        }
                    }
                }
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied."];
            }
        } catch (Exception $e) {
            $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time."];

        }

        echo json_encode($resposne);
    }

    public function doAttributeEdit($id){
        $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            
        if(empty($user_details)){
                $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                delete_cookie('token_id'); 
                 delete_cookie('role_id'); 
                echo '<script>
                    alert("Session expired please login again.");
                        window.location="'.base_url().'login";
                </script>';
                exit();
        }
        $user_details=current($user_details);
        $id=base64_decode($id);
        if(trim($id)==''){
            echo '<script>
                    alert("Invalid user.");
                        window.location="'.base_url().'user";
                </script>';
                exit();
        }
        $data=[]; 
        $data["menu"]="attribute";
        $data["useName"]=$user_details["Name"];
        $data["custom_js"]=['attribute.js'];
        set_cookie('useName',$user_details["Name"],'3600');
        $data['attr_detail'] = current($this->aM->getById($id));

        // print_r($data); die();
         
        $data['view_file'] = 'EditAttribute';
        view($data);
            
    }
    public function updateAttribute(){

        try {
            $data=$this->input->post();
            $resposne=["status"=>"0","message"=>"", "field"=>''];
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));

            if(trim($this->token_id)!=''){
                if(isset($data["name_en"]) && trim($data["name_en"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter Name.","field"=>'name_en'];
                }
                elseif(isset($data["name_fr"]) && trim($data["name_fr"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter Name in French.","field"=>'name_fr'];
                }
                elseif(isset($data["type"]) && trim($data["type"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter Type.","field"=>'type'];
                }
                else{
                    $check_duplicate_name = $this->aM->doAttrCheck($data["name_en"], base64_decode($data["id"]));

                    if($check_duplicate_name == 1) {
                        $resposne=["status"=>"1","message"=>"Duplicate Attribute Name.","field"=>'name_en'];
                    }
                    else {
                        
                       $formData = [
                            'name_en'   => $data['name_en'], 
                            'name_fr'   => $data['name_fr'], 
                            'type'      => $data['type'], 
                            'status'    => $data['active'],
                        ];
                        $res = $this->aM->saveAttribute($formData, base64_decode($data["id"]));

                        if($res){
                            $resposne=["status"=>"0","message"=>"Attribute Updated successfully."];
                        }
                        else{                                
                            $resposne=["status"=>"1","message"=>"Attribute Updation failed.Please try again after some time."];                                
                        }
                    }
                }
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied."];
            }
        } catch (Exception $e) {
            $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time."];

        }

        echo json_encode($resposne);
    }

    public function doAttrStatusChange(){
        $data=$this->input->post();
        $id=base64_decode($data["id"]);
        $resposne=["status"=>"0","message"=>"","field"=>''];
        try {
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
            if(trim($this->token_id)!=''){
                $res = $this->aM->saveAttribute(["status"=>$data["active"]], $id);
              
                if($res){
                    $resposne=["status"=>"0","message"=>"Attribute status change successfully."];
                }
                else{                                
                    $resposne=["status"=>"1","message"=>"Attribute change failed.Please try again after some time."];                                
                }
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
            }   
        }catch (Exception $e) {
                $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'status'];

        }

        echo json_encode($resposne);
    }

    public function doAttrTypeChange(){
        $data=$this->input->post();
        $id=base64_decode($data["id"]);
        $resposne=["status"=>"0","message"=>"","field"=>''];
        try {
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
            if(trim($this->token_id)!=''){
                $res = $this->aM->saveAttribute(["type"=>$data["type"]], $id);
              
                if($res){
                    $resposne=["status"=>"0","message"=>"Attribute type change successfully."];
                }
                else{                                
                    $resposne=["status"=>"1","message"=>"Attribute type change failed.Please try again after some time."];                                
                }
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
            }   
        }catch (Exception $e) {
                $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'status'];

        }

        echo json_encode($resposne);
    }

}