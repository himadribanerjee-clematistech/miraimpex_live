<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class ForumModel extends CI_Model {
	protected $forumTable = 'forum';
	
	public function __construct() {
        parent::__construct();
    }
    
    public function getforumCount($data){
    	//print_r($data); die();
    	try {
    		$this->db->db_debug = false;
    		$this->db->select("id, title_en, SUBSTRING(description, 1, 95) AS description, date, status");
    		$this->db->from($this->forumTable);
    		if(isset($data["title_en"]) && $data["title_en"]!=''){ 
	    		$this->db->like("title_en",$data["title_en"]);
	    	}
	    	if(isset($data["type"]) && $data["type"]!=''){ 
	    		$this->db->where("type",$data["type"]);
	    	}
	    	if(isset($data["status"]) && $data["status"]!=''){ 
	    		$this->db->where("status",$data["status"]);
	    	}

	    	// $this->db->get();
	    	// echo $this->db->last_query();die();
	    	
	    	$db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query();die();
	        	return $this->db->count_all_results();

	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
    }
    public function getforumList($data='',$limit='', $start=''){
    	try {
    		$this->db->db_debug = false;
    		$this->db->select("id, title_en, SUBSTRING(description, 1, 95) AS description, date, status");
    		$this->db->from($this->forumTable);
    		if(isset($data["title_en"]) && $data["title_en"]!=''){ 
	    		$this->db->like("title_en",$data["title_en"]);
	    	}
	    	if(isset($data["type"]) && $data["type"]!=''){ 
	    		$this->db->where("type",$data["type"]);
	    	}
	    	if(isset($data["status"]) && $data["status"]!=''){ 
	    		$this->db->where("status",$data["status"]);
	    	}
	    	
	    	$this->db->order_by('date', 'DESC'); 

	    	if ($limit != '' ) {
	    		$this->db->limit($limit, $start);
	    	}
	    	
 
 			$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query();die();
	        	$result=[];
	        	foreach($query->result_array() as $row){
	        		$result[]=$row;
	        	}
	        	return $result;
	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
    }	

    public function doForumCheck($data, $id=''){
    	// print_r($data);  print_r($id); die();
		try {
		 	$this->db->db_debug = FALSE;
	        $this->db->select("title_en");
	    	$this->db->from($this->forumTable);
	    	if(isset($data) && trim($data)!='' ){
	    		$this->db->where("title_en",$data);	    		
	    	}

	    	if($id!=''){
	    		$this->db->where("id !=",$id);
	    	}

	    	$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	// echo $this->db->last_query(); die();
	        	if($query->num_rows() >0) {
	        		return True;
	        	}
	        	else {
	        		return false;
	        	}
	        }
	        //return TRUE;
	    } catch (Exception $e) {
	       throw new Exception();
	    }	
    }

    public function saveForum($data, $where=''){
    	try {
    	 	$this->db->db_debug = False;

    	 	if ($where != '') {
    	 		$res = $this->db->update($this->forumTable, $data, ['id' => $where]);
    	 	}
    	 	else {
    	 		$res = $this->db->insert($this->forumTable, $data);
    	 	}

    	 	

	    	$db_error = $this->db->error();
	        if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }
	        else {
	        	if ($res) {
	        		return True;
	        	}
	        	else {
	        		return False;
	        	}
	        }
	        
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }
    }

    public function getById($id){
    	try {
		 	$this->db->db_debug = false;
	        $this->db->select("id, title_en, title_fr, description, type, quantity, image, DATE_FORMAT(date, '%d-%b-%Y') as date");
	    	$this->db->from($this->forumTable);
	    	
	    	$this->db->where("id", $id);
	    	
	    	$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	$data=[];
	        	foreach($query->result_array() as $row){
	        		 $data[]=$row;
	    	
	        	}
	        	// echo $this->db->last_query();
	        	return $data;
	        }
	        return TRUE;
	    } catch (Exception $e) {
	       throw new Exception();
	    }	
    }
}

?>