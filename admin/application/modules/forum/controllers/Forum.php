<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class Forum extends CI_Controller {
	protected $token_id='';
	protected $per_page=10;
	public function __construct() {
    	parent::__construct();
    	$this->token_id=get_cookie('token_id'); 
    	if(trim($this->token_id)==''){
    		redirect("login");
    	}
        $this->load->model("ForumModel", 'fM');
        $this->load->model("SettingsModel", 'sM');
        $this->load->model("userModel");
        $this->load->helper("pagination");
        $this->load->library("pagination");

        $settings_data=$this->sM->getSettings();
        if(isset($settings_data->per_page)){
            $this->per_page=$settings_data->per_page;
        }
    }
    public function index() { 
        // echo "Hell";
		try {
           $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            
            if(empty($user_details)){
                    $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                    delete_cookie('token_id'); 
                     delete_cookie('useName'); 
                    echo '<script>
                        alert("Unexpected server error.Please try again some time.");
                            window.location="'.base_url().'login";
                    </script>';
            }

            $user_details=current($user_details);
            $data=[]; 
            $data["menu"]="forum";
            $data["useName"]=$user_details["Name"];
            $data["custom_js"]=['forum.js'];
            
            set_cookie('useName',$user_details["Name"],'3600');

            //search 
            $search_input=$this->input->get();

            $forum_search=[];
            
            if(isset($search_input["title_en"]) && trim($search_input["title_en"])!=''){
            	$forum_search["title_en"] = $search_input["title_en"];
            }
            if(isset($search_input["type"]) && trim($search_input["type"])!=''){
                $forum_search["type"] = $search_input["type"];
            }
            if(isset($search_input["status"]) && trim($search_input["status"])!=''){
                $forum_search["status"] = $search_input["status"];
            }
            
            $data["forum_search"] = $forum_search;
            $page=0;
			if($this->input->get("per_page")){
	       		$page= $this->input->get("per_page");
	       	}

            $data["page"]=$page;

            $total_row=$this->fM->getforumCount($forum_search);
         
	        $data["links"] = createPageNationLink(base_url('forum/index'), $total_row, $this->per_page);

	        $start= (int)$page +1;
			$end = ($page+ $this->per_page >=$total_row)? $total_row : (int)$page  + $this->per_page;
			$data['result_count']='';
			if($total_row>0){
				$data['result_count']= "Showing ".$start." - ".$end." of ".$total_row." Results";
			}
			
			$data["forum_data"]=$this->fM->getforumList($forum_search, $this->per_page, $page);

            $data['view_file'] = 'Forum';
            // print_r($data); die();
            view($data);
           
        } catch (Exception $e) {
            $resposne=["status"=>"1","e"=>$e,"message"=>"Unexpected server error.Please try again some time.","field"=>'user_Name'];
        }   
	}
	
    public function addForum(){
        try {
            $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            if(empty($user_details)){
                    $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                    delete_cookie('token_id'); 
                    delete_cookie('role_id'); 
                    echo '<script>
                        alert("Session expired please login again.");
                            window.location="'.base_url().'login";
                    </script>';
                    exit();
            }
            $user_details=current($user_details);
            $data=[]; 
            $data["menu"]="forum";
            $data["useName"]=$user_details["Name"];
            $data["custom_js"]=['forum.js'];
            set_cookie('useName',$user_details["Name"],'3600');
             $data['view_file'] = 'AddForum';
            view($data);
        } catch (Exception $e) {
            echo '<script>
                alert("Unexpected server error.Please try again some time.");
                            window.location="'.base_url().'category";
            </script>';

        }
    }

    public function doAddforum(){
        try {
            $data=$this->input->post();
            $resposne=["status"=>"0","message"=>"", "field"=>''];
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));

            //print_r($data); die();

            if(trim($this->token_id)!=''){
                if(isset($data["title_en"]) && trim($data["title_en"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter Name.","field"=>'title_en'];
                }
                elseif(isset($data["title_fr"]) && trim($data["title_fr"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter Name in French.","field"=>'title_fr'];
                }
                elseif(isset($data["description"]) && trim($data["description"])==''){
                    $resposne=["status"=>"1","message"=>"Please EnterDescription.","field"=>'description'];
                }
                elseif(isset($data["type"]) && trim($data["type"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter Type.","field"=>'type'];
                }
                elseif(isset($data["qty"]) && trim($data["qty"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter Quantity.","field"=>'qty'];
                }
                else{
                    $check_duplicate_Name = $this->fM->doForumCheck($data["title_en"]);

                    if($check_duplicate_Name == 1) {
                        $resposne=["status"=>"1","message"=>"Duplicate forum Name.","field"=>'title_en'];
                    }
                    else {

                        $file_name=time().$_FILES["image"]["name"];
                        $allowedExts = array("gif", "jpeg", "jpg", "png");
                        $image_data=explode(".", $_FILES["image"]["name"]);
                        $extension =(!empty($image_data))?$image_data[1]:'';
                        if ((($_FILES["image"]["type"] == "image/gif")  || ($_FILES["image"]["type"] == "image/jpeg")  || ($_FILES["image"]["type"] == "image/jpg")  || ($_FILES["image"]["type"] == "image/png")) && in_array($extension, $allowedExts)){
                            $config['upload_path'] = './uploads/forum/';
                            $config['allowed_types'] = 'jpg|png|jpeg|gif';
                            // $config['max_size'] = '200';
                            //$config['max_width'] = '94';
                            //$config['max_height'] = '134';
                            $config['file_name'] =$file_name;

                            $this->load->library('upload', $config);
                            if ( ! $this->upload->do_upload('image')){
                                $resposne=["status"=>"1","message"=>"Please Select jpg/jpeg/png/jpeg Category Image. Dimension Should Be 94 X 134",'error'=> $this->upload->display_errors(),"field"=>'image'];

                            }else{
                                $formData = [
                                    'title_en'      => $data['title_en'], 
                                    'title_fr'      => $data['title_fr'], 
                                    'description'   => $data['description'], 
                                    'type'          => $data['type'], 
                                    'quantity'      => $data['qty'],
                                    'image'         => $file_name,
                                ];

                                $res = $this->fM->saveForum($formData);

                                if($res){
                                    $resposne=["status"=>"0","message"=>"Forum Data added successfully."];
                                }
                                else{                                
                                    $resposne=["status"=>"1","message"=>"Forum Data added failed.Please try again after some time."];                                
                                }
                            }
                        }
                        else{
                            $resposne=["status"=>"1","message"=>"Please Select jpg/jpeg/png/jpeg Category Image.",'error'=> '',"field"=>'image'];
                        }                        
                    }
                }
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied."];
            }
        } catch (Exception $e) {
            $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time."];

        }

        echo json_encode($resposne);
    }

    public function doforumEdit($id){
        $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            
        if(empty($user_details)){
                $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                delete_cookie('token_id'); 
                 delete_cookie('role_id'); 
                echo '<script>
                    alert("Session expired please login again.");
                        window.location="'.base_url().'login";
                </script>';
                exit();
        }
        $user_details=current($user_details);
        $id=base64_decode($id);
        if(trim($id)==''){
            echo '<script>
                    alert("Invalid user.");
                        window.location="'.base_url().'user";
                </script>';
                exit();
        }
        $data=[]; 
        $data["menu"]="forum";
        $data["useName"]=$user_details["Name"];
        $data["custom_js"]=['forum.js'];
        set_cookie('useName',$user_details["Name"],'3600');
        $data['forum_detail'] = current($this->fM->getById($id));

        // print_r($data); die();
         
        $data['view_file'] = 'EditForum';
        view($data);
            
    }
    public function updateForum(){

        try {
            $data=$this->input->post();
            $resposne=["status"=>"0","message"=>"", "field"=>''];
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));

            if(trim($this->token_id)!=''){
                if(isset($data["title_en"]) && trim($data["title_en"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter Name.","field"=>'title_en'];
                }
                elseif(isset($data["title_fr"]) && trim($data["title_fr"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter Name in French.","field"=>'title_fr'];
                }
                elseif(isset($data["description"]) && trim($data["description"])==''){
                    $resposne=["status"=>"1","message"=>"Please EnterDescription.","field"=>'description'];
                }
                elseif(isset($data["type"]) && trim($data["type"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter Type.","field"=>'type'];
                }
                elseif(isset($data["qty"]) && trim($data["qty"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter Quantity.","field"=>'qty'];
                }
                else{
                    $id=base64_decode($data["id"]);

                    // print_r($id); die();

                    $check_duplicate_Name = $this->fM->doForumCheck($data["title_en"], $id);

                    if($check_duplicate_Name == 1) {
                        $resposne=["status"=>"1","message"=>"Duplicate forum Name.","field"=>'title_en'];
                    }
                    else {                        
                        $forum_details=current($this->fM->getById($id));

                        if(empty($forum_details)){
                            $resposne=["status"=>"1","message"=>"Invalid Request.","field"=>'cat_name'];
                        }
                        else{

                            // print_r($_FILES); die();

                            if(!empty($_FILES["image"]) && trim($_FILES["image"]["name"])!=''){
                                $file_name=time().$_FILES["image"]["name"];
                                $allowedExts = array("gif", "jpeg", "jpg", "png");
                                $image_data=explode(".", $_FILES["image"]["name"]);
                                $extension =(!empty($image_data))?$image_data[1]:'';

                                if ((($_FILES["image"]["type"] == "image/gif")  || ($_FILES["image"]["type"] == "image/jpeg")  || ($_FILES["image"]["type"] == "image/jpg")  || ($_FILES["image"]["type"] == "image/png")) && in_array($extension, $allowedExts)){
                                        
                                    $config['upload_path'] = './uploads/forum/';
                                    $config['allowed_types'] = 'jpg|png|jpeg|gif';
                                    //$config['max_width'] = '94';
                                   // $config['max_height'] = '134';
                                    $config['file_name'] = $file_name;
                                        
                                    $this->load->library('upload', $config);
                                    if ( ! $this->upload->do_upload('image')){
                                        $resposne=["status"=>"1","message"=>"Please Select jpg/jpeg/png/jpeg Forum Image.",'error'=> $this->upload->display_errors(),"field"=>'image'];
                                    }else{
                                        if(file_exists('./uploads/forum/'.$forum_details["image"])){
                                            unlink('./uploads/forum/'.$forum_details["image"]);
                                        }
                                    } 
                                }else{
                                    $resposne=["status"=>"1","message"=>"Please Select jpg/jpeg/png/jpeg Forum Image.",'error'=> '',"field"=>'image'];
                                }
                            }else{
                                $file_name=$forum_details["image"];
                            }

                            $formData = [
                                'title_en'      => $data['title_en'], 
                                'title_fr'      => $data['title_fr'], 
                                'description'   => $data['description'], 
                                'type'          => $data['type'], 
                                'quantity'      => $data['qty'],
                                'image'         => $file_name,
                            ];

                            $res = $this->fM->saveForum($formData, $id);

                            if($res){
                                $resposne=["status"=>"0","message"=>"forum Updated successfully."];
                            }
                            else{                                
                                $resposne=["status"=>"1","message"=>"forum Updation failed.Please try again after some time."];                                
                            }
                        }
                    }
                }
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied."];
            }
        } catch (Exception $e) {
            $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time."];

        }

        echo json_encode($resposne);
    }

    public function getById() {
        try{
            $id = base64_decode($this->input->post('id'));

            $resposne=["status"=>"0","message"=>"","field"=>'', 'data'=> ''];

            $user_details = current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
            if(trim($this->token_id)!=''){
                $res = current($this->fM->getById($id));

                $resposne=["status"=>"0","message"=>"Success.","field"=>'', 'data'=> $res];
            }
            else {
                $resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
            }

        }
        catch(Exception $e) {
            $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'role_id'];
        }

        echo json_encode($resposne);
    }

    public function doForumStatusChange(){
        $data=$this->input->post();
        $id=base64_decode($data["id"]);
        $resposne=["status"=>"0","message"=>"","field"=>''];
        try {
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
            if(trim($this->token_id)!=''){
                $res = $this->fM->saveForum(["status"=>$data["status"]], $id);
              
                if($res){
                    $resposne=["status"=>"0","message"=>"Forum status change successfully."];
                }
                else{                                
                    $resposne=["status"=>"1","message"=>"Forum change failed.Please try again after some time."];                                
                }
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
            }   
        }catch (Exception $e) {
                $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'status'];

        }

        echo json_encode($resposne);
    }

}