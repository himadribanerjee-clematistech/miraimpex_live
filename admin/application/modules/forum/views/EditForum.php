<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="clearfix mb-10">
        <div class="pull-left">
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo base_url('forum')?>">Forum Details</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Update Following Forum Details</li>
                </ol>
            </nav>
        </div> 
        <div class="pull-right">
            <a class="btn btn-outline-primary" href="<?php echo base_url('forum')?>">Back</a>            
        </div> 
    </div> 

    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">

                <form id="editforumfrm" method="post" action="javascript:void(0);" onsubmit="updateforum();">

                    <input type="hidden" name="id" id="id" value="<?php echo (isset($forum_detail) && isset($forum_detail["id"]) )?base64_encode($forum_detail["id"]):'';?>">

                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-6">
                                <label label-for='name'> Title [English]<span class="mandatory_label">*</span></label>
                                <input type="text" name="title_en" id="title_en" placeholder="Title" value="<?php echo (isset($forum_detail) && isset($forum_detail["title_en"]) )?$forum_detail["title_en"]:'';?>" class="form-control">
                            </div>

                            <div class="col-lg-6">
                                <label label-for='name'> Title [French] <span class="mandatory_label">*</span></label>
                                <input type="text" name="title_fr" id="title_fr" placeholder="Titre" value="<?php echo (isset($forum_detail) && isset($forum_detail["title_fr"]) )?$forum_detail["title_fr"]:'';?>" class="form-control">
                            </div>
                        </div>
                    </div>

                    <br>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-8">
                                <label label-for='name'> Description<span class="mandatory_label">*</span></label>
                                <textarea name="description" id="description" placeholder="Description" class="form-control" rows="3"><?php echo (isset($forum_detail) && isset($forum_detail["description"]) )?$forum_detail["description"]:'';?></textarea>
                            </div>
                            <div class="col-lg-4">
                                <label label-for='name'> Post Describing Image</label>

                                <div class="text-center">
                                    <img src="<?php echo (isset($forum_detail) && ($forum_detail["image"] != '') )?base_url('uploads/forum/' . $forum_detail["image"]):base_url('assets/images/no-item-image.png'); ?>"  style="border: 2px dotted #ccc; border-radius: 2px; height: 150px; width: 160px;" id="chImg"/>
                                </div>

                                <input type="hidden" id="img_old" value="<?php echo (isset($forum_detail) && isset($forum_detail["image"]) )?$forum_detail["image"]:''; ?>" >

                                <div style="padding-top: 7px; ">
                                    <input type="file" name="img" id="img" onchange="loadFile()">
                                </div>
                                
                            </div>
                        </div>
                    </div>

                    <br>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-4">
                                <label label-for='name'> Type <span class="mandatory_label">*</span></label>
                                <input type="text" name="type" id="type" placeholder="Passes / Tickets" value="<?php echo (isset($forum_detail) && isset($forum_detail["type"]) )?$forum_detail["type"]:'';?>" class="form-control">
                            </div>

                            <div class="col-lg-3">
                                <label label-for='name'> Quantity <span class="mandatory_label">*</span></label>
                                <input type="text" name="qty" id="qty" placeholder="Quantity" value="<?php echo (isset($forum_detail) && isset($forum_detail["quantity"]) )?$forum_detail["quantity"]:'';?>" class="form-control">
                            </div>

                            <div class="col-lg-4 text-right pt-20">
                                <input type="submit" value="Update" name="submit" class="btn btn-outline-primary">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>




<?php
	if(isset($custom_js)){
		foreach($custom_js as $val){
			?>
			<script src="<?php echo base_url();?>assets/js/custom_js/<?php echo $val;?>"></script>
			<?php
		}
	}
	
?>
  