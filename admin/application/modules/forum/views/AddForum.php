<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="clearfix mb-10">
        <div class="pull-left">
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo base_url('forum')?>">Forum Details</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Add New Forum Details</li>
                </ol>
            </nav>
        </div>  
        <div class="pull-right">
            <a class="btn btn-outline-primary" href="<?php echo base_url('forum')?>">Back</a>            
        </div> 
    </div> 

    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">

                <form id="addForumfrm" method="post" action="javascript:void(0);" onsubmit="addForum();">

                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-6">
                                <label label-for='name'> Title [English]<span class="mandatory_label">*</span></label>
                                <input type="text" name="title_en" id="title_en" placeholder="Title" value="" class="form-control">
                            </div>

                            <div class="col-lg-6">
                                <label label-for='name'> Title [French] <span class="mandatory_label">*</span></label>
                                <input type="text" name="title_fr" id="title_fr" placeholder="Titre" value="" class="form-control">
                            </div>
                        </div>
                    </div>

                    <br>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-8">
                                <label label-for='name'> Description<span class="mandatory_label">*</span></label>
                                <textarea name="description" id="description" placeholder="Description" class="form-control" rows="3"></textarea>
                            </div>
                            <div class="col-lg-4">
                                <label label-for='name'> Post Describing Image</label>

                                <div class="text-center">
                                    <img src="<?php echo base_url('assets/images/no-item-image.png'); ?>"  style="border: 2px dotted #ccc; border-radius: 2px; height: 150px; width: 160px;" id="chImg"/>
                                </div>

                                <div style="padding-top: 7px; ">
                                    <input type="file" name="img" id="img" onchange="loadFile()">
                                </div>
                                
                            </div>
                        </div>
                    </div>

                    <br>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-4">
                                <label label-for='name'> Type <span class="mandatory_label">*</span></label>
                                <input type="text" name="type" id="type" placeholder="Passes / Tickets" value="" class="form-control">
                            </div>

                            <div class="col-lg-3">
                                <label label-for='name'> Quantity <span class="mandatory_label">*</span></label>
                                <input type="text" name="qty" id="qty" placeholder="Quantity" value="" class="form-control">
                            </div>

                            <!-- <div class="col-lg-3">
                                <label for="active"> Status </label>
                                <select name="active" class="form-control" id="active">
                                    <option value="1" >Active</option>
                                    <option value="0" >Inactive</option>
                                </select>
                            </div> -->

                            <div class="col-lg-4 text-right pt-20">
                                <input type="submit" value="Submit" name="submit" class="btn btn-outline-primary">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>




<?php
    if(isset($custom_js)){
        foreach($custom_js as $val){
            ?>
            <script src="<?php echo base_url();?>assets/js/custom_js/<?php echo $val;?>"></script>
            <?php
        }
    }
    
?>