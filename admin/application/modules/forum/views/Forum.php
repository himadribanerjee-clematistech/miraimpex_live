<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="clearfix mb-10">
        <div class="pull-left">
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Forum Details</a></li>
                </ol>
            </nav>
        </div>
        <div class="pull-right"> 
            <a class="btn btn-outline-primary" href="<?php echo base_url('forum/addForum')?>">Add New</a>            
        </div>
    </div>

    <div class="container-fluid bg-light"> 
        <div class="row">
            <div class="col-lg-12">

                <form id="attrsrch" action="<?php echo base_url('forum')?>">
                    <input type="hidden" name="per_page_search" value="<?php echo $page; ?>">
                    <div class="row align-items-center justify-content-center">
                        <div class="col-md-3 pt-4 pb-4">
                            <input type="text" name="title_en" id="title_en" value="<?php echo (isset($attr_search["title_en"]))?$attr_search["title_en"]:'';?>" placeholder="Title in English" class="form-control">
                        </div>

                        <div class="col-md-3 pt-4 pb-4">
                            <input type="text" name="type" id="type" value="<?php echo (isset($attr_search["type"]))?$attr_search["type"]:'';?>" placeholder="Type" class="form-control">
                        </div>

                        <div class="col-md-3 pt-4 pb-4">
                            <select id="status" name="status" class="form-control">
                                <option value="">Select Status</option>
                                <option value="1" <?php echo (isset($attr_search) && isset($attr_search["status"]) &&  $attr_search["status"]==1)?'selected':'';?>>Active</option>
                                <option value="0"  <?php echo (isset($attr_search) && isset($attr_search["status"]) &&  $attr_search["status"]==0)?'selected':'';?>>InActive</option>
                            </select>
                        </div>
                        <div class="col-md-3 pt-4 pb-4">
                            <a class="btn btn-outline-primary" href="<?php echo base_url('Forum')?>"><i class="fa fa-refresh" aria-hidden="true"></i> Reset</a> 
                                
                            <input type="submit" name="search" value="Search" class="btn btn-outline-primary">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="row pt-4">
        
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="data-table stripe hover no-wrap" style="font-size: 14px;">
            <thead>
                <tr class="titel_bar">
                    <th class="table-plus datatable-nosort" width="5%">#</th>
                    <th class="table-plus datatable-nosort" width="30%">Title</th>
                    <th class="table-plus datatable-nosort" width="30%">Description</th>
                    <th class="table-plus datatable-nosort" width="20%">Added</th>
                    <th class="table-plus datatable-nosort" width="20%">Status</th>
                    <th class="table-plus datatable-nosort" width="20%" ><center>Action</center></th>
                    <th class="table-plus datatable-nosort" width="20%" ><center>View</center></th>
                </tr>
            </thead>
            <tbody>

            <?php
            	if(!empty($forum_data)){
            		$i=0;
            		foreach($forum_data as $val){
            			$i++;
            			?>
            			 <tr class="">
							<td ><?php echo $i;?></td>	
							<td >
                                <?php echo $val["title_en"];?>           
                            </td>
                            <td >
                                <?php echo $val["description"];?> ....          
                            </td>	
                            <td>
                                <?php echo date('d-M-Y H:i', strtotime($val["date"])); ?>
                            </td>
                            <td>
                                <select name="status" id="status" onchange="doForumStatusChange(this.value,'<?php echo base64_encode($val["id"]);?>')" class="form-control">
                                    <option value="1" <?php echo (isset($val) && isset($val["status"]) && $val['status'] == 1)?'selected':''; ?>> Active </option>

                                    <option value="0" <?php echo (isset($val) && isset($val["status"]) && $val['status'] == 0)?'selected':''; ?>> InActive </option>
                                </select>
                            </td>
                            <td class="table-plus" style="text-align:center;">

                                <div class="dropdown">
                                    <a class="btn btn-outline-primary dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                        <i class="fa fa-ellipsis-h"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item" href="javascript:void(0);" style=" font-size: 15px;" onclick="doForumEdit('<?php echo base64_encode($val["id"]);?>')" >
                                            <i class="fa fa-edit" aria-hidden="true"></i> 
                                            Edit
                                        </a>
                                    </div>
                                </div>

                            </td>
                            <td class="table-plus text-center">
                                <a class="btn btn-outline-primary forum" href="#" role="button" data-toggle="modal" data-id="<?php echo base64_encode($val['id']) ?>">
                                    <i class="fa fa-eye"></i>
                                </a>
                            </td>
							

            		 	</tr>
            			<?php
            		}
            	}else{
            		?>
            		 <tr class="">
            		 	 <td colspan="6" class="text-center">No Record Found</td>	

            		 </tr>	
            		<?php
            	}
            ?>
           
                 
            </tbody>
        </table>
    </div>

    <div class="row pt-4">
        <div class="col-lg-6">
            <?php echo ($result_count) ? $result_count : ''; ?>
        </div>
        <div class="col-lg-6">
            <div style="float: right;"><?php echo $links; ?></div>
        </div>
    </div>
</div>


<!-- Modal -->

<div class="modal fade admin_modal" id="forumModal">
    <div class="modal-dialog modal-dialog-centered modal-sm">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h4 class="modal-title"> <span id="title_en_view">Title</span> </h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="row" style="padding-bottom: 20px;">
                    <div class="col-lg-12">
                        <div class="forum_info_img">
                            <img src="<?php echo base_url('assets/images/no-item-image.png'); ?>" id="image" name="image"/>
                        </div>
                    </div>                  
                </div>

                <div class="row" style="padding-bottom: 10px;">
                    <div class="col-lg-6 text-left">
                        <span id="type_">Type</span> <b>[</b> <span id="qty">Quantity</span> pcs. <b>]</b>
                    </div>
                    <div class="col-lg-6 text-left">
                        <label><b>Posted On:</b> <span id="date">Date</span> </label>
                    </div>
                </div>

                <div class="row" style="padding-bottom: 10px;">
                    <div class="col-lg-12">
                        <div class="description"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p></div>
                    </div>
                </div>
               
            </div>

        </div>
    </div>
</div>


<!-- Modal Ends -->


<?php
	if(isset($custom_js)){
		foreach($custom_js as $val){
			?>
			<script src="<?php echo base_url();?>assets/js/custom_js/<?php echo $val;?>"></script>
			<?php
		}
	}
	
?>
  