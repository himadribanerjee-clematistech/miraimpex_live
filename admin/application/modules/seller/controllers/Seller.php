<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class Seller extends CI_Controller {
	protected $token_id='';
	protected $per_page=10;
	
    public function __construct() {
    	parent::__construct();
    	$this->token_id=get_cookie('token_id'); 
    	if(trim($this->token_id)==''){
    		redirect("login");
    	}
        $this->load->model("customerModel");
        $this->load->model("sellerModel");
        $this->load->model("userModel");
        $this->load->helper("pagination");
        $this->load->library("pagination");
        $this->load->model("settingsModel", 'sM');
        $settings_data=$this->sM->getSettings();
        if(isset($settings_data->per_page)){
         $this->per_page=$settings_data->per_page;
        }

        //echo $this->per_page; die();
    }
    public function index() {
        // echo "Hell"; die();
		try {
           $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            
            if(empty($user_details)){
                    $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                    delete_cookie('token_id'); 
                     delete_cookie('useName'); 
                    echo '<script>
                        alert("Unexpected server error.Please try again some time.");
                            window.location="'.base_url().'login";
                    </script>';
            }
            $user_details=current($user_details);
            $data=[]; 
            $data["menu"]="seller";
            $data["useName"]=$user_details["Name"];
            $data["custom_js"]=['seller.js'];
            
            set_cookie('useName',$user_details["Name"],'3600');

            $search_input=$this->input->get();
            //print_r($search_input); die();

            $seller_search=[];
            
            if(isset($search_input["customer_id"]) && trim($search_input["customer_id"])!=''){
                $seller_search["customer_id"] = base64_decode($search_input["customer_id"]);
            }
            /*if(isset($search_input["business_name"]) && trim($search_input["business_name"])!=''){
                $seller_search["business_name"] = $search_input["business_name"];
            }*/
            if(isset($search_input["city"]) && trim($search_input["city"])!=''){
                $seller_search["city"] = $search_input["city"];
            }
            
            $data["seller_search"] = $seller_search;
            $page=0;

            //echo $this->input->get("per_page"); die();

            if($this->input->get("per_page")){
                $page= $this->input->get("per_page");
            }

            $data["page"]=$page;

            $total_row=$this->sellerModel->getSellerCount($seller_search);
         
            $data["links"] = createPageNationLink(base_url('seller/index'), $total_row,$this->per_page);

            //echo $total_row . '<br>' . $this->per_page . '<br>' . $data['links']; die();

            $start= (int)$page +1;
            $end = ($page+ $this->per_page >=$total_row)? $total_row : (int)$page  + $this->per_page;
            $data['result_count']='';
            if($total_row>0){
                $data['result_count']= "Showing ".$start." - ".$end." of ".$total_row." Results";
            }
            
            $data["seller"]=$this->sellerModel->getSellers($seller_search,$this->per_page, $page);
			
            $data["customer"]=$this->sellerModel->getCustomer();
			
            $data['view_file'] = 'Seller';

            // print_r($data); die();
            view($data);
           
        } catch (Exception $e) {
            $resposne=["status"=>"1","e"=>$e,"message"=>"Unexpected server error.Please try again some time.","field"=>'user_name'];
        }   
    }
    
    public function doLclSuppStatusChange(){
        $data=$this->input->post();
        //print_r($data);
        $id=base64_decode($data["id"]);
        $resposne=["status"=>"0","message"=>"","field"=>''];
        try {
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
            if(trim($this->token_id)!=''){
                $res = $this->customerModel->updateCust(["displayLocal"=>$data["displayLocal"], 'update_by' => 1, 'update_on' => date('Y-m-d H:i:s')], $id);
              
                if($res){
                    $resposne=["status"=>"0","message"=>"Seller status changed successfully."];
                }
                else{                                
                    $resposne=["status"=>"1","message"=>"Seller change failed.Please try again after some time."];                                
                }
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
            }   
        }catch (Exception $e) {
                $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'status'];

        }

        echo json_encode($resposne);
    }

    public function addSeller() {
        try {
            $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            if(empty($user_details)){
                    $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                    delete_cookie('token_id'); 
                     delete_cookie('role_id'); 
                    echo '<script>
                        alert("Session expired please login again.");
                            window.location="'.base_url().'login";
                    </script>';
                    exit();
            }
            $user_details=current($user_details);
            $data=[]; 
            $data["menu"]="seller";
            $data["useName"]=$user_details["Name"];
            $data["custom_js"]=['seller.js'];
            
            set_cookie('useName',$user_details["Name"],'3600');
            
            $data["customer"]=$this->sellerModel->getCustomer();

            $data['view_file'] = 'AddSeller';
            view($data);
        } catch (Exception $e) {
            echo '<script>
                alert("Unexpected server error.Please try again some time.");
                            window.location="'.base_url().'seller";
            </script>';

        }
    }

    public function doAddNew(){
        try {
            $data=$this->input->post();

            $resposne=["status"=>"0","message"=>"","field"=>''];
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));

            if(trim($this->token_id)!=''){
                if(isset($data["customer_id"]) && trim($data["customer_id"])==''){
                    $resposne=["status"=>"1","message"=>"Please Select Customer.","field"=>'customer_id'];
                }else if(isset($data["business_name"]) && trim($data["business_name"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter Bussiness Name.","field"=>'business_name'];
                }else if(isset($data["address"]) && trim($data["address"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter Address.","field"=>'address'];
                }else if(isset($data["state"]) && trim($data["state"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter State.","field"=>'state'];
                }else if(isset($data["city"]) && trim($data["city"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter City.","field"=>'city'];
                }else if(isset($data["lat"]) && trim($data["lat"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter Latitude.","field"=>'lat'];
                }else if(isset($data["long"]) && trim($data["long"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter Longitude.","field"=>'long'];
                }else if(isset($data["contact_name"]) && trim($data["contact_name"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter Contact Name.","field"=>'contact_name'];
                }else if(isset($data["contact_number"]) && trim($data["contact_number"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter Contact Number.","field"=>'contact_number'];
                }else if(empty($_FILES["business_logo"]) && !isset($_FILES["business_logo"]["name"])){
                    $resposne=["status"=>"1","message"=>"Please Select Image.","field"=>'business_logo','not_inseted'=>''];
                }else{
                    $check_duplicate_customer = $this->sellerModel->doSellerCheck(base64_decode($data["customer_id"]));

                    if($check_duplicate_customer == 1) {
                        $resposne=["status"=>"1","message"=>"Duplicate Customer.","field"=>'customer_id'];
                    }
                    else {
                        $check_duplicate_profile = $this->sellerModel->doSellerCheck('', $data["business_name"]);

                        if($check_duplicate_profile == 1) {
                            $resposne=["status"=>"1","message"=>"Duplicate Bussiness Name.","field"=>'business_name'];
                        }
                        else {

                            $sellerData = [
                                'cust_id'       =>  base64_decode($data['customer_id']),
                                'business_name' =>  $data['business_name'],
                                'address'       =>  $data['address'],
                                'state'         =>  $data['state'],
                                'city'          =>  $data['city'],
                                'lat'           =>  $data['lat'],
                                'long'          =>  $data['long'],
                                'contact_name'  =>  $data['contact_name'],
                                'contact_number'=>  $data['contact_number'],
                                'agent_YN'      =>  $data['agent_YN'],
                                'active'        =>  1,
                            ];

                            $id = $this->sellerModel->insertSeller($sellerData);

                            if($id > 0){
                                $last_id = $id;

                                $this->load->library('upload');
                                $dataInfo = '';
                                $files = $_FILES;

                                //print_r($_FILES); die();

                                $cpt = count($_FILES['business_logo']['name']);
                                $not_inseted=[];
                                $insert=0;

                                for($i=0; $i<$cpt; $i++)
                                {  
                                    if (trim($files['business_logo']['name'][$i]) =='') {
                                        continue;
                                    }  

                                    $fileName = time().'-'.$files['business_logo']['name'][$i];
                                    $allowedExts = array("gif", "jpeg", "jpg", "png");
                                    $image_data=explode(".", $files['business_logo']['name'][$i]);

                                    //$extension =(!empty($image_data))?$image_data[1]:'';
                                    $extension = end($image_data);

                                    $_FILES['business_logo']['name']        = $fileName;
                                    $_FILES['business_logo']['type']        = $files['business_logo']['type'][$i];
                                    $_FILES['business_logo']['tmp_name']    = $files['business_logo']['tmp_name'][$i];
                                    $_FILES['business_logo']['error']       = $files['business_logo']['error'][$i];
                                    $_FILES['business_logo']['size']        = $files['business_logo']['size'][$i]; 
                                
                                    if ((($_FILES["business_logo"]["type"] == "image/gif")  || ($_FILES["business_logo"]["type"] == "image/jpeg")  || ($_FILES["business_logo"]["type"] == "image/jpg")  || ($_FILES["business_logo"]["type"] == "image/png") && in_array($extension, $allowedExts))) {
                                     
                                        // File upload configuration
                                        $uploadPath = './uploads/seller/';

                                        $config = [];
                                        $config['upload_path']      = $uploadPath;
                                        $config['allowed_types']    = 'gif|jpg|jpeg|png';
                                        //$config['min_height']       = 296;
                                        //$config['min_width']        = 804;
                                        $config['overwrite']        = FALSE;
                                        $config['file_name']        = $fileName;  

                                        $this->upload->initialize($config);

                                        if ( ! $this->upload->do_upload('business_logo')){
                                            $resposne=["status"=>"1","message"=>"Please Select jpg/jpeg/png/jpeg Image.",'error'=> $this->upload->display_errors(),"field"=>'image'];
                                            //print_r($this->upload->display_errors());
                                        }else{
                                            $dataInfo = $this->upload->data();                     

                                            if(!empty($dataInfo)){
                                                // Insert files data into the database
                                                $sellerImg = [
                                                    'seller_id' =>  $last_id,
                                                    'image'     =>  $dataInfo['file_name'],
                                                ];
                                                
                                                $insert = $this->sellerModel->insertSellerImg($sellerImg);
                                            } else {
                                                $not_inseted[]=$dataInfo['file_name'];
                                            } 
                                        }
                                    }else{
                                        $not_inseted[]= $_FILES['business_logo']['name'];
                                    }
                                }

                                if($insert){
                                    $resposne=["status"=>"0","message"=>"Seller added successfully.","field"=>'image','not_inseted'=>implode(",", $not_inseted)];
                                }else{
                                    $resposne=["status"=>"1","message"=>"Seller added failed.Please try again after some time.","field"=>'image','not_inseted'=>implode(",", $not_inseted)];
                                }
                            }
                            else {
                                $resposne=["status"=>"1","message"=>"Please Select jpg/jpeg/png/jpeg Logo Image.",'error'=> '',"field"=>'image'];
                            }
                        }
                    }
                }
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
            }
        } catch (Exception $e) {
                $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'business_name'];

        }

        echo json_encode($resposne);
    }

    public function getById() {
        try{
            $id = base64_decode($this->input->post('id'));

            $resposne=["status"=>"0","message"=>"","field"=>'', 'data'=> ''];

            $user_details = current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
            if(trim($this->token_id)!=''){
                $res = current($this->sellerModel->getById($id));

                $resposne=["status"=>"0","message"=>"Success.","field"=>'', 'data'=> $res];
            }
            else {
                $resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
            }

        }
        catch(Exception $e) {
            $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'role_id'];
        }

        echo json_encode($resposne);
    }

    public function getAgentById() {
        try{
            $id = base64_decode($this->input->post('id'));

            $resposne=["status"=>"0","message"=>"","field"=>'', 'data'=> ''];

            $user_details = current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
            if(trim($this->token_id)!=''){
                $res = current($this->sellerModel->getAgentById($id));

                $resposne=["status"=>200,"message"=>"Success.","field"=>'', 'data'=> $res];
            }
            else {
                $resposne=["status"=>300,"message"=>"Permission Denied.","field"=>'to_login'];
            }

        }
        catch(Exception $e) {
            $resposne=["status"=>300,"message"=>"Unexpected server error.Please try again some time.","field"=>'role_id'];
        }

        echo json_encode($resposne);
    }

    public function doAgentChange() {
        $data = $this->input->post();
        $id = base64_decode($data["cust_id"]);
        $resposne=["status"=>"0","message"=>"","field"=>''];

        try {
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
            if(trim($this->token_id)!=''){
                $res = $this->sellerModel->updateAgent(["agent_YN" => $data["agent_YN"], 'update_by' => 1, 'update_on' => date('Y-m-d H:i:s')], $id);
              
                if($res){
                    
                    $row = $this->customerModel->getEmailId($id);

                    if ($data['agent_YN'] == 1) { // Accepted

                        $data_mail = ['message' => 'Thank You for Requesting As An Agent and You Have Successfully Granted As An Agent. Welcome to Akpoti Family.', 'name' => $row['name']];
                    }
                    else { //Rejected
                        $data_mail = ['message' => 'Thank You for Requesting As An Agent But We Are Sorry! We Cannot Grant Your Request At This Time. Please Contact Administrator!', 'name' => $row['name']];
                    }

                    $mail = email($row['email'], $data_mail);

                    if ($mail) {
                        $resposne=["status"=>"0","message"=>"Agent status Updated successfully."];
                    }
                    else {
                        $resposne=["status"=>"0","message"=>"Mail Not Sent. Contact System Admin"];
                    }                    
                }
                else{                                
                    $resposne=["status"=>"1","message"=>"Agent status Updation failed.Please try again after some time."];                                
                }
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
            }   
        }catch (Exception $e) {
                $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'status'];

        }

        echo json_encode($resposne);
    }

    

    /*public function doSellerEdit($id) {
        $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            
        if(empty($user_details)){
                $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                delete_cookie('token_id'); 
                 delete_cookie('role_id'); 
                echo '<script>
                    alert("Session expired please login again.");
                        window.location="'.base_url().'login";
                </script>';
                exit();
        }
        $user_details=current($user_details);
        $id=base64_decode($id);
        if(trim($id)==''){
            echo '<script>
                    alert("Invalid user.");
                        window.location="'.base_url().'seller";
                </script>';
                exit();
        }
        $data=[]; 
        $data["menu"]="seller";
        $data["useName"]=$user_details["Name"];
        $data["custom_js"]=['seller.js'];
        set_cookie('useName',$user_details["Name"],'3600');
        $data['sellerDetail'] = current($this->sellerModel->getById($id));

        //print_r($data['sellerDetail']); die();
        //print_r($data['sellerDetail']['image'][0]['img_id']); die();

        $data["customer"]=$this->sellerModel->getCustomer();
         
        $data['view_file'] = 'EditSeller';
        view($data);
    }*/

    /*public function doSellerImageDlt() {
        $data=$this->input->post();
        $dltid = $data['dltid'];
        $id=base64_decode($data["id"]);
        //echo $dltid; 
        //echo $id; die();
        $resposne=["status"=>"0","message"=>"","field"=>'', 'dltid'=>''];
        try {
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
            if(trim($this->token_id)!=''){

                $img = current($this->sellerModel->getImgById($id));

                $res = $this->sellerModel->dltSellerImg($id);
              
                if($res){

                    if(file_exists('./uploads/seller/' . $img["image"])) {
                        unlink('./uploads/seller/' . $img["image"]);
                    }

                    $resposne=["status"=>"0","message"=>"Image Deleted successfully.", "dltid" => $dltid ];
                }
                else{                                
                    $resposne=["status"=>"1","message"=>"Image Deletion failed.Please try again after some time."];                                
                }
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
            }   
        }catch (Exception $e) {
                $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'status'];

        }

        echo json_encode($resposne);
    }*/

    /*public function doUpdate(){
        $data=$this->input->post();
        $resposne=["status"=>"0","message"=>"","field"=>"", "not_inseted"=>""];
        
        try {
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
            if(trim($this->token_id)!=''){
                if(isset($data["id"]) && trim($data["id"])==''){
                    $resposne=["status"=>"1","message"=>"Invalid Request.","field"=>'business_name', "not_inseted"=>""];
                }else{
                    $id=base64_decode($data["id"]);
                    $seller_details=current($this->sellerModel->getById($id));
                    //print_r($seller_details); die();

                    if(empty($seller_details)){
                        $resposne=["status"=>"1","message"=>"Invalid Request.","field"=>'', "not_inseted"=>""];
                    }
                    else{
                        
                        if(isset($data["customer_id"]) && trim($data["customer_id"])==''){
                            $resposne=["status"=>"1","message"=>"Please Select Customer.","field"=>'customer_id',"not_inseted"=>""];
                        }else if(isset($data["business_name"]) && trim($data["business_name"])==''){
                            $resposne=["status"=>"1","message"=>"Please Enter Bussiness Name.","field"=>'business_name',"not_inseted"=>""];
                        }else if(isset($data["address"]) && trim($data["address"])==''){
                            $resposne=["status"=>"1","message"=>"Please Enter Address.","field"=>'address',"not_inseted"=>""];
                        }else if(isset($data["state"]) && trim($data["state"])==''){
                            $resposne=["status"=>"1","message"=>"Please Enter State.","field"=>'state',"not_inseted"=>""];
                        }else if(isset($data["city"]) && trim($data["city"])==''){
                            $resposne=["status"=>"1","message"=>"Please Enter City.","field"=>'city',"not_inseted"=>""];
                        }else if(isset($data["lat"]) && trim($data["lat"])==''){
                            $resposne=["status"=>"1","message"=>"Please Enter Latitude.","field"=>'lat',"not_inseted"=>""];
                        }else if(isset($data["long"]) && trim($data["long"])==''){
                            $resposne=["status"=>"1","message"=>"Please Enter Longitude.","field"=>'long',"not_inseted"=>""];
                        }else if(isset($data["contact_name"]) && trim($data["contact_name"])==''){
                            $resposne=["status"=>"1","message"=>"Please Enter Contact Name.","field"=>'contact_name',"not_inseted"=>""];
                        }else if(isset($data["contact_number"]) && trim($data["contact_number"])==''){
                            $resposne=["status"=>"1","message"=>"Please Enter Contact Number.","field"=>'contact_number',"not_inseted"=>""];
                        }
                        else{

                            $check_duplicate_customer = $this->sellerModel->doSellerCheck(base64_decode($data["customer_id"]), '', $id);

                            if($check_duplicate_customer == 1) {
                                $resposne=["status"=>"1","message"=>"Duplicate Customer.","field"=>'customer_id',"not_inseted"=>""];
                            }
                            else {
                                $check_duplicate_profile = $this->sellerModel->doSellerCheck('', $data["business_name"], $id);

                                if($check_duplicate_profile == 1) {
                                    $resposne=["status"=>"1","message"=>"Duplicate Bussiness Name.","field"=>'business_name',"not_inseted"=>""];
                                }
                                else {

                                    $sellerData = [
                                        'cust_id'       =>  base64_decode($data['customer_id']),
                                        'business_name' =>  $data['business_name'],
                                        'address'       =>  $data['address'],
                                        'state'         =>  $data['state'],
                                        'city'          =>  $data['city'],
                                        'lat'           =>  $data['lat'],
                                        'long'          =>  $data['long'],
                                        'contact_name'  =>  $data['contact_name'],
                                        'contact_number'=>  $data['contact_number'],
                                        'agent_YN'      =>  $data['agent_YN'],
                                    ];
                                    //print_r($sellerData); die();

                                    $update = $this->sellerModel->updateSeller($sellerData, $id);

                                    if ($update) {
                                        //print_r($_FILES["business_logo"]['name']);
                                        if(!empty($_FILES["business_logo"]['name'][1])){
                                            
                                            $this->load->library('upload');
                                            $dataInfo = '';
                                            $files = $_FILES;

                                            $cpt = count($_FILES['business_logo']['name']);
                                            $not_inseted = [];
                                            //$insert = 0;

                                            for($i=0; $i<$cpt; $i++) {    
                                                if (trim($files['business_logo']['name'][$i]) =='') {
                                                    continue;
                                                }

                                                $fileName = time().'-'.$files['business_logo']['name'][$i];
                                                $allowedExts = array("gif", "jpeg", "jpg", "png");
                                                $image_data=explode(".", $files['business_logo']['name'][$i]);

                                                $extension = end($image_data);

                                                $_FILES['business_logo']['name']        = $fileName;
                                                $_FILES['business_logo']['type']        = $files['business_logo']['type'][$i];
                                                $_FILES['business_logo']['tmp_name']    = $files['business_logo']['tmp_name'][$i];
                                                $_FILES['business_logo']['error']       = $files['business_logo']['error'][$i];
                                                $_FILES['business_logo']['size']        = $files['business_logo']['size'][$i]; 
                                            
                                                if ((($_FILES["business_logo"]["type"] == "image/gif")  || ($_FILES["business_logo"]["type"] == "image/jpeg")  || ($_FILES["business_logo"]["type"] == "image/jpg")  || ($_FILES["business_logo"]["type"] == "image/png") && in_array($extension, $allowedExts))) {
                                                 

                                                    // File upload configuration
                                                    $uploadPath = './uploads/seller/';

                                                    $config = [];
                                                    $config['upload_path']      = $uploadPath;
                                                    $config['allowed_types']    = 'gif|jpg|jpeg|png';
                                                    //$config['min_height']       = 296;
                                                    //$config['min_width']        = 804;
                                                    $config['overwrite']        = FALSE;
                                                    $config['file_name']        = $fileName;  

                                                    $this->upload->initialize($config);

                                                    if ( ! $this->upload->do_upload('business_logo')){
                                                        $resposne=["status"=>"1","message"=>"Please Select jpg/jpeg/png/jpeg Image.",'error'=> $this->upload->display_errors(),"field"=>'image',"not_inseted"=>""];
                                                    }else{
                                                        $dataInfo = $this->upload->data();

                                                        if(!empty($dataInfo)){
                                                            // Insert files data into the database
                                                            $sellerImg = [
                                                                'seller_id' =>  $id,
                                                                'image'     =>  $dataInfo['file_name'],
                                                            ];
                                                            
                                                            $insert = $this->sellerModel->insertSellerImg($sellerImg);
                                                        } else {
                                                            $not_inseted[]=$dataInfo[$i-1]['file_name'];
                                                        } 
                                                    }
                                                }else{
                                                    $not_inseted[]= $_FILES['business_logo']['name'];
                                                }
                                            }

                                            if($insert){
                                                $resposne=["status"=>"0","message"=>"Seller Profile Updated successfully.","field"=>'','not_inseted'=>implode(",", $not_inseted)];
                                            }else{
                                                $resposne=["status"=>"1","message"=>"Seller Profile Updated Failed.","field"=>'','not_inseted'=>implode(",", $not_inseted)];
                                            }                                            
                                        }
                                        else {
                                            $resposne=["status"=>"0","message"=>"Seller Profile Updated successfully.","field"=>'',"not_inseted"=>""];
                                        }
                                    }
                                    else {
                                        $resposne=["status"=>"1","message"=>"Seller Profile Updated failed.Please try again some time.","field"=>'',"not_inseted"=>""];  
                                    }
                                }
                            }
                        }
                    }
                }
            }                                        
            else{
                $resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
            }
        }catch (Exception $e) {
                $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'user_name','error'=>$e];

        }

        echo json_encode($resposne);
    }*/

    public function doSellerStatusChange(){
        $data=$this->input->post();
        //print_r($data);
        $id=base64_decode($data["id"]);
        $resposne=["status"=>"0","message"=>"","field"=>''];
        try {
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
            if(trim($this->token_id)!=''){
                $res = $this->sellerModel->updateSeller(["active"=>$data["status"], 'update_by' => 1, 'update_on' => date('Y-m-d H:i:s')],$id);
              
                if(!$res){
                    $resposne=["status"=>"1","message"=>"Seller change failed.Please try again after some time."];
                    // $resposne=["status"=>"0","message"=>"Buyer status changed successfully."];
                }
                else{
                    $cRes = $this->customerModel->updateCust(["active"=>$data["status"], 'update_by' => 1, 'update_on' => date('Y-m-d H:i:s')],$res);

                    if ($cRes) {
                        $resposne=["status"=>"0","message"=>"Seller status changed successfully."];
                    }
                    else {
                        $resposne=["status"=>"1","message"=>"Customer Status Change failed.Please try again after some time."];
                    }
                }
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
            }   
        }catch (Exception $e) {
                $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'status'];

        }

        echo json_encode($resposne);
    }

}
?>