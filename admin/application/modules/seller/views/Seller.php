<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="clearfix mb-10">
        <div class="pull-left">
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Seller Profile</a></li>
                    <li class="breadcrumb-item active" aria-current="page">List</li>
                </ol>
            </nav>
        </div> 
        <!-- <div class="pull-right">
            <a class="btn btn-outline-primary" href="<?php //echo base_url('seller/addSeller')?>">Add New</a>            
        </div> -->
    </div>

    <div class="container-fluid bg-light"> 
        <div class="row">
            <div class="col-lg-12">

                <form id="catsrch" action="<?php echo base_url('seller')?>">
                    <input type="hidden" name="per_page_search" value="<?php echo $page; ?>">

                    <div class="row align-items-center justify-content-center">
                        <div class="col-md-3 pt-3">
                            <div class="form-group ">
                                <select name="seller_id" id="seller_id" class="form-control">
                                    <option value=""> -- select Seller -- </option>

                            <?php

                                foreach ($seller as $cust) {
                                    
                            ?>

                                    <option value="<?php echo base64_encode($cust["id"]); ?>" <?php echo (isset($seller_search) && isset($seller_search["cust_id"]) && ($seller_search["cust_id"] == $cust["id"]) )? 'selected':''; ?> > <?php echo $cust["name"]; ?> </option>

                            <?php

                                }

                            ?>

                        
                                </select>
                            </div>
                        </div>
                        <!-- <div class="col-md-3 pt-3">
                            <div class="form-group">
                                <input type="text" name="business_name" id="business_name" value="<?php //echo (isset($seller_search["business_name"]))?$seller_search["business_name"]:'';?>" placeholder="Seller Name" class="form-control">
                            </div>
                        </div> -->
                        <div class="col-md-3 pt-3">
                            <div class="form-group">
                                <input type="text" name="city" id="city" value="<?php echo (isset($seller_search["city"]))?$seller_search["city"]:'';?>" placeholder="Seller City" class="form-control">
                            </div>
                        </div>
                       
                        <div class="col-md-3">
                            <a class="btn btn-outline-primary" href="<?php echo base_url('seller')?>"><i class="fa fa-refresh" aria-hidden="true"></i> Reset</a> 
                                
                            <input type="submit" name="search" value="Search" class="btn btn-outline-primary">
                        </div>
                    </div>
                    
                </form>
            </div>
        </div>
    </div>


    <div class="row pt-2">
        
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="data-table stripe hover no-wrap" style="font-size: 14px;">
            <thead>
                <tr class="titel_bar">
                    <th class="table-plus datatable-nosort" width="2%">#</th>
                    <th class="table-plus datatable-nosort" width="10%">Seller</th>
                    <th class="table-plus datatable-nosort" width="10%">Email</th>
                    <th class="table-plus datatable-nosort" width="10%">Bussiness</th>                    
                    <th class="table-plus datatable-nosort" width="8%">City</th>
                    <th class="table-plus datatable-nosort" width="10%">Contact</th>
                    <th class="table-plus datatable-nosort" width="15%"><center>Status</center></th>
                    <th class="table-plus datatable-nosort" width="10%"><center>Local<br>Supplier</center></th>
                    <th class="table-plus datatable-nosort" width="10%" ><center>Action</center></th>
                </tr>
            </thead>
            <tbody>

            <?php
                
                if(!empty($seller)){
                    $i=0;
                    foreach($seller as $val){
                        $i++;
            ?>
            
                <tr class="">
                    <td class="table-plus"><?php echo $i;?></td>
                    <td class="table-plus"><?php echo $val["seller"];?></td>
                    <td class="table-plus"><?php echo $val["email"];?></td>
                    <td class="table-plus"><?php echo $val["business_name"];?></td>
                    <td class="table-plus"><?php echo $val["city"];?></td>
                    <td class="table-plus"><?php echo $val["contact_number"];?></td>
                    
                    <td class="table-plus">
                        <select name="status" id="status" onchange="doSellerStatusChange(this.value, '<?php echo base64_encode($val["seller_id"]);?>')" class="form-control">
                            <option value="1" <?php echo (isset($val) && isset($val["active"]) && $val['active'] == 1)?'selected':''; ?>> Active </option>

                            <option value="0" <?php echo (isset($val) && isset($val["active"]) && $val['active'] == 0)?'selected':''; ?>> InActive </option>
                        </select>
                    </td>
                    <td class="table-plus">
                <select name="status" id="status" onchange="doLclSuppStatusChange(this.value,'<?php echo base64_encode($val["id"]);?>')" class="form-control">
                                <option value="1" <?php echo (isset($val) && isset($val["displayLocal"]) && $val['displayLocal'] == 1)?'selected':''; ?>> Yes </option>

                                <option value="0" <?php echo (isset($val) && isset($val["displayLocal"]) && $val['displayLocal'] == 0)?'selected':''; ?>> No </option>
                            </select>
                        </td>
                    <td class="table-plus text-center">
                        <a class="btn btn-outline-primary seller" href="#" role="button" data-toggle="modal" data-id="<?php echo base64_encode($val['seller_id']) ?>">
                            <i class="fa fa-eye"></i>
                        </a>
                    </td>
                    <!-- <td class="table-plus" style="text-align:center;">

                        <div class="dropdown">
                            <a class="btn btn-outline-primary dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                <i class="fa fa-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="javascript:void(0);" style=" font-size: 15px;" onclick="doSellerEdit('<?php //echo base64_encode($val["id"]);?>')" >
                                    <i class="fa fa-edit" aria-hidden="true"></i> 
                                    Edit
                                </a>
                            </div>
                        </div>

                    </td> -->
                </tr>
                        
            <?php
                    }
                }else{
            ?>

                <tr class="">
                    <td colspan="8"><center>No Seller Record Found</center></td>
                </tr>   
                    
            <?php
                }
            ?>
           
                 
            </tbody>
        </table>
        
        <div class="col-sm-12">
            <div style="float: right;"><?php echo $links; ?></div>
        </div>
    </div>
</div>



<!-- Modal -->

<div class="modal fade admin_modal" id="viewModal">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h4 class="modal-title"> <span id="business_name"></span> </h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="row" style="padding-bottom: 20px;">
                    <div class="col-lg-4">
                        <label style="font-weight: 400;">By : </label>
                        <br>
                        <span id="seller"></span>
                    </div>
                    <div class="col-lg-4">
                        <label style="font-weight: 400;">Contact Person: </label>
                        <br>
                        <span id="contact_name"></span>
                    </div>
                    <div class="col-lg-4">
                        <label style="font-weight: 400;">Contact : </label>
                        <br>
                        <span id="contact_number"></span>
                    </div>                    
                </div>

                <div class="row" style="padding-bottom: 20px;">
                    <div class="col-lg-4">
                        <label style="font-weight: 400;">City : </label>
                        <br>
                        <span id="city"></span>
                    </div>
                    <div class="col-lg-4">
                        <label style="font-weight: 400;">State : </label>
                        <br>
                        <span id="state"></span>
                    </div>                    
                </div>

                <div class="row" style="padding-bottom: 20px;">
                    <div class="col-lg-12">
                        <label style="font-weight: 400;">Address : </label>
                        <br>
                        <span id="address"></span>
                    </div>
                    
                    <!-- <div class="col-lg-3">
                        <label style="font-weight: 400;">Latitude : </label>
                        <br>
                        <span id="lat"></span>
                    </div>
                    <div class="col-lg-3">
                        <label style="font-weight: 400;">Longitude : </label>
                        <br>
                        <span id="long"></span>
                    </div> -->
                </div>

                <div class="row" style="padding-bottom: 20px;" id="imgRow">
                    <label>Image</label>
                    <div class="col-lg-12" id="imgCol"></div>
                </div>

                <div class="row" style="padding-bottom: 20px;" id="certRow">
                    <label>Certificates</label>
                    <div class="col-lg-12" id="certCol"></div>
                </div>

                <div class="row" style="padding-bottom: 20px;" id="docRow">
                    <label>Documents</label>
                    <div class="col-lg-12" id="docCol"></div>
                </div>
            </div>

        </div>
    </div>
</div>


<!-- Modal Ends -->


<style type="text/css">
    
.zoom:hover {
  -ms-transform: scale(4.0); /* IE 9 */
  -webkit-transform: scale(4.0); /* Safari 3-8 */
  transform: scale(4.0); 
  position: relative;
  z-index: 1;
}

</style>


<script type="text/javascript">
    
    $('.seller').click(function(){
        var base_url = $('#base_url').val();
        var iid = $(this).data('id');

        // console.log(iid);

        $.ajax({
            url: base_url + 'seller/getById',
            type: 'POST',
            data: {id: iid},
            dataType: 'JSON',
            success: function(response) {
                console.log(response.data.image);

                $('#business_name').html(response.data.business_name);
                $('#address').html(response.data.address);
                $('#city').html(response.data.city);
                $('#contact_name').html(response.data.contact_name);
                $('#contact_number').html(response.data.contact_number);
                $('#seller').html(response.data.seller);
                $('#lat').html(response.data.lat);
                $('#long').html(response.data.long);
                $('#state').html(response.data.state);

                $.each(response.data.image, function(key, val) {
                    if (val.type == 1) {
                        // Image
                        $('#imgRow').show();
                        $('#imgCol').html('');

                        $('#imgCol').append("<div style='display: inline-block; padding-right: 7px;' > <img src='" + base_url + "uploads/seller/" + val.image + "' height='160' width='140'  style='border: 2px dotted #ccc; border-radius: 2px;'/> </div>");

                    }
                    else if (val.type == 2) {
                        // Certificate
                        $('#certRow').show();
                        $('#certCol').html('');

                        if (val.ext == 'docx') {
                            $('#certCol').append("<div style='display: inline-block; padding-right: 7px;' ><a href='" + base_url + "uploads/seller/" + val.image + "' target='__blank'> <img src='" + base_url + "assets/images/docx.png' height='100' width='60' style='border: 2px dotted #ccc; border-radius: 2px;'/> </a> </div>");
                        }
                        else if (val.ext == 'pdf') {
                            $('#certCol').append("<div style='display: inline-block; padding-right: 7px;' ><a href='" + base_url + "uploads/seller/" + val.image + "' target='__blank'> <img src='" + base_url + "assets/images/pdf.png' height='100' width='60' style='border: 2px dotted #ccc; border-radius: 2px;'/> </a> </div>");
                        }
                        else {
                            $('#certCol').append("<div style='display: inline-block; padding-right: 7px;' > <img src='" + base_url + "uploads/seller/" + val.image + "' height='160' width='140'  style='border: 2px dotted #ccc; border-radius: 2px;'/> </div>");
                        }


                        // $('#certCol').append("<div style='display: inline-block; padding-right: 7px;' > <object data='" + base_url + "uploads/seller/" + val.image + "' width='100%' height='300' /></div>");
                    }
                    else {
                        // Document
                        $('#docRow').show();
                        $('#docCol').html('');

                        if (val.ext == 'docx') {
                            $('#docCol').append("<div style='display: inline-block; padding-right: 7px;' ><a href='" + base_url + "uploads/seller/" + val.image + "' target='__blank'> <img src='" + base_url + "assets/images/docx.png' height='100' width='60' style='border: 2px dotted #ccc; border-radius: 2px;'/> </a> </div>");
                        }
                        else if (val.ext == 'pdf') {
                            $('#docCol').append("<div style='display: inline-block; padding-right: 7px;' ><a href='" + base_url + "uploads/seller/" + val.image + "' target='__blank'> <img src='" + base_url + "assets/images/pdf.png' height='100' width='60' style='border: 2px dotted #ccc; border-radius: 2px;'/> </a> </div>");
                        }
                        else {
                            $('#docCol').append("<div style='display: inline-block; padding-right: 7px;' > <img src='" + base_url + "uploads/seller/" + val.image + "' height='160' width='140'  style='border: 2px dotted #ccc; border-radius: 2px;'/> </div>");
                        }

                        // $('#docCol').append("<div style='display: inline-block; padding-right: 7px;' > <img src='" + base_url + "uploads/seller/" + val.image + "' height='160' width='140'  style='border: 2px dotted #ccc; border-radius: 2px;'/> </div>");
                    }
                });

                $('#viewModal').modal('show');
            }
        });


    });


</script>




<?php
    if(isset($custom_js)){
        foreach($custom_js as $val){
            ?>
            <script src="<?php echo base_url();?>assets/js/custom_js/<?php echo $val;?>"></script>
            <?php
        }
    }
    
?>
  


