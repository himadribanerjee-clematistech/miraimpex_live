<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="clearfix mb-10">
        <div class="pull-left">
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Seller Profile</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Add New Seller</li>
                </ol>
            </nav>
        </div> 
        <div class="pull-right">
            <a class="btn btn-outline-primary" href="<?php echo base_url()?>seller">Back</a>            
        </div>
    </div>

    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                <form id="addSellerfrm" method="post" action="javascript:void(0);" autocomplete="off" enctype="multipart/form-data">

                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-3">
                                <label label-for='name'>Customer <span class="mandatory_label">*</span></label>
                                <select name="customer_id" id="customer_id" class="form-control">
                                        <option value=""> -- select -- </option>

                                <?php

                                    foreach ($customer as $cust) {
                                        
                                ?>

                                        <option value="<?php echo base64_encode($cust["id"]); ?>"> <?php echo $cust["name"]; ?> </option>

                                <?php

                                    }

                                ?>

                                    
                                </select>
                            </div>
                            <div class="col-lg-3">
                                <label label-for='name'>Business Name <span class="mandatory_label">*</span></label>
                                <input type="text" name="business_name" id="business_name" value="" placeholder="Business Name" class="form-control">
                            </div>
                            <div class="col-lg-3">
                                <label label-for='address'>Address <span class="mandatory_label">*</span></label>
                                <textarea name="address" id="address" placeholder="Enter Address" class="form-control" style="height: auto;"></textarea>
                            </div>
                            <div class="col-lg-3">
                                <label label-for='state'>State <span class="mandatory_label">*</span></label>
                                <input type="text" name="state" id="state" value="" placeholder="Buyer state" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-3">
                                <label label-for='city'>City <span class="mandatory_label">*</span></label>
                                <input type="text" name="city" id="city" value="" placeholder="Buyer City" class="form-control">
                            </div>
                            <div class="col-lg-3">
                                <label label-for='latitude'>Latitude <span class="mandatory_label">*</span></label>
                                <input type="text" name="lat" id="lat" value="" placeholder="Buyer Latitude" class="form-control">
                            </div>
                            <div class="col-lg-3">
                                <label label-for='longitude'>Longitude <span class="mandatory_label">*</span></label>
                                <input type="text" name="long" id="long" value="" placeholder="Buyer Longitude" class="form-control">
                            </div>
                            <div class="col-lg-3">
                                <label label-for='contact_name'>Contact Name <span class="mandatory_label">*</span></label>
                                <input type="text" name="contact_name" id="contact_name" value="" placeholder="Seller Contact Name" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-3">
                                <label label-for='contact_number'>Contact Number <span class="mandatory_label">*</span></label>
                                <input type="text" name="contact_number" id="contact_number" value="" placeholder="Seller Contact Number" class="form-control">
                            </div>
                            <div class="col-lg-3">
                                <label label-for='name'>Agent ? <span class="mandatory_label">*</span></label>
                                <select name="agent_YN" id="agent_YN" class="form-control">
                                    <option value="1"> Yes </option>
                                    <option value="0"> No </option>
                                </select>
                            </div>
                            <div class="col-lg-6">
                                <label label-for='logo'>Images <span class="mandatory_label">*</span></label>

                                <br>

                                <div id="testdv">
                                    <div id="filediv_0" class="testcls">
                                        <input type="file" name="business_logo[]" id="business_logo"/>
                                    </div>
                                </div>

                                <!-- <div class="imgShow"></div> --> 
                            </div>
                        </div>
 
                        <div class="row">                            
                            <div class="col-lg-12 text-right">
                                <input type="submit" value="Add" name="submit" class="btn btn-outline-primary">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    
    var abc = 0;      // Declaring and defining global increment variable.

    $(document).ready(function() {
        
        // Following function will executes on change event of file input to select different file.
        $('body').on('change', '#business_logo', function() {
            if (this.files && this.files[0]) {
                abc += 1; // Incrementing global variable by 1.
                var z = abc - 1;
                $(this).find('#previewimg' + z).remove();

                $('#testdv').after("<div style='display: inline-block; padding: 14px;'><div id='abcd_" + abc + "' class='abcd'><img id='previewimg" + abc + "' src='' style='width: 139px; height: 89px;' /><div style='bottom: 0; width: 100%; height: 24px; line-height: 9px; background: #3b485a; opacity: .85;'><a class='remove' href='javascript:void(0);' onclick='return false;' style='display: inline-block; width: 100%; height: 24px; background: url(../assets/images/uploader-icons.png) no-repeat center 3px; text-align: center; background-position: center -37px;'></a></div></div></div>");
            
                var reader = new FileReader();
                reader.onload = imageIsLoaded;
                reader.readAsDataURL(this.files[0]);
                $(this).hide();

                $("#abcd_" + abc).click(function(){
                    $(this).parent().remove();
                    var id=$(this).attr("id");
                    var id_arr=id.split("_");
                    var no =(parseInt(id_arr[1])-1);
                    //console.log(abc,id_arr[1],no);
                    if(id_arr.length==2){
                        $("#filediv_" + no).val("").trigger("change");
                        $("#filediv_" + no).remove();
                    }         
                   
                });



                $("#testdv").prepend($("<div id='filediv_"+abc+"' class ='testcls' />").fadeIn('slow').prepend($("<input/>", {
                    name: 'business_logo[]',
                    type: 'file',
                    id: 'business_logo'
                }), $("<br/><br/>")));
                
            }
        });

        // To Preview Image
        function imageIsLoaded(e) {
            $('#previewimg' + abc).attr('src', e.target.result);
        };


        $("#addSellerfrm").submit(function(e) {
            e.preventDefault();
            toastr.clear();

            var base_url=$("#base_url").val();
            var formData = new FormData(this);

            $('.loading').removeClass("hidden");
          
            $.ajax({
                url :base_url+"seller/doAddNew",
                type : 'POST',
                data : formData,
                processData: false,  // tell jQuery not to process the data
                contentType: false,  // tell jQuery not to set contentType
                success : function(result) {
                    //console.log(result);
                    $('.loading').addClass("hidden");
                    result=JSON.parse(result);
                    if(result.status==0){                       
                        if(result["not_inseted"]!=''){
                            var input = $("#business_logo");
                            input.replaceWith(input.val('').clone(true));
                            $("#imgShow").html("");
                            toastr.error(result["not_inseted"]+" image upload failed .Please upload valid image file.");
                        }else{
                            toastr.success(result["message"]);
                            setTimeout(
                              function() 
                              {
                                window.location=base_url+'seller';
                            
                              }, 1000);
                        }
                        
                    }else{
                        if(result["not_inseted"]!=''){
                            var input = $("#business_logo");
                            input.replaceWith(input.val('').clone(true));
                            $("#imgShow").html("");
                            toastr.error(result["not_inseted"]+" image upload failed .Please upload valid image file.");
                        }else{
                            toastr.error(result["message"]);
                            if(result["field"]=='to_login'){
                                window.location=base_url;
                            }else{
                                $("#"+result["field"]).focus();
                            }
                        }                 
                        
                    }
                }
            });
        });

    });

</script>