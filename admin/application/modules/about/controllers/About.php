<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class About extends CI_Controller {
	protected $token_id='';
	
    public function __construct() {
    	parent::__construct();
    	$this->token_id=get_cookie('token_id'); 
    	if(trim($this->token_id)==''){
    		redirect("login");
    	}
        $this->load->model("aboutModel", 'aM');
        $this->load->model("userModel");
    }
    public function index()
	{
		try {
           $user_details = $this->userModel->getUserDetailsbyId(trim($this->token_id) );
            
            if(empty($user_details)){
                    $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                    delete_cookie('token_id'); 
                     delete_cookie('useName'); 
                    echo '<script>
                        alert("Unexpected server error.Please try again some time.");
                            window.location="'.base_url().'login";
                    </script>';
            }
            $user_details=current($user_details);
            $data=[]; 
            $data["menu"]="about";
            $data["useName"]=$user_details["Name"];
            $data["custom_js"]=['about.js'];
            $data["vendor_js"]=['ckeditor.js'];
            set_cookie('useName',$user_details["Name"],'3600');
			
			$data["about_data"]=$this->aM->getAbout();

            $data['view_file'] = 'About';
            view($data);
           
        } catch (Exception $e) {
            $resposne=["status"=>"1","e"=>$e,"message"=>"Unexpected server error.Please try again some time.","field"=>'user_name'];
        }   
	}

    public function doAddAbout(){
        try {
            $data=$this->input->post();
            $resposne=["status"=>"0","message"=>"", "field"=>''];
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));

            if(trim($this->token_id)!=''){
                if(isset($data["name"]) && trim($data["name"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter About Title.","field"=>'name'];
                }
                elseif(isset($data["details"]) && trim($data["details"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter About Details.","field"=>'details'];
                }
                else{

                    $string = $data['name'];
                    $searchStringComma = ',';
                    $searchStringHyphen = '-';
                    $new_string = '';

                    if( strpos($string, $searchStringComma) !== false ) {
                        $new_string = str_replace(","," ", $string);
                    }

                    if ($new_string != '') { //If New String is not Blank
                        if( strpos($new_string, $searchStringHyphen) !== false ) {
                          $new_string = str_replace("-"," ", $new_string);
                          $new_string = preg_replace('!\s+!', ' ', $new_string);
                          
                          $slug = strtolower(str_replace(" ","-", $new_string));
                        }
                        else {
                            $new_string = preg_replace('!\s+!', ' ', $new_string);
                            $slug = strtolower(str_replace(" ","-", $new_string));
                        }
                    }
                    else { //If New String is Blank
                        if( strpos($string, $searchStringHyphen) !== false ) {
                          $new_string = str_replace("-"," ", $string);
                          $new_string = preg_replace('!\s+!', ' ', $new_string);
                          
                          $slug = strtolower(str_replace(" ","-", $new_string));
                        }
                        else {
                            $new_string = preg_replace('!\s+!', ' ', $string);
                            $slug = strtolower(str_replace(" ","-", $new_string));
                        }
                    }

                    $formData = [
                        'name' => $data['name'],
                        'details' => $data['details'],
                        'slug' => $slug
                    ];
                    
                    $res = $this->aM->updateAbout($formData, $data['code']);

                    if($res){
                        $resposne=["status"=>"0","message"=>"About Details Updated successfully."];
                    }
                    else{                                
                        $resposne=["status"=>"1","message"=>"About Details Updation failed.Please try again after some time."];                                
                    }
                }
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied."];
            }
        } catch (Exception $e) {
            $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time."];

        }

        echo json_encode($resposne);
    }

}
?>