<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class AboutModel extends CI_Model {
	protected $cmsTable = 'cms';
	
	public function __construct() {
        parent::__construct();
    }
    
    public function getAbout(){
    	try {
    		$this->db->db_debug = false;
    		$this->db->select("id, name, details, active, code");
    		$this->db->from($this->cmsTable);
    		$this->db->where('code', 3);    	
    		$this->db->where('active', 1);    	
 			$query=$this->db->get();

	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	return $query->row();
	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
    }	

    public function updateAbout($data, $where=''){

    	try {
    	 	$this->db->db_debug = False;

    	 	if ($where != '') {
    	 		$res = $this->db->update($this->cmsTable, $data, ['code' => $where]);
    	 	}
    	 	else {
    	 		$data['code'] = 3;
    			$res = $this->db->insert($this->cmsTable, $data);
    	 	}    	 	

	    	$db_error = $this->db->error();
	        if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }
	        else {
	        	if ($res) {
	        		return True;
	        	}
	        	else {
	        		return False;
	        	}
	        }
	        
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }
    }
}

?>