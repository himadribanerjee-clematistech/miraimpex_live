<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="clearfix mb-10">
        <div class="pull-left">
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Species</a></li>
                    <li class="breadcrumb-item active" aria-current="page">List</li>
                </ol>
            </nav>
        </div>
        <div class="pull-right">
            <a class="btn btn-outline-primary" href="<?php echo base_url('species/addspecies')?>">Add New</a>            
        </div>
    </div>

    <div class="container-fluid bg-light"> 
        <div class="row">
            <div class="col-lg-12">

                <form id="catsrch" action="<?php echo base_url('species')?>">
                    <input type="hidden" name="per_page_search" value="<?php echo $page; ?>">

                    <div class="row align-items-center justify-content-center">
                        <div class="col-md-4 pt-3">
                            <div class="form-group ">
                                <input type="text" name="speciesName" id="species_name" value="<?php echo (isset($cat_search["species_name"]))?$cat_search["species_name"]:'';?>" placeholder="Species Name" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-4 pt-3">
                            <div class="form-group ">
                                <select name="category" id="category" onchange="dogetSubCatByCat(this.value)" class="form-control">
                                    <option value=""> -- select Category-- </option>

                                <?php

                                    foreach ($cat_details as $cat) {
                                        
                                ?>

                                    <option value="<?php echo base64_encode($cat["id"]); ?>"
                                        <?php 
                                            echo (isset($species_search) && isset($species_search["category_id"]) && ($species_search["category_id"] == $cat["id"]) )? 'selected':'';
                                        ?>

                                    > <?php echo $cat["category_name"]; ?> </option>

                                <?php

                                    }

                                ?>

                                </select>
                            </div>
                        </div>
                        <div class="col-md-4 pt-3">
                            <div class="form-group ">
                                <select name="subcategory" id="subcategory" class="form-control">
                                    <option value=""> -- select Sub Category-- </option>

                                <?php

                                    foreach ($subcat_data as $subcat) {
                                        
                                ?>

                                    <option value="<?php echo base64_encode($subcat["id"]); ?>"
                                        <?php 
                                            echo (isset($species_search) && isset($species_search["subcategory_id"]) && ($species_search["subcategory_id"] == $subcat["id"]) )? 'selected':'';
                                        ?>

                                    > <?php echo $subcat["category_name"]; ?> </option>

                                <?php

                                    }

                                ?>

                                </select>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-3 pt-3">
                            <div class="form-group ">
                                <select id="actve" name="status" class="form-control">
                                    <option value="">Select Status</option>
                                    <option value="1" <?php echo (isset($species_search) && isset($species_search["actve"]) &&  $species_search["actve"]==1)?'selected':'';?>>Active</option>
                                    <option value="0"  <?php echo (isset($species_search) && isset($species_search["actve"]) &&  $species_search["actve"]==0)?'selected':'';?>>InActive</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3 pt-3">
                            <a class="btn btn-outline-primary" href="<?php echo base_url('species')?>"><i class="fa fa-refresh" aria-hidden="true"></i> Reset</a> 
                                
                            <input type="submit" name="search" value="Search" class="btn btn-outline-primary">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="row pt-2">
        
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="data-table stripe hover no-wrap" style="font-size: 14px;">

            <thead>
                <tr class="titel_bar">
                    <th class="table-plus datatable-nosort" width="5%">Sl No.</th>
                    <td class="table-plus datatable-nosort" width="15%">Species Name</th>
                    <td class="table-plus datatable-nosort" width="15%">Category</th>
                    <td class="table-plus datatable-nosort" width="15%">Sub Category</th>
                    <td class="table-plus datatable-nosort" width="10%">Item</th>
                    <td class="table-plus datatable-nosort" width="16%">Image</th>
                    <td class="table-plus datatable-nosort" width="40%">Status</th>
                    <td class="table-plus datatable-nosort" width="20%" ><center>Action</center></th>
                </tr>
            </thead>
            <tbody>

            <?php
                if(!empty($species_data)){
                    $i=0;
                    foreach($species_data as $val){
                        $i++;
                        ?>
                         <tr class="">
                            <td ><?php echo $i;?></td>  
                            <td >
                                <?php echo $val["species_name"];?>           
                            </td>
                            <td >
                                <?php echo $val["category_name"];?>           
                            </td>   
                             <td >
                                <?php echo $val["subcat_name"];?>           
                            </td>
                            <td >
                                <?php echo $val["item_name"];?>           
                            </td>
                           
                            <td >
                                <img src="<?php echo (isset($val) && ($val["species_image"] != '') )?base_url('uploads/species/' . $val["species_image"]):base_url('assets/images/no-item-image.png'); ?>" width='100' height='100'>


                                          
                            </td>
                            <td>

                               <select id="actve" name="status" onchange="doSpeciesStatusChange(this.value,'<?php echo base64_encode($val["id"]);?>');" class="form-control">
                                    <option value="1" <?php echo ($val["active"]==1)?'selected':'';?>>Active</option>
                                     <option value="0"  <?php echo ($val["active"]==0)?'selected':'';?>>InActive</option>
                                </select>   

                            </td>
                            <td class="table-plus" style="text-align:center;">

                                <div class="dropdown">
                                    <a class="btn btn-outline-primary dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                        <i class="fa fa-ellipsis-h"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item" href="javascript:void(0);" style=" font-size: 15px;" onclick="dospeciesEdit('<?php echo base64_encode($val["id"]);?>')" >
                                            <i class="fa fa-edit" aria-hidden="true"></i> 
                                            Edit
                                        </a>
                                    </div>
                                </div>

                            </td>

                        </tr>
                        <?php
                    }
                }else{
                    ?>
                     <tr class="">
                         <td colspan="7">No Record Found</td>   

                     </tr>  
                    <?php
                }
            ?>
           
                 
            </tbody>
    </table>
    <div class="col-sm-12">
            <div style="float: right;"><?php echo $links; ?></div>
        </div>
</div>
<?php
    if(isset($custom_js)){
        foreach($custom_js as $val){
            ?>
            <script src="<?php echo base_url();?>assets/js/custom_js/<?php echo $val;?>"></script>
            <?php
        }
    }
    
?>
  