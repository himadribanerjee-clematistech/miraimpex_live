<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="clearfix mb-10">
        <div class="pull-left">
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Species</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Add New Species</li>
                </ol>
            </nav>
        </div>  
        <div class="pull-right">
            <a class="btn btn-outline-primary" href="<?php echo base_url()?>species">Back</a>            
        </div> 
    </div>

    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">

                <form id="addspeciesfrm" method="post" action="javascript:void(0);" autocomplete="off" enctype="multipart/form-data" onsubmit="return addSpeciesDetails();">
                    
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-4">
                                <label label-for='name'>Species Name <span class="mandatory_label">*</span></label>
                                <input type="text" name="species_name" id="species_name" value="" placeholder="Species Name" class="form-control">
                            </div>
                            <div class="col-lg-4">
                                <label label-for='name'>Category <span class="mandatory_label">*</span></label>
                                <select name="category" id="category" onchange="dogetSubCatByCat(this.value)" class="form-control">
                                    <option value=""> -- Select Category-- </option>

                                    <?php

                                        foreach ($cat_details as $cat) {
                                            
                                    ?>

                                        <option value="<?php echo base64_encode($cat["id"]); ?>"> <?php echo $cat["category_name"]; ?> </option>

                                    <?php

                                        }

                                    ?>

                                </select>
                            </div>
                            <div class="col-lg-4">
                                <label label-for='name'>Sub Category <span class="mandatory_label">*</span></label>
                                <select name="subcategory" id="subcategory"  onchange="dogetItemByCatSubCat(this.value)" class="form-control">
                                    <option value=""> -- Select Sub Category-- </option>
                                    <?php

                                        foreach ($subcat_data as $cat) {
                                            
                                    ?>

                                        <option value="<?php echo base64_encode($cat["id"]); ?>"> <?php echo $cat["category_name"]; ?> </option>

                                    <?php

                                        }

                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-4">
                                <label label-for='name'>Item <span class="mandatory_label">*</span></label>
                                <select name="item_id" id="item_id" class="form-control">
                                    <option value=""> -- Select Item-- </option>
                                     <?php

                                        foreach ($item_data as $item) {
                                            
                                    ?>

                                        <option value="<?php echo $item["id"]; ?>"> <?php echo $item["item_name"]; ?> </option>

                                    <?php

                                        }

                                    ?>
                                </select>
                            </div>
                            <div class="col-lg-4">
                                <label label-for='name'>Species Image <span class="mandatory_label">*</span></label>
                                <input type="file" name="species_image" id="species_image" onchange="loadFile()">

                                <br><br><br>

                                <img src="<?php echo base_url('assets/images/no-item-image.png'); ?>" height="150" width="150"  style="border: 2px dotted #ccc; border-radius: 2px;" id="chImg"/>

                                <script type='text/javascript'>
                                    function loadFile(){
                                        
                                        var imgfilename = document.getElementById('chImg');

                                        imgfilename.src = URL.createObjectURL(event.target.files[0]);
                                    };
                                </script>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-12 text-right">
                                <input type="submit" value="Add" name="submit" class="btn btn-outline-primary">
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>

</div>


<?php
    if(isset($custom_js)){
        foreach($custom_js as $val){
            ?>
            <script src="<?php echo base_url();?>assets/js/custom_js/<?php echo $val;?>"></script>
            <?php
        }
    }
    
?>
  