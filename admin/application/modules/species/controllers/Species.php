<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class Species extends CI_Controller {
    protected $token_id='';
    protected $per_page=10;
    public function __construct() {
        parent::__construct();
        $this->token_id=get_cookie('token_id'); 
        if(trim($this->token_id)==''){
            redirect("login");
        }
        $this->load->model("speciesModel");
        $this->load->model("itemModel");
        
        $this->load->model("userModel");
        $this->load->model('categoryModel');
        $this->load->model("subcategoryModel");
        $this->load->helper("pagination");
        $this->load->library("pagination");
         $this->load->model("settingsModel", 'sM');
        $settings_data=$this->sM->getSettings();
        if(isset($settings_data->per_page)){
         $this->per_page=$settings_data->per_page;
        }

        //echo $this->per_page; die(); //5
    }
    public function index()
    {
        try {
           $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            
            if(empty($user_details)){
                    $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                    delete_cookie('token_id'); 
                     delete_cookie('useName'); 
                    echo '<script>
                        alert("Unexpected server error.Please try again some time.");
                            window.location="'.base_url().'login";
                    </script>';
            }
            $user_details=current($user_details);
            $data=[]; 
            $data["menu"]="species";
            $data["useName"]=$user_details["Name"];
            $data["custom_js"]=['species.js'];
            set_cookie('useName',$user_details["Name"],'3600');
            $data['cat_details'] = $this->categoryModel->getCatList(["active"=>1]);

           
            //search 
            $search_input=$this->input->get();
            //print_r($search_input); die();

            $species_search=[];
            $data["subcat_data"]=[];
            if(isset($search_input["speciesName"]) && trim($search_input["speciesName"])!=''){
                $species_search["species_name"] = $search_input["speciesName"];

            }

             if(isset($search_input["category"]) && trim($search_input["category"])!=''){
                $species_search["category_id"] = base64_decode($search_input["category"]);
                 $data["subcat_data"]=$this->subcategoryModel->getsubCatList(["active"=>1],'', '', $species_search["category_id"]);
               


            }
             if(isset($search_input["subcategory"]) && trim($search_input["subcategory"])!=''){
                $species_search["subcategory_id"] = base64_decode($search_input["subcategory"]);

            }
             if(isset($search_input["status"]) && trim($search_input["status"])!=''){
                $species_search["actve"]=$search_input["status"];
                
            }
            $data["species_search"] = $species_search;
            $page=0;
            if($this->input->get("per_page")){
                $page= $this->input->get("per_page");
            }

            //echo $this->per_page; die();

            $data["page"]=$page;

            $total_row=$this->speciesModel->getspeciesCount($species_search);
         
            $data["links"] = createPageNationLink(base_url('species/index'), $total_row,$this->per_page);

            $start= (int)$page +1;
            $end = ($page+ $this->per_page >=$total_row)? $total_row : (int)$page  + $this->per_page;
            $data['result_count']='';
            if($total_row>0){
                $data['result_count']= "Showing ".$start." - ".$end." of ".$total_row." Results";
            }
            
            $data["species_data"]=$this->speciesModel->getspeciesList($species_search,$this->per_page, $page);

            //echo '<pre>'; print_r($data); die();

            $data['view_file'] = 'specie';
            view($data);
           
        } catch (Exception $e) {
            $resposne=["status"=>"1","e"=>$e,"message"=>"Unexpected server error.Please try again some time.","field"=>'user_name'];
        }   
    }
    
    public function addspecies(){
        try {
            $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            if(empty($user_details)){
                    $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                    delete_cookie('token_id'); 
                     delete_cookie('role_id'); 
                    echo '<script>
                        alert("Session expired please login again.");
                            window.location="'.base_url().'login";
                    </script>';
                    exit();
            }
            $user_details=current($user_details);
            $data=[]; 
            $data["menu"]="species";
            $data["useName"]=$user_details["Name"];
            $data["custom_js"]=['species.js'];
            set_cookie('useName',$user_details["Name"],'3600');
             $data['cat_details'] = $this->categoryModel->getCatList(["active"=>1]);
             $data['uom_details'] = $this->speciesModel->getUom();

            $data['view_file'] = 'Addspecie';
            view($data);
        } catch (Exception $e) {
            echo '<script>
                alert("Unexpected server error.Please try again some time.");
                            window.location="'.base_url().'category";
            </script>';

        }
    }
    public function doAddSpecies(){

         try {
            $data=$this->input->post();
            $resposne=["status"=>"0","message"=>"","field"=>''];
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));

            //print_r($_FILES['species_image']);

            if(trim($this->token_id)!=''){
                if(isset($data["species_name"]) && trim($data["species_name"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter Species Name.","field"=>'species_name'];
                }else if(isset($data["category"]) && trim($data["category"])==''){
                    $resposne=["status"=>"1","message"=>"Please Select Category.","field"=>'cat_name'];
                }else if(isset($data["item_id"]) && trim($data["item_id"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter Select Item.","field"=>'item_id'];
                }else{

                   $check_duplicate_speciesname = $this->speciesModel->dospeciesCheck($data["species_name"]);


                    if($check_duplicate_speciesname == 1) {
                        $resposne=["status"=>"1","message"=>"Duplicate Species Name.","field"=>'species_name'];
                    }
                    if($resposne["status"]==0){
                         $file_name='';
                        if(isset($_FILES["species_image"]["name"])){
                            $file_name=time().$_FILES["species_image"]["name"];
                            $allowedExts = array("image/gif", "image/jpeg", "image/jpg", "image/png");
                            //$image_data=explode(".", $_FILES["species_image"]["name"]);
                            $extension = $_FILES["species_image"]["type"];
                             
                            if ((($_FILES["species_image"]["type"] == "image/gif")  || ($_FILES["species_image"]["type"] == "image/jpeg")  || ($_FILES["species_image"]["type"] == "image/jpg")  || ($_FILES["species_image"]["type"] == "image/png")) && in_array($extension, $allowedExts))
                            {
                                $config['upload_path'] = './uploads/species/';
                                $config['allowed_types'] = 'jpg|png|jpeg|gif';

                                // $config['max_size'] = '200';
                                // $config['max_width'] = '800';
                                // $config['max_height'] = '800';
                                $config['file_name'] = $file_name;
                            
                                $this->load->library('upload', $config);
                                if ( ! $this->upload->do_upload('species_image')){

                                     
                                    $resposne=["status"=>"1","message"=>"Please Select  jpg/png/jpeg/gif Species Image.",'error'=> $this->upload->display_errors(),"field"=>'image'];
                              
                                }
                                else {
                                    $string = $data['species_name'];
                                    $searchStringComma = ',';
                                    $searchStringHyphen = '-';
                                    $new_string = '';

                                    if( strpos($string, $searchStringComma) !== false ) {
                                        $new_string = str_replace(","," ", $string);
                                    }

                                    if ($new_string != '') { //If New String is not Blank
                                      if( strpos($new_string, $searchStringHyphen) !== false ) {
                                          $new_string = str_replace("-"," ", $new_string);
                                          $new_string = preg_replace('!\s+!', ' ', $new_string);
                                          
                                          $slug = strtolower(str_replace(" ","-", $new_string));
                                      }
                                      else {
                                        $new_string = preg_replace('!\s+!', ' ', $new_string);
                                        $slug = strtolower(str_replace(" ","-", $new_string));
                                      }
                                    }
                                    else { //If New String is Blank
                                      if( strpos($string, $searchStringHyphen) !== false ) {
                                          $new_string = str_replace("-"," ", $string);
                                          $new_string = preg_replace('!\s+!', ' ', $new_string);
                                          
                                          $slug = strtolower(str_replace(" ","-", $new_string));
                                      }
                                      else {
                                        $new_string = preg_replace('!\s+!', ' ', $string);
                                        $slug = strtolower(str_replace(" ","-", $new_string));
                                      }
                                    }
                       
                             
                                    $species_data = [
                                        'species_name' => $data['species_name'], 
                                        'item_id' =>  base64_decode($data['item_id']), 
                                        'species_image' => $file_name,
                                        'slug' => $slug
                                    ];

                                    $id = $this->speciesModel->insertspecies($species_data);
                       
                                    if($id>0){
                                        $resposne=["status"=>"0","message"=>" Species added successfully.","field"=>'species_name'];
                                    }else{
                                        
                                        $resposne=["status"=>"1","message"=>"Species added failed.Please try again some time.","field"=>'species_name'];
                                        
                                    } 
                                }
                            
                            }
                            else {
                                $resposne=["status"=>"1","message"=>"Please Select  jpg/png/jpeg/gif Species Image.",'error'=> '',"field"=>'image'];
                            }
                        }                       
                        
                    }

                }
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
            }
        } catch (Exception $e) {
                $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'species_name'];

        }

        echo json_encode($resposne);

    }
    

    public function dospeciesEdit($id){
        $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            
        if(empty($user_details)){
                $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                delete_cookie('token_id'); 
                 delete_cookie('role_id'); 
                echo '<script>
                    alert("Session expired please login again.");
                        window.location="'.base_url().'login";
                </script>';
                exit();
        }
        $user_details=current($user_details);
        $id=base64_decode($id);
        if(trim($id)==''){
            echo '<script>
                    alert("Invalid user.");
                        window.location="'.base_url().'user";
                </script>';
                exit();
        }
        $data=[]; 
        $data["menu"]="species";
        $data["useName"]=$user_details["Name"];
        $data["custom_js"]=['species.js'];
        set_cookie('useName',$user_details["Name"],'3600');

        $data['species_detail'] = current($this->speciesModel->getById($id));
        $data["subcat_data"]=$this->subcategoryModel->getsubCatList(["active"=>1],'', '', $data['species_detail']["category_id"]);
        $data['cat_details'] = $this->categoryModel->getCatList(["active"=>1]);
        $data['uom_details'] = $this->speciesModel->getUom();

          $item_search=[];
         $item_search["actve"] =1;
        if(trim( $data['species_detail']["category_id"])!=''){
             $item_search["category_id"] = $data['species_detail']["category_id"];

        }
        if(trim( $data['species_detail']["subcat_id"])!=''){
             $item_search["subcategory_id"] = $data['species_detail']["subcat_id"];

        }

        $item_data=$this->itemModel->getitemList( $item_search);
        foreach($item_data as $key=>$value){
            $item_data[$key]["id"]=base64_encode( $item_data[$key]["id"]);
            }  
         $data['item_data']=$item_data;
      
   

    // echo '<pre>'; print_r($data); die();
         
        $data['view_file'] = 'Editspecie';
        view($data);
            
    }
    public function doUpdateSpecies(){
        $data=$this->input->post();
         $resposne=["status"=>"0","message"=>"","field"=>''];
        
        try {
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
            if(trim($this->token_id)!=''){
                    if(isset($data["id"]) && trim($data["id"])==''){
                         $resposne=["status"=>"1","message"=>"Invalid Request.","field"=>'cat_name'];
                    }else{
                        $data["id"]=base64_decode($data["id"]);
                        $category_details=current($this->speciesModel->getById($data["id"]));
                        if(empty($category_details)){
                             $resposne=["status"=>"1","message"=>"Invalid Request.","field"=>'species_name'];
                         }else{

                           if(isset($data["species_name"]) && trim($data["species_name"])==''){
                                $resposne=["status"=>"1","message"=>"Please Enter Species Name.","field"=>'species_name'];
                            }else if(isset($data["category"]) && trim($data["category"])==''){
                                $resposne=["status"=>"1","message"=>"Please Select Category.","field"=>'cat_name'];
                            }else if(isset($data["item_id"]) && trim($data["item_id"])==''){
                                $resposne=["status"=>"1","message"=>"Please Enter Select Item.","field"=>'item_id'];
                            }else{


                                     $check_duplicate_catname = $this->speciesModel->dospeciesCheck($data["species_name"],$data["id"]);

                                    if(!empty($check_duplicate_catname)){
                                        $resposne=["status"=>"1","message"=>"Duplicate Species Name.","field"=>'species_name'];
                                    }
                                    if($resposne["status"]==0){

                                        if(!empty($_FILES["species_image"]) && trim($_FILES["species_image"]["name"])!=''){

                                          $file_name=time().$_FILES["species_image"]["name"];

                                           $allowedExts = array("gif", "jpeg", "jpg", "png");
                                            $image_data=explode(".", $_FILES["species_image"]["name"]);
                                            $extension =(!empty($image_data))?$image_data[1]:'';
                                             
                                            if ((($_FILES["species_image"]["type"] == "image/gif")  || ($_FILES["species_image"]["type"] == "image/jpeg")  || ($_FILES["species_image"]["type"] == "image/jpg")  || ($_FILES["species_image"]["type"] == "image/png")) && in_array($extension, $allowedExts))
                                            {

                                                $config['upload_path'] = './uploads/species/';
                                                 $config['allowed_types'] = 'jpg|png|jpeg|gif';
                                                // $config['max_size'] = '200';
                                                // $config['max_width'] = '800';
                                                // $config['max_height'] = '800';
                                                $config['file_name'] =$file_name;
                                            
                                                $this->load->library('upload', $config);
                                                if ( ! $this->upload->do_upload('species_image')){

                                                    $resposne=["status"=>"1","message"=>"Please Select  jpg/png/jpeg/gif Species Image.",'error'=> $this->upload->display_errors(),"field"=>'species_image'];
                                              
                                                }else{
                                                    if(file_exists('./uploads/species/'.$category_details["species_image"]) && trim($category_details["species_image"])!=''){
                                                        unlink('./uploads/species/'.$category_details["species_image"]);
                                                    }

                                                } 
                                            }
                                            else {
                                                $resposne=["status"=>"1","message"=>"Please Select  jpg/png/jpeg/gif Species Image",'error'=> '',"field"=>'image'];
                                            }

                                        }else{
                                             $file_name=$category_details["species_image"];
                                        }
                                        if($resposne["status"]==0){

                                            $string = $data['species_name'];
                                            $searchStringComma = ',';
                                            $searchStringHyphen = '-';
                                            $new_string = '';

                                            if( strpos($string, $searchStringComma) !== false ) {
                                                $new_string = str_replace(","," ", $string);
                                            }

                                            if ($new_string != '') { //If New String is not Blank
                                              if( strpos($new_string, $searchStringHyphen) !== false ) {
                                                  $new_string = str_replace("-"," ", $new_string);
                                                  $new_string = preg_replace('!\s+!', ' ', $new_string);
                                                  
                                                  $slug = strtolower(str_replace(" ","-", $new_string));
                                              }
                                              else {
                                                $new_string = preg_replace('!\s+!', ' ', $new_string);
                                                $slug = strtolower(str_replace(" ","-", $new_string));
                                              }
                                            }
                                            else { //If New String is Blank
                                              if( strpos($string, $searchStringHyphen) !== false ) {
                                                  $new_string = str_replace("-"," ", $string);
                                                  $new_string = preg_replace('!\s+!', ' ', $new_string);
                                                  
                                                  $slug = strtolower(str_replace(" ","-", $new_string));
                                              }
                                              else {
                                                $new_string = preg_replace('!\s+!', ' ', $string);
                                                $slug = strtolower(str_replace(" ","-", $new_string));
                                              }
                                            }


                                            $species_data = [
                                                'species_name' => $data['species_name'], 
                                                 'item_id' =>  base64_decode($data['item_id']), 
                                                 'species_image' => $file_name, 
                                                 'slug' => $slug
                                             ];
                                            $id=$this->speciesModel->updateSpecies($species_data,["id"=>$data["id"]]);
                                            if($id>0){
                                                $resposne=["status"=>"0","message"=>" Species updated successfully.","field"=>'species_name'];
                                            }else{
                                                
                                                $resposne=["status"=>"1","message"=>"Species updated failed.Please try again some time.","field"=>'species_name'];
                                                
                                            }
                                        }


                                    }


                            }

                         }

                    }


               
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
            }
        }catch (Exception $e) {
                $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'user_name','error'=>$e];

        }

        echo json_encode($resposne);
    }
  
    public function dospeciesStatusChange(){
         $data=$this->input->post();
        $data["id"]=base64_decode($data["id"]);
        $resposne=["status"=>"0","message"=>"","field"=>''];
        try {
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
            if(trim($this->token_id)!=''){
              $res=$this->speciesModel->updateSpecies(["active"=>$data["active"]],["id"=>$data["id"]]);
                        
                if($res){
                    $resposne=["status"=>"0","message"=>"Species status change successfully."];
                }
                else{                                
                    $resposne=["status"=>"1","message"=>"Species change failed.Please try again after some time."];                                
                }
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
            }   
        }catch (Exception $e) {
                $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'status'];

        }

        echo json_encode($resposne);
    }
    public function dospeciesDelete(){
        $data=$this->input->post();
        $data["id"]=base64_decode($data["id"]);
        $resposne=["status"=>"0","message"=>"","field"=>''];
        try {
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
            if(trim($this->token_id)!=''){
                $res = $this->speciesModel->doDeletespecies($data["id"]);
                        
                if($res){
                    $resposne=["status"=>"0","message"=>"Species Deleted successfully."];
                }
                else{                                
                    $resposne=["status"=>"1","message"=>"Species Deletion failed.Please try again after some time."];                                
                }
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
            }   
        }catch (Exception $e) {
                $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'role_id'];

        }

        echo json_encode($resposne);    
    }   

}
?>