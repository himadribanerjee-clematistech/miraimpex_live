<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class SpeciesModel extends CI_Model {
	protected $userTable = 'user';
	protected $speciesTable = 'species';
	protected $itemTable = 'item';
	protected $catTable = 'category';
	protected $uomTable = 'uom';
	
	public function __construct() {
        parent::__construct();
    }
    
   
    public function getspeciesCount($data){
    	try {
    		$this->db->db_debug = true;
    		$this->db->select("c.category_name, i.id, i.species_name, i.species_image, i.item_id");
    		$this->db->from($this->speciesTable . ' i');
    		$this->db->join($this->itemTable . ' itm', 'itm.id = i.item_id');
    		$this->db->join($this->catTable . ' c', 'itm.category_id = c.id');
    		$this->db->where("c.active",1);
			$this->db->where("itm.active",1);
			
    		$where='(case when c.parent_id=0  then c.active else ( SELECT active FROM t_category  WHERE id = c.parent_id ) end) = 1';
	    		$this->db->where($where);
			if(isset($data["species_name"]) && $data["species_name"]!=''){ 
	    		$this->db->like("species_name",$data["species_name"]);
	    	}
	    	if(isset($data["item_id"]) && $data["item_id"]!=''){ 
	    		$this->db->where("item_id",$data["item_id"]);
	    	}
	    	if(isset($data["actve"]) && $data["actve"]!=''){ 
	    		$this->db->where("i.active",$data["actve"]);

	    	}
	    	if(isset($data["subcategory_id"]) && $data["subcategory_id"]!=''){ 
	    		$this->db->where("category_id",$data["subcategory_id"]);

	    	}
	    	 if(isset($data["category_id"]) && $data["category_id"]!=''){
	    		$where='(case when c.parent_id=0  then c.id else ( SELECT id FROM t_category  WHERE id = c.parent_id ) end) = '.$data["category_id"];
	    		$this->db->where($where);

	    	}
	    	//$this->db->get();

	    	$db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	return $this->db->count_all_results();

	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
    }
    public function getspeciesList($data,$limit, $start){
    	try {
    		$this->db->db_debug = false;

    		$this->db->select("i.id, i.species_name, i.species_image, i.item_id,itm.item_name,i.active,IF (c.parent_id=0 ,c.category_name,(SELECT category_name FROM t_category WHERE id= c.parent_id) ) as category_name,IF (c.parent_id=0 ,c.id,(SELECT id FROM t_category WHERE id= c.parent_id) ) as category_id ,IF (c.parent_id=0 ,'',c.category_name ) as subcat_name ,IF (c.parent_id=0 ,'',c.id ) as subcat_id ");
    		$this->db->from($this->speciesTable . ' i');
    		$this->db->join($this->itemTable . ' itm', 'itm.id = i.item_id');
    		$this->db->join($this->catTable . ' c', 'itm.category_id = c.id');
    		$this->db->where("c.active",1);
			$this->db->where("itm.active",1);
			
    		$where='(case when c.parent_id=0  then c.active else ( SELECT active FROM t_category  WHERE id = c.parent_id ) end) = 1';
	    		$this->db->where($where);
			if(isset($data["species_name"]) && $data["species_name"]!=''){ 
	    		$this->db->like("species_name",$data["species_name"]);
	    	}
	    	if(isset($data["item_id"]) && $data["item_id"]!=''){ 
	    		$this->db->where("item_id",$data["item_id"]);
	    	}
	    	if(isset($data["subcategory_id"]) && $data["subcategory_id"]!=''){ 
	    		$this->db->where("category_id",$data["subcategory_id"]);

	    	}
	    	if(isset($data["actve"]) && $data["actve"]!=''){ 
	    		$this->db->where("i.active",$data["actve"]);

	    	}
	    	 if(isset($data["category_id"]) && $data["category_id"]!=''){
	    		$where='(case when c.parent_id=0  then c.id else ( SELECT id FROM t_category  WHERE id = c.parent_id ) end) = '.$data["category_id"];
	    		$this->db->where($where);

	    	}
	    	$this->db->order_by('species_name', 'DESC'); 
	    	$this->db->limit($limit, $start);
 
 			$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query();die();
	        	$result=[];
	        	foreach($query->result_array() as $row){
	        		$result[]=$row;
	        	}
	        	return $result;
	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
    }	

    public function getCat() {
    	return $this->db->select('id, category_name')->where('parent_id', 0)->where('active', 1)->from($this->catTable)->get();
    }

    public function getUom() {
    	return $this->db->select('id, name')->from($this->uomTable)->get();
    }

    public function dospeciesCheck($data, $id=''){
    	//print_r($data); die();
		try {
		 	$this->db->db_debug = FALSE;
	        $this->db->select("species_name");
	    	$this->db->from($this->speciesTable);
	    	if(isset($data) && trim($data)!='' ){
	    		$this->db->where("species_name",$data);
	    		
	    	}

	    	if($id!=''){
	    		$this->db->where("id !=",$id);
	    	}
	    	
	    	$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query() . '<br>';
	        	if($query->num_rows() >0) {
	        		return True;
	        	}
	        	else {
	        		return false;
	        	}
	        }
	        //return TRUE;
	    } catch (Exception $e) {
	       throw new Exception();
	    }	
    }
     
    public function insertspecies($data){
     	//print_r($data); die();
    	try {
    	 	$this->db->db_debug = true;
    	 	$res = $this->db->insert($this->speciesTable, $data);

    	 	$db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }
	        else {
	        	//echo $this->db->last_query() . '<br>';
	        	if ($res) {
	        		return true;
	        	}
	        	else {
	        		return false;
	        	}
	        }
	        //return  $this->db->insert_id();
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }
    	
    }

    public function getById($id){
    	try {
		 	$this->db->db_debug = false;
	        $this->db->select("i.id, i.species_name, i.species_image, i.item_id,itm.item_name,i.active,IF (c.parent_id=0 ,c.category_name,(SELECT category_name FROM t_category WHERE id= c.parent_id) ) as category_name,IF (c.parent_id=0 ,c.id,(SELECT id FROM t_category WHERE id= c.parent_id) ) as category_id ,IF (c.parent_id=0 ,'',c.category_name ) as subcat_name ,IF (c.parent_id=0 ,'',c.id ) as subcat_id ");
    		$this->db->from($this->speciesTable . ' i');
    		$this->db->join($this->itemTable . ' itm', 'itm.id = i.item_id');
    		$this->db->join($this->catTable . ' c', 'itm.category_id = c.id');
	    	
	    	$this->db->where("i.id", $id);
	    	
	    	$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	$data=[];
	        	foreach($query->result_array() as $row){
	        		 $data[]=$row;
	    	
	        	}
	        	//echo $this->db->last_query();
	        	return $data;
	        }
	        return TRUE;
	    } catch (Exception $e) {
	       throw new Exception();
	    }	
    }

    public function updateSpecies($data,$where){
    	
    	try {
    	 	$this->db->db_debug = False;
    	 	$this->db->trans_start(FALSE);
	        $this->db->where($where);
	    	$this->db->update($this->speciesTable, $data);
	        $this->db->trans_complete();
	        $db_error = $this->db->error();
	            if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	           
	        }
	        return TRUE;
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }
    }

    public function doDeletespecies($where){
    	
    	try {
    	 	$this->db->db_debug = False;
    	 	$res = $this->db->delete($this->speciesTable, ['id' => $where]);

	    	$db_error = $this->db->error();
	        if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }
	        else {
	        	if ($res) {
	        		return True;
	        	}
	        	else {
	        		return False;
	        	}
	        }
	        
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }
    }
   	
}

?>