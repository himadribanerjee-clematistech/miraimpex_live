
  
<!-- Simple Datatable start -->
<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="clearfix mb-20">
        <div class="pull-left">
            <h5 class="text-blue">List of Users</h5>
            
        </div>
        <div class="pull-right">
            <a class="btn btn-outline-primary" href="<?php echo base_url()?>user/addUser">Add User</a>
            
        </div>
    </div>
 

    <div class="container-fluid bg-light ">
            <form id="usersrch" action="<?php echo base_url()?>user">
              <input type="hidden" name="per_page_search" value="<?php echo $page; ?>">
            <div class="row align-items-center justify-content-center">
                        <div class="col-md-3 pt-3">
                           <div class="form-group ">
                              <input type="text" class="form-control" name="userName" id="user_name" value="<?php echo (isset($user_search["user_name"]))?$user_search["user_name"]:'';?>" placeholder="User Name">
                           </div>
                        </div>
                        <div class="col-md-3 pt-3">
                           <div class="form-group">
                              <input type="text"  class="form-control" name="name" id="Name" value="<?php echo (isset($user_search["Name"]))?$user_search["Name"]:'';?>" placeholder="Name">

                           </div>
                        </div>
                        <div class="col-md-3 pt-3">
                            <div class="form-group">
                             <select id="actve" name="status" class="form-control">
                            <option value="">Select Status</option>
                             <option value="1" <?php echo (isset($user_search) && isset($user_search["actve"]) &&  $user_search["actve"]==1)?'selected':'';?>>Active</option>
                             <option value="0"  <?php echo (isset($user_search) && isset($user_search["actve"]) &&  $user_search["actve"]==0)?'selected':'';?>>InActive</option>
                        </select>
                            </div>
                        </div>
                       
                        <div class="col-md-3">
                         <a class="" href="<?php echo base_url()?>user"><button type="button" class="btn btn-outline-primary "><i class="fa fa-refresh" aria-hidden="true"></i> Reset</button></a> 

                          <input type="submit" name="search" value="Search" class="btn btn-outline-primary">
                        </div>
            </div>
            </form>

    </div>
    <div class="row">
        <table class="data-table stripe hover nowrap" id="userlst">
            <thead>
                <tr>
                 <th width="2%"  class="table-plus datatable-nosort" >#</th>
                 <th width="20%"  class="table-plus datatable-nosort">User Name</th>
                 <th width="20%"  class="table-plus datatable-nosort" >Name</th>
                 <th width="10%"  class="table-plus datatable-nosort" >Mobile No</th>
                 <th width="10%"  class="table-plus datatable-nosort" >Email</th>
                 <th width="20%"  class="table-plus datatable-nosort" >Status</th>
                 <th width="10%"  class="table-plus datatable-nosort" >Action</th>
                    
                </tr>
            </thead>
            <tbody>

                <?php
                if(!empty($user_data)){
                    $i=0;
                    foreach($user_data as $val){
                        $i++;
                        ?>
                        <tr class="">
                            <td  class="table-plus"><?php echo $i;?></td>  
                            <td   class="table-plus"><?php echo $val["user_name"];?></td>   
                            <td   class="table-plus"><?php echo $val["Name"];?></td>    
                            <td  class="table-plus" ><?php echo $val["mobile"];?></td>  
                            <td  class="table-plus" ><?php echo $val["email"];?></td>
                             <td>
                             <?php
                                      if( ($val["id"]!=$user_id) && $val["id"]!=1){
                                        ?>
                               <select id="actve" class="form-control" name="status" onchange="doUserStatusChange(this.value,'<?php echo base64_encode($val["id"]);?>');">
                                    <option value="1" <?php echo ($val["actve"]==1)?'selected':'';?>>Active</option>
                                     <option value="0"  <?php echo ($val["actve"]==0)?'selected':'';?>>InActive</option>
                                </select>  
                                  <?php
                                      }else{

                                        ?>
                                         <select id="actve" class="form-control" disabled="" name="status" >
                                            <option value="1" <?php echo ($val["actve"]==1)?'selected':'';?>>Active</option>
                                             <option value="0"  <?php echo ($val["actve"]==0)?'selected':'';?>>InActive</option>
                                        </select>
                                       
                                        <?php
                                      }
                                      ?>

                            </td>
                             <td style="text-align:center;">
                                <?php
                                      if( ($val["id"]!=$user_id) && $val["id"]!=1){
                                        ?>

                                         <div class="dropdown">
                                                <a class="btn btn-outline-primary dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                                    <i class="fa fa-ellipsis-h"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                 
                                                    <a class="dropdown-item" href="javascript:void(0);" " onclick="doUserEdit('<?php echo base64_encode($val["id"]);?>')" ><i class="fa fa-pencil"></i> Edit</a>
                                                    
                                                </div>
                                            </div>
                                
                                        <?php
                                      }

                                ?>
                               
                            </td>

                        </tr>
                        
                        <?php
                    }
                }
                ?>

             
            </tbody>
        </table>
        <div class="col-sm-12">
            <div style="float: right;"><?php echo $links; ?></div>
        </div>
    </div>
</div>

    
<?php
    if(isset($custom_js)){
        foreach($custom_js as $val){
            ?>
            <script src="<?php echo base_url();?>assets/js/custom_js/<?php echo $val;?>"></script>
            <?php
        }
    }
    
?>
  