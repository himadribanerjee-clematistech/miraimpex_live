<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="clearfix mb-10">
        <div class="pull-left">
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Profile</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Change Password</li>
                </ol>
            </nav>
        </div>
    </div> 

    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">

                <form id="adduserfrm" method="post" action="javascript:void(0);" onsubmit="doChangePassword();">

                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-4">
                                <label for="old_password">Current Password <span class="mandatory_label">*</span></label>
                                <input type="password" name="old_password" id="old_password" value="" placeholder="Current Password" class="form-control">
                            </div>
                            <div class="col-lg-4">
                                <label label-for='new_password'>New Password <span class="mandatory_label">*</span></label>
                                <input type="password" name="new_password" id="new_password" value="" placeholder="New Password" class="form-control">
                            </div>
                            <div class="col-lg-4">
                                <label label-for='conf_password'>Confirm New Password <span class="mandatory_label">*</span></label>
                                <input type="password" name="conf_password" id="conf_password" value="" placeholder="Confirm New Password" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-12 text-right">
                                <input type="submit" value="Update" name="submit" class="btn btn-outline-primary">
                                <input type="hidden" name="id" value="<?php echo $user_details["user_id"];?>">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>




<?php
    if(isset($custom_js)){
        foreach($custom_js as $val){
            ?>
            <script src="<?php echo base_url();?>assets/js/custom_js/<?php echo $val;?>"></script>
            <?php
        }
    }
    
?>