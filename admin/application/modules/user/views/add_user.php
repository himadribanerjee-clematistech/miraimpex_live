<div class="min-height-200px">
    <div class="page-header">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="title">
                    
                </div>
                <nav aria-label="breadcrumb" role="navigation">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url()?>user">User</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Add User</li>
                    </ol>
                </nav>
            </div>

        </div>
    </div>

    <!-- horizontal Basic Forms Start -->
    <div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
   


         <form id="adduserfrm" method="post" action="javascript:void(0);" onsubmit="addUserDetails();">
          
            <div class="form-group">
                <div class="row">
                    
                    <div class="col-4">

                        <label for="user_name">User Name <sup class="mandatory_label">&#8727;</sup></label>
                        <input type="text" name="user_name" class="form-control" id="user_name" value="" placeholder="User Name">
                    </div>
                    
                   <div class="col-4">

                        <label for="Name">Name <sup class="mandatory_label">&#8727;</sup></label>
                        <input type="text" name="Name" id="Name" class="form-control"  value="" placeholder="Name">
                    </div>
                    
                    <div class="col-4">

                        <label for="email">Email <sup class="mandatory_label">&#8727;</sup></label>
                        <input type="text" name="email" id="email" class="form-control" value="" placeholder="Email">
                    </div>
                    
                </div>

            </div>
             <div class="form-group">
                <div class="row">
                    
                    <div class="col-4">

                        <label for="password">Password <sup class="mandatory_label">&#8727;</sup></label>
                        <input type="text" name="password" class="form-control" id="password" value="" placeholder="Password">
                    </div>
                    
                   <div class="col-4">

                        <label for="cpassword">Confirm Password <sup class="mandatory_label">&#8727;</sup></label>
                        <input type="text" name="cpassword" id="cpassword" class="form-control"  value="" placeholder="Confirm Password">
                    </div>
                    
                    <div class="col-4">

                        <label for="mobile">Mobile No <sup class="mandatory_label">&#8727;</sup></label>
                        <input type="text" name="mobile" id="mobile" class="form-control" value="" placeholder="Mobile No">
                    </div>
                    
                </div>

            </div>
            
            
            <div class="form-group">
                <div class="row">
                    <div class="col-6"></div>
                    <div class="col-6">
                        <input class="btn btn-primary" type="submit" value="Submit">
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- horizontal Basic Forms End -->

</div>

<?php
    if(isset($custom_js)){
        foreach($custom_js as $val){
            ?>
            <script src="<?php echo base_url();?>assets/js/custom_js/<?php echo $val;?>"></script>
            <?php
        }
    }
    
?>
  