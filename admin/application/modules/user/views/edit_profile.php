<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="clearfix mb-10">
        <div class="pull-left">
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Profile</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Update The Following Profile</li>
                </ol>
            </nav>
        </div>
    </div> 

    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">

                <form id="adduserfrm" method="post" action="javascript:void(0);" onsubmit="UpdateProfileDetails();">

                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-3">
                                <label label-for='name'>User Name <span class="mandatory_label">*</span></label>
                                <input type="text" name="user_name" id="user_name" value="<?php echo (isset($user_details) && isset($user_details["user_name"]) )?$user_details["user_name"]:'';?>" placeholder="User Name" class="form-control">
                            </div>
                            <div class="col-lg-3">
                                <label label-for='name'>Name <span class="mandatory_label">*</span></label>
                                <input type="text" placeholder="Name" name="Name" id="Name" value="<?php echo (isset($user_details) && isset($user_details["Name"]) )?$user_details["Name"]:'';?>" class="form-control">
                            </div>
                            <div class="col-lg-3">
                                <label label-for='name'>Email <span class="mandatory_label">*</span></label>
                                <input type="text" name="email" id="email" value="<?php echo (isset($user_details) && isset($user_details["email"]) )?$user_details["email"]:'';?>" placeholder="Email" class="form-control">
                            </div>
                            <div class="col-lg-3">
                                <label label-for='name'>Mobile <span class="mandatory_label">*</span></label>
                                <input type="text" placeholder="Mobile No" name="mobile" id="mobile" value="<?php echo (isset($user_details) && isset($user_details["mobile"]) )?$user_details["mobile"]:'';?>" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-12 text-right">
                                <input type="submit" value="Update" name="submit" class="btn btn-outline-primary">
                                <input type="hidden" name="id" value="<?php echo $user_details["user_id"];?>">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>




<?php
    if(isset($custom_js)){
        foreach($custom_js as $val){
            ?>
            <script src="<?php echo base_url();?>assets/js/custom_js/<?php echo $val;?>"></script>
            <?php
        }
    }
    
?>