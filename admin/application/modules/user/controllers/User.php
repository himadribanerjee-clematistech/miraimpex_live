<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class User extends CI_Controller {
	protected $token_id='';
	protected $per_page=10;
	public function __construct() {
    	parent::__construct();
    	$this->token_id=get_cookie('token_id'); 
    	if(trim($this->token_id)==''){
    		redirect("login");
    	}
        $this->load->model("userModel");
        $this->load->helper("pagination");
         $this->load->library("pagination");
         $this->load->library('email'); 
		$this->load->model("settingsModel", 'sM');
		$settings_data=$this->sM->getSettings();
		if(isset($settings_data->per_page)){
		$this->per_page=$settings_data->per_page;
		}
        
    }
    public function index()
	{
		try {
           $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            
            if(empty($user_details)){
                    $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                    delete_cookie('token_id'); 
                     delete_cookie('useName'); 
                    echo '<script>
                        alert("Unexpected server error.Please try again some time.");
                            window.location="'.base_url().'login";
                    </script>';
            }
            $user_details=current($user_details);
            $data=[]; 
            $data["menu"]="user";
            $data["useName"]=$user_details["Name"];
            $data["custom_js"]=['user.js'];
            $data["user_id"]=$user_details["user_id"];
            set_cookie('useName',$user_details["Name"],'3600');
            //search 
            $search_input=$this->input->get();
            $user_search=[];
            if(isset($search_input["userName"]) && trim($search_input["userName"])!=''){
            	$user_search["user_name"]=$search_input["userName"];

            }
            if(isset($search_input["name"]) && trim($search_input["name"])!=''){
            	$user_search["Name"]=$search_input["name"];
            	
            }
             if(isset($search_input["status"]) && trim($search_input["status"])!=''){
            	$user_search["actve"]=$search_input["status"];
            	
            }
            $data["user_search"]=$user_search;
            $page=0;
			if($this->input->get("per_page")){
	       		$page= $this->input->get("per_page");
	       	}

	       	$data["page"]=$page;
	       	
	       $total_row=$this->userModel->getUserCount($user_search);
	        $data["links"] = createPageNationLink(base_url() . "user/index", $total_row,$this->per_page);

	        $start= (int)$page +1;
			$end = ($page+ $this->per_page >=$total_row)? $total_row : (int)$page  + $this->per_page;
			$data['result_count']='';
			if($total_row>0){
				$data['result_count']= "Showing ".$start." - ".$end." of ".$total_row." Results";
			}
			
			$data["user_data"]=$this->userModel->getUserList($user_search,$this->per_page, $page);

	       //print_r($data);die();
            $data['view_file'] = 'user';
            view($data);
           
        } catch (Exception $e) {
            $resposne=["status"=>"1","e"=>$e,"message"=>"Unexpected server error.Please try again some time.","field"=>'user_name'];
         // var_dump($e->getMessage());
        }   
	}
	public function addUser(){
		try {
			$user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
			if(empty($user_details)){
					$this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
					delete_cookie('token_id'); 
					 delete_cookie('role_id'); 
					echo '<script>
						alert("Session expired please login again.");
							window.location="'.base_url().'login";
					</script>';
					exit();
			}
			$user_details=current($user_details);
			$data=[]; 
            $data["menu"]="user";
            $data["useName"]=$user_details["Name"];
            $data["custom_js"]=['user.js'];
            set_cookie('useName',$user_details["Name"],'3600');
             $data['view_file'] = 'add_user';
            view($data);
		} catch (Exception $e) {
			echo '<script>
				alert("Unexpected server error.Please try again some time.");
							window.location="'.base_url().'user";
			</script>';

		}
	}
	public function doAddUser(){
		try {
			$data=$this->input->post();
			$resposne=["status"=>"0","message"=>"","field"=>''];
			$user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));

			if(trim($this->token_id)!=''){
				if(isset($data["user_name"]) && trim($data["user_name"])==''){
					$resposne=["status"=>"1","message"=>"Please Enter User Name.","field"=>'user_name'];
				}else if(isset($data["Name"]) && trim($data["Name"])==''){
					$resposne=["status"=>"1","message"=>"Please Enter Name.","field"=>'Name'];
				}else if(isset($data["password"]) && trim($data["password"])==''){
					$resposne=["status"=>"1","message"=>"Please Enter Password.","field"=>'password'];
				}else if(isset($data["cpassword"]) && trim($data["cpassword"])==''){
					$resposne=["status"=>"1","message"=>"Please Enter Confirm Password.","field"=>'cpassword'];
				}else if(trim($data["cpassword"])!=trim($data["password"])){
					$resposne=["status"=>"1","message"=>"Password Mismatch.","field"=>'cpassword'];
				}else if(isset($data["email"]) && trim($data["email"])==''){
					$resposne=["status"=>"1","message"=>"Please Enter Email id.","field"=>'email'];
				}else if(isset($data["mobile"]) && trim($data["mobile"])==''){
					$resposne=["status"=>"1","message"=>"Please Enter Mobile No.","field"=>'mobile'];
				}else{

					$check_duplicate_email=$this->userModel->doEmailCheck(["email"=>$data["email"]]);
					$check_duplicate_username=$this->userModel->doUserNameCheck(["user_name"=>$data["user_name"]]);
					$check_duplicate_mobile=$this->userModel->doMobileCheck(["mobile"=>$data["mobile"]]);

					if(!empty($check_duplicate_username)){
						$resposne=["status"=>"1","message"=>"Duplicate User Name.","field"=>'user_name'];
					}else if(!empty($check_duplicate_email)){
						$resposne=["status"=>"1","message"=>"Duplicate Email id.","field"=>'email'];
					}else if(!empty($check_duplicate_mobile)){
						$resposne=["status"=>"1","message"=>"Duplicate Mobile No.","field"=>'mobile'];
					}
					if($resposne["status"]==0){
						$password=$this->userModel->passwordGenerate($data["password"]);
							$user_data=['user_name'=>$data["user_name"],'email'=>$data["email"],'mobile'=>$data["mobile"],'Name'=>$data["Name"],'actve'=>1,'password'=>$password];
							$id=$this->userModel->insertUser($user_data);
							if($id>0){
							 $resposne=["status"=>"0","message"=>" user added successfully.","field"=>'user_name'];
							}else{
								
								$resposne=["status"=>"1","message"=>"User added failed.Please try again some time.","field"=>'user_name'];
								
							}
					}

				}
			}else{
				$resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
			}
		} catch (Exception $e) {
				$resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'user_name'];

		}

		echo json_encode($resposne);
	}

	public function doUserEdit($id){
		$user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
			
			if(empty($user_details)){
					$this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
					delete_cookie('token_id'); 
					 delete_cookie('role_id'); 
					echo '<script>
						alert("Session expired please login again.");
							window.location="'.base_url().'login";
					</script>';
					exit();
			}
			$user_details=current($user_details);
			$id=base64_decode($id);
			if(trim($id)==''){
				echo '<script>
						alert("Invalid user.");
							window.location="'.base_url().'user";
					</script>';
					exit();
			}
			$data=[]; 
            $data["menu"]="user";
            $data["useName"]=$user_details["Name"];
            $data["custom_js"]=['user.js'];
            set_cookie('useName',$user_details["Name"],'3600');
            $data["user_details"]=current($this->userModel->getUserDetail($id,0));
			if(empty($data["user_details"])){
				echo '<script>
						alert("Invalid user.");
							window.location="'.base_url().'user";
					</script>';
					exit();
			}
			 $data['view_file'] = 'edit_user';
            view($data);
			
	}
	public function UpdateUserDetails(){
		$data=$this->input->post();
		$resposne=["status"=>"0","message"=>"","field"=>''];
		try {
			$user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
			if(trim($this->token_id)!=''){
				if(isset($data["user_name"]) && trim($data["user_name"])==''){
					$resposne=["status"=>"1","message"=>"Please Enter User Name.","field"=>'user_name'];
				}else if(isset($data["Name"]) && trim($data["Name"])==''){
					$resposne=["status"=>"1","message"=>"Please Enter Name.","field"=>'Name'];
				}else if(isset($data["email"]) && trim($data["email"])==''){
					$resposne=["status"=>"1","message"=>"Please Enter Email id.","field"=>'email'];
				}else if(isset($data["mobile"]) && trim($data["mobile"])==''){
					$resposne=["status"=>"1","message"=>"Please Enter Mobile No.","field"=>'mobile'];
				}else if(isset($data["actve"]) && trim($data["actve"])==''){
					$resposne=["status"=>"1","message"=>"Please Select Status.","field"=>'actve'];
				}else{
					$check_duplicate_email=$this->userModel->doEmailCheck(["email"=>$data["email"]],$data["id"]);
					$check_duplicate_username=$this->userModel->doUserNameCheck(["user_name"=>$data["user_name"]],$data["id"]);
					$check_duplicate_mobile=$this->userModel->doMobileCheck(["mobile"=>$data["mobile"]],$data["id"]);

					if(!empty($check_duplicate_username)){
						$resposne=["status"=>"1","message"=>"Duplicate User Name.","field"=>'user_name'];
					}else if(!empty($check_duplicate_email)){
						$resposne=["status"=>"1","message"=>"Duplicate Email id.","field"=>'email'];
					}else if(!empty($check_duplicate_mobile)){
						$resposne=["status"=>"1","message"=>"Duplicate Mobile No.","field"=>'mobile'];
					}
					if($resposne["status"]==0){
						$user_data=['user_name'=>$data["user_name"],'email'=>$data["email"],'mobile'=>$data["mobile"],'Name'=>$data["Name"],'actve'=>$data["actve"]];
							$id=$this->userModel->updateUser($user_data,["id"=>$data["id"]]);
							if($id>0){
							 $resposne=["status"=>"0","message"=>" user updated successfully.","field"=>'user_name'];
							}else{
								
								$resposne=["status"=>"1","message"=>"User updated failed.Please try again some time.","field"=>'user_name'];
								
							}
					}
				}
			}else{
				$resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
			}
		}catch (Exception $e) {
				$resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'user_name','error'=>$e];

		}

		echo json_encode($resposne);
	}

	public function douserStatusChange(){
		$data=$this->input->post();
		$data["id"]=base64_decode($data["id"]);
		$resposne=["status"=>"0","message"=>"","field"=>''];
		try {
			$user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
			if(trim($this->token_id)!=''){
				$user_detail=current($this->userModel->getUserDetail($data["id"]));
			
					if(empty($user_details)){
						$resposne=["status"=>"1","message"=>"Invalid User.","field"=>'user_name'];

					}else{
						$id=$this->userModel->updateUser(["actve"=>$data["actve"]],["id"=>$data["id"]]);
						$resposne=["status"=>"0","message"=>" user status change successfully.","field"=>'user_name'];
					}
			}else{
				$resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
			}	
		}catch (Exception $e) {
				$resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'role_id'];

		}

		echo json_encode($resposne);	
	}
	public function doUserDelete(){
		$data=$this->input->post();
		$data["id"]=base64_decode($data["id"]);
		$resposne=["status"=>"0","message"=>"","field"=>''];
		try {
			$user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
			if(trim($this->token_id)!=''){
				$user_detail=current($this->userModel->getUserDetail($data["id"]));
			
					if(empty($user_details)){
						$resposne=["status"=>"1","message"=>"Invalid User.","field"=>'user_name'];

					}else{
						$this->userModel->doDeleteUser(["id"=>$data["id"]]);
						$resposne=["status"=>"0","message"=>" user deleted successfully.","field"=>'user_name'];
					}
			}else{
				$resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
			}	
		}catch (Exception $e) {
				$resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'role_id'];

		}

		echo json_encode($resposne);	
	}	
	public function editProfile(){
		try {
			$user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
			
			if(empty($user_details)){
					$this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
					delete_cookie('token_id'); 
					 delete_cookie('role_id'); 
					echo '<script>
						alert("Session expired please login again.");
							window.location="'.base_url().'login";
					</script>';
					exit();
			}
			$user_details=current($user_details);
			$data=[]; 
            $data["menu"]="dashboard";
             $data["submenu"]="profile";
            $data["useName"]=$user_details["Name"];
            $data["custom_js"]=['user.js'];
            set_cookie('useName',$user_details["Name"],'3600');
			 $data["user_details"]=$user_details;
			 $data['view_file'] = 'edit_profile';
            view($data);
		} catch (Exception $e) {
			print_r($e);
			echo '<script>
				alert("Unexpected server error.Please try again some time.");
				</script>';

		}	
	}
	public function UpdateProfileDetails(){
		$data=$this->input->post();
		$resposne=["status"=>"0","message"=>"","field"=>''];
		try {
			$user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
			if(trim($this->token_id)!=''){
				if(isset($data["user_name"]) && trim($data["user_name"])==''){
					$resposne=["status"=>"1","message"=>"Please Enter User Name.","field"=>'user_name'];
				}else if(isset($data["Name"]) && trim($data["Name"])==''){
					$resposne=["status"=>"1","message"=>"Please Enter Name.","field"=>'Name'];
				}else if(isset($data["email"]) && trim($data["email"])==''){
					$resposne=["status"=>"1","message"=>"Please Enter Email id.","field"=>'email'];
				}else if(isset($data["mobile"]) && trim($data["mobile"])==''){
					$resposne=["status"=>"1","message"=>"Please Enter Mobile No.","field"=>'mobile'];
				}else{
					$check_duplicate_email=$this->userModel->doEmailCheck(["email"=>$data["email"]],$data["id"]);
					$check_duplicate_username=$this->userModel->doUserNameCheck(["user_name"=>$data["user_name"]],$data["id"]);
					$check_duplicate_mobile=$this->userModel->doMobileCheck(["mobile"=>$data["mobile"]],$data["id"]);

					if(!empty($check_duplicate_username)){
						$resposne=["status"=>"1","message"=>"Duplicate User Name.","field"=>'user_name'];
					}else if(!empty($check_duplicate_email)){
						$resposne=["status"=>"1","message"=>"Duplicate Email id.","field"=>'email'];
					}else if(!empty($check_duplicate_mobile)){
						$resposne=["status"=>"1","message"=>"Duplicate Mobile No.","field"=>'mobile'];
					}
					if($resposne["status"]==0){
						$user_data=['user_name'=>$data["user_name"],'email'=>$data["email"],'mobile'=>$data["mobile"],'Name'=>$data["Name"]];
							$id=$this->userModel->updateUser($user_data,["id"=>$data["id"]]);
							if($id>0){
							 $resposne=["status"=>"0","message"=>" user updated successfully.","field"=>'user_name'];
							}else{
								
								$resposne=["status"=>"1","message"=>"User updated failed.Please try again some time.","field"=>'user_name'];
								
							}
					}
				}
			}else{
				$resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
			}
		}catch (Exception $e) {
				$resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'user_name','error'=>$e];

		}

		echo json_encode($resposne);	
	}
	public function changePassword(){
		try {
			$user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
			if(empty($user_details)){
					$this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
					delete_cookie('token_id'); 
					 delete_cookie('role_id'); 
					echo '<script>
						alert("Unexpected server error.Please try again some time.");
						window.location="'.base_url().'login";
					</script>';
			}else{
				$user_details=current($user_details);
					$data=[]; 
		            $data["menu"]="dashboard";
		             $data["submenu"]="changepassword";
		            $data["useName"]=$user_details["Name"];
		            $data["custom_js"]=['user.js'];
		            set_cookie('useName',$user_details["Name"],'3600');
					 $data["user_details"]=$user_details;
					 $data['view_file'] = 'change_password';
		            view($data);
			}

		} catch (Exception $e) {
			echo '<script>
				alert("Unexpected server error.Please try again some time.");
				</script>';

		}	
	}
	public function doChangePassword(){
		try {	
				$data=$this->input->post();
				$resposne=["status"=>"0","message"=>"","field"=>''];

				if(trim($this->token_id)!=''){
					if(!(isset($data["old_password"]) && trim($data["old_password"])!=='')){
						$resposne=["status"=>"1","message"=>"Please Enter Current Password.","field"=>'old_password'];
					}else if(!(isset($data["new_password"]) && trim($data["new_password"])!=='')){
						$resposne=["status"=>"1","message"=>"Please Enter New Password.","field"=>'password'];
					}else if(!(isset($data["conf_password"]) && trim($data["conf_password"])!=='')){
						$resposne=["status"=>"1","message"=>"Please Enter Confirm New Password.","field"=>'cpassword'];
					}else if(trim($data["conf_password"])!=trim($data["new_password"])){
						$resposne=["status"=>"1","message"=>"Password Mismatch.","field"=>'cpassword'];
					}else{
						
						$user_data=$this->userModel->doPasswordCheck(["password"=>trim($data["old_password"]),"token_id"=>$this->token_id]);
						if(empty($user_data)){
							$resposne=["status"=>"1","message"=>"Invalid Current Password.","field"=>'old_password'];
						}else{
							$password=$this->userModel->passwordGenerate(trim($data["new_password"]));
							$this->userModel->updateUser(["password"=>$password],["token_id"=>$this->token_id] );
							$resposne=["status"=>"0","message"=>"Password Changed Successful"];
						}
					}
				}else{
					$resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
				}
		} catch (Exception $e) {
				$resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'first_name'];

		}

		echo json_encode($resposne);		
	}

}
?>