<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="clearfix mb-10">
        <div class="pull-left">
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">CMS</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Contact Content</li>
                </ol>
            </nav>
        </div>
    </div>

    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                
                <form id="addcontactfrm" method="post" action="javascript:void(0);" onsubmit="addContactDetails();" autocomplete="off">
                    <input type="hidden" name="code" id="code" value="<?php echo (isset($contact_data) && isset($contact_data->id) )?$contact_data->code:'';?>">

                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-3">
                                <label label-for="title">Title <span class="mandatory_label">*</span></label>
                            </div>
                            <div class="col-lg-9">
                                <input type="text" name="name" id="name" placeholder="Contact Title" value="<?php echo (isset($contact_data) && isset($contact_data->name) )?$contact_data->name:'';?>" class="form-control">
                            </div>
                        </div>
                    </div>

                    <br>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-3">
                                <label label-for="details">Details <span class="mandatory_label">*</span></label>
                            </div>
                            <div class="col-lg-9">
                                <textarea name="details" id="details" placeholder="Contact Details"><?php echo (isset($contact_data) && isset($contact_data->details) )?$contact_data->details:'';?></textarea>
                            </div>
                        </div>
                    </div>

                    <br>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-12 text-right">
                                <input type="submit" value="<?php echo (isset($contact_data))?'Update':'Submit';?>" name="submit" class="btn btn-outline-primary">
                            </div>
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>
</div>


<?php
	if(isset($custom_js)){
		foreach($custom_js as $val){
			?>
            <script src="<?php echo base_url();?>assets/js/custom_js/<?php echo $val;?>"></script>
			
			<?php
		}
	}

    if(isset($vendor_js)){
        foreach($vendor_js as $val){
            ?>
            <script src="<?php echo base_url();?>assets/vendors/ckeditor/<?php echo $val;?>"></script>
            <?php
        }
    }
	
?>
  