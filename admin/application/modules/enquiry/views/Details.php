<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="clearfix mb-10">
        <div class="pull-left">
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo base_url('enquiry')?>">Enquiry</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Details</li>
                </ol>
            </nav>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row" style="padding-bottom: 20px;">
            <div class="col-lg-12 text-right">
                <label style="font-weight: 400;"> <?php echo $result_details['date']; ?> </label>
            </div>
        </div>

        <div class="row" style="padding-bottom: 20px;">
            <div class="col-lg-6">
                <label style="font-weight: 400;">From : </label> <?php echo $result_details['from_cust']; ?> 
            </div>
            <div class="col-lg-6">
                <label style="font-weight: 400;">To : </label> <?php echo $result_details['to_cust']; ?> 
            </div>            
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="row" style="padding-bottom: 20px;">
                    <div class="col-lg-12">
                        <label style="font-weight: 400;">Quotation : </label>
                        <br>
                        <div id="message"> <?php echo $result_details['message']; ?> </div>
                    </div>
                </div>                
            </div>
        </div>        
    </div>

    <div class="container-fluid"> 
        <div class="row">      
            <table class="data-table stripe hover nowrap" id="userlst">
                <thead>
                    <tr>
                        <th width="2%"  class="table-plus datatable-nosort" >#</th>
                        <th width="15%"  class="table-plus datatable-nosort">From</th>
                        <th width="15%"  class="table-plus datatable-nosort">Replied By</th>
                        <th width="15%"  class="table-plus datatable-nosort">Replied On</th>
                        <!-- <th width="15%"  class="table-plus datatable-nosort">Message</th> -->
                        <th width="10%"  class="table-plus datatable-nosort text-center" >Action</th>                   
                    </tr>
                </thead>
                <tbody>

        <?php

            if(!empty($response_details)){
                $i=0;
                foreach($response_details as $val){
                    $i++;
        ?>

                    <tr class="">
                        <td class="table-plus" ><?php echo $i;?></td>  
                        <td class="table-plus" >
                            <?php echo $val["from_cust"];?>      
                        </td>
                        <td class="table-plus" >
                            <?php echo $val["to_cust"];?>                                              
                        </td>
                        <td class="table-plus">
                            <?php echo date('d-m-Y', strtotime($val["date"])); ?>                                              
                        </td>
                        <td class="table-plus text-center">
                            <a class="btn btn-outline-primary chat" href="#" role="button" data-toggle="modal" data-enq-id="<?php echo base64_encode($val['enquiry_id']) ?>" data-to-customer-id="<?php echo base64_encode($val['to_customer_id']) ?>" data-from-customer-id="<?php echo base64_encode($val['from_customer_id']) ?>" data-name="<?php echo $result_details['to_cust']; ?>">
                                <i class="fa fa-eye"></i>
                            </a>
                        </td>
                    </tr>

        <?php
                }
            }
          
        ?>
                    
                </tbody>
            </table>
        </div>
    </div>      
</div>



<!-- Modal -->

<div class="modal fade admin_modal" id="chatviewModal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h4 class="modal-title"> <span id="bussiness"> Bussiness Name </span> </h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                
                <div class="Message_box_control" id="chatBoxDetails"></div>

            </div>

        </div>
    </div>
</div>


<!-- Modal Ends -->


<div style="display: none;" id="clone">
    <div class="Message_box_sec replyMsg" id="replyMsg">
        <div class="Message_box_sec_img">
            <img src="" alt="" id="replyIcon">
        </div>
        <div class="Message_box_sec_info">
            <p class="Message_send_date"><span id="replyName"></span> <span id="replyDate"></span></p>
            <div class="Message_send_cont">
                <p id="replyText"></p>
                <p id="replyImage" class="d-none">
                    <img src="" alt="" id="replyImg">
                </p>
            </div>
        </div>
    </div>

    <div class="Message_box_sec myMsg" id="myMsg">
        <div class="Message_box_sec_img">
            <img src="" alt="" id="myIcon">
        </div>
        <div class="Message_box_sec_info">
            <p class="Message_send_date"><span id="myName"></span> <span id="myDate"></span></p>
            <div class="Message_send_cont">
                <p id="myText"></p>
                <p id="myImage" class="d-none">
                    <img src="" alt="" id="myImg">
                </p>
            </div>
        </div>
    </div>
</div>

<?php
    if(isset($custom_js)){
        foreach($custom_js as $val){
            ?>
            <script src="<?php echo base_url();?>assets/js/custom_js/<?php echo $val;?>"></script>
            <?php
        }
    }
    
?>