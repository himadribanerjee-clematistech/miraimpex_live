<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="clearfix mb-10">
        <div class="pull-left">
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo base_url('enquiry')?>">Enquiry Details</a></li>
                    <li class="breadcrumb-item active" aria-current="page">List</li>
                </ol>
            </nav>
        </div>
    </div>

    <div class="container-fluid bg-light"> 
        <div class="row">
            <div class="col-lg-12">
  
                <form id="enqsrch" action="<?php echo base_url('enquiry') ?>" autocomplete="off">

                    <input type="hidden" name="per_page_search" value="<?php echo $page; ?>">

                    <div class="row align-items-center justify-content-center">
                        <div class="col-md-3 pt-3">
                            <div class="form-group ">
                                <select id="type" name="type" class="form-control">
                                <option value="" <?php ($enquiry_search["type"]== '') ? 'selected="selected"' : '' ?>>All</option>
                                    <option value="1" <?php echo (isset($enquiry_search["type"]) &&  $enquiry_search["type"]== '1') ? 'selected="selected"' : '' ?> >Logistics</option>
                                    <option value="2" <?php echo (isset($enquiry_search["type"]) &&  $enquiry_search["type"]== '2') ? 'selected="selected"' : '' ?>>Contact</option>
                                </select>  
                            </div>
                        </div>

                        <div class="col-md-3 pt-3">
                            <div class="form-group">
                                <input type="text" name="to_date" id="to_date" class="form-control" placeholder="Enquiry Date" value="<?php echo (isset($enquiry_search) && isset($enquiry_search["to_date"]) &&  $enquiry_search["to_date"]!= '') ? date('d-m-Y', strtotime($enquiry_search["to_date"])) : ''; ?>">
                            </div>
                        </div>
                        
                        <div class="col-md-3 pt-3">
                            <div class="form-group">
                                <a class="" href="<?php echo base_url('enquiry')?>"><button type="button" class="btn btn-outline-primary "><i class="fa fa-refresh" aria-hidden="true"></i> Reset</button></a> 

                                <input type="submit" name="search" value="Search" class="btn btn-outline-primary">                                
                            </div>                       
                        </div>
                    </div>
                
                </form>

            </div>
        </div>
    </div>

    <div class="row">
        <table class="data-table stripe hover nowrap" id="userlst">
            <thead>
                <tr>
                    <th width="2%"  class="table-plus datatable-nosort" >#</th>
                    <th width="20%"  class="table-plus datatable-nosort">From</th>
                    <th width="20%"  class="table-plus datatable-nosort">Email</th>
                    <th width="15%"  class="table-plus datatable-nosort">Enquiry Date</th>
                    <th width="5%"  class="table-plus datatable-nosort">Type</th>
                    <th width=25%"  class="table-plus datatable-nosort text-center" >Messages</th>                 
                </tr>
            </thead>
            <tbody>

            <?php

                if(!empty($enq_data)){
                    $i=0;
                    foreach($enq_data as $val){
                        $i++;
                        $enq = json_decode($val['details'],true);
            ?>

                        <tr class="">
                            <td class="table-plus" valign="top" ><?php echo $i;?></td>  
                            <td class="table-plus"  valign="top">
                                <?php echo $enq["Name"];?>      
                            </td>
                            <td class="table-plus"  valign="top">
                                <?php echo $enq["Email"];?>                                              
                            </td>
                            <td class="table-plus"  valign="top">
                            <?php
                                echo date('d-m-Y', strtotime($val['sent_date']));
                            ?>                                              
                            </td>
                            <td class="table-plus"  valign="top"><?php echo ($val["type"]== '1') ? 'Logistics' : 'Contact' ?>  </td>
                            <td class="table-plus"  valign="top">
                                <?php 
                                   if(is_array($enq) && count($enq)>0)
                                   {
                                       foreach($enq as $k=>$v)
                                       {
                                           echo '<p style="margin-bottom:3px !important;">'.$k.': '.$v.'</p><br/>';
                                       }
                                   }
                                ?>                                              
                            </td>
                        </tr>

            <?php
                    }
                }
              
            ?>

             
            </tbody>
        </table>
        <div class="col-sm-12">
            <div style="float: right;"><?php echo $links; ?></div>
        </div>
    </div>
</div>


