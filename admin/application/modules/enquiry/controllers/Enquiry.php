<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class Enquiry extends CI_Controller {
    protected $token_id='';
    
    public function __construct() {
        parent::__construct();
        error_reporting(E_ALL^E_NOTICE);
        
        $this->token_id=get_cookie('token_id'); 
        if(trim($this->token_id)==''){
            redirect("login");
        }

        $this->load->model("userModel");
        $this->load->model("customerModel");

        $this->load->model("enquiryModel");       
        
        $this->load->helper("pagination");
        $this->load->library("pagination");
        $this->load->model("settingsModel", 'sM');
        $settings_data=$this->sM->getSettings();
        if(isset($settings_data->per_page)){
            $this->per_page=$settings_data->per_page;
        }
    }
    public function index() {
        try {
           $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            
            if(empty($user_details)){
                    $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                    delete_cookie('token_id'); 
                     delete_cookie('useName'); 
                    echo '<script>
                        alert("Unexpected server error.Please try again some time.");
                            window.location="'.base_url().'login";
                    </script>';
            }
            $user_details=current($user_details);
            $data=[]; 
            $data["menu"]="enquiry";
            $data["useName"]=$user_details["Name"];
            $data["custom_js"]=['enquiry.js'];
            set_cookie('useName',$user_details["Name"],'3600');
            
            
            //search 
            $search_input=$this->input->get();

            $enquiry_search=[];
            
            if(isset($search_input["type"]) && trim($search_input["type"])!=''){
                $enquiry_search["type"]=$search_input["type"];                
            }


            if(isset($search_input["to_date"]) && trim($search_input["to_date"])!=''){
                $enquiry_search["to_date"] = date('Y-m-d', strtotime($search_input["to_date"])); //2019-10-10             
            }

            $data["enquiry_search"] = $enquiry_search;
            $page=0;
            if($this->input->get("per_page")){
                $page= $this->input->get("per_page");
            }

            $data["page"]=$page;

            $total_row=$this->enquiryModel->getEnqCount($enquiry_search);
            
            $data["links"] = createPageNationLink(base_url('enquiry/index'), $total_row, $this->per_page);

            $start= (int)$page +1;
            $end = ($page+ $this->per_page >=$total_row)? $total_row : (int)$page  + $this->per_page;
            $data['result_count']='';
            if($total_row>0){
                $data['result_count']= "Showing ".$start." - ".$end." of ".$total_row." Results";
            }
            
            $data["enq_data"] = $this->enquiryModel->getEnqList($enquiry_search,$this->per_page, $page); 

            //print_r($data); die();
            $data['view_file'] = 'Enquiry';
            view($data);
           
        } catch (Exception $e) {
            $resposne=["status"=>"1","e"=>$e,"message"=>"Unexpected server error.Please try again some time.","field"=>'user_name'];
        }   
    }
    


}
?>