<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class EnquiryModel extends CI_Model {
	protected $enqTable = 't_customer_enquiry'; 
	
	public function __construct() {
        parent::__construct();
    }
     
    
    public function getEnqCount($data){
    	//print_r($data); //Array ( [to_date] => 2019-10-10 )
    	try {
    		$this->db->db_debug = false;


    		$this->db->select("e.id, e.details, e.type, e.sent_date");
    		$this->db->from($this->enqTable." e");
    		


	    	if(isset($data["to_date"]) && $data["to_date"]!=''){
	    		$this->db->where('DATE_FORMAT(e.sent_date, "%Y-%m-%d") = "' . $data["to_date"] . '"');
			}
			
			if(isset($data["type"]) && $data["type"]=='1'){
	    		$this->db->where('e.type', 1);
			}
			
			if(isset($data["type"]) && $data["type"]=='2'){
	    		$this->db->where('e.type', 2);
	    	}

	    	

	    	$this->db->order_by('e.sent_date', 'DESC');

			$query=$this->db->get();

	    	$db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query();die();
	        	return $this->db->count_all_results();

	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
    }

    public function getEnqList($data, $limit='', $start=0){
    	try {
    		$this->db->db_debug = false;

    		$this->db->select("e.id, e.details, e.type, e.sent_date");
    		$this->db->from($this->enqTable." e");
    		


	    	if(isset($data["to_date"]) && $data["to_date"]!=''){
	    		$this->db->where('DATE_FORMAT(e.sent_date, "%Y-%m-%d") = "' . $data["to_date"] . '"');
			}
			
			if(isset($data["type"]) && $data["type"]=='1'){
	    		$this->db->where('e.type', 1);
			}
			
			if(isset($data["type"]) && $data["type"]=='2'){
	    		$this->db->where('e.type', 2);
	    	}

	    	if($limit!='' && $start>=0 ){
	    		$this->db->limit($limit, $start);
	    	} 	    	
			
			$this->db->order_by('e.sent_date', 'DESC');

 			$query=$this->db->get();

 			//echo $this->db->last_query();


	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query();
	        	$result=[];
	        	foreach($query->result_array() as $row){
	        		$result[]=$row;
	        	}
	        	return $result;
	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
    }	

    

    
}

?>