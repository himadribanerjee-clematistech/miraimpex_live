<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class Logistic extends CI_Controller {
	protected $token_id='';
	protected $per_page=10;
	public function __construct() {
    	parent::__construct();
    	$this->token_id=get_cookie('token_id'); 
    	if(trim($this->token_id)==''){
    		redirect("login");
    	}

        $this->load->model("Logistic_userModel");
        $this->load->model("logisticModel", 'lM');
        $this->load->model("SettingsModel", 'sM');
        $this->load->model("userModel");
        $this->load->helper("pagination");
        $this->load->library("pagination");

        $settings_data=$this->sM->getSettings();
        if(isset($settings_data->per_page)){
            $this->per_page=$settings_data->per_page;
        }
    }
    public function index()
	{
		try {
           $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            
            if(empty($user_details)){
                    $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                    delete_cookie('token_id'); 
                     delete_cookie('useNlMe'); 
                    echo '<script>
                        alert("Unexpected server error.Please try again some time.");
                            window.location="'.base_url().'login";
                    </script>';
            }

            $user_details=current($user_details);
            $data=[]; 
            $data["menu"]="logistic";
            $data["useName"]=$user_details["Name"];
            $data["custom_js"]=['logistic.js']; 
            
            set_cookie('useNlMe',$user_details["Name"],'3600');

            //search 
            $search_input=$this->input->get();

            // print_r($search_input); 

            $log_search=[];
            
            if(isset($search_input["logistic_partner_id"]) && trim($search_input["logistic_partner_id"])!=''){
            	$log_search["logistic_partner_id"] = base64_decode($search_input["logistic_partner_id"]);
            }
            if(isset($search_input["to_date"]) && trim($search_input["to_date"])!=''){
                $log_search["to_date"] = $search_input["to_date"];
            }
            if(isset($search_input["from_date"]) && trim($search_input["from_date"])!=''){
                $log_search["from_date"] = $search_input["from_date"];
            }
            
            $data["log_search"] = $log_search;
            $page=0;
			if($this->input->get("per_page")){
	       		$page= $this->input->get("per_page");
	       	}

            $data["page"]=$page;

            $total_row=$this->lM->getLogisticCount($log_search);
         
	        $data["links"] = createPageNationLink(base_url('logistic/index'), $total_row, $this->per_page);

	        $start= (int)$page +1;
			$end = ($page+ $this->per_page >=$total_row)? $total_row : (int)$page  + $this->per_page;
			$data['result_count']='';
			if($total_row>0){
				$data['result_count']= "Showing ".$start." - ".$end." of ".$total_row." Results";
			}
			
			$data["logistic_data"]=$this->lM->getLogisticList($log_search, $this->per_page, $page);
            $data["logistic_user_data"] = $this->Logistic_userModel->getUserList('','','');

            $data['view_file'] = 'Logistic';
            // print_r($data); die();
            view($data);
           
        } catch (Exception $e) {
            $resposne=["status"=>"1","e"=>$e,"message"=>"Unexpected server error.Please try again some time.","field"=>'user_nlMe'];
        }   
	}

    public function Details() {
        try{
            $id = base64_decode($this->input->post('id'));

            $resposne=["status"=>"0","message"=>"","field"=>'', 'data'=> ''];

            $user_details = current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
            if(trim($this->token_id)!=''){
                $res = current($this->lM->getDetailById($id));

                $resposne=["status"=>"0","message"=>"Success.","field"=>'', 'data'=> $res];
            }
            else {
                $resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
            }

        }
        catch(Exception $e) {
            $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'role_id'];
        }

        echo json_encode($resposne);
    }
	
    /*public function addlogistic(){
        try {
            $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            if(empty($user_details)){
                    $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                    delete_cookie('token_id'); 
                     delete_cookie('role_id'); 
                    echo '<script>
                        alert("Session expired please login again.");
                            window.location="'.base_url().'login";
                    </script>';
                    exit();
            }
            $user_details=current($user_details);
            $data=[]; 
            $data["menu"]="logistic";
            $data["useNlMe"]=$user_details["NlMe"];
            $data["custom_js"]=['logistic.js'];
            set_cookie('useNlMe',$user_details["NlMe"],'3600');
             $data['view_file'] = 'Addlogistic';
            view($data);
        } catch (Exception $e) {
            echo '<script>
                alert("Unexpected server error.Please try again some time.");
                            window.location="'.base_url().'category";
            </script>';

        }
    }

    public function doAddlogistic(){
        try {
            $data=$this->input->post();
            $resposne=["status"=>"0","message"=>"", "field"=>''];
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));

            //print_r($data); die();

            if(trim($this->token_id)!=''){
                if(isset($data["nlMe_en"]) && trim($data["nlMe_en"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter NlMe.","field"=>'nlMe_en'];
                }
                elseif(isset($data["nlMe_fr"]) && trim($data["nlMe_fr"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter NlMe in French.","field"=>'nlMe_fr'];
                }
                elseif(isset($data["type"]) && trim($data["type"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter Type.","field"=>'type'];
                }
                else{
                    $check_duplicate_nlMe = $this->lM->doAttrCheck($data["nlMe_en"]);

                    if($check_duplicate_nlMe == 1) {
                        $resposne=["status"=>"1","message"=>"Duplicate logistic NlMe.","field"=>'nlMe_en'];
                    }
                    else {
                        
                        $formData = [
                            'nlMe_en'   => $data['nlMe_en'], 
                            'nlMe_fr'   => $data['nlMe_fr'], 
                            'type'      => $data['type'], 
                            'status'    => $data['active'],
                        ];
                        $res = $this->lM->savelogistic($formData);

                        if($res){
                            $resposne=["status"=>"0","message"=>"logistic added successfully."];
                        }
                        else{                                
                            $resposne=["status"=>"1","message"=>"logistic added failed.Please try again after some time."];                                
                        }
                    }
                }
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied."];
            }
        } catch (Exception $e) {
            $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time."];

        }

        echo json_encode($resposne);
    }

    public function dologisticEdit($id){
        $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            
        if(empty($user_details)){
                $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                delete_cookie('token_id'); 
                 delete_cookie('role_id'); 
                echo '<script>
                    alert("Session expired please login again.");
                        window.location="'.base_url().'login";
                </script>';
                exit();
        }
        $user_details=current($user_details);
        $id=base64_decode($id);
        if(trim($id)==''){
            echo '<script>
                    alert("Invalid user.");
                        window.location="'.base_url().'user";
                </script>';
                exit();
        }
        $data=[]; 
        $data["menu"]="logistic";
        $data["useNlMe"]=$user_details["NlMe"];
        $data["custom_js"]=['logistic.js'];
        set_cookie('useNlMe',$user_details["NlMe"],'3600');
        $data['attr_detail'] = current($this->lM->getById($id));

        // print_r($data); die();
         
        $data['view_file'] = 'Editlogistic';
        view($data);
            
    }
    public function updatelogistic(){

        try {
            $data=$this->input->post();
            $resposne=["status"=>"0","message"=>"", "field"=>''];
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));

            if(trim($this->token_id)!=''){
                if(isset($data["nlMe_en"]) && trim($data["nlMe_en"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter NlMe.","field"=>'nlMe_en'];
                }
                elseif(isset($data["nlMe_fr"]) && trim($data["nlMe_fr"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter NlMe in French.","field"=>'nlMe_fr'];
                }
                elseif(isset($data["type"]) && trim($data["type"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter Type.","field"=>'type'];
                }
                else{
                    $check_duplicate_nlMe = $this->lM->doAttrCheck($data["nlMe_en"], base64_decode($data["id"]));

                    if($check_duplicate_nlMe == 1) {
                        $resposne=["status"=>"1","message"=>"Duplicate logistic NlMe.","field"=>'nlMe_en'];
                    }
                    else {
                        
                       $formData = [
                            'nlMe_en'   => $data['nlMe_en'], 
                            'nlMe_fr'   => $data['nlMe_fr'], 
                            'type'      => $data['type'], 
                            'status'    => $data['active'],
                        ];
                        $res = $this->lM->savelogistic($formData, base64_decode($data["id"]));

                        if($res){
                            $resposne=["status"=>"0","message"=>"logistic Updated successfully."];
                        }
                        else{                                
                            $resposne=["status"=>"1","message"=>"logistic Updation failed.Please try again after some time."];                                
                        }
                    }
                }
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied."];
            }
        } catch (Exception $e) {
            $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time."];

        }

        echo json_encode($resposne);
    }

    public function doAttrStatusChange(){
        $data=$this->input->post();
        $id=base64_decode($data["id"]);
        $resposne=["status"=>"0","message"=>"","field"=>''];
        try {
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
            if(trim($this->token_id)!=''){
                $res = $this->lM->savelogistic(["status"=>$data["active"]], $id);
              
                if($res){
                    $resposne=["status"=>"0","message"=>"logistic status change successfully."];
                }
                else{                                
                    $resposne=["status"=>"1","message"=>"logistic change failed.Please try again after some time."];                                
                }
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
            }   
        }catch (Exception $e) {
                $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'status'];

        }

        echo json_encode($resposne);
    }

    public function doAttrTypeChange(){
        $data=$this->input->post();
        $id=base64_decode($data["id"]);
        $resposne=["status"=>"0","message"=>"","field"=>''];
        try {
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
            if(trim($this->token_id)!=''){
                $res = $this->lM->savelogistic(["type"=>$data["type"]], $id);
              
                if($res){
                    $resposne=["status"=>"0","message"=>"logistic type change successfully."];
                }
                else{                                
                    $resposne=["status"=>"1","message"=>"logistic type change failed.Please try again after some time."];                                
                }
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
            }   
        }catch (Exception $e) {
                $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'status'];

        }

        echo json_encode($resposne);
    }*/

}