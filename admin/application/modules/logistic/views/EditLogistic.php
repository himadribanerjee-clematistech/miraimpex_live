<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="clearfix mb-10">
        <div class="pull-left">
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo base_url('attribute')?>">Attribute Details</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Update Following Attribute Details</li>
                </ol>
            </nav>
        </div> 
        <div class="pull-right">
            <a class="btn btn-outline-primary" href="<?php echo base_url('attribute')?>">Back</a>            
        </div> 
    </div> 

    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">

                <form id="editattributefrm" method="post" action="javascript:void(0);" onsubmit="updateAttribute();">

                    <input type="hidden" name="id" id="id" value="<?php echo (isset($attr_detail) && isset($attr_detail["id"]) )?base64_encode($attr_detail["id"]):'';?>">

                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-6">
                                <label label-for='name'> Name <span class="mandatory_label">*</span></label>
                                <input type="text" name="name_en" id="name_en" placeholder="Name" value="<?php echo (isset($attr_detail) && isset($attr_detail["name_en"]) )?$attr_detail["name_en"]:'';?>" class="form-control">
                            </div>

                            <div class="col-lg-6">
                                <label label-for='name'> Name [French] <span class="mandatory_label">*</span></label>
                                <input type="text" name="name_fr" id="name_fr" placeholder="Name" value="<?php echo (isset($attr_detail) && isset($attr_detail["name_fr"]) )?$attr_detail["name_fr"]:'';?>" class="form-control">
                            </div>
                        </div>
                    </div>

                    <br>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-3">
                                <label label-for='type'> Type <span class="mandatory_label">*</span></label>
                                <select name="type" class="form-control" id="type">
                                    <option value=""> -- Select -- </option>
                                    <option value="1" <?php echo (isset($attr_detail)) ? ($attr_detail["type"] == 1) ? 'selected' : '' : ''; ?> >Item</option>
                                    <option value="2" <?php echo (isset($attr_detail)) ? ($attr_detail["type"] == 2) ? 'selected' : '' : ''; ?> >Service</option>
                                </select>
                            </div>

                            <div class="col-lg-3">
                                <label for="active"> Status </label>
                                <select name="active" class="form-control" id="active">
                                    <option value="1" <?php echo (isset($attr_detail)) ? ($attr_detail["status"] == 1) ? 'selected' : '' : ''; ?> >Active</option>
                                    <option value="0" <?php echo (isset($attr_detail)) ? ($attr_detail["status"] == 0) ? 'selected' : '' : ''; ?> >Inactive</option>
                                </select>
                            </div>

                            <div class="col-lg-6 text-right pt-20">
                                <input type="submit" value="Update" name="submit" class="btn btn-outline-primary">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>




<?php
	if(isset($custom_js)){
		foreach($custom_js as $val){
			?>
			<script src="<?php echo base_url();?>assets/js/custom_js/<?php echo $val;?>"></script>
			<?php
		}
	}
	
?>
  