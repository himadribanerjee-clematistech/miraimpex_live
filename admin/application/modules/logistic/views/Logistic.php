<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="clearfix mb-10">
        <div class="pull-left">
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Logistic Details</a></li>
                </ol>
            </nav>
        </div>
        <!-- <div class="pull-right"> 
            <a class="btn btn-outline-primary" href="<?php //echo base_url('Logistic/addLogistic')?>">Add New</a>            
        </div> -->
    </div>

    <div class="container-fluid bg-light"> 
        <div class="row">
            <div class="col-lg-12">

                <form id="attrsrch" action="<?php echo base_url('logistic')?>">
                    <input type="hidden" name="per_page_search" value="<?php echo $page; ?>">
                    <div class="row align-items-center justify-content-center">
                        <div class="col-md-3 pt-4 pb-4">
                            <label>Partner</label>
                            <select name="logistic_partner_id" id="logistic_partner_id" class="form-control">
                                <option value=""> -- select -- </option>

                            <?php

                                if (!empty($logistic_user_data)) {
                                    foreach($logistic_user_data as $val){

                            ?>

                                <option value="<?php echo base64_encode($val['id']); ?>" <?php echo (isset($log_search) && isset($log_search["logistic_partner_id"]) &&  $log_search["logistic_partner_id"] == $val['id'])?'selected':'';?>> <?php echo $val['name']; ?> </option>

                            <?php
                            
                                    }
                                }
                                else {
                                    echo '<option value=""> no names found </option>';
                                }

                            ?>

                            </select>
                        </div>

                        <div class="col-md-3 pt-4 pb-4">
                            <label>To Date</label>
                            <input type="date" name="to_date" id="to_date" class="form-control" value="<?php echo (isset($log_search) && isset($log_search["to_date"]) ) ? $log_search["to_date"] : ''; ?>">
                        </div>

                        <div class="col-md-3 pt-4 pb-4">
                            <label>From Date</label>
                            <input type="date" name="from_date" id="from_date" class="form-control" value="<?php echo (isset($log_search) && isset($log_search["from_date"]) ) ? $log_search["from_date"] : ''; ?>">
                        </div>

                        <!-- <div class="col-md-3 pt-4 pb-4">
                            <select id="status" name="status" class="form-control">
                                <option value="">Select Status</option>
                                <option value="1" <?php //echo (isset($attr_search) && isset($attr_search["status"]) &&  $attr_search["status"]==1)?'selected':'';?>>Active</option>
                                <option value="0"  <?php //echo (isset($attr_search) && isset($attr_search["status"]) &&  $attr_search["status"]==0)?'selected':'';?>>InActive</option>
                            </select>
                        </div> -->
                        <div class="col-md-3 pt-20">
                            <a class="btn btn-outline-primary" href="<?php echo base_url('logistic')?>"><i class="fa fa-refresh" aria-hidden="true"></i> Reset</a> 
                                
                            <input type="submit" name="search" value="Search" class="btn btn-outline-primary">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="row pt-4">
        
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="data-table stripe hover no-wrap" style="font-size: 14px;">
            <thead>
                <tr class="titel_bar">
                    <th class="table-plus datatable-nosort" width="5%">#</th>
                    <th class="table-plus datatable-nosort" width="20%">Partner</th>
                    <th class="table-plus datatable-nosort" width="30%">E-mail</th>
                    <th class="table-plus datatable-nosort" width="25%">Item</th>
                    <th class="table-plus datatable-nosort" width="5%">Quantity</th>
                    <th class="table-plus datatable-nosort text-center" width="20%">Date</th>
                    <th class="table-plus datatable-nosort text-center" width="30%" ><center>Action</center></th>
                </tr>
            </thead>
            <tbody>

            <?php
            	if(!empty($logistic_data)){
            		$i=0;
            		foreach($logistic_data as $val){
            			$i++;
            			?>
            			 <tr class="">
							<td ><?php echo $i;?></td>	
							<td >
                                <?php echo $val["name"];?>           
                            </td>
                            <td >
                                <?php echo $val["email"];?>           
                            </td>
                            <td >
                                <?php echo $val["item"];?>           
                            </td>
                            <td >
                                <?php echo $val["qty"];?>           
                            </td>
                            <td class="table-plus text-center">
                                <?php echo date('d-M-Y H:i:s', strtotime($val["date"]));?>           
                            </td>	
                            <td class="table-plus text-center">
                                <a class="btn btn-outline-primary details" href="#" role="button" data-toggle="modal" data-id="<?php echo base64_encode($val['id']) ?>">
                                    <i class="fa fa-eye"></i> 
                                </a>
                            </td>
							

            		 	</tr>
            			<?php
            		}
            	}else{
            		?>
            		 <tr class="">
            		 	 <td colspan="6" class="text-center">No Record Found</td>	

            		 </tr>	
            		<?php
            	}
            ?>
           
                 
            </tbody>
        </table>
    </div>

    <div class="row pt-4">
        <div class="col-sm-12">
            <div style="float: right;"><?php echo $links; ?></div>
        </div>
    </div>
</div>


<!-- Modal -->

<div class="modal fade admin_modal" id="viewModal">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h4 class="modal-title"> Logistic Details </h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            
            <!-- Modal body -->
            <div class="modal-body">
                <div class="row" style="padding-bottom: 20px;">
                    <div class="col-lg-4">
                        <label style="font-weight: 400;">Partner : </label> <br> <span id="partner"> Partner Name </span>
                    </div>
                    <div class="col-lg-4">
                        <label style="font-weight: 400;">Email : </label> <br> <span id="email"> Email </span>
                    </div>
                    <div class="col-lg-4">
                        <label style="font-weight: 400;">Date : </label> <br> <span id="date"> Date </span>
                    </div>
                </div>

                <div class="row" style="padding-bottom: 20px;">
                    <div class="col-lg-6">
                        <label style="font-weight: 400;">Port of Origin : </label> <br> <span id="port_of_origin"> Port of Origin </span>
                    </div>
                    <div class="col-lg-6">
                        <label style="font-weight: 400;">Port of Distribution : </label> <br> <span id="port_of_distribution"> Port of Distribution </span>
                    </div>
                </div>

                <div class="row" style="padding-bottom: 20px;">
                    <div class="col-lg-6">
                        <label style="font-weight: 400;">Item : </label> <br> <span id="item"> Item </span>
                    </div>
                    <div class="col-lg-6">
                        <label style="font-weight: 400;">Quantity : </label> <br> <span id="qty"> Quantity </span>
                    </div>
                </div>

                <div class="row detail_row">
                    <div class="col-lg-12"> 
                        <label style="font-weight: 400;">Message : </label>
                        <br>
                        <div id="message">Message</div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>


<!-- Modal Ends -->


<?php
	if(isset($custom_js)){
		foreach($custom_js as $val){
			?>
			<script src="<?php echo base_url();?>assets/js/custom_js/<?php echo $val;?>"></script>
			<?php
		}
	}
	
?>
  