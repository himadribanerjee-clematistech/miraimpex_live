<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class LogisticModel extends CI_Model {
	protected $LogisticTable = 'logistic';
	protected $LogisticUserTable = 'logistic_user';
	
	public function __construct() {
        parent::__construct();
    }
    
    public function getLogisticCount($data){
    	// print_r($data); die();
    	// print_r($data["to_date"]); die(); // 2020-02-01
    	try {
    		$this->db->db_debug = false;

    		// SELECT lu.name, l.id, l.email, l.item, l.qty, l.date FROM t_logistic_user lu INNER JOIN t_logistic l ON l.logistic_partner_id = lu.id

    		$this->db->select("lu.name, l.id, l.email, l.item, l.qty, l.date");
    		$this->db->from($this->LogisticTable . ' l');
    		$this->db->join($this->LogisticUserTable . ' lu', 'l.logistic_partner_id = lu.id', 'INNER');
    		if(isset($data["logistic_partner_id"]) && $data["logistic_partner_id"]!=''){ 
	    		$this->db->like("logistic_partner_id",$data["logistic_partner_id"]);
	    	}
	    	
	    	
	    	if( (isset($data["to_date"])) && (isset($data["from_date"]))){ 
	    		$this->db->where("DATE_FORMAT(`date`, '%Y-%m-%d') BETWEEN '" . $data["to_date"] . "' AND '" . $data["from_date"] . "'");
	    	}
	    	else if(isset($data["to_date"]) && $data["to_date"]!=''){
	    		$this->db->where("DATE_FORMAT(`date`, '%Y-%m-%d') > '" . $data["to_date"] . "'");
	    	}
	    	else if(isset($data["from_date"]) && $data["from_date"]!=''){ 
	    		$this->db->where("DATE_FORMAT(`date`, '%Y-%m-%d') < '" . $data["from_date"] . "'");
	    	}

	    	$this->db->get();
	    	// echo $this->db->last_query();die();	    	
	    	
	    	$db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	
	        	return $this->db->count_all_results();

	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
    }
    public function getLogisticList($data='',$limit='', $start=''){
    	try {
    		$this->db->db_debug = false;
    		$this->db->select("lu.name, l.id, l.email, l.item, l.qty, l.date");
    		$this->db->from($this->LogisticTable . ' l');
    		$this->db->join($this->LogisticUserTable . ' lu', 'l.logistic_partner_id = lu.id', 'INNER');
    		if(isset($data["logistic_partner_id"]) && $data["logistic_partner_id"]!=''){ 
	    		$this->db->like("logistic_partner_id",$data["logistic_partner_id"]);
	    	}
	    	
	    	
	    	if( (isset($data["to_date"])) && (isset($data["from_date"]))){ 
	    		$this->db->where("DATE_FORMAT(`date`, '%Y-%m-%d') BETWEEN '" . $data["to_date"] . "' AND '" . $data["from_date"] . "'");
	    	}
	    	else if(isset($data["to_date"]) && $data["to_date"]!=''){
	    		$this->db->where("DATE_FORMAT(`date`, '%Y-%m-%d') > '" . $data["to_date"] . "'");
	    	}
	    	else if(isset($data["from_date"]) && $data["from_date"]!=''){ 
	    		$this->db->where("DATE_FORMAT(`date`, '%Y-%m-%d') < '" . $data["from_date"] . "'");
	    	}
	    	
	    	$this->db->order_by('id', 'DESC'); 

	    	if ($limit != '' ) {
	    		$this->db->limit($limit, $start);
	    	}
	    	
 
 			$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query();die();
	        	$result=[];
	        	foreach($query->result_array() as $row){
	        		$result[]=$row;
	        	}
	        	return $result;
	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
    }

    public function getDetailById($id) {
    	try {
		 	$this->db->db_debug = false;

		 	// SELECT lu.name, l.id, l.email, l.item, l.qty, l.date FROM t_logistic_user lu INNER JOIN t_logistic l ON l.logistic_partner_id = lu.id

		 	$this->db->select("lu.name, l.email, l.port_of_origin, l.port_of_distribution, l.item, l.qty, l.message, DATE_FORMAT(l.date, '%d-%b-%Y %r') AS date");
    		$this->db->from($this->LogisticTable . ' l');
    		$this->db->join($this->LogisticUserTable . ' lu', 'l.logistic_partner_id = lu.id', 'INNER');
    		$this->db->where('l.id', $id);


		 	$query=$this->db->get();

    		//echo $this->db->last_query();die();

    		$db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	$data=[];
	        	foreach($query->result_array() as $row){
					$data[] = $row;
	        	}
	        	return array_values($data);
	        }
	        return TRUE;
	    } 
	    catch (Exception $e) {
	       throw new Exception();
	    }	

    }	

    /*public function doAttrCheck($data, $id=''){
    	//print_r($data); die();
		try {
		 	$this->db->db_debug = FALSE;
	        $this->db->select("name_en");
	    	$this->db->from($this->LogisticTable);
	    	if(isset($data) && trim($data)!='' ){
	    		$this->db->where("name_en",$data);	    		
	    	}

	    	if($id!=''){
	    		$this->db->where("id !=",$id);
	    	}

	    	$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	// echo $this->db->last_query() . '<br>';
	        	if($query->num_rows() >0) {
	        		return True;
	        	}
	        	else {
	        		return false;
	        	}
	        }
	        //return TRUE;
	    } catch (Exception $e) {
	       throw new Exception();
	    }	
    }

    public function saveLogistic($data, $where=''){
    	try {
    	 	$this->db->db_debug = False;

    	 	if ($where != '') {
    	 		$res = $this->db->update($this->LogisticTable, $data, ['id' => $where]);
    	 	}
    	 	else {
    	 		$res = $this->db->insert($this->LogisticTable, $data);
    	 	}

    	 	

	    	$db_error = $this->db->error();
	        if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }
	        else {
	        	if ($res) {
	        		return True;
	        	}
	        	else {
	        		return False;
	        	}
	        }
	        
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }
    }

    public function getById($id){
    	try {
		 	$this->db->db_debug = false;
	        $this->db->select("id, name_en, name_fr, type, status");
	    	$this->db->from($this->LogisticTable);
	    	
	    	$this->db->where("id", $id);
	    	
	    	$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	$data=[];
	        	foreach($query->result_array() as $row){
	        		 $data[]=$row;
	    	
	        	}
	        	// echo $this->db->last_query();
	        	return $data;
	        }
	        return TRUE;
	    } catch (Exception $e) {
	       throw new Exception();
	    }	
    }*/
}

?>