<style>
    .group_pay{
    background: #fff;
    padding: 15px;
    margin: 10px;
    -webkit-box-shadow: 0 0 5px 0 #B8B8B8;
    box-shadow: 0 0 5px 0 #B8B8B8;
}
.pay_heading{
  margin: -15px -15px 0px;
}
.pay_heading h4{
  color: #fff!important;
  padding: 7px 15px;
  font-size: 20px;
}
.right_pay_head{
  width: 100%;
  padding: 0 0 10px;
  display: flex;
}
.right_pay_head .pay_img{
  width: 110px;
}
.right_pay_head .pay_cnt{
  padding-left: 10px;
} 
.payment_page_info{
  width: 100%;
  padding: 6px 0;
  border-top: 1px solid #ddd;
  display: flex;
}
.pay_title{
  width: 80%;
  padding-right: 20px;
}
.pay_value{
  width: 115px;
  color: #333;
  text-align: right;
}
h5.pay_title{
  line-height: 23px;
  font-size: 15px;

}
h5.pay_value, h5.pay_title b{ 
  font-weight: bold;
}

.order_other_info > div{
  display: flex;
  padding: 0 0 5px;
  border-bottom: 1px solid #ddd;
  margin-bottom: 10px;
}
</style>
<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="clearfix mb-10">
        <div class="pull-left">
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Order Details</a></li>
                </ol>
            </nav>
        </div>
    </div>

    <div class="container-fluid bg-light d-none"> 
        <div class="row">
            <div class="col-lg-12">

                <form id="attrsrch" action="<?php echo base_url('orders')?>">
                    <input type="hidden" name="per_page_search" value="<?php echo $page; ?>">
                    <div class="row align-items-center justify-content-center">
                        <div class="col-md-3 pt-4 pb-4">
                            
                        </div>

                        <div class="col-md-3 pt-4 pb-4">
                            <label>To Date</label>
                            <input type="date" name="to_date" id="to_date" class="form-control" value="<?php echo (isset($log_search) && isset($log_search["to_date"]) ) ? $log_search["to_date"] : ''; ?>">
                        </div>

                        <div class="col-md-3 pt-4 pb-4">
                            <label>From Date</label>
                            <input type="date" name="from_date" id="from_date" class="form-control" value="<?php echo (isset($log_search) && isset($log_search["from_date"]) ) ? $log_search["from_date"] : ''; ?>">
                        </div>

                        <div class="col-md-3 pt-20">
                            <a class="btn btn-outline-primary" href="<?php echo base_url('orders')?>"><i class="fa fa-refresh" aria-hidden="true"></i> Reset</a> 
                                
                            <input type="submit" name="search" value="Search" class="btn btn-outline-primary">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="row pt-4">
        
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="data-table1 stripe hover no-wrap" style="font-size: 14px;">
            <thead>
                <tr class="titel_bar">
                    <th class="table-plus datatable-nosort" width="5%">#</th>
                    <th class="table-plus datatable-nosort" width="20%">Buyer Info</th>
                    <th class="table-plus datatable-nosort" width="30%">Seller Info</th>
                    <th class="table-plus datatable-nosort" width="25%">Total Amount</th>
                    <th class="table-plus datatable-nosort" width="5%">Payment Mode</th>
                    <th class="table-plus datatable-nosort text-center" width="20%">Date</th>
                    <th class="table-plus datatable-nosort text-center" width="30%" ><center>Action</center></th>
                </tr>
            </thead>
            <tbody>

            <?php
            	if(!empty($orderData)){
                
            		$i=0;
            		foreach($orderData as $val){
                  
                        $encData = json_encode($val, JSON_HEX_APOS);
                        
            			$i++;
            			?>
            			 <tr class="">
							<td >
                            <?php
                              if($val['proposal_id'] == 0)
                              {
                                echo '<p>Order Id:'.$val["id"].'</p><p>Transaction Id:'.$val["transction_id"].'</p>';
                              }elseif($val['proposal_id'] > 0)
                              {
                                echo '<p>Proposal Id:'.$val["id"].'</p><p>Transaction Id:'.$val["transction_id"].'</p>';
                              }
                            ?>    
                            </td>	
							<td >
                                <?php echo $val["buyername"];?><br><?php echo $val["buyeremail"];?><br><?php echo $val["buyerphone"];?><br>           
                            </td>
                            <td >
                            <?php echo $val["sellername"];?><br><?php echo $val["selleremail"];?><br><?php echo $val["sellerphone"];?><br>            
                            </td>
                            <td >
                                <?php echo '$'.$val["total_amount"];?>           
                            </td>
                            <td >
                                <?php 
                                 if($val['payment_mode'] == 1)
                                 {
                                     echo '<p>COD</p>';
                                 }elseif($val['payment_mode'] == 2)
                                {
                                    echo '<p>Bank Transfer (For Gambia)</p>';
                                    
                                }elseif($val['payment_mode'] == 3)
                                {
                                    echo '<p>Bank Transfer (For US)</p>';
                                    
                                }elseif($val['payment_mode'] == 4)
                                {
                                    echo '<p>Bank Transfer (For Europe)</p>';
                                    
                                }

                                if($val['status'] == 1)
                                    {
                                        echo '<p>Status: Success</p>';
                                    }elseif($val['status'] == 0)
                                    {
                                        echo '<p>Status: Pending</p>';
                                    }elseif($val['status'] == 2)
                                    {
                                        echo '<p>Status: Failed</p>';
                                    }
                                ?>           
                            </td>
                            <td class="table-plus text-center">
                                <?php echo date('d-M-Y H:i:s', strtotime($val["created_date"]));?>           
                            </td>	
                            <td class="table-plus text-center">
                                <a class="btn btn-outline-primary details" href="#" role="button" data-toggle="modal" data-id="<?php echo base64_encode($val['id']) ?>" raw='<?php echo $encData; ?>'>
                                    <i class="fa fa-eye"></i> 
                                </a>
                            </td>
							

            		 	</tr>
            			<?php
            		}
            	}else{
            		?>
            		 <tr class="">
            		 	 <td colspan="6" class="text-center">No Record Found</td>	

            		 </tr>	
            		<?php
            	}
            ?>
           
                 
            </tbody>
        </table>
    </div>

    <div class="row pt-4">
        <div class="col-sm-12">
            <div style="float: right;"><?php echo $links; ?></div>
        </div>
    </div>
</div>


<!-- Modal -->

<div class="modal fade admin_modal" id="viewModal">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h4 class="modal-title"> Order Details </h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            
            <!-- Modal body -->
            <div class="modal-body">
                <div class="row" style="padding-bottom: 20px;" id="orderSummery">
                    
                </div>

                <div class="row group_pay" style="padding-bottom: 20px;">
                <div class="order_other_info col-md-12">
                    <div><h5><i class="fa fa-user-circle-o"></i> Order Info</h5></div>
                    <div><p id="buyerorderdate"></p></div>
                    <div><p id="buyerorderid"></p></div>
                    <div><p id="buyerorderstatus"></p></div>
                    <div><p id="buyertransactionid"></p></div>
                    <div><p id="buyerpaymentmode"></p></div>   
                    <div><p id="buyerpaymentstatus"></p></div>   
                    </div>
                </div>

                <div class="row group_pay" style="padding-bottom: 20px;">
                <div class="order_other_info">
                    <div><h5><i class="fa fa-user-circle-o"></i> Buyer Info</h5></div>
                    <div><p id="buyerContName"></p></div><div><p id="buyerContNo"></p></div><div><p id="buyerEmail"></p></div><div><p id="buyercontactnumber1"></p></div><div><p id="buyercontactnumber2"></p></div>   
                    </div>
                </div>

                <div class="row group_pay" style="padding-bottom: 20px;">
                <div class="order_other_info" >
                    <div><h5><i class="fa fa-user-circle-o"></i> Supplier Info</h5></div>
                    <div><p id="compName"></p></div><div><p id="compContName"></p></div><div><p id="compContNo"></p></div><div><p id="compEmail"></p></div> 
                    </div>
                </div>

                
            </div>

        </div>
    </div>
</div>


<!-- Modal Ends -->
<script type="text/javascript">
$(document).ready(function(){
    $('.details').click(function(){
    var base_url = $('#base_url').val();
    var iid = $(this).data('id'); 
    $('#orderSummery').html('');

    var order_details = jQuery.parseJSON($(this).attr('raw'));
    console.log(order_details);

    var paymentMode = (order_details.payment_mode == "1") ? "Cash On Delivery" : (order_details.payment_mode == "2") ? "Bank Transfer" : "NA";

    var paymentStatus = (order_details.status == "0") ? "Pending" : (order_details.status == "1") ? "Success" : (order_details.status == "2") ? "Failed" : "NA";

    if(order_details.proposal_id == "0")
    {
        // ADD TO CART SECTION 

        var addtocart = JSON.parse(order_details.item_details);
        var totalToPay = 0;
        var currency = '';    

        var cartSchema = '<table class="table" style="border: 1px solid lightgray;"><thead style="background: #e2e1e138; color: black;"><tr><th width="45%" style="border-right: 1px solid lightgray;"><center>Product</center></th><th width="18%" style="border-right: 1px solid lightgray;"><center>Price</center></th><th width="20%" style="border-right: 1px solid lightgray;"><center>Quantity</center></th><th width="15%" style="border-right: 1px solid lightgray;"><center>Total</center></th></tr></thead><tbody>';

        if (addtocart) {
        $.each(addtocart, function (key, input) {              
            var variance = '';
            if (input.itemvariance != "") {
            var addtocartvariance = JSON.parse(input.itemvariance);
            $.each(addtocartvariance, function (vkey, vinput) {
                variance += '<span class="item-variance" style="padding-right:10px;">' + vinput.varianceName + ': ' + vinput.varianceAttribute + '</span>';
            });
            }
            
            var currencySymbol = '';
            if (input.currency == 'Dollar' || input.currency == 'USD') {
            currencySymbol = '$';
            } else if (input.currency == 'Euro' || input.currency == 'EUR') {
            currencySymbol = '€';
            } else {
            currencySymbol = '$';
            }

            currency = currencySymbol;

            var perItemTotalPrice = 0;

            perItemTotalPrice += input.quantity*input.perItemPrice;
            perItemTotalPrice = (Math.round(perItemTotalPrice * 100) / 100).toFixed(2);



            cartSchema += '<tr><td style="border-right: 1px solid #f1eded;"><div class="row"><div class="col-lg-3"><img src="' + input.itemimg + '" style="width: 100px;"></div><div class="col-lg-9"><h6>' + input.itemname + '</h6><p style="padding-top:10px; font-size: 12px;">'+variance+'</p></h4></div></div></td><td style="border-right: 1px solid #f1eded;"><p style="text-align: center; font-size:16px; font-weight:600;">' + currencySymbol + '' + input.perItemPrice + '</p></td><td style="border-right: 1px solid #f1eded;"> <div class="input-group" style=""><p style="text-align: center;">'+input.quantity+'</p></div><div style="text-align: center;"></div></td><td style="border-right: 1px solid #f1eded;"><p style="text-align: center; font-size: 20px; font-weight: 600; padding: 3px 34px 0 34px;">' + currencySymbol + '' + perItemTotalPrice + '</p><p style="text-align: center;"><span>incl. all taxes</span></p></td></tr>';
            
        });

        totalToPay = (Math.round(order_details.total_amount * 100) / 100).toFixed(2);

        cartSchema +='</tbody><tfoot style="background: #e2e1e138; color: black;"><tr><td colspan="2"></td><td><div><p style="font-size: 16px; font-weight: 600; color:#000">Total</p></div></td><td style="text-align: center;"><p style="font-size: 16px; font-weight: 600; color:#000">' + currency + ' ' + totalToPay + '</p></td></tr></tfoot>';

        cartSchema += '</table>';

        $('#orderSummery').html(cartSchema);

        
        }
    }else{
        
        // PROPOSAL SECTION        

        var proposalSchema = '<div class="group_pay"><div class="right_pay_head rfqsection"><div class="pay_img"><a href="<?php echo base_url();?>admin/uploads/proposal/'+order_details.proposal_file+'" id="proposal_doc_link" target="_blank" title="view proposal details"><img src="<?php echo base_url();?>assets/images/proposal-icon.png" alt=""></a></div><div class="pay_cnt"><a href="<?php echo base_url();?>admin/uploads/proposal/'+order_details.proposal_file+'" id="proposal_title" target="_blank" title="view proposal details">'+order_details.proposal_title+'</a></div></div><div class="payment_page_info rfqsection"><p class="pay_title" id="proposal_desc">'+order_details.proposal_description+'</p></div><div class="payment_page_info"><p class="pay_title">Transaction#</p><p class="pay_value" id="transactionId">'+order_details.transction_id+'</p></div><div class="payment_page_info"><p class="pay_title">Payment Mode</p><p class="pay_value" id="paymentMode">'+paymentMode+'</p></div><div class="payment_page_info"><p class="pay_title">Payment Status</p><p class="pay_value" id="paymentStatus">'+paymentStatus+'</p></div><div class="payment_page_info"><p class="pay_title">Proposal Date</p><p class="pay_value" id="proposal_date">'+order_details.proposal_created_date+'</p></div><div class="payment_page_info"><h5 class="pay_title"><b>Total</b></h5><h5 class="pay_value proposal_amount" id="proposal_amount">$'+order_details.proposal_amount+'</h5></div></div>';

        $('#orderSummery').html(proposalSchema);

    }   

    $('#buyerContName').html('<b>Full Name: </b>'+order_details.buyername);

          var buyer_info = JSON.parse(order_details.buyer_info);
          $.each(buyer_info, function (bk, bv) {
            if(bv.name == 'firstname')
            {              
              $('#buyerContNo').html('<b>Delivery Address: </b>'+bv.value);
            }
            if(bv.name == 'address')
            {
              $('#buyerContNo').append(', '+bv.value);
            }
            
            if(bv.name == 'city')
            {
              $('#buyerContNo').append(', '+bv.value);
            }
            if(bv.name == 'state')
            {
              $('#buyerContNo').append(', '+bv.value);
            }
            if(bv.name == 'zip')
            {
              $('#buyerContNo').append(', '+bv.value);
            }
            if(bv.name == 'email')
            {
              $('#buyerEmail').html('<b>Email: </b>'+bv.value);
            }
            if(bv.name == 'contactnumber1')
            {
              $('#buyercontactnumber1').html('<b>Contact No: </b>'+bv.value);
            }
            if(bv.name == 'contactnumber2')
            {
              // $('#buyercontactnumber2').html('<b>Contact No(optional): </b>'+bv.value);
            }
          });
          
          $('#compName').html('<b>Company Name: </b>'+order_details.seller_business_name);
          $('#compContName').html('<b>Contact Person: </b>'+order_details.seller_contact_name);
          $('#compContNo').html('<b>Contact No: </b>'+order_details.seller_contact_number);
          $('#compEmail').html('<b>Address: </b>'+order_details.seller_address);

          $('#buyerorderdate').html('<b>Order Date: </b>'+order_details.created_date);
          $('#buyerorderid').html('<b>Order ID: </b>'+order_details.id);

          var neworder = '';

          if(order_details.order_status == "0")
          {
            neworder = '<span style="background:#1c84b5; color:white; padding:6px;">New</span>';
          }
          if(order_details.order_status == "1")
          {
            neworder = '<span style="background:#1c84b5; color:white; padding:6px;">Accepted</span>';
          }
          if(order_details.order_status == "2")
          {
            neworder = '<span style="background:#1c84b5; color:white; padding:6px;">Shipped</span>';
          }
          if(order_details.order_status == "3")
          {
            neworder = '<span style="background:#1c84b5; color:white; padding:6px;">Delivered</span>';
          }
          if(order_details.order_status == "4")
          {
            neworder = '<span style="background:#6e3ce6; color:white; padding:6px;">Closed</span>';
          }
          if(order_details.order_status == "5")
          {
            neworder = '<span style="background:red; color:white; padding:6px;">Cancelled</span>';
          }

          $('#buyerorderstatus').html('<b>Order Status: </b>'+neworder);
          $('#buyertransactionid').html('<b>Transaction ID: </b>'+order_details.transction_id);
          $('#buyerpaymentmode').html('<b>Payment Mode: </b>'+paymentMode);
          $('#buyerpaymentstatus').html('<b>Payment Status: </b>'+paymentStatus);


    $('#viewModal').modal('show');
    });
});

</script>
  