<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class OrdersModel extends CI_Model {
	protected $orderTable = 't_order_log';
	
	public function __construct() {
        parent::__construct();
    }
    
    public function getOrderCount($data){
    	try {
    		$this->db->db_debug = false;

    		$this->db->select("o.id, o.proposal_id, o.transction_id, o.status, o.created_date, o.updated_date, o.total_amount, o.payment_mode, o.item_details, o.buyer_info, o.response, o.user_id, o.seller_id, o.order_status");
			$this->db->from($this->orderTable . ' o');
			
	    	
	    	
	    	if( (isset($data["to_date"])) && (isset($data["from_date"]))){ 
	    		$this->db->where("DATE_FORMAT(`created_date`, '%Y-%m-%d') BETWEEN '" . $data["to_date"] . "' AND '" . $data["from_date"] . "'");
	    	}
	    	else if(isset($data["to_date"]) && $data["to_date"]!=''){
	    		$this->db->where("DATE_FORMAT(`created_date`, '%Y-%m-%d') > '" . $data["to_date"] . "'");
	    	}
	    	else if(isset($data["from_date"]) && $data["from_date"]!=''){ 
	    		$this->db->where("DATE_FORMAT(`created_date`, '%Y-%m-%d') < '" . $data["from_date"] . "'");
	    	}

			
	    	// echo $this->db->last_query();die();	    	
	    	
	    	$db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	
	        	return $this->db->count_all_results();

	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
    }
    public function getOrderList($data='',$limit='', $start=''){
    	try {
    		$this->db->db_debug = false;
    		$this->db->select("o.id, o.proposal_id, o.transction_id, o.status, o.created_date, o.updated_date, o.total_amount, o.payment_mode, o.item_details, o.buyer_info, o.response, o.user_id, o.seller_id, o.order_status, user.active as buyer_status,user.name as buyername, user.email as buyeremail,user.phone as buyerphone,seller.active as seller_status, seller.name as sellername, seller.email as selleremail,seller.phone as sellerphone,business.business_name as seller_business_name,business.address as seller_address, business.country as seller_country, business.state as seller_state, business.city as seller_city,business.contact_name as seller_contact_name, business.contact_number as seller_contact_number, proposal.from_id as proposal_from_id, proposal.to_id as proposal_to_id, proposal.title as proposal_title, proposal.description as proposal_description, proposal.file as proposal_file, proposal.amount as proposal_amount, proposal.created_date as proposal_created_date, proposal.currency as proposal_currency, proposal.refference_id as proposal_refference_id, proposal.refference_type as proposal_refference_type, proposal.is_paid as proposal_is_paid");
			$this->db->from($this->orderTable . ' o');
			$this->db->join('t_customer user', 'o.user_id = user.id', 'LEFT');
			$this->db->join('t_customer seller', 'o.seller_id = seller.id', 'LEFT');
			$this->db->join('t_seller_profile business', 'o.seller_id = business.cust_id', 'LEFT');
			$this->db->join('t_proposal proposal', 'o.proposal_id = proposal.id', 'LEFT');
    		
	    	
	    	
	    	if( (isset($data["to_date"])) && (isset($data["from_date"]))){ 
	    		$this->db->where("DATE_FORMAT(`created_date`, '%Y-%m-%d') BETWEEN '" . $data["to_date"] . "' AND '" . $data["from_date"] . "'");
	    	}
	    	else if(isset($data["to_date"]) && $data["to_date"]!=''){
	    		$this->db->where("DATE_FORMAT(`created_date`, '%Y-%m-%d') > '" . $data["to_date"] . "'");
	    	}
	    	else if(isset($data["from_date"]) && $data["from_date"]!=''){ 
	    		$this->db->where("DATE_FORMAT(`created_date`, '%Y-%m-%d') < '" . $data["from_date"] . "'");
	    	}
	    	
	    	$this->db->order_by('o.created_date', 'DESC'); 

	    	if ($limit != '' ) {
	    		$this->db->limit($limit, $start);
	    	}
	    	
 
 			$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query();die();
	        	$result=[];
	        	foreach($query->result_array() as $row){
	        		$result[]=$row;
	        	}
	        	return $result;
	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
    }


}

?>