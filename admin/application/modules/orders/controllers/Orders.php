<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class Orders extends CI_Controller {
	protected $token_id='';
	protected $per_page=10;
	public function __construct() {
        parent::__construct();
        error_reporting('E_ALL^E_NOTICE');
    	$this->token_id=get_cookie('token_id'); 
    	if(trim($this->token_id)==''){
    		redirect("login");
    	}

        $this->load->model("ordersModel", 'oM');
        $this->load->model("SettingsModel", 'sM');
        $this->load->model("userModel");
        $this->load->helper("pagination");
        $this->load->library("pagination");

        $settings_data=$this->sM->getSettings();
        if(isset($settings_data->per_page)){
            $this->per_page=$settings_data->per_page;
        }
    }
    public function index()
	{
		try {
           $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            
            if(empty($user_details)){
                    $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                    delete_cookie('token_id'); 
                     delete_cookie('useNlMe'); 
                    echo '<script>
                        alert("Unexpected server error.Please try again some time.");
                            window.location="'.base_url().'login";
                    </script>';
            }
            
            $user_details=current($user_details);
            $data=[]; 
            $data["menu"]="Orders";
            $data["useName"]=$user_details["Name"];
            $data["custom_js"]=['orders.js']; 
            
            set_cookie('useNlMe',$user_details["Name"],'3600');

            //search 
            $search_input=$this->input->get();

            // print_r($search_input); 

            $log_search=[];
            
            if(isset($search_input["to_date"]) && trim($search_input["to_date"])!=''){
                $log_search["to_date"] = $search_input["to_date"];
            }
            if(isset($search_input["from_date"]) && trim($search_input["from_date"])!=''){
                $log_search["from_date"] = $search_input["from_date"];
            }
            
            $data["log_search"] = $log_search;
            $page=0;
			if($this->input->get("per_page")){
	       		$page= $this->input->get("per_page");
	       	}

            $data["page"]=$page;

            $total_row=$this->oM->getOrderCount($log_search);
            
         
	        $data["links"] = createPageNationLink(base_url('orders/index'), $total_row, $this->per_page);

	        $start= (int)$page +1;
			$end = ($page+ $this->per_page >=$total_row)? $total_row : (int)$page  + $this->per_page;
			$data['result_count']='';
			if($total_row>0){
				$data['result_count']= "Showing ".$start." - ".$end." of ".$total_row." Results";
			}
			
            $orderData=$this->oM->getOrderList($log_search, $this->per_page, $page);
            /* echo '<pre>';
            print_r($orderData);
            die; */ 
            $data['orderData'] = $orderData;

            $data['view_file'] = 'Orders';
            view($data);
           
        } catch (Exception $e) {
            $resposne=["status"=>"1","e"=>$e,"message"=>"Unexpected server error.Please try again some time.","field"=>'user_nlMe'];
        }   
	}

    public function Details() {
        try{
            $id = base64_decode($this->input->post('id'));

            $resposne=["status"=>"0","message"=>"","field"=>'', 'data'=> ''];

            $user_details = current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
            if(trim($this->token_id)!=''){
                $res = current($this->lM->getDetailById($id));

                $resposne=["status"=>"0","message"=>"Success.","field"=>'', 'data'=> $res];
            }
            else {
                $resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
            }

        }
        catch(Exception $e) {
            $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'role_id'];
        }

        echo json_encode($resposne);
    }
	
    

}