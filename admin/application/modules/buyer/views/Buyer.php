<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="clearfix mb-10">
        <div class="pull-left">
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Buyer Profile</a></li>
                    <li class="breadcrumb-item active" aria-current="page">List</li>
                </ol>
            </nav>
        </div>
        <!-- <div class="pull-right">
            <a class="btn btn-outline-primary" href="<?php //echo base_url('buyer/addBuyer')?>">Add New</a>            
        </div> -->
    </div>

    <div class="container-fluid bg-light"> 
        <div class="row">
            <div class="col-lg-12">

                <form id="catsrch" action="<?php echo base_url('buyer')?>">
                    <input type="hidden" name="per_page_search" value="<?php echo $page; ?>">

                    <div class="row align-items-center justify-content-center">
                        <div class="col-md-4 pt-3">
                            <div class="form-group ">
                                <select name="cust_id" id="cust_id" class="form-control">
                                    <option value=""> -- select Buyer -- </option>

                            <?php

                                foreach ($customer as $cust) {
                                    
                            ?>

                                    <option value="<?php echo base64_encode($cust["id"]); ?>"
                                        <?php 
                                            echo (isset($buyer_search) && isset($buyer_search["cust_id"]) && ($buyer_search["cust_id"] == $cust["id"]) )? 'selected':'';
                                        ?>

                                    > <?php echo $cust["name"]; ?> </option>

                                <?php

                                    }

                                ?>

                                    
                                </select>
                            </div>
                        </div>
                        <!-- <div class="col-md-3 pt-3">
                            <div class="form-group">
                                <input type="text" name="business_name" id="business_name" value="<?php //echo (isset($buyer_search["business_name"]))?$buyer_search["business_name"]:'';?>" placeholder="Bussiness Name" class="form-control">
                            </div>
                        </div> -->
                        <div class="col-md-3 pt-3">
                            <div class="form-group">
                                <input type="text" name="city" id="city" value="<?php echo (isset($buyer_search["city"]))?$buyer_search["city"]:'';?>" placeholder="Bussiness City" class="form-control">
                            </div>
                        </div>
                       
                        <div class="col-md-3">
                            <a class="btn btn-outline-primary" href="<?php echo base_url('buyer')?>"><i class="fa fa-refresh" aria-hidden="true"></i> Reset</a> 
                                
                            <input type="submit" name="search" value="Search" class="btn btn-outline-primary">
                        </div>
                    </div>
                    
                </form>
            </div>
        </div>
    </div>


    <div class="row pt-2">
        
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="data-table stripe hover no-wrap" style="font-size: 14px;">

            <thead>
                <tr class="titel_bar">
                    <th class="table-plus datatable-nosort" width="2%">#</th>
                    <th class="table-plus datatable-nosort" width="10%">Customer</th>
                    <th class="table-plus datatable-nosort" width="10%">Email</th>
                    <th class="table-plus datatable-nosort" width="10%">State</th>
                    <th class="table-plus datatable-nosort" width="8%">City</th>
                    <th class="table-plus datatable-nosort" width="10%"><center>Status</center></th>
                    <th class="table-plus datatable-nosort" width="8%" ><center>Action</center></th>
                </tr>
            </thead>
            <tbody>

            <?php
                if(!empty($buyer)){
                    $i=0;
                    foreach($buyer as $val){
                        $i++;
            ?>
            
                <tr class="">
                    <td class="table-plus"><?php echo $i;?></td>
                    <!-- <td class="table-plus">
                        <img src="<?php //echo (isset($val['business_logo']) && $val['business_logo'] != '') ? base_url() . 'uploads/bussiness_logo/' . $val['business_logo'] : base_url('assets/images/no-item-image.png'); ?>" height="80" width="80"  style="border: 2px dotted #ccc; border-radius: 2px;"/>
                    </td> -->
                    <td class="table-plus"><?php echo $val["cust_name"];?></td>
                    <td class="table-plus"><?php echo $val["email"];?></td>
                    <td class="table-plus"><?php echo $val["state"];?></td>
                    <td class="table-plus"><?php echo $val["city"];?></td>
                    <td class="table-plus">
                        <select name="status" id="status" onchange="doBuyerStatusChange(this.value, '<?php echo base64_encode($val["id"]);?>')" class="form-control">
                            <option value="1" <?php echo (isset($val) && isset($val["active"]) && $val['active'] == 1)?'selected':''; ?>> Active </option>

                            <option value="0" <?php echo (isset($val) && isset($val["active"]) && $val['active'] == 0)?'selected':''; ?>> InActive </option>
                        </select>
                    </td>
                    <td class="table-plus text-center">
                        <a class="btn btn-outline-primary buyer" href="#" role="button" data-toggle="modal" data-id="<?php echo base64_encode($val['id']) ?>">
                            <i class="fa fa-eye"></i>
                        </a>
                    </td>
                    <!-- <td class="table-plus" style="text-align:center;">

                        <div class="dropdown">
                            <a class="btn btn-outline-primary dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                <i class="fa fa-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="javascript:void(0);" style=" font-size: 15px;" onclick="doBuyerEdit('<?php //echo base64_encode($val["id"]);?>')" >
                                    <i class="fa fa-edit" aria-hidden="true"></i> 
                                    Edit
                                </a>
                            </div>
                        </div>

                    </td> -->
                </tr>
                        
            <?php
                    }
                }else{
            ?>

                <tr class="">
                    <td colspan="10"><center>No Buyer Record Found</center></td>
                </tr>   
                    
            <?php
                }
            ?>
           
                 
            </tbody>
    </table>
    <div class="col-sm-12">
            <div style="float: right;"><?php echo $links; ?></div>
        </div>
</div>


<!-- Modal -->

<div class="modal fade admin_modal" id="viewModal">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h4 class="modal-title"> <!-- <span id="business_name"></span> --> Buyer Details </h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="row" style="padding-bottom: 20px;">
                    <div class="col-lg-4">
                        <div>
                            <img src=""  style="border: 2px dotted #ccc; border-radius: 2px; height: 150px; width: 150px;" id="business_logo" />
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div class="row pb-20">
                            <div class="col-lg-4"><label style="font-weight: 400;">Name : </label></div>
                            <div class="col-lg-8"><span id="cust_name"> </span></div>
                        </div>
                        <div class="row pb-20">
                            <div class="col-lg-4"><label style="font-weight: 400;">Bussiness Name : </label></div>
                            <div class="col-lg-8"><span id="bussiness_name"> </span></div>
                        </div>
                        <div class="row pb-20">
                            <div class="col-lg-4"><label style="font-weight: 400;">Address : </label></div>
                            <div class="col-lg-8"><span id="address"> </span></div>
                        </div>
                        <div class="row pb-20">
                            <div class="col-lg-4"><label style="font-weight: 400;">City : </label></div>
                            <div class="col-lg-8"><span id="b_city"> </span></div>
                        </div>
                        <!-- <div class="row pb-20">
                            <div class="col-lg-4"><label style="font-weight: 400;">Latitude : </label></div>
                            <div class="col-lg-8"><span id="lat"> </span></div>
                        </div>
                        <div class="row pb-20">
                            <div class="col-lg-4"><label style="font-weight: 400;">Long : </label></div>
                            <div class="col-lg-8"><span id="long"> </span></div>
                        </div> -->
                        <div class="row">
                            <div class="col-lg-4"><label style="font-weight: 400;">State : </label></div>
                            <div class="col-lg-8"><span id="state"> </span></div>
                        </div>                         
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


<!-- Modal Ends -->


<?php
    if(isset($custom_js)){
        foreach($custom_js as $val){
            ?>
            <script src="<?php echo base_url();?>assets/js/custom_js/<?php echo $val;?>"></script>
            <?php
        }
    }
    
?>
  

<script type="text/javascript">
    
    

</script>