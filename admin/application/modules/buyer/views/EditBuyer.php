<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="clearfix mb-10">
        <div class="pull-left">
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Buyer Profile</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Update Following Buyer</li>
                </ol>
            </nav>
        </div> 
        <div class="pull-right">
            <a class="btn btn-outline-primary" href="<?php echo base_url()?>buyer">Back</a>            
        </div> 
    </div>

    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                <form id="editBuyerfrm" method="post" action="javascript:void(0);" onsubmit="editBuyer();" autocomplete="off">
                    <input type="hidden" name="id" id="id" value="<?php echo (isset($buyerDetail) && isset($buyerDetail["id"]) ) ? base64_encode($buyerDetail["id"]):'';?>">

                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-3">
                                <label label-for='name'>Customer <span class="mandatory_label">*</span></label>
                                <select name="cust_id" id="cust_id" class="form-control">
                                        <option value=""> -- select -- </option>

                                <?php

                                    foreach ($customer as $cust) {
                                        
                                ?>

                                        <option value="<?php echo base64_encode($cust["id"]); ?>" 
                                            <?php echo (isset($buyerDetail) && ($buyerDetail["cust_id"] == $cust["id"]) )? 'selected':''; ?> > <?php echo $cust["name"]; ?> </option>

                                <?php

                                    }

                                ?>

                                    
                                </select>
                            </div>
                            <div class="col-lg-3">
                                <label label-for='name'>Business Name <span class="mandatory_label">*</span></label>
                                <input type="text" name="business_name" id="business_name" value="<?php echo (isset($buyerDetail) && isset($buyerDetail["business_name"]) ) ? $buyerDetail["business_name"]:'';?>" placeholder="Business Name" class="form-control">
                            </div>
                            <div class="col-lg-3">
                                <label label-for='name'>Address <span class="mandatory_label">*</span></label>
                                <textarea name="address" id="address" placeholder="Enter Address" class="form-control"><?php echo (isset($buyerDetail) && isset($buyerDetail["address"]) ) ? $buyerDetail["address"]:'';?></textarea>
                            </div>
                            <div class="col-lg-3">
                                <label label-for='name'>State <span class="mandatory_label">*</span></label>
                                <input type="text" name="state" id="state" value="<?php echo (isset($buyerDetail) && isset($buyerDetail["state"]) ) ? $buyerDetail["state"]:'';?>" placeholder="Buyer state" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-3">
                                <label label-for='name'>City <span class="mandatory_label">*</span></label>
                                <input type="text" name="city" id="city" value="<?php echo (isset($buyerDetail) && isset($buyerDetail["city"]) ) ? $buyerDetail["city"]:'';?>" placeholder="Buyer City" class="form-control">         
                            </div>
                            <div class="col-lg-3">
                                <label label-for='name'>Latitude <span class="mandatory_label">*</span></label>
                                <input type="text" name="lat" id="lat" value="<?php echo (isset($buyerDetail) && isset($buyerDetail["lat"]) ) ? $buyerDetail["lat"]:'';?>" placeholder="Buyer Latitude" class="form-control">
                            </div>
                            <div class="col-lg-3">
                                <label label-for='name'>Longitude <span class="mandatory_label">*</span></label>
                                <input type="text" name="long" id="long" value="<?php echo (isset($buyerDetail) && isset($buyerDetail["long"]) ) ? $buyerDetail["long"]:'';?>" placeholder="Buyer Longitude" class="form-control">
                            </div>
                            <!-- <div class="col-lg-3">
                                <label label-for='name'>Bussiness Logo <span class="mandatory_label">*</span></label>
                                <input type="hidden" id="img_old" value="<?php //echo (isset($buyerDetail) && isset($buyerDetail["business_logo"]) )?$buyerDetail["business_logo"]:''; ?>" >

                                <img src="<?php //echo (isset($buyerDetail) && ($buyerDetail["business_logo"] != '') )?base_url('uploads/bussiness_logo/' . $buyerDetail["business_logo"]):base_url('assets/images/no-item-image.png'); ?>" height="150" width="150"  style="border: 2px dotted #ccc; border-radius: 2px;" id="chImg"/>


                                <input type="file" name="business_logo" id="business_logo" onchange="loadFile()">
                            </div> -->
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-12 text-right">
                                <input type="submit" value="Update" name="submit" class="btn btn-outline-primary">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<?php
	if(isset($custom_js)){
		foreach($custom_js as $val){
			?>
			<script src="<?php echo base_url();?>assets/js/custom_js/<?php echo $val;?>"></script>
			<?php
		}
	}
	
?>
  