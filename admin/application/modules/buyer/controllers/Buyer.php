<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class Buyer extends CI_Controller {
	protected $token_id='';
	protected $per_page=10;
	
    public function __construct() {
    	parent::__construct();
    	$this->token_id=get_cookie('token_id'); 
    	if(trim($this->token_id)==''){
    		redirect("login");
    	}
        $this->load->model("buyerModel");
        $this->load->model("userModel");
        $this->load->model("CustomerModel", 'cM');
        $this->load->helper("pagination");
        $this->load->library("pagination");
        $this->load->model("settingsModel", 'sM');
        $settings_data=$this->sM->getSettings();
        if(isset($settings_data->per_page)){
         $this->per_page=$settings_data->per_page;
        }
    }
    public function index()
	{
		try {
           $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            
            if(empty($user_details)){
                    $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                    delete_cookie('token_id'); 
                     delete_cookie('useName'); 
                    echo '<script>
                        alert("Unexpected server error.Please try again some time.");
                            window.location="'.base_url().'login";
                    </script>';
            }
            $user_details=current($user_details);
            $data=[]; 
            $data["menu"]="buyer";
            $data["useName"]=$user_details["Name"];
            $data["custom_js"]=['buyer.js'];
            
            set_cookie('useName',$user_details["Name"],'3600');

            $search_input=$this->input->get();
            //print_r($search_input); die();

            $buyer_search=[];
            
            if(isset($search_input["cust_id"]) && trim($search_input["cust_id"])!=''){
                $buyer_search["cust_id"] = base64_decode($search_input["cust_id"]);
            }
            if(isset($search_input["business_name"]) && trim($search_input["business_name"])!=''){
                $buyer_search["business_name"] = $search_input["business_name"];
            }
            if(isset($search_input["city"]) && trim($search_input["city"])!=''){
                $buyer_search["city"] = $search_input["city"];
            }
            
            $data["buyer_search"] = $buyer_search;
            $page=0;

            //echo $this->input->get("per_page"); die();

            if($this->input->get("per_page")){
                $page= $this->input->get("per_page");
            }
         
             $data["page"]=$page;
            $total_row=$this->buyerModel->getBuyerCount($buyer_search);
         
            $data["links"] = createPageNationLink(base_url('buyer/index'), $total_row,$this->per_page);

            //echo $total_row . '<br>' . $this->per_page . '<br>' . $data['links']; die();

            $start= (int)$page +1;
            $end = ($page+ $this->per_page >=$total_row)? $total_row : (int)$page  + $this->per_page;
            $data['result_count']='';
            if($total_row>0){
                $data['result_count']= "Showing ".$start." - ".$end." of ".$total_row." Results";
            }
            
            $data["buyer"]=$this->buyerModel->getBuyers($buyer_search,$this->per_page, $page);
			
            $data["customer"]=$this->buyerModel->getCustomer();
			
            $data['view_file'] = 'Buyer';

            // print_r($data); die();
            view($data);
           
        } catch (Exception $e) {
            $resposne=["status"=>"1","e"=>$e,"message"=>"Unexpected server error.Please try again some time.","field"=>'user_name'];
        }   
	}

    public function addBuyer() {
        try {
            $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            if(empty($user_details)){
                    $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                    delete_cookie('token_id'); 
                     delete_cookie('role_id'); 
                    echo '<script>
                        alert("Session expired please login again.");
                            window.location="'.base_url().'login";
                    </script>';
                    exit();
            }
            $user_details=current($user_details);
            $data=[]; 
            $data["menu"]="buyer";
            $data["useName"]=$user_details["Name"];
            $data["custom_js"]=['buyer.js'];
            
            set_cookie('useName',$user_details["Name"],'36000000');
            
            $data["customer"]=$this->buyerModel->getCustomer();

            $data['view_file'] = 'AddBuyer';
            view($data);
        } catch (Exception $e) {
            echo '<script>
                alert("Unexpected server error.Please try again some time.");
                            window.location="'.base_url().'buyer";
            </script>';

        }
    }

    public function doAddNew(){
        try {
            $data=$this->input->post();

            //print_r($_FILES);
            $resposne=["status"=>"0","message"=>"","field"=>''];
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));

            if(trim($this->token_id)!=''){
                if(isset($data["cust_id"]) && trim($data["cust_id"])==''){
                    $resposne=["status"=>"1","message"=>"Please Select Customer.","field"=>'cust_id'];
                }else if(isset($data["business_name"]) && trim($data["business_name"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter Bussiness Name.","field"=>'business_name'];
                }else if(isset($data["address"]) && trim($data["address"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter Address.","field"=>'address'];
                }else if(isset($data["state"]) && trim($data["state"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter State.","field"=>'state'];
                }else if(isset($data["city"]) && trim($data["city"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter City.","field"=>'city'];
                }else if(isset($data["lat"]) && trim($data["lat"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter Latitude.","field"=>'lat'];
                }else if(isset($data["long"]) && trim($data["long"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter Longitude.","field"=>'long'];
                }else if(isset($_FILES["business_logo"]) && trim($_FILES["business_logo"]['name'])==''){
                    $resposne=["status"=>"1","message"=>"Please Choose Logo Image.","field"=>'business_logo'];
                }else{

                    $check_duplicate_customer = $this->buyerModel->doBuyerCheck(base64_decode($data["cust_id"]));

                    if($check_duplicate_customer == 1) {
                        $resposne=["status"=>"1","message"=>"Duplicate Customer.","field"=>'cust_id'];
                    }
                    else {
                        $check_duplicate_profile = $this->buyerModel->doBuyerCheck('', $data["business_name"]);

                        if($check_duplicate_profile == 1) {
                            $resposne=["status"=>"1","message"=>"Duplicate Bussiness Name.","field"=>'business_name'];
                        }
                        else {
                            $file_name='';                        
                            $file_name=time().$_FILES["business_logo"]["name"];

                            $allowedExts = array("gif", "jpeg", "jpg", "png");
                            $image_data=explode(".", $_FILES["business_logo"]["name"]);
                            $extension =(!empty($image_data))?$image_data[1]:'';
                             
                            if ((($_FILES["business_logo"]["type"] == "image/gif")  || ($_FILES["business_logo"]["type"] == "image/jpeg")  || ($_FILES["business_logo"]["type"] == "image/jpg")  || ($_FILES["business_logo"]["type"] == "image/png")) && in_array($extension, $allowedExts)){
                                
                                $config['upload_path'] = './uploads/bussiness_logo/';
                               $config['allowed_types'] = 'jpg|png|jpeg|gif';
                                $config['file_name'] =$file_name;

                                $this->load->library('upload', $config);
                                if ( ! $this->upload->do_upload('business_logo')){
                                    $resposne=["status"=>"1","message"=>"Please Select jpg/jpeg/png/jpeg Logo Image.",'error'=> $this->upload->display_errors(),"field"=>'image'];
                                }
                                else {

                                    $buyerData = [
                                        'cust_id'       =>  base64_decode($data['cust_id']),
                                        'business_name' =>  $data['business_name'],
                                        'business_logo' =>  $file_name,
                                        'address'       =>  $data['address'],
                                        'state'         =>  $data['state'],
                                        'city'          =>  $data['city'],
                                        'lat'           =>  $data['lat'],
                                        'long'          =>  $data['long']
                                    ];

                                    $id = $this->buyerModel->insertBuyer($buyerData);
                                    if($id>0){
                                        $resposne=["status"=>"0","message"=>"Bussiness Profile added successfully.","field"=>'business_name'];
                                    }else{
                                        
                                        $resposne=["status"=>"1","message"=>"Bussiness Profile added failed.Please try again some time.","field"=>'business_name'];
                                        
                                    }
                                }    
                            }
                            else{
                                $resposne=["status"=>"1","message"=>"Please Select jpg/jpeg/png/jpeg Logo Image.",'error'=> '',"field"=>'image'];
                            }     
                            
                        }
                    }            

                }
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
            }
        } catch (Exception $e) {
                $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'business_name'];

        }

        echo json_encode($resposne);
    }

    public function getById() {
        try{
            $id = base64_decode($this->input->post('id'));

            $resposne=["status"=>"0","message"=>"","field"=>'', 'data'=> ''];

            $user_details = current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
            if(trim($this->token_id)!=''){
                $res = current($this->buyerModel->getById($id));

                $resposne=["status"=>"0","message"=>"Success.","field"=>'', 'data'=> $res];
            }
            else {
                $resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
            }
        }
        catch (Exception $e) {
            $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'role_id'];
        }

        echo json_encode($resposne);
    }

    /*public function doBuyerEdit($id) {
        $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            
        if(empty($user_details)){
                $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                delete_cookie('token_id'); 
                 delete_cookie('role_id'); 
                echo '<script>
                    alert("Session expired please login again.");
                        window.location="'.base_url().'login";
                </script>';
                exit();
        }
        $user_details=current($user_details);
        $id=base64_decode($id);
        if(trim($id)==''){
            echo '<script>
                    alert("Invalid user.");
                        window.location="'.base_url().'buyer";
                </script>';
                exit();
        }
        $data=[]; 
        $data["menu"]="buyer";
        $data["useName"]=$user_details["Name"];
        $data["custom_js"]=['buyer.js'];
        set_cookie('useName',$user_details["Name"],'3600');
        $data['buyerDetail'] = current($this->buyerModel->getById($id));
        $data["customer"]=$this->buyerModel->getCustomer();
         
        $data['view_file'] = 'EditBuyer';
        view($data);
    }*/

    /*public function doUpdateBuyer(){
        $data=$this->input->post();

        $resposne=["status"=>"0","message"=>"","field"=>''];
        
        try {
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
            if(trim($this->token_id)!=''){
                if(isset($data["id"]) && trim($data["id"])==''){
                    $resposne=["status"=>"1","message"=>"Invalid Request.","field"=>'business_name'];
                }else{
                    $id=base64_decode($data["id"]);
                    $buyer_details=current($this->buyerModel->getById($id));
                    if(empty($buyer_details)){
                         $resposne=["status"=>"1","message"=>"Invalid Request.","field"=>'business_name'];
                    }
                    else{

                        if(isset($data["cust_id"]) && trim($data["cust_id"])==''){
                            $resposne=["status"=>"1","message"=>"Please Select Customer.","field"=>'cust_id'];
                        }else if(isset($data["business_name"]) && trim($data["business_name"])==''){
                            $resposne=["status"=>"1","message"=>"Please Enter Bussiness Name.","field"=>'business_name'];
                        }else if(isset($data["address"]) && trim($data["address"])==''){
                            $resposne=["status"=>"1","message"=>"Please Enter Address.","field"=>'address'];
                        }else if(isset($data["state"]) && trim($data["state"])==''){
                            $resposne=["status"=>"1","message"=>"Please Enter State.","field"=>'state'];
                        }else if(isset($data["city"]) && trim($data["city"])==''){
                            $resposne=["status"=>"1","message"=>"Please Enter City.","field"=>'city'];
                        }else if(isset($data["lat"]) && trim($data["lat"])==''){
                            $resposne=["status"=>"1","message"=>"Please Enter Latitude.","field"=>'lat'];
                        }else if(isset($data["long"]) && trim($data["long"])==''){
                            $resposne=["status"=>"1","message"=>"Please Enter Longitude.","field"=>'long'];
                        }else if(!empty($_FILES["business_logo"]) && trim($_FILES["business_logo"]['name'])=='' && trim($buyer_details["business_logo"])==''){
                            $resposne=["status"=>"1","message"=>"Please Choose Logo Image.","field"=>'business_logo'];
                        }
                        else{
                            $check_duplicate_customer = $this->buyerModel->doBuyerCheck(base64_decode($data["cust_id"]), '', $id);

                            if($check_duplicate_customer == 1) {
                                $resposne=["status"=>"1","message"=>"Duplicate Customer.","field"=>'cust_id'];
                            }
                            else {
                                $check_duplicate_profile = $this->buyerModel->doBuyerCheck('', $data["business_name"], $id);

                                if($check_duplicate_profile == 1) {
                                    $resposne=["status"=>"1","message"=>"Duplicate Bussiness Name.","field"=>'business_name'];
                                }
                                else {
                            
                                    if(!empty($_FILES["business_logo"]) && trim($_FILES["business_logo"]["name"])!=''){

                                        $file_name=time().$_FILES["business_logo"]["name"];

                                        $allowedExts = array("gif", "jpeg", "jpg", "png");
                                        $image_data=explode(".", $_FILES["business_logo"]["name"]);
                                        $extension =(!empty($image_data))?$image_data[1]:'';
                                         
                                        if ((($_FILES["business_logo"]["type"] == "image/gif")  || ($_FILES["business_logo"]["type"] == "image/jpeg")  || ($_FILES["business_logo"]["type"] == "image/jpg")  || ($_FILES["business_logo"]["type"] == "image/png")) && in_array($extension, $allowedExts) ) 
                                        {
                                            $config['upload_path'] = './uploads/bussiness_logo/';
                                             $config['allowed_types'] = 'jpg|png|jpeg|gif';
                                            $config['file_name'] =$file_name;
                                                    
                                            $this->load->library('upload', $config);
                                            if ( ! $this->upload->do_upload('business_logo')){
                                                $resposne=["status"=>"1","message"=>"Please Select jpg/jpeg/png/jpeg Image.",'error'=> $this->upload->display_errors(),"field"=>'business_logo'];
                                            }
                                            else{
                                                if(file_exists('./uploads/bussiness_logo/'.$buyer_details["business_logo"])){
                                                    unlink('./uploads/bussiness_logo/'.$buyer_details["business_logo"]);
                                                }
                                            }

                                            
                                        }
                                        else{
                                            $resposne=["status"=>"1","message"=>"Please Select jpg/jpeg/png/jpeg Logo Image.",'error'=> '',"field"=>'image'];
                                        } 
                                    }
                                    else {
                                         $file_name=$buyer_details["business_logo"];
                                    }

                                    $buyerData = [
                                        'cust_id'       =>  base64_decode($data['cust_id']),
                                        'business_name' =>  $data['business_name'],
                                        'business_logo' =>  $file_name,
                                        'address'       =>  $data['address'],
                                        'state'         =>  $data['state'],
                                        'city'          =>  $data['city'],
                                        'lat'           =>  $data['lat'],
                                        'long'          =>  $data['long'],
                                    ];

                                    $id = $this->buyerModel->updateBuyer($buyerData, $id);

                                    if($id>0){
                                        $resposne=["status"=>"0","message"=>"Bussiness Profile Updated successfully.","field"=>'business_name'];
                                    }else{
                                        
                                        $resposne=["status"=>"1","message"=>"Bussiness Profile Updated failed.Please try again some time.","field"=>'business_name'];
                                        
                                    }
                                }
                            }
                        }
                    }
                }
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
            }
        }catch (Exception $e) {
                $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'user_name','error'=>$e];

        }

        echo json_encode($resposne);
    }*/

    public function doBuyerStatusChange(){
        $data=$this->input->post();
        //print_r($data);
        $id=base64_decode($data["id"]);
        $resposne=["status"=>"0","message"=>"","field"=>''];
        try {
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
            if(trim($this->token_id)!=''){
                $res = $this->buyerModel->updateBuyer(["active"=>$data["status"], 'update_by' => 1, 'update_on' => date('Y-m-d H:i:s')],$id);

                // echo $res; die();
              
                if(!$res){
                    $resposne=["status"=>"1","message"=>"Buyer change failed.Please try again after some time."];
                   // 
                }
                else{
                    $cRes = $this->cM->updateCust(["active"=>$data["status"], 'update_by' => 1, 'update_on' => date('Y-m-d H:i:s')],$res);

                    if ($cRes) {
                        $resposne=["status"=>"0","message"=>"Buyer status changed successfully."];
                    }
                    else {
                        $resposne=["status"=>"1","message"=>"Customer Status Change failed.Please try again after some time."];
                    }
                }
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
            }   
        }catch (Exception $e) {
                $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'status'];

        }

        echo json_encode($resposne);
    }

}
?>