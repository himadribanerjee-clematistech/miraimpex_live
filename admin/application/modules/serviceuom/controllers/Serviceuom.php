<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class Serviceuom extends CI_Controller {
	protected $token_id='';
	protected $per_page=10;
	public function __construct() {
    	parent::__construct();
    	$this->token_id=get_cookie('token_id'); 
    	if(trim($this->token_id)==''){
    		redirect("login");
    	}
        $this->load->model("serviceuomModel");
        $this->load->model("userModel");
        $this->load->helper("pagination");
        $this->load->library("pagination");
         $this->load->model("settingsModel", 'sM');
        $settings_data=$this->sM->getSettings();
        if(isset($settings_data->per_page)){
         $this->per_page=$settings_data->per_page;
        }
    }
    public function index() {
		try {
           $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            
            if(empty($user_details)){
                    $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                    delete_cookie('token_id'); 
                     delete_cookie('useName'); 
                    echo '<script>
                        alert("Unexpected server error.Please try again some time.");
                            window.location="'.base_url().'login";
                    </script>';
            }
            $user_details=current($user_details);
            $data=[]; 
            $data["menu"]="serviceuom";
            $data["useName"]=$user_details["Name"];
            $data["custom_js"]=['serviceuom.js'];
            set_cookie('useName',$user_details["Name"],'3600');
            //search 
            $search_input=$this->input->get();
            //print_r($search_input); die();

            $uom_search=[];
            
            if(isset($search_input["uomName"]) && trim($search_input["uomName"])!=''){
            	$uom_search["uom_name"] = $search_input["uomName"];
            }
            if(isset($search_input["status"]) && trim($search_input["status"])!=''){
                $uom_search["status"] = $search_input["status"];
            }
            
            $data["uom_search"] = $uom_search;
            $page=0;
			if($this->input->get("per_page")){
	       		$page= $this->input->get("per_page");
	       	}

            $data["page"]=$page;

            $total_row=$this->serviceuomModel->getuomCount($uom_search);
         
	        $data["links"] = createPageNationLink(base_url('serviceuom/index'), $total_row,$this->per_page);

	        $start= (int)$page +1;
			$end = ($page+ $this->per_page >=$total_row)? $total_row : (int)$page  + $this->per_page;
			$data['result_count']='';
			if($total_row>0){
				$data['result_count']= "Showing ".$start." - ".$end." of ".$total_row." Results";
			}
			
			$data["uom_data"]=$this->serviceuomModel->getuomList($uom_search, $this->per_page, $page);

            //echo '<pre>'; print_r($data); die();

            $data['view_file'] = 'Serviceuom';
            view($data);
           
        } catch (Exception $e) {
            $resposne=["status"=>"1","e"=>$e,"message"=>"Unexpected server error.Please try again some time.","field"=>'user_name'];
        }   
	}
	
    public function addUom(){
        try {
            $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            if(empty($user_details)){
                    $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                    delete_cookie('token_id'); 
                     delete_cookie('role_id'); 
                    echo '<script>
                        alert("Session expired please login again.");
                            window.location="'.base_url().'login";
                    </script>';
                    exit();
            }
            $user_details=current($user_details);
            $data=[]; 
            $data["menu"]="serviceuom";
            $data["useName"]=$user_details["Name"];
            $data["custom_js"]=['serviceuom.js'];
            set_cookie('useName',$user_details["Name"],'3600');
             $data['view_file'] = 'AddServiceuom';
            view($data);
        } catch (Exception $e) {
            echo '<script>
                alert("Unexpected server error.Please try again some time.");
                            window.location="'.base_url().'category";
            </script>';

        }
    }

    public function doAddUom(){
        try {
            $data=$this->input->post();
            $resposne=["status"=>"0","message"=>"", "field"=>''];
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));

            //print_r($data); die();

            if(trim($this->token_id)!=''){
                if(isset($data["uom_name"]) && trim($data["uom_name"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter Unit of Measurement Name.","field"=>'uom_name'];
                }
                else{
                    $check_duplicate_uomname = $this->serviceuomModel->doUomCheck($data["uom_name"]);

                    if($check_duplicate_uomname == 1) {
                        $resposne=["status"=>"1","message"=>"Duplicate Unit of Measurement Name.","field"=>'uom_name'];
                    }
                    else {
                        
                        $cat_data=['name'=>$data["uom_name"], 'type' => '2'];
                        $res = $this->serviceuomModel->insertUom($cat_data);

                        if($res){
                            $resposne=["status"=>"0","message"=>"Unit of Measurement added successfully."];
                        }
                        else{                                
                            $resposne=["status"=>"1","message"=>"Unit of Measurement added failed.Please try again after some time."];                                
                        }
                    }
                }
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied."];
            }
        } catch (Exception $e) {
            $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time."];

        }

        echo json_encode($resposne);
    }

    public function doUomEdit($id){
        $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            
        if(empty($user_details)){
                $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                delete_cookie('token_id'); 
                 delete_cookie('role_id'); 
                echo '<script>
                    alert("Session expired please login again.");
                        window.location="'.base_url().'login";
                </script>';
                exit();
        }
        $user_details=current($user_details);
        $id=base64_decode($id);
        if(trim($id)==''){
            echo '<script>
                    alert("Invalid user.");
                        window.location="'.base_url().'user";
                </script>';
                exit();
        }
        $data=[]; 
        $data["menu"]="serviceuom";
        $data["useName"]=$user_details["Name"];
        $data["custom_js"]=['serviceuom.js'];
        set_cookie('useName',$user_details["Name"],'3600');
        $data['uom_detail'] = current($this->serviceuomModel->getById($id));
         
        $data['view_file'] = 'EditServiceuom';
        view($data);
            
    }
    public function UpdateuomDetails(){

        try {
            $data=$this->input->post();
            $resposne=["status"=>"0","message"=>"", "field"=>''];
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));

            if(trim($this->token_id)!=''){
                if(isset($data["uom_name"]) && trim($data["uom_name"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter Unit of Measurement Name.","field"=>'uom_name'];
                }
                else{
                    $check_duplicate_uomname = $this->serviceuomModel->doUomCheck($data["uom_name"], $data["id"]);

                    if($check_duplicate_uomname == 1) {
                        $resposne=["status"=>"1","message"=>"Duplicate Unit of Measurement Name.","field"=>'uom_name'];
                    }
                    else {
                        
                        $uom_data=['name'=>$data["uom_name"]];
                        $res = $this->serviceuomModel->updateUom($uom_data, $data["id"]);

                        if($res){
                            $resposne=["status"=>"0","message"=>"Unit of Measurement Updated successfully."];
                        }
                        else{                                
                            $resposne=["status"=>"1","message"=>"Unit of Measurement Updation failed.Please try again after some time."];                                
                        }
                    }
                }
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied."];
            }
        } catch (Exception $e) {
            $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time."];

        }

        echo json_encode($resposne);
    }

    public function doUomStatusChange(){
        $data=$this->input->post();
        $id=base64_decode($data["id"]);
        $resposne=["status"=>"0","message"=>"","field"=>''];
        try {
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
            if(trim($this->token_id)!=''){
                $res = $this->serviceuomModel->updateUom(["active"=>$data["active"]],$id);
              
                if($res){
                    $resposne=["status"=>"0","message"=>"Unit of Measurement status change successfully."];
                }
                else{                                
                    $resposne=["status"=>"1","message"=>"Unit of Measurement status change failed.Please try again after some time."];                                
                }
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
            }   
        }catch (Exception $e) {
                $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'status'];

        }

        echo json_encode($resposne);
    }
}
?>