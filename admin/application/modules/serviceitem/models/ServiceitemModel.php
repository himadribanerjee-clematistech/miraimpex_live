<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class ServiceitemModel extends CI_Model {
	protected $itemTable = 'service_item';
	protected $catTable = 'service_category';
	protected $uomTable = 'uom';
	
	public function __construct() {
        parent::__construct();
    }
    
    public function getitemCount($data){
    	try {
    		$this->db->db_debug = false;

    		//SELECT si.service_item_id, si.name, si.image, sc.service_category_name, sc.active FROM t_service_item si JOIN t_service_category sc ON si.service_category_id = sc.id WHERE sc.active = 1

    		$this->db->select("si.service_item_id, si.name, si.image, sc.service_category_name, sc.active");
    		$this->db->from($this->itemTable . ' si');
    		$this->db->join($this->catTable . ' sc', 'si.service_category_id = sc.id');
			$this->db->where("sc.active",1);
	    	
	    	if(isset($data["item_name"]) && $data["item_name"]!=''){
	    		$this->db->like("si.name",$data["item_name"]);
	    	}
	    	if(isset($data["actve"]) && $data["actve"]!=''){ 
	    		$this->db->where("sc.active",$data["actve"]);

	    	}
	    	if(isset($data["category_id"]) && $data["category_id"]!=''){
	    		$this->db->where('sc.id', $data["category_id"]);
	    	}

	    	//$this->db->get();
	    	//echo $this->db->last_query();die();

	    	$db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	return $this->db->count_all_results();

	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
    }
    public function getitemList($data,$limit='', $start=0){
    	try {
    		$this->db->db_debug = false;

    		//SELECT si.service_item_id, si.name, si.image, sc.service_category_name, sc.active, si.status FROM t_service_item si JOIN t_service_category sc ON si.service_category_id = sc.id WHERE sc.active = 1

    		$this->db->select("si.service_item_id, si.name, si.image, sc.service_category_name, sc.active, si.status");
    		$this->db->from($this->itemTable . ' si');
    		$this->db->join($this->catTable . ' sc', 'si.service_category_id = sc.id');
			$this->db->where("sc.active",1);
	    	
	    	if(isset($data["item_name"]) && $data["item_name"]!=''){
	    		$this->db->like("si.name",$data["item_name"]);
	    	}
	    	if(isset($data["actve"]) && $data["actve"]!=''){ 
	    		$this->db->where("sc.active",$data["actve"]);

	    	}
	    	if(isset($data["category_id"]) && $data["category_id"]!=''){
	    		$this->db->where('sc.id', $data["category_id"]);

	    	}

	    	$this->db->order_by('si.name', 'ASC'); 
	    	if($limit!=''  && $start>=0){
	    		$this->db->limit($limit, $start);
	    	}
	    	
 
 			$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query();die();
	        	$result=[];
	        	foreach($query->result_array() as $row){
	        		$result[]=$row;
	        	}
	        	return $result;
	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
    }	

    public function doitemCheck($data, $id=''){
    	//print_r($data); die();
		try {
		 	$this->db->db_debug = FALSE;
	        $this->db->select("name");
	    	$this->db->from($this->itemTable);
	    	if(isset($data) && trim($data)!='' ){
	    		$this->db->where("name",$data);
	    		
	    	}

	    	if($id!=''){
	    		$this->db->where("service_item_id !=",$id);
	    	}
	    	
	    	$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query() . '<br>';
	        	if($query->num_rows() >0) {
	        		return True;
	        	}
	        	else {
	        		return false;
	        	}
	        }
	        //return TRUE;
	    } catch (Exception $e) {
	       throw new Exception();
	    }	
    }
     
    public function insertitem($data){
     	//print_r($data); die();
    	try {
    	 	$this->db->db_debug = FALSE;
    	 	$res = $this->db->insert($this->itemTable, $data);

    	 	$db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }
	        else {
	        	//echo $this->db->last_query() . '<br>';
	        	if ($res) {
	        		return true;
	        	}
	        	else {
	        		return false;
	        	}
	        }
	        //return  $this->db->insert_id();
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }
    	
    }

    public function getById($id){
    	try {
		 	$this->db->db_debug = false;

		 	//SELECT service_item_id, service_category_id, name, image, description FROM t_service_item WHERE service_item_id = 4

	        $this->db->select("service_item_id, service_category_id, name, image, description");
	    	$this->db->from($this->itemTable);		
	    	
	    	$this->db->where("service_item_id", $id);
	    	
	    	$query=$this->db->get();

	    	//echo $this->db->last_query(); die();

	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	$data=[];
	        	foreach($query->result_array() as $row){
	        		$data[]=$row;	    	
	        	}
	        	
	        	return $data;
	        }
	        return TRUE;
	    } catch (Exception $e) {
	       throw new Exception();
	    }	
    }

    public function updateItem($data,$where){
    	try {
    	 	$this->db->db_debug = False;
    	 	$this->db->trans_start(FALSE);
	        $this->db->where('service_item_id', $where);
	    	$this->db->update($this->itemTable, $data);
	        $this->db->trans_complete();
	        $db_error = $this->db->error();
	            if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	           
	        }

	        //echo $this->db->last_query();die();
	        return TRUE;
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }
    }

    /*public function doDeleteitem($where){
    	
    	try {
    	 	$this->db->db_debug = False;
    	 	$res = $this->db->delete($this->itemTable, ['id' => $where]);

	    	$db_error = $this->db->error();
	        if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }
	        else {
	        	if ($res) {
	        		return True;
	        	}
	        	else {
	        		return False;
	        	}
	        }
	        
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }
    }*/
   	
}

?>