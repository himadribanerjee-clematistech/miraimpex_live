<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="clearfix mb-10">
        <div class="pull-left">
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo base_url('serviceitem')?>">Service Item</a></li>
                    <li class="breadcrumb-item active" aria-current="page">List</li>
                </ol>
            </nav> 
        </div>
        <div class="pull-right">
            <a class="btn btn-outline-primary" href="<?php echo base_url('serviceitem/additem')?>">Add New</a>            
        </div>
    </div>

    <div class="container-fluid bg-light"> 
        <div class="row">
            <div class="col-lg-12">

                <form id="catsrch" action="<?php echo base_url('serviceitem')?>">
                    <input type="hidden" name="per_page_search" value="<?php echo $page; ?>">

                    <div class="row align-items-center justify-content-center">
                        <div class="col-md-3 pt-3">
                            <div class="form-group ">
                                <input type="text" name="itemName" id="item_name" value="<?php echo (isset($item_search["item_name"]))?$item_search["item_name"]:'';?>" placeholder="Item Name" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-3 pt-3">
                            <div class="form-group">
                                <select name="category" id="category" class="form-control">
                                    <option value=""> -- select Category-- </option>

                                <?php

                                    foreach ($service_cat_details as $cat) {
                                        
                                ?>

                                    <option value="<?php echo base64_encode($cat["id"]); ?>"
                                        <?php 
                                            echo (isset($item_search) && isset($item_search["category_id"]) && ($item_search["category_id"] == $cat["id"]) )? 'selected':'';
                                        ?>

                                    > <?php echo $cat["service_category_name"]; ?> </option>

                                <?php

                                    }

                                ?>

                                </select>
                            </div>
                        </div>
                        <div class="col-md-3 pt-3">
                            <div class="form-group ">
                                <select id="actve" name="status" class="form-control">
                                    <option value="">Select Status</option>
                                    <option value="1" <?php echo (isset($item_search) && isset($item_search["actve"]) &&  $item_search["actve"]==1)?'selected':'';?>>Active</option>
                                    <option value="0"  <?php echo (isset($item_search) && isset($item_search["actve"]) &&  $item_search["actve"]==0)?'selected':'';?>>InActive</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <a class="btn btn-outline-primary" href="<?php echo base_url('serviceitem')?>"><i class="fa fa-refresh" aria-hidden="true"></i> Reset</a> 
                                
                            <input type="submit" name="search" value="Search" class="btn btn-outline-primary">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <div class="row pt-2">
        
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="data-table stripe hover no-wrap" style="font-size: 14px;">

            <thead>
                <tr class="titel_bar">
                    <th class="table-plus datatable-nosort" width="5%">Sl No.</th>
                    <th class="table-plus datatable-nosort" width="30%">Item Name</th>
                    <th class="table-plus datatable-nosort" width="15%">Category</th>
                    <th class="table-plus datatable-nosort" width="20%">Image</th>
                    <th class="table-plus datatable-nosort" width="20%">Status</th>
                    <th class="table-plus datatable-nosort" width="30%" ><center>Action</center></th>
             </tr>
            </thead>
            <tbody>

            <?php
            	if(!empty($item_data)){
            		$i=0;
            		foreach($item_data as $val){
            			$i++;
            			?>
            			 <tr class="">
							<td ><?php echo $i;?></td>	
							<td >
                                <?php echo $val["name"];?>           
                            </td>
                            <td >
                                <?php echo $val["service_category_name"];?>           
                            </td>	
                            
                            <td >
                                <img src="<?php echo (isset($val) && ($val["image"] != '') )?base_url('uploads/serviceitem/' . $val["image"]):base_url('assets/images/no-item-image.png'); ?>" width='100' height='100'>
                                          
                            </td>
                            <td>

                               <select id="actve" name="status" onchange="doItemStatusChange(this.value,'<?php echo base64_encode($val["service_item_id"]);?>');" class="form-control">
                                    <option value="1" <?php echo ($val["status"]==1)?'selected':'';?>>Active</option>
                                     <option value="0"  <?php echo ($val["status"]==0)?'selected':'';?>>InActive</option>
                                </select>   

                            </td>

                            <td class="table-plus" style="text-align:center;">

                                <div class="dropdown">
                                    <a class="btn btn-outline-primary dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                        <i class="fa fa-ellipsis-h"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item" href="javascript:void(0);" style=" font-size: 15px;" onclick="doitemEdit('<?php echo base64_encode($val["service_item_id"]);?>')" >
                                            <i class="fa fa-edit" aria-hidden="true"></i> 
                                            Edit
                                        </a>
                                    </div>
                                </div>

                            </td>
                            

            		 	</tr>
            			<?php
            		}
            	}else{
            		?>
            		 <tr class="">
            		 	 <td colspan="7">No Record Found</td>	

            		 </tr>	
            		<?php
            	}
            ?>
           
                 
            </tbody>
    </table>
     <div class="col-sm-12">
            <div style="float: right;"><?php echo $links; ?></div>
        </div>
</div>
<?php
	if(isset($custom_js)){
		foreach($custom_js as $val){
			?>
			<script src="<?php echo base_url();?>assets/js/custom_js/<?php echo $val;?>"></script>
			<?php
		}
	}
	
?>
  