<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class Serviceitem extends CI_Controller {
	protected $token_id='';
	protected $per_page=10;
	public function __construct() {
    	parent::__construct();
    	$this->token_id=get_cookie('token_id'); 
    	if(trim($this->token_id)==''){
    		redirect("login");
    	}
        $this->load->model("serviceitemModel");
        $this->load->model("userModel");
        $this->load->model('servicecategoryModel');
        
        $this->load->helper("pagination");
        $this->load->library("pagination");
         $this->load->model("settingsModel", 'sM');
        $settings_data=$this->sM->getSettings();
        if(isset($settings_data->per_page)){
         $this->per_page=$settings_data->per_page;
        }
    }
    public function index()
	{
		try {
           $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            
            if(empty($user_details)){
                    $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                    delete_cookie('token_id'); 
                     delete_cookie('useName'); 
                    echo '<script>
                        alert("Unexpected server error.Please try again some time.");
                            window.location="'.base_url().'login";
                    </script>';
            }
            $user_details=current($user_details);
            $data=[]; 
            $data["menu"]="serviceitem";
            $data["useName"]=$user_details["Name"];
            $data["custom_js"]=['serviceitem.js'];
            set_cookie('useName',$user_details["Name"],'3600');
            $data['service_cat_details'] = $this->servicecategoryModel->getServiceCatList(["active"=>1]);

           
            //search 
            $search_input=$this->input->get();
            //print_r($search_input); die();

            $item_search=[];
            $data["subcat_data"]=[];
            if(isset($search_input["itemName"]) && trim($search_input["itemName"])!=''){
              $item_search["item_name"] = $search_input["itemName"];
            }

            if(isset($search_input["category"]) && trim($search_input["category"])!=''){
              $item_search["category_id"] = base64_decode($search_input["category"]);
            }

            if(isset($search_input["status"]) && trim($search_input["status"])!=''){
              $item_search["actve"]=$search_input["status"];
            }
            $data["item_search"] = $item_search;
            $page=0;
			   if($this->input->get("per_page")){
	       		$page= $this->input->get("per_page");
	       	}

          $data["page"]=$page;

            $total_row=$this->serviceitemModel->getitemCount($item_search);
         
	        $data["links"] = createPageNationLink(base_url('serviceitem/index'), $total_row,$this->per_page);

	        $start= (int)$page +1;
			$end = ($page+ $this->per_page >=$total_row)? $total_row : (int)$page  + $this->per_page;
			$data['result_count']='';
			if($total_row>0){
				$data['result_count']= "Showing ".$start." - ".$end." of ".$total_row." Results";
			}
			
			$data["item_data"]=$this->serviceitemModel->getitemList($item_search,$this->per_page, $page);

            //echo '<pre>'; print_r($data); die();

            $data['view_file'] = 'Serviceitem';
            view($data);
           
        } catch (Exception $e) {
            $resposne=["status"=>"1","e"=>$e,"message"=>"Unexpected server error.Please try again some time.","field"=>'user_name'];
        }   
	}
	
  public function additem(){
    try {
        $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
        if(empty($user_details)){
                $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                delete_cookie('token_id'); 
                 delete_cookie('role_id'); 
                echo '<script>
                    alert("Session expired please login again.");
                        window.location="'.base_url().'login";
                </script>';
                exit();
        }
        $user_details=current($user_details);
        $data=[]; 
        $data["menu"]="serviceitem";
        $data["useName"]=$user_details["Name"];
        $data["custom_js"]=['serviceitem.js'];
        set_cookie('useName',$user_details["Name"],'3600');
        $data['service_cat_details'] = $this->servicecategoryModel->getServiceCatList(["active"=>1]);
         //$data['uom_details'] = $this->serviceitemModel->getUom();

        $data['view_file'] = 'AddServiceitem';
        view($data);
    } catch (Exception $e) {
        echo '<script>
            alert("Unexpected server error.Please try again some time.");
                        window.location="'.base_url().'category";
        </script>';

    }
  }
  public function doAddItem(){

    try {
      $data=$this->input->post();
      $resposne=["status"=>"0","message"=>"","field"=>''];
      $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));

      if(trim($this->token_id)!=''){
          if(isset($data["item_name"]) && trim($data["item_name"])==''){
              $resposne=["status"=>"1","message"=>"Please Enter Item Name.","field"=>'item_name'];
          }else if(isset($data["category"]) && trim($data["category"])==''){
              $resposne=["status"=>"1","message"=>"Please Select Category.","field"=>'cat_name'];
          }else if(isset($data["item_description"]) && trim($data["item_description"])==''){
              $resposne=["status"=>"1","message"=>"Please Enter Item Description.","field"=>'item_description'];
          }else if(!empty($_FILES["item_image"]) && trim($_FILES["item_image"]["name"])==''){
              $resposne=["status"=>"1","message"=>"Please Select Item Image.","field"=>'item_image'];
          }else{

             $check_duplicate_itemname = $this->serviceitemModel->doitemCheck($data["item_name"]);


              if($check_duplicate_itemname == 1) {
                  $resposne=["status"=>"1","message"=>"Duplicate Item Name.","field"=>'item_name'];
              }
              if($resposne["status"]==0){
                  $file_name=time().$_FILES["item_image"]["name"];

                  $allowedExts = array("gif", "jpeg", "jpg", "png");
                  $image_data=explode(".", $_FILES["item_image"]["name"]);
                  $extension =(!empty($image_data))?$image_data[1]:'';
                   
                  if ((($_FILES["item_image"]["type"] == "image/gif")  || ($_FILES["item_image"]["type"] == "image/jpeg")  || ($_FILES["item_image"]["type"] == "image/jpg")  || ($_FILES["item_image"]["type"] == "image/png")) && in_array($extension, $allowedExts))
                  {
                        $config['upload_path'] = './uploads/serviceitem/';
                         $config['allowed_types'] = 'jpg|png|jpeg|gif';
                        // $config['max_size'] = '200';
                       // $config['max_width'] = '800';
                       // $config['max_height'] = '800';
                        $config['file_name'] =$file_name;
                      
                        $this->load->library('upload', $config);
                        if ( ! $this->upload->do_upload('item_image')){
                          $resposne=["status"=>"1","message"=>"Please Select  jpg/png/jpeg/gif Item Image.",'error'=> $this->upload->display_errors(),"field"=>'item_image'];
                        
                        }else{

                          $string = $data['item_name'];
                          $searchStringComma = ',';
                          $searchStringHyphen = '-';
                          $new_string = '';

                          if( strpos($string, $searchStringComma) !== false ) {
                              $new_string = str_replace(","," ", $string);
                          }

                          if ($new_string != '') { //If New String is not Blank
                            if( strpos($new_string, $searchStringHyphen) !== false ) {
                                $new_string = str_replace("-"," ", $new_string);
                                $new_string = preg_replace('!\s+!', ' ', $new_string);
                                
                                $slug = strtolower(str_replace(" ","-", $new_string));
                            }
                            else {
                              $new_string = preg_replace('!\s+!', ' ', $new_string);
                              $slug = strtolower(str_replace(" ","-", $new_string));
                            }
                          }
                          else { //If New String is Blank
                            if( strpos($string, $searchStringHyphen) !== false ) {
                                $new_string = str_replace("-"," ", $string);
                                $new_string = preg_replace('!\s+!', ' ', $new_string);
                                
                                $slug = strtolower(str_replace(" ","-", $new_string));
                            }
                            else {
                              $new_string = preg_replace('!\s+!', ' ', $string);
                              $slug = strtolower(str_replace(" ","-", $new_string));
                            }
                          }



                           $item_data = [
                              'name' => $data['item_name'], 
                              'description' => $data['item_description'], 
                              'service_category_id' => base64_decode($data['category']),
                              'image' => $file_name,
                              'slug' => $slug
                            ];

                            //print_r($item_data); die();

                            $id = $this->serviceitemModel->insertitem($item_data);
                             if($id>0){
                               $resposne=["status"=>"0","message"=>" Item added successfully.","field"=>'item_name'];
                              }else{
                                  
                                  $resposne=["status"=>"1","message"=>"Item added failed.Please try again some time.","field"=>'item_name'];
                                  
                              }
                        }

                  }
                  else {
                    $resposne=["status"=>"1","message"=>"Please Select  jpg/png/jpeg/gif Item Image.",'error'=> '',"field"=>'image'];
                  }
                  
              }

          }
      }else{
          $resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
      }
    } catch (Exception $e) {
          $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'item_name'];

    }

      echo json_encode($resposne);

  }
    

    public function doitemEdit($id){
        $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            
        if(empty($user_details)){
                $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                delete_cookie('token_id'); 
                 delete_cookie('role_id'); 
                echo '<script>
                    alert("Session expired please login again.");
                        window.location="'.base_url().'login";
                </script>';
                exit();
        }
        $user_details=current($user_details);
        $id=base64_decode($id);
        if(trim($id)==''){
            echo '<script>
                    alert("Invalid user.");
                        window.location="'.base_url().'user";
                </script>';
                exit();
        }
        $data=[]; 
        $data["menu"]="serviceitem";
        $data["useName"]=$user_details["Name"];
        $data["custom_js"]=['serviceitem.js'];
        set_cookie('useName',$user_details["Name"],'3600');

        $data['item_detail'] = current($this->serviceitemModel->getById($id));
        $data['service_cat_details'] = $this->servicecategoryModel->getServiceCatList(["active"=>1]);

       // echo '<pre>'; print_r($data); die();
         
        $data['view_file'] = 'EditServiceitem';
        view($data);
            
    }
    public function doUpdateItem(){
        $data=$this->input->post();
        //print_r($data); die();
         $resposne=["status"=>"0","message"=>"","field"=>''];
        
        try {
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
            if(trim($this->token_id)!=''){
                    if(isset($data["id"]) && trim($data["id"])==''){
                         $resposne=["status"=>"1","message"=>"Invalid Request.","field"=>'cat_name'];
                    }else{
                        $id=base64_decode($data["id"]);
                        $category_details=current($this->serviceitemModel->getById($id));
                        //print_r($category_details); die();

                        if(empty($category_details)){
                             $resposne=["status"=>"1","message"=>"Invalid Request.","field"=>'cat_name'];
                         }else{

                           if(isset($data["item_name"]) && trim($data["item_name"])==''){
                            $resposne=["status"=>"1","message"=>"Please Enter Item Name.","field"=>'item_name'];
                            }else if(isset($data["category"]) && trim($data["category"])==''){
                                $resposne=["status"=>"1","message"=>"Please Select Category.","field"=>'cat_name'];
                            }else if(isset($data["item_description"]) && trim($data["item_description"])==''){
                                $resposne=["status"=>"1","message"=>"Please Enter Item Description.","field"=>'item_description'];
                            }else if(!empty($_FILES["item_image"]) && trim($_FILES["item_image"]["name"])=='' && trim($category_details["image"])==''){
                                $resposne=["status"=>"1","message"=>"Please Select Item Image.","field"=>'image'];
                            }else{

                                     $check_duplicate_catname = $this->serviceitemModel->doitemCheck($data["item_name"],$id);

                                    if(!empty($check_duplicate_catname)){
                                        $resposne=["status"=>"1","message"=>"Duplicate Item Name.","field"=>'item_name'];
                                    }
                                    if($resposne["status"]==0){

                                        if(!empty($_FILES["item_image"]) && trim($_FILES["item_image"]["name"])!=''){

                                              $file_name=time().$_FILES["item_image"]["name"];

                                              $allowedExts = array("gif", "jpeg", "jpg", "png");
                                              $image_data=explode(".", $_FILES["item_image"]["name"]);
                                              $extension =(!empty($image_data))?$image_data[1]:'';
                                               
                                              if ((($_FILES["item_image"]["type"] == "image/gif")  || ($_FILES["item_image"]["type"] == "image/jpeg")  || ($_FILES["item_image"]["type"] == "image/jpg")  || ($_FILES["item_image"]["type"] == "image/png")) && in_array($extension, $allowedExts))
                                              {



                                                $config['upload_path'] = './uploads/serviceitem/';
                                                $config['allowed_types'] = 'jpg|png|jpeg|gif';
                                               // $config['max_size'] = '200';
                                               // $config['max_width'] = '800';
                                               // $config['max_height'] = '800';
                                                $config['file_name'] =$file_name;
                                                
                                                 $this->load->library('upload', $config);
                                                 if ( ! $this->upload->do_upload('item_image')){
                                                   $resposne=["status"=>"1","message"=>"Please Select  jpg/png/jpeg/gif Item Image.","field"=>'item_image'];
                                                    
                                                  
                                                }else{
                                                     if(file_exists('./uploads/serviceitem/'.$category_details["image"])){
                                                        unlink('./uploads/serviceitem/'.$category_details["image"]);
                                                    }

                                                }
                                              }
                                              else {
                                                 $resposne=["status"=>"1","message"=>"Please Select  jpg/png/jpeg/gif Item Image.",'error'=> '',"field"=>'image'];
                                              }


                                        }else{
                                             $file_name=$category_details["image"];
                                        }
                                        if($resposne["status"]==0){

                                          $string = $data['item_name'];
                                          $searchStringComma = ',';
                                          $searchStringHyphen = '-';
                                          $new_string = '';

                                          if( strpos($string, $searchStringComma) !== false ) {
                                              $new_string = str_replace(","," ", $string);
                                          }

                                          if ($new_string != '') { //If New String is not Blank
                                            if( strpos($new_string, $searchStringHyphen) !== false ) {
                                                $new_string = str_replace("-"," ", $new_string);
                                                $new_string = preg_replace('!\s+!', ' ', $new_string);
                                                
                                                $slug = strtolower(str_replace(" ","-", $new_string));
                                            }
                                            else {
                                              $new_string = preg_replace('!\s+!', ' ', $new_string);
                                              $slug = strtolower(str_replace(" ","-", $new_string));
                                            }
                                          }
                                          else { //If New String is Blank
                                            if( strpos($string, $searchStringHyphen) !== false ) {
                                                $new_string = str_replace("-"," ", $string);
                                                $new_string = preg_replace('!\s+!', ' ', $new_string);
                                                
                                                $slug = strtolower(str_replace(" ","-", $new_string));
                                            }
                                            else {
                                              $new_string = preg_replace('!\s+!', ' ', $string);
                                              $slug = strtolower(str_replace(" ","-", $new_string));
                                            }
                                          }

                                          $item_data = [
                                            'name' => $data['item_name'], 
                                            'description' => $data['item_description'], 
                                            'service_category_id' => base64_decode($data['category']),
                                            'image' => $file_name,
                                            'slug' => $slug
                                          ];

                                          //print_r($item_data); die();

                                            $id_=$this->serviceitemModel->updateItem($item_data, $id);
                                            if($id_>0){
                                                $resposne=["status"=>"0","message"=>" Item updated successfully.","field"=>'item_name'];
                                            }else{
                                                
                                                $resposne=["status"=>"1","message"=>"Item updated failed.Please try again some time.","field"=>'item_name'];
                                                
                                            }
                                        }


                                    }


                            }

                         }

                    }


               
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
            }
        }catch (Exception $e) {
                $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'user_name','error'=>$e];

        }

        echo json_encode($resposne);
    }
  
    public function doitemStatusChange(){
         $data=$this->input->post();
        $id=base64_decode($data["id"]);
        $resposne=["status"=>"0","message"=>"","field"=>''];
        try {
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
            if(trim($this->token_id)!=''){
              $res=$this->serviceitemModel->updateItem(["status"=>$data["active"]],$id);
                        
                if($res){
                    $resposne=["status"=>"0","message"=>"Item status change successfully."];
                }
                else{                                
                    $resposne=["status"=>"1","message"=>"Item change failed.Please try again after some time."];                                
                }
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
            }   
        }catch (Exception $e) {
                $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'status'];

        }

        echo json_encode($resposne);
    }
    public function doitemDelete(){
        $data=$this->input->post();
        $data["id"]=base64_decode($data["id"]);
        $resposne=["status"=>"0","message"=>"","field"=>''];
        try {
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
            if(trim($this->token_id)!=''){
                $res = $this->serviceitemModel->doDeleteitem($data["id"]);
                        
                if($res){
                    $resposne=["status"=>"0","message"=>"Item Deleted successfully."];
                }
                else{                                
                    $resposne=["status"=>"1","message"=>"Item Deletion failed.Please try again after some time."];                                
                }
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
            }   
        }catch (Exception $e) {
                $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'role_id'];

        }

        echo json_encode($resposne);    
    }   

}
?>