<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="clearfix mb-10">
        <div class="pull-left">
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo base_url('servicerfq')?>">Service RFQ</a></li>
                    <li class="breadcrumb-item active" aria-current="page">List</li>
                </ol>
            </nav>
        </div>
    </div>

    <div class="container-fluid bg-light"> 
        <div class="row">
            <div class="col-lg-12">
  
                <form id="quotesrch" action="<?php echo base_url('servicerfq') ?>">

                    <input type="hidden" name="per_page_search" value="<?php echo $page; ?>">

                    <div class="row align-items-center justify-content-center">
                        <div class="col-md-3 pt-3">
                            <div class="form-group ">
                                <select id="cust_id" name="cust_id" class="form-control">
                                    <option value="">-- Select Customer --</option>

                                <?php foreach ($customer as $c) : ?>

                                    <option value="<?php echo $c['id']; ?>" <?php echo (isset($rfq_search) && isset($rfq_search["cust_id"]) &&  $rfq_search["cust_id"]== $cust['id']) ? 'selected' : ''; ?> > <?php echo $c['name']; ?> </option>

                                <?php endforeach; ?>

                                </select>  
                            </div>
                        </div>

                        <div class="col-md-3 pt-3">
                           <div class="form-group">
                                <select id="active" name="status" class="form-control">
                                    <option value="">-- Select Status --</option>
                                    <option value="1" <?php echo (isset($rfq_search) && isset($rfq_search["active"]) &&  $rfq_search["active"]==1)?'selected':'';?>>Active</option>
                                    <option value="0"  <?php echo (isset($rfq_search) && isset($rfq_search["active"]) &&  $rfq_search["active"]==0)?'selected':'';?>>InActive</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="col-md-3 pt-3">
                            <div class="form-group">
                                <a class="" href="<?php echo base_url('servicerfq')?>"><button type="button" class="btn btn-outline-primary "><i class="fa fa-refresh" aria-hidden="true"></i> Reset</button></a> 

                                <input type="submit" name="search" value="Search" class="btn btn-outline-primary">                                
                            </div>                       
                        </div>
                    </div>
                
                </form>

            </div>
        </div>
    </div>

    <div class="row">
        <table class="data-table stripe hover nowrap" id="userlst">
            <thead>
                <tr>
                    <th width="2%"  class="table-plus datatable-nosort" >#</th>
                    <th width="15%"  class="table-plus datatable-nosort">Customer</th>
                    <th width="15%"  class="table-plus datatable-nosort">Category</th>
                    <th width="8%"  class="table-plus datatable-nosort text-center">Quantity</th>
                    <th width="15%"  class="table-plus datatable-nosort">Expiry Date</th>
                    <th width="10%"  class="table-plus datatable-nosort text-center" >Messages</th>
                    <th width="15%"  class="table-plus datatable-nosort text-center" >Status</th>
                    <th width="10%"  class="table-plus datatable-nosort text-center" >Action</th>                   
                </tr>
            </thead>
            <tbody>

            <?php

                if(!empty($quote_data)){
                    $i=0;
                    foreach($quote_data as $val){
                        $i++;
            ?>

                        <tr class="">
                            <td class="table-plus" ><?php echo $i;?></td>  
                            <td class="table-plus" >
                                <?php echo $val["customer"];?>      
                            </td>
                            <td class="table-plus" >
                                <?php echo $val["service_category_name"];?>                                              
                            </td>
                            <td class="table-plus text-center" >
                                <?php echo $val["quantity"];?> <?php echo $val["uom"];?>                        
                            </td>
                            <td class="table-plus">
                            <?php

                                if ($val['expiry'] != NULL) {
                                    echo $val['expiry'];
                                }

                            ?>                                              
                            </td>
                            <td class="table-plus text-center">
                                <a class="btn btn-outline-primary" href="<?php echo base_url('servicerfq/details/' . base64_encode($val['rfq_id'])); ?>">
                                    <i class="fa fa-comments" aria-hidden="true"></i>     
                                </a>
                            </td>                   
                            <td class="table-plus text-center">
                                <select  name="status" onchange="doStatusChange(this.value,'<?php echo base64_encode($val["rfq_id"]);?>');" class="form-control">
                                    <option value="1" <?php echo ($val["status"]==1)?'selected':'';?>>Active</option>
                                    <option value="0"  <?php echo ($val["status"]==0)?'selected':'';?>>InActive</option>
                                </select>
                            </td>
                            <td class="table-plus text-center">
                                <a class="btn btn-outline-primary response" href="#" role="button" data-toggle="modal" data-id="<?php echo base64_encode($val['rfq_id']) ?>">
                                    <i class="fa fa-eye"></i> 
                                </a>
                            </td>
                        </tr>

            <?php
                    }
                }
              
            ?>

             
            </tbody>
        </table>
        <div class="col-sm-12">
            <div style="float: right;"><?php echo $links; ?></div>
        </div>
    </div>
</div>



<!-- Modal -->

<div class="modal fade admin_modal" id="viewModal">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h4 class="modal-title"> <span id="bussiness"> Bussiness Name </span> </h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="row" style="padding-bottom: 20px;">
                    <div class="col-lg-6">
                        <label style="font-weight: 400;">By : </label> <span id="customer"> Customer Name </span>
                    </div>
                    <div class="col-lg-6" id="expiry_date_col">
                        <label style="font-weight: 400;">Expired On : </label> <span id="expiry_date"> Expiry Date </span>
                    </div>
                </div>

                <div class="row" style="padding-bottom: 20px;">
                    <div class="col-lg-4">
                        <label style="font-weight: 400;">Quantity : </label> 
                        <br>
                        <span id="quantity"> Quantity </span>
                    </div>                    
                    <div class="col-lg-4">
                        <label style="font-weight: 400;">Category : </label>
                        <br>
                        <span id="category"> Category Name </span>
                    </div>
                    <div class="col-lg-4">
                        <label style="font-weight: 400;">Status : </label>
                        <br>
                        <span id="status"> Status </span>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="row" style="padding-bottom: 20px;">
                            <div class="col-lg-12">
                                <label style="font-weight: 400;">Message : </label>
                                <br>
                                <div id="message">Message</div>
                            </div>
                        </div>

                        <div class="row note" style="padding-bottom: 20px;">
                            <div class="col-lg-12">
                                <label style="font-weight: 400;">Note : </label>
                                <br>
                                <div id="other_requerment">Other Requerment</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row" style="padding-bottom: 20px;" id="imgRow">
                    <div class="col-lg-12" id="imgCol"></div>
                </div>
            </div>

        </div>
    </div>
</div>


<!-- Modal Ends -->

    
<?php
    if(isset($custom_js)){
        foreach($custom_js as $val){
            ?>
            <script src="<?php echo base_url();?>assets/js/custom_js/<?php echo $val;?>"></script>
            <?php
        }
    }
    
?>