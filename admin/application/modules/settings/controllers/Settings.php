<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class Settings extends CI_Controller {
	protected $token_id='';
	//protected $per_page=10;
	
    public function __construct() {
    	parent::__construct();
    	$this->token_id=get_cookie('token_id'); 
    	if(trim($this->token_id)==''){
    		redirect("login");
    	}
        $this->load->model("settingsModel", 'sM');
        $this->load->model("userModel");
        $this->load->helper("pagination");
        $this->load->library("pagination");
    }
    public function index()
	{
		try {
           $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            
            if(empty($user_details)){
                    $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                    delete_cookie('token_id'); 
                     delete_cookie('useName'); 
                    echo '<script>
                        alert("Unexpected server error.Please try again some time.");
                            window.location="'.base_url().'login";
                    </script>';
            }
            $user_details=current($user_details);
            $data=[]; 
            $data["menu"]="settings";
            $data["useName"]=$user_details["Name"];
            $data["custom_js"]=['settings.js'];
            
            set_cookie('useName',$user_details["Name"],'3600');
			
			$data["settings_data"]=$this->sM->getSettings();

            $data['view_file'] = 'Settings';
            view($data);
           
        } catch (Exception $e) {
            $resposne=["status"=>"1","e"=>$e,"message"=>"Unexpected server error.Please try again some time.","field"=>'user_name'];
        }   
    }
    
    public function exchangerate()
	{
		try {
           $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            
            if(empty($user_details)){
                    $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                    delete_cookie('token_id'); 
                     delete_cookie('useName'); 
                    echo '<script>
                        alert("Unexpected server error.Please try again some time.");
                            window.location="'.base_url().'login";
                    </script>';
            }
            $user_details=current($user_details);
            $data=[]; 
            $data["menu"]="settings";
            $data["useName"]=$user_details["Name"];
            
            set_cookie('useName',$user_details["Name"],'3600');
			
			$data["exchangerate_data"]=current($this->sM->getExchangerate());
            /* echo '<pre>';
            print_r($data["exchangerate_data"]);
            die; */
            $data['view_file'] = 'exchangerate';
            view($data);
           
        } catch (Exception $e) {
            $resposne=["status"=>"1","e"=>$e,"message"=>"Unexpected server error.Please try again some time.","field"=>'user_name'];
        }   
    }
    
    public function updateexchangerate(){
        try {
            $data=$this->input->post();
            
            $formData = [
                'base_currency' => $data['base_currency'], 
                'rate' => $data['rate'],
                'updated_date' => date("Y-m-d H:i:s"), 
            ];
            $res = $this->sM->updateExchangerate($formData); 
            if($res)
            {
                redirect('/settings/exchangerate');
            }
        }catch (Exception $e) {
            $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'company_name'];
        }
    }

    public function doAddSettings(){
        try {
            $data=$this->input->post();
            $resposne=["status"=>"0","message"=>"","field"=>''];
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));

            if(trim($this->token_id)!=''){   
                $data['id'] = base64_decode($data["id"]);
                $settings_details = current($this->sM->getById($data['id']));

                if(empty($settings_details)){
                    if(isset($data["company_name"]) && trim($data["company_name"])==''){
                        $resposne=["status"=>"1","message"=>"Please Enter Company Name.","field"=>'company_name'];
                    }
                    else if(!empty($_FILES["company_logo"]) && trim($_FILES["company_logo"]["name"])==''){
                        $resposne=["status"=>"1","message"=>"Please Select Category Image.","field"=>'company_logo'];
                    }
                    elseif(isset($data["company_mobile"]) && trim($data["company_mobile"])==''){
                        $resposne=["status"=>"1","message"=>"Please Enter Company Mobile.","field"=>'company_mobile'];
                    }
                    elseif(isset($data["company_fax"]) && trim($data["company_fax"])==''){
                        $resposne=["status"=>"1","message"=>"Please Enter Company Fax Number.","field"=>'company_fax'];
                    }
                    elseif(isset($data["company_address"]) && trim($data["company_address"])==''){
                        $resposne=["status"=>"1","message"=>"Please Enter Company Address.","field"=>'company_address'];
                    }
                    elseif(isset($data["per_page"]) && trim($data["per_page"])==''){
                        $resposne=["status"=>"1","message"=>"Please Enter Pagination Details.","field"=>'per_page'];
                    }
                    elseif(isset($data["notification_time"]) && trim($data["notification_time"])==''){
                        $resposne=["status"=>"1","message"=>"Please Enter Proper Notification Lead Time.","field"=>'notification_time'];
                    }
                    elseif(isset($data["news_time"]) && trim($data["news_time"])==''){
                        $resposne=["status"=>"1","message"=>"Please Enter Proper News Lead Time.","field"=>'news_time'];
                    }
                    elseif(isset($data["information_time"]) && trim($data["information_time"])==''){
                        $resposne=["status"=>"1","message"=>"Please Enter Proper Information Lead Time.","field"=>'information_time'];
                    }
                    elseif(isset($data["rfq_closing"]) && trim($data["rfq_closing"])==''){
                        $resposne=["status"=>"1","message"=>"Please Enter Proper RFQ Default Closing Time.","field"=>'rfq_closing'];
                    }
                    else {
                        $file_name=time().$_FILES["company_logo"]["name"];
                        $config['upload_path'] = './uploads/company/';
                        $config['allowed_types'] = 'jpg|png';
                        $config['max_width'] = '800';
                        $config['max_height'] = '800';
                        $config['file_name'] = $file_name;
                                
                        $this->load->library('upload', $config);
                        if ( ! $this->upload->do_upload('company_logo')){
                            $resposne=["status"=>"1","message"=>"Please Select jpg/png Logo. Maximum Resolution 800X800.","field"=>'company_logo'];
                          
                        }
                        else{
                            $formData = [
                                'company_name'      => $data['company_name'], 
                                'company_logo'      => $file_name, 
                                'company_mobile'    => $data['company_mobile'], 
                                'company_fax'       => $data['company_fax'], 
                                'company_address'   => $data['company_address'], 
                                'per_page'          => $data['per_page'], 

                                'notification_time' => $data['notification_time'], 
                                'news_time'         => $data['news_time'], 
                                'information_time'  => $data['information_time'], 
                                'rfq_closing'       => $data['rfq_closing'], 
                            ];

                            $res = $this->sM->updateSetting($formData);     //For Add
                        }
                    }
                }
                else {
                    if(isset($data["company_name"]) && trim($data["company_name"])==''){
                        $resposne=["status"=>"1","message"=>"Please Enter Company Name.","field"=>'company_name'];
                    }
                    else if(!empty($_FILES["company_logo"]) && trim($_FILES["company_logo"]["name"])=='' && trim($settings_details["company_logo"])==''){
                        $resposne=["status"=>"1","message"=>"Please Select Category Image.","field"=>'company_logo'];
                    }
                    elseif(isset($data["company_mobile"]) && trim($data["company_mobile"])==''){
                        $resposne=["status"=>"1","message"=>"Please Enter Company Mobile.","field"=>'company_mobile'];
                    }
                    elseif(isset($data["company_fax"]) && trim($data["company_fax"])==''){
                        $resposne=["status"=>"1","message"=>"Please Enter Company Fax Number.","field"=>'company_fax'];
                    }
                    elseif(isset($data["company_address"]) && trim($data["company_address"])==''){
                        $resposne=["status"=>"1","message"=>"Please Enter Company Address.","field"=>'company_address'];
                    }
                    elseif(isset($data["per_page"]) && trim($data["per_page"])==''){
                        $resposne=["status"=>"1","message"=>"Please Enter Pagination Details.","field"=>'per_page'];
                    }
                    elseif(isset($data["notification_time"]) && trim($data["notification_time"])==''){
                        $resposne=["status"=>"1","message"=>"Please Enter Proper Notification Lead Time.","field"=>'notification_time'];
                    }
                    elseif(isset($data["news_time"]) && trim($data["news_time"])==''){
                        $resposne=["status"=>"1","message"=>"Please Enter Proper News Lead Time.","field"=>'news_time'];
                    }
                    elseif(isset($data["information_time"]) && trim($data["information_time"])==''){
                        $resposne=["status"=>"1","message"=>"Please Enter Proper Information Lead Time.","field"=>'information_time'];
                    }
                    elseif(isset($data["rfq_closing"]) && trim($data["rfq_closing"])==''){
                        $resposne=["status"=>"1","message"=>"Please Enter Proper RFQ Default Closing Time.","field"=>'rfq_closing'];
                    }
                    else {

                       if(!isset($_FILES["company_logo"]) || empty($_FILES["company_logo"])|| trim($_FILES["company_logo"]["name"])=='') {
                            //No New Image Selected But Already DB Has One

                            $formData = [
                                'company_name' => $data['company_name'], 
                                'company_mobile' => $data['company_mobile'], 
                                'company_fax' => $data['company_fax'], 
                                'company_address' => $data['company_address'], 
                                'per_page' => $data['per_page'], 

                                'notification_time' => $data['notification_time'], 
                                'news_time'         => $data['news_time'], 
                                'information_time'  => $data['information_time'], 
                                'rfq_closing'       => $data['rfq_closing'], 
                                'updated_by'        => 1,
                                'updated_on'        => date('Y-m-d H:i:s'),
                            ];
                        }
                        else {
                            //Added New Image Selected But Already DB Has One

                            if(file_exists('./uploads/company/' . $settings_details["company_logo"])){
                                unlink('./uploads/company/' . $settings_details["company_logo"]);
                            }

                            $file_name = time().$_FILES["company_logo"]["name"];
                            $config['upload_path'] = './uploads/company/';
                            $config['allowed_types'] = 'jpg|png';
                            $config['max_width'] = '800';
                            $config['max_height'] = '800';
                            $config['file_name'] = $file_name;
                                    
                            $this->load->library('upload', $config);
                            if ( ! $this->upload->do_upload('company_logo')){
                                $resposne=["status"=>"1","message"=>"Please Select jpg/png Logo. Maximum Resolution 800X800.","field"=>'company_logo'];                          
                            }
                            else{
                                $formData = [
                                    'company_name' => $data['company_name'], 
                                    'company_logo' => $file_name, 
                                    'company_mobile' => $data['company_mobile'], 
                                    'company_fax' => $data['company_fax'], 
                                    'company_address' => $data['company_address'], 
                                    'per_page' => $data['per_page'], 

                                    'notification_time' => $data['notification_time'], 
                                    'news_time'         => $data['news_time'], 
                                    'information_time'  => $data['information_time'], 
                                    'rfq_closing'       => $data['rfq_closing'], 
                                    'updated_by'        => 1,
                                    'updated_on'        => date('Y-m-d H:i:s'),
                                ];
                            }
                        }

                        $res = $this->sM->updateSetting($formData, $data['id']);    //For Update
                    }
                }

                if($res){
                    $resposne=["status"=>"0","message"=>"Settings Updated successfully.","field"=>'company_name'];
                }else{

                    $resposne=["status"=>"1","message"=>"Settings Updation failed.Please try again some time.","field"=>'company_name'];                        
                }
            }
            else{
                $resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
            }
        } catch (Exception $e) {
            $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'company_name'];
        }

        echo json_encode($resposne);
    }

}
?>