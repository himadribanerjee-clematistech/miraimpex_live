<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="clearfix mb-10">
        <div class="pull-left">
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Settings</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Modify Settings</li>
                </ol>
            </nav>
        </div>
    </div>

    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                
                <form id="addsettingfrm" method="post" action="javascript:void(0);" onsubmit="addSettings();">

                    <input type="hidden" name="id" id="id" value="<?php echo (isset($settings_data) && isset($settings_data->id) )?base64_encode($settings_data->id):'';?>">

                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-3">
                                <label label-for='name'>Company Name <span class="mandatory_label">*</span></label>
                                <input type="text" name="company_name" id="company_name" placeholder="Company Name" value="<?php echo (isset($settings_data) && isset($settings_data->company_name) )?$settings_data->company_name:'';?>" class="form-control">
                            </div>
                            <div class="col-lg-3">
                                <label label-for='name'>Company Mobile <span class="mandatory_label">*</span></label>
                                <input type="text" name="company_mobile" id="company_mobile" placeholder="Company Mobile Number" value="<?php echo (isset($settings_data) && isset($settings_data->company_mobile) )?$settings_data->company_mobile:'';?>" class="form-control">
                            </div>
                            <div class="col-lg-3">
                                <label label-for='name'>Company Fax <span class="mandatory_label">*</span></label>
                                <input type="text" name="company_fax" id="company_fax" placeholder="Company Fax Number" value="<?php echo (isset($settings_data) && isset($settings_data->company_fax) )?$settings_data->company_fax:'';?>" class="form-control">
                            </div>
                            <div class="col-lg-3">
                                <label label-for='name'>Per Page Showed Value <span class="mandatory_label">*</span> </label>
                                <input type="text" name="per_page" id="per_page" placeholder="Pagination Per Page" value="<?php echo (isset($settings_data) && isset($settings_data->per_page) )?$settings_data->per_page:'';?>" class="form-control">
                            </div>
                        </div>
                    </div>

                    <br>

                    <div class="form-group">
                        <div class="row">                            
                            <div class="col-lg-3">
                                <label label-for='name'>Company Logo <span class="mandatory_label">*</span></label>
                                <br>

                                <img src="<?php echo (isset($settings_data) && isset($settings_data->company_logo) ) ? base_url('uploads/company/' . $settings_data->company_logo) : base_url('assets/images/no-item-image.png'); ?>" height="170" width="180" style="border: 2px dotted #ccc; border-radius: 2px; padding: 8px;" id="chImg"/>

                                <br><br>

                                <input type="hidden" id="company_logo_old" name="company_logo_old" value="<?php echo (isset($settings_data) && isset($settings_data->company_logo) ) ? $settings_data->company_logo : ''; ?>" >

                                <input type="file" name="company_logo" id="company_logo" onchange="loadFile()">
                            </div>
                            <div class="col-lg-9">
                                <label label-for='name'>Company Address <span class="mandatory_label">*</span></label>
                                <textarea name="company_address" id="company_address" placeholder="Company Address" class="form-control" style="height: 100px;"><?php echo (isset($settings_data) && isset($settings_data->company_address) )?$settings_data->company_address:'';?></textarea>
                            </div>
                        </div>
                    </div>

                    <br>

                    <!-- New row --> 
                    <br>
                    <div class="form-group">
                        <div class="row">
                        <div class="col-lg-9">
                                <label label-for='name'>Delivery Charges</label>
                                <textarea name="delivery_charges" id="delivery_charges" placeholder="Delivery Charges(if any)" class="form-control" style="height: 100px;"><?php echo (isset($settings_data) && isset($settings_data->delivery_charges) )?$settings_data->delivery_charges:'';?></textarea>
                            </div>
                        </div>
                    </div>                   
                    <br>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-12 text-right">
                                <input type="submit" value="<?php echo (isset($settings_data))?'Update':'Submit';?>" name="submit" class="btn btn-outline-primary">
                            </div>
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>
</div>


<?php
    if(isset($custom_js)){
        foreach($custom_js as $val){
            ?>
            <script src="<?php echo base_url();?>assets/js/custom_js/<?php echo $val;?>"></script>
            <?php
        }
    }
    
?>