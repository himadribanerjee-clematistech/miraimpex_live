<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="clearfix mb-10">
        <div class="pull-left">
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Settings</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Currency Exchange rate</li>
                </ol>
            </nav>
        </div>
    </div>

    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                
                <form id="addsettingfrm" method="post" action="<?php echo base_url('settings')?>/updateexchangerate">


                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-3">
                                <label label-for='name'>Base Currency <span class="mandatory_label">*</span></label>
                                <p style="padding-top:10px;">1 USD is equivalent to:</p>
                                <input type="hidden" name="base_currency" value="USD">
                            </div>
                            <div class="col-lg-3">
                                <label label-for='name'>Gambian Dalasi (<?php echo $exchangerate_data['to_currency']; ?>)<span class="mandatory_label">*</span></label>
                                <input type="text" name="rate" id="gmd_exchange" placeholder="Exchange rate" value="<?php echo $exchangerate_data['rate']; ?>" class="form-control">
                            </div>
                            <div class="col-lg-3" style="margin-top:20px">
                            <input type="submit" value="<?php echo (isset($exchangerate_data))?'Update Now':'Submit';?>" name="submit" class="btn btn-outline-primary">
                            </div>
                            <div class="col-lg-3">
                                <label label-for='name'>Last updated on: </label>
                                <p style="padding-top:10px;"><?php echo $exchangerate_data['updated_date']; ?></p>
                            </div>
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>
</div>
