<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="clearfix mb-10">
        <div class="pull-left">
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo base_url('report-incident')?>">Report Incident</a></li>
                    <li class="breadcrumb-item active" aria-current="page">List</li>
                </ol>
            </nav>
        </div>
    </div>

    <div class="container-fluid bg-light"> 
        <div class="row">
            <div class="col-lg-12">
  
                <form id="quotesrch" action="<?php echo base_url('reportincident') ?>">

                    <input type="hidden" name="per_page_search" value="<?php echo $page; ?>">

                    <!-- <div class="row align-items-center justify-content-center">
                        <div class="col-md-3 pt-3">
                            <div class="form-group ">
                                <select id="cust_id" name="cust_id" class="form-control">
                                    <option value="">-- Select Customer --</option>

                                <?php foreach ($customer as $c) : ?>

                                    <option value="<?php echo $c['id']; ?>" <?php echo (isset($quotation_search) && isset($quotation_search["cust_id"]) &&  $quotation_search["cust_id"]== $cust['id']) ? 'selected' : ''; ?> > <?php echo $c['name']; ?> </option>

                                <?php endforeach; ?>

                                </select>  
                            </div>
                        </div>

                        <div class="col-md-3 pt-3">
                           <div class="form-group">
                                <select id="active" name="status" class="form-control">
                                    <option value="">-- Select Status --</option>
                                    <option value="1" <?php echo (isset($quotation_search) && isset($quotation_search["active"]) &&  $quotation_search["active"]==1)?'selected':'';?>>Active</option>
                                    <option value="0"  <?php echo (isset($quotation_search) && isset($quotation_search["active"]) &&  $quotation_search["active"]==0)?'selected':'';?>>InActive</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="col-md-3 pt-3">
                            <div class="form-group">
                                <a class="" href="<?php echo base_url('reportincident')?>"><button type="button" class="btn btn-outline-primary "><i class="fa fa-refresh" aria-hidden="true"></i> Reset</button></a> 

                                <input type="submit" name="search" value="Search" class="btn btn-outline-primary">                                
                            </div>                       
                        </div>
                    </div> -->
                
                </form>

            </div>
        </div>
    </div>

    <div class="row">
        <table class="data-table stripe hover nowrap" id="userlst">
            <thead>
                <tr>
                    <th width="2%"  class="table-plus datatable-nosort" >#</th>
                    <th width="20%"  class="table-plus datatable-nosort">Seller</th>
                    <th width="15%"  class="table-plus datatable-nosort text-center">Report Count</th>
                    <th width="15%"  class="table-plus datatable-nosort text-center" >Status</th>
                    <th width="10%"  class="table-plus datatable-nosort text-center" >Action</th>                   
                </tr>
            </thead>
            <tbody>

            <?php

                if(!empty($reportincident_data)){
                    $i=0;
                    foreach($reportincident_data as $val){
                        $i++;
            ?>

                        <tr class="">
                            <td class="table-plus" ><?php echo $i;?></td>  
                            <td class="table-plus" >
                                <?php echo $val["name"];?>      
                            </td>
                            <td class="table-plus text-center" >
                                <span style="color: red; font-weight: bold;">
                                    <?php echo $val["count_report"];?> 
                                </span>                                             
                            </td>
                            <td class="table-plus text-center">
                                <select  name="status" onchange="doStatusChange(this.value,'<?php echo base64_encode($val["to_id"]);?>');" class="form-control">
                                    <option value="1" <?php echo ($val["active"]==1)?'selected':'';?>>Active</option>
                                    <option value="0"  <?php echo ($val["active"]==0)?'selected':'';?>>InActive</option>
                                </select>
                            </td>
                            <td class="table-plus text-center">
                                <a class="btn btn-outline-primary" href="<?php echo base_url('reportincident/details/' . base64_encode($val['to_id'])); ?>">
                                    <i class="fa fa-eye" aria-hidden="true"></i>     
                                </a>
                            </td>
                        </tr>

            <?php
                    }
                }
              
            ?>

             
            </tbody>
        </table>
        <div class="col-sm-12">
            <div style="float: right;"><?php echo $links; ?></div>
        </div>
    </div>
</div>

    
<?php
    if(isset($custom_js)){
        foreach($custom_js as $val){
            ?>
            <script src="<?php echo base_url();?>assets/js/custom_js/<?php echo $val;?>"></script>
            <?php
        }
    }
    
?>