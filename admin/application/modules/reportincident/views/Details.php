<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="clearfix mb-10">
        <div class="pull-left">
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo base_url('report-incident')?>">Report Incident</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Details</li>
                </ol>
            </nav>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row" style="padding-bottom: 20px;">
            <div class="col-lg-8">
                <label style="font-weight: 400;">Accused Person: </label> <span id="accused" style="color: red; font-size: 22px; font-weight: 500"> <?php echo $accused['name']; ?> </span>
            </div>
        </div>
    </div>

    <div class="container-fluid"> 
        <div class="row">      
            <table class="data-table stripe hover nowrap" id="userlst">
                <thead>
                    <tr>
                        <th width="2%"  class="table-plus datatable-nosort" >#</th>
                        <th width="15%"  class="table-plus datatable-nosort">Accused By</th>
                        <th width="15%"  class="table-plus datatable-nosort">Accused On</th>
                        <th width="15%"  class="table-plus datatable-nosort text-center">Accused For</th>
                        <!-- <th width="15%"  class="table-plus datatable-nosort">Message</th> -->
                        <th width="10%"  class="table-plus datatable-nosort text-center" >View Report</th>                   
                    </tr>
                </thead>
                <tbody>

        <?php

            if(!empty($report_details)){
                $i=0;
                foreach($report_details as $val){
                    $i++;
        ?>

                    <tr class="">
                        <td class="table-plus" ><?php echo $i;?></td>  
                        <td class="table-plus" >
                            <?php echo $val["accused_by"];?>      
                        </td>
                        <td class="table-plus">
                            <?php

                                if ($val['created_date'] != NULL) {
                                    echo date('d-m-Y h:i:s A', strtotime($val['created_date']));
                                }

                            ?>                                              
                        </td>
                        <td class="table-plus text-center">
                            <a class="btn btn-outline-primary accusedFor" href="#" role="button" data-toggle="modal" data-type="<?php echo $val['type']; ?>" data-ref-id="<?php echo base64_encode($val['ref_id']); ?>" >
                                <i class="fa fa-file-text"></i>
                            </a>
                        </td>
                        <td class="table-plus text-center">
                            <a class="btn btn-outline-primary viewReport" href="#" role="button" data-toggle="modal" data-to-id="<?php echo base64_encode($val['to_id']) ?>" data-from-id="<?php echo base64_encode($val['from_id']) ?>">
                                <i class="fa fa-eye"></i>
                            </a>
                        </td>
                    </tr>

        <?php
                }
            }
          
        ?>
                    
                </tbody>
            </table>
        </div>
    </div>      
</div>



<!-- RFQ Modal -->

<div class="modal fade admin_modal" id="rfqModal">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h4 class="modal-title"> <span id="type_rfq"> Type </span> </h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="row" style="padding-bottom: 20px;">
                    <div class="col-lg-6">
                        <label style="font-weight: 400;">By : </label> <span id="customer"> Customer Name </span>
                    </div>
                    <div class="col-lg-6" id="expiry_date_col">
                        <label style="font-weight: 400;">Expired On : </label> <span id="expiry_date"> Expiry Date </span>
                    </div>
                </div>

                <div class="row" style="padding-bottom: 20px;">
                    <div class="col-lg-6">
                        <label style="font-weight: 400;">Unit of Measurement : </label> <span id="UOM"> Unit of Measurement </span>
                    </div>
                    <div class="col-lg-6">
                        <label style="font-weight: 400;">Quantity : </label> <span id="quantity_rfq"> Quantity </span>
                    </div>
                </div>

                <div class="row" style="padding-bottom: 20px;">
                    <div class="col-lg-3">
                        <label style="font-weight: 400;">Species : </label>
                        <br>
                        <span id="species_name"> Species Name </span>
                    </div>
                    <div class="col-lg-3">
                        <label style="font-weight: 400;">Item : </label>
                        <br>
                        <span id="item_name_rfq"> Item Name </span>
                    </div>
                    <div class="col-lg-3">
                        <label style="font-weight: 400;">Category : </label>
                        <br>
                        <span id="category"> Category Name </span>
                    </div>
                    <div class="col-lg-3">
                        <label style="font-weight: 400;">SubCategory : </label>
                        <br>
                        <span id="subcategory"> SubCategory Name </span>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="row" style="padding-bottom: 20px;">
                            <div class="col-lg-12">
                                <label style="font-weight: 400;">Quotation : </label>
                                <br>
                                <div id="message_rfq">Message</div>
                            </div>
                        </div>

                        <div class="row" style="padding-bottom: 20px;">
                            <div class="col-lg-12">
                                <label style="font-weight: 400;">Note : </label>
                                <br>
                                <div id="other_requerment">Other Requerment</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row" style="padding-bottom: 20px;" id="imgRow">
                    <div class="col-lg-12" id="imgCol"></div>
                </div>
            </div>

        </div>
    </div>
</div>


<!-- RFQ Modal Ends -->

<!-- ENQ Modal -->

<div class="modal fade admin_modal" id="enqModal">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h4 class="modal-title"> <span id="type_enq"> Type </span> </h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="row item" style="padding-bottom: 20px;">
                    <div class="col-lg-12">
                        <label style="font-weight: 400;">Item : </label> <span id="item_name_enq"> Item Name </span>
                    </div>
                </div>

                <div class="row" style="padding-bottom: 20px;">
                    <div class="col-lg-6">
                        <label style="font-weight: 400;">From : </label> <span id="from_cust"> From Customer </span>
                    </div>
                    <div class="col-lg-6">
                        <label style="font-weight: 400;">To : </label> <span id="to_cust"> To Customer </span>
                    </div>
                </div>

                <div class="row" style="padding-bottom: 20px;">
                    <div class="col-lg-6">
                        <label style="font-weight: 400;">Quantity : </label> <span id="quantity_enq"> Quantity </span>
                    </div>
                    <div class="col-lg-6">
                        <label style="font-weight: 400;">On : </label> <span id="date"> Date </span>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="row" style="padding-bottom: 20px;">
                            <div class="col-lg-12">
                                <label style="font-weight: 400;">Message : </label>
                                <br>
                                <div id="message_enq">Message</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


<!-- ENQ Modal Ends -->

<!-- Report Modal -->

<div class="modal fade admin_modal" id="reportModal">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header text-center">
                <!-- <h4 class="modal-title"> <span id="type1"> Type </span> </h4> -->
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="row item" style="padding-bottom: 20px;">
                    <div class="col-lg-6">
                        <label style="font-weight: 400;">Accused By : </label> <span id="accused_by"> Accused By </span>
                    </div>
                    <div class="col-lg-6">
                        <label style="font-weight: 400;">Accused On : </label> <span id="created_date"> Accused On </span>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="row" style="padding-bottom: 20px;">
                            <div class="col-lg-12">
                                <label style="font-weight: 400;">Message : </label>
                                <br>
                                <div id="message_report">Message</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row" style="padding-bottom: 20px;" id="imgReportRow">
                    <div class="col-lg-12" id="imgReportCol"></div>
                </div>
            </div>

        </div>
    </div>
</div>


<!-- Report Modal Ends -->


<?php
    if(isset($custom_js)){
        foreach($custom_js as $val){
            ?>
            <script src="<?php echo base_url();?>assets/js/custom_js/<?php echo $val;?>"></script>
            <?php
        }
    }
    
?>