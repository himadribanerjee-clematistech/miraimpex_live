<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class Reportincident extends CI_Controller {
    protected $token_id='';
    
    public function __construct() {
        parent::__construct();
        $this->token_id=get_cookie('token_id'); 
        if(trim($this->token_id)==''){
            redirect("login");
        }

        $this->load->model("userModel");
        $this->load->model("customerModel");
        $this->load->model("sellerModel");

        $this->load->model("reportincidentModel");       
        
        $this->load->helper("pagination");
        $this->load->library("pagination");
        $this->load->model("settingsModel", 'sM');
        $settings_data=$this->sM->getSettings();
        if(isset($settings_data->per_page)){
         $this->per_page=$settings_data->per_page;
        }
    }
    public function index() {
        try {
           $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            
            if(empty($user_details)){
                    $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                    delete_cookie('token_id'); 
                     delete_cookie('useName'); 
                    echo '<script>
                        alert("Unexpected server error.Please try again some time.");
                            window.location="'.base_url().'login";
                    </script>';
            }
            $user_details=current($user_details);
            $data=[]; 
            $data["menu"]="reportincident";
            $data["useName"]=$user_details["Name"];
            $data["custom_js"]=['reportincident.js'];
            set_cookie('useName',$user_details["Name"],'3600');
            
            //search 
            $search_input=$this->input->get();
            
            $quotation_search=[];
            
            if(isset($search_input["cust_id"]) && trim($search_input["cust_id"])!=''){
                $quotation_search["cust_id"]=$search_input["cust_id"];
                
            }

            if(isset($search_input["status"]) && trim($search_input["status"])!=''){
                $quotation_search["active"]=$search_input["status"];
                
            }

            $data["quotation_search"] = $quotation_search;
            $page=0;
            if($this->input->get("per_page")){
                $page= $this->input->get("per_page");
            }

            $data["page"]=$page;

            $total_row=$this->reportincidentModel->getReportincidentCount($quotation_search);
         
            $data["links"] = createPageNationLink(base_url('reportincident/index'), $total_row, $this->per_page);

            $start= (int)$page +1;
            $end = ($page+ $this->per_page >=$total_row)? $total_row : (int)$page  + $this->per_page;
            $data['result_count']='';
            if($total_row>0){
                $data['result_count']= "Showing ".$start." - ".$end." of ".$total_row." Results";
            }
            
            $data["reportincident_data"] = $this->reportincidentModel->getReportincidentList($quotation_search,$this->per_page, $page); 
            $data["customer"] = $this->customerModel->getCustList('','','');

            //print_r($data); die();
            $data['view_file'] = 'Reportincident';
            view($data);
           
        } catch (Exception $e) {
            $resposne=["status"=>"1","e"=>$e,"message"=>"Unexpected server error.Please try again some time.","field"=>'user_name'];
        }   
    }

    public function details($value) {
        $resposne=["status"=>"0","message"=>"","field"=>'', 'data'=> ''];
        try {
            $id = base64_decode($value);

            $user_details = current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));

            if(trim($this->token_id)!=''){
                $data['accused'] = current($this->reportincidentModel->getNameById($id));
                $data['report_details'] = $this->reportincidentModel->getDetailById($id);

                //print_r($data); die();

                $data["menu"]="reportincident";
                $data['view_file'] = 'Details';
                $data["custom_js"]=['reportincident.js'];
                view($data);
            }
            else {
                redirect(base_url(), 'refresh');
            }
        }
        catch (Exception $e) {
            $resposne=["status"=>"1","e"=>$e,"message"=>"Unexpected server error.Please try again some time.","field"=>'user_name'];
        }
    }

    public function accusedfor() {
        $resposne=["status"=>0,"message"=>"","field"=>'', "type"=>''];

        try{

            $user_details = current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
            if(trim($this->token_id)!=''){

                $type = $this->input->post('type');
                $reference_id = base64_decode($this->input->post('reference_id'));

                $res = current($this->reportincidentModel->getRefDetailById($type, $reference_id));

                $resposne=["status"=>"0","message"=>"Success.","field"=>'', 'data'=> $res, 'type'=> $type];
            }
            else {
                $resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
            }

        }
        catch(Exception $e) {
            $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>''];
        }

        echo json_encode($resposne);
    }

    public function accusedreport() {
        $resposne=["status"=>0,"message"=>"", "data" => ''];

        try{

            $user_details = current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
            if(trim($this->token_id)!=''){

                $to_id = base64_decode($this->input->post('to_id'));
                $from_id = base64_decode($this->input->post('from_id'));

                $res = current($this->reportincidentModel->getReportDetailById($to_id, $from_id));

                //print_r($res);

                $resposne=["status"=>"0","message"=>"Success.", 'data'=> $res];
            }
            else {
                $resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
            }

        }
        catch(Exception $e) {
            $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>''];
        }

        echo json_encode($resposne);
    }
    

    public function doStatusChange(){
        $data = $this->input->post();
        $id = base64_decode($data["id"]);
        $resposne=["status"=>"0","message"=>"","field"=>''];
        try {
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
            if(trim($this->token_id)!=''){

                //print_r($data);

                $res = $this->customerModel->updateCust( ["active" => $data["status"], 'update_by' => 1, 'update_on' => date('Y-m-d H:i:s')], $id);

                if($res){
                    $res2 = $this->sellerModel->updateSeller( ["active" => $data["status"], 'update_by' => 1, 'update_on' => date('Y-m-d H:i:s')], $id, 'ban');

                    if ($res2) {

                        if ($data['status'] == 0) {
                            $row = $this->customerModel->getEmailId($id);

                            $data_mail = ['message' => 'Sorry! We Have Suspended Your Profile Due To Violation of Our Rules. Please Contact Administrator!', 'name' => $row['name']];

                            $mail = email($row['email'], $data_mail); 

                            if ($mail) {
                                $resposne=["status"=>"0","message"=>"Approval of Seller status change successfully."];
                            }
                        }
                        else {
                            $resposne=["status"=>"0","message"=>"Approval of Seller status change successfully."];
                        }
                    }                    
                }
                else{                                
                    $resposne=["status"=>"1","message"=>"Approval of Seller status change failed.Please try again after some time."];                                
                }
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
            }   
        }catch (Exception $e) {
                $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'role_id'];

        }

        echo json_encode($resposne);  
    }


}
?>