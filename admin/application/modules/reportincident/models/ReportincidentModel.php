<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class ReportincidentModel extends CI_Model {
	protected $buyerTable = 'buyer_profile'; 
	//protected $sellerTable = 'seller_profile'; 
	protected $catTable = 'category';
	protected $itemTable = 'item';
	protected $speciesTable = 'species';
	protected $uomTable = 'uom';
	protected $quoteTable = 'rfq';
	protected $riTable = 'report_incident';
	protected $customerTable = 'customer';
	protected $rfqImgTable = 'rfq_image';
	protected $enqTable = 'enquiry'; 
	protected $siTable = 'seller_item';
	//protected $threadTable = 'rfq_thread';
	//protected $threadImgTable = 'rfq_thread_image';
	
	public function __construct() {
        parent::__construct();
    }
     
    
    public function getReportincidentCount($data){
    	try {
    		$this->db->db_debug = false;

    		// SELECT ri.to_id, c.name, COUNT(ri.to_id) AS count_report, c.active FROM t_report_incident ri LEFT JOIN t_customer c ON c.id = ri.to_id LEFT JOIN t_seller_profile sp ON c.id = sp.cust_id GROUP BY ri.to_id ORDER BY COUNT(ri.to_id) DESC


    		$this->db->select("ri.to_id, c.name, COUNT(ri.to_id) AS count_report, c.active");
    		$this->db->from($this->riTable." ri");
    		$this->db->join($this->customerTable . ' c' , 'ON c.id = ri.to_id', 'LEFT');
    		$this->db->group_by('ri.to_id');
    		
    		/*if(isset($data["cust_id"]) && $data["cust_id"]!=''){
	    		$this->db->where("r.buyer_id",$data["cust_id"]);
	    	}

	    	if(isset($data["active"]) && $data["active"]!=''){
	    		$this->db->where("r.status",$data["active"]);
	    	}*/

	    	$this->db->order_by('COUNT(ri.to_id)', 'DESC');

	    	$db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query();die();
	        	return $this->db->count_all_results();

	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
    }

    public function getReportincidentList($data, $limit='', $start=0){
    	try {
    		$this->db->db_debug = false;

    		$this->db->select("ri.to_id, c.name, COUNT(ri.to_id) AS count_report, c.active");
    		$this->db->from($this->riTable." ri");
    		$this->db->join($this->customerTable . ' c' , 'ON c.id = ri.to_id', 'LEFT');
    		$this->db->group_by('ri.to_id');
    		
    		/*if(isset($data["cust_id"]) && $data["cust_id"]!=''){
	    		$this->db->where("r.buyer_id",$data["cust_id"]);
	    	}

	    	if(isset($data["active"]) && $data["active"]!=''){
	    		$this->db->where("r.status",$data["active"]);
	    	}*/

	    	$this->db->order_by('COUNT(ri.to_id)', 'DESC');

	    	if($limit!='' && $start>=0 ){
	    		$this->db->limit($limit, $start);
	    	} 	    	
 
 			$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query();die();
	        	$result=[];
	        	foreach($query->result_array() as $row){
	        		$result[]=$row;
	        	}
	        	return $result;
	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
    }	

    public function getNameById($id) {
    	try {
		 	$this->db->db_debug = false;

		 	// SELECT name FROM t_customer WHERE id = 3


		 	$this->db->select("name");
    		$this->db->from($this->customerTable);

    		$this->db->where('id', $id);

    		$query=$this->db->get();

    		//echo $this->db->last_query();die();

    		$db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{


	        	$data=[];
	        	foreach($query->result_array() as $row){
					$data[] = $row;
	        	}
	        	
	        	//echo $this->db->last_query();die();
	        	//print_r($data); die();
	        	return $data;
	        }
	        return TRUE;
	    } 
	    catch (Exception $e) {
	       throw new Exception();
	    }	

    }
    
    public function getDetailById($id) {
    	try {
		 	$this->db->db_debug = false;

		 	// SELECT ri.id, ri.to_id, ri.from_id, ri.type, ri.ref_id, ct.name AS accused, cf.name AS accused_by, ri.created_date FROM t_report_incident ri LEFT JOIN t_customer ct ON ri.to_id = ct.id LEFT JOIN t_customer cf ON ri.from_id = cf.id WHERE ri.to_id = 3


		 	$this->db->select("ri.id, ri.to_id, ri.from_id, ri.type, ri.ref_id, ct.name AS accused, cf.name AS accused_by, ri.created_date");
    		$this->db->from($this->riTable." ri");
    		$this->db->join($this->customerTable . ' ct' , 'ON ri.to_id = ct.id', 'LEFT');
    		$this->db->join($this->customerTable . ' cf' , 'ON ri.from_id = cf.id', 'LEFT');

    		$this->db->where('ri.to_id', $id);

    		$query=$this->db->get();

    		//echo $this->db->last_query();die();

    		$db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{


	        	$data=[];
	        	foreach($query->result_array() as $row){
					$data[] = $row;
	        	}
	        	
	        	//echo $this->db->last_query();die();
	        	//print_r($data); die();
	        	return $data;
	        }
	        return TRUE;
	    } 
	    catch (Exception $e) {
	       throw new Exception();
	    }	

    }

    public function getRefDetailById($type, $id) {
    	try {
		 	$this->db->db_debug = false;

		 	if ($type == 'rfq') {
		 		// SELECT r.quantity, r.message, r.other_requerment, r.expiry_date, r.created_date, i.item_name, s.species_name, u.name AS UOM, c.name AS customer, cat.category_name AS category, subcat.category_name AS subcategory, ri.id AS img_id, ri.image, b.business_name FROM t_rfq r LEFT JOIN t_item i ON r.item_id = i.id LEFT JOIN t_species s ON r.species_id = s.id LEFT JOIN t_uom u ON r.uom_id = u.id LEFT JOIN t_customer c ON r.buyer_id = c.id LEFT JOIN t_buyer_profile b ON r.buyer_id = b.cust_id LEFT JOIN t_category cat ON r.cat_id = cat.id LEFT JOIN t_category subcat ON r.subcat_id = subcat.id LEFT JOIN t_rfq_image ri ON r.rfq_id = ri.req_id WHERE r.rfq_id = 1

		 		$this->db->select("r.rfq_id, r.quantity, r.message, r.other_requerment, r.expiry_date, i.item_name, s.species_name, u.name AS UOM, c.name AS customer, cat.category_name AS category, subcat.category_name AS subcategory, ri.id AS img_id, ri.image, b.business_name");
	    		$this->db->from($this->quoteTable." r");

	    		$this->db->join($this->itemTable . ' i' , 'ON r.item_id = i.id', 'LEFT');
	    		$this->db->join($this->speciesTable . ' s' , 'ON r.species_id = s.id', 'LEFT');
	    		$this->db->join($this->uomTable . ' u' , 'ON r.uom_id = u.id', 'LEFT');
	    		$this->db->join($this->customerTable . ' c' , 'ON r.buyer_id = c.id', 'LEFT');
	    		$this->db->join($this->buyerTable . ' b' , 'ON r.buyer_id = b.cust_id', 'LEFT');
	    		$this->db->join($this->catTable . ' cat' , 'ON r.cat_id = cat.id', 'LEFT');
	    		$this->db->join($this->catTable . ' subcat' , 'ON r.subcat_id = subcat.id', 'LEFT');
	    		$this->db->join($this->rfqImgTable . ' ri' , 'ON r.rfq_id = ri.req_id', 'LEFT');

	    		$this->db->where('r.rfq_id', $id);

	    		$query=$this->db->get();

	    		$data=[];
	        	foreach($query->result_array() as $row){
					$rfq = $row['rfq_id'] . '_data';

	        		if (!isset($data[$rfq])) {
	        			$data[$rfq]['id'] = $row['rfq_id'];
	        			$data[$rfq]['quantity'] = $row['quantity'];
	        			$data[$rfq]['message'] = $row['message'];
	        			$data[$rfq]['other_requerment'] = $row['other_requerment'];

	        			if ($row['expiry_date'] != NULL) {
	        				$data[$rfq]['expiry_date'] = date('d-m-Y', strtotime($row['expiry_date']));
	        			}
	        			else {
	        				$data[$rfq]['expiry_date'] = '';
	        			}
	        			
	        			$data[$rfq]['item_name'] = $row['item_name'];
	        			$data[$rfq]['species_name'] = $row['species_name'];
	        			$data[$rfq]['UOM'] = $row['UOM'];
	        			$data[$rfq]['customer'] = $row['customer'];
	        			$data[$rfq]['category'] = $row['category'];
	        			$data[$rfq]['subcategory'] = $row['subcategory'];
	        			$data[$rfq]['business_name'] = $row['business_name'];
	        			$data[$rfq]['image'] = [];
	        		}

	        		$data[$rfq]['image'][] = ['img_id' => $row['img_id'], 'image' => $row['image']];
	        	}
	        	
	        	return array_values($data);
		 	}
		 	else { //enq

		 		// SELECT e.id, ct.name AS to_cust, cf.name AS from_cust, e.quantity, e.message, DATE_FORMAT(e.date, "%d-%m-%Y %H:%i") AS date, i.item_name FROM t_enquiry e LEFT JOIN t_customer ct ON e.to_id = ct.id LEFT JOIN t_customer cf ON e.from_id = cf.id LEFT JOIN t_seller_item si ON e.seller_item_id = si.id LEFT JOIN t_item i ON si.item_id = i.id

	    		$this->db->select("ct.name AS to_cust, cf.name AS from_cust, e.quantity, e.message, DATE_FORMAT(e.date, '%d-%m-%Y %H:%i') AS date, i.item_name");
	    		$this->db->from($this->enqTable." e");
	    		$this->db->join($this->customerTable . ' ct' , 'ON e.to_id = ct.id', 'LEFT');
	    		$this->db->join($this->customerTable . ' cf' , 'ON e.from_id = cf.id', 'LEFT');
	    		$this->db->join($this->siTable . ' si' , 'ON e.seller_item_id = si.id', 'LEFT');
	    		$this->db->join($this->itemTable . ' i' , 'ON si.item_id = i.id', 'LEFT');

	    		$this->db->where('e.id', $id);

	    		$query=$this->db->get();

	    		$result=[];
	        	foreach($query->result_array() as $row){
	        		$result[]=$row;
	        	}
	        	return $result;
		 	}

		 	
	    } 
	    catch (Exception $e) {
	       throw new Exception();
	    }	

    }

    

    public function getReportDetailById($to_id, $from_id) {
    	try {
		 	$this->db->db_debug = false;

		 	// SELECT ri.id, ri.message, ri.images, DATE_FORMAT(ri.created_date, '%d-%m-%Y %r') AS created_date, cf.name AS accused_by, ct.name AS accused FROM t_report_incident ri LEFT JOIN t_customer cf ON cf.id = ri.from_id LEFT JOIN t_customer ct ON ct.id = ri.to_id WHERE ri.from_id = 1 AND ri.to_id = 3
 

		 	$this->db->select("ri.id, ri.message, ri.images, DATE_FORMAT(ri.created_date, '%d-%m-%Y %r') AS created_date, cf.name AS accused_by, ct.name AS accused");
		 	$this->db->from($this->riTable." ri");

    		$this->db->join($this->customerTable . ' ct' , 'ON ri.to_id = ct.id', 'LEFT');
    		$this->db->join($this->customerTable . ' cf' , 'ON ri.from_id = cf.id', 'LEFT');

    		$this->db->where('ri.from_id', $from_id);
    		$this->db->where('ri.to_id', $to_id);

    		$query=$this->db->get();

    		//echo $this->db->last_query();die();

    		$db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{

	        	if ($query->num_rows() > 0) {
	        		$result=[];
		        	foreach($query->result_array() as $row){
		        		if ($row['id'] != null) {
		        			$result[] = $row;
		        		}		        		
		        	}
		        	//print_r($result); die();
		        	return $result;
	        	}	        	
	        }
	    } 
	    catch (Exception $e) {
	       throw new Exception();
	    }	

    }
    
}

?>