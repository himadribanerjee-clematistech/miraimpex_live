<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="clearfix mb-10"> 
        <div class="pull-left">
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Item</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Add New Item</li>
                </ol>
            </nav>
        </div> 
        <div class="pull-right">
            <a class="btn btn-outline-primary" href="<?php echo base_url()?>item">Back</a>            
        </div> 
    </div>

    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                <form id="updateitemfrm" method="post" action="javascript:void(0);" autocomplete="off" enctype="multipart/form-data" onsubmit="return addItemDetails();">
                    <input type="hidden" id="id" value="<?php echo (isset($item_detail) && isset($item_detail["id"]) )?base64_encode($item_detail["id"]):''; ?>" >

                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-3">
                                <label label-for='name'>Item Name <span class="mandatory_label">*</span></label>
                                <input type="text" name="item_name" id="item_name" value="" placeholder="Item Name" class="form-control">
                            </div>
                            
                            <div class="col-lg-3">
                                <label label-for='name'>Category <span class="mandatory_label">*</span></label>
                                <select name="category" id="category" onchange="dogetSubCatByCat(this.value)" class="form-control">
                                    <option value=""> -- select Category-- </option>

                                    <?php

                                        foreach ($cat_details as $cat) {
                                            
                                    ?>

                                        <option value="<?php echo base64_encode($cat["id"]); ?>"> <?php echo $cat["category_name"]; ?> </option>

                                    <?php

                                        }

                                    ?>

                                </select>
                            </div>
                            <div class="col-lg-3">
                                <label label-for='name'>Sub Category <span class="mandatory_label">*</span></label>
                                <select name="subcategory" id="subcategory" class="form-control">
                                    <option value=""> -- select Sub Category-- </option>
                                     <?php

                                        foreach ($subcat_data as $cat) {
                                            
                                    ?>

                                        <option value="<?php echo base64_encode($cat["id"]); ?>"> <?php echo $cat["category_name"]; ?> </option>

                                    <?php

                                        }

                                    ?>
                                </select>
                            </div>
                            <div class="col-lg-3">
                                <label for="local_name">Local Name</label>
                                <input type="text" name="local_name" id="local_name" value="" placeholder="Local Name" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">                            
                            <div class="col-lg-4">
                                <label for="local_name">Item Description</label>
                                <textarea name="item_description" id="item_description" placeholder="Item Description" style="height: auto;" class="form-control" rows="10"></textarea>
                            </div>
                            <div class="col-lg-8">
                                <label for="local_name">Item Image</label>
                                <br>
                                <input type="file" name="item_image" id="item_image" onchange="loadFile()">

                                <br><br><br>
                                <img src="<?php echo base_url('assets/images/no-item-image.png'); ?>" height="150" width="150"  style="border: 2px dotted #ccc; border-radius: 2px;" id="chImg"/>

                                <script type='text/javascript'>
                                    function loadFile(){
                                        
                                        var imgfilename = document.getElementById('chImg');

                                        imgfilename.src = URL.createObjectURL(event.target.files[0]);
                                    };
                                </script>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-12 text-right">
                                <input type="submit" value="Add" name="submit" class="btn btn-outline-primary">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<?php
    if(isset($custom_js)){
        foreach($custom_js as $val){
            ?>
            <script src="<?php echo base_url();?>assets/js/custom_js/<?php echo $val;?>"></script>
            <?php
        }
    }
    
?>
  