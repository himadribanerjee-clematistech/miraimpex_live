<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class Item extends CI_Controller {
	protected $token_id='';
	protected $per_page=10;
	public function __construct() {
    	parent::__construct();
    	$this->token_id=get_cookie('token_id'); 
    	if(trim($this->token_id)==''){
    		redirect("login");
    	}
        $this->load->model("itemModel");
        $this->load->model("userModel");
        $this->load->model('categoryModel');
        $this->load->model("subcategoryModel");
        $this->load->helper("pagination");
        $this->load->library("pagination");
         $this->load->model("settingsModel", 'sM');
        $settings_data=$this->sM->getSettings();
        if(isset($settings_data->per_page)){
         $this->per_page=$settings_data->per_page;
        }
    }
    public function index()
	{
		try {
           $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            
            if(empty($user_details)){
                    $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                    delete_cookie('token_id'); 
                     delete_cookie('useName'); 
                    echo '<script>
                        alert("Unexpected server error.Please try again some time.");
                            window.location="'.base_url().'login";
                    </script>';
            }
            $user_details=current($user_details);
            $data=[]; 
            $data["menu"]="item";
            $data["useName"]=$user_details["Name"];
            $data["custom_js"]=['item.js'];
            set_cookie('useName',$user_details["Name"],'3600');
            $data['cat_details'] = $this->categoryModel->getCatList(["active"=>1]);

           
            //search 
            $search_input=$this->input->get();
            //print_r($search_input); die();

            $item_search=[];
            $data["subcat_data"]=[];
            if(isset($search_input["itemName"]) && trim($search_input["itemName"])!=''){
            	$item_search["item_name"] = $search_input["itemName"];

            }

             if(isset($search_input["category"]) && trim($search_input["category"])!=''){
                $item_search["category_id"] = base64_decode($search_input["category"]);
                 $data["subcat_data"]=$this->subcategoryModel->getsubCatList(["active"=>1],'', '', $item_search["category_id"]);
               


            }
             if(isset($search_input["subcategory"]) && trim($search_input["subcategory"])!=''){
                $item_search["subcategory_id"] = base64_decode($search_input["subcategory"]);

            }
             if(isset($search_input["status"]) && trim($search_input["status"])!=''){
                $item_search["actve"]=$search_input["status"];
                
            }
            $data["item_search"] = $item_search;
            $page=0;
			if($this->input->get("per_page")){
	       		$page= $this->input->get("per_page");
	       	}

          $data["page"]=$page;

            $total_row=$this->itemModel->getitemCount($item_search);
         
	        $data["links"] = createPageNationLink(base_url('item/index'), $total_row,$this->per_page);

	        $start= (int)$page +1;
			$end = ($page+ $this->per_page >=$total_row)? $total_row : (int)$page  + $this->per_page;
			$data['result_count']='';
			if($total_row>0){
				$data['result_count']= "Showing ".$start." - ".$end." of ".$total_row." Results";
			}
			
			$data["item_data"]=$this->itemModel->getitemList($item_search,$this->per_page, $page);

            // echo '<pre>'; print_r($data); die();

            $data['view_file'] = 'Item';
            view($data);
           
        } catch (Exception $e) {
            $resposne=["status"=>"1","e"=>$e,"message"=>"Unexpected server error.Please try again some time.","field"=>'user_name'];
        }   
	}
	public function dogetItemByCatSubCat(){
          try {
            $subcate=$this->input->post("subcate");
             $category=$this->input->post("category");
             $resposne=["status"=>"0","message"=>"", "field"=>''];
              $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
              if(trim($this->token_id)!=''){
                if($category==''){
                    $resposne=["status"=>"0","result"=>[]];
                }else{
                     $item_search=[];
                     $item_search["actve"] =1;
                    if(trim($category)!=''){
                         $item_search["category_id"] = base64_decode($category);

                    }
                    if(trim($subcate)!=''){
                         $item_search["subcategory_id"] = base64_decode($subcate);

                    }

                    $item_data=$this->itemModel->getitemList( $item_search);
                    foreach($item_data as $key=>$value){
                        $item_data[$key]["id"]=base64_encode( $item_data[$key]["id"]);
                        }  
                     $resposne=["status"=>"0","result"=>$item_data];
                  
                }

              }else{
                 $resposne=["status"=>"1","message"=>"Permission Denied."];
              }  
        } catch (Exception $e) {
            $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time."];

        }
        echo json_encode($resposne); 
    }
    public function additem(){
        try {
            $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            if(empty($user_details)){
                    $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                    delete_cookie('token_id'); 
                     delete_cookie('role_id'); 
                    echo '<script>
                        alert("Session expired please login again.");
                            window.location="'.base_url().'login";
                    </script>';
                    exit();
            }
            $user_details=current($user_details);
            $data=[]; 
            $data["menu"]="item";
            $data["useName"]=$user_details["Name"];
            $data["custom_js"]=['item.js'];
            set_cookie('useName',$user_details["Name"],'3600');
             $data['cat_details'] = $this->categoryModel->getCatList(["active"=>1]);
             //$data['uom_details'] = $this->itemModel->getUom();

            $data['view_file'] = 'AddItem';
            view($data);
        } catch (Exception $e) {
            echo '<script>
                alert("Unexpected server error.Please try again some time.");
                            window.location="'.base_url().'category";
            </script>';

        }
    }
    public function doAddItem(){

         try {
            $data=$this->input->post();
            $resposne=["status"=>"0","message"=>"","field"=>''];
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));

            if(trim($this->token_id)!=''){
                if(isset($data["item_name"]) && trim($data["item_name"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter Item Name.","field"=>'item_name'];
                }else if(isset($data["category"]) && trim($data["category"])==''){
                    $resposne=["status"=>"1","message"=>"Please Select Category.","field"=>'cat_name'];
                }else if(isset($data["item_description"]) && trim($data["item_description"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter Item Description.","field"=>'item_description'];
                }else if(!empty($_FILES["item_image"]) && trim($_FILES["item_image"]["name"])==''){
                    $resposne=["status"=>"1","message"=>"Please Select Item Image.","field"=>'item_image'];
                }else{

                   $check_duplicate_itemname = $this->itemModel->doitemCheck($data["item_name"]);


                    if($check_duplicate_itemname == 1) {
                        $resposne=["status"=>"1","message"=>"Duplicate Item Name.","field"=>'item_name'];
                    }
                    if($resposne["status"]==0){
                        $file_name=time().trim($_FILES["item_image"]["name"]);

                        $allowedExts = array("gif", "jpeg", "jpg", "png");
                        $image_data=explode(".", $_FILES["item_image"]["name"]);
                        $extension =(!empty($image_data))?$image_data[1]:'';
                         
                        if ((($_FILES["item_image"]["type"] == "image/gif")  || ($_FILES["item_image"]["type"] == "image/jpeg")  || ($_FILES["item_image"]["type"] == "image/jpg")  || ($_FILES["item_image"]["type"] == "image/png")) && in_array($extension, $allowedExts))
                        {
                              $config['upload_path'] = './uploads/item/';
                               $config['allowed_types'] = 'jpg|png|jpeg|gif';
                              // $config['max_size'] = '200';
                             // $config['max_width'] = '800';
                             // $config['max_height'] = '800';
                              $config['file_name'] =$file_name;
                            
                              $this->load->library('upload', $config);
                              if ( ! $this->upload->do_upload('item_image')){
                                $resposne=["status"=>"1","message"=>"Please Select  jpg/png/jpeg/gif Item Image.",'error'=> $this->upload->display_errors(),"field"=>'image'];
                              
                              }else{

                                $string = $data['item_name'];
                                $searchStringComma = ',';
                                $searchStringHyphen = '-';
                                $new_string = '';

                                if( strpos($string, $searchStringComma) !== false ) {
                                    $new_string = str_replace(","," ", $string);
                                }

                                if ($new_string != '') { //If New String is not Blank
                                  if( strpos($new_string, $searchStringHyphen) !== false ) {
                                      $new_string = str_replace("-"," ", $new_string);
                                      $new_string = preg_replace('!\s+!', ' ', $new_string);
                                      
                                      $slug = strtolower(str_replace(" ","-", $new_string));
                                  }
                                  else {
                                    $new_string = preg_replace('!\s+!', ' ', $new_string);
                                    $slug = strtolower(str_replace(" ","-", $new_string));
                                  }
                                }
                                else { //If New String is Blank
                                  if( strpos($string, $searchStringHyphen) !== false ) {
                                      $new_string = str_replace("-"," ", $string);
                                      $new_string = preg_replace('!\s+!', ' ', $new_string);
                                      
                                      $slug = strtolower(str_replace(" ","-", $new_string));
                                  }
                                  else {
                                    $new_string = preg_replace('!\s+!', ' ', $string);
                                    $slug = strtolower(str_replace(" ","-", $new_string));
                                  }
                                }



                                 $item_data = [
                                    'item_name' => $data['item_name'], 
                                     'item_description' => $data['item_description'], 
                                       'local_name' => $data['local_name'], 
                                       'category_id' => base64_decode((trim($data['subcategory'])!='')?$data['subcategory']:$data['category']),
                                    'item_image' => $file_name,
                                    'slug' => $slug
                                  ];

                                  //print_r($item_data); die();

                                  $id = $this->itemModel->insertitem($item_data);
                                   if($id>0){
                                     $resposne=["status"=>"0","message"=>" Item added successfully.","field"=>'item_name'];
                                    }else{
                                        
                                        $resposne=["status"=>"1","message"=>"Item added failed.Please try again some time.","field"=>'item_name'];
                                        
                                    }
                              }
   
                        }
                        else {
                          $resposne=["status"=>"1","message"=>"Please Select  jpg/png/jpeg/gif Item Image.",'error'=> '',"field"=>'image'];
                        }
                        
                    }

                }
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
            }
        } catch (Exception $e) {
                $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'item_name'];

        }

        echo json_encode($resposne);

    }
    

    public function doitemEdit($id){
        $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            
        if(empty($user_details)){
                $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                delete_cookie('token_id'); 
                 delete_cookie('role_id'); 
                echo '<script>
                    alert("Session expired please login again.");
                        window.location="'.base_url().'login";
                </script>';
                exit();
        }
        $user_details=current($user_details);
        $id=base64_decode($id);
        if(trim($id)==''){
            echo '<script>
                    alert("Invalid user.");
                        window.location="'.base_url().'user";
                </script>';
                exit();
        }
        $data=[]; 
        $data["menu"]="item";
        $data["useName"]=$user_details["Name"];
        $data["custom_js"]=['item.js'];
        set_cookie('useName',$user_details["Name"],'3600');

        $data['item_detail'] = current($this->itemModel->getById($id));
        $data["subcat_data"]=$this->subcategoryModel->getsubCatList(["active"=>1],'', '', $data['item_detail']["category_id"]);
        $data['cat_details'] = $this->categoryModel->getCatList(["active"=>1]);

       // echo '<pre>'; print_r($data); die();
         
        $data['view_file'] = 'EditItem';
        view($data);
            
    }
    public function doUpdateItem(){
        $data=$this->input->post();

        //print_r($data); die();
         $resposne=["status"=>"0","message"=>"","field"=>''];
        
        try {
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
            if(trim($this->token_id)!=''){
                    if(isset($data["id"]) && trim($data["id"])==''){
                         $resposne=["status"=>"1","message"=>"Invalid Request.","field"=>'cat_name'];
                    }else{
                        $data["id"]=base64_decode($data["id"]);
                        $category_details=current($this->itemModel->getById($data["id"]));
                        if(empty($category_details)){
                             $resposne=["status"=>"1","message"=>"Invalid Request.","field"=>'cat_name'];
                         }else{

                           if(isset($data["item_name"]) && trim($data["item_name"])==''){
                            $resposne=["status"=>"1","message"=>"Please Enter Item Name.","field"=>'item_name'];
                            }else if(isset($data["category"]) && trim($data["category"])==''){
                                $resposne=["status"=>"1","message"=>"Please Select Category.","field"=>'cat_name'];
                            }else if(isset($data["item_description"]) && trim($data["item_description"])==''){
                                $resposne=["status"=>"1","message"=>"Please Enter Item Description.","field"=>'item_description'];
                            }else if(!empty($_FILES["item_image"]) && trim($_FILES["item_image"]["name"])=='' && trim($category_details["item_image"])==''){
                                $resposne=["status"=>"1","message"=>"Please Select Item Image.","field"=>'image'];
                            }else{

                                     $check_duplicate_catname = $this->itemModel->doitemCheck($data["item_name"],$data["id"]);

                                    if(!empty($check_duplicate_catname)){
                                        $resposne=["status"=>"1","message"=>"Duplicate Item Name.","field"=>'item_name'];
                                    }
                                    if($resposne["status"]==0){

                                        if(!empty($_FILES["item_image"]) && trim($_FILES["item_image"]["name"])!=''){

                                              $file_name=time().'_'.$_FILES["item_image"]["name"];

                                              $allowedExts = array("gif", "jpeg", "jpg", "png");
                                              $image_data=explode(".", $_FILES["item_image"]["name"]);
                                              $extension =(!empty($image_data))?$image_data[1]:'';
                                               
                                              if ((($_FILES["item_image"]["type"] == "image/gif")  || ($_FILES["item_image"]["type"] == "image/jpeg")  || ($_FILES["item_image"]["type"] == "image/jpg")  || ($_FILES["item_image"]["type"] == "image/png")) && in_array($extension, $allowedExts))
                                              {



                                                $config['upload_path'] = './uploads/item/';
                                                $config['allowed_types'] = 'jpg|png|jpeg|gif';
                                               // $config['max_size'] = '200';
                                               // $config['max_width'] = '800';
                                               // $config['max_height'] = '800';
                                                $config['file_name'] =$file_name;
                                                
                                                 $this->load->library('upload', $config);
                                                 if ( ! $this->upload->do_upload('item_image')){
                                                   $resposne=["status"=>"1","message"=>"Please Select  jpg/png/jpeg/gif Item Image.","field"=>'image'];
                                                    
                                                  
                                                }else{
                                                     if(file_exists('./uploads/item/'.$category_details["item_image"])){
                                                        unlink('./uploads/item/'.$category_details["item_image"]);
                                                    }

                                                }
                                              }
                                              else {
                                                 $resposne=["status"=>"1","message"=>"Please Select  jpg/png/jpeg/gif Item Image.",'error'=> '',"field"=>'image'];
                                              }


                                        }else{
                                             $file_name=$category_details["item_image"];
                                        }
                                        if($resposne["status"]==0){

                                          $string = trim($data['item_name']);
                                          $searchStringComma = ',';
                                          $searchStringHyphen = '-';
                                          $new_string = '';

                                          if( strpos($string, $searchStringComma) !== false ) {
                                              $new_string = str_replace(","," ", $string);
                                          }

                                          if ($new_string != '') { //If New String is not Blank
                                            if( strpos($new_string, $searchStringHyphen) !== false ) {
                                                $new_string = str_replace("-"," ", $new_string);
                                                $new_string = preg_replace('!\s+!', ' ', $new_string);
                                                
                                                $slug = strtolower(str_replace(" ","-", $new_string));
                                            }
                                            else {
                                              $new_string = preg_replace('!\s+!', ' ', $new_string);
                                              $slug = strtolower(str_replace(" ","-", $new_string));
                                            }
                                          }
                                          else { //If New String is Blank
                                            if( strpos($string, $searchStringHyphen) !== false ) {
                                                $new_string = str_replace("-"," ", $string);
                                                $new_string = preg_replace('!\s+!', ' ', $new_string);
                                                
                                                $slug = strtolower(str_replace(" ","-", $new_string));
                                            }
                                            else {
                                              $new_string = preg_replace('!\s+!', ' ', $string);
                                              $slug = strtolower(str_replace(" ","-", $new_string));
                                            }
                                          }




                                           $item_data = [
                                            'item_name' => $data['item_name'], 
                                             'local_name' => $data['local_name'], 
                                              'item_description' => $data['item_description'], 
                                               'category_id' => base64_decode((trim($data['subcategory'])!='')?$data['subcategory']:$data['category']),
                                            'item_image' => $file_name,
                                            'slug' => $slug
                                          ];

                                          //print_r($item_data); die();

                                            $id=$this->itemModel->updateItem($item_data,["id"=>$data["id"]]);
                                            if($id>0){
                                                $resposne=["status"=>"0","message"=>" Item updated successfully.","field"=>'item_name'];
                                            }else{
                                                
                                                $resposne=["status"=>"1","message"=>"Item updated failed.Please try again some time.","field"=>'item_name'];
                                                
                                            }
                                        }


                                    }


                            }

                         }

                    }


               
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
            }
        }catch (Exception $e) {
                $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'user_name','error'=>$e];

        }

        echo json_encode($resposne);
    }

    

    public function doShwCaseStatusChange(){
         $data=$this->input->post();
        $data["id"]=base64_decode($data["id"]);
        $resposne=["status"=>"0","message"=>"","field"=>''];
        try {
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
            if(trim($this->token_id)!=''){
              $res=$this->itemModel->updateItem(["showcase"=>$data["showcase"]],["id"=>$data["id"]]);
                        
                if($res){
                    $resposne=["status"=>"0","message"=>"Item status change successfully."];
                }
                else{                                
                    $resposne=["status"=>"1","message"=>"Item change failed.Please try again after some time."];                                
                }
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
            }   
        }catch (Exception $e) {
                $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'status'];

        }

        echo json_encode($resposne);
    }

    public function doDisplayStatusChange(){
         $data=$this->input->post();
        $data["id"]=base64_decode($data["id"]);
        $resposne=["status"=>"0","message"=>"","field"=>''];
        try {
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
            if(trim($this->token_id)!=''){
              $res=$this->itemModel->updateItem(["on_display"=>$data["on_display"]],["id"=>$data["id"]]);
                        
                if($res){
                    $resposne=["status"=>"0","message"=>"Item status change successfully."];
                }
                else{                                
                    $resposne=["status"=>"1","message"=>"Item change failed.Please try again after some time."];                                
                }
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
            }   
        }catch (Exception $e) {
                $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'status'];

        }

        echo json_encode($resposne);
    }

  
    public function doitemStatusChange(){
         $data=$this->input->post();
        $data["id"]=base64_decode($data["id"]);
        $resposne=["status"=>"0","message"=>"","field"=>''];
        try {
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
            if(trim($this->token_id)!=''){
              $res=$this->itemModel->updateItem(["active"=>$data["active"]],["id"=>$data["id"]]);
                        
                if($res){
                    $resposne=["status"=>"0","message"=>"Item status change successfully."];
                }
                else{                                
                    $resposne=["status"=>"1","message"=>"Item change failed.Please try again after some time."];                                
                }
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
            }   
        }catch (Exception $e) {
                $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'status'];

        }

        echo json_encode($resposne);
    }
    public function doitemDelete(){
        $data=$this->input->post();
        $data["id"]=base64_decode($data["id"]);
        $resposne=["status"=>"0","message"=>"","field"=>''];
        try {
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
            if(trim($this->token_id)!=''){
                $res = $this->itemModel->doDeleteitem($data["id"]);
                        
                if($res){
                    $resposne=["status"=>"0","message"=>"Item Deleted successfully."];
                }
                else{                                
                    $resposne=["status"=>"1","message"=>"Item Deletion failed.Please try again after some time."];                                
                }
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
            }   
        }catch (Exception $e) {
                $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'role_id'];

        }

        echo json_encode($resposne);    
    }   

}
?>