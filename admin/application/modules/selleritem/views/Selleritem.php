
<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="clearfix mb-10">
        <div class="pull-left">
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Seller Item</a></li>
                    <li class="breadcrumb-item active" aria-current="page">List</li>
                </ol>
            </nav>
        </div>
        <!-- <div class="pull-right">
            <a class="btn btn-outline-primary" href="<?php //echo base_url('selleritem/additem')?>">Add New</a>            
        </div> -->
    </div>

    <div class="container-fluid bg-light"> 
        <div class="row">
            <div class="col-lg-12">

                <form id="catsrch" action="<?php echo base_url('selleritem')?>">
                    <input type="hidden" name="per_page_search" value="<?php echo $page; ?>">

                    <div class="row align-items-center justify-content-center">
                        <div class="col-md-3 pt-3">
                            <div class="form-group ">
                                <select name="customer_id" id="customer_id" class="form-control">
                                    <option value=""> -- select Customer -- </option>

                            <?php

                                foreach ($customer as $cust) {
                                    
                            ?>

                                    <option value="<?php echo base64_encode($cust["id"]); ?>" <?php echo (isset($item_search) && isset($item_search["customer_id"]) && ($item_search["customer_id"] == $cust["id"]) )? 'selected':''; ?> > <?php echo $cust["name"]; ?> </option>

                            <?php

                                }

                            ?>

                        
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3 pt-3">
                            <div class="form-group">
                                <select name="item_id" id="item_id" onchange="getSpecies(this.value)" class="form-control">
                                    <option value=""> -- select Item -- </option>

                            <?php

                                foreach ($item as $item) {
                                    
                            ?>

                                    <option value="<?php echo base64_encode($item["id"]); ?>" <?php echo (isset($item_search) && isset($item_search["item_id"]) && ($item_search["item_id"] == $item["id"]) )? 'selected':''; ?> > <?php echo $item["item_name"]; ?> </option>

                            <?php

                                }

                            ?>

                                
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3 pt-3">
                            <div class="form-group">
                                <select name="species_id" id="species_id" class="form-control"> 
                                    <option value=""> -- select Species-- </option>

                                <?php

                                    foreach ($species_data as $sp) {
                                        
                                ?>

                                        <option value="<?php echo base64_encode($sp["id"]); ?>"
                                            <?php 
                                                echo (isset($item_search) && isset($item_search["species_id"]) && ($item_search["species_id"] == $sp["id"]) )? 'selected':'';
                                            ?>

                                        > <?php echo $sp["species_name"]; ?> </option>

                                    <?php

                                        }

                                    ?>
                                </select>
                            </div>
                        </div>
                       
                        <div class="col-md-3">
                            <a class="btn btn-outline-primary" href="<?php echo base_url('selleritem')?>"><i class="fa fa-refresh" aria-hidden="true"></i> Reset</a> 
                                
                            <input type="submit" name="search" value="Search" class="btn btn-outline-primary">
                        </div>
                    </div>
                    
                </form>
            </div>
        </div>
    </div>

    <div class="row pt-2">
        
        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="data-table stripe hover no-wrap" style="font-size: 14px;">
            <thead>
                <tr class="titel_bar">
                    <td class="table-plus datatable-nosort"  width="15%">Customer</td>
                    <td class="table-plus datatable-nosort"  width="15%">Item</td>
                    <td class="table-plus datatable-nosort"  width="15%">Species</td>
                    <td class="table-plus datatable-nosort"  width="20%">Price</td>
                    <!-- <td class="table-plus datatable-nosort"  width="10%">Image</td> -->
                    <td class="table-plus datatable-nosort"  width="8%">Organic</td>
                    <td class="table-plus datatable-nosort text-right" width="10%" >Status</td>
                    <td class="table-plus datatable-nosort"  width="10%" ></td>
                    <!-- <td class="table-plus datatable-nosort"  width="10%" ><center>Action</center></td> -->
                </tr>
            </thead>
            <tbody>

    <?php
        if(!empty($seller_item)){
            foreach($seller_item as $val){
    ?>
                <tr class="">
                    <td class="table-plus" >
                        <?php echo $val["cust_name"];?>           
                    </td>
                    <td class="table-plus" >
                        <?php echo $val["item_name"];?>           
                    </td>   
                     <td class="table-plus" >
                        <?php echo $val["species_name"];?>           
                    </td>
                    <td class="table-plus" >
                        &#8358; <?php echo $val["min_price"];?> - &#8358; <?php echo $val["max_price"];?>
                            
                    </td>
                    <!-- <td class="table-plus" >
                        <img src="<?php //echo (isset($val) && ($val["item_image"] != '') )?base_url('uploads/selleritem/' . $val["item_image"]):base_url('assets/images/no-item-image.png'); ?>" width='100' height='100'>
                                  
                    </td> -->
                    <td class="table-plus">
                        <center>
                            <?php echo ($val['organic_YN'] == '1') ? '<i class="fa fa-check fa-2x" aria-hidden="true" style="color: green"></i>' : '<i class="fa fa-times fa-2x" aria-hidden="true" style="color: red"></i>'; ?>
                        </center>  
                    </td>
                    <td class="table-plus text-right">

                        <?php echo (!empty($val["active"]) && isset($val["active"]) && $val["active"]== 1) ? '<font size="4" color="green">Active</font>' : '<font size="4" color="red">Deleted</font>'; ?>

                    </td>
                    <td>
                        <center>
                            <a class="btn btn-outline-primary view" href="javascript:void(0);" data-id="<?php echo base64_encode($val["id"]); ?>" data-toggle="modal">
                                <i class="fa fa-eye" aria-hidden="true"></i> Details
                            </a>
                        </center>
                    </td>
                    <!-- <td class="table-plus" style="text-align:center;">

                        <div class="dropdown">
                            <a class="btn btn-outline-primary dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                <i class="fa fa-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="javascript:void(0);" style=" font-size: 15px;" onclick="doitemEdit('<?php //echo base64_encode($val["id"]);?>')" >
                                    <i class="fa fa-edit" aria-hidden="true"></i> 
                                    Edit
                                </a>
                            </div>
                        </div>

                    </td> -->

                </tr>
    <?php
            }
        }else{
    ?>

                <tr class="">
                    <td colspan="7">No Record Found</td>
                </tr>  

    <?php
        }
    ?>
           
                 
            </tbody>
        </table> 
    </div>
</div>




<div class="modal fade admin_modal" id="shwModal">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title"><small>Description of</small> <span class="species_name"></span></h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>Customer</label>
                            <br>
                            <span class="customer" style="font-weight: 500 !important;"></span>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>Item</label>
                            <br>
                            <span class="item_name" style="font-weight: 500 !important;"></span>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label>Quantity</label>
                            <br>
                            <span class="min_quantity" style="font-weight: 500 !important;"></span>
                        </div>
                    </div>
                </div>

                <br>

                <div class="row">
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label>Organic</label>
                            <br>
                            <span class="organic_YN"></span>
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="form-group">
                            <label>Price</label>
                            <br>
                            <span class="amount" style="font-weight: 500 !important;"></span>
                        </div>
                    </div>
                </div>

                <br>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Description</label>
                            <br>
                            <span class="item_description" style="font-weight: 500 !important;"></span>
                        </div>
                    </div>
                </div>

                <br>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Packing Information</label>
                            <br>
                            <span class="packing_information" style="font-weight: 500 !important;"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Shipping Information</label>
                            <br>
                            <span class="shipping_information" style="font-weight: 500 !important;"></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Special Information</label>
                            <br>
                            <span class="special_information" style="font-weight: 500 !important;"></span>
                        </div>
                    </div>
                </div>

                <br>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Images</label>
                            <br>
                            <span id="emImg"><ul class="image" style="list-style-type: none; display: flex;"></ul></span>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>





<script type="text/javascript">
    
    $(document).ready(function(){

        $('.view').click(function(e){
            var iid = $(this).attr('data-id');

            $.ajax({
                url: 'selleritem/getById',
                type: 'POST',
                data: { id: iid },
                dataType: 'JSON',
                success: function(response){

                    $('.image').html('');
                    $('.species_name').html('');
                    $('.customer').html('');
                    $('.item_description').html('');
                    $('.item_name').html('');
                    $('.amount').html('');
                    $('.min_quantity').html('');
                    $('.packing_information').html('');
                    $('.shipping_information').html('');
                    $('.special_information').html('');
                    
                    var price = '&#8358; ' + response.max_price + ' to &#8358; ' + response.min_price;

                    $('.species_name').html(response.species_name);
                    $('.customer').html(response.customer);
                    $('.item_description').html(response.item_description);
                    $('.item_name').html(response.item_name);
                    $('.amount').html(price);
                    $('.min_quantity').html(response.min_quantity);
                    $('.packing_information').html(response.packing_information);
                    $('.shipping_information').html(response.shipping_information);
                    $('.special_information').html(response.special_information);

                    if (response.organic_YN == 1) {
                        $('.organic_YN').html('<i class="fa fa-check" aria-hidden="true" style="color: green"></i>');
                    }
                    else {
                        $('.organic_YN').html('<i class="fa fa-times" aria-hidden="true" style="color: red"></i>');
                    }

                    $.each(response.image, function(key, value) {
                        if (value.image != null) {
                            $('.image').append('<li style="padding-left: 5px;"><img style="width: 110px; height: 110px;" src="uploads/selleritem/' + value.image + '" /></li>');
                        }
                        else {
                            $('.image').append('<li style="padding-left: 5px;"><img style="width: 110px; height: 110px;" src="assets/images/no-item-image.png" /></li>');
                        }                        
                    });               
                    

                    $('#shwModal').modal('show');
                }
            });
        });

    });

</script>


<?php
	if(isset($custom_js)){
		foreach($custom_js as $val){
			?>
			<script src="<?php echo base_url();?>assets/js/custom_js/<?php echo $val;?>"></script>
			<?php
		}
	}
	
?>
  