<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class SelleritemModel extends CI_Model {
	protected $custTable = 'customer';
	protected $itemTable = 'item';
	protected $speciesTable = 'species';
	protected $siTable = 'seller_item';
	protected $siiTable = 'seller_item_image';
	
	public function __construct() {
        parent::__construct();
    }
    
    public function getitemCount($data){
    	try {
    		$this->db->db_debug = false;
    		$this->db->select("c.name AS cust_name, i.item_name, s.species_name, si.*");
    		$this->db->from($this->siTable . ' si');
    		$this->db->join($this->custTable . ' c', 'c.id = si.customer_id');
    		$this->db->join($this->itemTable . ' i', 'i.id = si.item_id');
    		$this->db->join($this->speciesTable . ' s', 's.id = si.species_id');

			if(isset($data["customer_id"]) && $data["customer_id"]!=''){ 
	    		$this->db->like("si.customer_id",$data["customer_id"]);
	    	}
	    	if(isset($data["item_id"]) && $data["item_id"]!=''){ 
	    		$this->db->where("si.item_id",$data["item_id"]);

	    	}
	    	if(isset($data["species_id"]) && $data["species_id"]!=''){ 
	    		$this->db->where("si.species_id",$data["species_id"]);
	    	}
	    	
	    	$this->db->get();

	    	$db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	return $this->db->count_all_results();

	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
    }
    
    public function getSellerItem($data,$limit='', $start=''){
       	try {
    		$this->db->db_debug = false;
		
			$this->db->select("c.name AS cust_name, i.item_name, s.species_name, si.*");
    		$this->db->from($this->siTable . ' si');
    		$this->db->join($this->custTable . ' c', 'c.id = si.customer_id');
    		$this->db->join($this->itemTable . ' i', 'i.id = si.item_id');
    		$this->db->join($this->speciesTable . ' s', 's.id = si.species_id');
    		
    		if(isset($data["customer_id"]) && $data["customer_id"]!=''){ 
	    		$this->db->like("si.customer_id",$data["customer_id"]);
	    	}
	    	if(isset($data["item_id"]) && $data["item_id"]!=''){ 
	    		$this->db->where("si.item_id",$data["item_id"]);

	    	}
	    	if(isset($data["species_id"]) && $data["species_id"]!=''){ 
	    		$this->db->where("si.species_id",$data["species_id"]);
	    	}

	    	$this->db->order_by('si.id', 'DESC'); 
	    	
	    	if($limit!=''  && $start!=''){
	    		$this->db->limit($limit, $start);
	    	}
	    	
 
 			$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query();die();
	        	$result=[];
	        	foreach($query->result_array() as $row){
	        		$result[]=$row;
	        	}
	        	return $result;
	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
    }

    public function getCustomer() {
    	return $this->db->select('id, name')->where('active', 1)->from($this->custTable)->get()->result_array();
    }

    public function getItem() {
    	return $this->db->select('id, item_name')->from($this->itemTable)->where("active",1)->get()->result_array();
    }

    public function getSpecies($item_id = '') {
    	if ($item_id != '') {
    		return $this->db->select('id, species_name')->from($this->speciesTable)->where("item_id", $item_id)->where("active",1)->get()->result_array();
    	}
    	else {
    		return $this->db->select('id, species_name')->from($this->speciesTable)->where("active",1)->get()->result_array();
    	}
    	

    	//echo $this->db->last_query();
    }

    public function getDataById($id){
    	try {
		 	$this->db->db_debug = false;

		 	// SELECT s.species_name, i.item_name, c.name, si.item_description, si.special_information, si.packing_information, si.shipping_information, si.min_quantity, si.min_price, si.max_price, si.organic_YN, sii.image FROM t_seller_item si LEFT JOIN t_seller_item_image sii ON sii.seller_item_id = si.id LEFT JOIN t_species s ON s.id = si.species_id LEFT JOIN t_item i ON i.id = si.item_id LEFT JOIN t_customer c ON c.id = si.customer_id WHERE si.id = 1


		 	$this->db->select("s.species_name, i.item_name, c.name AS customer, si.id, si.item_description, si.special_information, si.packing_information, si.shipping_information, si.min_quantity, si.min_price, si.max_price, si.organic_YN, sii.image");
    		$this->db->from($this->siTable." si");
    		$this->db->join($this->siiTable . ' sii' , 'ON sii.seller_item_id = si.id', 'LEFT');
    		$this->db->join($this->speciesTable . ' s' , 'ON s.id = si.species_id', 'LEFT');
    		$this->db->join($this->itemTable . ' i' , 'ON i.id = si.item_id', 'LEFT');
    		$this->db->join($this->custTable . ' c' , 'ON c.id = si.customer_id', 'LEFT');
	    	
	    	$this->db->where("si.id", $id);
	    	
	    	$query=$this->db->get();

	    	//echo $this->db->last_query(); die();

	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	$data=[];
	        	foreach($query->result_array() as $row){
	        		$rcat = $row["id"]."_si";

	        		if(!isset($data[$rcat])){
	        			$data[$rcat]["id"]=base64_encode($row["id"]);
	        			$data[$rcat]["species_name"]=$row["species_name"];
	        			$data[$rcat]["item_name"]=$row["item_name"];
	        			$data[$rcat]["customer"]=$row["customer"];
	        			$data[$rcat]["item_description"]=$row["item_description"];
	        			$data[$rcat]["special_information"]=$row["special_information"];
	        			$data[$rcat]["packing_information"]=$row["packing_information"];
	        			$data[$rcat]["shipping_information"]=$row["shipping_information"];
	        			$data[$rcat]["min_quantity"]=$row["min_quantity"];
	        			$data[$rcat]["min_price"]=$row["min_price"];
	        			$data[$rcat]["max_price"]=$row["max_price"];
	        			$data[$rcat]["organic_YN"]=$row["organic_YN"];
	        			$data[$rcat]["image"]=[];
	        			
	        		}
	        		$data[$row["id"]."_si"]["image"][] = ["image" => $row["image"]];
	        		
	        		
	    	
	        	}

	        	//print_r(array_values($data)); die();

	        	//echo $this->db->last_query(); die();
	        	return array_values($data);
	        }
	        return TRUE;
	    } catch (Exception $e) {
	       throw new Exception();
	    }	
    }

    public function doCheck($cust, $item, $species, $id=''){
    	//print_r($data); die();
		try {
		 	$this->db->db_debug = FALSE;
	        $this->db->select("id");
	    	$this->db->from($this->siTable);
	    	if(isset($cust) && trim($cust)!='' ){
	    		$this->db->where("customer_id",$cust);
	    		
	    	}
	    	if(isset($item) && trim($item)!='' ){
	    		$this->db->where("item_id",$item);
	    		
	    	}
	    	if(isset($species) && trim($species)!='' ){
	    		$this->db->where("species_id",$species);
	    		
	    	}

	    	if($id!=''){
	    		$this->db->where("id !=",$id);
	    	}
	    	
	    	$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query() . '<br>';
	        	if($query->num_rows() >0) {
	        		return True;
	        	}
	        	else {
	        		return false;
	        	}
	        }
	        //return TRUE;
	    } catch (Exception $e) {
	       throw new Exception();
	    }	
    }
     
    public function insertNew($data){
     	//print_r($data); die();
    	try {
    	 	$this->db->db_debug = FALSE;
    	 	$res = $this->db->insert($this->siTable, $data);

    	 	$db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }
	        else {
	        	//echo $this->db->last_query() . '<br>';
	        	if ($res) {
	        		return true;
	        	}
	        	else {
	        		return false;
	        	}
	        }
	        //return  $this->db->insert_id();
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }
    	
    }

    public function getById($id){
    	try {
		 	$this->db->db_debug = false;
	        $this->db->where("id", $id);	    	
	    	$query=$this->db->get($this->siTable);
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	$data=[];
	        	foreach($query->result_array() as $row){
	        		 $data[]=$row;
	    	
	        	}
	        	//echo $this->db->last_query();
	        	return $data;
	        }
	        return TRUE;
	    } catch (Exception $e) {
	       throw new Exception();
	    }	
    }

    public function updateSellerItem($data,$where){
    	
    	try {
    	 	$this->db->db_debug = False;
    	 	$res = $this->db->update($this->siTable, $data, ['id' => $where]);

	    	$db_error = $this->db->error();
	        if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }
	        else {

	        	if ($res) {
	        		return True;
	        	}
	        	else {
	        		return False;
	        	}
	        }
	        
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }
    }
   	
}

?>