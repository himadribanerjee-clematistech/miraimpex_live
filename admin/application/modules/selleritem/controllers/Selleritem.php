<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class Selleritem extends CI_Controller {
	protected $token_id='';
	protected $per_page=10;
	public function __construct() {
    	parent::__construct();
    	$this->token_id=get_cookie('token_id'); 
    	if(trim($this->token_id)==''){
    		redirect("login");
    	}

        $this->load->model("userModel");
        $this->load->model("selleritemModel");
        $this->load->helper("pagination");
        $this->load->library("pagination");
         $this->load->model("settingsModel", 'sM');
        $settings_data=$this->sM->getSettings();
        if(isset($settings_data->per_page)){
         $this->per_page=$settings_data->per_page;
        }
    }
    public function index()
	{
		try {
           $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            
            if(empty($user_details)){
                $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                delete_cookie('token_id'); 
                 delete_cookie('useName'); 
                echo '<script>
                    alert("Unexpected server error.Please try again some time.");
                        window.location="'.base_url().'login";
                </script>';
            }
            $user_details=current($user_details);
            $data=[]; 
            $data["menu"]="selleritem";
            $data["useName"]=$user_details["Name"];
            $data["custom_js"]=['selleritem.js'];
            set_cookie('useName',$user_details["Name"],'3600');

            //search 
            $search_input=$this->input->get();
            //print_r($search_input); die();

            $item_search=[];
            $data["species_data"]=[];
            
            if(isset($search_input["customer_id"]) && trim($search_input["customer_id"])!=''){
                $item_search["customer_id"] = base64_decode($search_input["customer_id"]);

            }

            if(isset($search_input["item_id"]) && trim($search_input["item_id"])!=''){
                $item_search["item_id"] = base64_decode($search_input["item_id"]); 

                $data["species_data"]=$this->selleritemModel->getSpecies($item_search["item_id"]);
            }
             
            if(isset($search_input["species_id"]) && trim($search_input["species_id"])!=''){
                $item_search["species_id"] = base64_decode($search_input["species_id"]);

            }

            $data["item_search"] = $item_search;
            $page=0;
            if($this->input->get("per_page")){
                $page= $this->input->get("per_page");
            }

            $data["page"]=$page;

            $total_row=$this->selleritemModel->getitemCount($item_search);
         
            $data["links"] = createPageNationLink(base_url('selleritem/index'), $total_row,$this->per_page);

            $start= (int)$page +1;
            $end = ($page+ $this->per_page >=$total_row)? $total_row : (int)$page  + $this->per_page;
            $data['result_count']='';
            if($total_row>0){
                $data['result_count']= "Showing ".$start." - ".$end." of ".$total_row." Results";
            }

            $data["seller_item"] = $this->selleritemModel->getSellerItem($item_search,$this->per_page, $page);

            //Getting Customer
            $data['customer'] = $this->selleritemModel->getCustomer();

            //Getting Item
            $data['item'] = $this->selleritemModel->getItem();
            
            $data['view_file'] = 'Selleritem';
            view($data);

        } catch (Exception $e) {
            $resposne=["status"=>"1","e"=>$e,"message"=>"Unexpected server error.Please try again some time.","field"=>'user_name'];
        }   
    }

    public function additem(){
        try {
            $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            if(empty($user_details)){
                    $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                    delete_cookie('token_id'); 
                     delete_cookie('role_id'); 
                    echo '<script>
                        alert("Session expired please login again.");
                            window.location="'.base_url().'login";
                    </script>';
                    exit();
            }
            $user_details=current($user_details);
            $data=[]; 
            $data["menu"]="selleritem";
            $data["useName"]=$user_details["Name"];
            $data["custom_js"]=['selleritem.js'];
            set_cookie('useName',$user_details["Name"],'3600');

            //Getting Customer
            $data['customer'] = $this->selleritemModel->getCustomer();

            //Getting Item
            $data['item'] = $this->selleritemModel->getItem();

            

            $data['view_file'] = 'AddSelleritem';
            view($data);
        } catch (Exception $e) {
            echo '<script>
                alert("Unexpected server error.Please try again some time.");
                            window.location="'.base_url().'selleritem";
            </script>';

        }
    }

    public function getSpecies() {
        try{
            $id=$this->input->post("id");
            $resposne=["status"=>"0","message"=>"", "field"=>''];
              
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
              
            if(trim($this->token_id)!=''){
                if($id==''){
                    $resposne=["status"=>"0","result"=>[]];
                }else{
                    //Getting Species, Item wise
                    $species = $this->selleritemModel->getSpecies(base64_decode($id));
                    foreach($species as $key=>$value){
                        $species[$key]["id"]=base64_encode( $species[$key]["id"]);
                    }  

                    //print_r($species); die();

                    $resposne=["status"=>"0","result"=>$species];                  
                }
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied."];
            }

            echo json_encode($resposne);
            
        }
        catch (Exception $e) {
            echo '<script>
                alert("Unexpected server error.Please try again some time.");
                            window.location="'.base_url().'selleritem";
            </script>';
        }
    }

    public function doAddItem() {
        try {
            $data=$this->input->post();

            $resposne=["status"=>"0","message"=>"","field"=>''];
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));

            if(trim($this->token_id)!=''){
                if(!(isset($data["customer_id"]) && trim($data["customer_id"])!='')){
                    $resposne=["status"=>"1","message"=>"Please Select Customer Name.","field"=>'customer_id'];
                }
                else if(!(isset($data["item_id"]) && trim($data["item_id"])!='')){
                    $resposne=["status"=>"1","message"=>"Please Select Item.","field"=>'item_id'];
                }
                else if(!(isset($data["species_id"]) && trim($data["species_id"])!='')){
                    $resposne=["status"=>"1","message"=>"Please Select Species.","field"=>'species_id'];
                }
                else if(!(isset($data["item_description"]) && trim($data["item_description"])!='')){
                    $resposne=["status"=>"1","message"=>"Please Enter Item Description.","field"=>'item_description'];
                }
                else if(!(isset($data["special_information"]) && trim($data["special_information"])!='')){
                    $resposne=["status"=>"1","message"=>"Please Enter Special Information.","field"=>'special_information'];
                }
                else if(!(isset($data["packing_information"]) && trim($data["packing_information"])!='')){
                    $resposne=["status"=>"1","message"=>"Please Enter Packing Information.","field"=>'packing_information'];
                }
                else if(!(isset($data["shipping_information"]) && trim($data["shipping_information"])!='')){
                    $resposne=["status"=>"1","message"=>"Please Enter Packing Information.","field"=>'shipping_information'];
                }else if(!empty($_FILES["item_image"]) && trim($_FILES["item_image"]["name"])==''){
                    $resposne=["status"=>"1","message"=>"Please Select Item Image.","field"=>'item_image'];
                }else if(!(isset($data["min_quantity"]) && trim($data["min_quantity"])!='')){
                    $resposne=["status"=>"1","message"=>"Please Enter Minimum Quantity.","field"=>'min_quantity'];
                }else if(!(isset($data["min_price"]) && trim($data["min_price"])!='')){
                    $resposne=["status"=>"1","message"=>"Please Enter Minimum Price.","field"=>'min_price'];
                }else if(!(isset($data["max_price"]) && trim($data["max_price"])!='')){
                    $resposne=["status"=>"1","message"=>"Please Enter Maximum Price.","field"=>'max_price'];
                }else{

                   $check_duplicate = $this->selleritemModel->doCheck(base64_decode($data["customer_id"]), base64_decode($data['item_id']), base64_decode($data['species_id']));


                    if($check_duplicate == 1) {
                        $resposne=["status"=>"1","message"=>"Duplicate Item Name.","field"=>'item_id'];
                    }
                    if($resposne["status"]==0){
                        $file_name=time().$_FILES["item_image"]["name"];

                        $allowedExts = array("gif", "jpeg", "jpg", "png");
                        $image_data=explode(".", $_FILES["item_image"]["name"]);
                        $extension =(!empty($image_data))?$image_data[1]:'';
                         if ((($_FILES["item_image"]["type"] == "image/gif")  || ($_FILES["item_image"]["type"] == "image/jpeg")  || ($_FILES["item_image"]["type"] == "image/jpg")  || ($_FILES["item_image"]["type"] == "image/png")) && in_array($extension, $allowedExts)){
                             $config['upload_path'] = './uploads/selleritem/';
                           $config['allowed_types'] = 'jpg|png|jpeg|gif';
                            // $config['max_size'] = '200';
                           // $config['max_width'] = '800';
                           // $config['max_height'] = '800';
                            $config['file_name'] =$file_name;

                            $this->load->library('upload', $config);
                            if ( ! $this->upload->do_upload('item_image')){
                                $resposne=["status"=>"1","message"=>"Please Select  jpg/jpeg/png/jpeg Item Image.",'error'=> $this->upload->display_errors(),"field"=>'image'];
                              
                            }else{
                                $item_data = [
                                    'customer_id'           =>  base64_decode($data["customer_id"]),
                                    'item_id'               =>  base64_decode($data['item_id']),
                                    'species_id'            =>  base64_decode($data['species_id']),
                                    'item_description'      =>  $data['item_description'],
                                    'special_information'   =>  $data['special_information'],
                                    'packing_information'   =>  $data['packing_information'],
                                    'shipping_information'  =>  $data['shipping_information'],
                                    'item_image'            =>  $file_name,
                                    'min_quantity'          =>  $data['min_quantity'],
                                    'min_price'             =>  $data['min_price'],
                                    'max_price'             =>  $data['max_price'],
                                    'organic_YN'            =>  $data['organic_YN'],

                                ];

                                $res = $this->selleritemModel->insertNew($item_data);
                                   
                                if($res>0){
                                    $resposne=["status"=>"0","message"=>" Item added successfully.","field"=>'item_name'];
                                }else{
                                    $resposne=["status"=>"1","message"=>"Item added failed.Please try again some time.","field"=>'item_name'];
                                }
   
                            }
                         }else{
                            $resposne=["status"=>"1","message"=>"Please Select  jpg/jpeg/png/jpeg Item Image.",'error'=> '',"field"=>'image'];
                              
                         }

                       
                            
                        
                    }

                }
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
            }
        } catch (Exception $e) {
            $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'item_name'];

        }

        echo json_encode($resposne);
    }

    public function doitemEdit($id) {
        $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            
        if(empty($user_details)){
                $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                delete_cookie('token_id'); 
                 delete_cookie('role_id'); 
                echo '<script>
                    alert("Session expired please login again.");
                        window.location="'.base_url().'login";
                </script>';
                exit();
        }
        $user_details=current($user_details);
        $id=base64_decode($id);
        if(trim($id)==''){
            echo '<script>
                    alert("Invalid user.");
                        window.location="'.base_url().'selleritem";
                </script>';
                exit();
        }
        $data=[]; 
        $data["menu"]="selleritem";
        $data["useName"]=$user_details["Name"];
        $data["custom_js"]=['selleritem.js'];
        set_cookie('useName',$user_details["Name"],'3600');
        $data['selleritemDetail'] = current($this->selleritemModel->getById($id));

        //Getting Customer
        $data['customer'] = $this->selleritemModel->getCustomer();

        //Getting Item
        $data['item'] = $this->selleritemModel->getItem();

        $data['species'] = $this->selleritemModel->getSpecies();

         
        $data['view_file'] = 'EditSelleritem';
        view($data);
    }


    public function getById() {
        $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            
        if(empty($user_details)){
            $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
            delete_cookie('token_id'); 
            delete_cookie('role_id'); 
            echo '<script>
                alert("Session expired please login again.");
                    window.location="'.base_url().'login";
            </script>';
            exit();
        }
        $user_details=current($user_details);
        $id = base64_decode($this->input->post('id'));
        if(trim($id)==''){
            echo '<script>
                    alert("Invalid user.");
                        window.location="'.base_url().'selleritem";
                </script>';
                exit();
        }

        $selleritemDetail = current($this->selleritemModel->getDataById($id));

        echo json_encode($selleritemDetail);
    }



    public function doUpdateItem() {
        try {
            $data=$this->input->post();

            $resposne=["status"=>"0","message"=>"","field"=>''];
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));

            if(trim($this->token_id)!=''){
                $id=base64_decode($data["id"]);
                $seller_item_details=current($this->selleritemModel->getById($id));
                if(empty($seller_item_details)){
                    $resposne=["status"=>"1","message"=>"Invalid Request.","field"=>'customer_id'];
                }
                else{

                    if(!(isset($data["customer_id"]) && trim($data["customer_id"])!='')){
                        $resposne=["status"=>"1","message"=>"Please Select Customer Name.","field"=>'customer_id'];
                    }
                    else if(!(isset($data["item_id"]) && trim($data["item_id"])!='')){
                        $resposne=["status"=>"1","message"=>"Please Select Item.","field"=>'item_id'];
                    }
                    else if(!(isset($data["species_id"]) && trim($data["species_id"])!='')){
                        $resposne=["status"=>"1","message"=>"Please Select Species.","field"=>'species_id'];
                    }
                    else if(!(isset($data["item_description"]) && trim($data["item_description"])!='')){
                        $resposne=["status"=>"1","message"=>"Please Enter Item Description.","field"=>'item_description'];
                    }
                    else if(!(isset($data["special_information"]) && trim($data["special_information"])!='')){
                        $resposne=["status"=>"1","message"=>"Please Enter Special Information.","field"=>'special_information'];
                    }
                    else if(!(isset($data["packing_information"]) && trim($data["packing_information"])!='')){
                        $resposne=["status"=>"1","message"=>"Please Enter Packing Information.","field"=>'packing_information'];
                    }
                    else if(!(isset($data["shipping_information"]) && trim($data["shipping_information"])!='')){
                        $resposne=["status"=>"1","message"=>"Please Enter Packing Information.","field"=>'shipping_information'];
                    }else if(!empty($_FILES["item_image"]) && trim($_FILES["item_image"]["name"])=='' && trim($seller_item_details["item_image"])==''){
                        $resposne=["status"=>"1","message"=>"Please Select Item Image...","field"=>'item_image'];
                    }else if(!(isset($data["min_quantity"]) && trim($data["min_quantity"])!='')){
                        $resposne=["status"=>"1","message"=>"Please Enter Minimum Quantity.","field"=>'min_quantity'];
                    }else if(!(isset($data["min_price"]) && trim($data["min_price"])!='')){
                        $resposne=["status"=>"1","message"=>"Please Enter Minimum Price.","field"=>'min_price'];
                    }else if(!(isset($data["max_price"]) && trim($data["max_price"])!='')){
                        $resposne=["status"=>"1","message"=>"Please Enter Maximum Price.","field"=>'max_price'];
                    }
                    else{

                       $check_duplicate = $this->selleritemModel->doCheck(base64_decode($data["customer_id"]), base64_decode($data['item_id']), base64_decode($data['species_id']), $id);


                        if($check_duplicate == 1) {
                            $resposne=["status"=>"1","message"=>"Duplicate Item Name.","field"=>'item_id'];
                        }

                        if(!empty($_FILES["item_image"]) && trim($_FILES["item_image"]["name"])!=''){

                            $file_name=time().$_FILES["item_image"]["name"];
                             $allowedExts = array("gif", "jpeg", "jpg", "png");
                            $image_data=explode(".", $_FILES["item_image"]["name"]);
                            $extension =(!empty($image_data))?$image_data[1]:'';
                            if ((($_FILES["item_image"]["type"] == "image/gif")  || ($_FILES["item_image"]["type"] == "image/jpeg")  || ($_FILES["item_image"]["type"] == "image/jpg")  || ($_FILES["item_image"]["type"] == "image/png")) && in_array($extension, $allowedExts)){
                             $config['upload_path'] = './uploads/selleritem/';
                             $config['allowed_types'] = 'jpg|png|jpeg|gif';
                                // $config['max_size'] = '200';
                               // $config['max_width'] = '800';
                               // $config['max_height'] = '800';
                                $config['file_name'] =$file_name;
                                 $this->load->library('upload', $config);
                                if ( ! $this->upload->do_upload('item_image')){

                                    $resposne=["status"=>"1","message"=>"Please Select jjpg/jpeg/png/jpeg Image.",'error'=> $this->upload->display_errors(),"field"=>'item_image'];

                                }else{
                                    if(file_exists('./uploads/selleritem/'.$seller_item_details["item_image"])){
                                        unlink('./uploads/selleritem/'.$seller_item_details["item_image"]);
                                    }

                                } 
                            }else{

                                    $resposne=["status"=>"1","message"=>"Please Select jjpg/jpeg/png/jpeg Image.",'error'=> '',"field"=>'item_image'];
                            }

                           
                        }else{
                            $file_name=$seller_item_details["item_image"];
                        }

                         if($resposne["status"]==0){
                                $item_data = [
                                'customer_id'           =>  base64_decode($data["customer_id"]),
                                'item_id'               =>  base64_decode($data['item_id']),
                                'species_id'            =>  base64_decode($data['species_id']),
                                'item_description'      =>  $data['item_description'],
                                'special_information'   =>  $data['special_information'],
                                'packing_information'   =>  $data['packing_information'],
                                'shipping_information'  =>  $data['shipping_information'],
                                'item_image'            =>  $file_name,
                                'min_quantity'          =>  $data['min_quantity'],
                                'min_price'             =>  $data['min_price'],
                                'max_price'             =>  $data['max_price'],
                                'organic_YN'            =>  $data['organic_YN'],
                            ];

                            $res = $this->selleritemModel->updateSellerItem($item_data, $id);
                                       
                            if($res>0){
                                $resposne=["status"=>"0","message"=>" Item Updated successfully.","field"=>'item_name'];
                            }else{
                                $resposne=["status"=>"1","message"=>"Item Updated failed.Please try again some time.","field"=>'item_name'];
                            }
                        }
                        

                    }
                }
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
            }
        } catch (Exception $e) {
            $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'item_name'];

        }

        echo json_encode($resposne);
    }


    public function doStatusChange() {
        try{
            $data=$this->input->post();
            
            $id=base64_decode($data["id"]);
            
            $resposne=["status"=>"0","message"=>"","field"=>''];
            try {
                $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
                if(trim($this->token_id)!=''){
                    $res = $this->selleritemModel->updateSellerItem(["active"=>$data["status"]],$id);
                  
                    if($res){
                        $resposne=["status"=>"0","message"=>"Item status changed successfully."];
                    }
                    else{                                
                        $resposne=["status"=>"1","message"=>"Item change failed.Please try again after some time."];                                
                    }
                }else{
                    $resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
                }   
            }catch (Exception $e) {
                    $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'status'];

            }
        }
        catch (Exception $e) {
            $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'item_name'];
        }

        echo json_encode($resposne);
    }
}