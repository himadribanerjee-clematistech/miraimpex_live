<div class="min-height-200px">
    <div class="page-header">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="title">
                    
                </div>
                <nav aria-label="breadcrumb" role="navigation">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>subcategory/index/<?php echo base64_encode($cid);?>">Sub Category</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Add Sub Category of <?php echo $cate_name; ?></li>
                    </ol>
                </nav>
            </div>

        </div>
    </div>

    <!-- horizontal Basic Forms Start -->
    <div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
   


          <form id="addsubcatfrm" method="post" action="javascript:void(0);" onsubmit="addsubcatDetails();" autocomplete="off">
            <div class="form-group">
                <div class="row">
                    
                    <div class="col-4">

                        <label for="sub_cat_name">Sub Category Name <sup class="mandatory_label">&#8727;</sup></label>
                        <input type="text" name="sub_cat_name" class="form-control" id="sub_cat_name" value="" placeholder="Sub Category Name">
                    </div>
                    
                  
                    <div class="col-4">
                          <input class="btn btn-primary" type="submit" value="Submit" style="    margin-top: 7%;">
                          <input type="hidden" name="cid" id="cid" value="<?php echo base64_encode($cid);?>">
                            <input type="hidden" name="id_back" id="id_back" value="<?php echo base64_encode($cid);?>">

                    </div>
                    
                </div>

            </div>
            
        </form>
    </div>
    <!-- horizontal Basic Forms End -->

</div>

<?php
    if(isset($custom_js)){
        foreach($custom_js as $val){
            ?>
            <script src="<?php echo base_url();?>assets/js/custom_js/<?php echo $val;?>"></script>
            <?php
        }
    }
    
?>
  