
  
<!-- Simple Datatable start -->
<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="clearfix mb-20">
        <div class="pull-left">
            <h5 class="text-blue">List of <?php echo $cate_name; ?></h5>
            
        </div>
        <div class="pull-right">
            <a class="btn btn-outline-primary" href="<?php echo base_url(); ?>category">Back</a>
              <a class="btn btn-outline-primary" href="<?php echo base_url('subcategory/addNew/'. base64_encode($cid)) ; ?>">Add Sub Category</a>
        </div>
    </div>
 

    <div class="container-fluid bg-light ">
             
        <form id="catsrch" action="<?php echo base_url()?>subcategory/index/<?php echo $id;?>">
            <input type="hidden" name="per_page_search" value="<?php echo $page; ?>">
            <div class="row align-items-center justify-content-center">
                        <div class="col-md-3 pt-3">
                           <div class="form-group ">
                              <input type="text" class="form-control" name="subcatName" id="subcatName" value="<?php echo (isset($subcat_search["subcat_name"]))?$subcat_search["subcat_name"]:'';?>" placeholder="Sub Category Name">
                           </div>
                        </div>
                        
                        <div class="col-md-3 pt-3">
                            <div class="form-group">
                             <select id="active" name="status" class="form-control">
                            <option value="">Select Status</option>
                             <option value="1" <?php echo (isset($subcat_search) && isset($subcat_search["active"]) &&  $subcat_search["active"]==1)?'selected':'';?>>Active</option>
                             <option value="0"  <?php echo (isset($subcat_search) && isset($subcat_search["actve"]) &&  $subcat_search["active"]==0)?'selected':'';?>>InActive</option>
                        </select>
                            </div>
                        </div>
                       
                        <div class="col-md-3">
                         <a class="" href="<?php echo base_url(); ?>subcategory/index/<?php echo $id;?>"><button type="button" class="btn btn-outline-primary "><i class="fa fa-refresh" aria-hidden="true"></i> Reset</button></a> 

                          <input type="submit" name="search" value="Search" class="btn btn-outline-primary">
                        </div>
            </div>
            </form>

    </div>
    <div class="row">
        <table class="data-table stripe hover nowrap" id="userlst">
            <thead>
                <tr>
                 <th width="10%"  class="table-plus datatable-nosort" >Sl No.</th>
                 <th width="60%"  class="table-plus datatable-nosort">Sub Category Name</th>
                 <th width="20%"  class="table-plus datatable-nosort" >Status</th>
                 <th width="10%"  class="table-plus datatable-nosort" >Action</th>
                    
                </tr>
            </thead>
            <tbody>

                <?php
                if(!empty($subcat_data)){
                    $i=0;
                    foreach($subcat_data as $val){
                        $i++;
                        ?>
                         <tr class="">
                            <td class="table-plus" ><?php echo $i;?></td>  
                            <td  class="table-plus"><?php echo $val["category_name"];?></td>   
                            <td class="table-plus" >
                                  <select  name="status" onchange="doSubCatStatusChange(this.value,'<?php echo base64_encode($val["id"]);?>', '<?php echo base64_encode($cid);?>');" class="form-control">
                                    <option value="1" <?php echo ($val["active"]==1)?'selected':'';?>>Active</option>
                                     <option value="0"  <?php echo ($val["active"]==0)?'selected':'';?>>InActive</option>
                                </select>  

                            </td>
                             <td style="text-align:center;" class="table-plus">
                          
                                <div class="dropdown">
                                <a class="btn btn-outline-primary dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                    <i class="fa fa-ellipsis-h"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                 
                                    <a class="dropdown-item" href="<?php echo base_url()?>subcategory/dosubCatEdit/<?php echo base64_encode($val["id"]);?>/<?php echo base64_encode($cid);?>" ><i class="fa fa-pencil"></i> Edit</a>
                                    
                                </div>
                            </div>
                            </td>

                        </tr>
                        <?php
                    }
                }
             
                ?>

             
            </tbody>
        </table>
        <div class="col-sm-12">
            <div style="float: right;"><?php echo $links; ?></div>
        </div>
    </div>
</div>

    
<?php
    if(isset($custom_js)){
        foreach($custom_js as $val){
            ?>
            <script src="<?php echo base_url();?>assets/js/custom_js/<?php echo $val;?>"></script>
            <?php
        }
    }
    
?>
  