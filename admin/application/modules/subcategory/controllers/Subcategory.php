<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class Subcategory extends CI_Controller {
	protected $token_id='';
	protected $per_page=10;
	public function __construct() {
    	parent::__construct();
    	$this->token_id=get_cookie('token_id'); 
    	if(trim($this->token_id)==''){
    		redirect("login");
    	}
        $this->load->model("categoryModel");
        $this->load->model("subcategoryModel");
        $this->load->model("userModel");
        $this->load->helper("pagination");
        $this->load->library("pagination");
         $this->load->model("settingsModel", 'sM');
        $settings_data=$this->sM->getSettings();
        if(isset($settings_data->per_page)){
         $this->per_page=$settings_data->per_page;
        }
    }
    public function index($id='')
	{
		try {

            $iid = base64_decode($id);

            $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            
            if(empty($user_details)){
                    $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                    delete_cookie('token_id'); 
                     delete_cookie('useName'); 
                    echo '<script>
                        alert("Unexpected server error.Please try again some time.");
                            window.location="'.base_url().'login";
                    </script>';
            }
            $user_details=current($user_details);
            $data=[]; 
            $data["menu"]="category";
            $data["useName"]=$user_details["Name"];
            $data["id"]=$id;
            $data["custom_js"]=['subcategory.js'];
            set_cookie('useName',$user_details["Name"],'3600');
            //search 
            $search_input=$this->input->get();
            //print_r($search_input); die();

            $subcat_search=[];
            
            if(isset($search_input["subcatName"]) && trim($search_input["subcatName"])!=''){
            	$subcat_search["subcat_name"] = $search_input["subcatName"];

            }
            if(isset($search_input["status"]) && trim($search_input["status"])!=''){
                $subcat_search["active"]=$search_input["status"];
                
            }
            
            $data["subcat_search"] = $subcat_search;
            $page=0;
			if($this->input->get("per_page")){
	       		$page= $this->input->get("per_page");
	       	}

            $data["page"]=$page;

            $total_row=$this->subcategoryModel->getsubCatCount($subcat_search, $iid);
         
	        $data["links"] = createPageNationLink(base_url('subcategory/index/'.base64_encode($iid)), $total_row,$this->per_page);

	        $start= (int)$page +1;
			$end = ($page+ $this->per_page >=$total_row)? $total_row : (int)$page  + $this->per_page;
			$data['result_count']='';
			if($total_row>0){
				$data['result_count']= "Showing ".$start." - ".$end." of ".$total_row." Results";
			}
			
			$data["subcat_data"]=$this->subcategoryModel->getsubCatList($subcat_search,$this->per_page, $page, $iid);
            $data['cate_name'] = $this->subcategoryModel->getCatName($iid);
            $data['cid'] = $iid;

            //echo "<pre>"; print_r($data); die();

            $data['view_file'] = 'Subcategory';
            view($data);
           
        } catch (Exception $e) {
            $resposne=["status"=>"1","e"=>$e,"message"=>"Unexpected server error.Please try again some time.","field"=>'user_name'];
        }   
	}
	public function getSubCategoryListByCatId(){
        try {
            $id=$this->input->post("id");
             $resposne=["status"=>"0","message"=>"", "field"=>''];
              $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
              if(trim($this->token_id)!=''){
                if($id==''){
                     $resposne=["status"=>"0","result"=>[]];
                }else{
                    $subcat_data=$this->subcategoryModel->getsubCatList(["active"=>1],'', '', base64_decode($id));
                    foreach($subcat_data as $key=>$value){
                        $subcat_data[$key]["id"]=base64_encode( $subcat_data[$key]["id"]);
                        }  
                     $resposne=["status"=>"0","result"=>$subcat_data];
                  
                }

              }else{
                 $resposne=["status"=>"1","message"=>"Permission Denied."];
              }  
        } catch (Exception $e) {
            $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time."];

        }
        echo json_encode($resposne);   
    }
    public function addNew($id=''){
        try {
            $iid = base64_decode($id);

            $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            if(empty($user_details)){
                    $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                    delete_cookie('token_id'); 
                     delete_cookie('role_id'); 
                    echo '<script>
                        alert("Session expired please login again.");
                            window.location="'.base_url().'login";
                    </script>';
                    exit();
            }
            $user_details=current($user_details);
            $data=[]; 
            $data["menu"]="category";
            $data["useName"]=$user_details["Name"];
            $data["custom_js"]=['subcategory.js'];
            set_cookie('useName',$user_details["Name"],'3600');

            $data['cate_name'] = $this->subcategoryModel->getCatName($iid);
            $data['cid'] = $iid;

            $data['view_file'] = 'AddSubCategory';
            view($data);
        } catch (Exception $e) {
            echo '<script>
                alert("Unexpected server error.Please try again some time.");
                            window.location="'.base_url().'category";
            </script>';

        }
    }

    public function doAddsubCat(){
        try {
            $data=$this->input->post();
            $resposne=["status"=>"0","message"=>"", "field"=>''];
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));

            //print_r($data); die();

            if(trim($this->token_id)!=''){
                if(isset($data["sub_cat_name"]) && trim($data["sub_cat_name"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter Sub Category Name.","field"=>'sub_cat_name'];
                }
                else{
                    $check_duplicate_subcatname = $this->categoryModel->doCatCheck($data["sub_cat_name"],base64_decode($data['cid']) );

                    if($check_duplicate_subcatname == 1) {
                        $resposne=["status"=>"1","message"=>"Duplicate Sub Category Name.","field"=>'sub_cat_name'];
                    }
                    else {
                        
                        $subcat_data=['category_name'=>$data["sub_cat_name"],'parent_id'=> base64_decode($data['cid']),'image'=>'', 'is_approved' => 1, 'active'=>1];

                        //print_r($subcat_data); die();

                        $res = $this->categoryModel->insertCat($subcat_data);

                        if($res){
                            $resposne=["status"=>"0","message"=>"Sub Category added successfully."];
                        }
                        else{                                
                            $resposne=["status"=>"1","message"=>"Sub Category added failed.Please try again after some time."];                                
                        }
                    }
                }
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied."];
            }
        } catch (Exception $e) {
            $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time."];

        }

        echo json_encode($resposne);
    }

    public function dosubCatEdit($id, $iid){
        $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            
        if(empty($user_details)){
                $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                delete_cookie('token_id'); 
                 delete_cookie('role_id'); 
                echo '<script>
                    alert("Session expired please login again.");
                        window.location="'.base_url().'login";
                </script>';
                exit();
        }
        $user_details=current($user_details);
        $id=base64_decode($id);
        $iid=base64_decode($iid);
        if(trim($id)==''){
            echo '<script>
                    alert("Invalid user.");
                        window.location="'.base_url().'user";
                </script>';
                exit();
        }
        $data=[]; 
        $data["menu"]="category";
        $data["useName"]=$user_details["Name"];
        $data["custom_js"]=['subcategory.js'];
        set_cookie('useName',$user_details["Name"],'3600');
        $data['subcat_detail'] = current($this->categoryModel->getById($id));

        $data['cate_name'] = $this->subcategoryModel->getCatName($iid);
        $data['cid'] = $iid;

        $data['id'] = $id;
         
        $data['view_file'] = 'EditSubCategory';
        view($data);
            
    }
    public function UpdatesubCatDetails(){
        try {
            $data=$this->input->post();
            $resposne=["status"=>"0","message"=>"", "field"=>''];
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));

            if(trim($this->token_id)!=''){
                $id = base64_decode($data['id']);
                if(isset($data["sub_cat_name"]) && trim($data["sub_cat_name"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter Sub Category Name.","field"=>'sub_cat_name'];
                }
                else{
                    $check_duplicate_subcatname = $this->categoryModel->doCatCheck($data["sub_cat_name"],base64_decode($data['cid']), base64_decode($data["id"]));

                    if($check_duplicate_subcatname == 1) {
                        $resposne=["status"=>"1","message"=>"Duplicate Sub Category Name.","field"=>'sub_cat_name'];
                    }
                    else {
                        
                        $subcat_data=['category_name'=>$data["sub_cat_name"],'parent_id'=> base64_decode($data['cid']), 'is_approved' => 1, 'image'=>'', 'active'=>1];

                        $res = $this->categoryModel->updateCat($subcat_data, $id); 

                        if($res){
                            $resposne=["status"=>"0","message"=>"Sub Category Updated successfully."];
                        }
                        else{                                
                            $resposne=["status"=>"1","message"=>"Sub Category Updation failed.Please try again after some time."];                                
                        }
                    }
                }
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied."];
            }
        } catch (Exception $e) {
            $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time."];

        }

        echo json_encode($resposne);
    }
    
    public function doSubCatStatusChange(){
        $data=$this->input->post();
        $id=base64_decode($data["id"]);
        $resposne=["status"=>"0","message"=>"","field"=>''];
        try {
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
            if(trim($this->token_id)!=''){
                $res = $this->categoryModel->updateCatStat(["active"=>$data["actve"]], $id);
         
                if($res){
                    $resposne=["status"=>"0","message"=>"Sub Category status change successfully."];
                }
                else{                                
                    $resposne=["status"=>"1","message"=>"Sub Category  status change failed.Please try again after some time."];                                
                }
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
            }   
        }catch (Exception $e) {
                $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'role_id'];

        }

        echo json_encode($resposne);    
    }
    public function doSubCatDelete(){
        $data=$this->input->post();
        $data["id"]=base64_decode($data["id"]);
        $resposne=["status"=>"0","message"=>"","field"=>''];
        try {
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
            if(trim($this->token_id)!=''){
                $res = $this->categoryModel->doDeleteCat($data["id"]);
                        
                if($res){
                    $resposne=["status"=>"0","message"=>"Sub Category Deleted successfully."];
                }
                else{                                
                    $resposne=["status"=>"1","message"=>"Sub Category Deletion failed.Please try again after some time."];                                
                }
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
            }   
        }catch (Exception $e) {
                $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'role_id'];

        }

        echo json_encode($resposne);    
    }   

}
?>