<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="clearfix mb-10">
        <div class="pull-left">
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Service Category</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Add New Service Category</li>
                </ol>
            </nav>
        </div> 
        <div class="pull-right">
            <a class="btn btn-outline-primary" href="<?php echo base_url()?>servicecategory">Back</a>            
        </div>
    </div> 

    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">

                <form id="addcatfrm" method="post" action="javascript:void(0);" autocomplete="off" enctype="multipart/form-data" onsubmit="return addServiceCatagory();">

                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-3">
                                <label label-for='name'>Service Category Name <span class="mandatory_label">*</span></label>
                                <input type="text" name="service_category_name" id="service_category_name" value="" placeholder="Service Category Name" class="form-control">
                            </div>
                            <div class="col-4">
                                <label for="cat_name"> Unit of Measurement<sup class="mandatory_label">&#8727;</sup></label>
                                <select name="uom_id[]" class="form-control" id="uom_id" multiple="multiple">
                                    <option></option>
                                </select>
                            </div>
                            <div class="col-lg-5">
                                <label label-for='name'>Image <span class="mandatory_label">*</span></label>
                                <br>
                                <input type="file" name="image" id="image" onchange="loadFile()">

                                <br>

                                <img src="<?php echo base_url('assets/images/no-item-image.png'); ?>" height="150" width="150"  style="border: 2px dotted #ccc; border-radius: 2px;" id="chImg"/>

                                <script type='text/javascript'>
                                    function loadFile(){
                                        
                                        var imgfilename = document.getElementById('chImg');

                                        imgfilename.src = URL.createObjectURL(event.target.files[0]);
                                    };
                                </script>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">                            
                            <div class="col-lg-12 text-right">
                                <input type="submit" value="Add" name="submit" class="btn btn-outline-primary">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="clone">
    <!-- Value for UOM -->
    <option id="uom_cln_id" value=""></option>

</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>

<script type="text/javascript">
    $("#uom_id").select2();
</script>


<script type="text/javascript">
    
    $(document).ready(function(){
        var base_url = $('#base_url').val();
        
        $.ajax({
            url: base_url + 'servicecategory/getUOM',
            type: 'GET',
            dataType: 'JSON',
            success: function(response) {
                //console.log(response);

                if (response.status == 0) {
                    var uom = response.data.uom;

                    $("#uom_id").empty();

                    $.each(uom, function( index, value ) {
                        let uom_clone = $("#uom_cln_id").clone();
                        let uom_clone_child = uom_clone.attr("id","");
                        
                        uom_clone_child.val(value.id).end();
                        uom_clone_child.text(value.name).end();
                                
                        $("#uom_id").append(uom_clone_child);

                    });
                
                    
                }
                else {
                    console.log(response);
                }
                
            }
        });

    });

</script>


<?php
    if(isset($custom_js)){
        foreach($custom_js as $val){
            ?>
            <script src="<?php echo base_url();?>assets/js/custom_js/<?php echo $val;?>"></script>
            <?php
        }
    }
    
?>
  