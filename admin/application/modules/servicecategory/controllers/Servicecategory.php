<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class Servicecategory extends CI_Controller {
	protected $token_id='';
	protected $per_page=10;
	public function __construct() {
    	parent::__construct();
    	$this->token_id=get_cookie('token_id'); 
    	if(trim($this->token_id)==''){
    		redirect("login");
    	}
        $this->load->model("servicecategoryModel");
        $this->load->model("userModel");
        $this->load->model("serviceuomModel");
        $this->load->helper("pagination");
        $this->load->library("pagination");
          $this->load->model("settingsModel", 'sM');
        $settings_data=$this->sM->getSettings();
        if(isset($settings_data->per_page)){
         $this->per_page=$settings_data->per_page;
        }
    }
    public function index()
	{
		try {
           $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            
            if(empty($user_details)){
                    $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                    delete_cookie('token_id'); 
                     delete_cookie('useName'); 
                    echo '<script>
                        alert("Unexpected server error.Please try again some time.");
                            window.location="'.base_url().'login";
                    </script>';
            }
            $user_details=current($user_details);
            $data=[]; 
            $data["menu"]="servicecategory";
            $data["useName"]=$user_details["Name"];
            $data["custom_js"]=['servicecategory.js'];
            set_cookie('useName',$user_details["Name"],'3600');
            //search 
            $search_input=$this->input->get();
            
            $servicecat_search=[];
            
            if(isset($search_input["servicecatName"]) && trim($search_input["servicecatName"])!=''){
            	$servicecat_search["service_category_name"] = $search_input["servicecatName"];

            }
            if(isset($search_input["status"]) && trim($search_input["status"])!=''){
                $servicecat_search["active"]=$search_input["status"];
                
            }
            
            $data["servicecat_search"] = $servicecat_search;
            $page=0;
			if($this->input->get("per_page")){
	       		$page= $this->input->get("per_page");
	       	}

            $data["page"]=$page;

            $total_row=$this->servicecategoryModel->getServiceCatCount($servicecat_search);
         
	        $data["links"] = createPageNationLink(base_url('servicecategory/index'), $total_row,$this->per_page);

	        $start= (int)$page +1;
			$end = ($page+ $this->per_page >=$total_row)? $total_row : (int)$page  + $this->per_page;
			$data['result_count']='';
			if($total_row>0){
				$data['result_count']= "Showing ".$start." - ".$end." of ".$total_row." Results";
			}
			
			$data["service_cat_data"]=$this->servicecategoryModel->getServiceCatList($servicecat_search,$this->per_page, $page);

            //echo '<pre>'; print_r($data); die();

            $data['view_file'] = 'ServiceCategory';
            view($data);
           
        } catch (Exception $e) {
            $resposne=["status"=>"1","e"=>$e,"message"=>"Unexpected server error.Please try again some time.","field"=>'user_name'];
        }   
	}
	
    public function addServiceCat(){
        try {
            $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            if(empty($user_details)){
                    $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                    delete_cookie('token_id'); 
                     delete_cookie('role_id'); 
                    echo '<script>
                        alert("Session expired please login again.");
                            window.location="'.base_url().'login";
                    </script>';
                    exit();
            }
            $user_details=current($user_details);
            $data=[]; 
            $data["menu"]="servicecategory";
            $data["useName"]=$user_details["Name"];
            $data["custom_js"]=['servicecategory.js'];
            set_cookie('useName',$user_details["Name"],'3600');
             $data['view_file'] = 'AddServiceCategory';
            view($data);
        } catch (Exception $e) {
            echo '<script>
                alert("Unexpected server error.Please try again some time.");
                            window.location="'.base_url().'servicecategory";
            </script>';

        }
    }

    public function getUOM() {

        try{
            $resposne=["status"=>"0","message"=>"", 'data' => '', "selected" => ""];

            $uom = $this->serviceuomModel->getuomList();
            foreach ($uom as $val) {
                $val['id'] = base64_encode($val['id']);
                $result['uom'][] = $val;
            }

            $resposne=["status" => 0, "data" => $result];

        }
        catch(Exception $e) {
            $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.", 'data' => ''];
        }

        echo json_encode($resposne);
    }
    
    public function doAddServiceCat(){

        try {
            $data=$this->input->post();
            $resposne=["status"=>"0","message"=>"","field"=>''];
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));

            if(trim($this->token_id)!=''){
                if(isset($data["service_category_name"]) && trim($data["service_category_name"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter Service Category Name.","field"=>'service_category_name'];
                }else if(!empty($_FILES["image"]) && trim($_FILES["image"]["name"])==''){
                    $resposne=["status"=>"1","message"=>"Please Select Service Category Image.","field"=>'image'];
                }else{

                   $check_duplicate_service_catname = $this->servicecategoryModel->doServiceCatCheck($data["service_category_name"]);

                    if(!empty($check_duplicate_service_catname)){
                        $resposne=["status"=>"1","message"=>"Duplicate Service Category Name.","field"=>'service_category_name'];
                    }
                    if($resposne["status"]==0){
                        $file_name=time().$_FILES["image"]["name"];

                        $allowedExts = array("gif", "jpeg", "jpg", "png");
                        $image_data=explode(".", $_FILES["image"]["name"]);
                        $extension =(!empty($image_data))?$image_data[1]:'';
                         
                        if ((($_FILES["image"]["type"] == "image/gif")  || ($_FILES["image"]["type"] == "image/jpeg")  || ($_FILES["image"]["type"] == "image/jpg")  || ($_FILES["image"]["type"] == "image/png")) && in_array($extension, $allowedExts))
                        {
                            $config['upload_path'] = './uploads/servicecategory/';
                            $config['allowed_types'] = 'jpg|png|jpeg|gif';
                            // $config['max_size'] = '200';
                            //$config['max_width'] = '800';
                            //$config['max_height'] = '800';
                            $config['file_name'] =$file_name;
                            
                             $this->load->library('upload', $config);
                             if ( ! $this->upload->do_upload('image')){
                                $resposne=["status"=>"1","message"=>"Please Select  jpg/jpeg/png/jpeg  Service Category Image.","field"=>'image'];
                              
                            }else{
                                $string = $data['service_category_name'];
                                $searchStringComma = ',';
                                $searchStringHyphen = '-';
                                $new_string = '';

                                if( strpos($string, $searchStringComma) !== false ) {
                                    $new_string = str_replace(","," ", $string);
                                }

                                if ($new_string != '') { //If New String is not Blank
                                  if( strpos($new_string, $searchStringHyphen) !== false ) {
                                      $new_string = str_replace("-"," ", $new_string);
                                      $new_string = preg_replace('!\s+!', ' ', $new_string);
                                      
                                      $slug = strtolower(str_replace(" ","-", $new_string));
                                  }
                                  else {
                                    $new_string = preg_replace('!\s+!', ' ', $new_string);
                                    $slug = strtolower(str_replace(" ","-", $new_string));
                                  }
                                }
                                else { //If New String is Blank
                                  if( strpos($string, $searchStringHyphen) !== false ) {
                                      $new_string = str_replace("-"," ", $string);
                                      $new_string = preg_replace('!\s+!', ' ', $new_string);
                                      
                                      $slug = strtolower(str_replace(" ","-", $new_string));
                                  }
                                  else {
                                    $new_string = preg_replace('!\s+!', ' ', $string);
                                    $slug = strtolower(str_replace(" ","-", $new_string));
                                  }
                                }

                                $cat_data = [
                                    'service_category_name' => $data['service_category_name'], 
                                    'image' => $file_name,
                                    'parent_id' => '0',
                                    'slug' => $slug,
                                    'uom_id' => $data['uom_id'],
                                ];
                                
                                $id = $this->servicecategoryModel->insertServiceCat($cat_data);
                                
                                if($id>0){
                                    $resposne=["status"=>"0","message"=>" Service Category added successfully.","field"=>'user_name'];
                                }else{
                                    $resposne=["status"=>"1","message"=>"Service Category added failed.Please try again some time.","field"=>'service_category_name'];
                                }
   
                            }
                        }
                        else {
                            $resposne=["status"=>"1","message"=>"Please Select jpg/jpeg/png/jpeg Service Category Image.",'error'=> '',"field"=>'image'];
                        }
                        
                    }

                }
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
            }
        } catch (Exception $e) {
                $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'user_name'];

        }

        echo json_encode($resposne);
    }
   

    public function doServiceCatEdit($id){
        $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            
        if(empty($user_details)){
                $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                delete_cookie('token_id'); 
                 delete_cookie('role_id'); 
                echo '<script>
                    alert("Session expired please login again.");
                        window.location="'.base_url().'login";
                </script>';
                exit();
        }
        $user_details=current($user_details);
        $id=base64_decode($id);
        if(trim($id)==''){
            echo '<script>
                    alert("Invalid user.");
                        window.location="'.base_url().'user";
                </script>';
                exit();
        }
        $data=[]; 
        $data["menu"]="servicecategory";
        $data["useName"]=$user_details["Name"];
        $data["custom_js"]=['servicecategory.js'];
        set_cookie('useName',$user_details["Name"],'3600');
        
        $service_cat_detail_get = $this->servicecategoryModel->getById($id);
        //print_r($service_cat_detail_get); die();

        $data['uom'] = $this->serviceuomModel->getuomList();

        //print_r($data['uom']); die();

        $data['service_cat_detail'] = '';
        
        foreach ($service_cat_detail_get as $val) {
            $data['service_cat_detail'] = $val;
        }
         
        $data['view_file'] = 'EditServiceCategory';
        view($data);
            
    }
    
    public function UpdateServiceCatagory(){
        $data=$this->input->post();
        $resposne=["status"=>"0","message"=>"","field"=>''];

        //print_r($_FILES); die();
        //print_r($data); die();
        
        try {
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
            if(trim($this->token_id)!=''){
                    if(isset($data["id"]) && trim($data["id"])==''){
                         $resposne=["status"=>"1","message"=>"Invalid Request1.","field"=>'service_category_name'];
                    }else{
                        $id=base64_decode($data["id"]);
                        $servicecategory_details = current($this->servicecategoryModel->getById($id));

                        //print_r($servicecategory_details); die();

                        if(empty($servicecategory_details)){
                            $resposne=["status"=>"1","message"=>"No Data Found.","field"=>'service_category_name'];
                         }else{

                            if(isset($data["service_category_name"]) && trim($data["service_category_name"])==''){
                                $resposne=["status"=>"1","message"=>"Please Enter Service Category Name.","field"=>'service_category_name'];
                            }else if(!empty($_FILES["image"]) && trim($_FILES["image"]["name"])=='' && trim($servicecategory_details["image"])==''){
                                $resposne=["status"=>"1","message"=>"Please Select Service Category Image.","field"=>'image'];
                            }else{

                                $check_duplicate_service_catname = $this->servicecategoryModel->doServiceCatCheck($data["service_category_name"],$id);

                                if(!empty($check_duplicate_service_catname)){
                                    $resposne=["status"=>"1","message"=>"Duplicate servicecategory Name.","field"=>'service_category_name'];
                                }
                                if($resposne["status"]==0){

                                    if(!empty($_FILES["image"]) && trim($_FILES["image"]["name"])!=''){

                                        $file_name=time().$_FILES["image"]["name"];

                                        $allowedExts = array("gif", "jpeg", "jpg", "png");
                                        $image_data=explode(".", $_FILES["image"]["name"]);
                                        $extension =(!empty($image_data))?$image_data[1]:'';

                                        if ((($_FILES["image"]["type"] == "image/gif")  || ($_FILES["image"]["type"] == "image/jpeg")  || ($_FILES["image"]["type"] == "image/jpg")  || ($_FILES["image"]["type"] == "image/png")) && in_array($extension, $allowedExts)) {
                                            $config['upload_path'] = './uploads/servicecategory/';
                                            $config['allowed_types'] = 'jpg|png|jpeg|gif';
                                            // $config['max_size'] = '200';
                                            // $config['max_width'] = '800';
                                            // $config['max_height'] = '800';
                                            $config['file_name'] =$file_name;

                                            $this->load->library('upload', $config);
                                            if ( ! $this->upload->do_upload('image')){

                                                $resposne=["status"=>"1","message"=>"Please Select jpg/png/jpeg/gif Service Category Image.","field"=>'image'];

                                            }else{
                                                if (trim($servicecategory_details["image"]) != '') {
                                                    if(file_exists('./uploads/servicecategory/'.$servicecategory_details["image"])){
                                                        unlink('./uploads/servicecategory/'.$servicecategory_details["image"]);
                                                    }
                                                }
                                                
                                            } 
                                        }   
                                        else {
                                            $resposne=["status"=>"1","message"=>"Please Select jpg/jpeg/png/jpeg Service Category mage.",'error'=> '',"field"=>'image'];
                                        }

                                    }else{
                                        $file_name=$servicecategory_details["image"];
                                    }
                                
                                    if($resposne["status"]==0){
                                        $string = $data['service_category_name'];
                                        $searchStringComma = ',';
                                        $searchStringHyphen = '-';
                                        $new_string = '';

                                        if( strpos($string, $searchStringComma) !== false ) {
                                            $new_string = str_replace(","," ", $string);
                                        }

                                        if ($new_string != '') { //If New String is not Blank
                                          if( strpos($new_string, $searchStringHyphen) !== false ) {
                                              $new_string = str_replace("-"," ", $new_string);
                                              $new_string = preg_replace('!\s+!', ' ', $new_string);
                                              
                                              $slug = strtolower(str_replace(" ","-", $new_string));
                                          }
                                          else {
                                            $new_string = preg_replace('!\s+!', ' ', $new_string);
                                            $slug = strtolower(str_replace(" ","-", $new_string));
                                          }
                                        }
                                        else { //If New String is Blank
                                          if( strpos($string, $searchStringHyphen) !== false ) {
                                              $new_string = str_replace("-"," ", $string);
                                              $new_string = preg_replace('!\s+!', ' ', $new_string);
                                              
                                              $slug = strtolower(str_replace(" ","-", $new_string));
                                          }
                                          else {
                                            $new_string = preg_replace('!\s+!', ' ', $string);
                                            $slug = strtolower(str_replace(" ","-", $new_string));
                                          }
                                        }

                                        $cat_data = [
                                            'service_category_name' => $data['service_category_name'], 
                                            'image' => $file_name,
                                            'slug' => $slug,
                                            'uom_id' => $data['uom_id'],
                                        ];
                                        
                                        $id=$this->servicecategoryModel->updateServiceCat($cat_data,$id);
                                        if($id>0){
                                            $resposne=["status"=>"0","message"=>"Service Category updated successfully.","field"=>'service_category_name'];
                                        }else{

                                            $resposne=["status"=>"1","message"=>"Service Category updated failed.Please try again some time.","field"=>'service_category_name'];
                                        }
                                    }
                                }
                            }
                        }

                    }


               
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
            }
        }catch (Exception $e) {
                $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'user_name','error'=>$e];

        }

        echo json_encode($resposne);
    }

    public function doServiceStatusChange(){
        $data=$this->input->post();
        $id=base64_decode($data["id"]);
        $resposne=["status"=>"0","message"=>"","field"=>''];
        try {
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
            if(trim($this->token_id)!=''){
              $res=$this->servicecategoryModel->updateServiceCat(["active"=>$data["active"]],$id);
                        
                if($res){
                    $resposne=["status"=>"0","message"=>"Service Category status change successfully."];
                }
                else{                                
                    $resposne=["status"=>"1","message"=>"Service Category change failed.Please try again after some time."];                                
                }
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
            }   
        }catch (Exception $e) {
                $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'status'];

        }

        echo json_encode($resposne);
    }
    
    /*
    public function doCatDelete(){
        $data=$this->input->post();
        $data["id"]=base64_decode($data["id"]);
        $resposne=["status"=>"0","message"=>"","field"=>''];
        try {
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
            if(trim($this->token_id)!=''){
                if(isset($data["id"]) && trim($data["id"])==''){
                     $resposne=["status"=>"1","message"=>"Invalid Request.","field"=>'service_category_name'];
                }else{
                    $servicecategory_details=current($this->servicecategoryModel->getById($data["id"]));
                    if(empty($servicecategory_details)){
                         $resposne=["status"=>"1","message"=>"Invalid Request.","field"=>'service_category_name'];
                     }else{
                        $res = $this->servicecategoryModel->doDeleteCat($data["id"]);
                        
                        if($res){
                            if(file_exists('./uploads/servicecategory/'.$servicecategory_details["image"])){
                                unlink('./uploads/servicecategory/'.$servicecategory_details["image"]);
                            }
                            $resposne=["status"=>"0","message"=>"servicecategory Deleted successfully."];
                        }
                        else{                                
                            $resposne=["status"=>"1","message"=>"servicecategory Deletion failed.Please try again after some time."];                                
                        }

                     }
                }
                
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
            }   
        }catch (Exception $e) {
                $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'role_id'];

        }

        echo json_encode($resposne);    
    }   
    */

}
?>