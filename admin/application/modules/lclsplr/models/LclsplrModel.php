<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class LclsplrModel extends CI_Model {
	protected $lclsplr = 'local_supplier';
	protected $customer = 'customer';
	
	public function __construct() {
        parent::__construct();
    }
     
    
    public function getLSCount($data){
    	try {
    		$this->db->db_debug = false;

    		// SELECT ls.supplier_name, ls.description, ls.status, c.name FROM t_local_supplier ls, t_customer c WHERE c.id = ls.customer_id


    		$this->db->select("ls.id, ls.supplier_name, ls.description, ls.status, c.name");
    		$this->db->from($this->lclsplr." ls");
    		$this->db->join($this->customer . ' c' , 'c.id = ls.customer_id');
    		
    		if(isset($data["cust_id"]) && $data["cust_id"]!=''){
	    		$this->db->where("ls.customer_id",$data["cust_id"]);
	    	}

	    	if(isset($data["supp_id"]) && $data["supp_id"]!=''){
	    		$this->db->where("ls.id",$data["supp_id"]);
	    	}

	    	if(isset($data["active"]) && $data["active"]!=''){
	    		$this->db->where("ls.status",$data["active"]);
	    	}

	    	$this->db->order_by('ls.created_on', 'DESC');

	    	$db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query();die();
	        	return $this->db->count_all_results();

	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
    }

    public function getLSList($data, $limit='', $start=0){
    	try {
    		$this->db->db_debug = false;

    		$this->db->select("ls.id, ls.supplier_name, ls.description, ls.status, c.name");
    		$this->db->from($this->lclsplr." ls");
    		$this->db->join($this->customer . ' c' , 'c.id = ls.customer_id');
    		
    		if(isset($data["cust_id"]) && $data["cust_id"]!=''){
	    		$this->db->where("ls.customer_id",$data["cust_id"]);
	    	}

	    	if(isset($data["supp_id"]) && $data["supp_id"]!=''){
	    		$this->db->where("ls.id",$data["supp_id"]);
	    	}

	    	if(isset($data["active"]) && $data["active"]!=''){
	    		$this->db->where("ls.status",$data["active"]);
	    	}	    	
 
 			$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query();die();
	        	$result=[];
	        	foreach($query->result_array() as $row){
	        		$result[]=$row;
	        	}
	        	return $result;
	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
    }	

    
    public function getDetailById($id) {
    	try {
		 	$this->db->db_debug = false;

		 	// SELECT ls.supplier_name, ls.description, ls.status, c.name FROM t_local_supplier ls, t_customer c WHERE c.id = ls.customer_id


		 	$this->db->select("ls.id, ls.supplier_name, ls.description, ls.status, c.name, DATE_FORMAT(ls.created_on, '%M %d %Y') AS created");
    		$this->db->from($this->lclsplr." ls");
    		$this->db->join($this->customer . ' c' , 'c.id = ls.customer_id');

    		$this->db->where('ls.id', $id);

    		$query=$this->db->get();

    		//echo $this->db->last_query();die();

    		$db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	$data=[];
	        	foreach($query->result_array() as $row){
					$data[] = $row;
	        	}
	        	return array_values($data);
	        }
	        return TRUE;
	    } 
	    catch (Exception $e) {
	       throw new Exception();
	    }	

    }

    public function updateLclSupp($data, $id) {
    	$res = $this->db->update($this->lclsplr, $data, ['id' => $id]);

    	return ($res)?true:false;
    }
    
}

?>