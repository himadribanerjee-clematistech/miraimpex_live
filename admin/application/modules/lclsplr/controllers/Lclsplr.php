<?php

defined('BASEPATH') OR exit('No direct script access allowed');
class Lclsplr extends CI_Controller {
    protected $token_id='';
    protected $per_page=10;
    public function __construct() {
        parent::__construct();
        $this->token_id=get_cookie('token_id'); 
        if(trim($this->token_id)==''){
            redirect("login");
        }

        $this->load->model("userModel");
        $this->load->model("customerModel");
        
        $this->load->model("lclsplrModel", 'lM');       
        
        $this->load->helper("pagination");
        $this->load->library("pagination");
        $this->load->model("settingsModel", 'sM');
        $settings_data=$this->sM->getSettings();
        if(isset($settings_data->per_page)){
         $this->per_page=$settings_data->per_page;
        }
    }
    public function index() {
        try {
           $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            
            if(empty($user_details)){
                    $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                    delete_cookie('token_id'); 
                     delete_cookie('useName'); 
                    echo '<script>
                        alert("Unexpected server error.Please try again some time.");
                            window.location="'.base_url().'login";
                    </script>';
            }
            $user_details=current($user_details);
            $data=[]; 
            $data["menu"]="lclsplr";
            $data["useName"]=$user_details["Name"];
            $data["custom_js"]=['lclsplr.js'];
            set_cookie('useName',$user_details["Name"],'3600');
            
            //search 
            $search_input=$this->input->get();
            
            $lclsplr=[];
            
            if(isset($search_input["cust_id"]) && trim($search_input["cust_id"])!=''){
                $lclsplr["cust_id"]=$search_input["cust_id"];
                
            }

            if(isset($search_input["supp_id"]) && trim($search_input["supp_id"])!=''){
                $lclsplr["supp_id"]=$search_input["supp_id"];
                
            }

            if(isset($search_input["status"]) && trim($search_input["status"])!=''){
                $lclsplr["active"]=$search_input["status"];
                
            }

            $data["lclsplr_search"] = $lclsplr;
            $page=0;
            if($this->input->get("per_page")){
                $page= $this->input->get("per_page");
            }

            $data["page"]=$page;

            $total_row=$this->lM->getLSCount($lclsplr);
         
            $data["links"] = createPageNationLink(base_url('lclsplr/index'), $total_row, $this->per_page);

            $start= (int)$page +1;
            $end = ($page+ $this->per_page >=$total_row)? $total_row : (int)$page  + $this->per_page;
            $data['result_count']='';
            if($total_row>0){
                $data['result_count']= "Showing ".$start." - ".$end." of ".$total_row." Results";
            }
            
            $data["lclsplr_data"] = $this->lM->getLSList($lclsplr,$this->per_page, $page); 
            $data["customer"] = $this->customerModel->getCustList('','','');

            // print_r($data); die();
            $data['view_file'] = 'Lclsplr';
            view($data);
           
        } catch (Exception $e) {
            $resposne=["status"=>"1","e"=>$e,"message"=>"Unexpected server error.Please try again some time.","field"=>'user_name'];
        }   
    }
    

    public function Details() {
        try{
            $id = base64_decode($this->input->post('id'));

            $resposne=["status"=>"0","message"=>"","field"=>'', 'data'=> ''];

            $user_details = current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
            if(trim($this->token_id)!=''){
                $res = current($this->lM->getDetailById($id));

                $resposne=["status"=>"0","message"=>"Success.","field"=>'', 'data'=> $res];
            }
            else {
                $resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
            }

        }
        catch(Exception $e) {
            $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'role_id'];
        }

        echo json_encode($resposne);
    }
    

    public function doStatusChange(){
        $data=$this->input->post();
        $id=base64_decode($data["id"]);
        $resposne=["status"=>"0","message"=>"","field"=>''];
        try {
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
            if(trim($this->token_id)!=''){

                $res = $this->lM->updateLclSupp(["status"=>$data["active"]], $id);
         
                if($res){
                    $resposne=["status"=>"0","message"=>"Local Supplier status change successfully."];
                }
                else{                                
                    $resposne=["status"=>"1","message"=>"Local Supplier status change failed.Please try again after some time."];                                
                }
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
            }   
        }catch (Exception $e) {
                $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'role_id'];

        }

        echo json_encode($resposne);  
    }


}
?>