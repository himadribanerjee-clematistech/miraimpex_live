<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="clearfix mb-10">
        <div class="pull-left">
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<?php echo base_url('lclsplr')?>">Local Supplier</a></li>
                    <li class="breadcrumb-item active" aria-current="page">List</li>
                </ol>
            </nav>
        </div>
    </div>

    <div class="container-fluid bg-light"> 
        <div class="row">
            <div class="col-lg-12">
  
                <form id="quotesrch" action="<?php echo base_url('lclsplr') ?>">

                    <input type="hidden" name="per_page_search" value="<?php echo $page; ?>">

                    <div class="row align-items-center justify-content-center">
                        <div class="col-md-3 pt-3">
                            <div class="form-group ">
                                <select id="cust_id" name="cust_id" class="form-control">
                                    <option value="">-- Select Customer --</option>

                                <?php foreach ($customer as $c) : ?>

                                    <option value="<?php echo $c['id']; ?>" <?php echo (isset($lclsplr_search) && isset($lclsplr_search["cust_id"]) &&  $lclsplr_search["cust_id"]== $c['id']) ? 'selected' : ''; ?> > <?php echo $c['name']; ?> </option>

                                <?php endforeach; ?>

                                </select>  
                            </div>
                        </div>

                        <div class="col-md-3 pt-3">
                            <div class="form-group ">
                                <select id="supp_id" name="supp_id" class="form-control">
                                    <option value="">-- Select Supplier --</option>

                                <?php foreach ($lclsplr_data as $s) : ?>

                                    <option value="<?php echo $s['id']; ?>" <?php echo (isset($lclsplr_search) && isset($lclsplr_search["supp_id"]) &&  $lclsplr_search["supp_id"]== $s['id']) ? 'selected' : ''; ?> > <?php echo $s['supplier_name']; ?> </option>

                                <?php endforeach; ?>

                                </select>  
                            </div>
                        </div>

                        <div class="col-md-3 pt-3">
                           <div class="form-group">
                                <select id="status" name="status" class="form-control">
                                    <option value="">-- Select Status --</option>
                                    <option value="1" <?php echo (isset($lclsplr_search) && isset($lclsplr_search["active"]) &&  $lclsplr_search["active"]==1)?'selected':'';?>>Active</option>
                                    <option value="0"  <?php echo (isset($lclsplr_search) && isset($lclsplr_search["active"]) &&  $lclsplr_search["active"]==0)?'selected':'';?>>InActive</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="col-md-3 pt-3">
                            <div class="form-group">
                                <a class="" href="<?php echo base_url('lclsplr')?>"><button type="button" class="btn btn-outline-primary "><i class="fa fa-refresh" aria-hidden="true"></i> Reset</button></a> 

                                <input type="submit" name="search" value="Search" class="btn btn-outline-primary">                                
                            </div>                       
                        </div>
                    </div>
                
                </form>

            </div>
        </div>
    </div>

    <div class="row">
        <table class="data-table stripe hover nowrap" id="userlst">
            <thead>
                <tr>
                    <th width="2%"  class="table-plus datatable-nosort" >#</th>
                    <th width="20%"  class="table-plus datatable-nosort">Customer</th>
                    <th width="15%"  class="table-plus datatable-nosort">Supplier</th>
                    <th width="15%"  class="table-plus datatable-nosort text-center" >Status</th>
                    <th width="10%"  class="table-plus datatable-nosort text-center" >Action</th>                   
                </tr>
            </thead>
            <tbody>

            <?php

                if(!empty($lclsplr_data)){
                    $i=0;
                    foreach($lclsplr_data as $val){
                        $i++;
            ?>

                        <tr class="">
                            <td class="table-plus" ><?php echo $i;?></td>  
                            <td class="table-plus" >
                                <?php echo $val["name"];?>      
                            </td>
                            <td class="table-plus" >
                                <?php echo $val["supplier_name"];?>                                              
                            </td>                 
                            <td class="table-plus text-center">
                                <?php echo ($val["status"]==1)?'<span style="color: green">Active</span>':'<span style="color: red">InActive</span>';?>
                            </td>
                            <td class="table-plus text-center">
                                <a class="btn btn-outline-primary response" href="#" role="button" data-toggle="modal" data-id="<?php echo base64_encode($val['id']) ?>">
                                    <i class="fa fa-eye"></i> 
                                </a>
                            </td>

                        </tr>

            <?php
                    }
                }
              
            ?>

             
            </tbody>
        </table>
        <div class="col-sm-12">
            <div style="float: right;"><?php echo $links; ?></div>
        </div>
    </div>
</div>



<!-- Modal -->

<div class="modal fade admin_modal" id="viewModal">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h4 class="modal-title"> Supplier Details </h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="row" style="padding-bottom: 20px;">
                    <div class="col-lg-8">
                        <label style="font-weight: 400;">Name : </label> <span id="supplier"> Supplier Name </span>
                    </div>
                    <div class="col-lg-4">
                        <label style="font-weight: 400;">Supplier Id : </label> <span id="supplier_id"> Supplier ID </span>
                    </div>
                </div>

                <div class="row" style="padding-bottom: 20px;">
                    <div class="col-lg-6" id="expiry_date_col">
                        <label style="font-weight: 400;">Created By : </label> <span id="customer"> Customer Name </span>
                    </div>
                    <div class="col-lg-6" id="expiry_date_col">
                        <label style="font-weight: 400;">Created On : </label> <span id="created"> Created On </span>
                    </div>
                </div>

                <div class="row detail_row">
                    <div class="col-lg-12"> 
                        <label style="font-weight: 400;">Details : </label>
                        <br>
                        <div id="details">Details</div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>


<!-- Modal Ends -->

    
<?php
    if(isset($custom_js)){
        foreach($custom_js as $val){
            ?>
            <script src="<?php echo base_url();?>assets/js/custom_js/<?php echo $val;?>"></script>
            <?php
        }
    }
    
?>