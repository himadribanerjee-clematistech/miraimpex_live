<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="clearfix mb-10">
        <div class="pull-left">
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Customer Profile</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Update Following Customer</li>
                </ol>
            </nav>
        </div> 
        <div class="pull-right">
            <a class="btn btn-outline-primary" href="<?php echo base_url()?>customer">Back</a>            
        </div> 
    </div>

    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                 <form id="editCustfrm" method="post" action="javascript:void(0);" onsubmit="updateCustomer();" autocomplete="off">
                    <input type="hidden" name="id" id="id" value="<?php echo (isset($custDetail) && isset($custDetail["id"]) ) ? base64_encode($custDetail["id"]):'';?>">

                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-4">
                                <label label-for='name'>Customer Name <span class="mandatory_label">*</span></label>
                                <input type="text" name="name" id="name" value="<?php echo (isset($custDetail) && isset($custDetail["name"]) )?$custDetail["name"]:'';?>" placeholder="Customer Name" class="form-control">
                            </div>
                            <div class="col-lg-4">
                                <label label-for='name'>User Name <span class="mandatory_label">*</span></label>
                                <input type="text" name="username" id="username" value="<?php echo (isset($custDetail) && isset($custDetail["username"]) )?$custDetail["username"]:'';?>" placeholder="Customer User Name" class="form-control">
                            </div>
                            <div class="col-lg-4">
                                <label label-for='name'>Email Address <span class="mandatory_label">*</span></label>
                                <input type="text" name="email" id="email" value="<?php echo (isset($custDetail) && isset($custDetail["email"]) )?$custDetail["email"]:'';?>" placeholder="Customer Email Address" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-4">
                                <label label-for='name'>Contact <span class="mandatory_label">*</span></label>
                                <input type="text" name="phone" id="phone" value="<?php echo (isset($custDetail) && isset($custDetail["phone"]) )?$custDetail["phone"]:'';?>" placeholder="Customer Contact" class="form-control">
                            </div>
                            <div class="col-lg-4">
                                <label label-for='name'>Agent ? <span class="mandatory_label">*</span></label>
                                <select name="agent_YN" id="agent_YN" class="form-control">
                                    <option value="1" <?php echo (isset($custDetail) && isset($custDetail["agent_YN"]) && $custDetail['agent_YN'] == 1 ) ? 'selected' : ''; ?> > Yes </option>
                                    <option value="0" <?php echo (isset($custDetail) && isset($custDetail["agent_YN"]) && $custDetail['agent_YN'] == 0 ) ? 'selected' : ''; ?> > No </option>
                                </select>
                            </div>
                            <div class="col-lg-4" style="padding-top: 22px;">
                                <input type="submit" value="Update" name="submit" class="btn btn-outline-primary">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<?php
	if(isset($custom_js)){
		foreach($custom_js as $val){
			?>
			<script src="<?php echo base_url();?>assets/js/custom_js/<?php echo $val;?>"></script>
			<?php
		}
	}
	
?>
  