<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="clearfix mb-10">
        <div class="pull-left">
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Customer Profile</a></li>
                    <li class="breadcrumb-item active" aria-current="page">List</li>
                </ol>
            </nav>
        </div>
        <!-- <div class="pull-right">
            <a class="btn btn-outline-primary" href="<?php //echo base_url('customer/addCust')?>">Add New</a>            
        </div> -->
    </div>

    <div class="container-fluid bg-light"> 
        <div class="row">
            <div class="col-lg-12">

                <form id="catsrch" action="<?php echo base_url('customer')?>">
                    <input type="hidden" name="per_page_search" value="<?php echo $page; ?>">
                    <div class="row align-items-center justify-content-center">
                        <div class="col-md-4 pt-3">
                            <div class="form-group ">
                                <input type="text" name="Name" id="name" value="<?php echo (isset($cust_search["name"]))?$cust_search["name"]:'';?>" placeholder="Customer Name" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-4 pt-3">
                            <div class="form-group">
                                <input type="text" name="userName" id="username" value="<?php echo (isset($cust_search["username"]))?$cust_search["username"]:'';?>" placeholder="Customer User Name" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-4 pt-3">
                            <div class="form-group">
                                <input type="text" name="Email" id="email" value="<?php echo (isset($cust_search["email"]))?$cust_search["email"]:'';?>" placeholder="Customer Email Address" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="row">                       
                        <div class="col-md-4 pb-3">
                            <select id="active" name="status" class="form-control">
                                <option value="">Select Status</option>
                                <option value="1" <?php echo (isset($cust_search) && isset($cust_search["active"]) &&  $cust_search["active"]==1)?'selected':'';?>>Active</option>
                                <option value="0"  <?php echo (isset($cust_search) && isset($cust_search["active"]) &&  $cust_search["active"]==0)?'selected':'';?>>InActive</option>
                            </select>
                        </div>
                        <div class="col-md-4 pb-3">
                            <a class="btn btn-outline-primary" href="<?php echo base_url('seller')?>"><i class="fa fa-refresh" aria-hidden="true"></i> Reset</a> 
                                
                            <input type="submit" name="search" value="Search" class="btn btn-outline-primary">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="row pt-4">
        <div class="col-sm-12">       
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="data-table stripe hover no-wrap" style="font-size: 14px;">
                <thead>
                    <tr class="titel_bar">
                        <th class="table-plus datatable-nosort" width="2%">#</th>
                        <th class="table-plus datatable-nosort" width="10%">Customer Name</th>
                        <th class="table-plus datatable-nosort" width="10%">Contact</th>
                        <th class="table-plus datatable-nosort" width="5%">Email<br>Verified</th>
                        <th class="table-plus datatable-nosort" width="5%">Phone<br>Verified</th>
                        <th class="table-plus datatable-nosort text-center" width="10%">Type (<span style="font-size: 10px">Buyer / Seller / Both</span>)</th>
                        <th class="table-plus datatable-nosort" width="8%"><center>Resend Password</center></th>
                        <th class="table-plus datatable-nosort" width="15%"><center>Status</center></th>
                        <th width="8%"  class="table-plus datatable-nosort text-center" >Action</th>
                    </tr>
                </thead>
                <tbody>

                <?php
                	if(!empty($cust_data)){
                		$i=0;
                		foreach($cust_data as $val){
                			$i++;
                ?>
                
                    <tr class="">
                        <td class="table-plus"><?php echo $i;?></td>
                        <td class="table-plus"><?php echo $val["name"];?></td>
                        <td class="table-plus"><?php echo $val["phone"];?></td>
                        <td class="table-plus">
                            <center>
                                <?php echo ($val['email_verify'] == '1') ? '<i class="fa fa-check fa-2x" aria-hidden="true" style="color: green"></i>' : '<i class="fa fa-times fa-2x" aria-hidden="true" style="color: red"></i>'; ?>
                            </center>
                        </td>
                        <td class="table-plus">
                            <center>
                                <?php echo ($val['phone_verify'] == '1') ? '<i class="fa fa-check fa-2x" aria-hidden="true" style="color: green"></i>' : '<i class="fa fa-times fa-2x" aria-hidden="true" style="color: red"></i>'; ?>
                            </center>
                        </td>
                        <td class="table-plus">
                            <center>
                                <?php echo ($val['supplier_YN'] == '0') ? 'Buyer' : ($val['supplier_YN'] == '1') ? 'Seller' : 'Both'; ?>
                            </center>
                        </td>
                        <td class="table-plus">
                            <center>
                                <a class="btn btn-outline-primary" href="javascript:void(0);" style=" font-size: 11px;" onclick="doResendPass('<?php echo base64_encode($val["id"]);?>')" >
                                    Resend Password
                                </a>
                            </center>
                        </td>
                        
                        

                        <td class="table-plus">
                            <select name="status" id="status" onchange="doCustStatusChange(this.value,'<?php echo base64_encode($val["id"]);?>')" class="form-control">
                                <option value="1" <?php echo (isset($val) && isset($val["active"]) && $val['active'] == 1)?'selected':''; ?>> Active </option>

                                <option value="0" <?php echo (isset($val) && isset($val["active"]) && $val['active'] == 0)?'selected':''; ?>> InActive </option>
                            </select>
                        </td>

                        <td class="table-plus text-center">
                            <a class="btn btn-outline-primary customer" href="#" role="button" data-toggle="modal" data-id="<?php echo base64_encode($val['id']) ?>">
                                <i class="fa fa-eye"></i>
                            </a>
                        </td>
                        <!-- <td class="table-plus" style="text-align:center;">

                            <div class="dropdown">
                                <a class="btn btn-outline-primary dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                    <i class="fa fa-ellipsis-h"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <a class="dropdown-item" href="javascript:void(0);" style=" font-size: 15px;" onclick="doCustEdit('<?php //echo base64_encode($val["id"]);?>')" >
                                        <i class="fa fa-edit" aria-hidden="true"></i> 
                                        Edit
                                    </a>
                                </div>
                            </div>

                        </td> -->
                	</tr>
                			
                <?php
                		}
                	}else{
                ?>

                    <tr class="">
                		<td colspan="10"><center>No Customer Record Found</center></td>
                    </tr>	
                		
                <?php
                	}
                ?>
               
                     
                </tbody>
            </table>
        </div>
        <div class="col-sm-12">
            <div style="float: right;"><?php echo $links; ?></div>
        </div>
    </div>
</div>



<!-- Modal -->

<div class="modal fade admin_modal" id="viewModal">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h4 class="modal-title"> Customer Details </h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="row" style="padding-bottom: 20px;">
                    <div class="col-lg-12">
                        <label style="font-weight: 400;">Name : </label> <span id="cust_name"> </span>
                    </div>
                </div>

                <div class="row" style="padding-bottom: 20px;">
                    <div class="col-lg-6">
                        <label style="font-weight: 400;">E-mail : </label> <span id="cust_email"> </span>
                    </div>
                    <div class="col-lg-6">
                        <label style="font-weight: 400;">Contact : </label> <span id="cust_phone"> </span>
                    </div>
                </div>

                <div class="row" style="padding-bottom: 20px;">
                    <div class="col-lg-6">
                        <label style="font-weight: 400;">Username : </label> <span id="cust_username"> </span>
                    </div>
                    <div class="col-lg-6">
                        <label style="font-weight: 400;">Created On : </label> <span id="created_date"> </span>
                    </div> 
                </div>
            </div>

        </div>
    </div>
</div>


<!-- Modal Ends -->



<?php
	if(isset($custom_js)){
		foreach($custom_js as $val){
			?>
			<script src="<?php echo base_url();?>assets/js/custom_js/<?php echo $val;?>"></script>
			<?php
		}
	}
	
?>


<script type="text/javascript">
    
    $('.customer').click(function(){
        var base_url = $('#base_url').val();
        var iid = $(this).data('id');

        $.ajax({
            url: base_url + 'customer/getById',
            type: 'POST',
            data: {id: iid},
            dataType: 'JSON',
            success: function(response) {
                //console.log(response.data);
                
                $('#cust_name').html(response.data.name);
                $('#created_date').html(response.data.created_date);
                $('#cust_email').html(response.data.email);
                $('#cust_phone').html(response.data.phone);
                $('#cust_username').html(response.data.username);

                $('#viewModal').modal('show');
            }
        });


    });

</script>
  