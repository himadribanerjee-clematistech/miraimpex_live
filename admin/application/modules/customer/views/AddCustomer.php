<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
    <div class="clearfix mb-10">
        <div class="pull-left">
            <nav aria-label="breadcrumb" role="navigation">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">Customer</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Add New Customer</li>
                </ol>
            </nav> 
        </div> 
        <div class="pull-right">
            <a class="btn btn-outline-primary" href="<?php echo base_url()?>customer">Back</a>            
        </div> 
    </div>

    <div class="container-fluid"> 
        <div class="row">
            <div class="col-lg-12">
                 <form id="addCustfrm" method="post" action="javascript:void(0);" onsubmit="addCustomer();" autocomplete="off">
                    
                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-3">
                                <label label-for='name'>Customer Name <span class="mandatory_label">*</span></label>
                                <input type="text" name="name" id="name" value="" placeholder="Customer Name" class="form-control">
                            </div>
                            <div class="col-lg-3">
                                <label label-for='name'>User Name <span class="mandatory_label">*</span></label>
                                <input type="text" name="username" id="username" value="" placeholder="User Name" class="form-control">
                            </div>
                            <div class="col-lg-3">
                                <label label-for='name'>Business Name <span class="mandatory_label">*</span></label>
                                <input type="text" name="business_name" id="business_name" value="" placeholder="Business Name" class="form-control">
                            </div>
                            <div class="col-lg-3">
                                <label label-for='name'>Email Address <span class="mandatory_label">*</span></label>
                                <input type="text" name="email" id="email" value="" placeholder="Email Address" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-6">
                                <label label-for='name'>Address <span class="mandatory_label">*</span></label>
                                <input type="text" name="address" id="address" value="" placeholder="Enter Address" class="form-control">
                            </div>
                            <div class="col-lg-3">
                                <label label-for='name'>State <span class="mandatory_label">*</span></label>
                                <input type="text" name="state" id="state" value="" placeholder="State" class="form-control">
                            </div>
                            <div class="col-lg-3">
                                <label label-for='name'>City <span class="mandatory_label">*</span></label>
                                <input type="text" name="city" id="city" value="" placeholder="City" class="form-control">         
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-lg-3">
                                <label label-for='name'>Contact <span class="mandatory_label">*</span></label>
                                <input type="text" name="phone" id="phone" value="" placeholder="Contact" class="form-control">
                            </div>
                            <div class="col-lg-3">
                                <label label-for='name'>Latitude <span class="mandatory_label">*</span></label>
                                <input type="text" name="lat" id="lat" value="" placeholder="Latitude" class="form-control">
                            </div>
                            <div class="col-lg-3">
                                <label label-for='name'>Longitude <span class="mandatory_label">*</span></label>
                                <input type="text" name="long" id="long" value="" placeholder="Longitude" class="form-control">
                            </div>
                            <div class="col-lg-3">
                                <label label-for='name'>Bussiness Logo <span class="mandatory_label">*</span></label>

                                <br>

                                <img src="<?php echo base_url('assets/images/no-item-image.png'); ?>" height="150" width="140"  style="border: 2px dotted #ccc; border-radius: 2px;" id="chImg"/>
                                    
                                <br><br>

                                <input type="file" name="business_logo" id="business_logo" onchange="loadFile()">

                                <script type="text/javascript">
                                    
                                    function loadFile(){
                                
                                        var imgfilename = document.getElementById('chImg');

                                        imgfilename.src = URL.createObjectURL(event.target.files[0]);
                                    };

                                </script>
                                
                            </div>
                            
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row justify-content-end">
                            <div class="col-lg-2">
                            <input type="submit" value="Add" name="submit" class="btn btn-outline-primary" id="add">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<?php
    if(isset($custom_js)){
        foreach($custom_js as $val){
            ?>
            <script src="<?php echo base_url();?>assets/js/custom_js/<?php echo $val;?>"></script>
            <?php
        }
    }
    
?>
  