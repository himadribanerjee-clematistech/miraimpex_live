<?php  

defined('BASEPATH') OR exit('No direct script access allowed');
class Customer extends CI_Controller {
	protected $token_id='';
	protected $per_page=10;
	public function __construct() {
    	parent::__construct();
    	$this->token_id=get_cookie('token_id'); 
    	if(trim($this->token_id)==''){
    		redirect("login");
    	}
        $this->load->model("customerModel");
        $this->load->model("userModel");
        $this->load->helper("pagination");
        $this->load->library("pagination");
        $this->load->library('email'); 
         $this->load->model("settingsModel", 'sM');
         $this->load->model("buyerModel");
        $settings_data=$this->sM->getSettings();
        if(isset($settings_data->per_page)){
         $this->per_page=$settings_data->per_page;
        }
    }
    public function index() {
		try {
           $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            
            if(empty($user_details)){
                    $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                    delete_cookie('token_id'); 
                     delete_cookie('useName'); 
                    echo '<script>
                        alert("Unexpected server error.Please try again some time.");
                            window.location="'.base_url().'login";
                    </script>';
            }
            $user_details=current($user_details);
            $data=[]; 
            $data["menu"]="customer";
            $data["useName"]=$user_details["Name"];
            $data["custom_js"]=['customer.js'];
            set_cookie('useName',$user_details["Name"],'3600');
            //search 
            $search_input=$this->input->get();
            //print_r($search_input); die();

            $cust_search=[];
            
            if(isset($search_input["Name"]) && trim($search_input["Name"])!=''){
            	$cust_search["name"] = $search_input["Name"];
            }
            if(isset($search_input["userName"]) && trim($search_input["userName"])!=''){
                $cust_search["username"] = $search_input["userName"];
            }
            if(isset($search_input["Email"]) && trim($search_input["Email"])!=''){
                $cust_search["email"] = $search_input["Email"];
            }
            if(isset($search_input["status"]) && trim($search_input["status"])!=''){
                $cust_search["active"] = $search_input["status"];
            }
            
            $data["cust_search"] = $cust_search;
            $page=0;
			if($this->input->get("per_page")){
	       		$page= $this->input->get("per_page");
	       	}

            $data["page"]=$page;

            $total_row=$this->customerModel->getCustCount($cust_search);
         
	        $data["links"] = createPageNationLink(base_url('customer/index'), $total_row,$this->per_page);

	        $start= (int)$page +1;
			$end = ($page+ $this->per_page >=$total_row)? $total_row : (int)$page  + $this->per_page;
			$data['result_count']='';
			if($total_row>0){
				$data['result_count']= "Showing ".$start." - ".$end." of ".$total_row." Results";
			}
			
			$data["cust_data"]=$this->customerModel->getCustList($cust_search,$this->per_page, $page);

            // echo '<pre>'; print_r($data); die();

            $data['view_file'] = 'Customer';
            view($data);
           
        } catch (Exception $e) {
            $resposne=["status"=>"1","e"=>$e,"message"=>"Unexpected server error.Please try again some time.","field"=>'user_name'];
        }   
	}
	
    public function addCust(){
        try {
            $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            if(empty($user_details)){
                    $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                    delete_cookie('token_id'); 
                     delete_cookie('role_id'); 
                    echo '<script>
                        alert("Session expired please login again.");
                            window.location="'.base_url().'login";
                    </script>';
                    exit();
            }
            $user_details=current($user_details);
            $data=[]; 
            $data["menu"]="customer";
            $data["useName"]=$user_details["Name"];
            $data["custom_js"]=['customer.js'];
            set_cookie('useName',$user_details["Name"],'3600');
             $data['view_file'] = 'AddCustomer';
            view($data);
        } catch (Exception $e) {
            echo '<script>
                alert("Unexpected server error.Please try again some time.");
                            window.location="'.base_url().'customer";
            </script>';

        }
    }

    public function doAddCust(){
        try {
            $data=$this->input->post();

            //print_r($data); die();

            $resposne=["status"=>"0","message"=>"", "field"=>''];
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));

            if(trim($this->token_id)!=''){
                if(isset($data["name"]) && trim($data["name"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter Customer Name.","field"=>'name'];
                }else if(isset($data["username"]) && trim($data["username"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter Customer User Name.","field"=>'username'];
                }else if(isset($data["email"]) && trim($data["email"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter Customer Email Address.","field"=>'email'];
                }else if(isset($data["phone"]) && trim($data["phone"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter Customer Contact.","field"=>'phone'];
                }else if(isset($data["business_name"]) && trim($data["business_name"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter Bussiness Name.","field"=>'business_name'];
                }else if(isset($data["address"]) && trim($data["address"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter Address.","field"=>'address'];
                }else if(isset($data["state"]) && trim($data["state"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter State.","field"=>'state'];
                }else if(isset($data["city"]) && trim($data["city"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter City.","field"=>'city'];
                }else if(isset($data["lat"]) && trim($data["lat"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter Latitude.","field"=>'lat'];
                }else if(isset($data["long"]) && trim($data["long"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter Longitude.","field"=>'long'];
                }else if(isset($_FILES["business_logo"]) && trim($_FILES["business_logo"]['name'])==''){
                    $resposne=["status"=>"1","message"=>"Please Choose Logo Image.","field"=>'business_logo'];
                }
                else{ 
                    $check_duplicate_cust_name = $this->customerModel->doCustCheck($data["name"]);

                    if($check_duplicate_cust_name == 1) {
                        $resposne=["status"=>"1","message"=>"Duplicate Customer Name.","field"=>'name'];
                    }
                    else {
                        $check_duplicate_profile = $this->buyerModel->doBuyerCheck('', $data["business_name"]);

                        if($check_duplicate_profile == 1) {
                            $resposne=["status"=>"1","message"=>"Duplicate Bussiness Name.","field"=>'business_name'];
                        }
                        else {

                            $password = $this->randomPassword();

                            //echo $password;

                            // Mail Format
                            $to = $data["email"];
                            $subject = 'Agro Wholesale Login Password for ' . $data['name'];
                            $from = 'info@agrowholesale.com';
                            $headers  = 'MIME-Version: 1.0' . "\r\n";
                            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                            $headers .= 'From: ' . $from . "\r\n". 'Reply-To: ' . $from . "\r\n" . 'X-Mailer: PHP/' . phpversion();
                            
                            $message = '<html>
                                            <body style="margin:0px;">
                                                <table style="padding: 20px 25px;background: linear-gradient(to left top, rgb(51, 204, 255) 0%, rgb(255, 153, 204) 100%);">
                                                    <tr>
                                                        <td>
                                                            <table style="width: 100%;margin: 0px auto; padding: 0px 25px 15px;text-align: center;font-family: sans-serif;">
                                                                <tr>
                                                                    <th>
                                                                        <img src="http://agro.clematistech.com/assets/img/logo.png" alt="">
                                                                    </th>
                                                                </tr>
                                                            </table>
                                                            <table style="width: 100%;margin: 0px auto; padding:30px 25px 20px;text-align: left;font-family: sans-serif;background-color: #ffffff;">
                                                                <tr>
                                                                    <td style="text-align: center;">
                                                                        <p style="font-size: 20px; color:#333;padding-top: 7px;margin: 0px;">
                                                                            Hello
                                                                            <span style="font-size: 22px; color: #f1a306;padding-left: 10px;">' . $data['name'] . '</span>
                                                                        </p>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table style="width: 100%;margin: 0px auto; padding:0px 25px 20px;text-align: left;font-family: sans-serif;background-color: #ffffff;">
                                                                <tr>
                                                                    <td style="text-align: left;">
                                                                        <p style="font-size: 15px; color:#333;line-height: 25px; padding-bottom: 10px;margin: 0px;">
                                                                            Here your userid and password attached below, please use this at the login time.
                                                                        </p>
                                                                        <p style="font-size: 15px; color:#333;line-height: 25px; padding-bottom: 10px;margin: 0px;">User Id : ' . $data['username'] . '<br>Password : ' . $password . '
                                                                        </p>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <table style="width: 100%;margin: 0px auto; padding: 30px 25px 0px;text-align: center;font-family: sans-serif;">
                                                                <tr>
                                                                    <th style="width: auto;display: inline-block;">
                                                                        <a href="#" style="color: #fff;text-decoration: none;padding: 0px 5px;">
                                                                            <img src="http://agro.clematistech.com/assets/img/facebook.png" alt="">
                                                                        </a>
                                                                    </th>
                                                                    <th style="width: auto;display: inline-block;">
                                                                        <a href="#" style="font-size: 15px;color: #fff;text-decoration: none;padding: 0px 5px;">
                                                                            <img src="http://agro.clematistech.com/assets/img/twitter.png" alt="">
                                                                        </a>
                                                                    </th>
                                                                    <th style="width: auto;display: inline-block;">
                                                                        <a href="#" style="font-size: 15px;color: #fff;text-decoration: none;padding: 0px 5px;">
                                                                            <img src="http://agro.clematistech.com/assets/img/pinterest.png" alt="">
                                                                        </a>
                                                                    </th>
                                                                </tr>
                                                                <tr>
                                                                    <th>
                                                                        <p style="font-size: 13px;color: #eee;font-weight: 100;padding-top: 0px;margin: 0px;">
                                                                            © 2019 <a href="http://agro.clematistech.com/" style="font-size: 14px;color: #333;">Akpoti.farm</a>. All rights reserved.
                                                                        </p>
                                                                    </th>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </body>
                                        </html>';
                            
                            $mail = mail($to, $subject, $message, $headers);

                            if ($mail) {
                                $custData = [
                                    'username'  =>  $data['username'],
                                    'name'      =>  $data['name'],
                                    'email'     =>  $data['email'],
                                    'phone'     =>  $data['phone'],
                                    'password'  =>  $this->userModel->passwordGenerate(trim($password)),
                                    //'agent_YN'  =>  $data['agent_YN'],
                                ];

                                $res = $this->customerModel->insertCust($custData);

                                if ($res) {
                                    $last_id = $this->db->insert_id();

                                    $file_name='';                        
                                    $file_name=time().$_FILES["business_logo"]["name"];

                                    $allowedExts = array("gif", "jpeg", "jpg", "png");
                                    $image_data=explode(".", $_FILES["business_logo"]["name"]);
                                    $extension =(!empty($image_data))?$image_data[1]:'';
                                     
                                    if ((($_FILES["business_logo"]["type"] == "image/gif")  || ($_FILES["business_logo"]["type"] == "image/jpeg")  || ($_FILES["business_logo"]["type"] == "image/jpg")  || ($_FILES["business_logo"]["type"] == "image/png")) && in_array($extension, $allowedExts)){
                                        
                                        $config['upload_path'] = './uploads/bussiness_logo/';
                                        $config['allowed_types'] = 'jpg|png|jpeg|gif';
                                        $config['file_name'] =$file_name;

                                        $this->load->library('upload', $config);
                                        if ( ! $this->upload->do_upload('business_logo')){
                                            $resposne=["status"=>"1","message"=>"Please Select jpg/jpeg/png/jpeg Logo Image.",'error'=> $this->upload->display_errors(),"field"=>'image'];
                                        }
                                        else {

                                            $buyerData = [
                                                'cust_id'       =>  $last_id,
                                                'business_name' =>  $data['business_name'],
                                                'business_logo' =>  $file_name,
                                                'address'       =>  $data['address'],
                                                'state'         =>  $data['state'],
                                                'city'          =>  $data['city'],
                                                'lat'           =>  $data['lat'],
                                                'long'          =>  $data['long']
                                            ];

                                            $buyer = $this->buyerModel->insertBuyer($buyerData);
                                            if($buyer>0){
                                                $resposne=["status"=>"0","message"=>"Customer Profile added successfully.","field"=>''];
                                            }else{
                                                
                                                $resposne=["status"=>"1","message"=>"Customer Profile added failed.Please try again some time.","field"=>'business_name'];
                                                
                                            }
                                        }
                                    }
                                    else {
                                        $resposne=["status"=>1,"message"=>"Some Error Happened in uploading Image. Please Try Again. Thank You."];
                                    } 
                                }
                                else {
                                    $resposne=["status"=>1,"message"=>"Some Error Happened. Please Try After Sometime. Thank You."];
                                } 
                            }
                            else {
                                $resposne=["status"=>1,"message"=>"Please Contact System Administrator Immediately"];
                            }
                        }                    
                    }
                }
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied."];
            }
        } catch (Exception $e) {
            $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time."];

        }

        echo json_encode($resposne);
    }

    public function doResendPass() {
        $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            
        if(empty($user_details)){
                $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                delete_cookie('token_id'); 
                 delete_cookie('role_id'); 
                echo '<script>
                    alert("Session expired please login again.");
                        window.location="'.base_url().'login";
                </script>';
                exit();
        }
        $user_details=current($user_details);
        $iid = $this->input->post('id');
        $id=base64_decode($iid);
        if(trim($id)==''){
            echo '<script>
                    alert("Invalid user.");
                        window.location="'.base_url().'customer";
                </script>';
                exit();
        }
        else {

            $resposne=["status"=>"0","message"=>"", "field"=>''];

            $cust = current($this->customerModel->getById($id));

            if (empty($cust)) {
                $resposne=["status"=>"1","message"=>"No Data Found According to This Following Id."]; 
            }
            else {
                $password = $this->randomPassword();

                $message = '<h3>Hello ' . $cust['name'] . ',</h3> <p>Here your userid and password attached below, please use this at the login time.</p><p>User Id : ' . $cust['username'] . '<br>Password : ' . $password . '</p>';

                $mail = mail($cust['email'], 'Agro Wholesale User Id & Password', $message);

                if ($mail) {
                    $custData = [
                        'password'  => $this->userModel->passwordGenerate(trim($password)),
                    ];

                    $res = $this->customerModel->updateCust($custData, $id);

                    if($res){
                        $resposne=["status"=>"0","message"=>"Password Reset successfully. Kindly Check Your Email"];
                    }
                    else{                                
                        $resposne=["status"=>"1","message"=>"Please try again after some time."];                                
                    }
                }
                else {
                    $resposne=["status"=>1,"message"=>"Some Error Happened. Please Try After Sometime. Thank You."];
                }  
            }

            echo json_encode($resposne);

        }
    }

    public function getById() {
        try{
            $id = base64_decode($this->input->post('id'));

            $resposne=["status"=>"0","message"=>"","field"=>'', 'data'=> ''];

            $user_details = current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
            if(trim($this->token_id)!=''){
                $res = current($this->customerModel->getById($id));

                $resposne=["status"=>"0","message"=>"Success.","field"=>'', 'data'=> $res];
            }
            else {
                $resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
            }
        }
        catch (Exception $e) {
            $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'role_id'];
        }

        echo json_encode($resposne);
    }

    /*public function doCustEdit($id){
        $user_details=$this->userModel->getUserDetailsbyId(trim($this->token_id) );
            
        if(empty($user_details)){
                $this->userModel->updateUser(["token_id"=>null],["token_id"=>$this->token_id] );
                delete_cookie('token_id'); 
                 delete_cookie('role_id'); 
                echo '<script>
                    alert("Session expired please login again.");
                        window.location="'.base_url().'login";
                </script>';
                exit();
        }
        $user_details=current($user_details);
        $id=base64_decode($id);
        if(trim($id)==''){
            echo '<script>
                    alert("Invalid user.");
                        window.location="'.base_url().'customer";
                </script>';
                exit();
        }
        $data=[]; 
        $data["menu"]="customer";
        $data["useName"]=$user_details["Name"];
        $data["custom_js"]=['customer.js'];
        set_cookie('useName',$user_details["Name"],'3600');
        $data['custDetail'] = current($this->customerModel->getById($id));
         
        $data['view_file'] = 'EditCustomer';
        view($data);
            
    }
    public function doUpdateCust(){

        try {
            $data=$this->input->post();

            $resposne=["status"=>"0","message"=>"", "field"=>''];
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));

            if(trim($this->token_id)!=''){
                if(isset($data["name"]) && trim($data["name"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter Customer Name.","field"=>'name'];
                }
                else if(isset($data["username"]) && trim($data["username"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter Customer User Name.","field"=>'username'];
                }
                else if(isset($data["email"]) && trim($data["email"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter Customer Email Address.","field"=>'email'];
                }
                else if(isset($data["phone"]) && trim($data["phone"])==''){
                    $resposne=["status"=>"1","message"=>"Please Enter Customer Contact.","field"=>'phone'];
                }
                else{
                    $check_duplicate_cust_name = $this->customerModel->doCustCheck($data["name"], base64_decode($data['id']));

                    if($check_duplicate_cust_name == 1) {
                        $resposne=["status"=>"1","message"=>"Duplicate Customer Name.","field"=>'name'];
                    }
                    else {
                        $custData = [
                            'username'  =>  $data['username'],
                            'name'      =>  $data['name'],
                            'email'     =>  $data['email'],
                            'phone'     =>  $data['phone'],
                            //'agent_YN'  =>  $data['agent_YN'],
                        ];

                        $res = $this->customerModel->updateCust($custData, base64_decode($data['id']));

                        if($res){
                            $resposne=["status"=>"0","message"=>"Customer Updated successfully."];
                        }
                        else{                                
                            $resposne=["status"=>"1","message"=>"Customer Updation failed.Please try again after some time."];                                
                        }                      
                    }
                }
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied."];
            }
        } catch (Exception $e) {
            $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time."];

        }

        echo json_encode($resposne);
    }*/
    

    

    public function doCustStatusChange(){
        $data=$this->input->post();
        //print_r($data);
        $id=base64_decode($data["id"]);
        $resposne=["status"=>"0","message"=>"","field"=>''];
        try {
            $user_details=current($this->userModel->getUserDetailsbyId(trim($this->token_id) ));
            if(trim($this->token_id)!=''){
                $res = $this->customerModel->updateCust(["active"=>$data["active"], 'update_by' => 1, 'update_on' => date('Y-m-d H:i:s')], $id);
              
                if($res){
                    $resposne=["status"=>"0","message"=>"Customer status changed successfully."];
                }
                else{                                
                    $resposne=["status"=>"1","message"=>"Customer change failed.Please try again after some time."];                                
                }
            }else{
                $resposne=["status"=>"1","message"=>"Permission Denied.","field"=>'to_login'];
            }   
        }catch (Exception $e) {
                $resposne=["status"=>"1","message"=>"Unexpected server error.Please try again some time.","field"=>'status'];

        }

        echo json_encode($resposne);
    }



    public function randomPassword() {
        $quote = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+=_)(*&^%$#@!|}{><?/';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($quote) - 1; //put the length -1 in cache
        for ($i = 0; $i < 10; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $quote[$n];
        }
        return implode($pass); //turn the array into a string
    }  

}
?>