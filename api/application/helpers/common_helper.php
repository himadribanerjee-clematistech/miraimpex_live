<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}


if (!function_exists('generateOTP')) {

    function generateOTP() {
      	$generator = "1357902468"; 
  		$result = ""; 
	  	$n=6;
	    for ($i = 1; $i <= $n; $i++) { 
	        $result .= substr($generator, (rand()%(strlen($generator))), 1); 
	    } 
	  
	    // Return result 
	    return $result; 
    }

}

if (!function_exists('buildTree')) {
  function buildTree(array $elements, $parentId = 0,$level,$level_count) {
          $branch = array();
          
          foreach ($elements as $element) {

              if ($element['parent_id'] == $parentId ) {
                  $level_count++;
                  $children = buildTree($elements, $element['id'],$level,$level_count);
                   if ($children  && $level_count<=$level) {
                      $element['children'] = $children;
                  }
                  $element["id"]=base64_encode($element["id"]);
                  $element["parent_id"]=base64_encode($element["parent_id"]);
                  $branch[] = $element;
              }
          }

          return $branch;
      }
}

if (!function_exists('buildTreeID')) {
  function buildTreeID(array $elements,array $ids) {
         
          foreach ($elements as $element) {
          		$ids[]=base64_decode($element["id"]);
          		if(isset($element["children"]) && !empty($element["children"])){
          			$ids= buildTreeID($element["children"], $ids);
          		}
     }

          return $ids;
      }
}
?>
