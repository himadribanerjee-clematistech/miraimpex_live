<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST");
class Messagecenter extends REST_Controller {
	
	public function __construct() {
        parent::__construct();
        $this->load->helper(['jwt', 'authorization']); 
        $this->load->model("itemModel");
        $this->load->model("rfqModel");
         $this->load->model("customerModel");
         $this->load->model("categoryModel");
         $this->load->model("messagecenterModel");
	 }
     public function index_post(){
        $status = parent::HTTP_UNAUTHORIZED;
            $response = ['status' => $status, 'msg' => 'Unauthorized Access! '];
            $this->response($response, $status);
     }
  	 private function verify_request(){
         $headers = $this->input->request_headers();
         if(isset($headers['Authorization'])){
             $token = $headers['Authorization'];
             try {
                 $data = AUTHORIZATION::validateToken($token);
                    if ($data === false) {
                        $status = parent::HTTP_UNAUTHORIZED;
                        $response = ['status' => $status, 'msg' => 'Unauthorized Access!'];
                        $this->response($response, $status);

                        exit();
                    } else {
                        $customer_data=current($this->customerModel->doAuthenticateCheck(["token_id"=>$data->token_id]));
                        
                        return $customer_data;
                    }
            } catch (Exception $e) {
               $status = parent::HTTP_UNAUTHORIZED;
                $response = ['status' => $status, 'msg' => 'Unauthorized Access! '];
                $this->response($response, $status);
            }
        }else if(isset($headers['authorization'])){
             $token = $headers['authorization'];
             $data = AUTHORIZATION::validateToken($token);
                if ($data === false) {
                    $status = parent::HTTP_UNAUTHORIZED;
                    $response = ['status' => $status, 'msg' => 'Unauthorized Access!'];
                    $this->response($response, $status);
                    
                    exit();
                } else {
                    $customer_data=current($this->customerModel->doAuthenticateCheck(["token_id"=>$data->token_id]));
                    
                    return $customer_data;
                }
        }else{
             $status = parent::HTTP_UNAUTHORIZED;
                $response = ['status' => $status, 'msg' => 'Unauthorized Access!'];
                $this->response($response, $status);

                exit();
        }
        
        
    }
   public function getProposalDetails_post(){
    try {
        $data1=$this->post();
        $data = $this->verify_request();
        if(!empty( $data)){
            if(!(isset($data1["proposal_id"]) && trim($data1["proposal_id"])!='' )){
                $this->response(['msg' => 'Invalid Proposal','field'=>'proposal_id'], parent::HTTP_BAD_REQUEST);
            }else{
                $search21=[];
                $search21["id"]=base64_decode($data1["proposal_id"]);
                $proposal_data=current($this->messagecenterModel->getProposal($search21));
                if(!empty( $proposal_data)){
                    $proposal_data["refference_id"]=base64_encode($proposal_data["refference_id"]);
                    $proposal_data["id"]=base64_encode($proposal_data["id"]);
                    $proposal_data["from_id"]=base64_encode($proposal_data["from_id"]);
                    $proposal_data["to_id"]=base64_encode($proposal_data["to_id"]);
                    $proposal_data["created_date"]=date_format(date_create($proposal_data["created_date"]),"d/m/Y ");
                    $status = parent::HTTP_OK;
                    $response = ['status' => $status,'msg'=>'Proposal Data.','proposal_data'=>$proposal_data];
                    $this->response($response, $status);

                }else{
                    $this->response(['msg' => 'Invalid Proposal','field'=>'proposal_id'], parent::HTTP_BAD_REQUEST); 
                }   
            }
        }else{
            $status = parent::HTTP_UNAUTHORIZED;
            $response = ['status' => $status, 'msg' => 'Unauthorized Access! '];
            $this->response($response, $status);
        }        
    } catch (Exception $e) {

        $this->response(['msg' => 'server busy .Please try again some time.','field'=>'user_name'], parent::HTTP_NOT_FOUND);
      } 
   }

   public function getProposalList_post(){
    try {
        $data1=$this->post();
        $data = $this->verify_request();
        if(!empty( $data)){
            $limit=20;
            $start=0;
            if(!isset($data1["pageno"])){
              $data1["pageno"]=1;
            }
   
            if((isset($data1["pageno"])&& trim($data1["pageno"])!='')){
             $start=($data1["pageno"]-1)* $limit;
           }
           $search21=[];
           $search21["user_id"]=$data["id"];
           $proposal_data=$this->messagecenterModel->getProposal($search21,$limit,$start);
           if(!empty( $proposal_data)){
            foreach($proposal_data as $key=>$val){
                     $proposal_data[$key]["refference_id"]=base64_encode($val["refference_id"]);
                    $proposal_data[$key]["id"]=base64_encode($val["id"]);
                    $proposal_data[$key]["from_id"]=base64_encode($val["from_id"]);
                    $proposal_data[$key]["to_id"]=base64_encode($val["to_id"]);
                    $proposal_data[$key]["created_date"]=date_format(date_create($val["created_date"]),"d/m/Y ");
            }
           }
           $status = parent::HTTP_OK;
           $response = ['status' => $status,'proposal_data'=>$proposal_data,'msg'=>' proposal list.'];
           $this->response($response, $status);
        }else{
            $status = parent::HTTP_UNAUTHORIZED;
            $response = ['status' => $status, 'msg' => 'Unauthorized Access! '];
            $this->response($response, $status);
        }  
    } catch (Exception $e) {

        $this->response(['msg' => 'server busy .Please try again some time.','field'=>'user_name'], parent::HTTP_NOT_FOUND);
      }      
   }
   public function getOrderList_post(){
               try {
                $data1=$this->post();
                $data = $this->verify_request();
                if(!empty( $data)){
                    $limit=20;
                    $start=0;
                    if(!isset($data1["pageno"])){
                      $data1["pageno"]=1;
                    }
           
                    if((isset($data1["pageno"])&& trim($data1["pageno"])!='')){
                     $start=($data1["pageno"]-1)* $limit;
                   }

                   $search21=[];
                   $search21["user_id"]=$data["id"];
                   $order_data=$this->messagecenterModel->getOrderLog($search21,$limit,$start);
                   foreach($order_data as $key=>$val){
                    $order_data[$key]["proposal_id"]=base64_encode($val["proposal_id"]);
                    $order_data[$key]["from_id"]=base64_encode($val["from_id"]);
                    $order_data[$key]["to_id"]=base64_encode($val["to_id"]);
                   
                     if(trim($val["seller_id"])!=''){
                        $order_data[$key]["seller_id"]=base64_encode($val["seller_id"]);
                     }
                      if(trim($val["user_id"])!=''){
                        $order_data[$key]["buyer_id"]=base64_encode($val["user_id"]);
                     }
                     unset($order_data[$key]["user_id"]);

                       unset($order_data[$key]["ofrom_name"]);
                        unset($order_data[$key]["ofrom_email"]);
                        unset($order_data[$key]["ofrom_phone"]);
                        unset($order_data[$key]["ofrom_profile_image"]);
                         unset($order_data[$key]["ofrom_business_name"]);
                         
                   }

                   $status = parent::HTTP_OK;
                   $response = ['status' => $status,'order_data'=>$order_data,'msg'=>' order list.'];
                   $this->response($response, $status);
                 

                }else{
                    $status = parent::HTTP_UNAUTHORIZED;
                    $response = ['status' => $status, 'msg' => 'Unauthorized Access! '];
                    $this->response($response, $status);
                }  
            } catch (Exception $e) {

                $this->response(['msg' => 'server busy .Please try again some time.','field'=>'user_name'], parent::HTTP_NOT_FOUND);
              }      
   }

   public function getOrderDetails_post(){
    try {
        $data1=$this->post();
        if(!isset($data1["lang"])){
            $data1["lang"]='en';
        } 
        $data = $this->verify_request();
        if(!empty( $data)){
            if(!(isset($data1["order_id"]) && trim($data1["order_id"])!='' )){
                $this->response(['msg' => 'Invalid Order','field'=>'order_id'], parent::HTTP_BAD_REQUEST);
            }else{
                $search21=[];
                $search21["order_id"]=$data1["order_id"];
                $order_data=current($this->messagecenterModel->getOrderLog($search21));
		
                $order_data["reference_details"]=[];
                $order_data["proposal_details"]=[];
                if(!empty( $order_data)){
                    if($order_data["proposal_id"]>0){
                        $search21=[];
                        $search21["id"]=$order_data["proposal_id"];
                        $proposal_data=current($this->messagecenterModel->getProposal($search21));
                        if(!empty( $proposal_data)){
                               
                                
                           
                            if($proposal_data["refference_type"]=='rfq'){
                                $search1=[];
                                $search1["rfq_id"]=$proposal_data["refference_id"];
                                
                                $search1["group_by"]=["rfq.rfq_id"];
                                $rfqdata1=$this->messagecenterModel->getRFQList($search1);
                                    if(!empty($rfqdata1)){
                                        foreach($rfqdata1 as $val){
                                            $dataset=[];
                                             $dataset["rfq_id"]=base64_encode($val["rfq_id"]);
                                            $dataset["buyer_id"]=base64_encode($val["buyer_id"]);
                                            $dataset["cat_id"]=base64_encode($val["cat_id"]);
                                            $dataset["uom_id"]=base64_encode($val["uom_id"]);
                                            $dataset["author_id"]=base64_encode($val["to_customer_id"]);
                                            $dataset["target_id"]=base64_encode($val["from_customer_id"]);
                                            $dataset["to_id"]=base64_encode($val["to_customer_id"]);
                                            $dataset["from_id"]=base64_encode($val["from_customer_id"]);
                                            $dataset["from_name"]=$val["fromcust_name"];
                                            $dataset["target_name"]=$val["fromcust_name"];
                                            if(trim($val["from_business_name"])!=''){
                                                $dataset["target_name"]=$val["from_business_name"];
                                            }
                
                                            
                                            $dataset["target_profile_image"]=$val["fromcust_profile_image"];
                
                                            $dataset["quantity"]=$val["quantity"];
                                            $dataset["message"]=$val["message"];
                                            $dataset["other_requerment"]=$val["other_requerment"];
                                            $dataset["qrname"]=$val["qrname"];
                                            $dataset["rfqlocation"]=$val["rfqlocation"];
                                            $dataset["latitudes"]=$val["latitudes"];
                                            $dataset["longitude"]=$val["longitude"];
                                            $dataset["business_logo"]=$val["business_logo"];
                                            $dataset["buyer_name"]=$val["name"];
                                            $dataset["buyer_email"]=$val["email"];
                                            $dataset["buyer_phone"]=$val["phone"];
                                            $dataset["created_date"]=date_format(date_create($val["created_date"]),"d/m/Y");
                                            
                                            
                                            if($val["date"]!=''){
                                                $dataset["last_message_date"]=date_format(date_create($val["date"]),"d/m/Y h:i:s a ");
                                               
                                               
                                                    
                                            }else{
                                                $dataset["last_message_date"]='';
                                               
                                            }
                                            $dataset["type"]="rfq";
                                            $dataset["images"]=[];
                                            if($data1["lang"]=='en'){
                                                $dataset["category_name"]=$val["category_name_en"];
                                                $dataset["uom_name"]=$val["uom_name_en"];
                                            }else{
                                                $dataset["category_name"]=$val["category_name_fr"];
                                                $dataset["uom_name"]=$val["uom_name_fr"];
                                            }
                                            $rfqimages=$this->rfqModel->getrfqmage([$val["rfq_id"]]);
                                            $order_data["reference_details"]=$dataset;
                                            $order_data["reference_details"]["images"]=[];
                                            foreach( $rfqimages as $val2){
                                                $order_data["reference_details"]["images"][]=["image_id"=>base64_encode($val2["image_id"]),"image_name"=>$val2["image"]];
                                            }
                                            
                                            
                                        }
                                    }
    
                            }else  if($proposal_data["refference_type"]=='enq'){
                                $search1=[];
                                $search1["enq_id"]=$proposal_data["refference_id"];
                
                                $search1["order_by"]=[["enq.date","DESC"]];
                                $response_data=[];
                                $this->load->model("enquiryModel");
                                $enquiry_data=current($this->enquiryModel->getEnquiryList($search1));
                                foreach( $enquiry_data as $enqVal){
                      
                     
                                    $enqdata=[];
                                    $enqdata["login_customer_id"] = base64_encode($data["id"]);
                                    $enqdata["enq_id"]=base64_encode($enqVal["enq_id"]);
                                    $enqdata["enq_to_id"]=base64_encode($enqVal["enq_to_id"]);
                                    $enqdata["enq_from_id"]=base64_encode($enqVal["enq_from_id"]);
                                    $enqdata["enq_seller_item_id"]=base64_encode($enqVal["enq_seller_item_id"]);
                  
                                    $enqdata["author_id"]=base64_encode($data["id"]);
                                    
                                    if($data["id"]!=$enqVal["enq_from_id"]){
                                      $enqdata["target_id"]=base64_encode($enqVal["enq_from_id"]);
                                      $enqdata["target_name"]=$enqVal["buyer_name"];
                                      if(trim($enqVal["from_business_name"])!=''){
                                          $enqdata["target_name"]=$enqVal["from_business_name"];
                                      }
                                      $enqdata["target_profile_image"]=$enqVal["buyer_profile_image"];
                                    }else{
                                      $enqdata["target_id"]=base64_encode($enqVal["enq_to_id"]);
                                       $enqdata["target_name"]=$enqVal["seller_name"];
                                      if(trim($enqVal["business_name"])!=''){
                                          $enqdata["target_name"]=$enqVal["business_name"];
                                      }
                                      $enqdata["target_profile_image"]=$enqVal["seller_profile_image"];
                                    }
                                    $search1=[];
                                    $search1["item_id"]=$enqVal["enq_seller_item_id"];
                                    $this->load->model("itemModel");
                                    $itemdetails=current($this->itemModel->getItem($search1));
                                    $enqdata["itemdata"]=[];
                                    if($data1["lang"]=='en'){
                                      $enqdata["itemdata"]["name"]=$itemdetails["item_name_en"];
                                    }else{
                                      $enqdata["itemdata"]["name"]=$itemdetails["item_name_fr"];
                                    }
                                    $enqdata["itemdata"]["thumbnail"]=$itemdetails["thumbnail"];
                                    $enqdata["buyer_name"]=$enqVal["buyer_name"];
                                     $enqdata["buyer_profile_image"]=$enqVal["buyer_profile_image"];
                                    $enqdata["seller_name"]=$enqVal["seller_name"];
                                    $enqdata["seller_business_name"]=$enqVal["business_name"];
                                    $enqdata["seller_address"]=$enqVal["address"];
                                    $enqdata["seller_country"]=$enqVal["country"];
                                    $enqdata["seller_state"]=$enqVal["state"];
                                    $enqdata["seller_city"]=$enqVal["city"];
                                    $enqdata["seller_profile_image"]=$enqVal["seller_profile_image"];
                                    $enqdata["enq_message"]=$enqVal["enq_message"];
                                    $enqdata["enq_date"]=date_format(date_create($enqVal["enq_date"]),"d/m/Y ");
                                    if($enqVal["enq_type"] == 1)
                                    {
                                      $enqdata["type"] = 'enq';
                                    }elseif($enqVal["enq_type"] == 2)
                                    {
                                      $enqdata["type"] = 'service_enquiry';
                                    }
                                     $enqdata["attachments"]=json_decode($enqVal["attachments"]);
                                     
                                     $order_data["reference_details"]=$enqdata;
                                  }
    
                            }
                            $proposal_data["refference_id"]=base64_encode($proposal_data["refference_id"]);
                            $proposal_data["id"]=base64_encode($proposal_data["id"]);
                            $proposal_data["from_id"]=base64_encode($proposal_data["from_id"]);
                            $proposal_data["to_id"]=base64_encode($proposal_data["to_id"]);
                            $order_data["status"]=$order_data["status"];
                            $order_data["total_amount"]=$order_data["total_amount"];
                            $order_data["payment_mode"]=$order_data["payment_mode"];
                            $order_data["item_details"]=json_decode($order_data["item_details"],true);
                            $order_data["buyer_info"]=json_decode($order_data["buyer_info"],true);
                            $proposal_data["created_date"]=date_format(date_create($proposal_data["created_date"]),"d/m/Y ");
                            $order_data["proposal_details"]=$proposal_data;
                            $status = parent::HTTP_OK;
                                   $response = ['status' => $status,'msg'=>' Order Data.',"order_details"=>$order_data];
                                   $this->response($response, $status);
                        }else{
                            $this->response(['msg' => 'Invalid Proposal','field'=>'proposal_id'], parent::HTTP_BAD_REQUEST); 
                        }
                    }else{
                        $order_data["status"]=$order_data["status"];
                         $order_data["from_name"]=$order_data["ofrom_name"];
                         $order_data["from_email"]=$order_data["ofrom_email"];
                         $order_data["from_phone"]=$order_data["ofrom_phone"];
                         $order_data["from_profile_image"]=$order_data["ofrom_profile_image"];
                          $order_data["from_business_name"]=$order_data["ofrom_business_name"];
                        $order_data["total_amount"]=$order_data["total_amount"];
                        $order_data["payment_mode"]=$order_data["payment_mode"];
                        $order_data["item_details"]=json_decode($order_data["item_details"],true);
                        $order_data["buyer_info"]=json_decode($order_data["buyer_info"],true);
                        $order_data["created_date"]=date_format(date_create($order_data["created_date"]),"d/m/Y ");
                        unset($order_data["ofrom_name"]);
                        unset($order_data["ofrom_email"]);
                        unset($order_data["ofrom_phone"]);
                        unset($order_data["ofrom_profile_image"]);
                         unset($order_data["ofrom_business_name"]);

			$order_data["buyer_id"]=base64_encode($order_data["user_id"]);
			$order_data["seller_id"]=base64_encode($order_data["seller_id"]);

                          //unset($order_data["user_id"]);
                           //unset($order_data["seller_id"]);
                        $status = parent::HTTP_OK;
                        $response = ['status' => $status,'msg'=>' Order Data.',"order_details"=>$order_data];
                        $this->response($response, $status);
                    }
                   

                }else{
                    $this->response(['msg' => 'Invalid Order','field'=>'order_id'], parent::HTTP_BAD_REQUEST); 
                }

            }
        }else{
            $status = parent::HTTP_UNAUTHORIZED;
            $response = ['status' => $status, 'msg' => 'Unauthorized Access! '];
            $this->response($response, $status);
        }  
    } catch (Exception $e) {

        $this->response(['msg' => 'server busy .Please try again some time.','field'=>'user_name'], parent::HTTP_NOT_FOUND);
      }        
   }
    public function confirmPayment_post(){
        try {
            $data1=$this->post();
            $data = $this->verify_request();
            if(!empty( $data)){
                if(!(isset($data1["order_id"]) && trim($data1["order_id"])!='' )){
                    $this->response(['msg' => 'Invalid Order','field'=>'order_id'], parent::HTTP_BAD_REQUEST);
                }else{
                    $search21=[];
                    $search21["order_id"]=$data1["order_id"];
                    $order_data=current($this->messagecenterModel->getOrderLog($search21));
                    if(!empty( $order_data)){
                        
                        $search21=[];
                        $search21["id"]=$order_data["proposal_id"];
                        $proposal_data=current($this->messagecenterModel->getProposal($search21));
                        if(!empty( $proposal_data)){

                            $updated_data=[];
                            $updated_data["status"]=(isset($data1["status"]))?$data1["status"]:$order_data["status"];
                            $updated_data["transction_id"]=(isset($data1["transction_id"]))?$data1["transction_id"]:$order_data["transction_id"];
                            $updated_data["updated_date"]=date("Y-m-d H:i:s");
                              $this->messagecenterModel->updateOrder($updated_data,["id"=>$data1["order_id"]]);
                               $this->messagecenterModel->updateProposal(["is_paid"=>1],["to_id"=>$proposal_data["to_id"],"from_id"=>$proposal_data["from_id"],"refference_id"=>$proposal_data["refference_id"],"refference_type"=>$proposal_data["refference_type"]]);
                               $status = parent::HTTP_OK;
                               $response = ['status' => $status,'msg'=>' payment successfully Done.'];
                               $this->response($response, $status);


                       }else{
                            $this->response(['msg' => 'Invalid Proposal','field'=>'proposal_id'], parent::HTTP_BAD_REQUEST); 
                        }

                     

                     
                    }else{
                        $this->response(['msg' => 'Invalid Order','field'=>'order_id'], parent::HTTP_BAD_REQUEST); 
                    }
                }
            }else{
                $status = parent::HTTP_UNAUTHORIZED;
                $response = ['status' => $status, 'msg' => 'Unauthorized Access! '];
                $this->response($response, $status);
            }        
        } catch (Exception $e) {

            $this->response(['msg' => 'server busy .Please try again some time.','field'=>'user_name'], parent::HTTP_NOT_FOUND);
          }      
    }
    public function processPayment_post(){
        try {
            $data1=$this->post();
            $data = $this->verify_request();
            if(!isset($data1["item_details"])){
                $data1["item_details"]='';
            }
            if(!isset($data1["buyer_info"])){
                $data1["buyer_info"]='';
            }
            if(!isset($data1["payment_mode"])){
                $data1["payment_mode"]=1;
            }
            if(!empty( $data)){
                if(!((isset($data1["proposal_id"]) && trim($data1["proposal_id"])!='' ) || (trim($data1["item_details"])!='' && trim($data1["buyer_info"])!='' ))){
                    $this->response(['msg' => 'Invalid request','field'=>'proposal_id'], parent::HTTP_BAD_REQUEST);
                }else{

                    if((isset($data1["proposal_id"]) && trim($data1["proposal_id"])!='' )){
                        $search21=[];
                        $search21["id"]=base64_decode($data1["proposal_id"]);
                        $proposal_data=current($this->messagecenterModel->getProposal($search21));
                        if(empty($proposal_data)){
                            $this->response(['msg' => 'Invalid Proposal','field'=>'proposal_id'], parent::HTTP_BAD_REQUEST);
                        }else{
                            if($proposal_data["to_id"]==$data["id"]){
                                $buyer_info=json_decode($data1["buyer_info"],true);
                                $item_details=json_decode($data1["item_details"],true);
                                $inserted_data=[];
                                $inserted_data["proposal_id"]=base64_decode($data1["proposal_id"]);
                                $inserted_data["status"]=1;
                                $inserted_data["buyer_info"]=json_encode($buyer_info);
                                $inserted_data["transction_id"]=time();
                                $inserted_data["payment_mode"]=$data1["payment_mode"];
                                $inserted_data["total_amount"]=$proposal_data["amount"];
                                $order_id=$this->messagecenterModel->addOrderLog( $inserted_data);
                                if($order_id){
                                    $status = parent::HTTP_OK;
                                    $response = ['status' => $status,'msg'=>'process payment successfully.','order_id'=>$order_id,'total_amount_paid'=>$inserted_data["total_amount"],"transction_id"=> $inserted_data["transction_id"]];
                                    $this->response($response, $status);
                                }else{
                                    $this->response(['msg' => 'Process payment failed due to network failure.Please try again some time ','field'=>'proposal_id'], parent::HTTP_BAD_REQUEST);
                                }
    
                            }else{
                                $status = parent::HTTP_UNAUTHORIZED;
                                $response = ['status' => $status, 'msg' => 'Unauthorized Access1! '];
                                $this->response($response, $status);
    
                            }
                        }
                    }else{
                        $inserted_data=[];
                        
                        $inserted_data["user_id"]=$data["id"];
                        $inserted_data["proposal_id"]=0;
                        $inserted_data["status"]=1;
                        $inserted_data["total_amount"]=0;
                        $inserted_data["payment_mode"]=$data1["payment_mode"];
                        $buyer_info=json_decode($data1["buyer_info"],true);
                        $item_details=json_decode($data1["item_details"],true);
                        $inserted_data["transction_id"]=time();
                        $inserted_data["buyer_info"]=json_encode($buyer_info);
                        $inserted_data["item_details"]=json_encode($item_details);
                         $inserted_data["seller_id"]=NULL;
                        $itemids=[];
                        $items=[];
                        foreach($item_details as $val){
                            $itemids[]=base64_decode($val["itemid"]);
                            $items[base64_decode($val["itemid"])]=$val;
                            $inserted_data["seller_id"]=base64_decode($val["seller_id"]);
                        }
                        $search=[];
                        $search["item_id_arr"]= $itemids;
                        $itemdata=$this->itemModel->getItem($search);
                      
                        foreach($itemdata as $val){
                         
                            $inserted_data["total_amount"]+=($val["fixedPrice"]*$items[$val["item_id"]]["quantity"]) ;
                        }
                        $order_id=$this->messagecenterModel->addOrderLog( $inserted_data);
                        if($order_id){
                            $status = parent::HTTP_OK;
                            $response = ['status' => $status,'msg'=>'process payment successfully.','order_id'=>$order_id,'total_amount_paid'=>$inserted_data["total_amount"],"transction_id"=> $inserted_data["transction_id"]];
                            $this->response($response, $status);
                        }else{
                            $this->response(['msg' => 'Process payment failed due to network failure.Please try again some time ','field'=>'proposal_id'], parent::HTTP_BAD_REQUEST);
                        }
                    }
                  
                }
            }else{
                $status = parent::HTTP_UNAUTHORIZED;
                $response = ['status' => $status, 'msg' => 'Unauthorized Access! '];
                $this->response($response, $status);
            }    
        } catch (Exception $e) {

            $this->response(['msg' => 'server busy .Please try again some time.','field'=>'user_name'], parent::HTTP_NOT_FOUND);
          }      
    }



    public function addProposal_post(){
        try {
            $data1=$this->post();
            if(!isset($data1["type"])){
                $data1["type"]='all';
            } 
            if(!isset($data1["lang"])){
                $data1["lang"]='en';
            }      
            $data = $this->verify_request();
           
                if(!empty( $data)){
                    if(!(isset($data1["refference_id"]) && trim($data1["refference_id"])!='' )){
                        $this->response(['msg' => 'Invalid Request','field'=>'refference_id'], parent::HTTP_BAD_REQUEST);
                    }else if(!(isset($data1["from_id"]) && trim($data1["from_id"])!='' )){
                        $this->response(['msg' => 'Invalid Request','field'=>'from_id'], parent::HTTP_BAD_REQUEST);
                    }else if(!(isset($data1["to_id"]) && trim($data1["to_id"])!='' )){
                        $this->response(['msg' => 'Invalid Request','field'=>'to_id'], parent::HTTP_BAD_REQUEST);
                    }else if(!(isset($data1["refference_type"]) && trim($data1["refference_type"])!='' )){
                        $this->response(['msg' => 'Invalid Request','field'=>'refference_type'], parent::HTTP_BAD_REQUEST);
                    }else if(!(isset($data1["title"]) && trim($data1["title"])!='' )){
                        $this->response(['msg' => 'Invalid Title','field'=>'title'], parent::HTTP_BAD_REQUEST);
                    }else if(!(isset($data1["amount"]) && trim($data1["amount"])!='' )){
                        $this->response(['msg' => 'Invalid Amount','field'=>'amount'], parent::HTTP_BAD_REQUEST);
                    }else if(trim($data1["amount"])!='' && !preg_match('/^(?:[1-9]\d*|0)?(?:\.\d+)?$/', $data1["amount"])){
                        $this->response(['msg' => 'Please Enter Valid Amount','field'=>'amount'], parent::HTTP_BAD_REQUEST);
                    }else if(!(isset($data1["currency"]) && trim($data1["currency"])!='' )){
                        $this->response(['msg' => 'Invalid Currency','field'=>'currency'], parent::HTTP_BAD_REQUEST);
                    }else if(!(isset($_FILES["file"]) && trim($_FILES["file"]["name"])!='' )){
                        $this->response(['msg' => 'Invalid File','field'=>'file'], parent::HTTP_BAD_REQUEST);
                    }else{
                        if($data1["refference_type"]=='rfq'){
                            $insered_data=[];
                            $search1=[];
                         $search1["rfq_id"]=base64_decode($data1["refference_id"]);
                        
                         $search1["group_by"]=["rfq.rfq_id"];
                         $rfqdata1=current($this->messagecenterModel->getRFQList($search1));
                            if(!empty($rfqdata1)){
                                if($rfqdata1["buyer_id"]!=$data["id"]){
                                    $insered_data["refference_id"]=base64_decode($data1["refference_id"]);
                                    $insered_data["from_id"]=base64_decode($data1["from_id"]);
                                    $insered_data["to_id"]=base64_decode($data1["to_id"]);
                                    $insered_data["refference_type"]=$data1["refference_type"];
                                    $insered_data["title"]=$data1["title"];
                                    $insered_data["description"]=$data1["description"];
                                    $insered_data["amount"]=$data1["amount"];
                                    $insered_data["currency"]=$data1["currency"];

                                    $insered_data["file"]='';
                                    $type=$_FILES["file"]["type"];
                                      $this->load->library('image_lib');
                                      
                                        if ((($_FILES["file"]["type"]== "image/gif")  || ($_FILES["file"]["type"] == "image/jpeg")  || ($_FILES["file"]["type"] == "image/jpg")  || ($_FILES["file"]["type"] == "image/png")) )
                                        {
                                           // File upload configuration
                                          $images=[];
                                          $uploadPath = '../admin/uploads/proposal/';
                                          $fileName = time().'-'.$_FILES["file"]["name"];
                                         
                                          if(move_uploaded_file($_FILES['file']['tmp_name'],  $uploadPath."".$fileName))
                                          {
                                               $insered_data["file"]=$fileName;
                                                $insert_id = $this->messagecenterModel->addProposal($insered_data);
                                                $status = parent::HTTP_OK;
                                                $response = ['status' => $status,'msg'=>'Proposal send successfully.'];
                                                $this->response($response, $status);
                                           }else{
                                            $this->response(['msg' => 'Send proposal failed due to invalid file ','field'=>'file'], parent::HTTP_BAD_REQUEST);
                                           
                                           }
                                                       
                                        }else if (($type == "application/pdf")) {
                                            $uploadPath = '../admin/uploads/proposal/';
                                          $fileName = time().'-'.$_FILES["file"]["name"];
                                            if ( move_uploaded_file($_FILES['file']['tmp_name'],  $uploadPath."".$fileName)){
                                            
                                                $insered_data["file"]=$fileName;
                                                $insert_id = $this->messagecenterModel->addProposal($insered_data);
                                                $status = parent::HTTP_OK;
                                                $response = ['status' => $status,'msg'=>'Proposal send successfully.'];
                                                $this->response($response, $status);
                                           }else{
                                            $this->response(['msg' => 'Send proposal failed due to invalid file ','field'=>'file'], parent::HTTP_BAD_REQUEST);
                                           
                                           }
                                
                                    }else if (($type == "application/msword") || ($type == "application/doc") || ($type == "application/vnd.openxmlformats-officedocument.wordprocessingml.document") ) {
                                        $uploadPath = '../admin/uploads/proposal/';
                                          $fileName = time().'-'.$_FILES["file"]["name"];
                                                    if ( move_uploaded_file($_FILES['file']['tmp_name'],  $uploadPath."".$fileName)){
                                                        
                                                        $insered_data["file"]=$fileName;
                                                        $insert_id = $this->messagecenterModel->addProposal($insered_data);
                                                        $status = parent::HTTP_OK;
                                                        $response = ['status' => $status,'msg'=>'Proposal send successfully.'];
                                                        $this->response($response, $status);
                                                }else{
                                                    $this->response(['msg' => 'Send proposal failed due to invalid file ','field'=>'file'], parent::HTTP_BAD_REQUEST);
                                                
                                                }
                                      }else if (($type == "application/msword") || ($type == "application/doc") || ($type == "application/vnd.openxmlformats-officedocument.wordprocessingml.document") ) {
                                        $uploadPath = '../admin/uploads/proposal/';
                                        $fileName = time().'-'.$_FILES["file"]["name"];
                                                    if ( move_uploaded_file($_FILES['file']['tmp_name'],  $uploadPath."".$fileName)){
                                                        
                                                        $insered_data["file"]=$fileName;
                                                        $insert_id = $this->messagecenterModel->addProposal($insered_data);
                                                        $status = parent::HTTP_OK;
                                                        $response = ['status' => $status,'msg'=>'Proposal send successfully.'];
                                                        $this->response($response, $status);
                                                }else{
                                                    $this->response(['msg' => 'Send proposal failed due to invalid file ','field'=>'file'], parent::HTTP_BAD_REQUEST);
                                                
                                                }
                                      }else{
                                            $this->response(['msg' => 'Send proposal failed due to invalid file  type','field'=>'file'], parent::HTTP_BAD_REQUEST);
                                        }
                                                   
                                      
                                    
                                   
                                }else{
                                    $this->response(['msg' => 'Invalid Request','field'=>'refference_id'], parent::HTTP_BAD_REQUEST);
                                }
                               
                            }else{
                                $this->response(['msg' => 'Invalid Request','field'=>'refference_id'], parent::HTTP_BAD_REQUEST);
                            }

                        }else if($data1["refference_type"]=='enq'){

                            $search1=[];
                            $search1["enq_id"]=base64_decode($data1["refference_id"]);
            
                            $search1["order_by"]=[["enq.date","DESC"]];
                            $response_data=[];
                            $this->load->model("enquiryModel");
                              $enquiry_data=current($this->enquiryModel->getEnquiryList($search1));
                              if(!empty($enquiry_data)){
                                 if($enquiry_data["enq_from_id"]!=$data["id"]){
                                    $insered_data["refference_id"]=base64_decode($data1["refference_id"]);
                                    $insered_data["from_id"]=base64_decode($data1["from_id"]);
                                    $insered_data["to_id"]=base64_decode($data1["to_id"]);
                                    $insered_data["refference_type"]=$data1["refference_type"];
                                    $insered_data["title"]=$data1["title"];
                                    $insered_data["description"]=$data1["description"];
                                    $insered_data["amount"]=$data1["amount"];
                                    $insered_data["currency"]=$data1["currency"];

                                    $insered_data["file"]='';
                                    $type=$_FILES["file"]["type"];
                                      $this->load->library('image_lib');
                                      
                                        if ((($_FILES["file"]["type"]== "image/gif")  || ($_FILES["file"]["type"] == "image/jpeg")  || ($_FILES["file"]["type"] == "image/jpg")  || ($_FILES["file"]["type"] == "image/png")) )
                                        {
                                           // File upload configuration
                                          $images=[];
                                          $uploadPath = '../admin/uploads/proposal/';
                                          $fileName = time().'-'.$_FILES["file"]["name"];
                                         
                                          if(move_uploaded_file($_FILES['file']['tmp_name'],  $uploadPath."".$fileName))
                                          {
                                               $insered_data["file"]=$fileName;
                                                $insert_id = $this->messagecenterModel->addProposal($insered_data);
                                                $status = parent::HTTP_OK;
                                                $response = ['status' => $status,'msg'=>'Proposal send successfully.'];
                                                $this->response($response, $status);
                                           }else{
                                            $this->response(['msg' => 'Send proposal failed due to invalid file ','field'=>'file'], parent::HTTP_BAD_REQUEST);
                                           
                                           }
                                                       
                                        }else if (($type == "application/pdf")) {
                                            $uploadPath = '../admin/uploads/proposal/';
                                          $fileName = time().'-'.$_FILES["file"]["name"];
                                            if ( move_uploaded_file($_FILES['file']['tmp_name'],  $uploadPath."".$fileName)){
                                            
                                                $insered_data["file"]=$fileName;
                                                $insert_id = $this->messagecenterModel->addProposal($insered_data);
                                                $status = parent::HTTP_OK;
                                                $response = ['status' => $status,'msg'=>'Proposal send successfully.'];
                                                $this->response($response, $status);
                                           }else{
                                            $this->response(['msg' => 'Send proposal failed due to invalid file ','field'=>'file'], parent::HTTP_BAD_REQUEST);
                                           
                                           }
                                
                                    }else if (($type == "application/msword") || ($type == "application/doc") || ($type == "application/vnd.openxmlformats-officedocument.wordprocessingml.document") ) {
                                        $uploadPath = '../admin/uploads/proposal/';
                                          $fileName = time().'-'.$_FILES["file"]["name"];
                                                    if ( move_uploaded_file($_FILES['file']['tmp_name'],  $uploadPath."".$fileName)){
                                                        
                                                        $insered_data["file"]=$fileName;
                                                        $insert_id = $this->messagecenterModel->addProposal($insered_data);
                                                        $status = parent::HTTP_OK;
                                                        $response = ['status' => $status,'msg'=>'Proposal send successfully.'];
                                                        $this->response($response, $status);
                                                }else{
                                                    $this->response(['msg' => 'Send proposal failed due to invalid file ','field'=>'file'], parent::HTTP_BAD_REQUEST);
                                                
                                                }
                                      }else if (($type == "application/msword") || ($type == "application/doc") || ($type == "application/vnd.openxmlformats-officedocument.wordprocessingml.document") ) {
                                        $uploadPath = '../admin/uploads/proposal/';
                                        $fileName = time().'-'.$_FILES["file"]["name"];
                                                    if ( move_uploaded_file($_FILES['file']['tmp_name'],  $uploadPath."".$fileName)){
                                                        
                                                        $insered_data["file"]=$fileName;
                                                        $insert_id = $this->messagecenterModel->addProposal($insered_data);
                                                        $status = parent::HTTP_OK;
                                                        $response = ['status' => $status,'msg'=>'Proposal send successfully.'];
                                                        $this->response($response, $status);
                                                }else{
                                                    $this->response(['msg' => 'Send proposal failed due to invalid file ','field'=>'file'], parent::HTTP_BAD_REQUEST);
                                                
                                                }
                                      }else{
                                            $this->response(['msg' => 'Send proposal failed due to invalid file  type','field'=>'file'], parent::HTTP_BAD_REQUEST);
                                        }
                                                   
                                      
                                    
                                   
                                }else{
                                    $this->response(['msg' => 'Invalid Request','field'=>'refference_id'], parent::HTTP_BAD_REQUEST);
                                }
                               
                            }else{
                                $this->response(['msg' => 'Invalid Request','field'=>'refference_id'], parent::HTTP_BAD_REQUEST);
                            }

                        }else{
                            $this->response(['msg' => 'Invalid Request','field'=>'refference_id'], parent::HTTP_BAD_REQUEST);
                        }
                     

                    }
                }else{
                    $status = parent::HTTP_UNAUTHORIZED;
                    $response = ['status' => $status, 'msg' => 'Unauthorized Access! '];
                    $this->response($response, $status);
                }
        } catch (Exception $e) {

            $this->response(['msg' => 'server busy .Please try again some time.','field'=>'user_name'], parent::HTTP_NOT_FOUND);
          }   
    }
    public function getMessageList_post(){
        try {

            $data=[];
           $result=[];
           $data1=$this->post();
            if(!isset($data1["type"])){
                $data1["type"]='all';
            } 
            if(!isset($data1["lang"])){
                $data1["lang"]='en';
            }           
           $data = $this->verify_request();
           
           if(!empty( $data)){
            if($data1["type"]=='rfq' || $data1["type"]=='all'){
                if($data["supplier_YN"]==0){
                    $search1=[];
                     $search1["buyer_id"]=$data["id"];
                     $search1["to_customer_id"]=$data["id"];
                    $search1["group_by"]=["th.from_customer_id","rfq.rfq_id"];
                    $search1["order_by"]=[["th.is_seen","ASC"],["th.date","DESC"],["rfq.created_date","DESC"]];
                    $rfqdata=$this->messagecenterModel->getRFQListOneToOne($search1);
                    $ids=[];
                        foreach($rfqdata as $val){
                            $dataset=[];
                            $ids[]=$val["rfq_id"];
                            $dataset["rfq_id"]=base64_encode($val["rfq_id"]);
                            $dataset["buyer_id"]=base64_encode($val["buyer_id"]);
                            $dataset["cat_id"]=base64_encode($val["cat_id"]);
                            $dataset["uom_id"]=base64_encode($val["uom_id"]);
                            $dataset["author_id"]=base64_encode($val["to_customer_id"]);
                            $dataset["target_id"]=base64_encode($val["from_customer_id"]);
                            $dataset["to_id"]=base64_encode($val["to_customer_id"]);
                            $dataset["from_id"]=base64_encode($val["from_customer_id"]);
                            $dataset["from_name"]=$val["fromcust_name"];
                            $dataset["target_name"]=$val["fromcust_name"];
                            if(trim($val["from_business_name"])!=''){
                                $dataset["target_name"]=$val["from_business_name"];
                            }

                            
                            $dataset["target_profile_image"]=$val["fromcust_profile_image"];

                            $dataset["quantity"]=$val["quantity"];
                            $dataset["message"]=$val["message"];
                            $dataset["other_requerment"]=$val["other_requerment"];
                            $dataset["qrname"]=$val["qrname"];
                            $dataset["rfqlocation"]=$val["rfqlocation"];
                            $dataset["latitudes"]=$val["latitudes"];
                            $dataset["longitude"]=$val["longitude"];
                            $dataset["business_logo"]=$val["business_logo"];
                            $dataset["buyer_name"]=$val["name"];
                            $dataset["buyer_email"]=$val["email"];
                            $dataset["buyer_phone"]=$val["phone"];
                            $dataset["created_date"]=date_format(date_create($val["created_date"]),"d/m/Y");
                            
                            
                            if($val["date"]!=''){
                                $dataset["last_message_date"]=date_format(date_create($val["date"]),"d/m/Y h:i:s a ");
                                $dataset["is_seen"]=$val["is_seen"];
                                $dataset["last_message_date1"]=date_create($val["date"]);
                                    
                            }else{
                                $dataset["last_message_date"]='';
                                $dataset["is_seen"]='';
                                $dataset["last_message_date1"]=date_create($val["created_date"]);
                            }
                            $dataset["type"]="rfq";
                            $dataset["images"]=[];
                            if($data1["lang"]=='en'){
                                $dataset["category_name"]=$val["category_name_en"];
                                $dataset["uom_name"]=$val["uom_name_en"];
                            }else{
                                $dataset["category_name"]=$val["category_name_fr"];
                                $dataset["uom_name"]=$val["uom_name_fr"];
                            }
                            $searchUnread = [];
                            $searchUnread["rfq_id"] = $val["rfq_id"];
                            $searchUnread["to_customer_id"] = $data["id"];
                            $searchUnread["from_customer_id"] = $val["from_customer_id"];
                            $rfq_unread_message_data=$this->messagecenterModel->getRfqUnreadSingle($searchUnread);
                            $searchUnread["total_message"] = 'ALL';
                            $dataset["total_message"] = $this->messagecenterModel->getRfqUnreadSingle($searchUnread);

                            $dataset["unread"]=$rfq_unread_message_data;
                            $result[]=$dataset;
                        }
                        
                       
                }else{
                    $search1=[];
                     $search1["buyer_id"]=$data["id"];
                     $search1["to_customer_id"]=$data["id"];
                    $search1["group_by"]=["th.from_customer_id","rfq.rfq_id"];
                    $search1["order_by"]=[["th.is_seen","ASC"],["th.date","DESC"],["rfq.created_date","DESC"]];
                    $rfqdata=$this->messagecenterModel->getRFQListOneToOne($search1);
                    $ids=[];
                        foreach($rfqdata as $val){
                            $dataset=[];
                            $ids[]=$val["rfq_id"];
                            $dataset["rfq_id"]=base64_encode($val["rfq_id"]);
                            $dataset["buyer_id"]=base64_encode($val["buyer_id"]);
                            $dataset["cat_id"]=base64_encode($val["cat_id"]);
                            $dataset["uom_id"]=base64_encode($val["uom_id"]);
                            $dataset["author_id"]=base64_encode($val["to_customer_id"]);
                            $dataset["target_id"]=base64_encode($val["from_customer_id"]);
                            $dataset["to_id"]=base64_encode($val["to_customer_id"]);
                            $dataset["from_id"]=base64_encode($val["from_customer_id"]);
                            $dataset["from_name"]=$val["fromcust_name"];
                            $dataset["target_name"]=$val["fromcust_name"];
                            if(trim($val["from_business_name"])!=''){
                                $dataset["target_name"]=$val["from_business_name"];
                            }

                            
                            $dataset["target_profile_image"]=$val["fromcust_profile_image"];

                            $dataset["quantity"]=$val["quantity"];
                            $dataset["message"]=$val["message"];
                            $dataset["other_requerment"]=$val["other_requerment"];
                            $dataset["qrname"]=$val["qrname"];
                            $dataset["rfqlocation"]=$val["rfqlocation"];
                            $dataset["latitudes"]=$val["latitudes"];
                            $dataset["longitude"]=$val["longitude"];
                            $dataset["business_logo"]=$val["business_logo"];
                            $dataset["buyer_name"]=$val["name"];
                            $dataset["buyer_email"]=$val["email"];
                            $dataset["buyer_phone"]=$val["phone"];
                            $dataset["created_date"]=date_format(date_create($val["created_date"]),"d/m/Y");
                            
                            
                            if($val["date"]!=''){
                                $dataset["last_message_date"]=date_format(date_create($val["date"]),"d/m/Y h:i:s a ");
                                $dataset["is_seen"]=$val["is_seen"];
                                $dataset["last_message_date1"]=date_create($val["date"]);
                                    
                            }else{
                                $dataset["last_message_date"]='';
                                $dataset["is_seen"]='';
                                $dataset["last_message_date1"]=date_create($val["created_date"]);
                            }
                            $dataset["type"]="rfq";
                            $dataset["images"]=[];
                            if($data1["lang"]=='en'){
                                $dataset["category_name"]=$val["category_name_en"];
                                $dataset["uom_name"]=$val["uom_name_en"];
                            }else{
                                $dataset["category_name"]=$val["category_name_fr"];
                                $dataset["uom_name"]=$val["uom_name_fr"];
                            }
                            $searchUnread = [];
                            $searchUnread["rfq_id"] = $val["rfq_id"];
                            $searchUnread["to_customer_id"] = $data["id"];
                            $searchUnread["from_customer_id"] = $val["from_customer_id"];
                            $rfq_unread_message_data=$this->messagecenterModel->getRfqUnreadSingle($searchUnread);
                            $searchUnread["total_message"] = 'ALL';
                            $dataset["total_message"] = $this->messagecenterModel->getRfqUnreadSingle($searchUnread);

                            $dataset["unread"]=$rfq_unread_message_data;
                            $result[]=$dataset;
                        }

                       
                    $search1=[];
                     $search1["from_customer_id"]=$data["id"];
                     $search1["buyer_id_not"]=$data["id"];
                    $search1["group_by"]=["th.from_customer_id","rfq.rfq_id"];
                    $search1["order_by"]=[["th.is_seen","ASC"],["th.date","DESC"],["rfq.created_date","DESC"]];
                    $rfqdata=$this->messagecenterModel->getRFQListOneToOne($search1);
                   
                    $ids=[];
                        foreach($rfqdata as $val){
                            $dataset=[];
                            $ids[]=$val["rfq_id"];
                            $dataset["rfq_id"]=base64_encode($val["rfq_id"]);
                            $dataset["buyer_id"]=base64_encode($val["buyer_id"]);
                            $dataset["cat_id"]=base64_encode($val["cat_id"]);
                            $dataset["uom_id"]=base64_encode($val["uom_id"]);
                            $dataset["author_id"]=base64_encode($val["from_customer_id"]);
                            $dataset["target_id"]=base64_encode($val["to_customer_id"]);
                            $dataset["to_id"]=base64_encode($val["to_customer_id"]);
                            $dataset["from_id"]=base64_encode($val["from_customer_id"]);
                            $dataset["from_name"]=$val["fromcust_name"];
                            $dataset["target_name"]=$val["tocust_name"];
                            if(trim($val["to_business_name"])!=''){
                                $dataset["target_name"]=$val["to_business_name"];
                            }
                            $dataset["target_profile_image"]=$val["tocust_profile_image"];
                            $dataset["quantity"]=$val["quantity"];
                            $dataset["message"]=$val["message"];
                            $dataset["other_requerment"]=$val["other_requerment"];
                            $dataset["qrname"]=$val["qrname"];
                            $dataset["rfqlocation"]=$val["rfqlocation"];
                            $dataset["latitudes"]=$val["latitudes"];
                            $dataset["longitude"]=$val["longitude"];
                            $dataset["business_logo"]=$val["business_logo"];
                            $dataset["buyer_name"]=$val["name"];
                            $dataset["buyer_email"]=$val["email"];
                            $dataset["buyer_phone"]=$val["phone"];
                            $dataset["created_date"]=date_format(date_create($val["created_date"]),"d/m/Y");
                            
                            
                            if($val["date"]!=''){
                                $dataset["last_message_date"]=date_format(date_create($val["date"]),"d/m/Y h:i:s a ");
                                $dataset["is_seen"]=$val["is_seen"];
                                $dataset["last_message_date1"]=date_create($val["date"]);
                                    
                            }else{
                                $dataset["last_message_date"]='';
                                $dataset["is_seen"]='';
                                $dataset["last_message_date1"]=date_create($val["created_date"]);
                            }
                            $dataset["type"]="rfq";
                            $dataset["images"]=[];
                            if($data1["lang"]=='en'){
                                $dataset["category_name"]=$val["category_name_en"];
                                $dataset["uom_name"]=$val["uom_name_en"];
                            }else{
                                $dataset["category_name"]=$val["category_name_fr"];
                                $dataset["uom_name"]=$val["uom_name_fr"];
                            }
                            $searchUnread = [];
                            $searchUnread["rfq_id"] = $val["rfq_id"];
                            $searchUnread["from_customer_id"] = $data["id"];
                            $searchUnread["to_customer_id"] = $val["to_customer_id"];
                            $rfq_unread_message_data=$this->messagecenterModel->getRfqUnreadSingle($searchUnread);
                            $searchUnread["total_message"] = 'ALL';
                            $dataset["total_message"] = $this->messagecenterModel->getRfqUnreadSingle($searchUnread);
                            $dataset["unread"]=$rfq_unread_message_data;
                            $result[]=$dataset;
                        }
                    $itemdata=$this->itemModel->getItem(["supp_id"=>$data["id"]]);
                    $cat_ids=implode(",",array_column($itemdata,"category_id"));
                    if(empty($cat_ids)){
                        $cat_ids="-1";
                    }
                    $search1=[];
                    $search1["cat_ids_buyer"]=$cat_ids;
                    $search1["thnull"]=1;
                    
                    $search1["group_by"]=["rfq.rfq_id"];
                         $search1["order_by"]=[["rfq.created_date","DESC"]];
                    $rfqdata2=$this->messagecenterModel->getRFQList($search1);
                     foreach($rfqdata2 as $val3){
                        $dataset=[];
                        $ids[]=$val3["rfq_id"];
                        $dataset["rfq_id"]=base64_encode($val3["rfq_id"]);
                        $dataset["buyer_id"]=base64_encode($val3["buyer_id"]);
                        $dataset["cat_id"]=base64_encode($val3["cat_id"]);
                        $dataset["uom_id"]=base64_encode($val3["uom_id"]);
                        $dataset["author_id"]=base64_encode($data["id"]);
                        $dataset["target_id"]=base64_encode($val3["buyer_id"]);
                        $dataset["to_id"]=base64_encode($val3["buyer_id"]);
                        $dataset["from_id"]=base64_encode($data["id"]);
                        $dataset["from_name"]=$data["name"];
                        $dataset["target_name"]=$val3["name"];

                        if(trim($val3["from_business_name"])!=''){
                            $dataset["target_name"]=$val3["from_business_name"];
                        }
                        $dataset["target_profile_image"]=$val3["business_logo"];
                         $dataset["quantity"]=$val3["quantity"];
                        $dataset["message"]=$val3["message"];
                        $dataset["other_requerment"]=$val3["other_requerment"];
                        $dataset["qrname"]=$val3["qrname"];
                        $dataset["rfqlocation"]=$val3["rfqlocation"];
                        $dataset["latitudes"]=$val3["latitudes"];
                        $dataset["longitude"]=$val3["longitude"];
                        $dataset["business_logo"]=$val3["business_logo"];
                        $dataset["buyer_name"]=$val3["name"];
                        $dataset["buyer_email"]=$val3["email"];
                        $dataset["buyer_phone"]=$val3["phone"];
                        $dataset["created_date"]=date_format(date_create($val3["created_date"]),"d/m/Y");
                        
                        $dataset["last_message_date"]='';
                        $dataset["is_seen"]='';
                        $dataset["last_message_date1"]=date_create($val3["created_date"]);
                        $dataset["type"]="rfq";
                        $dataset["images"]=[];
                        if($data1["lang"]=='en'){
                            $dataset["category_name"]=$val3["category_name_en"];
                            $dataset["uom_name"]=$val3["uom_name_en"];
                        }else{
                            $dataset["category_name"]=$val3["category_name_fr"];
                            $dataset["uom_name"]=$val3["uom_name_fr"];
                        }
                      
                         $dataset["total_message"] =0;

                        $dataset["unread"]=0;
                        $result[]=$dataset;
                    }

                   
                   
                }
              
               

               
            }
            
            $result=array_values($result);
            if($data1["type"]=='enq' || $data1["type"]=='all'){

                $search1=[];
                $search1["customer_id"]=$data["id"];

                $search1["order_by"]=[["enq.date","DESC"]];
                $response_data=[];
                $this->load->model("enquiryModel");
                  $enquiry_data=$this->enquiryModel->getEnquiryList($search1);
                  foreach( $enquiry_data as $enqVal){
                  
                 
                  $enqdata=[];
                  $enqdata["login_customer_id"] = base64_encode($data["id"]);
                  $enqdata["enq_id"]=base64_encode($enqVal["enq_id"]);
                  $enqdata["enq_to_id"]=base64_encode($enqVal["enq_to_id"]);
                  $enqdata["enq_from_id"]=base64_encode($enqVal["enq_from_id"]);
                  $enqdata["enq_seller_item_id"]=base64_encode($enqVal["enq_seller_item_id"]);

                  $enqdata["author_id"]=base64_encode($data["id"]);
                  
                  if($data["id"]!=$enqVal["enq_from_id"]){
                    $enqdata["target_id"]=base64_encode($enqVal["enq_from_id"]);
                    $enqdata["target_name"]=$enqVal["buyer_name"];
                    if(trim($enqVal["from_business_name"])!=''){
                        $enqdata["target_name"]=$enqVal["from_business_name"];
                    }
                    $enqdata["target_profile_image"]=$enqVal["buyer_profile_image"];
                  }else{
                    $enqdata["target_id"]=base64_encode($enqVal["enq_to_id"]);
                     $enqdata["target_name"]=$enqVal["seller_name"];
                    if(trim($enqVal["business_name"])!=''){
                        $enqdata["target_name"]=$enqVal["business_name"];
                    }
                    $enqdata["target_profile_image"]=$enqVal["seller_profile_image"];
                  }
                  $search1=[];
                  $search1["item_id"]=$enqVal["enq_seller_item_id"];
                  $this->load->model("itemModel");
                  $itemdetails=current($this->itemModel->getItem($search1));
                  $enqdata["itemdata"]=[];
                  if($data1["lang"]=='en'){
                    $enqdata["itemdata"]["name"]=$itemdetails["item_name_en"];
                  }else{
                    $enqdata["itemdata"]["name"]=$itemdetails["item_name_fr"];
                  }
                  $enqdata["itemdata"]["thumbnail"]=$itemdetails["thumbnail"];
                  $enqdata["buyer_name"]=$enqVal["buyer_name"];
                   $enqdata["buyer_profile_image"]=$enqVal["buyer_profile_image"];
                  $enqdata["seller_name"]=$enqVal["seller_name"];
                  $enqdata["seller_business_name"]=$enqVal["business_name"];
                  $enqdata["seller_address"]=$enqVal["address"];
                  $enqdata["seller_country"]=$enqVal["country"];
                  $enqdata["seller_state"]=$enqVal["state"];
                  $enqdata["seller_city"]=$enqVal["city"];
                  $enqdata["seller_profile_image"]=$enqVal["seller_profile_image"];
                  $enqdata["enq_message"]=$enqVal["enq_message"];
                  $enqdata["enq_date"]=date_format(date_create($enqVal["enq_date"]),"d/m/Y ");
                  if($enqVal["enq_type"] == 1)
                  {
                    $enqdata["type"] = 'enq';
                  }elseif($enqVal["enq_type"] == 2)
                  {
                    $enqdata["type"] = 'service_enquiry';
                  }
                   $enqdata["last_message_date1"]=date_create($enqVal["enq_date"]);
                   $enqdata["attachments"]=json_decode($enqVal["attachments"]);
                   $searchUnread = [];
                   $searchUnread["enq_id"] = $enqVal["enq_id"];
                   $searchUnread["to_customer_id"] = $data["id"];
                   $searchUnread["from_customer_id"] = base64_decode($enqdata["target_id"]);
                   $enquiry_unread_message_data=$this->messagecenterModel->getEnquiryUnreadSingle($searchUnread);
                   $enqdata["unread"]=$enquiry_unread_message_data;
                  
                   $searchUnread["total_message"] = 'ALL';
                   $enqdata["total_message"] = $this->messagecenterModel->getEnquiryUnreadSingle($searchUnread);
                   $result[]=$enqdata;
                }
            }
            $sort = array();
                foreach($result as $k=>$v) {
                    $sort['unread'][$k] = $v['unread'];
                     $sort['last_message_date1'][$k] = $v['last_message_date1'];
                  
                }
                if(!empty($result)){
                  # sort by event_type desc and then title asc
                array_multisort($sort['unread'], SORT_DESC,$sort['last_message_date1'], SORT_DESC,$result);

                }
                

                $status = parent::HTTP_OK;
                                  $response = ['status' => $status,'msg'=>'message data.','response_data'=>$result];
                                  $this->response($response, $status);
           }else{
            $status = parent::HTTP_UNAUTHORIZED;
            $response = ['status' => $status, 'msg' => 'Unauthorized Access! '];
            $this->response($response, $status);
           }
        } catch (Exception $e) {

            $this->response(['msg' => 'server busy .Please try again some time.','field'=>'user_name'], parent::HTTP_NOT_FOUND);
          }    
    }

    public function getMessageDetails_post(){
        try{
            $response_data=[];
            $quatation_details=[];
            if(!isset($data1["lang"])){
                $data1["lang"]='en';
            } 
           $data1=$this->post();
           $data = $this->verify_request();
            if(!empty( $data)){
                if(!(isset($data1["type"]) && trim($data1["type"])!='' )){
                    $this->response(['msg' => 'Invalid Type','field'=>'type'], parent::HTTP_BAD_REQUEST);
                }else if(!(isset($data1["rfq_id"]) && trim($data1["rfq_id"])!='' )){
                    $this->response(['msg' => 'Invalid Request','field'=>'rfq_id'], parent::HTTP_BAD_REQUEST);
                }else if(!(isset($data1["author_id"]) && trim($data1["author_id"])!='' )){
                    $this->response(['msg' => 'Invalid Request','field'=>'author_id'], parent::HTTP_BAD_REQUEST);
                }else if(!(isset($data1["target_id"]) && trim($data1["target_id"])!='' )){
                    $this->response(['msg' => 'Invalid Request','field'=>'target_id'], parent::HTTP_BAD_REQUEST);
                }else{
                    $result=[];
                    $result["message"]=[];
                    $result["quatation"]=[];
                    $result["proposal"]='';
                   

                    if((isset($data1["type"]) && isset($data1["rfq_id"]) && trim($data1["rfq_id"])!='')  && ($data1["type"]=='rfq')){
                       

                        $search=[];
                        $search1["rfq_id"]=base64_decode($data1["rfq_id"]);
                        $search1["to_customer_ids"]=base64_decode($data1["author_id"]);
                        $search1["from_customer_ids"]=base64_decode($data1["target_id"]);
                        $search1["group_by"]=["th.id"];
                        $search1["order_by"]=[["th.date","ASC"]];
                       if(isset($data1["is_seen"])){
                        $search1["is_seen"]=$data1["is_seen"];
                       }
                         $rfq_message_data1=$this->messagecenterModel->getRFQListOneToOne($search1);
                         foreach($rfq_message_data1 as $val12){
                            $rdata=[];
                            $rdata["rfq_id"]=base64_encode($val12["rfq_id"]);
                            $rdata["rfq_buyer_id"]=base64_encode($val12["buyer_id"]);
                            $rdata["buyer_name"]=$val12["name"];
                            $rdata["buyer_profile_image"]=$val12["business_logo"];
    
                            $rdata["to_customer_id"]=base64_encode($val12["to_customer_id"]);
                            $rdata["tocust_name"]=$val12["tocust_name"];
                            $rdata["tocust_profile_image"]=$val12["tocust_profile_image"];
    
                            $rdata["from_customer_id"]=base64_encode($val12["from_customer_id"]);
                            $rdata["fromcust_name"]=$val12["fromcust_name"];
                            $rdata["fromcust_profile_image"]=$val12["fromcust_profile_image"];
    
                            $rdata["message"]=$val12["thread_message"];
                            $rdata["image"]=$val12["images"];
                            
                            $rdata["date"]=date_format(date_create($val12["date"]),"d/m/Y h:i:s a ");
    
                            $rdata["ismine"]='from_customer_id';
                            if($val12["to_customer_id"]==$data["id"]){
                                $rdata["ismine"]='to_customer_id';
                            }
                            $rdata["is_seen"]=$val12["is_seen"];
                            $result["message"][]=$rdata;
                         }
                         $search=[];
                         $search1["rfq_id"]=base64_decode($data1["rfq_id"]);
                        
                         $search1["group_by"]=["rfq.rfq_id"];
                        $rfqdata1=$this->messagecenterModel->getRFQList($search1);
                        $search21=[];
                        $search21["refference_id"]=base64_decode($data1["rfq_id"]);
                        $search21["refference_type"]='rfq';
                        $search21["from_id"]=base64_decode($data1["author_id"]);
                        $search21["to_id"]=base64_decode($data1["target_id"]);
                          $proposal_data=current($this->messagecenterModel->getProposal($search21));
                        if(!empty($proposal_data)){
                            
                            $proposal_data["refference_id"]=base64_encode($proposal_data["refference_id"]);
                            $proposal_data["id"]=base64_encode($proposal_data["id"]);
                            $proposal_data["from_id"]=base64_encode($proposal_data["from_id"]);
                            $proposal_data["to_id"]=base64_encode($proposal_data["to_id"]);
                            $proposal_data["created_date"]=date_format(date_create($proposal_data["created_date"]),"d/m/Y ");
                            $result["proposal"]=$proposal_data;
                        }
                        foreach($rfqdata1 as $val){
                            $search3=[];
                            $search3["rfq_id"]=base64_decode($data1["rfq_id"]);
                           
                            $dataset=[];
                            $ids[]=$val["rfq_id"];
                            $dataset["rfq_id"]=base64_encode($val["rfq_id"]);
                            $dataset["buyer_id"]=base64_encode($val["buyer_id"]);
                            $dataset["cat_id"]=base64_encode($val["cat_id"]);
                            $dataset["uom_id"]=base64_encode($val["uom_id"]);
                            $dataset["quantity"]=$val["quantity"];
                            $dataset["message"]=$val["message"];
                            $dataset["other_requerment"]=$val["other_requerment"];
                            $dataset["qrname"]=$val["qrname"];
                            $dataset["rfqlocation"]=$val["rfqlocation"];
                            $dataset["latitudes"]=$val["latitudes"];
                            $dataset["longitude"]=$val["longitude"];
                            $dataset["business_logo"]=$val["business_logo"];
                             $dataset["buyer_name"]=$val["name"];
                            $dataset["buyer_email"]=$val["email"];
                            $dataset["buyer_phone"]=$val["phone"];
                            $dataset["created_date"]=date_format(date_create($val["created_date"]),"d/m/Y");
                            $dataset["is_paid"]=0;
                            if(!empty($proposal_data)){
                                $dataset["is_paid"]=$proposal_data["is_paid"];
                            }
                            $dataset["is_sent_proposal_permission"]=0;
                            if($val["buyer_id"]!=$data["id"]){
                                $dataset["is_sent_proposal_permission"]=1;
                            }
                            $dataset["type"]="rfq";
                            $dataset["images"]=[];
                            if($data1["lang"]=='en'){
                                $dataset["category_name"]=$val["category_name_en"];
                                $dataset["uom_name"]=$val["uom_name_en"];
                            }else{
                                $dataset["category_name"]=$val["category_name_fr"];
                                $dataset["uom_name"]=$val["uom_name_fr"];
                            }
                           
                           
                            $result["quatation"]=$dataset;
                        }
                        
                        if(empty($ids)){
                            $ids=['-1'];
                        }
                        $rfqimages=$this->rfqModel->getrfqmage($ids);
                        foreach( $rfqimages as $val2){
                            $result["quatation"]["images"][]=["image_id"=>base64_encode($val2["image_id"]),"image_name"=>$val2["image"]];
                        }
                      
                    }else  if((isset($data1["type"]) && isset($data1["rfq_id"]) && trim($data1["rfq_id"])!='')  && ($data1["type"]=='enq')){

                        $search1=[];
                        $search1["enq_id"]=base64_decode($data1["rfq_id"]);
        
                        $search1["order_by"]=[["enq.date","DESC"]];
                        $response_data=[];
                        $this->load->model("enquiryModel");
                          $enquiry_data=$this->enquiryModel->getEnquiryList($search1);
                          $search21=[];
                        $search21["refference_id"]=base64_decode($data1["rfq_id"]);
                        $search21["refference_type"]='rfq';
                        $search21["from_id"]=base64_decode($data1["author_id"]);
                        $search21["to_id"]=base64_decode($data1["target_id"]);
                          $proposal_data=current($this->messagecenterModel->getProposal($search21));
                        if(!empty($proposal_data)){
                            $proposal_data["id"]=base64_encode($proposal_data["id"]);
                            $proposal_data["refference_id"]=base64_encode($proposal_data["refference_id"]);
                            $proposal_data["from_id"]=base64_encode($proposal_data["from_id"]);
                            $proposal_data["to_id"]=base64_encode($proposal_data["to_id"]);
                            $proposal_data["created_date"]=date_format(date_create($proposal_data["created_date"]),"d/m/Y ");
                            $result["proposal"]=$proposal_data;
                        }
                            foreach( $enquiry_data as $enqVal){
                                    $enqdata=[];
                                    $enqdata["enq_id"]=base64_encode($enqVal["enq_id"]);
                                    $enqdata["enq_to_id"]=base64_encode($enqVal["enq_to_id"]);
                                    $enqdata["enq_from_id"]=base64_encode($enqVal["enq_from_id"]);
                                    $enqdata["enq_seller_item_id"]=base64_encode($enqVal["enq_seller_item_id"]);
                    
                                    $enqdata["author_id"]=base64_encode($data["id"]);
                                    
                                    if($data["id"]!=$enqVal["enq_from_id"]){
                                        $enqdata["target_id"]=base64_encode($enqVal["enq_from_id"]);
                                        $enqdata["target_name"]=$enqVal["buyer_name"];
                                        if(trim($enqVal["from_business_name"])!=''){
                                            $enqdata["target_name"]=$enqVal["from_business_name"];
                                        }
                                        $enqdata["target_profile_image"]=$enqVal["buyer_profile_image"];
                                    }else{
                                        $enqdata["target_id"]=base64_encode($enqVal["enq_to_id"]);
                                        $enqdata["target_name"]=$enqVal["seller_name"];
                                        if(trim($enqVal["business_name"])!=''){
                                            $enqdata["target_name"]=$enqVal["business_name"];
                                        }
                                        $enqdata["target_profile_image"]=$enqVal["seller_profile_image"];
                                    }
                                    $search1=[];
                                    $search1["item_id"]=$enqVal["enq_seller_item_id"];
                                    $this->load->model("itemModel");
                                    $itemdetails=current($this->itemModel->getItem($search1));
                                    $enqdata["itemdata"]=[];
                                    if($data1["lang"]=='en'){
                                        $enqdata["itemdata"]["name"]=$itemdetails["item_name_en"];
                                    }else{
                                        $enqdata["itemdata"]["name"]=$itemdetails["item_name_fr"];
                                    }
                                    $enqdata["itemdata"]["thumbnail"]=$itemdetails["thumbnail"];
                                    $enqdata["buyer_name"]=$enqVal["buyer_name"];
                                    $enqdata["buyer_profile_image"]=$enqVal["buyer_profile_image"];
                                    $enqdata["seller_name"]=$enqVal["seller_name"];
                                    $enqdata["seller_business_name"]=$enqVal["business_name"];
                                    $enqdata["seller_address"]=$enqVal["address"];
                                    $enqdata["seller_country"]=$enqVal["country"];
                                    $enqdata["seller_state"]=$enqVal["state"];
                                    $enqdata["seller_city"]=$enqVal["city"];
                                    $enqdata["seller_profile_image"]=$enqVal["seller_profile_image"];
                                    $enqdata["enq_message"]=$enqVal["enq_message"];
                                    $enqdata["enq_date"]=date_format(date_create($enqVal["enq_date"]),"d/m/Y ");
                                    $enqdata["attachments"]=json_decode($enqVal["attachments"]);
                                    $searchUnread = [];
                                    $searchUnread["enq_id"] = $enqVal["enq_id"];
                                    $enqdata["type"]=$data1["type"];
                                    $dataset["is_paid"]=0;
                                    if(!empty($proposal_data)){
                                        $dataset["is_paid"]=$proposal_data["is_paid"];
                                    }
                                    $dataset["is_sent_proposal_permission"]=0;
                                    if($enqVal["enq_to_id"]==$data["id"]){
                                        $dataset["is_sent_proposal_permission"]=1;
                                    }
                                    $result["quatation"]=$enqdata;
                            }
                            $search1=[];
                            $search1["enq_id"]=base64_decode($data1["rfq_id"]);
                            $search1["type"]=$data1["type"];
                            $search1["order_by"]=[["th.date","ASC"]];
                            if(isset($data1["is_seen"])){
                             $search1["is_seen"]=$data1["is_seen"];
                            }
                            $search1["login_customer_id"]=$data["id"];
                            $enquiry_message_data=$this->messagecenterModel->getEnquiryDetails($search1);
                           
                         foreach( $enquiry_message_data as $val12){
                            $edata=[];
                            $edata["th_enquiry_id"]=base64_encode($val12["th_enquiry_id"]);
                            
                            $edata["to_customer_id"]=base64_encode($val12["th_to_customer_id"]);

                            $edata["tocust_name"]=$val12["tocust_name"];
                           
                            if(trim($val12["to_business_name"])!=''){
                                $edata["tocust_name"]=$val12["to_business_name"];
                               
                            }
                            $edata["tocust_profile_image"]=$val12["tocust_profile_image"];
        
                            $edata["from_customer_id"]=base64_encode($val12["th_from_customer_id"]);
                            $edata["fromcust_name"]=$val12["fromcust_name"];
                            if(trim($val12["from_business_name"])!=''){
                                $edata["fromcust_name"]=$val12["from_business_name"];
                            }
                            $edata["fromcust_profile_image"]=$val12["fromcust_profile_image"];
        
                            $edata["message"]=$val12["th_message"];
                            $edata["image"]=$val12["images"];
                            $edata["date"]=date_format(date_create($val12["th_date"]),"d/m/Y h:i:s a ");
        
                             $edata["is_seen"]=$val12["th_is_seen"];
                                $result["message"][]=$edata;
        
                            
                          }
                    }
                    
                    $status = parent::HTTP_OK;
                    $response = ['status' => $status,'msg'=>'message data.','response_data'=>$result];
                    $this->response($response, $status);


                }

            }else{
                $status = parent::HTTP_UNAUTHORIZED;
                $response = ['status' => $status, 'msg' => 'Unauthorized Access! '];
                $this->response($response, $status);
               }
        } catch (Exception $e) {

            $this->response(['msg' => 'server busy .Please try again some time.','field'=>'user_name'], parent::HTTP_NOT_FOUND);
          }     
    }
   
    public function SendMessage_post(){
        try {
        $data=[];
         $result=[];
         $details=$this->post();
         $data = $this->verify_request();
          if(!empty( $data)){
            if(!isset($details["rfq_id"]) || trim($details["rfq_id"])==''){
                   $this->response(['msg' => 'Invalid Request!','field'=>'rfq_id'], parent::HTTP_BAD_REQUEST);
            }else if(!isset($details["to_customer_id"]) || trim($details["to_customer_id"])==''){
                   $this->response(['msg' => 'Invalid Request!','field'=>'to_customer_id'], parent::HTTP_BAD_REQUEST);
            }else if(!((isset($details["message"]) && trim($details["message"])!='' ) || (isset($_FILES["image"]) && $_FILES["image"]["name"]!=''))){
                   $this->response(['msg' => 'Please Enter Message OR Select Message','field'=>'message'], parent::HTTP_BAD_REQUEST);
            }else{
               
                $response_data=[];
                if($details["type"]=='rfq'  || $details["type"]=='service_rfq' ){
                   $search1=[];
                   $search1["rfq_id"]=base64_decode($details["rfq_id"]);
                   $rfq_data=[];
                   if($details["type"]=='rfq'){
                         $search1["group_by"]=["rfq.rfq_id"];
                        $rfq_data=$this->messagecenterModel->getRFQList($search1);
                   }
                    if(!empty($rfq_data)){

                      $insered_data=[];
                      $insered_data["rfq_id"]=$search1["rfq_id"];
                       $insered_data["to_customer_id"]=base64_decode($details["to_customer_id"]);
                        $insered_data["from_customer_id"]=$data["id"];
                         $insered_data["message"]=$details["message"];
                          $insered_data["date"]=date("Y-m-d H:i:s");
                          $insered_data["is_seen"]=0;
                          $insered_data["images"]='';
                          $message='Message sent successfully';
                          if(!empty($_FILES) && isset($_FILES["image"]) && $_FILES["image"]["name"]!='')
                          {
                            $this->load->library('image_lib');
                            
                              if ((($_FILES["image"]["type"]== "image/gif")  || ($_FILES["image"]["type"] == "image/jpeg")  || ($_FILES["image"]["type"] == "image/jpg")  || ($_FILES["image"]["type"] == "image/png")) )
                              {
                                 // File upload configuration
                                $images=[];
                                $uploadPath = '../admin/uploads/messagecenter/';
                                $fileName = time().'-'.$_FILES["image"]["name"];
                               
                                if(move_uploaded_file($_FILES['image']['tmp_name'],  $uploadPath."".$fileName))
                                {
      
                                  $config['image_library'] = 'gd2';
                                  $config['source_image'] = $uploadPath."".$fileName;
                                  $config['create_thumb'] = false;
                                  $config['maintain_ratio'] = TRUE;
                                  $config['width']     = 600;
                                  $config['height']   = 600;
                                  $this->image_lib->clear();
                                  $this->image_lib->initialize($config);
                                  $this->image_lib->resize();
                                 $insered_data["images"]=$fileName;
                                 $insert_id = $this->messagecenterModel->insertRfqThread($insered_data);
                                 $status = parent::HTTP_OK;
                                 }else{
                                    $status = parent::HTTP_BAD_REQUEST;
                                  $message='Message sent failed due to invalid image ';
                                  }
                                             
                              }else{
                                $status = parent::HTTP_BAD_REQUEST;
                                  $message='Message sent failed due to invalid image type1';
                              }
                                         
                            
                          }else{
                              if(trim( $insered_data["message"])!=''){
                                $status = parent::HTTP_OK;
                                  $insert_id = $this->messagecenterModel->insertRfqThread($insered_data);
                                  
                              }else{
                                $status = parent::HTTP_BAD_REQUEST;
                                  $message='Message sent failed due to invalid image';
                              }
                            
                          }
                        
                       
                          $response = ['status' => $status,'msg'=>$message];

                    }else{
                           $this->response(['msg' => 'Invalid Request!','field'=>'rfq_id'], parent::HTTP_BAD_REQUEST);
                    }
                }elseif($details["type"]=='enq' || $details["type"]=='service_enquiry'){
                  $search=[];
                  $this->load->model("enquiryModel");
                  $search1["enq_id"]=base64_decode($details["rfq_id"]);
                  $enquiry_data=$this->enquiryModel->getEnquiryList($search1);

                  if(!empty($enquiry_data))
                  {
                       

                    $insered_data=[];
                    $insered_data["enquiry_id"]=$search1["enq_id"];
                    $insered_data["to_customer_id"]=base64_decode($details["to_customer_id"]);
                    $insered_data["from_customer_id"]=$data["id"];
                    $insered_data["message"]=$details["message"];
                    $insered_data["date"]=date("Y-m-d H:i:s");
                    $insered_data["is_seen"]=0;
                    $message='Message sent successfully';
                    if(!empty($_FILES) && isset($_FILES["image"]) && $_FILES["image"]["name"]!='')
                    {
                      $this->load->library('image_lib');
                      
                        if ((($_FILES["image"]["type"]== "image/gif")  || ($_FILES["image"]["type"] == "image/jpeg")  || ($_FILES["image"]["type"] == "image/jpg")  || ($_FILES["image"]["type"] == "image/png")) )
                        {
                           // File upload configuration
                          $images=[];
                          $uploadPath = '../admin/uploads/messagecenter/';
                          $fileName = time().'-'.$_FILES["image"]["name"];
                         
                          if(move_uploaded_file($_FILES['image']['tmp_name'],  $uploadPath."".$fileName))
                          {

                            $config['image_library'] = 'gd2';
                            $config['source_image'] = $uploadPath."".$fileName;
                            $config['create_thumb'] = false;
                            $config['maintain_ratio'] = TRUE;
                            $config['width']     = 600;
                            $config['height']   = 600;
                            $this->image_lib->clear();
                            $this->image_lib->initialize($config);
                            $this->image_lib->resize();
                           $insered_data["images"]=$fileName;
                           $insert_id = $this->messagecenterModel->insertEnquiryThread($insered_data);

                           }else{
                            $message='Message sent failed due to invalid image ';
                            }
                                       
                        }else{
                            $message='Message sent failed due to invalid image type';
                        }
                                   
                      
                    }else{
                        if(trim( $insered_data["message"])!=''){
                            $insert_id = $this->messagecenterModel->insertEnquiryThread($insered_data);
                        }else{
                            $message='Message sent failed due to invalid image';
                        }
                      
                    }
                  
                    $status = parent::HTTP_BAD_REQUEST;
                    $response = ['status' => $status,'msg'=>$message];
                  }else{
                       $this->response(['msg' => 'Invalid Enquiry Request!','field'=>'enq_id'], parent::HTTP_BAD_REQUEST);
                    }

                }else{
                       $status = parent::HTTP_BAD_REQUEST;
                         $response = ['status' => $status,'msg'=>'Invalid Request'];
                }
            }

             $this->response($response, $status);
           
          }else{
            $status = parent::HTTP_UNAUTHORIZED;
            $response = ['status' => $status, 'msg' => 'Unauthorized Access! '];
            $this->response($response, $status);
          }

      } catch (Exception $e) {

        $this->response(['msg' => 'server busy .Please try again some time.','field'=>'user_name'], parent::HTTP_NOT_FOUND);
      }
  }
   
}
?>
