<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST");
class Enquiry extends REST_Controller {
	
	public function __construct() {
        parent::__construct();
        $this->load->helper(['jwt', 'authorization']); 
        $this->load->model("customerModel");
        error_reporting(E_ALL^E_NOTICE);
	 } 
     public function index_post(){
        $status = parent::HTTP_UNAUTHORIZED;
            $response = ['status' => $status, 'msg' => 'Unauthorized Access! '];
            $this->response($response, $status);
     }
  	 private function verify_request()
	{
		 $headers = $this->input->request_headers();

		 try {
            if(isset($headers['Authorization'])){
                   $token = $headers['Authorization'];
                   $data = AUTHORIZATION::validateToken($token);
                      if ($data === false) {
                          $status = parent::HTTP_UNAUTHORIZED;
                          $response = ['status' => $status, 'msg' => 'Unauthorized Access!'];
                          $this->response($response, $status);

                          exit();
                      } else {
                          $customer_data=current($this->customerModel->doAuthenticateCheck(["token_id"=>$data->token_id]));
                          
                          return $customer_data;
                      }
              }else if(isset($headers['authorization'])){
                         $token = $headers['authorization'];
                         $data = AUTHORIZATION::validateToken($token);
                            if ($data === false) {
                                $status = parent::HTTP_UNAUTHORIZED;
                                $response = ['status' => $status, 'msg' => 'Unauthorized Access!'];
                                $this->response($response, $status);

                                exit();
                            } else {
                                $customer_data=current($this->customerModel->doAuthenticateCheck(["token_id"=>$data->token_id]));
                                
                                return $customer_data;
                            }
             }else{
                $status = parent::HTTP_UNAUTHORIZED;
                $response = ['status' => $status, 'msg' => 'Unauthorized Access! '];
                $this->response($response, $status);
              }
          
	    } catch (Exception $e) {
	       $status = parent::HTTP_UNAUTHORIZED;
	        $response = ['status' => $status, 'msg' => 'Unauthorized Access! '];
	        $this->response($response, $status);
	    }
	}
    public function addEnquiry_post(){
      try {
        $data=[];
       $result=[];
       
         $data = $this->verify_request();
          if(!empty( $data)){

             $details=$this->post();

             /*echo '<pre>';
             print_r($details);
             die; */

             if(!empty($details)){
                  if(!isset($details["to_id"]) || trim($details["to_id"])==''){
                     $this->response(['msg' => 'Unauthorized Access!','field'=>'to_id'], parent::HTTP_UNAUTHORIZED);
                  }else if(!isset($details["from_id"]) || trim($details["from_id"])==''){
                     $this->response(['msg' => 'Unauthorized Access!','field'=>'from_id'], parent::HTTP_UNAUTHORIZED);
                  }else if(!isset($details["message"]) || trim($details["message"])==''){
                     $this->response(['msg' => 'Please Enter Description','field'=>'message'], parent::HTTP_BAD_REQUEST);
                  }else{
                     $this->load->model("customerModel");

                      $sellerinfo= current($this->customerModel->getSellerInfo(base64_decode($details["to_id"])));
                     
                      if( !empty($sellerinfo)){
                            unset($details["seller_id"]);
                            $details["from_id"]=base64_decode( $details["from_id"]);
                            $details["to_id"]=base64_decode( $details["to_id"]);
                            $details["seller_item_id"]=base64_decode( $details["seller_item_id"]);
                            if(isset($details["uom_id"]) && $details["uom_id"]!=''){
                               $details["uom_id"]=base64_decode( $details["uom_id"]);
                            }

                            
                             $details["quantity"]=$details["quantity"];
                             $details["message"]=$details["message"];

                             // Upload Attachment section start

                             $files = $_FILES;
                             $not_inseted=[];

                             $this->load->library('image_lib');

                             if(isset($_FILES['image']['name'])){
                              $cpt = count($_FILES['image']['name']);
                              $insert=0;
                              for($i=0; $i<$cpt; $i++){  
                                if (trim($files['image']['name'][$i]) =='') {
                                    continue;
                                }  

                                $fileName = time().'-'.$files['image']['name'][$i];
                                $allowedExts = array("gif", "jpeg", "jpg", "png");
                                $image_data=explode(".", $files['image']['name'][$i]);
                                $_FILES['image']['name']        = $fileName;
                                $_FILES['image']['type']        = $files['image']['type'][$i];
                                $_FILES['image']['tmp_name']    = $files['image']['tmp_name'][$i];
                                $_FILES['image']['error']       = $files['image']['error'][$i];
                                $_FILES['image']['size']        = $files['image']['size'][$i]; 

                                if ((($_FILES["image"]["type"] == "image/gif")  || ($_FILES["image"]["type"] == "image/jpeg")  || ($_FILES["image"]["type"] == "image/jpg")  || ($_FILES["image"]["type"] == "image/png") )) {
                                  $uploadPath = '../admin/uploads/enquiry/';
                                       
                                         
                                  if ( move_uploaded_file($_FILES['image']['tmp_name'],  $uploadPath."".$fileName)){
                                    $config = [];  
                                    $config['image_library'] = 'gd2';
                                    $config['source_image'] = $uploadPath."".$fileName;
                                    $config['create_thumb'] = false;
                                    $config['maintain_ratio'] = TRUE;
                                    $config['width']     = 600;
                                    $config['height']   = 600;
                                    $this->image_lib->clear();
                                    $this->image_lib->initialize($config);
                                    $this->image_lib->resize();
                                    $enquiryAttachments[] = $fileName;
                                    
                                  }else{
                                    $not_inseted[]=$files['image']['name'][$i];
                                  }

                                 
                                }else{
                                 $not_inseted[]= $_FILES['image']['name'];
                                }
                              }

                          }
                             //Upload Attachment section end
                             
                             $details["attachments"] = json_encode($enquiryAttachments);

                             $insertData['to_id'] = $details['to_id'];
                             $insertData['from_id'] = $details['from_id'];
                             $insertData['seller_item_id'] = $details['seller_item_id'];
                             $insertData['type'] = $details['type'];
                             $insertData['quantity'] = $details['quantity'];
                             $insertData['uom_id'] = $details['uom_id'];
                             $insertData['message'] = $details['message'];
                             $insertData['attachments'] = $details['attachments'];
                             $insertData['variance_json'] = $details['itemAttribute'];

                             $this->load->model("enquiryModel");
                            $id=$this->enquiryModel->insertEnquiry($insertData);
                          
                            if($id>0)
                            {
                              $status = parent::HTTP_OK;
                              $response = ['status' => $status,'msg'=>' Thank you for contacting us.Please wait for Our Seller Response.'];
                              $this->response($response, $status);
                            }else{
                              $this->response(['msg' => 'Enquiry submission failed.'], parent::HTTP_BAD_REQUEST);
                            }

                           
                      }else{
                          $status = parent::HTTP_UNAUTHORIZED;
                          $response = ['status' => $status, 'msg' => 'Unauthorized Access!!! '];
                          $this->response($response, $status);
                      }
                     
                  }
                
             }else{
               $this->response(['msg' => 'Invaild parameter.','field'=>'item_id'], parent::HTTP_BAD_REQUEST);

             }
          }else{
           $status = parent::HTTP_UNAUTHORIZED;
          $response = ['status' => $status, 'msg' => 'Unauthorized Access! '];
          $this->response($response, $status);
        }
      } catch (Exception $e) {

          $this->response(['msg' => 'server busy .Please try again some time.','field'=>'item_id'], parent::HTTP_NOT_FOUND);
      }
    }
 	 
    public function getenquirylist_post(){
       try {

         $details=$this->input->post();
         $data = $this->verify_request();

         if(trim($data["id"])=='' && (!($data["supplier_YN"]==0 || $data["supplier_YN"]==1 || $data["supplier_YN"]==2))){
            $this->response(['msg' => 'Unauthorized Access!','field'=>'user_id'], parent::HTTP_UNAUTHORIZED);
         }else{
             if(!isset($details["lang"])){
                $details["lang"]='en';
            }
             $limit=20;
              $start=0;
              if(!isset($details["pageno"])){
                $details["pageno"]=1;
              }

              if((isset($details["pageno"])&& trim($details["pageno"])!='')){
                $start=($details["pageno"]-1)* $limit;
              }
              $search1=[];
              $seller_info=current($this->customerModel->getSellerInfo($data["id"]));
              
              $search1["customer_id"]=$data["id"];

                $search1["order_by"]=[["enq.date","DESC"]];
                $response_data=[];
                $this->load->model("enquiryModel");
                  $enquiry_data=$this->enquiryModel->getEnquiryList($search1);
                 foreach( $enquiry_data as $enqVal){
                  $enqdata=[];
                  $enqdata["login_customer_id"] = base64_encode($data["id"]);
                  $enqdata["enq_id"]=base64_encode($enqVal["enq_id"]);
                  $enqdata["enq_to_id"]=base64_encode($enqVal["enq_to_id"]);
                  $enqdata["enq_from_id"]=base64_encode($enqVal["enq_from_id"]);
                  $enqdata["enq_seller_item_id"]=$enqVal["enq_seller_item_id"];
                  $search1=[];
                  $search1["item_id"]=$enqVal["enq_seller_item_id"];
                  $this->load->model("itemModel");
                  $itemdetails=current($this->itemModel->getItem($search1));
                  $enqdata["itemdata"]=[];
                  if($details["lang"]=='en'){
                    $enqdata["itemdata"]["name"]=$itemdetails["item_name_en"];
                  }else{
                    $enqdata["itemdata"]["name"]=$itemdetails["item_name_fr"];
                  }
                  $enqdata["itemdata"]["thumbnail"]=$itemdetails["thumbnail"];
                  $enqdata["buyer_name"]=$enqVal["buyer_name"];
                   $enqdata["buyer_profile_image"]=$enqVal["buyer_profile_image"];
                  $enqdata["seller_name"]=$enqVal["seller_name"];
                  $enqdata["seller_business_name"]=$enqVal["business_name"];
                  $enqdata["seller_address"]=$enqVal["address"];
                  $enqdata["seller_country"]=$enqVal["country"];
                  $enqdata["seller_state"]=$enqVal["state"];
                  $enqdata["seller_city"]=$enqVal["city"];
                  $enqdata["seller_profile_image"]=$enqVal["seller_profile_image"];
                  $enqdata["enq_message"]=$enqVal["enq_message"];
                  $enqdata["enq_date"]=date_format(date_create($enqVal["enq_date"]),"d/m/Y ");
                  if($enqVal["enq_type"] == 1)
                  {
                    $enqdata["type"] = 'enquiry';
                  }elseif($enqVal["enq_type"] == 2)
                  {
                    $enqdata["type"] = 'service_enquiry';
                  }
                   $enqdata["created_date1"]=date_create($enqVal["enq_date"]);
                   $enqdata["attachments"]=json_decode($enqVal["attachments"]);
                   
                   $response_data[]=$enqdata;
                }
                
                $status = parent::HTTP_OK;
                $response = ['status' => $status,'msg'=>'enquiry data.','response_data'=>$response_data];
                                    $this->response($response, $status);



         }
        } catch (Exception $e) {

          $this->response(['msg' => 'server busy .Please try again some time.','field'=>'user_name'], parent::HTTP_NOT_FOUND);
      }
    }

}
?>
