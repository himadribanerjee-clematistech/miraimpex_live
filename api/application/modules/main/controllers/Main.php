<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

header("Access-Control-Allow-Origin: *"); //Access-Control-Allow-Origin
header("Access-Control-Allow-Methods: POST");
class Main extends REST_Controller {
	
	public function __construct() {
        parent::__construct();
        $this->load->helper(['jwt', 'authorization']); 
        $this->load->model("categoryModel");
        $this->load->model("itemModel");
        $this->load->model("customerModel");
	 }
     public function index_post(){
        $status = parent::HTTP_UNAUTHORIZED;
            $response = ['status' => $status, 'msg' => 'Unauthorized Access! '];
            $this->response($response, $status);
     }
  	 private function verify_request(){
         $headers = $this->input->request_headers();
         if(isset($headers['Authorization'])){
             $token = $headers['Authorization'];
             try {
                 $data = AUTHORIZATION::validateToken($token);
                    if ($data === false) {
                        $status = parent::HTTP_UNAUTHORIZED;
                        $response = ['status' => $status, 'msg' => 'Unauthorized Access!'];
                        $this->response($response, $status);

                        exit();
                    } else {
                        $customer_data=current($this->customerModel->doAuthenticateCheck(["token_id"=>$data->token_id]));
                        
                        return $customer_data;
                    }
            } catch (Exception $e) {
               $status = parent::HTTP_UNAUTHORIZED;
                $response = ['status' => $status, 'msg' => 'Unauthorized Access! '];
                $this->response($response, $status);
            }
        }else if(isset($headers['authorization'])){
             $token = $headers['authorization'];
             $data = AUTHORIZATION::validateToken($token);
                if ($data === false) {
                    $status = parent::HTTP_UNAUTHORIZED;
                    $response = ['status' => $status, 'msg' => 'Unauthorized Access!'];
                    $this->response($response, $status);

                    exit();
                } else {
                    $customer_data=current($this->customerModel->doAuthenticateCheck(["token_id"=>$data->token_id]));
                    
                    return $customer_data;
                }
        }else{
             $status = parent::HTTP_UNAUTHORIZED;
                $response = ['status' => $status, 'msg' => 'Unauthorized Access!'];
                $this->response($response, $status);

                exit();
        }
        
        
    }

    public function getuomlist_post(){
        try { 
              $data=$this->post();  
             
              if(!isset($data["lang"])){
                 $data["lang"]='en';
              }

               $lang = $data["lang"];
              $uom_data=$this->categoryModel->getuom(["lang"=>$lang]);
              foreach($uom_data as $k=>$val){
                $uom_data[$k]["id"]=base64_encode($val["id"]);
              }
               $status = parent::HTTP_OK;
              $response = ['status' => $status,'uom_data'=>$uom_data];
              $this->response($response, $status);
           } catch (Exception $e) {
             $this->response(['msg' => 'server busy .Please try again some time.','field'=>'user_name'], parent::HTTP_NOT_FOUND);
        }
    }
    public function getAttribute_post(){
      try { 
        $data=$this->post();  
        if(!isset($data["lang"])){
          $data["lang"]='en';
        }
        $type='';
        if(isset($data["type"]) && trim($data["type"])!=''){

          $type=$data["type"];

        }
        $lang = $data["lang"];

        $attribute_data=$this->categoryModel->getattribute(["lang"=>$lang,"type"=>$type]); 
        $attribute=[];
            foreach($attribute_data as $val){  
              $attribute[]=["id"=>base64_encode($val["id"]),"name"=>$val["name"]];
            }  
        $status = parent::HTTP_OK;
        $response = ['status' => $status,'result'=>$attribute];
           $this->response($response, $status);
      } catch (Exception $e) {
        $this->response(['msg' => 'server busy .Please try again some time.','field'=>'user_name'], parent::HTTP_NOT_FOUND);
      }
    }

    public function getVariance_post(){
      try { 
        $data=$this->post();  
        if(!isset($data["lang"])){
          $data["lang"]='en';
        }
        $type='';
        if(isset($data["type"]) && trim($data["type"])!=''){

          $type=$data["type"];

        }
        $lang = $data["lang"];

        $variance_data=$this->categoryModel->getvariance(["lang"=>$lang,"type"=>$type]); 
        $variance=[];
            foreach($variance_data as $val){  
              $variance[]=["id"=>base64_encode($val["id"]),"name"=>$val["name"]];
            }  
        $status = parent::HTTP_OK;
        $response = ['status' => $status,'result'=>$variance];
           $this->response($response, $status);
      } catch (Exception $e) {
        $this->response(['msg' => 'server busy .Please try again some time.','field'=>'user_name'], parent::HTTP_NOT_FOUND);
      }
    }

    public function getsupplierByCategory_post(){
      try { 
        $data=$this->input->post();
        if(!isset($data["lang"])){
          $data["lang"]='en';
        }
        $lang = $data["lang"];
         if(isset($data["category_id"]) && trim($data["category_id"])!=''){
         $data["category_id"]=base64_decode($data["category_id"]);

         $limit=20;
         $start=0;
         if(!isset($data["pageno"])){
           $data["pageno"]=1;
         }

         if((isset($data["pageno"])&& trim($data["pageno"])!='')){
          $start=($data["pageno"]-1)* $limit;
        }
       
        $seller=$this->itemModel->getItem(["category_json"=>$data["category_id"],"group_by"=>['itm.supp_id']],$limit,$start);
        $result=[];
        $seller_ids=[];
         foreach($seller as $val){
          $seller_ids[]=$val["seller_id"];
          $result[$val["seller_id"]]["seller_id"]=base64_encode($val["seller_id"]);
          $result[$val["seller_id"]]["name"]=$val["name"];
          $result[$val["seller_id"]]["phone"]=$val["phone"];
          $result[$val["seller_id"]]["email"]=$val["email"];
          $result[$val["seller_id"]]["business_name"]=$val["business_name"];
          $result[$val["seller_id"]]["address"]=$val["address"];
          $result[$val["seller_id"]]["country"]=$val["country"];
          $result[$val["seller_id"]]["state"]=$val["state"];
          $result[$val["seller_id"]]["sellerCategory"]=$val["sellerCategory"];
          $result[$val["seller_id"]]["city"]=$val["city"];
          $result[$val["seller_id"]]["contact_name"]=$val["contact_name"];
          $result[$val["seller_id"]]["contact_number"]=$val["contact_number"];
          $result[$val["seller_id"]]["buyer_name"]=$val["buyer_name"];
          $result[$val["seller_id"]]["profile_image"]=$val["profile_image"];
          $result[$val["seller_id"]]["buyer_address"]=$val["buyer_address"];
          $result[$val["seller_id"]]["buyer_city"]=$val["buyer_city"];
          $result[$val["seller_id"]]["buyer_country"]=$val["buyer_country"];
          $result[$val["seller_id"]]["buyer_state"]=$val["buyer_state"];
          $result[$val["seller_id"]]["seller_image"]=[];
          $result[$val["seller_id"]]["certificate"]=[];
          $result[$val["seller_id"]]["seller_items"]=[];
          
          
         }

         if(empty($seller_ids)){
          $seller_ids=[-1];
         }
        $images=$this->customerModel->getSellerimagesList(["seller_ids"=> $seller_ids]);

        foreach($images as $val1){
          if(isset($result[$val1["seller_id"]]) && $val1["type"]==1 ){
            $result[$val1["seller_id"]]["seller_image"][]=["image_id"=>base64_encode($val1["id"]),"seller_image"=>$val1["seller_image"]];
          }
          if(isset($result[$val1["seller_id"]]) && $val1["type"]==2 ){
            $result[$val1["seller_id"]]["certificate"][]=["image_id"=>base64_encode($val1["id"]),"seller_image"=>$val1["seller_image"]];
          }
         
        }
              
              // Variance table
            
        $listVariance = $this->itemModel->getlistVarience($data);

        $items=$this->itemModel->getItem(["seller_ids"=> $seller_ids]);
        foreach($items as $val3){
          if(isset($result[$val3["seller_id"]])  ){
              ///////////////////////
              $result2=[];
              $result2["item_id"]=base64_encode($val3["item_id"]);
               $result2["seller_id"]=base64_encode($val3["seller_id"]);
                $result2["local_sup_id"]=base64_encode($val3["local_sup_id"]);
                $result2["category_id"]=base64_encode($val3["category_id"]);
                 $result2["price_uom_id"]=base64_encode($val3["price_uom_id"]);
                 $result2["moq_uom_id"]=base64_encode($val3["moq_uom_id"]);
                if(isset($val3["category_json"])){
                  $val3["category_json"]=json_decode($val3["category_json"]);
                  foreach($val3["category_json"] as $val31){
                     $category_json[]=base64_encode($val31);
                  }
                  $result2["category_id"]=json_encode($category_json);
                 }
                if($data["lang"]=='en'){
                   $result2["item_name"]=$val3["item_name_en"];
                    $result2["description"]=$val3["description_en"];
                    $result2["lead_time"]=$val3["lead_time"];
                    $result2["moq_uom_name_en"]=$val3["moq_uom_name_en"];
                    $result2["price_uom_name_en"]=$val3["price_uom_name_en"];
                    
                   
                }else{
                   $result2["item_name"]=$val3["item_name_fr"];
                   $result2["description"]=$val3["description_fr"];
                    $result2["lead_time"]=$val3["lead_time"];
                    $result2["moq_uom_name_fr"]=$val3["moq_uom_name_fr"];
                    $result2["price_uom_name_fr"]=$val3["price_uom_name_fr"];
                   
                }
                $result2["sellerinfo"]["business_name"]=$val3["business_name"];
                $result2["sellerinfo"]["address"]=$val3["address"];
                $result2["sellerinfo"]["country"]=$val3["country"];
                $result2["sellerinfo"]["state"]=$val3["state"];
                $result2["sellerinfo"]["city"]=$val3["city"];
                $new_attribute_variance_arr=[];
                if($val3["variance"] != '')
              {
                  $attribute_variance_arr = json_decode($val3["variance"],true);
                  
                  if(is_array($attribute_variance_arr) && count($attribute_variance_arr)>0)
                  {
                      foreach($attribute_variance_arr as $hk=>$hv)
                      {
                          //$listVariance
                          
                          $varianceName = $listVariance[$hk]['name'];

                          foreach($hv as $hkk=>$hvv)
                          {
                              
                          foreach($hvv as $hkkk=>$hvvv)
                          {
                              $new_attribute_variance_arr[$varianceName][] = $hvvv;
                              
                          }
                              
                          }

                          
                      }
                  }
              }
              
              $result2["modified_variance"]=json_encode($new_attribute_variance_arr);


              $new_attribute_define_arr=[];
              if($val3["attribute"] != '')
              {
                  $attribute_define_arr = json_decode($val3["attribute"],true);
                  
                  if(is_array($attribute_define_arr) && count($attribute_define_arr)>0)
                  {
                      foreach($attribute_define_arr as $hk=>$hv)
                      {                            
                          
                          foreach($hv as $hkk=>$hvv)
                          {
                              
                              $new_attribute_define_arr[$hkk] = $hvv;
                              
                          }

                          
                      }
                  }
              }                
              $result2["modified_attribute"]=json_encode($new_attribute_define_arr);
              
              $result2["price_min"]=$val3["price_min"];
              $result2["price_max"]=$val3["price_max"];
              $result2["moq"]=$val3["moq"];
              $result2["currency"]=$val3["currency"];
              $result2["keywords"]=$val3["keywords"];  
              $result2["packaging"]=$val3["packaging"];  
              $result2["thumbnail"]=$val3["thumbnail"];   
              $result2["variance"]=json_decode($val3["variance"],true);
              $result2["attribute"]=json_decode($val3["attribute"],true);
              
              $result2["showcase"]=$val3["showcase"];   
               $result2["item_image"]=$val3["item_image"];
                 $result2["active"]=$val3["active"];
                  $result2["is_approved"]=$val3["is_approved"];
                   $result2["supplier_name"]=$val3["name"];
                    $result2["supplier_phone"]=$val3["phone"];
                     $result2["supplier_email"]=$val3["email"];

                     $result[$val3["seller_id"]]["seller_items"][]= $result2;
              ///////////////////////
          }
        }
        $status = parent::HTTP_OK;
        $response = ['status' => $status,'msg'=>' Seller Item ',"item"=> array_values($result)];
         $this->response($response, $status);
        }else{
          $this->response(['msg' => 'Please Select Category','field'=>'category_id'], parent::HTTP_BAD_REQUEST);
        }
      } catch (Exception $e) {
        $this->response(['msg' => 'server busy .Please try again some time.','field'=>'user_name'], parent::HTTP_NOT_FOUND);
      }
    }
    public function getCategoryChain_post(){
      try { 
        $data=$this->post();  
        if(!isset($data["lang"])){
          $data["lang"]='en';
        }
        $lang = $data["lang"];
        $parent_id=0;
        $type='';
        $level=4;
        if(isset($data["level"]) && trim($data["level"])!=''){

          $level=$data["level"];
         }
        if(isset($data["parent_id"]) && trim($data["parent_id"])!=''){

          $parent_id=base64_decode($data["parent_id"]);
         }
         if(isset($data["type"]) && trim($data["type"])!=''){

          $type=$data["type"];

        }
        $category_data=$this->categoryModel->getCategory(["parent_id"=>$parent_id,'level'=>$level,"type"=>$type,"lang"=>$lang,"active"=>1]);     
            $cat_data=[];
            foreach($category_data as $val){
                if(!isset($cat_data[base64_encode($val["parent_id"])])){
                  $cat_data[base64_encode($val["parent_id"])]=[];
                }
                $cat_data[base64_encode($val["parent_id"])][]=["id"=>base64_encode($val["id"]),"category_name"=>$val["category_name"],"image"=>$val["image"],"link_json"=>json_decode($val["link_json"],true),"metadata"=>$val["metadata"],"slug"=>$val["slug"],"type"=>$val["type"]];
            }
            $status = parent::HTTP_OK;
            $response = ['status' => $status,'result'=>$cat_data];
               $this->response($response, $status);
      } catch (Exception $e) {
        $this->response(['msg' => 'server busy .Please try again some time.','field'=>'user_name'], parent::HTTP_NOT_FOUND);
      }
    }
    public function getCategoryList_post(){
      // echo 'Hello';
         try { 
            $data=$this->post();  
            $level=1;
            $parent_id=0;
            $displayLocal=0;
            $limit='';
            $categoy_datas=[];
            $type='';
            if(!isset($data["lang"])){
              $data["lang"]='en';
            }            
            $lang = $data["lang"];

            if(!isset($data["displayLocal"])){
              $data["displayLocal"] = 0;
            }
            $displayLocal = $data["displayLocal"];
            
            if(isset($data["level"]) && trim($data["level"])!=''){

             $level=$data["level"];
            }
            if(isset($data["parent_id"]) && trim($data["parent_id"])!=''){

             $parent_id=base64_decode($data["parent_id"]);
            }
             if(isset($data["limit"]) && trim($data["limit"])!=''){

              $limit=$data["limit"];

            }
            if(isset($data["type"]) && trim($data["type"])!=''){

              $type=$data["type"];

            }

            if(isset($data["active"]) && trim($data["active"])!=''){

              $active=$data["active"];

            }else{
              $active=1;
            }

            if($level==1){
              $category_datas=$this->categoryModel->getCategory(["parent_id"=>$parent_id,"limit"=>$limit,"type"=>$type,"lang"=>$lang,"active"=>$active,"displayLocal"=>$displayLocal]);
                foreach($category_datas as $key=>$val){                  
                  $category_datas[$key]["id"]=base64_encode($val["id"]);
                   $category_datas[$key]["parent_id"]=base64_encode($val["parent_id"]);
                }
            }else{
              $category_data=$this->categoryModel->getCategory(["parent_id"=>$parent_id,'level'=>$level,"type"=>$type,"lang"=>$lang,"active"=>$active,"displayLocal"=>$displayLocal]);              
              
              $category_datas = buildTree($category_data,$parent_id,999999,1);
                    
              
            }
          $status = parent::HTTP_OK;
            $response = ['status' => $status,'result'=>$category_datas];
               $this->response($response, $status);
        
        } catch (Exception $e) {
             $this->response(['msg' => 'server busy .Please try again some time.','field'=>'user_name'], parent::HTTP_NOT_FOUND);
        }
    }
    public function getCatSearch_post(){
      try { 
        $data=$this->input->post();
        if(isset( $data["keyword"]) && is_array($data["keyword"]) ){
          $data["keyword"]=$data["keyword"]["term"];
        }

        if(isset( $data["keyword"]) && trim( $data["keyword"])!='' && strlen( $data["keyword"])>=3){
          if(!isset($data["lang"])){
            $data["lang"]='en';
          }
          $lang = $data["lang"];
          $level=999;
          $parent_id=0;
          $category_data=$this->categoryModel->getCategory(["lang"=>$lang,"active"=>1]); 
          $category_datas = buildTree($category_data,$parent_id,999999,1); 
          
          $new_data=[];
          $i=0;
          foreach($category_datas as $val){
            $new_data1=["ids"=>[],"breadcumb"=>[]];
            
            $new_data1["ids"][]=base64_decode($val["id"]);
            $new_data1["breadcumb"][]=$val["category_name"];
            $new_data[$i]=$new_data1;
            if(isset($val["children"]) && !empty($val["children"])){
             $new_data2=$this->createBreadcumb($new_data,$val["children"],$i);
              $new_data=$new_data2["new_data"];
               $i=$new_data2["i"];
            }
            $i++;
          }

          $category_data12=$this->categoryModel->getCategory(["lang"=>$lang,"keyword"=>$data["keyword"],"active"=>1]); 
          $cat_ids=array_column( $category_data12,"id");
          $result_data=[];
           foreach($new_data as $val){
            $result_ins=array_intersect($cat_ids,$val["ids"]);
            if(!empty($result_ins)){
              $set_data=[];
              $set_data["id"]=base64_encode(end($val["ids"]));
              //$set_data["ids"]=$val["ids"];
              $set_data["breadcumb"]=implode("->",$val["breadcumb"]);
              $result_data[]=$set_data;
            }
           
          }
            
          $status = parent::HTTP_OK;
          $response = ['status' => $status,'result'=>$result_data];
             $this->response($response, $status);
        }else{
          $status = parent::HTTP_OK;
          $response = ['status' => $status,'result'=>[]];
             $this->response($response, $status);
        }
        
      } catch (Exception $e) {
        $this->response(['msg' => 'server busy .Please try again some time.','field'=>'user_name'], parent::HTTP_NOT_FOUND);
     }
    }
   public function createBreadcumb($new_array,$category_data,$i){
     $k=$i;
     $old_data=$new_array[$i];
     $j=0;
      foreach($category_data as $val1){

        if($j==0){
          $new_array[$i]["ids"][]=base64_decode($val1["id"]);
          $new_array[$i]["breadcumb"][]=$val1["category_name"];;
        }else{
          $i++;
          $new_array[$i]["ids"]=$old_data["ids"];
          $new_array[$i]["breadcumb"]=$old_data["breadcumb"];
          $new_array[$i]["ids"][]=base64_decode($val1["id"]);
          $new_array[$i]["breadcumb"][]=$val1["category_name"];;
        }
        if(isset($val1["children"]) && !empty($val1["children"])){
          $new_array1=$this->createBreadcumb($new_array,$val1["children"],$i);
          
          $new_array=$new_array1["new_data"];
          $i=$new_array1["i"];
         }
       
        $j++;
      }
     return ["new_data"=>$new_array,"i"=>$i];
   }
   
}
?>
