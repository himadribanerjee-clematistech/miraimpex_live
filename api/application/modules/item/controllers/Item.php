<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST");
class Item extends REST_Controller {
	
	public function __construct() {
        parent::__construct();
        $this->load->helper(['jwt', 'authorization']); 
        $this->load->model("itemModel");
         $this->load->model("customerModel");
         $this->load->model("categoryModel");
         
	 }
     public function index_post(){
        $status = parent::HTTP_UNAUTHORIZED;
            $response = ['status' => $status, 'msg' => 'Unauthorized Access! '];
            $this->response($response, $status);
     }
  	 private function verify_request(){
         $headers = $this->input->request_headers();
         if(isset($headers['Authorization'])){
             $token = $headers['Authorization'];
             try {
                 $data = AUTHORIZATION::validateToken($token);
                    if ($data === false) {
                        $status = parent::HTTP_UNAUTHORIZED;
                        $response = ['status' => $status, 'msg' => 'Unauthorized Access!'];
                        $this->response($response, $status);

                        exit();
                    } else {
                        $customer_data=current($this->customerModel->doAuthenticateCheck(["token_id"=>$data->token_id]));
                        
                        return $customer_data;
                    }
            } catch (Exception $e) {
               $status = parent::HTTP_UNAUTHORIZED;
                $response = ['status' => $status, 'msg' => 'Unauthorized Access! '];
                $this->response($response, $status);
            }
        }else if(isset($headers['authorization'])){
             $token = $headers['authorization'];
             $data = AUTHORIZATION::validateToken($token);
                if ($data === false) {
                    $status = parent::HTTP_UNAUTHORIZED;
                    $response = ['status' => $status, 'msg' => 'Unauthorized Access!'];
                    $this->response($response, $status);

                    exit();
                } else {
                    $customer_data=current($this->customerModel->doAuthenticateCheck(["token_id"=>$data->token_id]));
                    
                    return $customer_data;
                }
        }else{
             $status = parent::HTTP_UNAUTHORIZED;
                $response = ['status' => $status, 'msg' => 'Unauthorized Access!'];
                $this->response($response, $status);

                exit();
        }
        
        
    }
    public function getSellerDetails_post(){
        try {
            $data=$this->input->post();
            if(!isset($data["lang"])){
              $data["lang"]='en';
            }
            $lang = $data["lang"];
            if(isset($data["seller_id"]) && trim($data["seller_id"])!=''){
             $data["seller_id"]=base64_decode($data["seller_id"]);
             $limit=20;
             $start=0;
             if(!isset($data["pageno"])){
               $detdataails["pageno"]=1;
             }
    
             if((isset($data["pageno"])&& trim($data["pageno"])!='')){
              $start=($data["pageno"]-1)* $limit;
            }
            $seller=$this->itemModel->getItem(["supp_id"=>$data["seller_id"],"group_by"=>['itm.supp_id']],$limit,$start);

            $result=[];
                $seller_ids=[];
                foreach($seller as $val){
                $seller_ids[]=$val["seller_id"];
                $result[$val["seller_id"]]["seller_id"]=base64_encode($val["seller_id"]);
                $result[$val["seller_id"]]["name"]=$val["name"];
                $result[$val["seller_id"]]["phone"]=$val["phone"];
                $result[$val["seller_id"]]["email"]=$val["email"];
                $result[$val["seller_id"]]["business_name"]=$val["business_name"];
                $result[$val["seller_id"]]["address"]=$val["address"];
                $result[$val["seller_id"]]["country"]=$val["country"];
                $result[$val["seller_id"]]["state"]=$val["state"];
                $result[$val["seller_id"]]["sellerCategory"]=$val["sellerCategory"];
                
                $result[$val["seller_id"]]["city"]=$val["city"];
                $result[$val["seller_id"]]["contact_name"]=$val["contact_name"];
                $result[$val["seller_id"]]["contact_number"]=$val["contact_number"];
                $result[$val["seller_id"]]["buyer_name"]=$val["buyer_name"];
                $result[$val["seller_id"]]["profile_image"]=$val["profile_image"];
                $result[$val["seller_id"]]["buyer_address"]=$val["buyer_address"];
                $result[$val["seller_id"]]["buyer_city"]=$val["buyer_city"];
                $result[$val["seller_id"]]["buyer_country"]=$val["buyer_country"];
                $result[$val["seller_id"]]["buyer_state"]=$val["buyer_state"];
                $result[$val["seller_id"]]["seller_image"]=[];
                $result[$val["seller_id"]]["certificate"]=[];
                $result[$val["seller_id"]]["seller_items"]=[];
                
                
                }

                if(empty($seller_ids)){
                    $seller_ids=[-1];
                   }
                  $images=$this->customerModel->getSellerimagesList(["seller_ids"=> $seller_ids]);
          
                  foreach($images as $val1){
                    if(isset($result[$val1["seller_id"]]) && $val1["type"]==1 ){
                      $result[$val1["seller_id"]]["seller_image"][]=["image_id"=>base64_encode($val1["id"]),"seller_image"=>$val1["seller_image"]];
                    }
                    if(isset($result[$val1["seller_id"]]) && $val1["type"]==2 ){
                      $result[$val1["seller_id"]]["certificate"][]=["image_id"=>base64_encode($val1["id"]),"seller_image"=>$val1["seller_image"]];
                    }
                    
                  }
                        
                        // Variance table
                      
                  $listVariance = $this->itemModel->getlistVarience($data);
          
                  $items=$this->itemModel->getItem(["seller_ids"=> $seller_ids]);

                  foreach($items as $val3){
                    if(isset($result[$val3["seller_id"]])  ){
                        ///////////////////////
                        $result2=[];
                        $result2["item_id"]=base64_encode($val3["item_id"]);
                         $result2["seller_id"]=base64_encode($val3["seller_id"]);
                          $result2["local_sup_id"]=base64_encode($val3["local_sup_id"]);
                          $result2["category_id"]=base64_encode($val3["category_id"]);
                           $result2["price_uom_id"]=base64_encode($val3["price_uom_id"]);
                           $result2["moq_uom_id"]=base64_encode($val3["moq_uom_id"]);
                          if(isset($val3["category_json"])){
                            $val3["category_json"]=json_decode($val3["category_json"]);
                            foreach($val3["category_json"] as $val31){
                               $category_json[]=base64_encode($val31);
                            }
                            $result2["category_id"]=json_encode($category_json);
                           }
                          if($data["lang"]=='en'){
                             $result2["item_name"]=$val3["item_name_en"];
                              $result2["description"]=$val3["description_en"];
                              $result2["lead_time"]=$val3["lead_time"];
                              $result2["moq_uom_name_en"]=$val3["moq_uom_name_en"];
                              $result2["price_uom_name_en"]=$val3["price_uom_name_en"];
                              
                             
                          }else{
                             $result2["item_name"]=$val3["item_name_fr"];
                             $result2["description"]=$val3["description_fr"];
                              $result2["lead_time"]=$val3["lead_time"];
                              $result2["moq_uom_name_fr"]=$val3["moq_uom_name_fr"];
                              $result2["price_uom_name_fr"]=$val3["price_uom_name_fr"];
                             
                          }
                          $result2["sellerinfo"]["business_name"]=$val3["business_name"];
                          $result2["sellerinfo"]["address"]=$val3["address"];
                          $result2["sellerinfo"]["country"]=$val3["country"];
                          $result2["sellerinfo"]["state"]=$val3["state"];
                          $result2["sellerinfo"]["city"]=$val3["city"];
                          $new_attribute_variance_arr=[];
                          if($val3["variance"] != '')
                        {
                            $attribute_variance_arr = json_decode($val3["variance"],true);
                            
                            if(is_array($attribute_variance_arr) && count($attribute_variance_arr)>0)
                            {
                                foreach($attribute_variance_arr as $hk=>$hv)
                                {
                                    //$listVariance
                                    
                                    $varianceName = $listVariance[$hk]['name'];
          
                                    foreach($hv as $hkk=>$hvv)
                                    {
                                        
                                    foreach($hvv as $hkkk=>$hvvv)
                                    {
                                        $new_attribute_variance_arr[$varianceName][] = $hvvv;
                                        
                                    }
                                        
                                    }
          
                                    
                                }
                            }
                        }
                        
                        $result2["modified_variance"]=json_encode($new_attribute_variance_arr);
          
          
                        $new_attribute_define_arr=[];
                        if($val3["attribute"] != '')
                        {
                            $attribute_define_arr = json_decode($val3["attribute"],true);
                            
                            if(is_array($attribute_define_arr) && count($attribute_define_arr)>0)
                            {
                                foreach($attribute_define_arr as $hk=>$hv)
                                {                            
                                    
                                    foreach($hv as $hkk=>$hvv)
                                    {
                                        
                                        $new_attribute_define_arr[$hkk] = $hvv;
                                        
                                    }
          
                                    
                                }
                            }
                        }                
                        $result2["modified_attribute"]=json_encode($new_attribute_define_arr);
                        
                        $result2["price_min"]=$val3["price_min"];
                        $result2["price_max"]=$val3["price_max"];
                        $result2["fixedPrice"]=$val3["fixedPrice"];
                        $result2["moq"]=$val3["moq"];
                        $result2["currency"]=$val3["currency"];
                        $result2["keywords"]=$val3["keywords"];  
                        $result2["packaging"]=$val3["packaging"];  
                        $result2["thumbnail"]=$val3["thumbnail"];   
                        $result2["variance"]=json_decode($val3["variance"],true);
                        $result2["attribute"]=json_decode($val3["attribute"],true);
                        
                        $result2["showcase"]=$val3["showcase"];   
                         $result2["item_image"]=$val3["item_image"];
                           $result2["active"]=$val3["active"];
                            $result2["is_approved"]=$val3["is_approved"];
                             $result2["supplier_name"]=$val3["name"];
                              $result2["supplier_phone"]=$val3["phone"];
                               $result2["supplier_email"]=$val3["email"];
          
                               $result[$val3["seller_id"]]["seller_items"][]= $result2;
                        ///////////////////////
                    }
                  }
                  $status = parent::HTTP_OK;
                  $response = ['status' => $status,'msg'=>' Seller Item ',"item"=> array_values($result)];
                   $this->response($response, $status);

            }else{
                $this->response(['msg' => 'Invalid Seller','field'=>'seller_id'], parent::HTTP_BAD_REQUEST);
            }
        } catch (Exception $e) {
          $this->response(['msg' => 'server busy .Please try again some time.','field'=>'user_name'], parent::HTTP_NOT_FOUND);
        }   
    }
    public function getitem_post(){
      try {
            $details=$this->input->post();
            if(!isset($details["type"])){
                $details["type"]=1;
            }
            if(!isset($details["category_id"])){
                $details["category_id"]='';
            }
            if(!isset($details["item_id"])){
                $details["item_id"]='';
            }
            if(!isset($details["lang"])){
                $details["lang"]='en';
            }
            if(!isset($details["displayLocal"])){
              $details["displayLocal"]='';
           } 
           if(!isset($details["search_keyword"])){
            $details["search_keyword"]='';
         }
           $showcase='';
           if(!isset($details["showcase"])){
              $showcase=1;
           } 
           if(trim($details["category_id"])=='' &&  trim($details["item_id"])=='' ){
               $this->response(['msg' => 'Invalid category!','field'=>'category_id'], parent::HTTP_BAD_REQUEST);
           }else{
             $itemdata=[];
            if(trim($details["item_id"])==''){
                $category_data=$this->categoryModel->getCategory(["parent_id"=>base64_decode($details["category_id"]),'level'=>99999,"type"=>$details["type"],"lang"=>$details["lang"]]);
                 $limit=20000;
                  $start=0;
                  if(!isset($details["pageno"])){
                    $details["pageno"]=1;
                  }

                  if((isset($details["pageno"])&& trim($details["pageno"])!='')){
                    $start=($details["pageno"]-1)* $limit;
                  }
                $category_datas = buildTree($category_data,base64_decode($details["category_id"]),99999,1);
                 $ids=buildTreeID($category_datas,[]);
                 
                if(empty($ids)){
                   $ids=[base64_decode($details["category_id"])];
                }
               $itemdata=$this->itemModel->getItem(["ids"=>$ids,"showcase"=>$showcase,"displayLocal"=>$details["displayLocal"],"search_keyword"=>$details["search_keyword"]],$limit,$start);
                
            }else{
               $itemdata=$this->itemModel->getItem(["showcase"=>$showcase,"displayLocal"=>$details["displayLocal"],"item_id"=>base64_decode($details["item_id"]),"search_keyword"=>$details["search_keyword"]]);
                
            }
             
           
           
         		 $attribute=$this->itemModel->getAttribute();
         		 $att=[];
         		 foreach ($attribute as $key => $value) {
         		 	if($details["lang"]=='en'){
         		 		$att[base64_encode($value["id"])]=$value["name_en"];
         		 	}else{
         		 		$att[base64_encode($value["id"])]=$value["name_fr"];
         		 	}
         		 }
                   
              // Variance table
              $this->db->db_debug = true;
              if($details["lang"]=='en')
              {
                $this->db->select("v.id,v.name_en as name,v.type,v.status");
              }elseif($details["lang"]=='fr')
              {
                $this->db->select("v.id,v.name_fr as name,v.type,v.status");
              }else{
                $this->db->select("v.id,v.name_en as name,v.type,v.status");
              }
				
				$this->db->from('variance v' );
				$this->db->where("v.status",1);
				$query=$this->db->get();
                $db_error = $this->db->error();
                $listVariance = $query->result_array();

              $results=[];
              foreach($itemdata as $val){
                $result=[];
                $result["item_id"]=base64_encode($val["item_id"]);
                $result["sl"]=$val["item_id"];
                 $result["seller_id"]=base64_encode($val["seller_id"]);
                  $result["local_sup_id"]=base64_encode($val["local_sup_id"]);
                  $result["category_id"]=base64_encode($val["category_id"]);
                   $result["price_uom_id"]=base64_encode($val["price_uom_id"]);
                   $result["moq_uom_id"]=base64_encode($val["moq_uom_id"]);
                   $result["fixedPrice"]=$val["fixedPrice"];
                    $result["delivery_available"]=$val["delivery_available"];
                     $result["shipping_time"]=$val["shipping_time"];
                  if(isset($val["category_json"])){
                    $val["category_json"]=json_decode($val["category_json"]);
                    foreach($val["category_json"] as $val1){
                       $category_json[]=base64_encode($val1);
                    }
                    $result["category_id"]=json_encode($category_json);
                   }
                      $result["item_name_en"]=$val["item_name_en"];
                      $result["description_en"]=$val["description_en"];
                      $result["moq_uom_name_en"]=$val["moq_uom_name_en"];
                      $result["price_uom_name_en"]=$val["price_uom_name_en"];
                       $result["item_name_fr"]=$val["item_name_fr"];
                      $result["description_fr"]=$val["description_fr"];
                      $result["moq_uom_name_fr"]=$val["moq_uom_name_fr"];
                      $result["price_uom_name_fr"]=$val["price_uom_name_fr"];

                  if($details["lang"]=='en'){
                     $result["item_name"]=$val["item_name_en"];
                      $result["description"]=$val["description_en"];
                      $result["lead_time"]=$val["lead_time"];
                      $result["moq_uom_name"]=$val["moq_uom_name_en"];
                      $result["price_uom_name"]=$val["price_uom_name_en"];
                      
                     
                  }else{
                     $result["item_name"]=$val["item_name_fr"];
                     $result["description"]=$val["description_fr"];
                      $result["lead_time"]=$val["lead_time"];
                      $result["moq_uom_name"]=$val["moq_uom_name_fr"];
                      $result["price_uom_name"]=$val["price_uom_name_fr"];
                     
                  }
                  $result["sellerinfo"]["business_name"]=$val["business_name"];
                  $result["sellerinfo"]["address"]=$val["address"];
                  $result["sellerinfo"]["country"]=$val["country"];
                  $result["sellerinfo"]["state"]=$val["state"];
                  $result["sellerinfo"]["city"]=$val["city"];
                  $result["sellerinfo"]["displayLocal"]=$val["displayLocal"];
                  $new_attribute_variance_arr=[];
                  if($val["variance"] != '')
                {
                    $attribute_variance_arr = json_decode($val["variance"],true);
                    
                    if(is_array($attribute_variance_arr) && count($attribute_variance_arr)>0)
                    {
                        foreach($attribute_variance_arr as $hk=>$hv)
                        {
                            //$listVariance
                            foreach($listVariance as $lvk=>$lvv)
                            {
                              if($lvv['id'] == current(array_keys($hv)))
                              {
                                $varianceName = $lvv['name'];
                              }

                            }
                            
                            
                            foreach($hv as $hkk=>$hvv)
                            {
                                
                            foreach($hvv as $hkkk=>$hvvv)
                            {
                                $new_attribute_variance_arr[$varianceName][] = $hvvv;
                                
                            }
                                
                            }

                            
                        }
                    }
                }
                
                $result["modified_variance"]=json_encode($new_attribute_variance_arr);


                $new_attribute_define_arr=[];
                if($val["attribute"] != '')
                {
                    $attribute_define_arr = json_decode($val["attribute"],true);
                    
                    if(is_array($attribute_define_arr) && count($attribute_define_arr)>0)
                    {
                        foreach($attribute_define_arr as $hk=>$hv)
                        {                            
                            
                            foreach($hv as $hkk=>$hvv)
                            {
                                
                                $new_attribute_define_arr[$hkk] = $hvv;
                                
                            }

                            
                        }
                    }
                }                
                $result["modified_attribute"]=json_encode($new_attribute_define_arr);
                
                $result["price_min"]=$val["price_min"];
                $result["price_max"]=$val["price_max"];
                $result["moq"]=$val["moq"];
                $result["currency"]=$val["currency"];
                $result["keywords"]=$val["keywords"];  
                $result["packaging"]=$val["packaging"];  
                $result["thumbnail"]=$val["thumbnail"];   
                $result["variance"]=json_decode($val["variance"],true);
                $result["attribute"]=json_decode($val["attribute"],true);
                
                $result["showcase"]=$val["showcase"];   
                 $result["item_image"]=$val["item_image"];
                   $result["active"]=$val["active"];
                    $result["is_approved"]=$val["is_approved"];
                     $result["supplier_name"]=$val["name"];
                      $result["supplier_phone"]=$val["phone"];
                       $result["supplier_email"]=$val["email"];
                 $results[]=$result;

              }

              


              $status = parent::HTTP_OK;
                                $response = ['status' => $status,'msg'=>' Seller Item ',"item"=> $results,"variance"=>$listVariance];
                                 $this->response($response, $status);


           }
         } catch (Exception $e) {

            $this->response(['msg' => 'server busy .Please try again some time.','field'=>'user_name'], parent::HTTP_NOT_FOUND);
        }
    }

    public function getsellerItembyid_post(){
    	try {
    		$details=$this->input->post();
         	$data = $this->verify_request();
        	 if(trim($data["id"])=='' && (!($data["supplier_YN"]==1 || $data["supplier_YN"]==2))){
                 $this->response(['msg' => 'Unauthorized Access!','field'=>'supp_id'], parent::HTTP_UNAUTHORIZED);
             }else if(!((isset($details["item_id"])  &&  trim($details["item_id"])!=''))){
                 $this->response(['msg' => 'Invalid Item!','field'=>'item_id'], parent::HTTP_BAD_REQUEST);
             }else{
				
				$itemdata=$this->itemModel->getItem(["item_id"=>base64_decode($details["item_id"])]);

				   $results=[];
            	foreach($itemdata as $val){
					$result=[];
					$result["item_id"]=base64_encode($val["item_id"]);
					$result["seller_id"]=base64_encode($val["seller_id"]);
					$result["local_sup_id"]=base64_encode($val["local_sup_id"]);
					$result["category_id"]=base64_encode($val["category_id"]);
                    $result["price_uom_id"]=base64_encode($val["price_uom_id"]);
                    $result["moq_uom_id"]=base64_encode($val["moq_uom_id"]);
                   
	                  if(isset($val["category_json"])){
	                    $val["category_json"]=json_decode($val["category_json"]);
	                    foreach($val["category_json"] as $val1){
	                       $category_json[]=base64_encode($val1);
	                    }
	                    $result["category_json"]=$category_json;
	                   }

						$result["item_name_en"]=$val["item_name_en"];
						$result["description_en"]=$val["description_en"];
						$result["lead_time"]=$val["lead_time"];
						$result["price_uom_name_en"]=$val["price_uom_name_en"];
            $result["moq_uom_name_en"]=$val["moq_uom_name_en"];
            $result["fixedPrice"]=$val["fixedPrice"];
             $result["delivery_available"]=$val["delivery_available"];
             $result["shipping_time"]=$val["shipping_time"];
          	$result["item_name_fr"]=$val["item_name_fr"];
						$result["description_fr"]=$val["description_fr"];
						$result["price_uom_name_fr"]=$val["price_uom_name_fr"];
						$result["moq_uom_name_fr"]=$val["moq_uom_name_fr"];

                 
                        $result["on_display"]=$val["on_display"];
                         $result["price_min"]=$val["price_min"];
						$result["price_max"]=$val["price_max"];
						$result["moq"]=$val["moq"];
                        $result["currency"]=$val["currency"];
                        $result["keywords"]=$val["keywords"];
                        $result["packaging"]=$val["packaging"];
						 $result["variance"]='';
		                if($val["variance"] != '')
		                {
		                    $attribute_variance_arr = json_decode($val["variance"],true);
		                     $result["variance"]=$attribute_variance_arr;
		                    
		                }
                
               	

		                 $result["attribute"]='';

	                if($val["attribute"] != '')
	                {
	                    $attribute_define_arr = json_decode($val["attribute"],true);
	                    $result["attribute"]=$attribute_define_arr;
	                   
	                }                
                

	              
               	$result["item_image"]=json_decode($val["item_image"],true);
                $result["active"]=$val["active"];
                $result["on_display"]=$val["on_display"];
                $result["thumbnail"]=$val["thumbnail"];
                $result["showcase"]=$val["showcase"];
				$result["is_approved"]=$val["is_approved"];
				$result["supplier_name"]=$val["name"];
				$result["supplier_phone"]=$val["phone"];
				$result["supplier_email"]=$val["email"];
				$results=$result;

              }

              $status = parent::HTTP_OK;
	            $response = ['status' => $status,'msg'=>' Seller Item ',"item"=> $results];
	             $this->response($response, $status);
             }
    	  } catch (Exception $e) {

            $this->response(['msg' => 'server busy .Please try again some time.','field'=>'user_name'], parent::HTTP_NOT_FOUND);
        }	
    }
     public function getsellerItem_post(){
    	try {
    		$details=$this->input->post();
         	$data = $this->verify_request();
        	 if(trim($data["id"])=='' && (!($data["supplier_YN"]==1 || $data["supplier_YN"]==2))){
                 $this->response(['msg' => 'Unauthorized Access!','field'=>'supp_id'], parent::HTTP_UNAUTHORIZED);
             }else{
				$limit=20;
				$start=0;
				if(!isset($details["pageno"])){
					$details["pageno"]=1;
				}

				if((isset($details["pageno"])&& trim($details["pageno"])!='')){
					$start=($details["pageno"]-1)* $limit;
				}
			//	$itemdata=$this->itemModel->getItem(["supp_id"=>$data["id"]],$limit,$start);
				$itemdata=$this->itemModel->getItem(["supp_id"=>$data["id"]]);
				   $results=[];
            	foreach($itemdata as $val){
					$result=[];
					$result["item_id"]=base64_encode($val["item_id"]);
					$result["seller_id"]=base64_encode($val["seller_id"]);
					$result["local_sup_id"]=base64_encode($val["local_sup_id"]);
					$result["category_id"]=base64_encode($val["category_id"]);
                    $result["price_uom_id"]=base64_encode($val["price_uom_id"]);
                    $result["moq_uom_id"]=base64_encode($val["moq_uom_id"]);
	                  if(isset($val["category_json"])){
	                    $val["category_json"]=json_decode($val["category_json"]);
	                    foreach($val["category_json"] as $val1){
	                       $category_json[]=base64_encode($val1);
	                    }
	                    $result["category_id"]=$category_json;
	                   }

						$result["item_name_en"]=$val["item_name_en"];
						$result["description_en"]=$val["description_en"];
						$result["lead_time"]=$val["lead_time"];
						$result["price_uom_name_en"]=$val["price_uom_name_en"];
						$result["moq_uom_name_en"]=$val["moq_uom_name_en"];

						$result["item_name_fr"]=$val["item_name_fr"];
						$result["description_fr"]=$val["description_fr"];
						$result["price_uom_name_fr"]=$val["price_uom_name_fr"];
						$result["moq_uom_name_fr"]=$val["moq_uom_name_fr"];

                 
						$result["price_min"]=$val["price_min"];
						$result["price_max"]=$val["price_max"];
						$result["moq"]=$val["moq"];
                        $result["currency"]=$val["currency"];
                        $result["keywords"]=$val["keywords"];
                        $result["packaging"]=$val["packaging"];
						 $result["variance"]='';
		                if($val["variance"] != '')
		                {
		                    $attribute_variance_arr = json_decode($val["variance"],true);
		                     $result["variance"]=$attribute_variance_arr;
		                    
		                }
                
               	

		                 $result["attribute"]='';

	                if($val["attribute"] != '')
	                {
	                    $attribute_define_arr = json_decode($val["attribute"],true);
	                    $result["attribute"]=$attribute_define_arr;
	                   
	                }                
                

	              
               	$result["item_image"]=json_decode($val["item_image"],true);
                $result["active"]=$val["active"];
                $result["thumbnail"]=$val["thumbnail"];
                $result["showcase"]=$val["showcase"];
				$result["is_approved"]=$val["is_approved"];
				$result["supplier_name"]=$val["name"];
				$result["supplier_phone"]=$val["phone"];
				$result["supplier_email"]=$val["email"];
				$results[]=$result;

              }

              $status = parent::HTTP_OK;
	            $response = ['status' => $status,'msg'=>' Seller Item ',"item"=> $results];
	             $this->response($response, $status);
             }
    	  } catch (Exception $e) {

            $this->response(['msg' => 'server busy .Please try again some time.','field'=>'user_name'], parent::HTTP_NOT_FOUND);
        }	
    }
    public function addItem_post(){

       try {
        $details=$this->input->post();
         $data = $this->verify_request();
        
          if(!empty($details)){
             if(trim($data["id"])=='' && (!($data["supplier_YN"]==1 || $data["supplier_YN"]==2))){
                 $this->response(['msg' => 'Unauthorized Access!','field'=>'supp_id'], parent::HTTP_UNAUTHORIZED);
             }else if(!((isset($details["category_id"])  &&  trim($details["category_id"])!=''))){
                $this->response(['msg' => 'Invalid Category!','field'=>'category_id'], parent::HTTP_BAD_REQUEST);
            }else if(!((isset($details["category_json"])  &&  !empty($details["category_json"])))){
                $this->response(['msg' => 'Invalid Category breadcumb data!','field'=>'category_json'], parent::HTTP_BAD_REQUEST);
            }else if(!((isset($details["item_name_en"])  &&  trim($details["item_name_en"])!='') )){

                 $this->response(['msg' => 'Invalid Item Name In English!','field'=>'item_name_en'], parent::HTTP_BAD_REQUEST);

             }else if(!( (isset($details["item_name_fr"])  &&  trim($details["item_name_fr"])!=''))){

                $this->response(['msg' => 'Invalid Item Name In French!','field'=>'item_nitem_name_frame_en'], parent::HTTP_BAD_REQUEST);

            }else if(!((isset($details["description_en"])  &&  trim($details["description_en"])!='') )){

                $this->response(['msg' => 'Invalid Item Details In English!','field'=>'description_en'], parent::HTTP_BAD_REQUEST);

            }else if(!( (isset($details["description_fr"])  &&  trim($details["description_fr"])!=''))){

               $this->response(['msg' => 'Invalid Item Details In French!','field'=>'description_fr'], parent::HTTP_BAD_REQUEST);

            }else if(!( (isset($_FILES["thumbnail"])  &&  trim($_FILES["thumbnail"]["name"])!=''))){

                $this->response(['msg' => 'Invalid thumbnail!','field'=>'thumbnail'], parent::HTTP_BAD_REQUEST);

            }else if(!( (isset($details["keywords"])  &&  trim($details["keywords"])!=''))){

                $this->response(['msg' => 'Invalid Item Keywords!','field'=>'keywords'], parent::HTTP_BAD_REQUEST);
 
             }else if(trim($details["price_min"])==''){
                 $this->response(['msg' => 'Please Enter Minimum Price','field'=>'price_min'], parent::HTTP_BAD_REQUEST);
              }else if(trim($details["price_min"])!='' && !preg_match('/^(?:[1-9]\d*|0)?(?:\.\d+)?$/', $details["price_min"])){
                 $this->response(['msg' => 'Please Enter Valid Minimum Price','field'=>'min_price'], parent::HTTP_BAD_REQUEST);
              }else if(trim($details["price_max"])==''){
                 $this->response(['msg' => 'Please Enter Maximum Price','field'=>'price_max'], parent::HTTP_BAD_REQUEST);
              }else if(trim($details["price_max"])!='' && !preg_match('/^(?:[1-9]\d*|0)?(?:\.\d+)?$/', $details["price_max"])){
                 $this->response(['msg' => 'Please Enter Valid Maximum Price','field'=>'price_max'], parent::HTTP_BAD_REQUEST);
              }else if(trim($details["currency"])==''){
                 $this->response(['msg' => 'Please Enter Currency','field'=>'currency'], parent::HTTP_BAD_REQUEST);

              }else if(trim($details["price_uom_id"])==''){
                 $this->response(['msg' => 'Please select Price UOM','field'=>'price_uom_id'], parent::HTTP_BAD_REQUEST);
              }else if(!( (isset($details["moq"])  &&  trim($details["moq"])!=''))){

                $this->response(['msg' => 'Invalid Item Min Order!','field'=>'moq'], parent::HTTP_BAD_REQUEST);
 
             }else if(trim($details["moq"])!='' && !preg_match('/^(?<=\s|^)\d+(?=\s|$)$/', $details["moq"])){
                $this->response(['msg' => 'Please Enter Valid Item Min Order','field'=>'moq'], parent::HTTP_BAD_REQUEST);
             }else if(trim($details["price_uom_id"])==''){
                $this->response(['msg' => 'Please select Item Min Order UOM','field'=>'price_uom_id'], parent::HTTP_BAD_REQUEST);
             }else{

                  $inserted_data=[];
                  $inserted_data["supp_id"]=$data["id"];
                  $inserted_data["local_sup_id"]=(isset($details["local_sup_id"]))?base64_decode($details["local_sup_id"]):0;
                  $inserted_data["item_name_en"]=(isset($details["item_name_en"]))?$details["item_name_en"]:'';
                  $inserted_data["item_name_fr"]=(isset($details["item_name_fr"]))?$details["item_name_fr"]:'';
                  $inserted_data["description_en"]=(isset($details["description_en"]))?$details["description_en"]:'';
                  $inserted_data["description_fr"]=(isset($details["description_fr"]))?$details["description_fr"]:'';
                  $inserted_data["keywords"]=(isset($details["keywords"]))?$details["keywords"]:'';
                  $inserted_data["category_id"]=(isset($details["category_id"]))?base64_decode($details["category_id"]):'';
                  $inserted_data["price_min"]=(isset($details["price_min"]))?$details["price_min"]:'';
                  $inserted_data["price_max"]=(isset($details["price_max"]))?$details["price_max"]:'';
                  $inserted_data["currency"]=(isset($details["currency"]))?$details["currency"]:'';
                  $inserted_data["lead_time"]=(isset($details["lead_time"]))?$details["lead_time"]:'';
                  $inserted_data["price_uom_id"]=(isset($details["price_uom_id"]))?base64_decode($details["price_uom_id"]):'';
                  $inserted_data["moq"]=(isset($details["moq"]))?$details["moq"]:'';
                  $inserted_data["moq_uom_id"]=(isset($details["moq_uom_id"]))?base64_decode($details["moq_uom_id"]):'';
                  $inserted_data["packaging"]=(isset($details["packaging"]))?$details["packaging"]:'';
                  $inserted_data["variance"]=(isset($details["variance"]))?$details["variance"]:'';
                  $inserted_data["attribute"]=(isset($details["attribute"]))?$details["attribute"]:'';
                  $inserted_data["on_display"]=(isset($details["on_display"]))?$details["on_display"]:1;
                  $inserted_data["attribute"]=(isset($details["attribute"]))?$details["attribute"]:'';
                  $inserted_data["fixedPrice"]=(isset($details["fixedPrice"]))?$details["fixedPrice"]:'';
                  $inserted_data["showcase"]=(isset($details["showcase"]))?$details["showcase"]:'';

                  $inserted_data["delivery_available"]=(isset($details["delivery_available"]))?$details["delivery_available"]:'';
                  $inserted_data["shipping_time"]=(isset($details["shipping_time"]))?$details["shipping_time"]:'';

                 $uploadPath = '../admin/uploads/selleritem/';

                  if ((($_FILES["thumbnail"]["type"] == "image/gif")  || ($_FILES["thumbnail"]["type"] == "image/jpeg")  || ($_FILES["thumbnail"]["type"] == "image/jpg")  || ($_FILES["thumbnail"]["type"]== "image/png")) ){
                    $fileName1 = time().'-'.$_FILES["thumbnail"]["name"];
                    $this->load->library('image_lib');
                    if ( move_uploaded_file($_FILES['thumbnail']['tmp_name'],  $uploadPath."".$fileName1)){
                                $config['image_library'] = 'gd2';
                                $config['source_image'] = $uploadPath."".$fileName1;
                                $config['create_thumb'] = false;
                                $config['maintain_ratio'] = TRUE;
                                $config['width']     = 368;
                                $config['height']   = 368;
                                $this->image_lib->clear();
                                $this->image_lib->initialize($config);
                                $this->image_lib->resize();
                                $inserted_data["thumbnail"]=$fileName1;
                                $inserted_data["active"]=1;
                                $category_json=[];
                                if(isset($details["category_json"])){
             
                                 foreach($details["category_json"] as $val){
                                    $category_json[]=base64_decode($val);
                                 }
                                }
                                 $inserted_data["category_json"]=json_encode($category_json);
                                   $images=[];
                                   $id=$this->itemModel->insertItem($inserted_data);
                                    $images=[];
                                  if(!empty($_FILES) && isset($_FILES["more_image"]) && !empty($_FILES["more_image"]["name"])){
                                    $this->load->library('image_lib');
                                      foreach($_FILES["more_image"]["name"] as $key=>$val){
                                          if ((($_FILES["more_image"]["type"][$key] == "image/gif")  || ($_FILES["more_image"]["type"][$key] == "image/jpeg")  || ($_FILES["more_image"]["type"][$key] == "image/jpg")  || ($_FILES["more_image"]["type"][$key] == "image/png")) ){
                                                
                                              
                                                $fileName = time().'-'.$val;
                                                if ( move_uploaded_file($_FILES['more_image']['tmp_name'][$key],  $uploadPath."".$fileName)){
                                                    $config['image_library'] = 'gd2';
                                                     $config['source_image'] = $uploadPath."".$fileName;
                                                     $config['create_thumb'] = false;
                                                     $config['maintain_ratio'] = TRUE;
                                                     $config['width']     = 368;
                                                     $config['height']   = 368;
                                                     $this->image_lib->clear();
                                                     $this->image_lib->initialize($config);
                                                     $this->image_lib->resize();
                                                     $image=["name"=>$fileName];
                                                     
                                                     $images[]=$image;
             
                                                }
                                          }
                                      }
                                  }
                                  if(!empty($images)){
                                    $img=json_encode($images);
                                     $this->itemModel->updateItem(["item_image"=>$img],["id"=>$id]);
                                     
                                  }
                               $status = parent::HTTP_OK;
                                             $response = ['status' => $status,'msg'=>' Seller Item  Successfully Added'];
                                              $this->response($response, $status);
             

                    }else{
                    $this->response(['msg' => 'Invalid thumbnail format!','field'=>'thumbnail'], parent::HTTP_BAD_REQUEST);
                    }

                  }else{
                    $this->response(['msg' => 'Invalid thumbnail format!','field'=>'thumbnail'], parent::HTTP_BAD_REQUEST);
                  }
                  
                  
                



              }


             
            
            
        }else{
            $status = parent::HTTP_BAD_REQUEST;
              $response = ['status' => $status, 'msg' => 'Invalid Input! '];
              $this->response($response, $status);
        }

       } catch (Exception $e) {

            $this->response(['msg' => 'server busy .Please try again some time.','field'=>'user_name'], parent::HTTP_NOT_FOUND);
        }
    }
  
    public function updateItem_post(){

       try {
        $details=$this->input->post();
         $data = $this->verify_request();

         // $this->response(['msg' => '','field'=>'', 'data'=>$details], parent::HTTP_NOT_FOUND);

         // die();
        
          if(!empty($details)){
             if(trim($data["id"])=='' && (!($data["supplier_YN"]==1 || $data["supplier_YN"]==2))){
                 $this->response(['msg' => 'Unauthorized Access!','field'=>'supp_id'], parent::HTTP_UNAUTHORIZED);
             }else if(!(isset($details["item_id"]) && trim($details["item_id"])!='')){
                 $this->response(['msg' => 'Invalid Item','field'=>'item_id'], parent::HTTP_BAD_REQUEST);
              }else if(!((isset($details["category_id"])  &&  trim($details["category_id"])!=''))){
                $this->response(['msg' => 'Invalid Category!','field'=>'category_id'], parent::HTTP_BAD_REQUEST);
            }else if(!((isset($details["category_json"])  &&  !empty($details["category_json"])))){
                $this->response(['msg' => 'Invalid Category breadcumb data!','field'=>'category_json'], parent::HTTP_BAD_REQUEST);
            }else if(!((isset($details["item_name_en"])  &&  trim($details["item_name_en"])!='') )){

                 $this->response(['msg' => 'Invalid Item Name In English!','field'=>'item_name_en'], parent::HTTP_BAD_REQUEST);

             }else if(!( (isset($details["item_name_fr"])  &&  trim($details["item_name_fr"])!=''))){

                $this->response(['msg' => 'Invalid Item Name In French!','field'=>'item_nitem_name_frame_en'], parent::HTTP_BAD_REQUEST);

            }else if(!((isset($details["description_en"])  &&  trim($details["description_en"])!='') )){

                $this->response(['msg' => 'Invalid Item Details In English!','field'=>'description_en'], parent::HTTP_BAD_REQUEST);

            }else if(!( (isset($details["description_fr"])  &&  trim($details["description_fr"])!=''))){

               $this->response(['msg' => 'Invalid Item Details In French!','field'=>'description_fr'], parent::HTTP_BAD_REQUEST);

            }else if(!( (isset($details["keywords"])  &&  trim($details["keywords"])!=''))){

                $this->response(['msg' => 'Invalid Item Keywords!','field'=>'keywords'], parent::HTTP_BAD_REQUEST);
 
             }else if(trim($details["price_min"])==''){
                 $this->response(['msg' => 'Please Enter Minimum Price','field'=>'price_min'], parent::HTTP_BAD_REQUEST);
              }else if(trim($details["price_min"])!='' && !preg_match('/^(?:[1-9]\d*|0)?(?:\.\d+)?$/', $details["price_min"])){
                 $this->response(['msg' => 'Please Enter Valid Minimum Price','field'=>'min_price'], parent::HTTP_BAD_REQUEST);
              }else if(trim($details["price_max"])==''){
                 $this->response(['msg' => 'Please Enter Maximum Price','field'=>'price_max'], parent::HTTP_BAD_REQUEST);
              }else if(trim($details["price_max"])!='' && !preg_match('/^(?:[1-9]\d*|0)?(?:\.\d+)?$/', $details["price_max"])){
                 $this->response(['msg' => 'Please Enter Valid Maximum Price','field'=>'price_max'], parent::HTTP_BAD_REQUEST);
              }else if(trim($details["currency"])==''){
                 $this->response(['msg' => 'Please Enter Currency','field'=>'currency'], parent::HTTP_BAD_REQUEST);

              }else if(trim($details["price_uom_id"])==''){
                 $this->response(['msg' => 'Please select Price UOM','field'=>'price_uom_id'], parent::HTTP_BAD_REQUEST);
              }else if(!( (isset($details["moq"])  &&  trim($details["moq"])!=''))){

                $this->response(['msg' => 'Invalid Item Min Order!','field'=>'moq'], parent::HTTP_BAD_REQUEST);
 
             }else if(trim($details["moq"])!='' && !preg_match('/^(?<=\s|^)\d+(?=\s|$)$/', $details["moq"])){
                $this->response(['msg' => 'Please Enter Valid Item Min Order','field'=>'moq'], parent::HTTP_BAD_REQUEST);
             }else if(trim($details["price_uom_id"])==''){
                $this->response(['msg' => 'Please select Item Min Order UOM','field'=>'price_uom_id'], parent::HTTP_BAD_REQUEST);
             }else{

                  $itemdata=$this->itemModel->getItem(["item_id"=>base64_decode($details["item_id"])]);
                  if(empty($itemdata)){
                     $this->response(['msg' => 'Invalid Item','field'=>'item_id'], parent::HTTP_BAD_REQUEST);
                   }else{
                    $itemdata=current($itemdata);
                    $updated_data=[];
                    $updated_data["supp_id"]=$data["id"];
                    $updated_data["local_sup_id"]=(isset($details["local_sup_id"]))?base64_decode($details["local_sup_id"]):0;
                    $updated_data["item_name_en"]=(isset($details["item_name_en"]))?$details["item_name_en"]:'';
                    $updated_data["item_name_fr"]=(isset($details["item_name_fr"]))?$details["item_name_fr"]:'';
                    $updated_data["description_en"]=(isset($details["description_en"]))?$details["description_en"]:'';
                    $updated_data["description_fr"]=(isset($details["description_fr"]))?$details["description_fr"]:'';
                    $updated_data["active"]=(isset($details["active"]))?$details["active"]:$itemdata["active"];
                    $updated_data["on_display"]=(isset($details["on_display"]))?$details["on_display"]:$itemdata["on_display"];
                    $updated_data["keywords"]=(isset($details["keywords"]))?$details["keywords"]:'';
                    $updated_data["category_id"]=(isset($details["category_id"]))?base64_decode($details["category_id"]):'';
                    $updated_data["price_min"]=(isset($details["price_min"]))?$details["price_min"]:'';
                    $updated_data["price_max"]=(isset($details["price_max"]))?$details["price_max"]:'';
                    $updated_data["currency"]=(isset($details["currency"]))?$details["currency"]:'';
                    $updated_data["lead_time"]=(isset($details["lead_time"]))?$details["lead_time"]:'';
                    $updated_data["price_uom_id"]=(isset($details["price_uom_id"]))?base64_decode($details["price_uom_id"]):'';
                    $updated_data["moq"]=(isset($details["moq"]))?$details["moq"]:'';
                    $updated_data["moq_uom_id"]=(isset($details["moq_uom_id"]))?base64_decode($details["moq_uom_id"]):'';
                    $updated_data["packaging"]=(isset($details["packaging"]))?$details["packaging"]:'';
                    $updated_data["variance"]=(isset($details["variance"]))?$details["variance"]:'';
                    $updated_data["attribute"]=(isset($details["attribute"]))?$details["attribute"]:'';
                    $updated_data["attribute"]=(isset($details["attribute"]))?$details["attribute"]:'';
                    $updated_data["showcase"]=(isset($details["showcase"]))?$details["showcase"]:'';
                    $updated_data["fixedPrice"]=(isset($details["fixedPrice"]))?$details["fixedPrice"]:'';


                  $updated_data["delivery_available"]=(isset($details["delivery_available"]))?$details["delivery_available"]:'';
                  $updated_data["shipping_time"]=(isset($details["shipping_time"]))?$details["shipping_time"]:'';
                     $uploaupdated_datadPath = '../admin/uploads/selleritem/';
                    $error='';
                    $this->load->library('image_lib');
                     if(!empty($_FILES) && isset($_FILES["thumbnail"]) && trim($_FILES["thumbnail"]["name"])!=''){
                        if ((($_FILES["thumbnail"]["type"] == "image/gif")  || ($_FILES["thumbnail"]["type"] == "image/jpeg")  || ($_FILES["thumbnail"]["type"] == "image/jpg")  || ($_FILES["thumbnail"]["type"]== "image/png")) ){
                            $fileName1 = time().'-'.$_FILES["thumbnail"]["name"];
                            if ( move_uploaded_file($_FILES['thumbnail']['tmp_name'],  $uploadPath."".$fileName1)){
                                $config['image_library'] = 'gd2';
                                $config['source_image'] = $uploadPath."".$fileName1;
                                $config['create_thumb'] = false;
                                $config['maintain_ratio'] = TRUE;
                                $config['width']     = 368;
                                $config['height']   = 368;
                                $this->image_lib->clear();
                                $this->image_lib->initialize($config);
                                $this->image_lib->resize();
                                $updated_data["thumbnail"]=$fileName1;
                                $uploadPath = '../admin/uploads/selleritem/';
                                if(file_exists($uploadPath.$itemdata["thumbnail"])){
                                  unlink($uploadPath.$itemdata["thumbnail"]);
                                }

                            }else{
                                $error='Invalid thumbnail format!'; 
                            }
                        }else{
                            $error='Invalid thumbnail format!';
                        }

                     }
                        if(trim($error)==''){
                            $category_json=[];
                            if(isset($details["category_json"])){
     
                             foreach($details["category_json"] as $val){
                                $category_json[]=base64_decode($val);
                             }
                            }
                             $updated_data["category_json"]=json_encode($category_json);
                             $images=[]; 
                         
                          $old_image=json_decode($itemdata["item_image"]);
                          foreach($old_image as $vv1){
                                  $image=["name"=>$vv1->name];
                              
                                $images[]=$image;
                            }

                            if(!empty($_FILES) && isset($_FILES["more_image"]) && !empty($_FILES["more_image"]["name"])){
                                $this->load->library('image_lib');
                                foreach($_FILES["more_image"]["name"] as $key=>$val){
                                    if ((($_FILES["more_image"]["type"][$key] == "image/gif")  || ($_FILES["more_image"]["type"][$key] == "image/jpeg")  || ($_FILES["more_image"]["type"][$key] == "image/jpg")  || ($_FILES["more_image"]["type"][$key] == "image/png")) ){
                                          
                                           $uploadPath = '../admin/uploads/selleritem/';
                                          $fileName = time().'-'.$val;
                                          if ( move_uploaded_file($_FILES['more_image']['tmp_name'][$key],  $uploadPath."".$fileName)){
                                              $config['image_library'] = 'gd2';
                                               $config['source_image'] = $uploadPath."".$fileName;
                                               $config['create_thumb'] = false;
                                               $config['maintain_ratio'] = TRUE;
                                               $config['width']     = 368;
                                               $config['height']   = 368;
                                               $this->image_lib->clear();
                                               $this->image_lib->initialize($config);
                                               $this->image_lib->resize();
                                               $image=["name"=>$fileName];
                                               
                                               $images[]=$image;

                                          }
                                    }
                                }
                            }
                            $updated_data["item_image"]=json_encode($images);
                            $this->itemModel->updateItem($updated_data,["id"=>base64_decode($details["item_id"])]);
                            
                            $status = parent::HTTP_OK;
                                 $response = ['status' => $status,'msg'=>' Seller Item  Successfully updated'];
                                  $this->response($response, $status);

                        }else{
                            $this->response(['msg' => $error,'field'=>'thumbnail'], parent::HTTP_BAD_REQUEST);
                        }
                     
                          
                         
                        
                            
                             


                    }
                  
                   
                    
                



              }


             
            
            
        }else{
           $status = parent::HTTP_BAD_REQUEST;
              $response = ['status' => $status, 'msg' => 'Invalid Input! '];
              $this->response($response, $status);
        }

       } catch (Exception $e) {

            $this->response(['msg' => 'server busy .Please try again some time.','field'=>'user_name'], parent::HTTP_NOT_FOUND);
        }
    }
    
   public function deleteSellerItemImage_post(){
      try {
        $data=[];
       $result=[];
       
         $data = $this->verify_request();
            $details=$this->post();
          if(!empty( $details)){
               if(trim($data["id"])=='' && (!($data["supplier_YN"]==1 || $data["supplier_YN"]==2))){
                 $this->response(['msg' => 'Unauthorized Access!','field'=>'supp_id'], parent::HTTP_UNAUTHORIZED);
             }else if(!((isset($details["image_name"])  &&  trim($details["image_name"])!=''))){
                 $this->response(['msg' => 'Invalid Image!','field'=>'image_name'], parent::HTTP_BAD_REQUEST);
             }else if(!(isset($details["item_id"]) && trim($details["item_id"])!='')){
                 $this->response(['msg' => 'Invalid Item','field'=>'item_id'], parent::HTTP_BAD_REQUEST);
              }else{
                $itemdata=$this->itemModel->getItem(["item_id"=>base64_decode($details["item_id"])]);
                  if(empty($itemdata)){
                     $this->response(['msg' => 'Invalid Item','field'=>'item_id'], parent::HTTP_BAD_REQUEST);
                   }else{

                     $itemdata=current($itemdata);
                      if( $itemdata["seller_id"]!=$data["id"]){
                          $this->response(['msg' => 'Unauthorized Access!','field'=>'supp_id'], parent::HTTP_UNAUTHORIZED);
                        }else{
                            $old_image=json_decode($itemdata["item_image"]);
                            $imgg1=array_column($old_image, "name");

                             if(!in_array(trim($details["image_name"]), $imgg1)){
                                $this->response(['msg' => 'Image not found','field'=>'image_name'], parent::HTTP_BAD_REQUEST);
                             }else{
                                $uploadPath = '../admin/uploads/selleritem/';
                                if(file_exists($uploadPath.$details["image_name"])){
                                 unlink($uploadPath.$details["image_name"]);
                                }
                                $images=[];
                                 foreach($old_image as $vv1){
                                     
                                      if( trim($vv1->name)!=$details["image_name"]){
                                           $image=["name"=>$vv1->name];
                                           $images[]=$image;
                                     }
                                   
                                }

                               
                                    $img=json_encode($images);
                               $this->itemModel->updateItem(["item_image"=>$img],["id"=>base64_decode($details["item_id"])]);
                                $status = parent::HTTP_OK;
                                    $response = ['status' => $status,'msg'=>' Seller Item Image Successfully Deleted'];
                                     $this->response($response, $status);
                   
                             }

                        }
                     
                    
                    
                   }
              
             }
          
            
          }else{
            $status = parent::HTTP_BAD_REQUEST;
              $response = ['status' => $status, 'msg' => 'Invalid Input! '];
              $this->response($response, $status);
        }
      } catch (Exception $e) {

          $this->response(['msg' => 'server busy .Please try again some time.','field'=>'item_id'], parent::HTTP_NOT_FOUND);
      }
    }
    public function deleteSellerItem_post(){
      try {
        $data=[];
       $result=[];
       
         $data = $this->verify_request();
            $details=$this->post();
          if(!empty( $details)){
               if(trim($data["id"])=='' && (!($data["supplier_YN"]==1 || $data["supplier_YN"]==2))){
                 $this->response(['msg' => 'Unauthorized Access!','field'=>'supp_id'], parent::HTTP_UNAUTHORIZED);
             }else if(!(isset($details["item_id"]) && trim($details["item_id"])!='')){
                 $this->response(['msg' => 'Invalid Item','field'=>'item_id'], parent::HTTP_BAD_REQUEST);
              }else{
                $itemdata=$this->itemModel->getItem(["item_id"=>base64_decode($details["item_id"])]);
                  if(empty($itemdata)){
                     $this->response(['msg' => 'Invalid Item','field'=>'item_id'], parent::HTTP_BAD_REQUEST);
                   }else{

                     $itemdata=current($itemdata);
                      if( $itemdata["seller_id"]!=$data["id"]){
                          $this->response(['msg' => 'Unauthorized Access!','field'=>'supp_id'], parent::HTTP_UNAUTHORIZED);
                        }else{
                             $this->itemModel->updateItem(["is_deleted"=>1],["id"=>base64_decode($details["item_id"])]);
                                $status = parent::HTTP_OK;
                                    $response = ['status' => $status,'msg'=>' Seller Item  Successfully Deleted'];
                                     $this->response($response, $status);
                   

                        }
                     
                    
                    
                   }
              
             }
          
            
          }else{
            $status = parent::HTTP_BAD_REQUEST;
              $response = ['status' => $status, 'msg' => 'Invalid Input! '];
              $this->response($response, $status);
        }
      } catch (Exception $e) {

          $this->response(['msg' => 'server busy .Please try again some time.','field'=>'item_id'], parent::HTTP_NOT_FOUND);
      }
    }

  public function getlatestprice_post(){
    try {
      $data=[];
      $result=[];
       
      $details=$this->post();
      if(!empty( $details)){
        if(!(isset($details["itemIdArr"]))){
          $this->response(['msg' => 'Invalid Item','field'=>'item_id'], parent::HTTP_BAD_REQUEST);
       }else{
         $itemIdArr = [];
         if(is_array($details["itemIdArr"]) && count($details["itemIdArr"]) > 0)
         {
            foreach($details["itemIdArr"] as $k=>$v)
            {
              $itemIdArr[] = base64_decode($v);
            }

            $itemdata=$this->itemModel->getItem(["item_id_arr"=>$itemIdArr]);
            $results=[];
            
            foreach($itemdata as $val){
					    $result=[];
              $result["item_id"]=base64_encode($val["item_id"]);
              $result["fixedPrice"]=$val["fixedPrice"];
              $result["currency"]=$val["currency"];
              $results[]=$result;
            }
            $status = parent::HTTP_OK;
            $response = ['status' => $status,'msg'=>' Seller Item ',"item"=> $results];
             $this->response($response, $status);
            
         }
       }
      }else{
      $status = parent::HTTP_BAD_REQUEST;
        $response = ['status' => $status, 'msg' => 'Invalid Input! '];
        $this->response($response, $status);
  }
      }catch (Exception $e) {

        $this->response(['msg' => 'server busy .Please try again some time.','field'=>'item_id'], parent::HTTP_NOT_FOUND);
    }
}
}
?>
