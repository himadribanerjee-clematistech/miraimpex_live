<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST");
class Login extends REST_Controller {
	protected $salt='HA{t~xOC;07Dp,Q+$y02F??:}/?LMj_';
	public function __construct() {
        parent::__construct();
        $this->load->helper(['jwt', 'authorization']); 
        $this->load->model("customerModel");
       
	 }
  	 private function verify_request()
    {
         $headers = $this->input->request_headers();
         if(isset($headers['Authorization']) || isset($headers['authorization'])){
           //  $token = $headers['Authorization'];
             try {
               
                    if(isset($headers['Authorization'])){
                         $token = $headers['Authorization'];
                         $data = AUTHORIZATION::validateToken($token);
                            if ($data === false) {
                                $status = parent::HTTP_UNAUTHORIZED;
                                $response = ['status' => $status, 'msg' => 'Unauthorized Access!'];
                                $this->response($response, $status);

                                exit();
                            } else {
                                $customer_data=current($this->customerModel->doAuthenticateCheck(["token_id"=>$data->token_id]));
                                $customer_data["created_date"]=date('d-m-Y',strtotime($customer_data["created_date"]));  
                                return $customer_data;
                            }
                    }else if(isset($headers['authorization'])){
                         $token = $headers['authorization'];
                         $data = AUTHORIZATION::validateToken($token);
                            if ($data === false) {
                                $status = parent::HTTP_UNAUTHORIZED;
                                $response = ['status' => $status, 'msg' => 'Unauthorized Access!'];
                                $this->response($response, $status);

                                exit();
                            } else {
                                $customer_data=current($this->customerModel->doAuthenticateCheck(["token_id"=>$data->token_id]));
                                
                                return $customer_data;
                            }
                    }else{
                      $status = parent::HTTP_UNAUTHORIZED;
                      $response = ['status' => $status, 'msg' => 'Unauthorized Access! '];
                      $this->response($response, $status);
                    }
            } catch (Exception $e) {
               $status = parent::HTTP_UNAUTHORIZED;
                $response = ['status' => $status, 'msg' => 'Unauthorized Access! '];
                $this->response($response, $status);
            }
        }else{
             $status = parent::HTTP_UNAUTHORIZED;
                $response = ['status' => $status, 'msg' => 'Unauthorized Access'];
                $this->response($response, $status);

                exit();
        }
        
        
    }

    public function doLogout_post(){
          try {

            $data=[];
             $result=[];
              $data = $this->verify_request();
              if(!empty( $data)){
                      $this->load->model("sellerModel");
                       if(isset($data["id"])){
                           $this->customerModel->updateCustomer(["token_id"=>''],["id"=>$data["id"]]);
                           
                           $status = parent::HTTP_OK;
                            $response = ['status' => $status, 'msg' => 'logout successful'];
                        }else{
                             $status = parent::HTTP_UNAUTHORIZED;
                            $response = ['status' => $status, 'msg' => 'Unauthorized Access! '];
                            $this->response($response, $status);
                          }
                        
              }else{
                 $status = parent::HTTP_UNAUTHORIZED;
                $response = ['status' => $status, 'msg' => 'Unauthorized Access! '];
                $this->response($response, $status);
              }
             

            $this->response($response, $status);
            
        } catch (Exception $e) {

            $this->response(['msg' => 'server busy .Please try again some time.','field'=>'user_name'], parent::HTTP_NOT_FOUND);
        }

    }
    public function doLogin_post(){

    	try { 
    		$data=[];
    		$data["user_name"] = $this->post('user_name');
        	$data["password"] = $this->post('password');

    		if(!(isset($data["user_name"]) && trim($data["user_name"])!=='')){
    			 $this->response(['msg' => 'Please Enter Mobile no or user Name.','field'=>'user_name'], parent::HTTP_NOT_FOUND);
				
			}else if(!(isset($data["password"]) && trim($data["password"])!=='')){
				 $this->response(['msg' => 'Please Enter Password.','field'=>'user_name'], parent::HTTP_NOT_FOUND);
				
			}else{
				$customer_data=$this->customerModel->doLoginCheck($data);
				if(count($customer_data)==1){
					$token=$this->customerModel->generateToken();
					$customer_data=current($customer_data);
					$this->customerModel->updateCustomer(["token_id"=>$token],$customer_data);
					$token = AUTHORIZATION::generateToken(['token_id' => $token]);
					$status = parent::HTTP_OK;
		            $response = ['status' => $status,'msg'=>'Login Successful.', 'token' => $token];
		            $this->response($response, $status);
				}else{
					$this->response(['msg' => 'Invalid username or password!','field'=>'user_name'], parent::HTTP_NOT_FOUND);
				}

			}
    	} catch (Exception $e) {
		 	 $this->response(['msg' => 'Invalid username or password!','field'=>'user_name'], parent::HTTP_NOT_FOUND);
		}
       
 	}
    // public function do


 



    public function doChangePassword_post(){

      //////////////////////////////////////////

          try { 

            $data=[];
            $data=$this->post();

             $result=[];
              $data1 = $this->verify_request();
              if(!empty($data1)){
               
                  if(!isset($data["old_password"]) || trim($data["old_password"])==''){
                     $this->response(['msg' => 'Please Enter Old Password.','field'=>'old_password'], parent::HTTP_UNAUTHORIZED);
                  }else if(!isset($data["new_password"]) || trim($data["new_password"])==''){
                     $this->response(['msg' => 'Please Enter New Password.','field'=>'new_password'], parent::HTTP_UNAUTHORIZED);
                  }else if(!isset($data["confirm_password"]) || trim($data["confirm_password"])==''){
                     $this->response(['msg' => 'Please Enter Confirm Password.','field'=>'confirm_password'], parent::HTTP_UNAUTHORIZED);
                  }else if($data["new_password"]!=$data["confirm_password"]){
                     $this->response(['msg' => 'Password Mismatch','field'=>'confirm_password'], parent::HTTP_UNAUTHORIZED);
                  }else if(!isset($data["otp"]) || trim($data["otp"])==''){
                     $this->response(['msg' => 'Please Enter OTP.','field'=>'otp'], parent::HTTP_UNAUTHORIZED);
                  }else{
                    $cdata=[];
                    $cdata["password"]=md5(crypt($data["old_password"],$this->salt));
                    $cdata["id"]=$data1["id"];
                     $customer=$this->customerModel->getUserList($cdata);
                     if(!empty($customer)){
                      $cdata=[];
                      $cdata["change_password_otp"]=$data["otp"];
                      $cdata["id"]=$data1["id"];
                       $customer1=$this->customerModel->getUserList($cdata);
                       if(!empty($customer1)){
                              $inputdata=[];
                              $inputdata["change_password_otp"]=null;
                              $inputdata["password"]=md5(crypt($data["confirm_password"],$this->salt));
                              $this->customerModel->updateCustomer($inputdata,["id"=>$data1["id"]]);
                              $status = parent::HTTP_OK;
                              $response = ['status' => $status,'msg'=>'Password Changed Successfully.'];
                              $this->response($response, $status);
                           }else{
                              $this->response(['msg' => 'Invalid OTP','field'=>'otp'], parent::HTTP_NOT_FOUND);
                            }
                      }else{
                        $this->response(['msg' => 'Invalid Old Password','field'=>'old_password'], parent::HTTP_NOT_FOUND);
                      }
                  }

                  $inputdata=[];
                    $inputdata["change_email_otp"]=1234;
                     $this->customerModel->updateCustomer($inputdata,["id"=>$data1["id"]]);
                  //////////////////////
                  $status = parent::HTTP_OK;
                    $response = ['status' => $status,'msg'=>'Please check phone for OTP.'];
                    $this->response($response, $status);
                  }else{
                     $status = parent::HTTP_UNAUTHORIZED;
                    $response = ['status' => $status, 'msg' => 'Unauthorized Access!'];
                    $this->response($response, $status);
                  }
            
        } catch (Exception $e) {
             $this->response(['msg' => 'Invalid Request!','field'=>'name'], parent::HTTP_NOT_FOUND);
        }
      ///////////////////


  
    }

    public function doAuthenticateMe_post(){
      try {
        $data=[];
        $result=[];
        $data = $this->verify_request();
        if(!empty( $data)){
          $this->load->model("customerModel");
          if(isset($data["id"])){

            $data["buyer_info"]=current($this->customerModel->getBuyerInfo($data["id"]));
            if(!empty($data["buyer_info"])){
              $data["buyer_info"]["id"]=base64_encode($data["buyer_info"]["id"]);
              
           }
         
            $data["seller_info"]=[];
           if($data["supplier_YN"]==1 || $data["supplier_YN"]==2){
             $seller_info=current($this->customerModel->getSellerInfo($data["id"]));
            $data["seller_info"]=[];
             if(!empty($seller_info)){
              $data["seller_info"]["seller_id"]=base64_encode($seller_info["id"]);
               $data["seller_info"]["cust_id"]=base64_encode($seller_info["cust_id"]);
                $data["seller_info"]["business_name"]=$seller_info["business_name"];
                $data["seller_info"]["address"]=$seller_info["address"];
                $data["seller_info"]["country"]=$seller_info["country"];
                $data["seller_info"]["state"]=$seller_info["state"];
                $data["seller_info"]["city"]=$seller_info["city"];
                $data["seller_info"]["lat"]=$seller_info["lat"];
                 $data["seller_info"]["long"]=$seller_info["long"];
                  $data["seller_info"]["contact_name"]=$seller_info["contact_name"];
                  $data["seller_info"]["contact_number"]=$seller_info["contact_number"];
                   $data["seller_info"]["store_currency"]=$seller_info["store_currency"];
             }
            
           }
           $data["seller_info"]["seller_images"]=[];
             $data["seller_info"]["certificate"]=[];
              $data["seller_info"]["document"]=[];
          $sellerImageinfo= $this->customerModel-> getSellerimagesList(["seller_id"=>$data["id"]]);
          foreach($sellerImageinfo as $v1){
            if($v1["type"]==1){
               $data["seller_info"]["seller_images"][]=["image_id"=>base64_encode($v1["id"]),"image"=>$v1["seller_image"]];
            }
             if($v1["type"]==2){
               $data["seller_info"]["certificate"][]=["id"=>base64_encode($v1["id"]),"certificate"=>$v1["seller_image"]];
            }
             if($v1["type"]==3){
               $data["seller_info"]["document"][]=["id"=>base64_encode($v1["id"]),"document"=>$v1["seller_image"]];
            }
          
          }
          $RateExchangeInfo= $this->customerModel-> getRateExchangeList([]);
        $data["rate_exchange_info"]=$RateExchangeInfo;
             $data["id"]=base64_encode($data["id"]);
           $status = parent::HTTP_OK;
           $response = ['status' => $status,'result'=>$data];

          }else{
              $status = parent::HTTP_UNAUTHORIZED;
            $response = ['status' => $status, 'msg' => 'Unauthorized Access! '];
            $this->response($response, $status);
          }

        }else{
          $status = parent::HTTP_UNAUTHORIZED;
          $response = ['status' => $status, 'msg' => 'Unauthorized Access! '];
          $this->response($response, $status);
        }

        $this->response($response, $status);

      }catch (Exception $e) {

        $this->response(['msg' => 'server busy .Please try again some time.','field'=>'user_name'], parent::HTTP_NOT_FOUND);
    }
  }

  public function upgreadeToSeller_post(){
    try {
      $data=[];
      $result=[];
      $data = $this->verify_request();
      if(!empty( $data)){
        if(trim($data["id"])=='' || (($data["supplier_YN"]==1 || $data["supplier_YN"]==2))){
          $status = parent::HTTP_UNAUTHORIZED;
          $response = ['status' => $status, 'msg' => 'Unauthorized Access! '];
          $this->response($response, $status);
        }else{
          $details=$this->input->post();
          $inputdata=[];
          $seller_info=current($this->customerModel->getSellerInfo($data["id"]));
          if(empty($seller_info)){
            if(!isset($details["business_name"]) || trim($details["business_name"])==''){
              $inputdata["business_name"]='';
           }else{
             $inputdata["business_name"]=$details["business_name"];
           }
           if(!isset($details["address"]) || trim($details["address"])==''){
              $inputdata["address"]='';
           }else{
             $inputdata["address"]=$details["address"];
           }
            if(!isset($details["contact_name"]) || trim($details["contact_name"])==''){
              $inputdata["contact_name"]='';
           }else{
             $inputdata["contact_name"]=$details["contact_name"];
           }
            if(!isset($details["contact_number"]) || trim($details["contact_number"])==''){
              $inputdata["contact_number"]='';
           }else{
             $inputdata["contact_number"]=$details["contact_number"];
           }
           $update_data=[];
           if(!isset($details["store_currency"]) || trim($details["store_currency"])==''){
                $update_data["store_currency"]='';
             }else{
               $update_data["store_currency"]=$details["store_currency"];
             }
             if(!isset($details["sellerCategory"]) || trim($details["sellerCategory"])==''){
                $update_data["sellerCategory"]='';
            }else{
              $update_data["sellerCategory"]=$details["sellerCategory"];
            }
            $update_data["supplier_YN"]=1;
             
            if(!isset($details["country"]) || trim($details["country"])==''){
              $inputdata["country"]='';
           }else{
             $inputdata["country"]=$details["country"];
           } 
           if(!isset($details["state"]) || trim($details["state"])==''){
              $inputdata["state"]='';
           }else{
             $inputdata["state"]=$details["state"];
           }
            if(!isset($details["city"]) || trim($details["city"])==''){
              $inputdata["city"]='';
           }else{
             $inputdata["city"]=$details["city"];
           } 
           if(!isset($details["lat"]) || trim($details["lat"])==''){
             $inputdata["lat"]='';
           }else{
             $inputdata["lat"]=$details["lat"];
           }
           if(!isset($details["long"]) || trim($details["long"])==''){
             $inputdata["long"]='';
           }else{
             $inputdata["long"]=$details["long"];
           }
           $inputdata["cust_id"]=$data["id"];

           $this->customerModel->insertseller($inputdata);
           $this->customerModel->updateCustomer($update_data,["id"=>$data["id"]]);
           $last_id=$data["id"];
           ///////////////////////////////////////
           $files = $_FILES;
           $not_inseted_certificate=[];
         $cpt = count($_FILES['certificate']['name']);
             $this->load->library('image_lib');
             for($i=0; $i<$cpt; $i++){ 
                $uploadPath = '../admin/uploads/seller/';
                if (trim($files['certificate']['name'][$i]) =='') {
            
                    continue;
                }  
                $fileName = time().'-'.$files['certificate']['name'][$i];
                $this->load->library('upload');
                $_FILES['certificate']['name']        = $fileName;
                    $_FILES['certificate']['type']        = $files['certificate']['type'][$i];
                $_FILES['certificate']['tmp_name']    = $files['certificate']['tmp_name'][$i];
                $_FILES['certificate']['error']       = $files['certificate']['error'][$i];
                $_FILES['certificate']['size']        = $files['certificate']['size'][$i];
                $type = $_FILES["certificate"]["type"];
                if ((($_FILES["certificate"]["type"] == "image/gif")  || ($_FILES["certificate"]["type"] == "image/jpeg")  || ($_FILES["certificate"]["type"] == "image/jpg")  || ($_FILES["certificate"]["type"] == "image/png") )) {
        
                  $config = [];
                  if ( move_uploaded_file($_FILES['certificate']['tmp_name'],  $uploadPath."".$fileName)){
          
                              $config = [];  
                              $config['image_library'] = 'gd2';
                              $config['source_image'] = $uploadPath."".$fileName;
                              $config['create_thumb'] = false;
                              $config['maintain_ratio'] = TRUE;
                              $config['width']     = 600;
                              $config['height']   = 600;
                              $this->image_lib->clear();
                          $this->image_lib->initialize($config);
                          $this->image_lib->resize();
                              
                                  $sellerImg = [
                                      'seller_id' =>  $last_id,
                                      'image'     =>  $fileName,
                                      'type'     => 2
                                  ];
                                  
                                  $insert = $this->customerModel->insertSellerImg($sellerImg);
                              
          
                    }else{
                        $not_inseted_certificate[]=$files['certificate']['name'][$i];
                    }
                }else if (($type == "application/pdf")) {
                      if ( move_uploaded_file($_FILES['certificate']['tmp_name'],  $uploadPath."".$fileName)){
                      
                              $sellerImg = [
                                          'seller_id' =>  $last_id,
                                          'image'     =>  $fileName,
                                          'type'     => 2
                                      ];
                                  
                                  $insert = $this->customerModel->insertSellerImg($sellerImg);
                      }else{
          
                          $not_inseted_certificate[]=$files['certificate']['name'][$i];
                  }
          
              }else if (($type == "application/msword") || ($type == "application/doc") || ($type == "application/vnd.openxmlformats-officedocument.wordprocessingml.document") ) {
                  if ( move_uploaded_file($_FILES['certificate']['tmp_name'],  $uploadPath."".$fileName)){
                        $sellerImg = [
                                        'seller_id' =>  $last_id,
                                        'image'     =>  $fileName,
                                        'type'     => 2
                                    ];
                                
                                $insert = $this->customerModel->insertSellerImg($sellerImg);
        
                    }else{
        
                              $not_inseted_certificate[]=$_FILES["certificate"]["name"][$i];
                      }
                }else if (($type == "application/msword") || ($type == "application/doc") || ($type == "application/vnd.openxmlformats-officedocument.wordprocessingml.document") ) {
                      if ( move_uploaded_file($_FILES['certificate']['tmp_name'],  $uploadPath."".$fileName)){
                        $sellerImg = [
                                        'seller_id' =>  $last_id,
                                        'image'     =>  $fileName,
                                        'type'     => 2
                                    ];
                                
                                $insert = $this->customerModel->insertSellerImg($sellerImg);
        
                    }else{
        
                              $not_inseted_certificate[]=$_FILES["certificate"]["name"][$i];
                      }
                }else{
                  $not_inseted_certificate[]=$files['certificate']['name'][$i];
                }
             }
         
           ////////////////////////

           $files = $_FILES;
           $not_inseted_document=[];
           $cpt = count($_FILES['document']['name']);
               $this->load->library('image_lib');
           for($i=0; $i<$cpt; $i++){ 
               $uploadPath = '../admin/uploads/seller/';
               if (trim($files['document']['name'][$i]) =='') {

                   continue;
               }  
               $fileName = time().'-'.$files['document']['name'][$i];
               

               $this->load->library('upload');
               $_FILES['document']['name']        = $fileName;
                   $_FILES['document']['type']        = $files['document']['type'][$i];
               $_FILES['document']['tmp_name']    = $files['document']['tmp_name'][$i];
               $_FILES['document']['error']       = $files['document']['error'][$i];
               $_FILES['document']['size']        = $files['document']['size'][$i];

                   $type = $_FILES["document"]["type"];
               if ((($_FILES["document"]["type"] == "image/gif")  || ($_FILES["document"]["type"] == "image/jpeg")  || ($_FILES["document"]["type"] == "image/jpg")  || ($_FILES["document"]["type"] == "image/png") )) {
                   
                       

                   $config = [];
                   if ( move_uploaded_file($_FILES['document']['tmp_name'],  $uploadPath."".$fileName)){

                               $config = [];  
                               $config['image_library'] = 'gd2';
                               $config['source_image'] = $uploadPath."".$fileName;
                               $config['create_thumb'] = false;
                               $config['maintain_ratio'] = TRUE;
                               $config['width']     = 600;
                               $config['height']   = 600;
                               $this->image_lib->clear();
                           $this->image_lib->initialize($config);
                           $this->image_lib->resize();
                               
                                   $sellerImg = [
                                       'seller_id' =>  $last_id,
                                       'image'     =>  $fileName,
                                       'type'     => 3
                                   ];
                                   
                                   $insert = $this->customerModel->insertSellerImg($sellerImg);
                               

                           }else{
                               $not_inseted_document[]=$files['document']['name'][$i];
                           }
                   

               }else if (($type == "application/pdf")) {
                       if ( move_uploaded_file($_FILES['document']['tmp_name'],  $uploadPath."".$fileName)){
                       
                               $sellerImg = [
                                           'seller_id' =>  $last_id,
                                           'image'     =>  $fileName,
                                           'type'     => 3
                                       ];
                                   
                                   $insert = $this->customerModel->insertSellerImg($sellerImg);
                       }else{

                           $not_inseted_document[]=$files['document']['name'][$i];
                   }

               }else if (($type == "application/msword") || ($type == "application/doc") || ($type == "application/vnd.openxmlformats-officedocument.wordprocessingml.document") ) {

                       if ( move_uploaded_file($_FILES['document']['tmp_name'],  $uploadPath."".$fileName)){
                           $sellerImg = [
                                           'seller_id' =>  $last_id,
                                           'image'     =>  $fileName,
                                           'type'     => 3
                                       ];
                                   
                                   $insert = $this->customerModel->insertSellerImg($sellerImg);

                       }else{

                           $not_inseted_document[]=$_FILES["document"]["name"][$i];
                       }
               }else{

                   $not_inseted_document[]=$files['document']['name'][$i];
               }  


           } 
           ////////////////////////////
           $status = parent::HTTP_OK;
          $response = ['status' => $status,'msg'=>' Upgreade to Seller  Successful',"not_inseted_document"=>implode(",",$not_inseted_document),"not_inseted_certificate"=>implode(",",$not_inseted_certificate)];
          $this->response($response, $status);
          }else{
            $status = parent::HTTP_UNAUTHORIZED;
            $response = ['status' => $status, 'msg' => 'Unauthorized Access! '];
            $this->response($response, $status);
          }
         
        }
       
      }else{
        $status = parent::HTTP_UNAUTHORIZED;
       $response = ['status' => $status, 'msg' => 'Unauthorized Access! '];
       $this->response($response, $status);
      }
    }catch (Exception $e) {

      $this->response(['msg' => 'server busy .Please try again some time.','field'=>'user_name'], parent::HTTP_NOT_FOUND);
   } 
  }
  public function updateSellerInfo_post(){
    try {
      $data=[];
        $result=[];
        $data = $this->verify_request();
         if(!empty( $data)){
            if(trim($data["id"])=='' || (!($data["supplier_YN"]==1 || $data["supplier_YN"]==2))){
              $status = parent::HTTP_UNAUTHORIZED;
              $response = ['status' => $status, 'msg' => 'Unauthorized Access! '];
              $this->response($response, $status);
            }else{
               $details=$this->input->post();
                 $inputdata=[];
                   $seller_info=current($this->customerModel->getSellerInfo($data["id"]));
                 if(!isset($details["business_name"]) || trim($details["business_name"])==''){
                     $inputdata["business_name"]='';
                  }else{
                    $inputdata["business_name"]=$details["business_name"];
                  }
                  if(!isset($details["address"]) || trim($details["address"])==''){
                     $inputdata["address"]='';
                  }else{
                    $inputdata["address"]=$details["address"];
                  }
                   if(!isset($details["contact_name"]) || trim($details["contact_name"])==''){
                     $inputdata["contact_name"]='';
                  }else{
                    $inputdata["contact_name"]=$details["contact_name"];
                  }
                   if(!isset($details["contact_number"]) || trim($details["contact_number"])==''){
                     $inputdata["contact_number"]='';
                  }else{
                    $inputdata["contact_number"]=$details["contact_number"];
                  }
                  $update_data=[];
                  if(!isset($details["store_currency"]) || trim($details["store_currency"])==''){
                       $update_data["store_currency"]='';
                    }else{
                      $update_data["store_currency"]=$details["store_currency"];
                    }
                    
                  if(!isset($details["state"]) || trim($details["state"])==''){
                     $inputdata["state"]='';
                  }else{
                    $inputdata["state"]=$details["state"];
                  }
                   if(!isset($details["city"]) || trim($details["city"])==''){
                     $inputdata["city"]='';
                  }else{
                    $inputdata["city"]=$details["city"];
                  } 
                  if(!isset($details["lat"]) || trim($details["lat"])==''){
                    $inputdata["lat"]='';
                  }else{
                    $inputdata["lat"]=$details["lat"];
                  }
                  if(!isset($details["long"]) || trim($details["long"])==''){
                    $inputdata["long"]='';
                  }else{
                    $inputdata["long"]=$details["long"];
                  }
                  $inputdata["cust_id"]=$data["id"];
                //  print_r( $update_data);die();
                  if(empty( $seller_info)){
                    $this->customerModel->insertseller($inputdata);
                  }else{
                    $this->customerModel->updateseller($inputdata,["id"=>$seller_info["id"]]);
                  }
                    $this->customerModel->updateCustomer($update_data,["id"=>$data["id"]]);
                    $status = parent::HTTP_OK;
                 $response = ['status' => $status,'message'=>"Seller information upddated successfully"];

                  $this->response($response, $status);
            }
         }else{
           $status = parent::HTTP_UNAUTHORIZED;
          $response = ['status' => $status, 'msg' => 'Unauthorized Access! '];
          $this->response($response, $status);
         }
    }catch (Exception $e) {

        $this->response(['msg' => 'server busy .Please try again some time.','field'=>'user_name'], parent::HTTP_NOT_FOUND);
    }
  }



public function doSendOTPVerifyEmail_post(){
    try { 
        $data=$this->post();
        ////////////////////////////////////

        $data=[];
         $result=[];
          $data1 = $this->verify_request();
          if(!empty($data1)){
              $inputdata=[];
                $inputdata["email_otp"]=1234;
                $this->customerModel->updateCustomer($inputdata,["id"=>$data1["id"]]);
                //EMAIL SEND
                if(isset($data1["email"]) && trim($data1["email"])!='')
                {
                  $email_data = [];
                  $email_data['toemail'] = $data1["email"];
                  $email_data['name'] = $data1["name"];
                  $email_data['operationTitle'] = "Confirmation needed.";
                  $email_data['operationName'] = "Verify your email address.";
                  $email_data['operationDetails'] = 'Your 4 digit code for email confirmation is <b>'.$inputdata["email_otp"].'</b>. Please paste the code to verify your email.';
                  $this->load->model("emailtemplateModel");
                //  $enquiry_message_data=$this->emailtemplateModel->sendEmail($email_data);
                }
              //////////////////////
              $status = parent::HTTP_OK;
                $response = ['status' => $status,'msg'=>'4 digit otp has been sent to your registered email address. Please paste the otp below to complete verification process.'];
                $this->response($response, $status);
              }else{
                 $status = parent::HTTP_UNAUTHORIZED;
                $response = ['status' => $status, 'msg' => 'Unauthorized Access!'];
                $this->response($response, $status);
              }
        
    } catch (Exception $e) {
         $this->response(['msg' => 'Invalid Request!','field'=>'name'], parent::HTTP_NOT_FOUND);
    }
  }
    
     public function doVerifyEmailUpdate_post(){

      //////////////////////////////////////////

          try { 

            $data=[];
            $data=$this->post();

             $result=[];
              $data1 = $this->verify_request();
              if(!empty($data1)){
               
                  if(!isset($data["otp"]) || trim($data["otp"])==''){
                     $this->response(['msg' => 'Please Enter OTP!','field'=>'otp'], parent::HTTP_UNAUTHORIZED);
                  }else{
                    ///app testing purpose ////
                /*   $inputdata=[];
                  $inputdata["email_otp"]=1234;
                  $this->customerModel->updateCustomer($inputdata,["id"=>$data1["id"]]);*/
                   ///app testing purpose ////

                    $cdata=[];
                    $cdata["email_otp"]=$data["otp"];
                    $cdata["id"]=$data1["id"];
                  
                     $customer=$this->customerModel->getUserList($cdata);
                     if(!empty($customer)){
                        $inputdata=[];
                        $customer=current($customer);
                        $inputdata["email_otp"]=null;
                         $inputdata["email"] = $customer["email"];
                         $inputdata["email_verify"] = 1;
                         $this->customerModel->updateCustomer($inputdata,["id"=>$data1["id"]]);
                         $status = parent::HTTP_OK;
                          $response = ['status' => $status,'msg'=>'Email Id Updated Successfully.'];
                          $this->response($response, $status);
                      }else{
                        $this->response(['msg' => 'Invalid OTP','field'=>'otp'], parent::HTTP_NOT_FOUND);
                      }
                  }

               /*   $inputdata=[];
                    $inputdata["change_email_otp"]=1234;
                     $this->customerModel->updateCustomer($inputdata,["id"=>$data1["id"]]);
                  //////////////////////
                  $status = parent::HTTP_OK;
                    $response = ['status' => $status,'msg'=>'Please check phone for OTP.'];
                    $this->response($response, $status);*/
                  }else{
                     $status = parent::HTTP_UNAUTHORIZED;
                    $response = ['status' => $status, 'msg' => 'Unauthorized Access!'];
                    $this->response($response, $status);
                  }
            
        } catch (Exception $e) {
             $this->response(['msg' => 'Invalid Request!','field'=>'name'], parent::HTTP_NOT_FOUND);
        }
      ///////////////////


  
    }


       public function updateBuyerInfo_post(){
          try {
            $data=[];
              $result=[];
              $data = $this->verify_request();
               if(!empty( $data)){
                  if(trim($data["id"])=='' && (!($data["supplier_YN"]==0 || $data["supplier_YN"]==1 || $data["supplier_YN"]==2))){
                    $status = parent::HTTP_UNAUTHORIZED;
                    $response = ['status' => $status, 'msg' => 'Unauthorized Access! '];
                    $this->response($response, $status);
                  }else{
                     $details=$this->input->post();
                       $inputdata=[];
                         $buyer_info=current($this->customerModel->getBuyerInfo($data["id"]));
                       if(!isset($details["business_name"]) || trim($details["business_name"])==''){
                           $inputdata["business_name"]='';
                        }else{
                          $inputdata["business_name"]=$details["business_name"];
                        }
                        if(!isset($details["address"]) || trim($details["address"])==''){
                           $inputdata["address"]='';
                        }else{
                          $inputdata["address"]=$details["address"];
                        }
                         $update_data=[];
                        if(!isset($details["cust_language"]) || trim($details["cust_language"])==''){
                           $update_data["cust_language"]='';
                        }else{
                          $update_data["cust_language"]=$details["cust_language"];
                        }
                       

                        if(!isset($details["state"]) || trim($details["state"])==''){
                           $inputdata["state"]='';
                        }else{
                          $inputdata["state"]=$details["state"];
                        }
                        if(!isset($details["country"]) || trim($details["country"])==''){
                           $inputdata["country"]='';
                        }else{
                          $inputdata["country"]=$details["country"];
                        }
                         if(!isset($details["city"]) || trim($details["city"])==''){
                           $inputdata["city"]='';
                        }else{
                          $inputdata["city"]=$details["city"];
                        } 
                        if(!isset($details["lat"]) || trim($details["lat"])==''){
                          $inputdata["lat"]='';
                        }else{
                          $inputdata["lat"]=$details["lat"];
                        }
                        if(!isset($details["long"]) || trim($details["long"])==''){
                          $inputdata["long"]='';
                        }else{
                          $inputdata["long"]=$details["long"];
                        }
                        $inputdata["cust_id"]=$data["id"];
                        if(empty( $buyer_info)){
                          $this->customerModel->insertbuyer($inputdata);
                        }else{
                         
                          $this->customerModel->updatebuyer($inputdata,["id"=>$buyer_info["id"]]);
                        
                        }
                       
                         $this->customerModel->updateCustomer($update_data,["id"=>$data["id"]]);
                          $status = parent::HTTP_OK;
                       $response = ['status' => $status,'message'=>"Buyer information upddated successfully"];

                        $this->response($response, $status);
                  }
               }else{
                 $status = parent::HTTP_UNAUTHORIZED;
                $response = ['status' => $status, 'msg' => 'Unauthorized Access! '];
                $this->response($response, $status);
               }
          }catch (Exception $e) {

              $this->response(['msg' => 'server busy .Please try again some time.','field'=>'user_name'], parent::HTTP_NOT_FOUND);
          }
  }
   public function doSellerImageAdd_post(){
       try {
        $data=[];
        $result=[];
         
         $data = $this->verify_request();


          if(!empty( $data)){
             
             $details=$this->post();
             $files = $_FILES;
             
                  if(!isset($files["business_logo"]) || empty($files["business_logo"])){
                     $this->response(['msg' => 'Please Select Seller Image','field'=>'business_logo'], parent::HTTP_BAD_REQUEST);
                  }else{
                    $check_customer=$this->customerModel->getUserDetail($data["id"]);
                    
                    if(!empty($check_customer)){
                           $seller_info=current($this->customerModel->getSellerInfo($data["id"]));
                    
                      if(!empty($seller_info)){

                               $last_id=$data["id"]; 
                               $files = $_FILES;
                                $not_inseted=[];
                                $cpt = count($_FILES['business_logo']['name']);
                                 $this->load->library('image_lib');
                                for($i=0; $i<$cpt; $i++){ 
                                  if (trim($files['business_logo']['name'][$i]) =='') {

                                        continue;
                                    }  
                                   $fileName = time().'-'.$files['business_logo']['name'][$i];

                                    $this->load->library('upload');
                                    $_FILES['business_logo']['name']        = $fileName;
                                     $_FILES['business_logo']['type']        = $files['business_logo']['type'][$i];
                                    $_FILES['business_logo']['tmp_name']    = $files['business_logo']['tmp_name'][$i];
                                    $_FILES['business_logo']['error']       = $files['business_logo']['error'][$i];
                                    $_FILES['business_logo']['size']        = $files['business_logo']['size'][$i];
                                    if ((($_FILES["business_logo"]["type"] == "image/gif")  || ($_FILES["business_logo"]["type"] == "image/jpeg")  || ($_FILES["business_logo"]["type"] == "image/jpg")  || ($_FILES["business_logo"]["type"] == "image/png") )) {
                                       
                                          $uploadPath = '../admin/uploads/seller/';

                                        $config = [];
                                        if ( move_uploaded_file($_FILES['business_logo']['tmp_name'],  $uploadPath."".$fileName)){
                                                  $config = [];  
                                                  $config['image_library'] = 'gd2';
                                                  $config['source_image'] = $uploadPath."".$fileName;
                                                  $config['create_thumb'] = false;
                                                  $config['maintain_ratio'] = TRUE;
                                                  $config['width']     = 600;
                                                  $config['height']   = 600;
                                                 $this->image_lib->clear();
                                                $this->image_lib->initialize($config);
                                                $this->image_lib->resize();
                                                   
                                                       $sellerImg = [
                                                          'seller_id' =>  $last_id,
                                                          'image'     =>  $fileName,
                                                          'type'     => 1
                                                      ];
                                                      
                                                      $insert = $this->customerModel->insertSellerImg($sellerImg);
                                                  

                                              }else{
                                                   $not_inseted[]=$files['business_logo']['name'][$i];
                                              }
                                     

                                    }else{
                                        $not_inseted[]=$files['business_logo']['name'][$i];
                                    }   
                                } 
                              $status = parent::HTTP_OK;
                              $response = ['status' => $status,'msg'=>'Seller Image Uploaded Successfully',"not_inseted"=>implode(",",$not_inseted)];
                              $this->response($response, $status);
                      }else{
                        
                        $this->response(['msg' => 'Unauthorized Access!','field'=>'item_id'], parent::HTTP_UNAUTHORIZED);
                      }
                            
                         
                    
                    }else{
                       $this->response(['msg' => 'Unauthorized Access!','field'=>'item_id'], parent::HTTP_UNAUTHORIZED);
                    }
                   
                  }

            
          }else{
              $status = parent::HTTP_UNAUTHORIZED;
              $response = ['status' => $status, 'msg' => 'Unauthorized Access! '];
              $this->response($response, $status);
          }
        } catch (Exception $e) {

          $this->response(['msg' => 'server busy .Please try again some time.','field'=>'item_id'], parent::HTTP_NOT_FOUND);
      }
    }
     public function doSellerCertificateAdd_post(){
       try {
        $data=[];
        $result=[];
         
         $data = $this->verify_request();


          if(!empty( $data)){
             
             $details=$this->post();
             $files = $_FILES;
             
                  if(!isset($files["certificate"]) || empty($files["certificate"])){
                     $this->response(['msg' => 'Please Select Seller Certificate','field'=>'certificate'], parent::HTTP_BAD_REQUEST);
                  }else{
                    $check_customer=$this->customerModel->getUserDetail($data["id"]);
                    
                    if(!empty($check_customer)){
                           $seller_info=current($this->customerModel->getSellerInfo($data["id"]));
                    
                      if(!empty($seller_info)){

                               $last_id=$data["id"]; 
                               $files = $_FILES;
                                $not_inseted=[];
                                $cpt = count($_FILES['certificate']['name']);
                                 $this->load->library('image_lib');
                                for($i=0; $i<$cpt; $i++){ 
                                  $uploadPath = '../admin/uploads/seller/';
                                  if (trim($files['certificate']['name'][$i]) =='') {

                                        continue;
                                    }  
                                   $fileName = time().'-'.$files['certificate']['name'][$i];
                                  

                                    $this->load->library('upload');
                                    $_FILES['certificate']['name']        = $fileName;
                                     $_FILES['certificate']['type']        = $files['certificate']['type'][$i];
                                    $_FILES['certificate']['tmp_name']    = $files['certificate']['tmp_name'][$i];
                                    $_FILES['certificate']['error']       = $files['certificate']['error'][$i];
                                    $_FILES['certificate']['size']        = $files['certificate']['size'][$i];

                                     $type = $_FILES["certificate"]["type"];
                                    if ((($_FILES["certificate"]["type"] == "image/gif")  || ($_FILES["certificate"]["type"] == "image/jpeg")  || ($_FILES["certificate"]["type"] == "image/jpg")  || ($_FILES["certificate"]["type"] == "image/png") )) {
                                       
                                          

                                        $config = [];
                                        if ( move_uploaded_file($_FILES['certificate']['tmp_name'],  $uploadPath."".$fileName)){

                                                  $config = [];  
                                                  $config['image_library'] = 'gd2';
                                                  $config['source_image'] = $uploadPath."".$fileName;
                                                  $config['create_thumb'] = false;
                                                  $config['maintain_ratio'] = TRUE;
                                                  $config['width']     = 600;
                                                  $config['height']   = 600;
                                                 $this->image_lib->clear();
                                                $this->image_lib->initialize($config);
                                                $this->image_lib->resize();
                                                   
                                                       $sellerImg = [
                                                          'seller_id' =>  $last_id,
                                                          'image'     =>  $fileName,
                                                          'type'     => 2
                                                      ];
                                                      
                                                      $insert = $this->customerModel->insertSellerImg($sellerImg);
                                                  

                                              }else{
                                                   $not_inseted[]=$files['certificate']['name'][$i];
                                              }
                                     

                                    }else if (($type == "application/pdf")) {
                                         if ( move_uploaded_file($_FILES['certificate']['tmp_name'],  $uploadPath."".$fileName)){
                                          
                                                 $sellerImg = [
                                                                'seller_id' =>  $last_id,
                                                                'image'     =>  $fileName,
                                                                'type'     => 2
                                                            ];
                                                      
                                                      $insert = $this->customerModel->insertSellerImg($sellerImg);
                                          }else{

                                              $not_inseted[]=$files['certificate']['name'][$i];
                                        }

                                   }else if (($type == "application/msword") || ($type == "application/doc") || ($type == "application/vnd.openxmlformats-officedocument.wordprocessingml.document") ) {

                                          if ( move_uploaded_file($_FILES['certificate']['tmp_name'],  $uploadPath."".$fileName)){
                                                $sellerImg = [
                                                                'seller_id' =>  $last_id,
                                                                'image'     =>  $fileName,
                                                                'type'     => 2
                                                            ];
                                                      
                                                      $insert = $this->customerModel->insertSellerImg($sellerImg);

                                          }else{

                                             $not_inseted[]=$_FILES["certificate"]["name"][$i];
                                        }
                                   }else{

                                        $not_inseted[]=$files['certificate']['name'][$i];
                                    }  


                                } 
                             
                              $status = parent::HTTP_OK;
                              $response = ['status' => $status,'msg'=>'Seller Certificate Uploaded Successfully',"not_inseted"=>implode(",",$not_inseted)];
                              $this->response($response, $status);
                      }else{
                        
                        $this->response(['msg' => 'Unauthorized Access!','field'=>'item_id'], parent::HTTP_UNAUTHORIZED);
                      }
                            
                         
                    
                    }else{
                       $this->response(['msg' => 'Unauthorized Access!','field'=>'item_id'], parent::HTTP_UNAUTHORIZED);
                    }
                   
                  }

            
          }else{
              $status = parent::HTTP_UNAUTHORIZED;
              $response = ['status' => $status, 'msg' => 'Unauthorized Access! '];
              $this->response($response, $status);
          }
        } catch (Exception $e) {

          $this->response(['msg' => 'server busy .Please try again some time.','field'=>'item_id'], parent::HTTP_NOT_FOUND);
      }
    }

     public function doSellerDocumentAdd_post(){
       try {
        $data=[];
        $result=[];
         
         $data = $this->verify_request();


          if(!empty( $data)){
             
             $details=$this->post();
             $files = $_FILES;
             
                  if(!isset($files["document"]) || empty($files["document"])){
                     $this->response(['msg' => 'Please Select Seller Document','field'=>'document'], parent::HTTP_BAD_REQUEST);
                  }else{
                    $check_customer=$this->customerModel->getUserDetail($data["id"]);
                    
                    if(!empty($check_customer)){
                           $seller_info=current($this->customerModel->getSellerInfo($data["id"]));
                    
                      if(!empty($seller_info)){

                               $last_id=$data["id"]; 
                               $files = $_FILES;
                                $not_inseted=[];
                                $cpt = count($_FILES['document']['name']);
                                 $this->load->library('image_lib');
                                for($i=0; $i<$cpt; $i++){ 
                                  $uploadPath = '../admin/uploads/seller/';
                                  if (trim($files['document']['name'][$i]) =='') {

                                        continue;
                                    }  
                                   $fileName = time().'-'.$files['document']['name'][$i];
                                  

                                    $this->load->library('upload');
                                    $_FILES['document']['name']        = $fileName;
                                     $_FILES['document']['type']        = $files['document']['type'][$i];
                                    $_FILES['document']['tmp_name']    = $files['document']['tmp_name'][$i];
                                    $_FILES['document']['error']       = $files['document']['error'][$i];
                                    $_FILES['document']['size']        = $files['document']['size'][$i];

                                     $type = $_FILES["document"]["type"];
                                    if ((($_FILES["document"]["type"] == "image/gif")  || ($_FILES["document"]["type"] == "image/jpeg")  || ($_FILES["document"]["type"] == "image/jpg")  || ($_FILES["document"]["type"] == "image/png") )) {
                                       
                                          

                                        $config = [];
                                        if ( move_uploaded_file($_FILES['document']['tmp_name'],  $uploadPath."".$fileName)){

                                                  $config = [];  
                                                  $config['image_library'] = 'gd2';
                                                  $config['source_image'] = $uploadPath."".$fileName;
                                                  $config['create_thumb'] = false;
                                                  $config['maintain_ratio'] = TRUE;
                                                  $config['width']     = 600;
                                                  $config['height']   = 600;
                                                 $this->image_lib->clear();
                                                $this->image_lib->initialize($config);
                                                $this->image_lib->resize();
                                                   
                                                       $sellerImg = [
                                                          'seller_id' =>  $last_id,
                                                          'image'     =>  $fileName,
                                                          'type'     => 3
                                                      ];
                                                      
                                                      $insert = $this->customerModel->insertSellerImg($sellerImg);
                                                  

                                              }else{
                                                   $not_inseted[]=$files['document']['name'][$i];
                                              }
                                     

                                    }else if (($type == "application/pdf")) {
                                         if ( move_uploaded_file($_FILES['document']['tmp_name'],  $uploadPath."".$fileName)){
                                          
                                                 $sellerImg = [
                                                                'seller_id' =>  $last_id,
                                                                'image'     =>  $fileName,
                                                                'type'     => 3
                                                            ];
                                                      
                                                      $insert = $this->customerModel->insertSellerImg($sellerImg);
                                          }else{

                                              $not_inseted[]=$files['document']['name'][$i];
                                        }

                                   }else if (($type == "application/msword") || ($type == "application/doc") || ($type == "application/vnd.openxmlformats-officedocument.wordprocessingml.document") ) {

                                          if ( move_uploaded_file($_FILES['document']['tmp_name'],  $uploadPath."".$fileName)){
                                                $sellerImg = [
                                                                'seller_id' =>  $last_id,
                                                                'image'     =>  $fileName,
                                                                'type'     => 3
                                                            ];
                                                      
                                                      $insert = $this->customerModel->insertSellerImg($sellerImg);

                                          }else{

                                             $not_inseted[]=$_FILES["document"]["name"][$i];
                                        }
                                   }else{

                                        $not_inseted[]=$files['document']['name'][$i];
                                    }  


                                } 
                             
                              $status = parent::HTTP_OK;
                              $response = ['status' => $status,'msg'=>'Seller Document Uploaded Successfully',"not_inseted"=>implode(",",$not_inseted)];
                              $this->response($response, $status);
                      }else{
                        
                        $this->response(['msg' => 'Unauthorized Access!','field'=>'item_id'], parent::HTTP_UNAUTHORIZED);
                      }
                            
                         
                    
                    }else{
                       $this->response(['msg' => 'Unauthorized Access!','field'=>'item_id'], parent::HTTP_UNAUTHORIZED);
                    }
                   
                  }

            
          }else{
              $status = parent::HTTP_UNAUTHORIZED;
              $response = ['status' => $status, 'msg' => 'Unauthorized Access! '];
              $this->response($response, $status);
          }
        } catch (Exception $e) {

          $this->response(['msg' => 'server busy .Please try again some time.','field'=>'item_id'], parent::HTTP_NOT_FOUND);
      }
    }
     public function doSellerImageRemove_post(){
      try {
        $data=[];
       $result=[];
       
         $data = $this->verify_request();
          if(!empty( $data)){
            
             $details=$this->post();

            
                  if(!isset($details["seller_image_id"]) || trim($details["seller_image_id"])==''){
                     $this->response(['msg' => 'Please Select Image!','field'=>'seller_image_id'], parent::HTTP_BAD_REQUEST);
                  }else{
                    
                        $seller_info=current($this->customerModel->getSellerInfo($data["id"]));

                      if(!empty($seller_info)){

                            $sellerItemdata=current($this->customerModel->getSellerimagesList(["id"=>base64_decode( $details["seller_image_id"]),"seller_id"=>$data["id"]]));
                               if(!empty($sellerItemdata)){
                              //  unset($details["seller_id"]);
                                  
                                  if(file_exists('../admin/uploads/seller/'.$sellerItemdata["seller_image"])){
                                    unlink('../admin/uploads/seller/'.$sellerItemdata["seller_image"]);
                                  }
                                  $this->customerModel->deleteSellerImage(base64_decode( $details["seller_image_id"]));
                                  $status = parent::HTTP_OK;
                              $response = ['status' => $status,'msg'=>'Seller Image Deleted Successfully'];
                                $this->response($response, $status);
                            }else{
                             $this->response(['msg' => 'Invalid Image!'], parent::HTTP_BAD_REQUEST);
                            }

                      }else{
                         $status = parent::HTTP_UNAUTHORIZED;
                        $response = ['status' => $status, 'msg' => 'Unauthorized Access! '];
                        $this->response($response, $status);
                      }     
                  }
                     
                
                
           
            }else{
             $status = parent::HTTP_UNAUTHORIZED;
            $response = ['status' => $status, 'msg' => 'Unauthorized Access! '];
            $this->response($response, $status);
          }
      } catch (Exception $e) {

          $this->response(['msg' => 'server busy .Please try again some time.','field'=>'item_id'], parent::HTTP_NOT_FOUND);
      }
    }

    public function doBuyerUpdateImage_post(){
          try {

            $data=[];
            $result=[];
              $data = $this->verify_request();
              if(!empty( $data)){
                        if(isset($data["id"])){
                          $buyer_info=current($this->customerModel->getBuyerInfo($data["id"]));

                         
                          if(!empty($_FILES) && isset($_FILES["image"]) && trim($_FILES["image"]["name"])!=''){

                                  $this->load->library('image_lib');
                                
                                      if ((($_FILES["image"]["type"] == "image/gif")  || ($_FILES["image"]["type"] == "image/jpeg")  || ($_FILES["image"]["type"]== "image/jpg")  || ($_FILES["image"]["type"] == "image/png")) ){
                                        // File upload configuration
                                        $images=[];
                                        $uploadPath = '../admin/uploads/bussiness_logo/';
                                      $fileName = time().'-'.$_FILES["image"]["name"];
                                      
                                        if ( move_uploaded_file($_FILES['image']['tmp_name'],  $uploadPath."".$fileName)){

                                          $config['image_library'] = 'gd2';
                                          $config['source_image'] = $uploadPath."".$fileName;
                                          $config['create_thumb'] = false;
                                          $config['maintain_ratio'] = TRUE;
                                          $config['width']     = 400;
                                          $config['height']   = 400;
                                          $this->image_lib->clear();
                                          $this->image_lib->initialize($config);
                                          $this->image_lib->resize();
                                            
                                            
                                            if(empty( $buyer_info)){
                                              $this->customerModel->insertbuyer($inputdata);
                                            }else{
                                              $this->customerModel->updateBuyer(["business_logo"=>$fileName,"update_by"=>$data["id"],"update_on"=>date('Y-m-d H:i:s')],["cust_id"=>$data["id"]]);
                                            }
                                            $status = parent::HTTP_OK;
                                            $response = ['status' => $status, 'msg' => 'Profile Image Upload successful'];
                                              
                                          $this->response($response, $status);
                                          }else{
                                            $this->response(['msg' => 'Please Select Valid Profile Image','field'=>'image'], parent::HTTP_BAD_REQUEST);
                                          }
                                      
                                      }else{
                                        $this->response(['msg' => 'Please Select Valid Profile Image','field'=>'image'], parent::HTTP_BAD_REQUEST);
                                      }
                                  
                              
                                }else{
                                  $this->response(['msg' => 'Please Select Valid Profile Image','field'=>'image'], parent::HTTP_BAD_REQUEST);
                                }
                          
                        }else{
                            $status = parent::HTTP_UNAUTHORIZED;
                            $response = ['status' => $status, 'msg' => 'Unauthorized Access! '];
                            $this->response($response, $status);
                          }
                        
              }else{
                $status = parent::HTTP_UNAUTHORIZED;
                $response = ['status' => $status, 'msg' => 'Unauthorized Access! '];
                $this->response($response, $status);
              }
            

          
            
        } catch (Exception $e) {

            $this->response(['msg' => 'server busy .Please try again some time.','field'=>'user_name'], parent::HTTP_NOT_FOUND);
        }

    }
    public function doSendOTPEmail_post(){
      try { 
          $data=$this->post();
          ////////////////////////////////////

          $data=[];
           $result=[];
            $data1 = $this->verify_request();
            if(!empty($data1)){
                $inputdata=[];
                  $inputdata["change_email_otp"]=1234;
                  
                   $this->customerModel->updateCustomer($inputdata,["id"=>$data1["id"]]);
                //////////////////////
                $status = parent::HTTP_OK;
                  $response = ['status' => $status,'msg'=>'Please check phone for OTP.'];
                  $this->response($response, $status);
                }else{
                   $status = parent::HTTP_UNAUTHORIZED;
                  $response = ['status' => $status, 'msg' => 'Unauthorized Access!'];
                  $this->response($response, $status);
                }
          
      } catch (Exception $e) {
           $this->response(['msg' => 'Invalid Request!','field'=>'name'], parent::HTTP_NOT_FOUND);
      }
  }

  public function doSendOTPchangePassword_post(){
    try { 
        $data=$this->post();
        ////////////////////////////////////

        $data=[];
         $result=[];
          $data1 = $this->verify_request();
          if(!empty($data1)){
              $inputdata=[];
                $inputdata["change_password_otp"]=1234;
                
                 $this->customerModel->updateCustomer($inputdata,["id"=>$data1["id"]]);
              //////////////////////
              $status = parent::HTTP_OK;
                $response = ['status' => $status,'msg'=>'Please check phone for OTP.'];
                $this->response($response, $status);
              }else{
                 $status = parent::HTTP_UNAUTHORIZED;
                $response = ['status' => $status, 'msg' => 'Unauthorized Access!'];
                $this->response($response, $status);
              }
        
    } catch (Exception $e) {
         $this->response(['msg' => 'Invalid Request!','field'=>'name'], parent::HTTP_NOT_FOUND);
    }
}

  public function doEmailUpdate_post(){

    //////////////////////////////////////////

        try { 

          $data=[];
          $data=$this->post();

           $result=[];
            $data1 = $this->verify_request();
            if(!empty($data1)){
             
                if(!isset($data["otp"]) || trim($data["otp"])==''){
                   $this->response(['msg' => 'Please Enter OTP!','field'=>'otp'], parent::HTTP_UNAUTHORIZED);
                }else if(!isset($data["email"]) || trim($data["email"])==''){
                   $this->response(['msg' => 'Please Enter Email Id!','field'=>'email'], parent::HTTP_UNAUTHORIZED);
                }else if(!(isset($data["email"]) && trim($data["email"])!='' && preg_match('/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix', $data["email"]))){
                   $this->response(['msg' => 'Please Enter Valid Email Id!','field'=>'email'], parent::HTTP_UNAUTHORIZED);
                }else{
                  $cdata=[];
                  $cdata["change_email_otp"]=$data["otp"];
                  $cdata["id"]=$data1["id"];
                   $customer=$this->customerModel->getUserList($cdata);
                   if(!empty($customer)){
                      $inputdata=[];
                      $inputdata["change_email_otp"]=null;
                       $inputdata["email"]=$data["email"];
                       $this->customerModel->updateCustomer($inputdata,["id"=>$data1["id"]]);
                       $status = parent::HTTP_OK;
                        $response = ['status' => $status,'msg'=>'Email Id Updated Successfully.'];
                        $this->response($response, $status);
                    }else{
                      $this->response(['msg' => 'Invalid OTP','field'=>'otp'], parent::HTTP_NOT_FOUND);
                    }
                }

                $inputdata=[];
                  $inputdata["change_email_otp"]=1234;
                   $this->customerModel->updateCustomer($inputdata,["id"=>$data1["id"]]);
                //////////////////////
                $status = parent::HTTP_OK;
                  $response = ['status' => $status,'msg'=>'Please check phone for OTP.'];
                  $this->response($response, $status);
                }else{
                   $status = parent::HTTP_UNAUTHORIZED;
                  $response = ['status' => $status, 'msg' => 'Unauthorized Access!'];
                  $this->response($response, $status);
                }
          
      } catch (Exception $e) {
           $this->response(['msg' => 'Invalid Request!','field'=>'name'], parent::HTTP_NOT_FOUND);
      }
    ///////////////////



  }

    

}
?>