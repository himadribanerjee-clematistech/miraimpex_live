<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: POST");
class Rfq extends REST_Controller {
	
	public function __construct() {
        parent::__construct();
        $this->load->helper(['jwt', 'authorization']); 
        $this->load->model("itemModel");
        $this->load->model("rfqModel");
         $this->load->model("customerModel");
         $this->load->model("categoryModel");
         
	 }
     public function index_post(){
        $status = parent::HTTP_UNAUTHORIZED;
            $response = ['status' => $status, 'msg' => 'Unauthorized Access! '];
            $this->response($response, $status);
     }
  	 private function verify_request(){
         $headers = $this->input->request_headers();
         if(isset($headers['Authorization'])){
             $token = $headers['Authorization'];
             try {
                 $data = AUTHORIZATION::validateToken($token);
                    if ($data === false) {
                        $status = parent::HTTP_UNAUTHORIZED;
                        $response = ['status' => $status, 'msg' => 'Unauthorized Access!'];
                        $this->response($response, $status);

                        exit();
                    } else {
                        $customer_data=current($this->customerModel->doAuthenticateCheck(["token_id"=>$data->token_id]));
                        
                        return $customer_data;
                    }
            } catch (Exception $e) {
               $status = parent::HTTP_UNAUTHORIZED;
                $response = ['status' => $status, 'msg' => 'Unauthorized Access! '];
                $this->response($response, $status);
            }
        }else if(isset($headers['authorization'])){
             $token = $headers['authorization'];
             $data = AUTHORIZATION::validateToken($token);
                if ($data === false) {
                    $status = parent::HTTP_UNAUTHORIZED;
                    $response = ['status' => $status, 'msg' => 'Unauthorized Access!'];
                    $this->response($response, $status);

                    exit();
                } else {
                    $customer_data=current($this->customerModel->doAuthenticateCheck(["token_id"=>$data->token_id]));
                    
                    return $customer_data;
                }
        }else{
             $status = parent::HTTP_UNAUTHORIZED;
                $response = ['status' => $status, 'msg' => 'Unauthorized Access!'];
                $this->response($response, $status);

                exit();
        }
        
        
    }
   public function getRfqlist_post(){
    try {
        $data = $this->verify_request();
        $details=$this->input->post();
        if(!empty($data)){
            if(trim($data["id"])==''){
                $this->response(['msg' => 'Unauthorized Access!','field'=>'supp_id'], parent::HTTP_UNAUTHORIZED);
            }else{
                if(!isset($details["lang"])){
                    $details["lang"]='en';
                }
                 $limit=20;
                  $start=0;
                  if(!isset($details["pageno"])){
                    $details["pageno"]=1;
                  }
                  if((isset($details["pageno"])&& trim($details["pageno"])!='')){
                    $start=($details["pageno"]-1)* $limit;
                  }
                  $rfqdata=[];
                if($data["supplier_YN"]==0){
                    $rfqdata=$this->rfqModel->getRFQ(["buyer_id"=>$data["id"]],$limit,$start);
                    //echo $this->db->last_query();
                    //print_r($rfqdata);die();
                   
                }else{

                    $itemdata=$this->itemModel->getItem(["supp_id"=>$data["id"]]);
                    $cat_ids=implode(",",array_column($itemdata,"category_id"));
                    if(empty($cat_ids)){
                        $cat_ids="-1";
                    }
                    $rfqdata=$this->rfqModel->getRFQ(["cat_ids_buyer"=>$cat_ids,"buyer_id_buyer"=>$data["id"]],$limit,$start);

                    //echo $this->db->last_query();die();
                    
                }
               
               
                $result=[];
                $ids=[];
                foreach($rfqdata as $val){
                    $dataset=[];
                    $ids[]=$val["rfq_id"];
                    $dataset["rfq_id"]=base64_encode($val["rfq_id"]);
                    $dataset["buyer_id"]=base64_encode($val["buyer_id"]);
                    $dataset["cat_id"]=base64_encode($val["cat_id"]);
                    $dataset["uom_id"]=base64_encode($val["uom_id"]);
                    $dataset["quantity"]=$val["quantity"];
                    $dataset["message"]=$val["message"];
                    $dataset["other_requerment"]=$val["other_requerment"];
                    $dataset["qrname"]=$val["qrname"];
                    $dataset["rfqlocation"]=$val["rfqlocation"];
                    $dataset["latitudes"]=$val["latitudes"];
                    $dataset["longitude"]=$val["longitude"];
                    $dataset["business_logo"]=$val["business_logo"];
                     $dataset["buyer_name"]=$val["name"];
                    $dataset["buyer_email"]=$val["email"];
                    $dataset["buyer_phone"]=$val["phone"];
                    $dataset["created_date"]=$val["created_date"];
                    $dataset["images"]=[];
                    if($details["lang"]=='en'){
                        $dataset["category_name"]=$val["category_name_en"];
                        $dataset["uom_name"]=$val["uom_name_en"];
                    }else{
                        $dataset["category_name"]=$val["category_name_fr"];
                        $dataset["uom_name"]=$val["uom_name_fr"];
                    }
                    $result[base64_encode($val["rfq_id"])]=$dataset;
                }
                
                if(empty($ids)){
                    $ids=['-1'];
                }
                $rfqimages=$this->rfqModel->getrfqmage($ids);
                foreach( $rfqimages as $val2){
                    $result[base64_encode($val2["req_id"])]["images"][]=["image_id"=>base64_encode($val2["image_id"]),"image_name"=>$val2["image"]];
                }
                $status = parent::HTTP_OK;
                $response = ['status' => $status,'msg'=>'RFQ List.','data'=>array_values($result)];
                 $this->response($response, $status);
            }
        }else{
            $this->response(['msg' => 'Unauthorized Access!','field'=>'supp_id'], parent::HTTP_UNAUTHORIZED);
        }
    } catch (Exception $e) {

        $this->response(['msg' => 'server busy .Please try again some time.','field'=>'user_name'], parent::HTTP_NOT_FOUND);
    }
   }
    public function addRfq_post(){

       try {
        $details=$this->input->post();
         $data = $this->verify_request();
        
          if(!empty($details)){
             if(trim($data["id"])==''){
                 $this->response(['msg' => 'Unauthorized Access!','field'=>'supp_id'], parent::HTTP_UNAUTHORIZED);
             }else if(!((isset($details["rfqName"])  &&  trim($details["rfqName"])!='') )){

                 $this->response(['msg' => 'Invalid RFQ name!','field'=>'rfqName'], parent::HTTP_BAD_REQUEST);

             }else if(!((isset($details["cat_id"])  &&  trim($details["cat_id"])!=''))){
                 $this->response(['msg' => 'Invalid Category!','field'=>'cat_id'], parent::HTTP_BAD_REQUEST);
             }else if(!((isset($details["quantity"])  &&  trim($details["quantity"])!=''))){
                 $this->response(['msg' => 'Invalid quantity!','field'=>'quantity'], parent::HTTP_BAD_REQUEST);
             }else if(trim($details["quantity"])!='' && !preg_match('/^[0-9]*$/', $details["quantity"])){
                 $this->response(['msg' => 'Please Enter Valid quantity','field'=>'quantity'], parent::HTTP_BAD_REQUEST);
             }else if(!((isset($details["uom_id"])  &&  trim($details["uom_id"])!=''))){
                 $this->response(['msg' => 'Invalid Uom!','field'=>'uom_id'], parent::HTTP_BAD_REQUEST);
             }else if(!((isset($details["message"])  &&  trim($details["message"])!=''))){
                 $this->response(['msg' => 'Invalid message!','field'=>'message'], parent::HTTP_BAD_REQUEST);
             }else{

                  $inserted_data=[];
                  $inserted_data["cat_id"]=base64_decode($details["cat_id"]);
                  $inserted_data["uom_id"]=base64_decode($details["uom_id"]);
                  $inserted_data["buyer_id"]=$data["id"];
                  $inserted_data["qrname"]=$details["rfqName"];
                  $inserted_data["quantity"]=$details["quantity"];
                  $inserted_data["message"]=$details["message"];
                  $inserted_data["rfqlocation"]=(isset($details["rfqLocation"]))?$details["rfqLocation"]:'';
                  $inserted_data["latitudes"]=(isset($details["lat"]))?$details["lat"]:'';
                  $inserted_data["longitude"]=(isset($details["long"]))?$details["long"]:'';
                  
                  $inserted_data["other_requerment"]=$details["other_requerment"];
                  $catdeatails=$this->categoryModel->getCategory(["id"=>$inserted_data["cat_id"]]);
                   $uomdeatails=$this->categoryModel->getuom(["id"=>$inserted_data["uom_id"]]);

                   if(!empty($catdeatails)){
                       if(!empty($uomdeatails)){
                           
                              $id=$this->rfqModel->insertRfq($inserted_data);


                          ////////////////////////////image upload//////////////////////


                           $files = $_FILES;
                            $not_inseted=[];
                            $this->load->library('image_lib');
                            $inserted_image=[];
                            if(isset($_FILES['image']['name'])){
                                $cpt = count($_FILES['image']['name']);
                                 $insert=0;
                                for($i=0; $i<$cpt; $i++){  
                                  if (trim($files['image']['name'][$i]) =='') {
                                      continue;
                                  }  
                                   $fileName = time().'-'.$files['image']['name'][$i];
                                  $allowedExts = array("gif", "jpeg", "jpg", "png");
                                  $image_data=explode(".", $files['image']['name'][$i]);
                                  $_FILES['image']['name']        = $fileName;
                                  $_FILES['image']['type']        = $files['image']['type'][$i];
                                  $_FILES['image']['tmp_name']    = $files['image']['tmp_name'][$i];
                                  $_FILES['image']['error']       = $files['image']['error'][$i];
                                  $_FILES['image']['size']        = $files['image']['size'][$i]; 

                                  if ((($_FILES["image"]["type"] == "image/gif")  || ($_FILES["image"]["type"] == "image/jpeg")  || ($_FILES["image"]["type"] == "image/jpg")  || ($_FILES["image"]["type"] == "image/png") )) {
                                    $uploadPath = '../admin/uploads/rfq/';
                                         
                                    if ( move_uploaded_file($_FILES['image']['tmp_name'],  $uploadPath."".$fileName)){
                                      $config = [];  
                                      $config['image_library'] = 'gd2';
                                      $config['source_image'] = $uploadPath."".$fileName;
                                      $config['create_thumb'] = false;
                                      $config['maintain_ratio'] = TRUE;
                                      $config['width']     = 600;
                                      $config['height']   = 600;
                                      $this->image_lib->clear();
                                      $this->image_lib->initialize($config);
                                      $this->image_lib->resize();
                                      $sellerImg = [ 'req_id' =>  $id, 'image'     =>  $fileName];
                                      $inserted_image[]= $sellerImg;
                                      $insert = $this->rfqModel->insertRfqImage($sellerImg);
                                    }else{
                                      $not_inseted[]=$files['image']['name'][$i];
                                    }

                                   
                                  }else{
                                   $not_inseted[]= $_FILES['image']['name'];
                                  }
                                }

                            }
                           
                              $status = parent::HTTP_OK;
                                        $response = ['status' => $status,'msg'=>'Thank you for contacting us.Please wait for Our Seller Response.','not_inseted'=>implode(",", $not_inseted)];
                                         $this->response($response, $status);
                       }else{
                           $this->response(['msg' => 'Invalid UOM!','field'=>'uom_id'], parent::HTTP_BAD_REQUEST);
                       }
                     


                   }else{
                         $this->response(['msg' => 'Invalid category!','field'=>'cat_id'], parent::HTTP_BAD_REQUEST);
                   }
                 


              }


             
            
            
        }else{
           $status = parent::HTTP_UNAUTHORIZED;
              $response = ['status' => $status, 'msg' => 'Unauthorized Access! '];
              $this->response($response, $status);
        }

       } catch (Exception $e) {

            $this->response(['msg' => 'server busy .Please try again some time.','field'=>'user_name'], parent::HTTP_NOT_FOUND);
        }
    }
  
   
   
}
?>
