<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class RfqModel extends CI_Model {
	protected $rfqTable = 'rfq';
    protected $rfqImageTable = 'rfq_image';
    protected $customerTable = 'customer';
    protected $BuyerProfileTable = 'buyer_profile';
    
    protected $uomTable = 'uom';
    protected $catTable = 'category';
	public function __construct() {
        parent::__construct();
    }
      public function insertRfq($data){

        try {
            $this->db->db_debug = FALSE;
             $this->db->insert($this->rfqTable, $data);
            
            $db_error = $this->db->error();
           if (!empty($db_error) && $db_error['code']>0) {
                throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
                return false; // unreachable retrun statement !!!
            }
            return  $this->db->insert_id();;
        } catch (Exception $e) {
            throw new Exception();
            return;
        }
        
    }
    public function getRFQ($data ,$limit='', $start='') {
        try {
            $this->db->db_debug = true;
            $this->db->select('rfq.rfq_id,rfq.buyer_id,rfq.cat_id,rfq.uom_id,rfq.quantity,rfq.message,rfq.other_requerment,rfq.qrname,rfq.rfqlocation,rfq.latitudes,rfq.longitude,cust.name,cust.email,cust.phone,cat.category_name_en,cat.category_name_fr,uom.name_en as uom_name_en,uom.name_fr as uom_name_fr,rfq.created_date,buyer.business_logo');
            $this->db->from($this->rfqTable." rfq");
            $this->db->join($this->customerTable." cust","cust.id=rfq.buyer_id");
            $this->db->join($this->BuyerProfileTable." buyer","cust.id=buyer.cust_id","left");
             $this->db->join($this->catTable." cat","cat.id=rfq.cat_id");
              $this->db->join($this->uomTable." uom","uom.id=rfq.uom_id");

              

             if(isset($data["cat_ids"]) && !empty($data["cat_ids"])){
                $this->db->where_in("rfq.cat_id",$data["cat_ids"]);
            }  

            if(isset($data["cat_ids_buyer"]) && !empty($data["cat_ids_buyer"]) && isset($data["buyer_id_buyer"]) && trim($data["buyer_id_buyer"])!='' ){
                $this->db->where("(rfq.cat_id IN (".$data["cat_ids_buyer"].") ||  rfq.buyer_id=".$data["buyer_id_buyer"].")");
            }  
              if(isset($data["rfq_id"]) && trim($data["rfq_id"])!=''){
                $this->db->where("rfq.rfq_id",$data["iterfq_idm_id"]);
            }  
             if(isset($data["buyer_id"]) && trim($data["buyer_id"])!=''){
                $this->db->where("rfq.buyer_id",$data["buyer_id"]);
            } 
            
             $this->db->where("rfq.status",1);
            
             if(isset($limit) && trim($limit)!=''){
                $this->db->limit($limit,$start);
            }   


            $query = $this->db->get(); 

            $db_error = $this->db->error();

            if (!empty($db_error) && $db_error['code']>0) {
                throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
                return false; // unreachable retrun statement !!!
            }else{
               // echo $this->db->last_query();die();
                $result=[];
                foreach($query->result_array() as $row){
                    $result[]=$row;
                }
                return $result;
            }
         } catch (Exception $e) {
            throw new Exception();
            return;
        }    
    }
     
      public function insertRfqImage($data) {
        try {
            $this->db->db_debug = true;
            $res = $this->db->insert($this->rfqImageTable, $data);

            $db_error = $this->db->error();
            if (!empty($db_error) && $db_error['code']>0) {
                throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
                return false; // unreachable retrun statement !!!
            }
            else {
                //echo $this->db->last_query() . '<br>';
                if ($res) {
                    return  $this->db->insert_id();
                }
                else {
                    return false;
                }
            }
            //return  $this->db->insert_id();
        } catch (Exception $e) {
            throw new Exception();
            return;
        }
    } 

   
   public function getrfqmage($ids){
        try {
            $this->db->db_debug = false;

            $this->db->select(" i.id as image_id, i.image,i.req_id  ");
            $this->db->from($this->rfqImageTable . ' i');
            $this->db->join($this->rfqTable . ' rfq', 'rfq.rfq_id = i.req_id');
            
            
            
            if(isset($ids) && !empty($ids)){ 
                $this->db->where("i.req_id in(".implode(",",$ids).")");
            }
            
            $this->db->order_by("i.id","DESC");
            $query=$this->db->get();
            $db_error = $this->db->error();
            if (!empty($db_error) && $db_error['code']>0) {
                throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
                return false; // unreachable retrun statement !!!
            }else{
                //echo $this->db->last_query();die();
                $result=[];
                foreach($query->result_array() as $row){
                    $result[]=$row;
                }
                return $result;
            }
        } catch (Exception $e) {
           // echo $this->db->last_query();die();
           throw new Exception();
        }
    }


    

    
}

?>
