<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class ItemModel extends CI_Model {
	protected $itemTable = 'item';
	protected $localsupplierTable ='local_supplier';
    protected $customerTable = 'customer';
    protected $sellerTable = 'seller_profile';
    protected $buyerTable = 'buyer_profile';
    protected $uomTable = 'uom';
     protected $attributeTable = 'attribute';
	public function __construct() {
        parent::__construct();
    }
      public function insertItem($data){

        try {
            $this->db->db_debug = FALSE;
             $this->db->insert($this->itemTable, $data);
            
            $db_error = $this->db->error();
           if (!empty($db_error) && $db_error['code']>0) {
                throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
                return false; // unreachable retrun statement !!!
            }
            return  $this->db->insert_id();;
        } catch (Exception $e) {
            throw new Exception();
            return;
        }
        
    }

     public function updateItem($data,$where){
        
        try {
            $this->db->db_debug = False;
            $this->db->trans_start(FALSE);
            $this->db->where($where);
            $this->db->update($this->itemTable, $data);
            $this->db->trans_complete();
            $db_error = $this->db->error();
                if (!empty($db_error) && $db_error['code']>0) {
                throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
                return false; // unreachable retrun statement !!!
            }
            return TRUE;
        } catch (Exception $e) {
            throw new Exception();
            return;
        }
    }
    public function getlistVarience($data){
        $this->db->db_debug = true;
        if($data["lang"]=='en')
        {
          $this->db->select("v.id,v.name_en as name,v.type,v.status");
        }elseif($data["lang"]=='fr')
        {
          $this->db->select("v.id,v.name_fr as name,v.type,v.status");
        }else{
          $this->db->select("v.id,v.name_en as name,v.type,v.status");
        }
        $this->db->from('variance v' );
        $this->db->where("v.status",1);
        $query=$this->db->get();
        $db_error = $this->db->error();
        return $listVariance = $query->result_array();
    }
    public function getItem($data ,$limit='', $start='') {
        try {
            $this->db->db_debug = true;
            $this->db->select('itm.id as item_id, itm.supp_id as seller_id, itm.local_sup_id, itm.item_name_en, itm.item_name_fr, itm.description_en, itm.description_fr, itm.category_id, itm.category_json, itm.price_min, itm.price_max, itm.currency, itm.lead_time, itm.moq, itm.packaging, itm.variance, itm.attribute, itm.thumbnail, itm.item_image, itm.showcase, itm.price_uom_id, itm.active, itm.is_approved,priceuom.name_en as price_uom_name_en,priceuom.name_fr as price_uom_name_fr,cust.name,cust.phone,cust.email,moquom.name_en as moq_uom_name_en,moquom.name_fr as moq_uom_name_fr,itm.moq_uom_id,itm.keywords,itm.on_display,sellerp.business_name,sellerp.address,sellerp.country,sellerp.state,sellerp.city,sellerp.contact_name,sellerp.contact_number,buyer.business_name as buyer_name,buyer.business_logo as profile_image,buyer.address as buyer_address,buyer.city as buyer_city,buyer.country as buyer_country,buyer.state as buyer_state,cust.sellerCategory,cust.displayLocal,itm.fixedPrice,itm.delivery_available,itm.shipping_time');
            $this->db->from($this->itemTable." itm");
            $this->db->join($this->localsupplierTable." lcl","lcl.id=itm.local_sup_id",'Left');
             $this->db->join($this->customerTable." cust","cust.id=itm.supp_id");
              $this->db->join($this->uomTable." priceuom","priceuom.id=itm.price_uom_id");
              $this->db->join($this->uomTable." moquom","moquom.id=itm.moq_uom_id");
              $this->db->join($this->sellerTable . ' sellerp', 'cust.id = sellerp.cust_id',"left");
              $this->db->join($this->buyerTable . ' buyer', 'cust.id = buyer.cust_id',"left");
              if(isset($data["ids"]) && !empty($data["ids"])){
                $this->db->where_in("itm.category_id",$data["ids"]);
                }  
                if(isset($data["supp_ids"]) && !empty($data["supp_ids"])){
                    $this->db->where_in("itm.supp_id",$data["supp_ids"]);
                    }  
              if(isset($data["displayLocal"]) && trim($data["displayLocal"])!=''){
                $this->db->where("cust.displayLocal",$data["displayLocal"]);
            }  
            if(isset($data["item_id"]) && trim($data["item_id"])!=''){
                $this->db->where("itm.id",$data["item_id"]);
            }
            if(isset($data["search_keyword"]) && trim($data["search_keyword"])!=''){
                $this->db->like('itm.item_name_en', $data["search_keyword"]);
                $this->db->or_like('itm.item_name_fr', $data["search_keyword"]);
                $this->db->or_like('itm.keywords', $data["search_keyword"]);
            }
            if(isset($data["item_id_arr"]) && count($data["item_id_arr"])>0){
                $this->db->where_in("itm.id",$data["item_id_arr"]);
            }  
               if(isset($data["supp_id"]) && trim($data["supp_id"])!=''){
                $this->db->where("itm.supp_id",$data["supp_id"]);
            } 
            if(isset($data["category_json"]) && trim($data["category_json"])!=''){
                $this->db->where("JSON_SEARCH(`category_json`, 'one', ".$data["category_json"].") IS NOT NULL");
            } 
            if(isset($data["showcase"]) && trim($data["showcase"])!=''){
                $this->db->where("itm.showcase",$data["showcase"]);
            } 
            if(isset($data["group_by"]) && !empty($data["group_by"])){
              foreach($data["group_by"] as $k){
                $this->db->group_by($k); 
              }
            } 
             $this->db->where("itm.is_deleted",0);
             $this->db->where("itm.active",1);
             if(isset($limit) && trim($limit)!=''){
                $this->db->limit($limit,$start);
            }   


            $query = $this->db->get(); 

            $db_error = $this->db->error();

            if (!empty($db_error) && $db_error['code']>0) {
                throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
                return false; // unreachable retrun statement !!!
            }else{
                //echo $this->db->last_query();die();
                $result=[];
                foreach($query->result_array() as $row){
                    $result[]=$row;
                }
                return $result;
            }
         } catch (Exception $e) {
            throw new Exception();
            return;
        }    
    }

    public function getAttribute() {
        try {
            $this->db->db_debug = true;
            $this->db->select('id,name_en,name_fr, type');
            $this->db->from($this->attributeTable);
            


            $query = $this->db->get(); 

            $db_error = $this->db->error();

            if (!empty($db_error) && $db_error['code']>0) {
                throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
                return false; // unreachable retrun statement !!!
            }else{
               // echo $this->db->last_query();die();
                $result=[];
                foreach($query->result_array() as $row){
                    $result[]=$row;
                }
                return $result;
            }
         } catch (Exception $e) {
            throw new Exception();
            return;
        }    
    }

  


   
    
   
   

    

    
}

?>