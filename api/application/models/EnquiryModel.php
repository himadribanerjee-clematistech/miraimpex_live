<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class EnquiryModel extends CI_Model {
	protected $enqTable = 'enquiry';
	protected $customerTable = 'customer';
	protected $sellerTable = 'seller_profile';
	protected $buyerTable = 'buyer_profile';
	public function __construct() {
		parent::__construct();
		
    }
   
   
  
     public function insertEnquiry($data) {
    	try {
    	 	$this->db->db_debug = true;
    	 	$res = $this->db->insert($this->enqTable, $data);

    	 	$db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }
	        else {
	        	//echo $this->db->last_query() . '<br>';
	        	if ($res) {
	        		return  $this->db->insert_id();
	        	}
	        	else {
	        		return false;
	        	}
	        }
	        //return  $this->db->insert_id();
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }
	}
	
	public function getEnquiryList($data ,$limit='', $start=''){
		try {

			$this->db->db_debug = true;
			$this->db->select("enq.id as enq_id, enq.to_id as enq_to_id, enq.from_id as enq_from_id, enq.seller_item_id as enq_seller_item_id, enq.type as enq_type, enq.message as enq_message, enq.date as enq_date, buycust.name as buyer_name,buyer.business_logo as buyer_profile_image, sellcust.name as seller_name,seller.business_logo as seller_profile_image,enq.attachments,sellerp.business_name,sellerp.address,sellerp.country,sellerp.state,sellerp.city,sellerf.business_name as from_business_name");
            $this->db->from($this->enqTable . ' enq');
    	$this->db->join($this->customerTable . ' buycust', 'enq.from_id = buycust.id');
		$this->db->join($this->buyerTable . ' buyer', 'buycust.id = buyer.cust_id',"left");
        $this->db->join($this->customerTable . ' sellcust', 'enq.to_id = sellcust.id');
		$this->db->join($this->buyerTable . ' seller', 'sellcust.id = seller.cust_id',"left");
		$this->db->join($this->sellerTable . ' sellerp', 'sellcust.id = sellerp.cust_id',"left");
		$this->db->join($this->sellerTable . ' sellerf', 'sellerf.id = buyer.cust_id',"left");
		
		if(isset($data["customer_id"]) && $data["customer_id"]!='')
    	{ 
			$this->db->where("( enq.to_id ='".$data["customer_id"]."' || enq.from_id='".$data["customer_id"]."')"  );
    	}

    	if(isset($data["enq_id"]) && $data["enq_id"]!='')
    	{ 
			$this->db->where("enq.id" , $data["enq_id"]);
    	}
    
    	if(!empty($data["order_by"]))
    	{
    		foreach($data["order_by"] as $val)
    		{
    			$this->db->order_by($val[0],$val[1]);
    		}
    	}

    	$query=$this->db->get();
        $db_error = $this->db->error();

        if (!empty($db_error) && $db_error['code']>0) 
        {
            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
        }else{
            //echo $this->db->last_query();die();
        	$result=[];
        	foreach($query->result_array() as $row)
        	{
        		$result[]=$row;
        	}

        	return $result;
        }
		} catch (Exception $e) {
            throw new Exception();
            return;
        }  	

	}
    
    
    


}

?>