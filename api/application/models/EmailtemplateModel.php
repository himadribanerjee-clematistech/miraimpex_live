<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class EmailtemplateModel extends CI_Model {	
	
	public function __construct() {
        parent::__construct();
        
    }   

   public function sendEmail($data){
    try {
        
        $this->load->library('email');
        $fromemail="info@miraimpex.com";
        $toemail = $data['toemail'];
        $subject = $data['operationName'];
        if(!isset($data['operationTitle']))
        {
            $title = $data['operationName'];
        }else{
            $title = $data['operationTitle'];
        }
                
        $emailContent = $this->load->view('template/email',$data,true);


        $config=array(
        'charset'=>'utf-8',
        'wordwrap'=> TRUE,
        'mailtype' => 'html'
        );

        $this->email->initialize($config);

        $this->email->to($toemail);
        $this->email->from($fromemail, $title);
        $this->email->subject($subject);
        $this->email->message($emailContent);
        $mail = $this->email->send(); 
        
        
    } catch (Exception $e) {
       throw new Exception();
    }
  }


}

?>