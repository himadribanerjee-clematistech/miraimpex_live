<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class MessagecenterModel extends CI_Model {
	protected $rfqTable = 'rfq';
    protected $rfqImageTable = 'rfq_image';
    protected $customerTable = 'customer';
    protected $BuyerProfileTable = 'buyer_profile';

    protected $SellerProfileTable = 'seller_profile';
     protected $rfqThreadTable = 'rfq_thread';
	protected $rfqThreadImagesTable = 'rfq_thread_image';
	protected $sellerEnquiryTable = 'enquiry';
	protected $enquiryThreadTable = 'enquiry_thread';
	protected $enquiryThreadImagesTable = 'enquiry_image';
    protected $uomTable = 'uom';
    protected $catTable = 'category';
    protected $proposalTable = 'proposal';
    protected $orderlogTable = 'order_log';
	public function __construct() {
        parent::__construct();
    }
    public function updateProposal($data,$where){
    	
    	try {
    	 	$this->db->db_debug = False;
    	 	$this->db->trans_start(FALSE);
	        $this->db->where($where);
	    	$this->db->update($this->proposalTable, $data);
	        $this->db->trans_complete();
	        $db_error = $this->db->error();
	            if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }
	        return TRUE;
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }
    }
    public function updateOrder($data,$where){
    	
    	try {
    	 	$this->db->db_debug = False;
    	 	$this->db->trans_start(FALSE);
	        $this->db->where($where);
	    	$this->db->update($this->orderlogTable, $data);
	        $this->db->trans_complete();
	        $db_error = $this->db->error();
	            if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }
	        return TRUE;
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }
    }
    public function getProposal($data,$limit='', $start=''){
        try {
            $this->db->db_debug = true;
            $this->db->select('p.id,p.from_id,p.to_id,p.title,p.description,p.file,p.amount,p.created_date,p.currency,p.refference_id,p.refference_type,p.is_paid,custto.name as to_name,custto.email as to_email,custto.phone as to_phone,buyerto.business_logo as to_profile_image, sellerto.business_name as to_business_name,custfrom.name as from_name,custfrom.email as from_email,custfrom.phone as from_phone,buyerfrom.business_logo as from_profile_image, sellerfrom.business_name as from_business_name');
            $this->db->from($this->proposalTable." p");
            $this->db->join($this->customerTable." custto",'custto.id=p.to_id');
            $this->db->join($this->BuyerProfileTable." buyerto",'buyerto.cust_id=p.to_id','left');
            $this->db->join($this->SellerProfileTable." sellerto",'sellerto.cust_id=p.to_id','left');

            $this->db->join($this->customerTable." custfrom",'custfrom.id=p.from_id');
            $this->db->join($this->BuyerProfileTable." buyerfrom",'buyerfrom.cust_id=p.from_id','left');
            $this->db->join($this->SellerProfileTable." sellerfrom",'sellerfrom.cust_id=p.from_id','left');
            if(isset($data["refference_id"]) && trim($data["refference_id"])!=''){
                $this->db->where("p.refference_id",$data["refference_id"]);
            } 
            if(isset($data["refference_type"]) && trim($data["refference_type"])!=''){
                $this->db->where("p.refference_type",$data["refference_type"]);
            }
            if(isset($data["id"]) && trim($data["id"])!=''){
                $this->db->where("p.id",$data["id"]);
            }
            if(isset($data["from_id"]) && $data["from_id"]!='' && isset($data["to_id"]) && $data["to_id"]!='' )
            { 
                $this->db->where("( (p.to_id= '".$data["to_id"]."' &&  p.from_id= '".$data["from_id"]."' ) OR(p.to_id= '".$data["from_id"]."' &&  p.from_id= '".$data["to_id"]."' ) ) ");
                
            }
            if(isset($data["user_id"]) && $data["user_id"]!=''  )
            { 
                $this->db->where("( (p.to_id= '".$data["user_id"]."' ) OR( p.from_id= '".$data["user_id"]."' ) ) ");
                
            }
	    	$this->db->order_by("id","DESC");
	    	
             if(isset($limit) && trim($limit)!=''){
                $this->db->limit($limit,$start);
            }   


            $query = $this->db->get(); 

            $db_error = $this->db->error();

            if (!empty($db_error) && $db_error['code']>0) {
                throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
                return false; // unreachable retrun statement !!!
            }else{
               // echo $this->db->last_query();die();
                $result=[];
                foreach($query->result_array() as $row){
                    $result[]=$row;
                }
                return $result;
            }
         } catch (Exception $e) {
            throw new Exception();
            return;
        }    
    }
     public function addProposal($data){
        try{
    		$this->db->db_debug = false;

    		$insert = $this->db->insert($this->proposalTable, $data);

    		$db_error = $this->db->error();
    		if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	if($insert) 
	        	{
	        		return  $this->db->insert_id();
	        	}else{
	        		return false;
	        	}
	        }
    	}
    	catch(Exception $e) {
            echo $this->db->last_query();die();
    		throw new Exception("Error Processing Request", 1);    		
    	}
     }
     public function getOrderLog($data,$limit='', $start=''){
        try {
            $this->db->db_debug = true;
            $this->db->select('o.id as order_id,o.transction_id,o.created_date as order_date,o.status,p.id as proposal_id,p.from_id,p.to_id,p.title,p.description,p.file,p.amount,p.created_date,p.currency,p.refference_id,p.refference_type,p.is_paid,custto.name as to_name,custto.email as to_email,custto.phone as to_phone,buyerto.business_logo as to_profile_image, sellerto.business_name as to_business_name,custfrom.name as from_name,custfrom.email as from_email,custfrom.phone as from_phone,buyerfrom.business_logo as from_profile_image, sellerfrom.business_name as from_business_name,o.total_amount,o.payment_mode,o.item_details,o.buyer_info,custfromo.name as seller_name,custfromo.email as seller_email,custfromo.phone as seller_phone,buyerfromo.business_logo as seller_profile_image, sellerfromo.business_name as seller_business_name,custtoo.name as ofrom_name,custtoo.email as ofrom_email,custtoo.phone as ofrom_phone,buyertoo.business_logo as ofrom_profile_image, sellertoo.business_name as ofrom_business_name,o.user_id,o.seller_id,o.order_status');
            $this->db->from($this->orderlogTable." o");
            $this->db->join($this->proposalTable." p",'o.proposal_id=p.id','left');
            $this->db->join($this->customerTable." custto",'custto.id=p.to_id','left');
            $this->db->join($this->BuyerProfileTable." buyerto",'buyerto.cust_id=p.to_id','left');
            $this->db->join($this->SellerProfileTable." sellerto",'sellerto.cust_id=p.to_id','left');

            $this->db->join($this->customerTable." custfrom",'custfrom.id=p.from_id','left');
            $this->db->join($this->BuyerProfileTable." buyerfrom",'buyerfrom.cust_id=p.from_id','left');
            $this->db->join($this->SellerProfileTable." sellerfrom",'sellerfrom.cust_id=p.from_id','left');
             $this->db->join($this->customerTable." custfromo",'custfromo.id=o.seller_id','left');
            $this->db->join($this->BuyerProfileTable." buyerfromo",'buyerfromo.cust_id=o.seller_id','left');
            $this->db->join($this->SellerProfileTable." sellerfromo",'sellerfromo.cust_id=o.seller_id','left');


             $this->db->join($this->customerTable." custtoo",'custtoo.id=o.user_id','left');
            $this->db->join($this->BuyerProfileTable." buyertoo",'buyertoo.cust_id=o.user_id','left');
            $this->db->join($this->SellerProfileTable." sellertoo",'sellertoo.cust_id=o.user_id','left');


            if(isset($data["refference_id"]) && trim($data["refference_id"])!=''){
                $this->db->where("p.refference_id",$data["refference_id"]);
            } 
            if(isset($data["refference_type"]) && trim($data["refference_type"])!=''){
                $this->db->where("p.refference_type",$data["refference_type"]);
            }
            if(isset($data["order_id"]) && trim($data["order_id"])!=''){
                $this->db->where("o.id",$data["order_id"]);
            }
            if(isset($data["proposal_id"]) && trim($data["proposal_id"])!=''){
                $this->db->where("p.id",$data["proposal_id"]);
            }
            if(isset($data["from_id"]) && $data["from_id"]!='' && isset($data["to_id"]) && $data["to_id"]!='' )
            { 
                $this->db->where("( (p.to_id= '".$data["to_id"]."' &&  p.from_id= '".$data["from_id"]."' ) OR(p.to_id= '".$data["from_id"]."' &&  p.from_id= '".$data["to_id"]."' ) ) ");
                
            }
            if(isset($data["user_id"]) && $data["user_id"]!=''  )
            { 
                $this->db->where("( (p.to_id= '".$data["user_id"]."' ) OR( p.from_id= '".$data["user_id"]."' )   OR ( o.user_id= '".$data["user_id"]."' OR o.seller_id='".$data["user_id"]."' ) ) ");
                
            }
	    	$this->db->order_by("o.id","DESC");
	    	
             if(isset($limit) && trim($limit)!=''){
                $this->db->limit($limit,$start);
            }   


            $query = $this->db->get(); 

            $db_error = $this->db->error();

            if (!empty($db_error) && $db_error['code']>0) {
                throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
                return false; // unreachable retrun statement !!!
            }else{
               // echo $this->db->last_query();die();
                $result=[];
                foreach($query->result_array() as $row){
                    $result[]=$row;
                }
                return $result;
            }
         } catch (Exception $e) {
            throw new Exception();
            return;
        }    
     }
     public function addOrderLog($data){
        try{
    		$this->db->db_debug = false;

    		$insert = $this->db->insert($this->orderlogTable, $data);

    		$db_error = $this->db->error();
    		if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	if($insert) 
	        	{
	        		return  $this->db->insert_id();
	        	}else{
	        		return false;
	        	}
	        }
    	}
    	catch(Exception $e) {
            echo $this->db->last_query();die();
    		throw new Exception("Error Processing Request", 1);    		
    	}
     }
    public function getRFQList($data ,$limit='', $start='') {
        try {
            $this->db->db_debug = true;
            $this->db->select('rfq.rfq_id,rfq.buyer_id,rfq.cat_id,rfq.uom_id,rfq.quantity,rfq.message,rfq.other_requerment,rfq.qrname,rfq.rfqlocation,rfq.latitudes,rfq.longitude,cust.name,cust.email,cust.phone,cat.category_name_en,cat.category_name_fr,uom.name_en as uom_name_en,uom.name_fr as uom_name_fr,rfq.created_date,buyer.business_logo, th.id as th_id, th.to_customer_id, tocust.name as tocust_name,tbto.business_logo as tocust_profile_image,th.from_customer_id,fromcust.name as fromcust_name,  tbfrom.business_logo as fromcust_profile_image,th.date,th.message as thread_message,COALESCE(th.is_seen,th.is_seen,0) as is_seen,th.images,sellerpf.business_name as from_business_name');
            $this->db->from($this->rfqTable." rfq");
            $this->db->join($this->customerTable." cust","cust.id=rfq.buyer_id");
            $this->db->join($this->BuyerProfileTable." buyer","cust.id=buyer.cust_id","left");
            $this->db->join($this->SellerProfileTable . ' sellerpf', 'sellerpf.cust_id =cust.id',"left");
             $this->db->join($this->catTable." cat","cat.id=rfq.cat_id");
              $this->db->join($this->uomTable." uom","uom.id=rfq.uom_id");
              $this->db->join($this->rfqThreadTable . ' th', 'th.rfq_id = rfq.rfq_id',"left");
              $this->db->join($this->customerTable . ' tocust', 'tocust.id = th.to_customer_id',"left");
	    		$this->db->join($this->BuyerProfileTable . ' tbto', 'tbto.cust_id = th.to_customer_id',"left");
    			
    			$this->db->join($this->customerTable . ' fromcust', 'fromcust.id = th.from_customer_id',"left");
	    		$this->db->join($this->BuyerProfileTable . ' tbfrom', 'tbfrom.cust_id = th.from_customer_id',"left");
               
                

             if(isset($data["cat_ids"]) && !empty($data["cat_ids"])){
                $this->db->where_in("rfq.cat_id",$data["cat_ids"]);
            }  

            if(isset($data["cat_ids_buyer"]) && !empty($data["cat_ids_buyer"]) && isset($data["buyer_id_buyer"]) && trim($data["buyer_id_buyer"])!='' ){
                $this->db->where("(rfq.cat_id IN (".$data["cat_ids_buyer"].") ||  rfq.buyer_id=".$data["buyer_id_buyer"].")");
            }  
              if(isset($data["rfq_id"]) && trim($data["rfq_id"])!=''){
                $this->db->where("rfq.rfq_id",$data["rfq_id"]);
            }  
             if(isset($data["buyer_id"]) && trim($data["buyer_id"])!=''){
                $this->db->where("rfq.buyer_id",$data["buyer_id"]);
            } 
            if(isset($data["thnull"]) && trim($data["thnull"])==1){
                $this->db->where("th.id IS  NULL");
            } 
             $this->db->where("rfq.status",1);
             if(isset($data["to_idsorfrom_ids"]) && $data["to_idsorfrom_ids"]!=''){ 
                
	    		$this->db->where("(th.to_customer_id=".$data["to_idsorfrom_ids"]." OR th.from_customer_id=".$data["to_idsorfrom_ids"]." ) ");

	    	}
	    	
	    	
	    	if(!empty($data["order_by"])){
	    		foreach($data["order_by"] as $val){
	    			$this->db->order_by($val[0],$val[1]);
	    		}
	    	}
	    	 if(!empty($data["group_by"])){
	    		foreach($data["group_by"] as $val){
	    			$this->db->group_by($val);
	    		}
	    	}
	    	
             if(isset($limit) && trim($limit)!=''){
                $this->db->limit($limit,$start);
            }   


            $query = $this->db->get(); 

            $db_error = $this->db->error();

            if (!empty($db_error) && $db_error['code']>0) {
                throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
                return false; // unreachable retrun statement !!!
            }else{
               // echo $this->db->last_query();die();
                $result=[];
                foreach($query->result_array() as $row){
                    $result[]=$row;
                }
                return $result;
            }
         } catch (Exception $e) {
            throw new Exception();
            return;
        }    
    }
     
    public function getRFQListOneToOne($data ,$limit='', $start='') {
        try {
            $this->db->db_debug = true;
            $this->db->select('rfq.rfq_id,rfq.buyer_id,rfq.cat_id,rfq.uom_id,rfq.quantity,rfq.message,rfq.other_requerment,rfq.qrname,rfq.rfqlocation,rfq.latitudes,rfq.longitude,cust.name,cust.email,cust.phone,cat.category_name_en,cat.category_name_fr,uom.name_en as uom_name_en,uom.name_fr as uom_name_fr,rfq.created_date,buyer.business_logo, th.id as th_id, th.to_customer_id, tocust.name as tocust_name,tbto.business_logo as tocust_profile_image,th.from_customer_id,fromcust.name as fromcust_name,  tbfrom.business_logo as fromcust_profile_image,th.date,th.message as thread_message,COALESCE(th.is_seen,th.is_seen,0) as is_seen,th.images,sellerpt.business_name as to_business_name,sellerpf.business_name as from_business_name');
            $this->db->from($this->rfqTable." rfq");
            $this->db->join($this->customerTable." cust","cust.id=rfq.buyer_id");
            $this->db->join($this->BuyerProfileTable." buyer","cust.id=buyer.cust_id","left");
             $this->db->join($this->catTable." cat","cat.id=rfq.cat_id");
              $this->db->join($this->uomTable." uom","uom.id=rfq.uom_id");
              $this->db->join($this->rfqThreadTable . ' th', 'th.rfq_id = rfq.rfq_id');
              $this->db->join($this->customerTable . ' tocust', 'tocust.id = th.to_customer_id',"left");
                $this->db->join($this->BuyerProfileTable . ' tbto', 'tbto.cust_id = th.to_customer_id',"left");
               
    			
    			$this->db->join($this->customerTable . ' fromcust', 'fromcust.id = th.from_customer_id',"left");
                $this->db->join($this->BuyerProfileTable . ' tbfrom', 'tbfrom.cust_id = th.from_customer_id',"left");
                $this->db->join($this->SellerProfileTable . ' sellerpf', 'sellerpf.cust_id = th.from_customer_id',"left");
                $this->db->join($this->SellerProfileTable . ' sellerpt', 'sellerpt.cust_id = th.to_customer_id',"left");


             if(isset($data["cat_ids"]) && !empty($data["cat_ids"])){
                $this->db->where_in("rfq.cat_id",$data["cat_ids"]);
            }  

            if(isset($data["cat_ids_buyer"]) && !empty($data["cat_ids_buyer"]) ){
                $this->db->where("(rfq.cat_id IN (".$data["cat_ids_buyer"].")");
            }  
              if(isset($data["rfq_id"]) && trim($data["rfq_id"])!=''){
                $this->db->where("rfq.rfq_id",$data["rfq_id"]);
            }  
             if(isset($data["buyer_id"]) && trim($data["buyer_id"])!=''){
                $this->db->where("rfq.buyer_id",$data["buyer_id"]);
            } 
            if(isset($data["buyer_id_not"]) && trim($data["buyer_id_not"])!=''){
                $this->db->where("rfq.buyer_id !=".$data["buyer_id_not"]);
            } 
            if(isset($data["to_customer_id"]) && trim($data["to_customer_id"])!=''){
                $this->db->where("th.to_customer_id",$data["to_customer_id"]);
            }
            if(isset($data["from_customer_id"]) && trim($data["from_customer_id"])!=''){
                $this->db->where("th.from_customer_id",$data["from_customer_id"]);
            } 
             $this->db->where("rfq.status",1);
             if(isset($data["to_customer_ids"]) && $data["to_customer_ids"]!='' && isset($data["from_customer_ids"]) && $data["from_customer_ids"]!='' )
             { 
                 $this->db->where("( (th.to_customer_id= '".$data["to_customer_ids"]."' &&  th.from_customer_id= '".$data["from_customer_ids"]."' ) OR(th.to_customer_id= '".$data["from_customer_ids"]."' &&  th.from_customer_id= '".$data["to_customer_ids"]."' ) ) ");
                 
             }
             if( isset($data["from_customer"]) && $data["from_customer"]!='' )
             { 
                 $this->db->where("( (th.to_customer_id= '".$data["from_customer"]."' OR  th.from_customer_id= '".$data["from_customer"]."' )  ) ");
                 
             }
	    	
	    	if(!empty($data["order_by"])){
	    		foreach($data["order_by"] as $val){
	    			$this->db->order_by($val[0],$val[1]);
	    		}
	    	}
	    	 if(!empty($data["group_by"])){
	    		foreach($data["group_by"] as $val){
	    			$this->db->group_by($val);
	    		}
	    	}
	    	
             if(isset($limit) && trim($limit)!=''){
                $this->db->limit($limit,$start);
            }   


            $query = $this->db->get(); 

            $db_error = $this->db->error();

            if (!empty($db_error) && $db_error['code']>0) {
                throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
                return false; // unreachable retrun statement !!!
            }else{
               // echo $this->db->last_query();die();
                $result=[];
                foreach($query->result_array() as $row){
                    $result[]=$row;
                }
                return $result;
            }
         } catch (Exception $e) {
            throw new Exception();
            return;
        }    
    }

    public function getRfqUnreadSingle($data,$limit='', $start='')
    {
    	try{

    	$this->db->db_debug = false;
    	$this->db->select("rfq.id");

    	$this->db->from($this->rfqThreadTable . ' rfq');

		if(isset($data["total_message"]) && $data["total_message"] == 'ALL')
        { 
            $this->db->where("rfq.rfq_id",$data["rfq_id"]);
            if(isset($data["login_customer_id"]) && $data["login_customer_id"]!='')
            { 
                $this->db->where("(rfq.to_customer_id=".$data["login_customer_id"]." OR rfq.from_customer_id=".$data["login_customer_id"]." ) ");

            }

            if(isset($data["to_customer_id"]) && $data["to_customer_id"]!='' && isset($data["from_customer_id"]) && $data["from_customer_id"]!='' )
            { 
                $this->db->where("( (rfq.to_customer_id= '".$data["to_customer_id"]."' &&  rfq.from_customer_id= '".$data["from_customer_id"]."' ) OR(rfq.to_customer_id= '".$data["from_customer_id"]."' &&  rfq.from_customer_id= '".$data["to_customer_id"]."' ) ) ");
                
            }
        }else{
            $this->db->where("rfq.rfq_id",$data["rfq_id"]);
            $this->db->where("rfq.is_seen",0); 

            if(isset($data["rfq_id"]) && $data["rfq_id"]!='')
            { 
                $this->db->where("rfq.rfq_id",$data["rfq_id"]);
            } 

            if(isset($data["login_customer_id"]) && $data["login_customer_id"]!='')
            { 
                $this->db->where("rfq.to_customer_id = ",$data["login_customer_id"]);
            }

            if(isset($data["to_customer_id"]) && $data["to_customer_id"]!='' && isset($data["from_customer_id"]) && $data["from_customer_id"]!='' )
            { 
                $this->db->where("( (rfq.to_customer_id= '".$data["to_customer_id"]."' &&  rfq.from_customer_id= '".$data["from_customer_id"]."' ) OR(rfq.to_customer_id= '".$data["from_customer_id"]."' &&  rfq.from_customer_id= '".$data["to_customer_id"]."' ) ) ");
                
            }
        } 

    	    	
    	

    	$query=$this->db->get();
    	//echo $this->db->last_query();die();
    	$db_error = $this->db->error();

        if (!empty($db_error) && $db_error['code']>0) 
        {
            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
        }else{
            
        	$result=[];
        	foreach($query->result_array() as $row)
        	{
        		$result[]=$row;
        	}
        	
        	return count($result);
        }


    	}catch (Exception $e) {
    		echo $this->db->last_query();die();
	       throw new Exception();
	    }
    }
    public function getEnquiryUnreadSingle($data,$limit='', $start='')
    {
    	try{

    	$this->db->db_debug = false;
    	$this->db->select("enq.id");

    	$this->db->from($this->enquiryThreadTable . ' enq');

        if(isset($data["total_message"]) && $data["total_message"] == 'ALL')
        {
            if(isset($data["enq_id"]) && $data["enq_id"]!='')
            { 
                $this->db->where("enq.enquiry_id",$data["enq_id"]);
            }

            if(isset($data["login_customer_id"]) && $data["login_customer_id"]!='')
            { 
                $this->db->where("(enq.to_customer_id=".$data["login_customer_id"]." OR enq.from_customer_id=".$data["login_customer_id"]." ) ");
            }
            if(isset($data["to_customer_id"]) && $data["to_customer_id"]!='' && isset($data["from_customer_id"]) && $data["from_customer_id"]!='' )
            { 
                $this->db->where("( (enq.to_customer_id= '".$data["to_customer_id"]."' &&  enq.from_customer_id= '".$data["from_customer_id"]."' ) OR(enq.to_customer_id= '".$data["from_customer_id"]."' &&  enq.from_customer_id= '".$data["to_customer_id"]."' ) ) ");
                
            }

        }else{
            if(isset($data["enq_id"]) && $data["enq_id"]!='')
            { 
                $this->db->where("enq.enquiry_id",$data["enq_id"]);
            }

            $this->db->where("enq.is_seen",0); 

            if(isset($data["login_customer_id"]) && $data["login_customer_id"]!='')
            { 
                $this->db->where("enq.to_customer_id = ",$data["login_customer_id"]);
            }
            if(isset($data["to_customer_id"]) && $data["to_customer_id"]!='' && isset($data["from_customer_id"]) && $data["from_customer_id"]!='' )
            { 
                $this->db->where("( (enq.to_customer_id= '".$data["to_customer_id"]."' &&  enq.from_customer_id= '".$data["from_customer_id"]."' ) OR(enq.to_customer_id= '".$data["from_customer_id"]."' &&  enq.from_customer_id= '".$data["to_customer_id"]."' ) ) ");
                
            }
        }
		
    	

    	$query=$this->db->get();
    	//echo $this->db->last_query();die();
    	$db_error = $this->db->error();

        if (!empty($db_error) && $db_error['code']>0) 
        {
            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
        }else{
            
        	$result=[];
        	foreach($query->result_array() as $row)
        	{
        		$result[]=$row;
        	}
        	
        	return count($result);
        }


    	}catch (Exception $e) {
    		//echo $this->db->last_query();die();
	       throw new Exception();
	    }
    }

    
    public function getEnquiryDetails($data,$limit='', $start='')
    {
    	try{

    	$this->db->db_debug = false;
    	$this->db->select("th.id as th_id, th.enquiry_id as th_enquiry_id, th.to_customer_id as th_to_customer_id, th.from_customer_id as th_from_customer_id, th.date as th_date, th.message as th_message, th.is_seen as th_is_seen, tocust.name as tocust_name, tbto.business_logo as tocust_profile_image, fromcust.name as fromcust_name, tbfrom.business_logo as ,th.images,tbfrom.business_logo as fromcust_profile_image,sellerpt.business_name as to_business_name,sellerpf.business_name as from_business_name");

    	$this->db->from($this->enquiryThreadTable . ' th');
    	$this->db->join($this->customerTable . ' tocust', 'tocust.id = th.to_customer_id',"left");
    	$this->db->join($this->BuyerProfileTable . ' tbto', 'tbto.cust_id = th.to_customer_id',"left");
    	$this->db->join($this->customerTable . ' fromcust', 'fromcust.id = th.from_customer_id',"left");
        $this->db->join($this->BuyerProfileTable . ' tbfrom', 'tbfrom.cust_id = th.from_customer_id',"left");
        $this->db->join($this->SellerProfileTable . ' sellerpf', 'sellerpf.cust_id = th.from_customer_id',"left");
                $this->db->join($this->SellerProfileTable . ' sellerpt', 'sellerpt.cust_id = th.to_customer_id',"left");

    	if(isset($data["enq_id"]) && $data["enq_id"]!='')
    	{ 
    		$this->db->where("th.enquiry_id",$data["enq_id"]);
    	}
    	
    	if(!empty($data["order_by"]))
    	{
    		foreach($data["order_by"] as $val)
    		{
    			$this->db->order_by($val[0],$val[1]);
    		}
    	}

    	$query=$this->db->get();
        $db_error = $this->db->error();

        if (!empty($db_error) && $db_error['code']>0) 
        {
            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
        }else{
            //echo $this->db->last_query();die();
        	$result=[];
        	foreach($query->result_array() as $row)
        	{
        		$result[]=$row;

                if(isset($data["enq_id"]) && $data["enq_id"]!=''){ 
                        if(isset($data["login_customer_id"]) && $data["login_customer_id"]!='')
                        {
                            if($row['th_to_customer_id'] == $data["login_customer_id"])
                            {
                                $data2=[];
                                $data2["is_seen"]=1;
                                $res = $this->db->update($this->enquiryThreadTable, $data2, ['id' => $row['th_id']]);
                            }
                        }
                        
                    }
        	}
        	return $result;
        }


    	}catch (Exception $e) {
    	//	echo $this->db->last_query();die();
	       throw new Exception();
	    }
    }
    public function insertEnquiryThread($data){
    	try{
    		$this->db->db_debug = false;

    		$insert = $this->db->insert($this->enquiryThreadTable, $data);

    		$db_error = $this->db->error();
    		if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	if($insert) 
	        	{
	        		return  $this->db->insert_id();
	        	}else{
	        		return false;
	        	}
	        }
    	}
    	catch(Exception $e) {
            echo $this->db->last_query();die();
    		throw new Exception("Error Processing Request", 1);    		
    	}
    }
    public function insertRfqThread($data){
        try{
          $this->db->db_debug = false;
  
          $insert = $this->db->insert($this->rfqThreadTable, $data);
  
          $db_error = $this->db->error();
          if (!empty($db_error) && $db_error['code']>0) {
                throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
                return false; // unreachable retrun statement !!!
            }else{
              if($insert) 
              {
                return  $this->db->insert_id();
              }else{
                return false;
              }
  
            }
        }
        catch(Exception $e) {
          throw new Exception("Error Processing Request", 1);       
        }
      }
      public function getEnquirySingle($data,$limit='', $start='')
      {
          try{
  
          $this->db->db_debug = false;
          $this->db->select("th.id as th_id, th.enquiry_id as th_enquiry_id, th.to_customer_id as th_to_customer_id, th.from_customer_id as th_from_customer_id, th.date as th_date, th.message as th_message, th.is_seen as th_is_seen, tocust.name as tocust_name, tbto.business_logo as tocust_profile_image, fromcust.name as fromcust_name, tbfrom.business_logo as ,th.images,tbfrom.business_logo as fromcust_profile_image,sellerpt.business_name as to_business_name,sellerpf.business_name as from_business_name");

          $this->db->from($this->enquiryThreadTable . ' th');
          $this->db->join($this->customerTable . ' tocust', 'tocust.id = th.to_customer_id',"left");
          $this->db->join($this->BuyerProfileTable . ' tbto', 'tbto.cust_id = th.to_customer_id',"left");
          $this->db->join($this->customerTable . ' fromcust', 'fromcust.id = th.from_customer_id',"left");
          $this->db->join($this->BuyerProfileTable . ' tbfrom', 'tbfrom.cust_id = th.from_customer_id',"left");
          $this->db->join($this->SellerProfileTable . ' sellerpf', 'sellerpf.cust_id = th.from_customer_id',"left");
                  $this->db->join($this->SellerProfileTable . ' sellerpt', 'sellerpt.cust_id = th.to_customer_id',"left");
          $this->db->where("tocust.active",1);
         
          if(isset($data["enq_id"]) && $data["enq_id"]!='')
          { 
              $this->db->where("th.enquiry_id",$data["enq_id"]);
          }    	
          
  
          $query=$this->db->get();
          $db_error = $this->db->error();
  
          if (!empty($db_error) && $db_error['code']>0) 
          {
              throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
                  return false; // unreachable retrun statement !!!
          }else{
              //echo $this->db->last_query();die();
              $result=[];
              foreach($query->result_array() as $row)
              {
                  $result[]=$row;
              }
  
              return $result;
          }
  
  
          }catch (Exception $e) {
          //	echo $this->db->last_query();die();
             throw new Exception();
          }
      }
  
}

?>