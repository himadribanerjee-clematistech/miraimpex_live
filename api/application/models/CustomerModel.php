<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class CustomerModel extends CI_Model {
	protected $customerTable = 't_customer';
	protected $sellerTable = 't_seller_profile';
	protected $buyerTable = 't_buyer_profile';
	protected $RateExchangeTable = 't_rate_exchange';
	protected $sellerImageTable = 't_seller_image';
	protected $salt='HA{t~xOC;07Dp,Q+$y02F??:}/?LMj_';
	public function __construct() {
        parent::__construct();
    }
    public function passwordGenerate($password){
    	return md5(crypt($password,$this->salt));

    }
	public function getRateExchangeList($data){
    	try {
    		$this->db->db_debug = false;

    		$this->db->select("base_currency,to_currency,rate,updated_date");
    		$this->db->from($this->RateExchangeTable );
    			$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query();die();
	        	$result=[];
	        	foreach($query->result_array() as $row){
	        		$result[]=$row;
	        	}
	        	return $result;
	        }
    	} catch (Exception $e) {
    		//echo $this->db->last_query();die();
	       throw new Exception();
	    }
    }
    public function getSellerimagesList($data){
    	try {
    		$this->db->db_debug = false;

    		$this->db->select(" id,image as seller_image,seller_id,type");
    		$this->db->from($this->sellerImageTable );
    		$this->db->where("active",1);
    		if(isset($data["id"]) && $data["id"]!=''){ 
	    		$this->db->where("id",$data["id"]);

			}
			if(isset($data["seller_ids"]) && !empty($data["seller_ids"])){
                $this->db->where_in("seller_id",$data["seller_ids"]);
            }  
	    	if(isset($data["seller_id"]) && $data["seller_id"]!=''){ 
	    		$this->db->where("seller_id",$data["seller_id"]);

	    	}
	    	
    		
	    	
 
 			$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query();die();
	        	$result=[];
	        	foreach($query->result_array() as $row){
	        		$result[]=$row;
	        	}
	        	return $result;
	        }
    	} catch (Exception $e) {
    		//echo $this->db->last_query();die();
	       throw new Exception();
	    }
    }
   
     public function doPasswordCheck($data){
    	
		 try {
		 	$this->db->db_debug = FALSE;
	        $this->db->select("id");
	    	$this->db->from($this->customerTable);
	    	if(isset($data["token_id"]) && isset($data["password"]) ){
	    		$this->db->where("token_id",$data["token_id"]);
	    		$this->db->where("password",md5(crypt($data["password"],$this->salt)));
	    	}
	    	$this->db->where("actve",1);
	    	$this->db->where("is_admin",1);
	    	$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query();
	        	return $query->result_array();
	        }
	        return TRUE;
	    } catch (Exception $e) {
	       throw new Exception();
	    }	
    }
  
  
   
    public function doLoginCheck($data){
    	
		try {

		 	$this->db->db_debug = FALSE;

	        $this->db->select("id,username,name,email,email_verify,phone,phone_verify,active");
	    	$this->db->from($this->customerTable);

	    	if(isset($data["user_name"]) && isset($data["password"]) ){
	    		$this->db->where("(username='".$data["user_name"]."' OR phone='".$data["user_name"]."' )");
	    		
	    		$this->db->where("password",md5(crypt($data["password"],$this->salt)));
	    		$this->db->where("active",1);

	    		$query=$this->db->get();
		        $db_error = $this->db->error();
		        // echo $this->db->last_query();die();
		       	if (!empty($db_error) && $db_error['code']>0) {
		            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
		           
		        }else{

		        	return $query->result_array();
		        }
	    	}else{
	    		 return [];
	    	}
	    	

	        
	    } catch (Exception $e) {
	       throw new Exception();
	    }	
    }
    public function doAuthenticateCheck($data){
    	try {

		 	$this->db->db_debug = FALSE;

	        $this->db->select("id,name,email,email_verify,phone,phone_verify,active,location_key,location_city,supplier_YN,sellerCategory,created_date");
	    	$this->db->from($this->customerTable);

	    	if(isset($data["token_id"]) && isset($data["token_id"]) ){
	    		
	    		$this->db->where("token_id",$data["token_id"]);
	    		$this->db->where("active",1);

	    		$query=$this->db->get();
		        $db_error = $this->db->error();
		        // echo $this->db->last_query();die();
		       	if (!empty($db_error) && $db_error['code']>0) {
		            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
		           
		        }else{

		        	return $query->result_array();
		        }
	    	}else{
	    		 return [];
	    	}
	    	

	        
	    } catch (Exception $e) {
	       throw new Exception();
	    }	

    }
    public function doCustNameCheck($name,$id=0){

		try {

		 	$this->db->db_debug = FALSE;

	        $this->db->select("id,username,name,emailemail_verify,phone,phone_verify,active");
	    	$this->db->from($this->customerTable);

	    	if(isset($name) && isset($name) ){
	    		$this->db->where("name",$name);
	    		if($id>0){
	    			$this->db->where("id!=",$id);
	    		}
	    		$query=$this->db->get();
		        $db_error = $this->db->error();
		         echo $this->db->last_query();die();
		       	if (!empty($db_error) && $db_error['code']>0) {
		            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
		           
		        }else{

		        	return $query->result_array();
		        }
	    	}else{
	    		 return [];
	    	}
	    	

	        
	    } catch (Exception $e) {
	       throw new Exception();
	    }	

    }
    public function doCustEmailCheck($email,$id=0){

		try {

		 	$this->db->db_debug = FALSE;

	        $this->db->select("id,username,name,emailemail_verify,phone,phone_verify,active");
	    	$this->db->from($this->customerTable);

	    	if(isset($email) && isset($email) ){
	    		$this->db->where("email",$email);
	    		if($id>0){
	    			$this->db->where("id!=",$id);
	    		}
	    		$query=$this->db->get();
		        $db_error = $this->db->error();
		         //echo $this->db->last_query();die();
		       	if (!empty($db_error) && $db_error['code']>0) {
		            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
		           
		        }else{

		        	return $query->result_array();
		        }
	    	}else{
	    		 return [];
	    	}
	    	

	        
	    } catch (Exception $e) {
	       throw new Exception();
	    }	

    }
    public function doCustPhoneCheck($phone,$id=0){

		try {

		 	$this->db->db_debug = FALSE;

	        $this->db->select("id,username,name,email,email_verify,phone,phone_verify,active");
	    	$this->db->from($this->customerTable);

	    	if(isset($phone) && isset($phone) ){
	    		$this->db->where("phone",$phone);
	    		if($id>0){
	    			$this->db->where("id!=",$id);
	    		}
	    		$query=$this->db->get();
		        $db_error = $this->db->error();
		         //echo $this->db->last_query();die();
		       	if (!empty($db_error) && $db_error['code']>0) {
		            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
		           
		        }else{

		        	return $query->result_array();
		        }
	    	}else{
	    		 return [];
	    	}
	    	

	        
	    } catch (Exception $e) {
	    	//echo $this->db->last_query();die();
	       throw new Exception();
	    }	

    }
   public function updateCustomer($data,$where){
    	
    	try {
    	 	$this->db->db_debug = False;
    	 	$this->db->trans_start(FALSE);
	        $this->db->where($where);
	    	$this->db->update($this->customerTable, $data);
	        $this->db->trans_complete();
	        $db_error = $this->db->error();
	            if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }
	        return TRUE;
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }
    }
    public function getUserDetail($id){
    	 try {
		 	$this->db->db_debug = false;
	         $this->db->select("u.username,u.id as user_id,u.email_verify,u.email,u.phone,u.Name,u.active");
	    	$this->db->from($this->customerTable." u");
	    	
	    	$this->db->where("u.id",$id);
	    	$this->db->where("u.active",1);
	    	
	    	$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	$data=[];
	        	foreach($query->result_array() as $row){
	        		 $data[]=$row;
	    	
	        	}
	        	//echo $this->db->last_query();
	        	return $data;
	        }
	        return TRUE;
	    } catch (Exception $e) {
	    	echo $this->db->last_query();die();
	       throw new Exception();
	    }	
    }
     public function getUserDetailsbyId($id){
    	 try {
		 	$this->db->db_debug = false;
	        $this->db->select("u.user_name,u.id as user_id,u.email,u.mobile,u.Name,u.actve");
	    	$this->db->from($this->customerTable." u");
	    	
	    	$this->db->where("u.token_id",$id);
	    	$this->db->where("u.actve",1);
	    	$this->db->where("u.is_admin",1);
	    	$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	$data=[];
	        	foreach($query->result_array() as $row){
	        		 $data[]=$row;
	    	
	        	}
	        	//echo $this->db->last_query();
	        	return $data;
	        }
	        return TRUE;
	    } catch (Exception $e) {
	       throw new Exception();
	    }	
    }
     public function getUserDetailsbyUserId($id){
    	 try {
		 	$this->db->db_debug = false;
	        $this->db->select("u.user_name,u.id as user_id,u.email,u.mobile,u.Name,u.actve");
	    	$this->db->from($this->customerTable." u");
	    	
	    	$this->db->where("u.id",$id);
	    	$this->db->where("u.actve",1);
	    	$this->db->where("u.is_admin",1);
	    	$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	$data=[];
	        	foreach($query->result_array() as $row){
	        		 $data[]=$row;
	    	
	        	}
	        	//echo $this->db->last_query();
	        	return $data;
	        }
	        return TRUE;
	    } catch (Exception $e) {
	       throw new Exception();
	    }	
    }
    public function getUserDetailsbyEmailIdOTP($otp,$email){
    	try {
		 	$this->db->db_debug = false;
	        $this->db->select("u.user_name,u.id as user_id,u.email,u.mobile,u.Name,u.actve");
	    	$this->db->from($this->customerTable." u");
	    	
	    	$this->db->where("u.email_otp",$otp);
	    	$this->db->where("u.email",$email);
	    	$this->db->where("u.actve",1);
	    	$this->db->where("u.is_admin",1);
	    	$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	$data=[];
	        	foreach($query->result_array() as $row){
	        		 $data[]=$row;
	    	
	        	}
	        	//echo $this->db->last_query();
	        	return $data;
	        }
	        return TRUE;
	    } catch (Exception $e) {
	       throw new Exception();
	    }	

    }
     public function getUserDetailsbyEmailId($email){
    	 try {
		 	$this->db->db_debug = false;
	        $this->db->select("u.user_name,u.id as user_id,u.email,u.mobile,u.Name,u.actve");
	    	$this->db->from($this->customerTable." u");
	    	
	    	$this->db->where("u.email",$email);
	    	$this->db->where("u.actve",1);
	    	$this->db->where("u.is_admin",1);
	    	$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	$data=[];
	        	foreach($query->result_array() as $row){
	        		 $data[]=$row;
	    	
	        	}
	        	//echo $this->db->last_query();
	        	return $data;
	        }
	        return TRUE;
	    } catch (Exception $e) {
	       throw new Exception();
	    }	
    }
    public function getUserCount($data){
    	try {
    		$this->db->db_debug = false;
    		$this->db->select("u.id");
    		$this->db->from($this->customerTable." u");
    		if(isset($data["user_name"]) && $data["user_name"]!=''){
	    		$this->db->like("u.user_name",$data["user_name"]);
	    	}
	    	if(isset($data["Name"]) && $data["Name"]!=''){
	    		$this->db->like("u.Name",$data["Name"]);
	    	}
	    	if(isset($data["actve"]) && $data["actve"]!=''){
	    		$this->db->where("u.actve",$data["actve"]);
	    	}
	    	$db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query();die();
	        	return $this->db->count_all_results();

	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
    }
    public function getUserList($data,$limit='', $start=''){
    	try {
    		$this->db->db_debug = false;
    		$this->db->select("u.id,u.username,u.email,u.email_otp,u.name,u.active,u.phone_otp,u.phone,u.phone_verify");
    		$this->db->from($this->customerTable." u");
    		if(isset($data["username"]) && $data["username"]!=''){
	    		$this->db->like("u.username",$data["username"]);
	    	}
	    	if(isset($data["name"]) && $data["name"]!=''){
	    		$this->db->like("u.name",$data["name"]);
	    	}
	    	if(isset($data["active"]) && $data["active"]!=''){
	    		$this->db->where("u.active",$data["active"]);
	    	}
	    	if(isset($data["phone_otp"]) && $data["phone_otp"]!=''){
	    		$this->db->where("u.phone_otp",$data["phone_otp"]);
	    	}
	    	if(isset($data["change_password_otp"]) && $data["change_password_otp"]!=''){
	    		$this->db->where("u.change_password_otp",$data["change_password_otp"]);
	    	}
	    	if(isset($data["change_email_otp"]) && $data["change_email_otp"]!=''){
	    		$this->db->where("u.change_email_otp",$data["change_email_otp"]);
	    	}
	    	if(isset($data["email_otp"]) && $data["email_otp"]!=''){
	    		$this->db->where("u.email_otp",$data["email_otp"]);
	    	}
	    	if(isset($data["password"]) && $data["password"]!=''){
	    		$this->db->where("u.password",$data["password"]);
	    	}
	    	if(isset($data["id"]) && $data["id"]!=''){
	    		$this->db->where("u.id",$data["id"]);
	    	}
	    	if(isset($data["phone"]) && $data["phone"]!=''){
	    		$this->db->where("u.phone",$data["phone"]);
	    	}
	    	$this->db->order_by('u.username', 'DESC'); 
	    	if($limit!=""){
	    		$this->db->limit($limit, $start);
	    	}
	    	

 			$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query();die();
	        	$result=[];
	        	foreach($query->result_array() as $row){
	        		$result[]=$row;
	        	}
	        	return $result;
	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }
    }	
    public function doEmailCheck($data,$id=''){
    	
		 try {
		 	$this->db->db_debug = FALSE;
	        $this->db->select("id");
	    	$this->db->from($this->customerTable);
	    	if(isset($data["email"]) && trim($data["email"])!='' ){
	    		$this->db->where("email",$data["email"]);
	    		
	    	}

	    	if($id!=''){
	    		$this->db->where("id !=",$id);
	    	}
	    	$this->db->where("active",1);
	    	$query=$this->db->get();

	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	return $query->result_array();
	        }
	        return TRUE;
	    } catch (Exception $e) {
	    	//echo $this->db->last_query();die();
	       throw new Exception();
	    }	
    }
     public function doMobileCheck($data,$id=''){
    	
		 try {
		 	$this->db->db_debug = FALSE;
	        $this->db->select("id");
	    	$this->db->from($this->customerTable);
	    	if(isset($data["mobile"]) && trim($data["mobile"])!='' ){
	    		$this->db->where("mobile",$data["mobile"]);
	    		
	    	}

	    	if($id!=''){
	    		$this->db->where("id !=",$id);
	    	}
	    	$this->db->where("active",1);
	    	$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	return $query->result_array();
	        }
	        return TRUE;
	    } catch (Exception $e) {
	       throw new Exception();
	    }	
    }
     public function doUserNameCheck($data,$id=''){
    	
		 try {
		 	$this->db->db_debug = FALSE;
	        $this->db->select("id");
	    	$this->db->from($this->customerTable);
	    	if(isset($data["user_name"]) &&trim($data["user_name"])!='' ){
	    		$this->db->where("user_name",$data["user_name"]);
	    		
	    	}
	    	if($id!=''){
	    		$this->db->where("id !=",$id);
	    	}
	    	$this->db->where("actve",1);
	    	$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query();
	        	return $query->result_array();
	        }
	        return TRUE;
	    } catch (Exception $e) {
	       throw new Exception();
	    }	
    }
     public function insertUser($data){

    	try {
    	 	$this->db->db_debug = FALSE;
    	 	 $this->db->insert($this->customerTable, $data);
	        
	        $db_error = $this->db->error();
	       if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }
	        return  $this->db->insert_id();;
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }
    	
    }
     public function doDeleteUser($data){

    	try {
    	 	$this->db->db_debug = FALSE;
    	 	 $this->db->where($data);
			$this->db->delete("t_user");

	        $db_error = $this->db->error();
	       if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }
	        return  true;
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }
    	
    }
   	public function generateToken(){
    	return md5(crypt(rand().time(),$this->salt));
	}
	
	public function getBuyerInfo($id){
    	try {
    		$this->db->db_debug = false;

    		$this->db->select(" sell.id,sell.business_logo as profile_image,sell.address,sell.state,sell.city, sell.business_name, sell.country,cust.cust_language ");
    		$this->db->from($this->buyerTable . ' sell');
    		$this->db->join('t_customer' . ' cust', 'cust.id = sell.cust_id');

    		$this->db->where("cust.active",1);
    		$this->db->where("sell.active",1);
    		if(isset($id) && $id!=''){ 
	    		$this->db->where("sell.cust_id",$id);

	    	}

			$query=$this->db->get();
	        $db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query();die();
	        	$result=[];
	        	foreach($query->result_array() as $row){
	        		$result[]=$row;
	        	}
	        	return $result;
	        }
    	} catch (Exception $e) {
    		//echo $this->db->last_query();die();
	       throw new Exception();
	    }
	}
	
	public function getSellerInfo($id,$active=1){
    	try {
    		$this->db->db_debug = false;

    		$this->db->select("sell.id,cust_id,business_name,address,country,state,city,lat,long,contact_name,contact_number,cust.store_currency  ");
    		$this->db->from($this->sellerTable . ' sell');
    		$this->db->join('t_customer' . ' cust', 'cust.id = sell.cust_id');

			$this->db->where("cust.active",1);
			$this->db->where("cust.supplier_YN != '0'");
    		$this->db->where("sell.active",1);
    		if(isset($id) && $id!=''){ 
	    		$this->db->where("sell.cust_id",$id);

	    	}   		
	    	
 
 			$query=$this->db->get();

			$db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query();die();
	        	$result=[];
	        	foreach($query->result_array() as $row){
	        		$result[]=$row;
	        	}
	        	return $result;
	        }
    	} catch (Exception $e) {
    		//echo $this->db->last_query();die();
	       throw new Exception();
	    }
    }

     public function insertseller($data){

    	try {
    	 	$this->db->db_debug = FALSE;
    	 	 $this->db->insert($this->sellerTable, $data);
	        
	        $db_error = $this->db->error();
	       if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }
	        return  $this->db->insert_id();;
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }
    	
    }

      public function updateseller($data,$where){
    	
    	try {
    	 	$this->db->db_debug = False;
    	 	$this->db->trans_start(FALSE);
	        $this->db->where($where);
	    	$this->db->update($this->sellerTable, $data);
	        $this->db->trans_complete();
	        $db_error = $this->db->error();
	            if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }
	        return TRUE;
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }
    }
    
    public function insertbuyer($data){

    	try {
    	 	$this->db->db_debug = FALSE;
    	 	 $this->db->insert($this->buyerTable, $data);
	        
	        $db_error = $this->db->error();
	       if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }
	        return  $this->db->insert_id();;
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }
    	
    }

      public function updatebuyer($data,$where){
    	
    	try {
    	 	$this->db->db_debug = False;
    	 	$this->db->trans_start(FALSE);
	        $this->db->where($where);
	    	$this->db->update($this->buyerTable, $data);
	        $this->db->trans_complete();
	        $db_error = $this->db->error();
	            if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }
	        return TRUE;
	    } catch (Exception $e) {
	    	throw new Exception();
	        return;
	    }
    }

     public function insertSellerImg($data){
    	try{
    		$this->db->db_debug = false;

    		$insert = $this->db->insert($this->sellerImageTable, $data);

    		$db_error = $this->db->error();
    		if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	return $insert?true:false;
	        }
    	}
    	catch(Exception $e) {
    		throw new Exception("Error Processing Request", 1);    		
    	}
    }
       public function deleteSellerImage($id) {
    	try {
    	 	$this->db->db_debug = true;
    	 	$this->db->where('id', $id);
			$this->db->delete($this->sellerImageTable);
    	 	$db_error = $this->db->error();
	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }
	        else {
	        	//echo $this->db->last_query() . '<br>';
	        	return true;
	        }
	        //return  $this->db->insert_id();
	    } catch (Exception $e) {

	    	throw new Exception();
	        return;
	    }
    }
}

?>