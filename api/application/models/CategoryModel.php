<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class CategoryModel extends CI_Model {
	protected $catTable = 'category';
	protected $attributeTable = 'attribute';
	protected $varianceTable = 'variance';
	 protected $uomTable='uom';
	public function __construct() {
        parent::__construct();
	}
	public function getvariance($data){
        try {
                $this->db->db_debug = true;
             
            if(isset($data["lang"]) && $data["lang"]=='en')
            {
                $this->db->select('id, name_en as name,type');
            }elseif(isset($data["lang"]) && $data["lang"]=='fr')
            {
               $this->db->select('id, name_fr as name,type');
            }else{
               $this->db->select('id, name_en as name,type');
            }
              if(isset($data["id"]) && trim($data["id"])!=''){
                $this->db->where("id",$data["id"]);
                
			}
			if(isset($data["type"]) && trim($data["type"])!=''){
                $this->db->where("type",$data["type"]);
                
            }
            $this->db->where("status",1);
            $this->db->from($this->varianceTable);
            $query = $this->db->get();

            $db_error = $this->db->error();

            if (!empty($db_error) && $db_error['code']>0) {
                throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
                return false; // unreachable retrun statement !!!
            }else{
                //echo $this->db->last_query();die();
                $result=[];
                foreach($query->result_array() as $row){
                    $result[]=$row;
                }
                return $result;
            }
         } catch (Exception $e) {
           throw new Exception();
        }   
    }
	public function getattribute($data){
        try {
                $this->db->db_debug = true;
             
            if(isset($data["lang"]) && $data["lang"]=='en')
            {
                $this->db->select('id, name_en as name,type');
            }elseif(isset($data["lang"]) && $data["lang"]=='fr')
            {
               $this->db->select('id, name_fr as name,type');
            }else{
               $this->db->select('id, name_en as name,type');
            }
              if(isset($data["id"]) && trim($data["id"])!=''){
                $this->db->where("id",$data["id"]);
                
			}
			if(isset($data["type"]) && trim($data["type"])!=''){
                $this->db->where("type",$data["type"]);
                
            }
            $this->db->where("status",1);
            $this->db->from($this->attributeTable);
            $query = $this->db->get();

            $db_error = $this->db->error();

            if (!empty($db_error) && $db_error['code']>0) {
                throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
                return false; // unreachable retrun statement !!!
            }else{
                //echo $this->db->last_query();die();
                $result=[];
                foreach($query->result_array() as $row){
                    $result[]=$row;
                }
                return $result;
            }
         } catch (Exception $e) {
           throw new Exception();
        }   
    }
     public function getuom($data){
        try {
                $this->db->db_debug = true;
             
            if(isset($data["lang"]) && $data["lang"]=='en')
            {
                $this->db->select('id, name_en as name,type');
            }elseif(isset($data["lang"]) && $data["lang"]=='fr')
            {
               $this->db->select('id, name_fr as name,type');
            }else{
               $this->db->select('id, name_en as name,type');
            }
              if(isset($data["id"]) && trim($data["id"])!=''){
                $this->db->where("id",$data["id"]);
                
            }
            $this->db->where("active",1);
            $this->db->from($this->uomTable);
            $query = $this->db->get();

            $db_error = $this->db->error();

            if (!empty($db_error) && $db_error['code']>0) {
                throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
                return false; // unreachable retrun statement !!!
            }else{
                //echo $this->db->last_query();die();
                $result=[];
                foreach($query->result_array() as $row){
                    $result[]=$row;
                }
                return $result;
            }
         } catch (Exception $e) {
           throw new Exception();
        }   
    }
    public function getCategory($data) {
    	try {
            
			$this->db->db_debug = true;
			if(isset($data["lang"]) && $data["lang"]=='en')
			{
				$this->db->select('id, category_name_en as category_name,image,parent_id,slug,type,active,metadata,link_json,displayLocal');
			}elseif(isset($data["lang"]) && $data["lang"]=='fr')
			{
				$this->db->select('id, category_name_fr as category_name,image,parent_id,slug,type,active,metadata,link_json,displayLocal');
			}else{
				$this->db->select('id, category_name_en as category_name,image,parent_id,slug,type,active,metadata,link_json,displayLocal');
			}
    		
    		$this->db->from($this->catTable);

    		if(isset($data["parent_id"]) && trim($data["parent_id"])!='' && isset($data["level"]) && trim($data["level"])!=''){

    			$this->db->where("id >=".$data["parent_id"]);
    		}else if(isset($data["parent_id"]) && trim($data["parent_id"])!=''){
    			$this->db->where("parent_id",$data["parent_id"]);
    		}
    		if(isset($data["active"]) && trim($data["active"])!=''){
    			$this->db->where("active",$data["active"]);
    			
            }
            if(isset($data["displayLocal"]) && trim($data["displayLocal"]) > 0){
    			$this->db->where("displayLocal",$data["displayLocal"]);
    			
    		}
            if(isset($data["type"]) && trim($data["type"])!=''){
                $this->db->where("type",$data["type"]);
                
            }
              if(isset($data["id"]) && trim($data["id"])!=''){
                $this->db->where("id",$data["id"]);
                
            }

            if(isset($data["keyword"]) && trim($data["keyword"])!=''){
                if(isset($data["lang"]) && $data["lang"]=='en')
			    {
                    $this->db->like('category_name_en', trim($data["keyword"]), 'both');  
                }else{
                    $this->db->like('category_name_fr', trim($data["keyword"]), 'both');  
                }
               
                
            }
    		 if(isset($data["limit"]) && trim($data["limit"])!=''){
    			$this->db->limit($data["limit"],0);
    		}	
    		$this->db->where("active",1);
    		$this->db->order_by('parent_id', 'ASC');

    		$this->db->order_by('category_name_en', 'ASC');
    		$this->db->order_by('category_name_fr', 'ASC');

    		$this->db->order_by('id', 'DESC');

    		$query = $this->db->get();

	        $db_error = $this->db->error();

	       	if (!empty($db_error) && $db_error['code']>0) {
	            throw new Exception('Database error! Error Code [' . $db_error['code'] . '] Error: ' . $db_error['message']);
	            return false; // unreachable retrun statement !!!
	        }else{
	        	//echo $this->db->last_query();die();
	        	$result=[];
	        	foreach($query->result_array() as $row){
	        		$result[]=$row;
	        	}
	        	return $result;
	        }
    	} catch (Exception $e) {
	       throw new Exception();
	    }

    }


   
    
   
   

    

    
}

?>