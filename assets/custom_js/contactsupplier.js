var varianceArr = [];

function getObjectIndex(arr, varianceName) {
    // Loop over array to find the matching element/object
    for (var i = 0; i < arr.length; i++) {
        var obj = arr[i];
        if (obj.varianceName === varianceName) {
            // When all key-value matches return the array index
            return i;
        }
    }
    // When no match found, return false
    return false;
}

function getvariance(obj) {
    var varianceName = $(obj).attr('relparent');
    var varianceAttribute = $(obj).attr('relval');
    //alert(varianceName+'-'+varianceAttribute);

    var index = getObjectIndex(varianceArr, varianceName);

    // If object found in the array
    if (index !== false) {
        // Update the quantity in the same element
        varianceArr[index].varianceName = varianceName;
        varianceArr[index].varianceAttribute = varianceAttribute;
    } else {
        // Add the element in the array
        varianceArr.push({
            varianceName: varianceName,
            varianceAttribute: varianceAttribute
        });
    }

    $(obj).closest('ul').children().removeClass('varianceActive');
    $(obj).closest('ul li').addClass('varianceActive');

    var xt = JSON.stringify(varianceArr);

    $('#itemAttribute').val(xt);
    //console.log(xt);

}

var abc = 0;

$(document).ready(function () {
    if (localStorage.getItem("miraimpex_token") === null) {
        window.location.replace($("#base_url").val());
    }

    if (localStorage.getItem("miraimpex_isLoggedIn") == 'Y') {
        var total_info = JSON.parse(localStorage.getItem("miraimpex_total_info"));

        //if(total_info.email_verify == 1)
        //{
        $('#from_email').val(total_info.email).attr('disabled', true);
        //}

        $('#from_id').val(total_info.id);
    }

    // Following function will executes on change event of file input to select different file.
    $('body').on('change', '#enq-image-add', function () {
        if (this.files && this.files[0]) {
            abc += 1; // Incrementing global variable by 1.
            var z = abc - 1;
            $(this).find('#rfqpreviewimg' + z).remove();

            $('#rfqimgdv').append("<div style='display: inline-block; padding: 14px;'><div id='rfq_" + abc + "' class='abcd'><img id='rfqpreviewimg" + abc + "' src='' style='width: 139px; height: 89px;' /><div style='bottom: 0; width: 100%; height: 24px; line-height: 9px; background: #3b485a; opacity: .85;'><a class='remove' href='javascript:void(0);' onclick='return false;' style='display: inline-block; width: 100%; height: 24px; background: url(../assets/images/uploader-icons.png) no-repeat center 3px; text-align: center; background-position: center -37px;'><i class=\"fa fa-trash\" aria-hidden=\"true\" style=\"padding-top: 3px;font-size: 16px;\"></i></a></div></div></div>");

            var reader = new FileReader();
            reader.onload = rfqimageIsLoaded;
            reader.readAsDataURL(this.files[0]);
            $(this).hide();

            $("#rfq_" + abc).click(function () {
                $(this).parent().remove();
                var id = $(this).attr("id");
                var id_arr = id.split("_");
                var no = (parseInt(id_arr[1]) - 1);
                //console.log(abc,id_arr[1],no);
                if (id_arr.length == 2) {
                    $("#rfqfilediv_" + no).val("").trigger("change");
                    $("#rfqfilediv_" + no).remove();
                }

            });



            $("#rfqimgdv").after($("<div id='rfqfilediv_" + abc + "' class ='testcls' />").fadeIn('slow').prepend($("<input/>", {
                name: 'image[]',
                type: 'file',
                id: 'enq-image-add'
            }), $("")));

        }
    });
});

// To Preview Image
function rfqimageIsLoaded(e) {
    $('#rfqpreviewimg' + abc).attr('src', e.target.result);
};

function countChar(val) {
    var len = val.value.length;
    if (len >= 8000) {
        val.value = val.value.substring(0, 8000);
    } else {
        var text = 'You have ' + (8000 - len) + ' characters remaining.';
        $('#charNum').text(text);
    }
};


function sendQuotation() {
    //alert('send');
    var submit = true;
    toastr.clear();
    if ($.trim($('#message').val()) == '') {
        toastr.error('Please write a message!');
        submit = false;
        $('#message').focus();
        return false;
    }

    if (localStorage.getItem("miraimpex_customer_id") == $('#miraimpex_seller_customer_id').val()) {
        toastr.error('You can\'t send message to yourself.');
        submit = false;
        return false;
    }

    if ($('#check2').is(":checked")) {


    } else {
        toastr.error('Please accept conditions.');
        submit = false;
        return false;
    }

    if (submit) {

        var form_data = new FormData();
        // Read selected files
        var totalfiles = document.getElementById('enq-image-add').files.length;
        for (var index = 0; index < totalfiles; index++) {
            form_data.append("image[]", document.getElementById('enq-image-add').files[index]);
        }

        var other_data = $('#contactSupplierForm').serializeArray();
        $.each(other_data, function (key, input) {
            form_data.append(input.name, input.value);
        });
        //console.log(data);
        $.ajax({
            url: $("#base_url").val() + "api/enquiry/addEnquiry",
            data: form_data,
            contentType: false,
            processData: false,
            beforeSend: function (xhr) {
                /* Authorization header */
                xhr.setRequestHeader("Authorization", localStorage.getItem("miraimpex_token"));
                xhr.setRequestHeader("X-Mobile", "false");
            },
            method: "post",
            error: function (result) {
                //console.log("Unauthorised");
                toastr.clear();
                toastr.error(result.responseJSON.msg);
            },

            success: function (result) {
                if (result.status === 200) {
                    toastr.success('Message sent successfully');
                    setTimeout(
                        function () {
                            window.location.replace($("#base_url").val());

                        }, 500);
                } else {
                    console.log(result.status + ':/enquiry/addEnquiry');
                }

            }
        });

    }
}