$(document).ready(function () {
    getsupplierlists();
});

function getsupplierlists() {
    var base_url = $("#base_url").val();
    var values = {};
    values.lang = $("#viewLang").val();
    values.seller_id = $("#seller_id").val();

    $.ajax({
        url: base_url + "api/item/getSellerDetails",
        data: values,
        method: "post",
        success: function (result) {
            //console.log(result);
            if (result.status == 200) {
                if (result.item.length > 0) {
                    $.each(result.item, function (i1, seller) {
                        $('#companyDetails_SuppContName').html(seller.contact_name);
                        $('#companyDetails_SuppContNo').html(seller.contact_number);
                        $('#companyDetails_SuppName').html(seller.business_name);
                        $('#companyDetails_Address').html(seller.address);
                        $('#companyDetails_City').html(seller.city);
                        $('#companyDetails_State').html(seller.state);
                        $('#companyDetails_Country').html(seller.country);

                        if (seller.seller_image.length > 0) {
                            $.each(seller.seller_image, function (index, iv) {

                                var bigSchema = '<div class="item"><div><a href="#"><img src="' + base_url + 'admin/uploads/seller/' + iv.seller_image + '?text=' + index + '" alt="img-' + index + '"/></a></div></div>';

                                $("#supplierGalleryPanel").append(bigSchema);

                            });


                        } else {

                            var bigSchema = '<div class="item"><div><a href="#"><img src="' + base_url + 'assets/images/no-image.jpg" alt="img-"/></a></div></div>';

                            $("#supplierGalleryPanel").append(bigSchema);

                        }

                        loadGalleryPanel();

                        if (seller.seller_items.length > 0) {
                            $.each(seller.seller_items, function (sik, siv) {

                                var bigSchema = '<div class="item"><div><a href="' + base_url + 'product/details/' + siv.item_id + '"><img src="' + base_url + 'admin/uploads/selleritem/' + siv.thumbnail + '" alt="img-' + sik + '"/></a></div><div class="products_slider_info"><div><a href="' + base_url + 'product/details/' + siv.item_id + '">' + siv.item_name + '</a></div><div><a href="' + base_url + 'contactsupplier?seller=' + siv.seller_id + '&item=' + siv.item_id + '"><h5>Get Latest Price</h5></a></div><div><h4>' + siv.currency + ' ' + siv.price_min + ' - ' + siv.price_max + '</h4></div><div><p>' + siv.moq + ' ' + siv.moq_uom_name_en + ' <span>(MOQ)</span></p></div></div></div>';

                                $("#seller_other_items").append(bigSchema);

                            });

                            loadOtherItemGalleryPanel();
                        } else {
                            $('#seller_other_items_panel').remove();
                        }

                        if (seller.certificate.length > 0) {
                            //alert(seller.certificate.length);
                            $.each(seller.certificate, function (cik, civ) {

                                var bigSchema = '<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 image"><div class="img-wrapper"><a href="' + base_url + 'admin/uploads/seller/' + civ.seller_image + '"><img src="' + base_url + 'admin/uploads/seller/' + civ.seller_image + '" class="img-responsive"></a><div class="img-overlay"><i class="fa fa-plus-circle" aria-hidden="true"></i></div></div></div>';

                                $("#image-gallery").append(bigSchema);

                            });

                            //loadcertificategalpanel();

                        }

                    });
                } else {
                    // No item found
                    $('#supplierListsPanel').append('<div class=""><h2 style="text-align:center;">No seller yet.</h2></div>');
                }

            } else {
                // Something went wrong
            }


        },
        error: function (result) {
            console.log(result.responseJSON.msg);
        }
    });



}

function loadcertificategalpanel() {
    $(".img-wrapper").hover(
        function () {
            $(this).find(".img-overlay").animate({ opacity: 1 }, 200);
        }, function () {
            $(this).find(".img-overlay").animate({ opacity: 0 }, 200);
        }
    );

    // Lightbox
    var $overlay = $('<div id="overlay"></div>');
    var $image = $("<img>");
    var $prevButton = $('<div id="prevButton"><i class="fa fa-chevron-left"></i></div>');
    var $nextButton = $('<div id="nextButton"><i class="fa fa-chevron-right"></i></div>');
    var $exitButton = $('<div id="exitButton"><i class="fa fa-times"></i></div>');

    // When next button is clicked
    $nextButton.click(function (event) {
        // Hide the current image
        $("#overlay img").hide();
        // Overlay image location
        var $currentImgSrc = $("#overlay img").attr("src");
        // Image with matching location of the overlay image
        var $currentImg = $('#image-gallery img[src="' + $currentImgSrc + '"]');
        // Finds the next image
        var $nextImg = $($currentImg.closest(".image").next().find("img"));
        // All of the images in the gallery
        var $images = $("#image-gallery img");
        // If there is a next image
        if ($nextImg.length > 0) {
            // Fade in the next image
            $("#overlay img").attr("src", $nextImg.attr("src")).fadeIn(800);
        } else {
            // Otherwise fade in the first image
            $("#overlay img").attr("src", $($images[0]).attr("src")).fadeIn(800);
        }
        // Prevents overlay from being hidden
        event.stopPropagation();
    });

    // When previous button is clicked
    $prevButton.click(function (event) {
        // Hide the current image
        $("#overlay img").hide();
        // Overlay image location
        var $currentImgSrc = $("#overlay img").attr("src");
        // Image with matching location of the overlay image
        var $currentImg = $('#image-gallery img[src="' + $currentImgSrc + '"]');
        // Finds the next image
        var $nextImg = $($currentImg.closest(".image").prev().find("img"));
        // Fade in the next image
        $("#overlay img").attr("src", $nextImg.attr("src")).fadeIn(800);
        // Prevents overlay from being hidden
        event.stopPropagation();
    });

    // When the exit button is clicked
    $exitButton.click(function () {
        // Fade out the overlay
        $("#overlay").fadeOut("slow");
    });

    // Add overlay
    $overlay.append($image).prepend($prevButton).append($nextButton).append($exitButton);
    $("#gallery").append($overlay);

    // Hide overlay on default
    $overlay.hide();

    // When an image is clicked
    $(".img-overlay").click(function (event) {
        // Prevents default behavior
        event.preventDefault();
        // Adds href attribute to variable
        var imageLocation = $(this).prev().attr("href");
        // Add the image src to $image
        $image.attr("src", imageLocation);
        // Fade in the overlay
        $overlay.fadeIn("slow");
    });

    // When the overlay is clicked
    $overlay.click(function () {
        // Fade out the overlay
        $(this).fadeOut("slow");
    });

    $(".owl-carousel.supplier_certificate_sld").owlCarousel({
        items: 4,
        itemsDesktop: [1200, 3],
        itemsDesktopSmall: [1199, 3],
        itemsTablet: [950, 3],
        itemsTabletSmall: [767, 3],
        itemsMobile: [479, 2],
        slideSpeed: 800,
        autoPlay: 6000,
        stopOnHover: true,
        navigation: true,
        pagination: false,
        responsive: true,
        autoHeight: true,
        navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"]
    });
}

function loadGalleryPanel() {
    $(".owl-carousel.supplier_details_slg_sld").owlCarousel({
        items: 1,
        itemsDesktop: [1200, 3],
        itemsDesktopSmall: [1199, 3],
        itemsTablet: [950, 3],
        itemsTabletSmall: [767, 3],
        itemsMobile: [479, 2],
        slideSpeed: 800,
        autoPlay: 6000,
        stopOnHover: true,
        navigation: true,
        pagination: false,
        responsive: true,
        autoHeight: true,
        navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"]
    });
}

function loadOtherItemGalleryPanel() {
    $(".owl-carousel.product_details_sld").owlCarousel({
        items: 4,
        itemsDesktop: [1200, 3],
        itemsDesktopSmall: [1199, 3],
        itemsTablet: [950, 3],
        itemsTabletSmall: [767, 3],
        itemsMobile: [479, 2],
        slideSpeed: 600,
        autoPlay: 3000,
        stopOnHover: true,
        navigation: true,
        pagination: false,
        responsive: true,
        autoHeight: true,
        navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"]
    });
}