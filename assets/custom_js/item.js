function doitemEdit(id){
	var base_url=$("#base_url").val();
	window.location=base_url+"item/doitemEdit/"+id;
}
function dogetSubCatByCat(id){
	var base_url=$("#base_url").val();
	$.ajax({
			url: base_url+"subcategory/getSubCategoryListByCatId",
			data:"id="+id,
			method:"post",
			success: function(result){
			 	$('.loading').addClass("hidden");
			 	result=JSON.parse(result);
		 		if(result.status==0){
		 			var data=result.result;
		 			$("#subcategory").html("")
		 			 $("#subcategory") .append('<option value="">-- select Sub Category--</option>')
		 			if(data.length>0){
		 				$.each(result.result, function( index, value ) {
						  $("#subcategory") .append('<option value="'+value.id+'">'+value.category_name+'</option>')
						});
		 			}
		 			
		 		}else{
			 		toastr.error(result["message"]);						
			 	}
	    	
	  		}
	 	 });

}
function addItemDetails(){
	toastr.clear();
	var base_url=$("#base_url").val();
	var item_name=$("#item_name").val();
	var category=$("#category").val();
	var subcategory=$("#subcategory").val();
	var item_image=$("#item_image").val();
	var item_description=$("#item_description").val();
	var local_name=$("#local_name").val();
	if($.trim(item_name)=='' ){
		toastr.error('Please Enter Item Name.');
		$("#item_name").focus();
		return false;
	}else if($.trim(category)=='' ){
		toastr.error('Please Select Category.');
		$("#category").focus();
		return false;
	}else if($.trim(item_image)=='' ){
		toastr.error('Please Select Item Image.');
		$("#item_image").focus();
		return false;
	}else if($.trim(item_description)=='' ){
		toastr.error('Please Enter Item Description.');
		$("#item_description").focus();
		return false;
	}else{
		var formData = new FormData();
		formData.append('item_image', $('#item_image')[0].files[0]);
		formData.append('item_name', item_name);
		formData.append('category', category);
		formData.append('subcategory', subcategory);
		formData.append('item_description', item_description);
		formData.append('local_name', local_name);
		$('.loading').removeClass("hidden");
		$.ajax({
		       url :base_url+"item/doAddItem",
		       type : 'POST',
		       data : formData,
		       processData: false,  // tell jQuery not to process the data
		       contentType: false,  // tell jQuery not to set contentType
		       success : function(result) {
		           $('.loading').addClass("hidden");
			 	result=JSON.parse(result);
			 		if(result.status==0){
			 			toastr.success(result["message"]);
			 			setTimeout(
						  function() 
						  {
						    window.location=base_url+'item';
					 	
						  }, 1000);
			 		}else{
				 		toastr.error(result["message"]);
				 		if(result["field"]=='to_login'){
				 			window.location=base_url;
				 		}else{
				 			$("#"+result["field"]).focus();
				 		}
						
				 	}
		       }
		});
	}
}
function updateItemDetails(){
	toastr.clear();
	var base_url=$("#base_url").val();
	var item_name=$("#item_name").val();
	var category=$("#category").val();
	var subcategory=$("#subcategory").val();
	var item_image=$("#item_image").val();
	var img_old=$("#img_old").val();
	var local_name=$("#local_name").val();
	var id=$("#id").val();
	var item_description=$("#item_description").val();
	if($.trim(item_name)=='' ){
		toastr.error('Please Enter Item Name.');
		$("#item_name").focus();
		return false;
	}else if($.trim(category)=='' ){
		toastr.error('Please Select Category.');
		$("#category").focus();
		return false;
	}else if($.trim(item_image)=='' && $.trim(img_old)=='' ){
		toastr.error('Please Select Item Image.');
		$("#item_image").focus();
		return false;
	}else if($.trim(item_description)=='' ){
		toastr.error('Please Enter Item Description.');
		$("#item_description").focus();
		return false;
	}else{
		var formData = new FormData();
		formData.append('item_image', $('#item_image')[0].files[0]);
		formData.append('item_name', item_name);
		formData.append('category', category);
		formData.append('subcategory', subcategory);
		formData.append('item_description', item_description);
		formData.append('local_name', local_name);
		
		formData.append('id', id);
		$('.loading').removeClass("hidden");
		$.ajax({
		       url :base_url+"item/doUpdateItem",
		       type : 'POST',
		       data : formData,
		       processData: false,  // tell jQuery not to process the data
		       contentType: false,  // tell jQuery not to set contentType
		       success : function(result) {
		           $('.loading').addClass("hidden");
			 	result=JSON.parse(result);
			 		if(result.status==0){
			 			toastr.success(result["message"]);
			 			setTimeout(
						  function() 
						  {
						    window.location=base_url+'item';
					 	
						  }, 1000);
			 		}else{
				 		toastr.error(result["message"]);
				 		if(result["field"]=='to_login'){
				 			window.location=base_url;
				 		}else{
				 			$("#"+result["field"]).focus();
				 		}
						
				 	}
		       }
		});
	}
}
function doItemStatusChange(value,id){
	toastr.clear();
	var base_url=$("#base_url").val();
	if(confirm("Do you want to change status of this Item?")){
		$('.loading').removeClass("hidden");
		$.ajax({
			url: base_url+"item/doitemStatusChange",
			data:"id="+id+'&active='+value,
			method:"post",
			success: function(result){
			 	$('.loading').addClass("hidden");
			 	result=JSON.parse(result);
		 		if(result.status==0){
		 			toastr.success(result["message"]);
		 			/*setTimeout(
					  function() 
					  {
					    window.location=base_url+'item'+location.search;
				 	
					  }, 1000);*/
		 		}else{
			 		toastr.error(result["message"]);						
			 	}
	    	
	  		}
	 	 });
	}
}
function doitemDelete(id) {
	toastr.clear();
	var base_url=$("#base_url").val();
	if(confirm("Do you want to delete this Item?")){
		$('.loading').removeClass("hidden");
		$.ajax({
			url: base_url+"item/doitemDelete",
			data:"id="+id,
			method:"post",
			success: function(result){
			 	$('.loading').addClass("hidden");
			 	result=JSON.parse(result);
		 		if(result.status==0){
		 			toastr.success(result["message"]);
		 			setTimeout(
					  function() 
					  {
					    window.location=base_url+'item';
				 	
					  }, 1000);
		 		}else{
			 		toastr.error(result["message"]);						
			 	}
	    	
	  		}
	 	 });
	}
}