$(document).ready(function () {

    if (localStorage.getItem("miraimpex_token") === null) {
        window.location.replace($("#base_url").val());
    }
    if (localStorage.getItem("miraimpex_isLoggedIn") == 'Y') {
        var total_info = JSON.parse(localStorage.getItem("miraimpex_total_info"));

        if (total_info.email_verify == "0") {
            $('#buyerEmail').html(total_info.email);

        } else {
            $('#buyerEmail').html(total_info.email + ' ' + '<a href="javascript:void(0);">[Verified]</a>');
        }
    }

    $("#changeEmailOtp").keypress(function (e) {
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            //display error message
            toastr.clear();
            toastr.error('Digits only!');
            return false;
        }
    });

    $("#updateEmailForm").submit(function (e) {
        e.preventDefault();
        $('#changeEmailSubmit').prop("disabled", true);
        if (checkEmailForm()) {
            //alert('success');
            var data = {};
            data.email = $('#changeEmailNewEmail').val();
            data.otp = $('#changeEmailOtp').val();
            $.ajax({
                url: $("#base_url").val() + "api/login/doEmailUpdate",
                data: data,
                method: "post",
                beforeSend: function (xhr) {
                    /* Authorization header */
                    xhr.setRequestHeader("Authorization", localStorage.getItem("miraimpex_token"));
                    xhr.setRequestHeader("X-Mobile", "false");
                },
                error: function (result) {
                    toastr.clear();
                    toastr.error(result.responseJSON.message);
                    $('#changeEmailSubmit').prop("disabled", false);
                },
                success: function (result) {
                    //console.log(result);
                    if (result.status === 200) {
                        toastr.clear();
                        toastr.success(result.msg);
                        $('#changeEmailS1').addClass('active');
                        $('#changeEmailS2').addClass('active');
                        $('#changeEmailS2').addClass('active');
                        doAuthenticateMe();
                        setTimeout(
                            function () {

                                window.location.replace($("#base_url").val() + 'profile');

                            }, 1000);


                    } else {

                        $('#changeEmailSubmit').prop("disabled", false);
                        console.log(result.status + ':emailupdate');
                    }

                }
            });
        }
    });

});

function checkEmailForm() {

    var emailOTP = $('#changeEmailOtp').val();
    var email = $('#changeEmailNewEmail').val();
    if (emailOTP.length < 4) {
        $('#changeEmailSubmit').prop("disabled", false);
        toastr.clear();
        toastr.error('Invalid OTP!');
        $('#changeEmailOtp').focus();
        return false;
    }
    if (!isEmail(email)) {
        $('#changeEmailSubmit').prop("disabled", false);
        toastr.clear();
        toastr.error('Invalid Email!');
        $('#changeEmailNewEmail').focus();
        return false;
    }

    return true;
}

function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}


function sendOtpForEmailChange() {
    if (localStorage.getItem("miraimpex_token") === null) {
        toastr.clear();
        toastr.error('Something went wrong! Please login.');

    } else {
        $('#buttonOTP').prop("disabled", true);
        var data = {};

        $.ajax({
            url: $("#base_url").val() + "api/login/doSendOTPEmail",
            data: data,
            method: "post",
            beforeSend: function (xhr) {
                /* Authorization header */
                xhr.setRequestHeader("Authorization", localStorage.getItem("miraimpex_token"));
                xhr.setRequestHeader("X-Mobile", "false");
            },
            error: function (result) {
                toastr.clear();
                toastr.error(result.responseJSON.message);
                $('#buttonOTP').prop("disabled", false);
                $('#changeEmailS1').removeClass('active');
                $('#changeEmailS2').removeClass('active');
            },
            success: function (result) {
                //console.log(result);
                if (result.status === 200) {
                    toastr.clear();
                    toastr.success(result.msg);
                    $('#changeEmailS1').addClass('active');
                    $('#changeEmailS2').addClass('active');
                    $('#buttonOTP').prop("disabled", false);
                    $('.changeEmail').removeClass('d-none');


                } else {
                    $('#changeEmailS1').removeClass('active');
                    $('#changeEmailS2').removeClass('active');
                    $('#buttonOTP').prop("disabled", false);
                    console.log(result.status + ':doSendOTP');
                }

            }
        });
    }
}