var msgType = 'all';
var readStatus = '';
var myProfileImage = '';
var to_id = '';
var sellerProfileImage = '';
var sellerName = '';


$(document).ready(function () {
  $('#sendProposalDiv').hide();
  myProfileImage = $("#base_url").val() + 'assets/images/user.png';
  sellerProfileImage = $("#base_url").val() + 'assets/images/user.png';

  if (localStorage.getItem("miraimpex_token") === null) {
    window.location.replace($("#base_url").val());
  }

  if (localStorage.getItem("miraimpex_isLoggedIn") == 'Y') {
    var total_info = JSON.parse(localStorage.getItem("miraimpex_total_info"));
    //console.log(total_info);
    if (total_info.supplier_YN == 0) {
      $('#sendProposalDiv').remove();
      $('#send_proposal_model').remove();
    }

    if (total_info.buyer_info.profile_image) {
      var profileImg = total_info.buyer_info.profile_image;
      myProfileImage = $("#base_url").val() + 'admin/uploads/bussiness_logo/' + profileImg;
    }

    //chatboxLists();

    $.when(chatboxLists()).done(function (x) {
      setTimeout(function () {
        if ($.trim($('#sType').val()) != '' && $.trim($('#sId').val()) != '') {
          var sType = $.trim($('#sType').val());
          var sId = $.trim($('#sId').val());

          $("a[reltype$='" + sType + "'][relid$='" + sId + "']").trigger('click');
        } else {
          $("ul#messageList li:first a").trigger('click');
        }

      }, 1000);


    });

  } else {
    window.location.replace($("#base_url").val());
  }


  var addImgCount = 0;

  // Following function will executes on change event of file input to select different file.
  $('body').on('change', '#msgcenter-photo-add', function () {
    if (this.files && this.files[0]) {
      addImgCount += 1; // Incrementing global variable by 1.
      var z = addImgCount - 1;
      $(this).find('#msgcenterpreviewimg' + z).remove();

      $('#msgAddImgdv').append("<div style='display: inline-block; padding: 14px;'><div id='addImg_" + addImgCount + "' class='addImg'><img id='msgcenterpreviewimg" + addImgCount + "' src='' style='width: 139px; height: 89px;' /><div style='bottom: 0; width: 100%; height: 24px; line-height: 9px; background: #3b485a; opacity: .85;'><a class='remove' href='javascript:void(0);' onclick='return false;' style='display: inline-block; width: 100%; height: 24px; background: url(../assets/images/uploader-icons.png) no-repeat center 3px; text-align: center; background-position: center -37px;'><i class=\"fa fa-trash\" aria-hidden=\"true\" style=\"padding-top: 3px;font-size: 16px;\"></i></a></div></div></div>");

      var reader = new FileReader();
      reader.onload = msgimageIsLoaded;
      reader.readAsDataURL(this.files[0]);
      $(this).hide();

      $("#addImg_" + addImgCount).click(function () {
        $(this).parent().remove();
        var id = $(this).attr("id");
        var id_arr = id.split("_");
        var no = (parseInt(id_arr[1]) - 1);

        if (id_arr) {
          if (id_arr.length == 2) {
            $("#msgcenterfilediv_" + no).val("").trigger("change");
            $("#msgcenterfilediv_" + no).remove();
          }
        }


      });



      $("#msgAddImgdv").prepend($("<div id='msgcenterfilediv_" + addImgCount + "' class ='testcls' />").fadeIn('slow').prepend($("<input/>", {
        name: 'image[]',
        type: 'file',
        id: 'msgcenter-photo-add'
      }), $("")));

    }
  });

  // To Preview Image
  function msgimageIsLoaded(e) {
    $('#msgcenterpreviewimg' + addImgCount).attr('src', e.target.result);
  };




  $('#localSearch').on('keyup', function (event) {
    var childUlPath = 'ul#messageList>li';
    var messageText = $('#localSearch').val();
    //alert(messageText); 

    $(childUlPath).each(function () {
      var text = $(this).text().toLowerCase();
      //alert(text);
      (text.indexOf(messageText) == 0) ? $(this).removeClass('d-none') : $(this).addClass('d-none');
    });

  });

});

$(document).ready(function () {
  $('#btn-chat').click(function () {

    var imgSendCount = $('#msgcenter-photo-addd').get(0).files.length;
    var boxval = $.trim($('#chatcontent').val());
    if (boxval.length > 0 || imgSendCount > 0) {
      $('#sendingMessageIcon').removeClass('d-none');
      $('#noChat').remove();
      $('#btn-chat').prop("disabled", true);
      $('#chatcontent').val('').css("height", "40px");
      $('.loading_section').addClass('d-none');

      var data = {};
      data.rfq_id = $('#rfq_id').val();
      data.to_customer_id = $('#to_id').val();
      data.message = boxval;
      data.type = $('#msgType').val();

      var msg_img_data = $("#msgcenter-photo-addd").prop("files")[0];

      var form_data = new FormData();
      form_data.append("rfq_id", $('#rfq_id').val());
      form_data.append("to_customer_id", $('#to_id').val());
      form_data.append("message", boxval);
      form_data.append("type", $('#msgType').val());
      form_data.append("image", msg_img_data);

      if ($('#msgType').val() == 'enquiry') {
        data.type = 'enq';
      }


      $.ajax({
        url: $("#base_url").val() + "api/messagecenter/SendMessage",
        data: form_data,
        beforeSend: function (xhr) {
          /* Authorization header */
          xhr.setRequestHeader("Authorization", localStorage.getItem("miraimpex_token"));
          xhr.setRequestHeader("X-Mobile", "false");
        },
        method: "post",
        processData: false,
        contentType: false,
        error: function (result) {
          //console.log("Unauthorised");
          $('#btn-chat').prop("disabled", false);
          $('#sendingMessageIcon').addClass('d-none');
          toastr.clear();
          toastr.error(result.responseJSON.msg);
        },
        success: function (result) {
          //console.log(result);
          if (result.status === 200) {
            setTimeout(
              function () {
                var modifiedText = autolinkgenerate(boxval);

                if (imgSendCount > 0) {
                  modifiedText = '<img src="' + $("#base_url").val() + 'assets/images/icon/picture.png" alt="">';
                }

                var sampleHtml = '<div class="Message_box_sec myMsg"><div class="Message_box_sec_img"><img src="' + myProfileImage + '" alt=""></div><div class="Message_box_sec_info"><p class="Message_send_date"><span>You a moment ago.</span></p><div class="Message_send_cont"><p>' + modifiedText + '</p></div></div></div>';

                $('#chatBoxDetails').append(sampleHtml);
                $("#chatBoxDetails").animate({ scrollTop: $('#chatBoxDetails').prop("scrollHeight") }, 1000);

                $('#sendingMessageIcon').addClass('d-none');

              }, 500);

          } else {
            toastr.clear();
            toastr.error('Something went wrong.');
            $('#sendingMessageIcon').addClass('d-none');
          }

        }
      });



    } else {
      toastr.clear();
      toastr.error('Message is empty.');
      $('#chatcontent').val('').css("height", "40px");
      $('#chatcontent').focus();
    }

  });

  $('#chatcontent').keyup(function (e) {
    e.stopImmediatePropagation();
    if ((e.keyCode || e.which) == 13 && !e.shiftKey) {
      var chatText = $.trim($('#chatcontent').val());
      if (chatText != '' && chatText.length > 0) {
        $('#btn-chat').trigger("click");
      } else {
        toastr.clear();
        toastr.error('Message is empty.');
        $('#chatcontent').val('').css("height", "40px");
        $('#chatcontent').focus();
      }
    }
  });


  $("#report_incidentForm").submit(function (e) {
    e.preventDefault();
    toastr.clear();
    var submit = true;
    if ($.trim($('#report_incident_msg').val()) == '') {
      toastr.error('Please write about the incident!');
      submit = false;
      $('#report_incident_msg').focus();
      return false;
    }
    if ($.trim($('#report_incident_to_id').val()) == '') {
      toastr.error('Something went wrong!');
      submit = false;
      return false;
    }
    if ($.trim($('#report_incident_from_id').val()) == '') {
      toastr.error('Something went wrong!');
      submit = false;
      return false;
    }
    if ($.trim($('#report_incident_ref_id').val()) == '') {
      toastr.error('Something went wrong!');
      submit = false;
      return false;
    }
    if ($.trim($('#report_incident_ref_type').val()) == '') {
      toastr.error('Something went wrong!');
      submit = false;
      return false;
    }

    if (submit) {
      // disabled the submit button
      $("#report_incident_Submit").prop("disabled", true);
      var formData = new FormData(this);

      $.ajax({
        type: "POST",
        method: "post",
        enctype: 'multipart/form-data',
        url: $("#base_url").val() + "api/messagecenter/reportIncident",
        data: formData,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        beforeSend: function (xhr) {
          /* Authorization header */
          xhr.setRequestHeader("Authorization", localStorage.getItem("miraimpex_token"));
          xhr.setRequestHeader("X-Mobile", "false");
        },
        success: function (data) {
          console.log("SUCCESS : ", data);
          toastr.success(data.msg);
          $("#report_incident_Submit").prop("disabled", false);
          $('#report_incidentForm').trigger("reset");
          $('#msgAddImgdv').empty();
          document.getElementById('report_incident_model').style.display = 'none';

        },
        error: function (e) {
          toastr.clear();
          toastr.error(e.responseJSON.msg);
          $("#report_incident_Submit").prop("disabled", false);

        }
      });
    }

  });


  $("#send_proposalForm").submit(function (e) {
    e.preventDefault();
    toastr.clear();
    var submit = true;

    if ($.trim($('#send_proposal_to_id').val()) == '') {
      toastr.error('Something went wrong!');
      submit = false;
      return false;
    }
    if ($.trim($('#send_proposal_from_id').val()) == '') {
      toastr.error('Something went wrong!');
      submit = false;
      return false;
    }
    if ($.trim($('#send_proposal_ref_id').val()) == '') {
      toastr.error('Something went wrong!');
      submit = false;
      return false;
    }
    if ($.trim($('#send_proposal_ref_type').val()) == '') {
      toastr.error('Something went wrong!');
      submit = false;
      return false;
    }

    if ($.trim($('#send_proposal_title').val()) == '') {
      toastr.error('Proposal title is empty!');
      submit = false;
      return false;
    }

    if ($.trim($('#send_proposal_title').val()) == '') {
      toastr.error('Proposal title is empty!');
      submit = false;
      return false;
    }

    if ($.trim($('#send_proposal_amount').val()) < 0) {
      toastr.error('Proposal amount is empty!');
      submit = false;
      return false;
    }

    if ($.trim($('#send_proposal_desc').val()) == '') {
      toastr.error('Proposal description is empty!');
      submit = false;
      return false;
    }

    if ($('#sendproposal-photo-add').get(0).files.length === 0) {
      toastr.error('Please attach proposal documents!');
      submit = false;
      return false;
    }

    if (submit) {
      // disabled the submit button
      $("#send_proposal_Submit").prop("disabled", true);
      var formData = new FormData(this);

      $.ajax({
        type: "POST",
        method: "post",
        enctype: 'multipart/form-data',
        url: $("#base_url").val() + "api/messagecenter/addProposal",
        data: formData,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        beforeSend: function (xhr) {
          /* Authorization header */
          xhr.setRequestHeader("Authorization", localStorage.getItem("miraimpex_token"));
          xhr.setRequestHeader("X-Mobile", "false");
        },
        success: function (data) {
          //console.log("SUCCESS : ", data);
          toastr.success(data.msg);
          setTimeout(
            function () {
              window.location.reload();
            }, 1000);

        },
        error: function (e) {
          toastr.clear();
          toastr.error(e.responseJSON.msg);
          $("#send_proposal_Submit").prop("disabled", false);

        }
      });
    }

  });

});


function selectMsgType(type) {
  //alert(msgType);
  msgType = type;
  var res = msgType.toUpperCase();
  $('.displayMsgType1').html(type);
  $('#topDisp').html(res);
  $('.displayMsgType2').html(res + ' <i class="fa fa-angle-down" aria-hidden="true"></i>');
  $('.all_statuses_view').hide();

  chatboxLists();
}

function selectReadUnreadStatus(obj) {
  if ($(obj).prop("checked") == true) {
    readStatus = 0;
  } else {
    readStatus = 1;
  }
  chatboxLists();

}

function changeState(obj) {
  $("#leftPanelNavi li").removeClass("active");
  $(obj).parent().addClass("active");
}

function chatboxLists() {
  $('#messageList').html('<li class="nav-item"><div class="loading_section"><img src="' + $("#base_url").val() + 'assets/images/icon/Loading_icon.gif" alt=""></div></li>');
  //alert(msgType+'=='+readStatus);
  var data = {};
  data.type = msgType;
  data.is_seen = readStatus;
  data.lang = $("#viewLang").val();

  $.ajax({
    url: $("#base_url").val() + "api/messagecenter/getMessageList",
    data: data,
    beforeSend: function (xhr) {
      /* Authorization header */
      xhr.setRequestHeader("Authorization", localStorage.getItem("miraimpex_token"));
      xhr.setRequestHeader("X-Mobile", "false");
    },
    method: "post",
    error: function (result) {
      //console.log("Unauthorised");
      toastr.clear();
      toastr.error(result.responseJSON.msg);

    },
    success: function (result) {
      //console.log(result);
      if (result.status === 200) {
        if (result.response_data) {
          $('#messageList').html('');

          var totalMessages = result.response_data.length;

          if (totalMessages == 0) {
            $('#messageList').html('<li class="nav-item">No chat found.</li>');
            $('#p2').addClass('d-none');
            $('#p3').addClass('d-none');
            $('#withContent').addClass('d-none');
            $('#withoutContent').removeClass('d-none').html('<div class="loading_section"><h2>No message found.</h2></div>');
          } else {
            $('#p2').removeClass('d-none');
            $('#p3').removeClass('d-none');
            $('#withContent').removeClass('d-none');
            $('#withoutContent').addClass('d-none').html('');
          }

          $.each(result.response_data, function (index, value) {

            if (value.author_id == value.target_id)
              return true;

            var profileImg = $("#base_url").val() + 'assets/images/user.png';

            if (value.target_profile_image) {
              if (value.target_profile_image.length > 2) {
                profileImg = $("#base_url").val() + 'admin/uploads/bussiness_logo/' + value.target_profile_image;
              }
            }


            if (value.type == 'rfq' || value.type == 'service_rfq') {
              var unreadCount = '';
              if (value.unread > 0) {
                unreadCount = '<span class="newUnreadcls">' + value.unread + '</span>';
              }

              var newarrival = '';
              if (value.total_message == 0) {
                newarrival = 'new';
              }

              if (value.type == 'rfq') {
                var listHtml = '<li class="nav-item ' + newarrival + '"><a class="nav-link" data-toggle="tab" href="#tabs-1" role="tab" reltype="' + value.type + '" buyerid="' + value.buyer_id + '" authorid="' + value.author_id + '" targetid="' + value.target_id + '" relpic="' + profileImg + '" relname="' + value.target_name + '" relpic="' + value.target_profile_image + '" relid="' + value.rfq_id + '" relseen="' + value.is_seen + '" onclick="javascript:chatboxDetails(this)" title="RFQ by ' + value.buyer_name + ' in: ' + value.category_name + '"><div class="all_messages_list_info"><div class="Message_list_img"><img id="update_pro_img" src="' + profileImg + '" alt="' + value.target_name + '" title="' + value.target_name + '"></div><div class="Message_list_info"><div class="top_mess"><p>' + value.target_name + '(' + value.category_name + ')</p></div><div class="show_message"><span>' + value.message + '</span></div><div class="second_mess_info"><div class="second_mess_info_dwn"><span>' + value.created_date + '</span> ' + unreadCount + '</div></div></div></div></a></li>';

                $('#messageList').append(listHtml);
              } else if (value.type == 'service_rfq') {
                var listHtml = '<li class="nav-item ' + newarrival + '"><a class="nav-link" data-toggle="tab" href="#tabs-1" role="tab" reltype="' + value.type + '" buyerid="' + value.buyer_id + '" authorid="' + value.author_id + '" targetid="' + value.target_id + '" relpic="' + profileImg + '" relname="' + value.target_name + '"relpic="' + value.target_profile_image + '" relid="' + value.rfq_id + '" relseen="' + value.is_seen + '" onclick="javascript:chatboxDetails(this)"><div class="all_messages_list_info"><div class="Message_list_img"><img id="update_pro_img" src="' + profileImg + '" alt="' + value.target_name + '" title="' + value.target_name + '"></div><div class="Message_list_info"><div class="top_mess"><p>' + value.target_name + '(' + value.category_name + ')</p></div><div class="show_message"><span>' + value.rfq_message + '</span></div><div class="second_mess_info"><div class="second_mess_info_dwn"><span>' + value.created_date + '</span> ' + unreadCount + '</div></div></div></div></a></li>';

                $('#messageList').append(listHtml);
              }
            } else if (value.type == 'enq' || value.type == 'service_enquiry') {
              var unreadCount = '';
              if (value.unread > 0) {
                unreadCount = '<span class="newUnreadcls">' + value.unread + '</span>';
              }

              var newarrival = '';
              if (value.total_message == 0) {
                newarrival = 'new';
              }

              var userName = '';
              var enqtitle = '';
              if (value.login_customer_id == value.enq_from_id) {
                if (value.seller_profile_image) {
                  if (value.seller_profile_image.length > 2) {
                    profileImg = $("#base_url").val() + 'admin/uploads/bussiness_logo/' + value.seller_profile_image;
                  }
                } else {
                  profileImg = $("#base_url").val() + 'assets/images/user.png';
                }

                userName = value.seller_business_name;

              } else if (value.login_customer_id == value.enq_to_id) {
                if (value.buyer_profile_image) {
                  if (value.buyer_profile_image.length > 2) {
                    profileImg = $("#base_url").val() + 'admin/uploads/bussiness_logo/' + value.buyer_profile_image;
                  }
                } else {
                  profileImg = $("#base_url").val() + 'assets/images/user.png';
                }
                userName = value.buyer_name;
              }

              enqtitle = 'Enquiry by ' + value.buyer_name + ' to ' + value.seller_business_name;

              var listHtml = '<li class="nav-item ' + newarrival + '"><a class="nav-link" data-toggle="tab" href="#tabs-1" role="tab" reltype="' + value.type + '" relpic="' + profileImg + '" buyerid="' + value.enq_from_id + '" authorid="' + value.author_id + '" targetid="' + value.target_id + '" relname="' + userName + '" relid="' + value.enq_id + '" relitemid="' + value.enq_seller_item_id + '" relseen="" onclick="javascript:chatboxDetails(this)" title="' + enqtitle + '"><div class="all_messages_list_info"><div class="Message_list_img"><img id="update_pro_img" src="' + profileImg + '"  alt="' + userName + '" title="' + userName + '"></div><div class="Message_list_info"><div class="top_mess"><p>' + userName + '</p></div><div class="show_message"><span>' + value.enq_message + '</span></div><div class="second_mess_info"><div class="second_mess_info_dwn"><span>' + value.enq_date + '</span> ' + unreadCount + '</div></div></div></div></a></li>';

              $('#messageList').append(listHtml);
            }



          });
        }


      } else {

      }

    }
  });
}

function chatboxDetails(obj) {
  $('#chatBoxDetails').html('<div class="loading_section"><img src="' + $("#base_url").val() + 'assets/images/icon/Loading_icon.gif" alt=""></div>');

  $('#rfqSmallImgPanel').html('');

  var type = $(obj).attr('reltype');
  //var is_seen = $(obj).attr('relseen');
  var rfq_id = $(obj).attr('relid');
  var relitemid = $(obj).attr('relitemid');
  var relpic = $(obj).attr('relpic');
  var relname = $(obj).attr('relname');

  var author_id = $(obj).attr('authorid');
  var target_id = $(obj).attr('targetid');
  var buyer_id = $(obj).attr('buyerid');

  $(obj).find('.newUnreadcls').remove();

  $('#msgType').val(type);
  $('#send_proposal_to_id').val(target_id);

  $("#messageList li").removeClass("active");
  $("#messageList li a").removeClass("active");

  $('#rfqTrackId').html('').addClass('d-none');
  $('#rfqMsg').html('').addClass('d-none');
  $('#rfqOthReq').html('').addClass('d-none');
  $('#rfqSmallImgPanel').html('').addClass('d-none');

  $(obj).addClass('active');
  //alert(localStorage.getItem("miraimpex_customer_id") + ' == ' + buyer_id);  

  var data = {};
  data.type = type;
  //data.is_seen = is_seen;
  data.rfq_id = rfq_id;
  data.itemid = relitemid;

  data.author_id = author_id;
  data.target_id = target_id;
  data.lang = 'en';

  $('#rfq_id').val(rfq_id);
  $('#to_id').val(target_id);

  sellerProfileImage = relpic;
  sellerName = relname;

  $.ajax({
    url: $("#base_url").val() + "api/messagecenter/getMessageDetails",
    data: data,
    beforeSend: function (xhr) {
      /* Authorization header */
      xhr.setRequestHeader("Authorization", localStorage.getItem("miraimpex_token"));
      xhr.setRequestHeader("X-Mobile", "false");
    },
    method: "post",
    error: function (result) {
      //console.log("Unauthorised");
      toastr.clear();
      toastr.error(result.responseJSON.msg);

    },
    success: function (result) {
      console.log(result);
      if (result.status === 200) {

        var quotationDetails = result.response_data.quatation;
        var proposalDetails = result.response_data.proposal;

        if (quotationDetails.is_sent_proposal_permission == 0) {
          $('#sendProposalDiv').hide();

        } else {
          $('#sendProposalDiv').show();

        }

        if (proposalDetails != "") {
          $('#traking_proposal').removeClass('d-none');
          if (proposalDetails.is_paid == 0) {
            $('#replyMsgCenter').removeClass('d-none');
            $('#cantReplyPanel').addClass('d-none');
          } else {
            $('#replyMsgCenter').addClass('d-none');
            $('#cantReplyPanel').removeClass('d-none');
          }

          $('#proposal_doc_link').attr('href', $("#base_url").val() + "admin/uploads/proposal/" + proposalDetails.file);
          $('#proposal_title').attr('href', $("#base_url").val() + "admin/uploads/proposal/" + proposalDetails.file);
          $('#proposal_title').html('<b style="font-weight:bold">' + proposalDetails.title + '</b>');
          $('#proposal_desc').html(proposalDetails.description);
          $('#proposal_date').html('<b>' + proposalDetails.created_date + '</b>');
          $('#proposal_amount').html('<b>' + proposalDetails.currency + ' ' + proposalDetails.amount + '</b>');
          if (proposalDetails.from_id == localStorage.getItem("miraimpex_customer_id")) {
            $('#proposal_pay_link').attr('href', 'javascript:void(0);');
            $('#proposal_pay_link').addClass('d-none');
          } else {
            if (proposalDetails.is_paid == "0") {
              $('#proposal_pay_link').removeClass('d-none');
              $('#proposal_pay_link').attr('href', $("#base_url").val() + "payment/checkout?token=" + proposalDetails.id);
            } else {
              $('#proposal_pay_link').attr('href', 'javascript:void(0);');
              $('#proposal_pay_link').addClass('d-none');
            }

          }



        } else {
          $('#traking_proposal').addClass('d-none');
          $('#replyMsgCenter').removeClass('d-none');
          $('#cantReplyPanel').addClass('d-none');
        }

        //rfq_id = quotationDetails.rfq_id;


        $('#report_incident_to_id').val(result.to_id);
        $('#report_incident_from_id').val(localStorage.getItem("miraimpex_customer_id"));

        if (quotationDetails.type == 'rfq') {
          $('#report_incident_ref_id').val(quotationDetails.rfq_id);
          $('#send_proposal_ref_id').val(quotationDetails.rfq_id);

        } else if (quotationDetails.type == 'enq' || quotationDetails.type == 'service_enquiry') {
          $('#report_incident_ref_id').val(quotationDetails.enq_id);
          $('#send_proposal_ref_id').val(quotationDetails.enq_id);
        }

        $('#report_incident_ref_type').val(quotationDetails.type);
        $('#send_proposal_ref_type').val(quotationDetails.type);




        $('#send_proposal_from_id').val(localStorage.getItem("miraimpex_customer_id"));


        var displayLi = '';
        /* if (quotationDetails.qrname) {
          displayLi += '<li>' + quotationDetails.qrname + '</li>';
        } */
        if (quotationDetails.category_name) {
          displayLi += '<li>' + quotationDetails.category_name + '</li><li><i class="fa fa-arrow-right" aria-hidden="true"></i></li>';
        }





        if (quotationDetails.type == 'rfq') {
          if (quotationDetails.message) {
            if (quotationDetails.uom_name) {

            } else {
              quotationDetails.uom_name = '';
            }
            $('#rfqMsg').removeClass('d-none').html('<p>' + quotationDetails.qrname + '</p><p><b>Quantity:</b>' + quotationDetails.quantity + ' ' + quotationDetails.uom_name + '</p><p><b>Message:</b> ' + quotationDetails.message + '</p>');
          }
        } else if (quotationDetails.type == 'enq') {
          displayLi += '<li>' + quotationDetails.itemdata.name + '</li>';
          if (quotationDetails.enq_message) {

            $('#rfqMsg').removeClass('d-none').html('<p><b>Message:</b> ' + quotationDetails.enq_message + '</p>');
          }
        }

        $('#rfqTrackId').removeClass('d-none').html(displayLi);

        if (quotationDetails.other_requerment) {
          $('#rfqOthReq').removeClass('d-none').html('<p><b>Other requirement:</b>' + quotationDetails.other_requerment + '</p>');
        }

        if (quotationDetails.images) {
          if (quotationDetails.images.length > 0) {
            $('#rfqSmallImgPanel').removeClass('d-none');
            $.each(quotationDetails.images, function (ix, val) {

              var imgP = '<div><img src="' + $("#base_url").val() + 'admin/uploads/rfq/' + val.image_name + '" alt=""></div>';
              $('#rfqSmallImgPanel').append(imgP);
            });
          }

        }


        $('#chatBoxDetails').html('');
        if (result.response_data.message) {
          var totalMessages = result.response_data.message.length;

          if (totalMessages == 0) {
            $('#chatBoxDetails').html('<div id="noChat">Conversation not started yet.</div>');
          }

        }


        $.each(result.response_data.message, function (index, value) {

          var profileImg = $("#base_url").val() + 'assets/images/user.png';
          var loginId = '';
          var listHtml = '';

          var authorId = value.from_customer_id;

          var mycls = '';

          if (localStorage.getItem("miraimpex_customer_id") == value.from_customer_id) {
            mycls = 'myMsg';
          } else {
            mycls = 'replyMsg';

          }






          if (value.fromcust_profile_image) {
            if (value.fromcust_profile_image.length > 2) {
              profileImg = $("#base_url").val() + 'admin/uploads/bussiness_logo/' + value.fromcust_profile_image;
            }
          }

          chatMImg = '';
          if (value.image) {
            chatMImg = '<img src="' + $("#base_url").val() + 'admin/uploads/messagecenter/' + value.image + '" alt="">';
          }

          listHtml = '<div class="Message_box_sec ' + mycls + '"><div class="Message_box_sec_img"><img src="' + profileImg + '" alt=""></div><div class="Message_box_sec_info"><p class="Message_send_date">' + value.fromcust_name + ' <span>' + value.date + '</span></p><div class="Message_send_cont"><p>' + value.message + '' + chatMImg + '</p></div></div></div>';



          $('#chatBoxDetails').append(listHtml);

        });

        $('#sellerProfileName_msg').html(sellerName);
        $('#sellerProfileImg_msg').attr('src', sellerProfileImage);
        $("#chatBoxDetails").animate({ scrollTop: $('#chatBoxDetails').prop("scrollHeight") }, 1000);


      } else {

      }
      $("#chatBoxDetails").animate({ scrollTop: $('#chatBoxDetails').prop("scrollHeight") }, 1000);
    }

  });
}

function AutoGrowTextArea(textField) {

  if (textField.clientHeight < textField.scrollHeight) {
    textField.style.height = textField.scrollHeight + "px";
    if (textField.clientHeight < textField.scrollHeight) {
      textField.style.height = (textField.scrollHeight * 2 - textField.clientHeight) + "px";
    }
  }

}

function autolinkgenerate(inputText) {
  var replacedText, replacePattern1, replacePattern2, replacePattern3;

  //URLs starting with http://, https://, or ftp://
  replacePattern1 = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gim;
  replacedText = inputText.replace(replacePattern1, '<a href="$1" target="_blank">$1</a>');

  //URLs starting with "www." (without // before it, or it'd re-link the ones done above).
  replacePattern2 = /(^|[^\/])(www\.[\S]+(\b|$))/gim;
  replacedText = replacedText.replace(replacePattern2, '$1<a href="http://$2" target="_blank">$2</a>');

  //Change email addresses to mailto:: links.
  replacePattern3 = /(([a-zA-Z0-9\-\_\.])+@[a-zA-Z\_]+?(\.[a-zA-Z]{2,6})+)/gim;
  replacedText = replacedText.replace(replacePattern3, '<a href="mailto:$1">$1</a>');

  return replacedText;

}


