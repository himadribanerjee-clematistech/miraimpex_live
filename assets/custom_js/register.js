var imgLess = new imagelessCaptcha(3, true, true);
var question = imgLess.formPhrase();
var answer = imgLess.getInt();

window.onload = function () {

    // put question into form
    document.getElementById("question").innerHTML = question;

}

$(document).ready(function () {
    if (localStorage.getItem("miraimpex_token") === null) {

    } else {
        window.location.replace($("#base_url").val());
    }
});

function getSuppliertype(obj) {
    var st = $(obj).attr('value');
    //alert(st);
    if (st == 'supplier') {
        $('#supplierTypePanel').show();
        $('#compnamePanel').show();
    } else {
        $('#supplierTypePanel').hide();
        $('#compnamePanel').hide();
    }
}

async function sendemailtoclient(email) {
    var base_url = $('#base_url').val();
    $.ajax({
        url: base_url + 'register/sendregistrationemailtoclient',
        type: 'POST',
        data: { 'email': email },
        dataType: 'JSON',
        success: function (response) {
            //console.log(response);

        },
        error: function (response) {
            //console.log(response);
        }
    });
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

$(document).ready(function () {
    var base_url = $('#base_url').val();
    var step = parseInt($('#step').val());
    var param_mail = $('#param_mail').val();

    $('#emailFormDiv').show();
    $('#succMsgDiv').hide();
    $('#regFormDiv').hide();

    if (step == '3') {
        $('#emailFormDiv').hide();
        $('#succMsgDiv').hide();
        $('#regFormDiv').show();
        $('#steponeli').removeClass('active');
        $('#steptwoli').removeClass('active');
        $('#stepthreeli').addClass('active');
        $('#email_').html(param_mail);
        $('#email_g').val(param_mail);
    }

    $('#emailForm').submit(function (e) {
        e.preventDefault();
        toastr.clear();

        var email = $('#email').val();
        var now = Date.now();
        var userAnswer = Number(document.getElementById("vcode").value);

        if ($.trim(email) == '') {
            toastr.error('Please Enter Email Address.');
            $("#email").focus();
            return false;
        } else if (!validateEmail($.trim(email))) {
            toastr.error('Invalid Email Address.');
            $("#email").focus();
            return false;

        } else if (userAnswer != answer) {
            toastr.error('Error: The answer you submitted is incorrect. Please try again.');
            $("#vcode").focus();
            return false;
        } else {
            $.ajax({
                url: base_url + 'register/check_mail',
                type: 'POST',
                data: { 'email': email },
                dataType: 'JSON',
                success: function (response) {
                    //console.log(response);
                    if (response.status == 1) {
                        toastr.success(response.message);
                        $('#emailFormDiv').hide();
                        $('#succMsgDiv').show();
                        $('#steponeli').removeClass('active');
                        $('#steptwoli').addClass('active');
                        $('#stepthreeli').removeClass('active');
                        $('#email_sent').html(email);
                        $('#clink_').html('<a href="' + base_url + 'register?step=2&email=' + email + '&time=' + response.sendTime + '">Click here to continue registration.</a>');

                        sendemailtoclient(email);
                    } else {
                        toastr.error(response.message);
                        $('#emailFormDiv').show();
                        $('#succMsgDiv').hide();
                        $('#steponeli').addClass('active');
                        $('#steptwoli').removeClass('active');
                        $('#stepthreeli').removeClass('active');
                    }
                },
                error: function (response) {
                    console.log(response);
                }
            });
        }
    });

    /* $('#country').change(function(){
        var country = $('#country').val();
    	
        if (country == 'Africa') {
            $('.c_code_').val('27');
            $('#c_code').val('27');

        }
        else if (country == 'India') {					

            $('.c_code_').val('91');
            $('#c_code').val('91');
        }

    	
    }); */

    $('#regForm').submit(function (e) {
        e.preventDefault();
        toastr.clear();

        var pass = $('#password').val();
        var con_pass = $('#conf_pass').val();
        var country_code = $('#c_code').val();

        if (pass != con_pass) {
            toastr.error('Your Entered Password and Confirm Password Value doesn\'t Matched!');
        }
        else {
            $("#submit").attr("disabled", true);
            var mobileNo = $.trim($('#regForm').find('input[id="c_num_1"]').val());
            var new_mobileNo = mobileNo.replace(/-|\s/g, "");
            $('#c_num_1').val(new_mobileNo);

            var formData = $('#regForm').serializeArray();

            $.ajax({
                url: base_url + 'register/registration',
                type: 'POST',
                data: formData,
                dataType: 'JSON',
                success: function (response) {
                    //console.log(response);
                    if (response.status == 200) {
                        toastr.success(response.msg);
                        $('#emailFormDiv').hide();
                        $('#succMsgDiv').hide();
                        $('#regFormDiv').hide();
                        $('#successConfirmation').show();
                    } else {
                        toastr.error(response.msg);
                    }
                    $("#submit").attr("disabled", false);
                },
                error: function (response) {
                    toastr.error(response.msg);
                    $("#submit").attr("disabled", false);
                }
            });
        }

        //console.log(formData)
    });
});