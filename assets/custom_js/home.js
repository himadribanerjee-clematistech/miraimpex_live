$(document).ready(function () {
    $("html, body").animate({ scrollTop: 0 }, "slow");


    loadProductCategoryList();
    getuomlists();
    //loadServiceCategoryList();
});

var productIds = [];
var serviceIds = [];

function getuomlists() {
    var base_url = $("#base_url").val();
    var values = {};
    values.lang = $("#viewLang").val();

    $.ajax({
        url: base_url + "api/main/getuomlist",
        data: values,
        method: "post",
        success: function (result) {
            if (result.status == 200) {
                $("#uom").empty();
                $.each(result.uom_data, function (index, value) {
                    $('#uom').append($('<option/>', {
                        value: value.id,
                        text: value.name
                    }));
                });

            }
        },
        error: function (result) {
            console.log(result.responseJSON.msg);
        }
    });
}

function loadProductCategoryList() {
    var base_url = $("#base_url").val();
    var values = {};
    values.type = 1;
    values.type = 1;
    values.level = 3;
    values.lang = $("#viewLang").val();
    values.parent_id = 'MA==';
    values.limit = 3;
    $('#category-menu-product').empty().html('<div class="loading"><img src="' + base_url + 'assets/images/lg.ring-loading-gif.gif" alt="Loading Category"></div>');

    $.ajax({
        url: base_url + "api/main/getCategoryList",
        data: values,
        method: "post",
        success: function (result) {
            if (result.status == 200) {
                //console.log(result.result);

                var menuSchema = '';
                var i = 1;
                $.each(result.result, function (i1, v1) {
                    if (i > 10) {
                        return false;
                    }

                    var submenuSchema = '';
                    if (v1.children) {

                        submenuSchema += '<div class="side-sub-menu"><div class="side_menu_cover">';

                        var subcatarr = [];

                        $.each(v1.children, function (i1c, v1c) {
                            //productIds.push(v1c.id);
                            subcatarr.push({
                                image: v1c.image,
                                id: v1c.id,
                                category_name: v1c.category_name,
                            });

                            submenuSchema += '<ul class="clearfix col-md-3"><li><a href="' + base_url + 'category/view/' + v1c.id + '"><h5>' + v1c.category_name + '</h5></a></li>';
                            if (v1c.children) {
                                $.each(v1c.children, function (i1cc, v1cc) {
                                    submenuSchema += '<li><a href="' + base_url + 'category/view/' + v1cc.id + '"><span>' + v1cc.category_name + '</span></a></li>';
                                });
                            }
                            submenuSchema += '</ul>';
                        });

                        submenuSchema += '</div></div>';

                        productIds.push({
                            type: v1.type,
                            image: v1.image,
                            id: v1.id,
                            category_name: v1.category_name,
                            subcarArr: subcatarr
                        });
                    }
                    menuSchema += '<li>\
                    <a href="' + base_url + 'category/view/' + v1.id + '"><span>' + v1.category_name + '</span><i class="pull-right fa fa-angle-right"></i></a>' + submenuSchema + '</li>';
                    i++;
                });

                menuSchema += '<li class="active"><a href="' + base_url + 'category/all/products">All Product Categories </a></li>';

                $('#category-menu-product').empty().html(menuSchema);

                //console.log(productIds);
                loadServiceCategoryList();
            }
        },
        error: function (result) {
            console.log(result.responseJSON.msg);
        }
    });

}

function loadServiceCategoryList() {
    var base_url = $("#base_url").val();
    var values = {};
    values.type = 2;
    values.level = 3;
    values.lang = $("#viewLang").val();
    values.parent_id = 'MA==';
    values.limit = 3;
    $('#category-menu-service').empty().html('<div class="loading"><img src="' + base_url + 'assets/images/lg.ring-loading-gif.gif" alt="Loading Category"></div>');

    $.ajax({
        url: base_url + "api/main/getCategoryList",
        data: values,
        method: "post",
        success: function (result) {
            if (result.status == 200) {
                //console.log(result.result);
                var menuSchema = '';
                var i = 1;
                $.each(result.result, function (i1, v1) {
                    if (i > 10) {
                        return false;
                    }

                    var submenuSchema = '';
                    if (v1.children) {

                        submenuSchema += '<div class="side-sub-menu"><div class="side_menu_cover">';

                        var subcatarr = [];
                        $.each(v1.children, function (i1c, v1c) {
                            subcatarr.push({
                                image: v1c.image,
                                id: v1c.id,
                                category_name: v1c.category_name,
                            });
                            //serviceIds.push(v1c.id);
                            submenuSchema += '<ul class="clearfix col-md-3"><li><a href="' + base_url + 'category/view/' + v1c.id + '"><h5>' + v1c.category_name + '</h5></a></li>';
                            if (v1c.children) {
                                $.each(v1c.children, function (i1cc, v1cc) {
                                    submenuSchema += '<li><a href="' + base_url + 'category/view/' + v1cc.id + '"><span>' + v1cc.category_name + '</span></a></li>';
                                });
                            }
                            submenuSchema += '</ul>';
                        });

                        submenuSchema += '</div></div>';

                        serviceIds.push({
                            type: v1.type,
                            image: v1.image,
                            id: v1.id,
                            category_name: v1.category_name,
                            subcarArr: subcatarr
                        });


                    }
                    menuSchema += '<li>\
                    <a href="' + base_url + 'category/view/' + v1.id + '"><span>' + v1.category_name + '</span><i class="pull-right fa fa-angle-right"></i></a>' + submenuSchema + '</li>';
                    i++;
                });
                menuSchema += '<li class="active"><a href="' + base_url + 'category/all/services">All Service Categories </a></li>';
                $('#category-menu-service').empty().html(menuSchema);

                //console.log(serviceIds);
                loadFeaturedSubmenuPanel();
            }
        },
        error: function (result) {
            console.log(result.responseJSON.msg);
        }
    });

}

function interleave(a, b) {
    c = [];
    tot = a.length + b.length;
    while (c.length < tot) {
        if (aa = a.shift()) {
            c.push(aa);
        }
        if (bb = b.shift()) {
            c.push(bb);
        }
    }
    return c;
}

function loadFeaturedSubmenuPanel() {
    if (productIds.length > 0 || serviceIds.length > 0) {

        // console.log(productIds, typeof (productIds), productIds.length);
        $('#featuredSubmenuPanel').empty().html('');

        var productserviceIdsCombined = interleave(productIds, serviceIds);

        // console.log(productserviceIds);
        $.each(productserviceIdsCombined, function (pik, piv) {
            var base_url = $("#base_url").val();
            //console.log(piv);
            var img = base_url + 'admin/assets/images/no-item-image.png';
            if (piv.image != '') {
                img = base_url + 'admin/uploads/category/' + piv.image;
            }

            var divType = '-pro';
            if (piv.type == 1) {
                divType = 'featuredSubmenu-pro';
            } else if (piv.type == 2) {
                divType = 'featuredSubmenu-ser';
            }

            var fetSubmenu = '<div class="product-wrapper ' + divType + ' clearfix">\
            <div class="container">\
                <div class="row">\
                    <div class="col-sm-12">\
                        <div class="product clearfix">\
                            <h4 class="heading">\
                                <span>'+ piv.category_name + '</span>\
                            </h4>\
                            <div class="product-contant clearfix">\
                                <div class="product-banner">\
                                    <a href="' + base_url + 'category/view/' + piv.id + '">\
                                        <img src="'+ img + '" alt="' + piv.category_name + '"/>\
                                    </a>\
                                    <div class="banner-link">\
                                        <a href="' + base_url + 'category/view/' + piv.id + '">VIEW ALL</a>\
                                    </div>\
                                </div>\
                                <div class="product-block clearfix">\
                                    <div class="tab-content">\
                                        <div class="tab-pane fade in active" id="submenu-' + piv.category_name + '">\
                                            <div class="item-block clearfix">';
            var xx = 0;
            $.each(piv.subcarArr, function (i1c, v1c) {
                if (xx >= 6) {
                    return false;
                }
                var simg = base_url + 'admin/assets/images/no-item-image.png';
                if (v1c.image != '') {
                    simg = base_url + 'admin/uploads/category/' + v1c.image;
                }
                fetSubmenu += '<div class="product-item">\
                <ul class="products-row">\
                <li class="image-block">\
                <a href="' + base_url + 'category/view/' + v1c.id + '"><span><img src="' + simg + '" alt="' + v1c.category_name + '"/></span></a>\
                </li>\
                <li class="products-details">\
                <span>' + v1c.category_name + '</span>\
                </li>\
                </ul>\
            </div>';
                xx++;
            });

            fetSubmenu += '</div>\
                                        </div>\
                                    </div>\
                                </div>\
                            </div>\
                        </div>\
                    </div>\
                </div>\
            </div>\
            <div class="spacer30"></div>\
        </div>';
            //console.log(pik);
            $('#featuredSubmenuPanel').append(fetSubmenu);



        });

    }
}

