var cartArr = [];
var cartSellerId = localStorage.getItem("miraimpex_cartSellerId");

if (localStorage.getItem("miraimpex_token") === null) {
	cartArr = [];
} else {
	if (localStorage.getItem("miraimpex_addtocart") === null) {
		cartArr = [];
	} else {
		cartArr = JSON.parse(localStorage.getItem("miraimpex_addtocart"));
	}

}

var varianceArr = [];



function getObjectIndex(arr, varianceName) {
	// Loop over array to find the matching element/object
	for (var i = 0; i < arr.length; i++) {
		var obj = arr[i];
		if (obj.varianceName === varianceName) {
			// When all key-value matches return the array index
			return i;
		}
	}
	// When no match found, return false
	return false;
}



function getCartObjectIndex(arr, itemid) {
	// Loop over array to find the matching element/object
	for (var i = 0; i < arr.length; i++) {
		var obj = arr[i];
		if (obj.itemid === itemid) {
			// When all key-value matches return the array index
			return i;
		}
	}
	// When no match found, return false
	return false;
}


function getvariance(obj) {
	var varianceName = $(obj).attr('relparent');
	var varianceAttribute = $(obj).attr('relval');

	var targetid = $(obj).attr('targetid');
	//alert(varianceName+'-'+varianceAttribute);

	var index = getObjectIndex(varianceArr, varianceName);

	// If object found in the array
	if (index !== false) {
		// Update the quantity in the same element
		varianceArr[index].varianceName = varianceName;
		varianceArr[index].varianceAttribute = varianceAttribute;
	} else {
		// Add the element in the array
		varianceArr.push({
			varianceName: varianceName,
			varianceAttribute: varianceAttribute
		});
	}

	$(obj).closest('ul').children().removeClass('varianceActive');
	$(obj).closest('ul li').addClass('varianceActive');

	var xt = JSON.stringify(varianceArr);

	$("#" + targetid).val(xt);
	//console.log($("#" + targetid));
	//console.log(xt);

}


function addtocart(obj) {
	$('.noitemincart').remove();
	var itemvariance = $(obj).attr('itemvariance');
	var itemid = $(obj).attr('itemid');
	var varianceExists = $(obj).attr('varianceExists');

	var fixedPrice = $(obj).attr('fixedPrice');
	var currency = $(obj).attr('currency');
	var itemname = $(obj).attr('itemname');
	var itemimg = $(obj).attr('itemimg');
	var moq = $(obj).attr('moq');
	var uom = $(obj).attr('uom');
	var seller_id = $(obj).attr('seller_id');
	var business_name = $(obj).attr('business_name');

	var itemvarianceJs = $("#" + itemvariance).val();
	itemvarianceJs = itemvarianceJs.replace(/'/g, '"');

	if ($("#" + itemvariance).val() == '' && varianceExists == 'Y') {
		toastr.clear();
		toastr.error('Please select variance!');
	} else {
		if ((cartSellerId === null) || (cartSellerId != null && cartSellerId == seller_id)) {
			cartSellerId = seller_id;
			localStorage.setItem("miraimpex_cartSellerId", seller_id);
			var cartindex = getCartObjectIndex(cartArr, itemid, seller_id);
			// If object found in the array
			if (cartindex !== false) {
				// Update the quantity in the same element
				cartArr[cartindex].itemid = itemid;
				cartArr[cartindex].itemvariance = itemvarianceJs;
				cartArr[cartindex].perItemPrice = fixedPrice;
				cartArr[cartindex].currency = currency;
				cartArr[cartindex].itemname = itemname;
				cartArr[cartindex].itemimg = itemimg;
				cartArr[cartindex].quantity = moq;
				cartArr[cartindex].moq = moq;
				cartArr[cartindex].uom = uom;
				cartArr[cartindex].seller_id = seller_id;
				cartArr[cartindex].seller_id = business_name;

				toastr.clear();
				toastr.success('Item updated successfully to cart!');
			} else {
				// Add the element in the array
				cartArr.push({
					itemid: itemid,
					itemvariance: itemvarianceJs,
					perItemPrice: fixedPrice,
					currency: currency,
					itemname: itemname,
					itemimg: itemimg,
					quantity: moq,
					moq: moq,
					seller_id: seller_id,
					business_name: business_name,
					uom: uom
				});

				toastr.clear();
				toastr.success('Item added successfully to cart!');
			}
			//console.log(cartArr);
			localStorage.setItem("miraimpex_addtocart", JSON.stringify(cartArr));
			setTimeout(
				function () {

					processAddToCart();

				}, 1000);
		} else {
			toastr.clear();
			toastr.error('You can\'t add item from different supplier!');
		}

	}

}

$(document).ready(function () {
	$('.loading').removeClass('hide');
	getproductdetails();
});

function getproductdetails() {
	var base_url = $("#base_url").val();
	var values = {};
	values.lang = $("#viewLang").val();
	values.item_id = itemId;

	$.ajax({
		url: base_url + "api/item/getitem",
		data: values,
		method: "post",
		success: function (result) {
			//console.log(result);
			if (result.status == 200) {
				if (result.item.length > 0) {
					$.each(result.item, function (i1, item) {
						var currencySymbol = '';
						if (item.currency == 'Dollar') {
							currencySymbol = '<i class="fa fa-dollar"></i>';
						} else if (item.currency == 'Euro') {
							currencySymbol = '<i class="fa fa-euro"></i>';
						} else {
							currencySymbol = '<i class="fa fa-dollar"></i>';
						}

						$('#productName').html(item.item_name);
						if (item.fixedPrice > 0) {
							$('#price_min').html(currencySymbol + '' + item.fixedPrice);
							$('#price_max').html('');
							$('#sep').hide();
						} else {
							$('#price_min').html(currencySymbol + '' + item.price_min);
							$('#price_max').html(currencySymbol + '' + item.price_max);
						}
						$('#price-uom-name').html('/' + item.price_uom_name);

						$('#item_uom').html(item.moq + ' ' + item.moq_uom_name_en);
						$('#leadTime').html((item.lead_time) ? item.lead_time : 'Not mentioned.');
						$('#shippingTime').html((item.shipping_time) ? item.shipping_time : 'Not mentioned.');
						$('#delivery_available').html((item.delivery_available) ? item.delivery_available : 'Not mentioned.');
						$('#supplierName').html(item.sellerinfo.business_name);
						$('#supplierName').attr('href', base_url + 'supplier/details/' + item.seller_id);
						$('#seller_id').val(item.seller_id);
						$('#supplierLocation').html(item.sellerinfo.state + ', ' + item.sellerinfo.country);
						$('#productDescription').html(item.description);

						var varianceExists = 'N';

						var attribute_variance_html = '';

						var predefvarianceArr = [];
						var pcount = 0;
						var subcount = 0;

						if (item.modified_variance != "") {
							var attribute_variance = jQuery.parseJSON(item.modified_variance);

							$.each(attribute_variance, function (i3, v3) {
								attribute_variance_html += '<div><fieldset class="attribute_fieldset"><label class="attribute_label">' + i3 + ':</label><div class="attribute_list"><ul class="attribute_color">';
								$.each(v3, function (v3k, v3v) {
									var colorHexCode = colorconvert(v3v);
									if (colorHexCode) {
										attribute_variance_html += '<li style="background-color:' + colorHexCode + '; width:32px; height:25px;" title="' + v3v + '"><a href="javascript:void(0);" onclick="javascript:getvariance(this)" relparent="' + i3 + '" relval="' + v3v + '" targetid="itemVariance"></a></li>';

									} else {
										attribute_variance_html += '<li><a href="javascript:void(0);" onclick="javascript:getvariance(this)" relparent="' + i3 + '" relval="' + v3v + '" targetid="itemVariance">' + v3v + '</a></li>';
									}

									if (pcount == 0) {
										predefvarianceArr.push({
											varianceName: i3,
											varianceAttribute: v3v
										});
									}

									varianceExists = 'Y';
									pcount++;
									subcount++;
								})

								attribute_variance_html += ' </ul></div></fieldset></div>';
								pcount = 0;
							});
							$('#attributePanel').html(attribute_variance_html);
						}

						var predefvariance = JSON.stringify(predefvarianceArr);
						predefvariance = predefvariance.replace(/"/g, "'");

						if (predefvariance == '[]') {
							predefvariance = '';
						}

						var thumbnailimg = base_url + 'admin/assets/images/no-item-image.png';

						if (item.thumbnail != "") {
							thumbnailimg = base_url + 'admin/uploads/selleritem/' + item.thumbnail;

							$('#productDescription').append('<div class="spacer30"></div><div class="col-md-6"><div class="spacer30"></div><img src="' + base_url + 'admin/uploads/selleritem/' + item.thumbnail + '"></div>');

						}

						$('#xzoom-default').attr('src', thumbnailimg);
						$('#xzoom-default').attr('xoriginal', thumbnailimg);

						var xzoom_thumb_schema_thumbnail = '<a href="' + thumbnailimg + '" src="' + thumbnailimg + '"><img class="xzoom-gallery"  src="' + thumbnailimg + '" xpreview="' + thumbnailimg + '" title="' + item.item_name + '"></a>';

						$('#xzoom-thumbs').html(xzoom_thumb_schema_thumbnail);

						if (item.item_image) {
							var item_image = jQuery.parseJSON(item.item_image);

							if (item_image.length > 0) {
								$.each(item_image, function (index, iv) {

									var thumbnailimg = base_url + 'admin/uploads/selleritem/' + iv.name;

									var bigSchema = '<a href="' + thumbnailimg + '"><img class="xzoom-gallery"  src="' + thumbnailimg + '"   xpreview="' + thumbnailimg + '" title="' + item.item_name + '"></a>';

									$("#xzoom-thumbs").append(bigSchema);

									$('#productDescription').append('<div class="col-md-6"><div class="spacer30"></div><img src="' + thumbnailimg + '"></div>');

								});


							}
						}


						var variance_html = '';
						var varianceCount = 0;
						if (item.modified_variance != "") {
							var variance = jQuery.parseJSON(item.modified_variance);

							$.each(variance, function (i3, v3) {
								variance_html += '<li><p>' + i3 + ': </p><p class="value">';
								$.each(v3, function (v3k, v3v) {
									variance_html += v3v + ', ';

								})

								variance_html += ' </p></li>';
								varianceCount++;
							});
							$('#detailsVariance').html(variance_html);
						}

						if (varianceCount == 0) {
							$('#detailsVarianceDiv').remove();
						}

						var attribute_html = '';
						var attributeCount = 0;
						if (item.modified_attribute != "") {

							var attribute = jQuery.parseJSON(item.modified_attribute);

							$.each(attribute, function (i3, v3) {
								attribute_html += '<li><p>' + i3 + ': </p><p class="value">' + v3 + '</p></li>';
								attributeCount++;
							});
							$('#detailsAttributes').html(attribute_html);
						}

						if (attributeCount == 0) {
							$('#detailsAttributesDiv').remove();
						}

						if (item.keywords != "") {
							var keywords = item.keywords.split(',');
							$.each(keywords, function (keyk, keyv) {
								$('#itemTags').append('<li><a href="javascript:void(0);">' + keyv + '</a></li>');
							});
						}

						if (item.fixedPrice > 0 && item.sellerinfo.displayLocal == "1") {

							$('#itemVariance').val(predefvariance);

							$('#contact_supplier_addtocart').html('<a href="javascript:void(0);" onclick="javascript:addtocart(this);" itemid="' + item.item_id + '" itemvariance="itemVariance" varianceExists="' + varianceExists + '" fixedPrice="' + item.fixedPrice + '" currency="' + item.currency + '" itemname="' + item.item_name + '" business_name="' + item.sellerinfo.business_name + '" seller_id="' + item.seller_id + '" moq="' + item.moq + '" uom="' + item.moq_uom_name_en + '" itemimg="' + thumbnailimg + '" title="Add To Cart"><i class="fa fa-shopping-cart"></i> Add To Cart</a>');
						} else {
							$('#contact_supplier_addtocart').html('<a href="' + base_url + 'contactsupplier?seller=' + item.seller_id + '&item=' + item.item_id + '"><i class="fa fa-envelope"></i> Contact Supplier</a>');
						}

						getsupplierlists();
					});
					$('.loading').addClass('hide');
				} else {
					// No item found
					$('#supplierListsPanel').append('<div class=""><h2 style="text-align:center;">No seller yet.</h2></div>');
				}

			} else {
				// Something went wrong
			}


		},
		error: function (result) {
			console.log(result.responseJSON.msg);
		}
	});



}

function getsupplierlists() {
	var base_url = $("#base_url").val();
	var values = {};
	values.lang = $("#viewLang").val();
	values.seller_id = $("#seller_id").val();

	$.ajax({
		url: base_url + "api/item/getSellerDetails",
		data: values,
		method: "post",
		success: function (result) {
			//console.log(result);
			if (result.status == 200) {
				if (result.item.length > 0) {
					$.each(result.item, function (i1, seller) {

						if (seller.seller_items.length > 0) {
							$.each(seller.seller_items, function (sik, siv) {
								if (itemId != siv.item_id) {
									if (siv.fixedPrice > 0) {
										var bigSchema = '<div class="item"><div><a href="' + base_url + 'product/details/' + siv.item_id + '"><img src="' + base_url + 'admin/uploads/selleritem/' + siv.thumbnail + '" alt="img-' + sik + '"/></a></div><div class="products_slider_info"><div><a href="' + base_url + 'product/details/' + siv.item_id + '">' + siv.item_name + '</a></div><div><h4>' + siv.currency + ' ' + siv.fixedPrice + '</h4></div><div><p>' + siv.moq + ' ' + siv.moq_uom_name_en + ' <span>(MOQ)</span></p></div></div></div>';

										$("#seller_other_items").append(bigSchema);
									} else {
										var bigSchema = '<div class="item"><div><a href="' + base_url + 'product/details/' + siv.item_id + '"><img src="' + base_url + 'admin/uploads/selleritem/' + siv.thumbnail + '" alt="img-' + sik + '"/></a></div><div class="products_slider_info"><div><a href="' + base_url + 'product/details/' + siv.item_id + '">' + siv.item_name + '</a></div><div><h4>' + siv.currency + ' ' + siv.price_min + ' - ' + siv.price_max + '</h4></div><div><p>' + siv.moq + ' ' + siv.moq_uom_name_en + ' <span>(MOQ)</span></p></div></div></div>';

										$("#seller_other_items").append(bigSchema);
									}

								}

							});

							loadOtherItemGalleryPanel();
						} else {
							$('#seller_other_items_panel').remove();
						}

						if (seller.certificate.length > 0) {
							//alert(seller.certificate.length);
							$.each(seller.certificate, function (cik, civ) {

								var bigSchema = '<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 image"><div class="img-wrapper"><a href="' + base_url + 'admin/uploads/seller/' + civ.seller_image + '"><img src="' + base_url + 'admin/uploads/seller/' + civ.seller_image + '" class="img-responsive"></a><div class="img-overlay"><i class="fa fa-plus-circle" aria-hidden="true"></i></div></div></div>';

								$("#image-gallery").append(bigSchema);

							});

							//loadcertificategalpanel();

						}

					});
				} else {
					// No item found
				}

			} else {
				// Something went wrong
			}


		},
		error: function (result) {
			console.log(result.responseJSON.msg);
		}
	});



}

function loadOtherItemGalleryPanel() {
	$(".owl-carousel.product_details_sld").owlCarousel({
		items: 4,
		itemsDesktop: [1200, 3],
		itemsDesktopSmall: [1199, 3],
		itemsTablet: [950, 3],
		itemsTabletSmall: [767, 3],
		itemsMobile: [479, 2],
		slideSpeed: 600,
		autoPlay: 3000,
		stopOnHover: true,
		navigation: true,
		pagination: false,
		responsive: true,
		autoHeight: true,
		navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"]
	});
}

