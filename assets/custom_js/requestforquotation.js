var rfqSearchTimeout = null;

function openmanualcat() {
    //alert('ok');
    $('#catSelectManual').removeClass('d-none');
    $(".show_panel").toggle();
}

function progessbar() {
    var cat_id = $("input[name='cat_id']:checked").val();
    var qty = $("#qty").val();
    var uom = $("#uom").val();
    var description = $("#description").val();
    //var rfqExpiryDate = $("#rfqExpiryDate").val();
    var tnc = 0;
    if ($("#tnc").prop("checked") == true) {
        tnc = 1;
    }
    var total = 10;
    if ($.trim(cat_id) != '') {
        total = parseInt(total) + 10;
    }
    if ($.trim(qty) != '') {
        total = parseInt(total) + 10;
    }
    if ($.trim(uom) != '') {
        total = parseInt(total) + 10;
    }
    if ($.trim(description) != '') {
        total = parseInt(total) + 10;
    }

    if (tnc == 1) {
        total = parseInt(total) + 10;
    }


    //console.log(total);
    $('#one').removeClass('active');
    $('#two').removeClass('active');
    $('#three').removeClass('active');

    if (parseInt(total) >= 40) {
        $('#one').addClass('active');
    }
    if (parseInt(total) >= 50) {
        $('#two').addClass('active');

    }
    if (parseInt(total) >= 60) {
        $('#three').addClass('active');
    }


}

function getCatBreadcumb(obj) {
    var breadcumb = $(obj).attr('rel');
    var catId = $(obj).attr('relid');
    $('#rfq_cat_id').val(catId);
    $('#displaycatBreadcumb').html(breadcumb);
    $(".show_panel").toggle();
}


$(document).ready(function () {

    var base_url = $("#base_url").val();

    initializeRfqLocationGoogleAddress();
    $nav = $("#rfqnav");

    //hide all but the first sub list
    $nav.find("ul").hide();

    $("a.ancroot").click(function () {
        $nav.find("ul").hide();
        $(this).closest("li").children("ul").show().addClass("open");
        progessbar();
    });

    $("a.anc").click(function () {
        $(this).closest("li").children("ul").show().addClass("open");
        progessbar();
    });

    getuomlists();
    progessbar();

    $(".moo").keyup(function () {
        progessbar()
    });

    $('#catKeyword').keyup(function () {
        var base_url = $("#base_url").val();
        var key = $('#catKeyword').val();
        $('#catSearchloading').removeClass('d-none');
        if (rfqSearchTimeout !== null) {
            clearTimeout(rfqSearchTimeout);
        }
        rfqSearchTimeout = setTimeout(function () {

            if (key.length > 2) {
                var name = $.trim($('#catKeyword').val());
                //alert(name);
                $.ajax({
                    url: base_url + 'api/main/getCatSearch',
                    type: 'POST',
                    data: { keyword: name },
                    dataType: 'JSON',
                    success: function (response) {
                        //console.log(response);
                        console.log(response.result.length);
                        if (response.result.length > 0) {
                            //alert('response.result.length');
                            $('#catSearchloading').addClass('d-none');
                            $('#rfqcatsrchList').html('').show();
                            var template = '';

                            $.each(response.result, function (key, value) {
                                var sercatSlug = '';

                                template += '<li style="cursor: pointer;"><a style="color:blue" relid="' + value.id + '"  rel="' + value.breadcumb + '" onclick="getCatBreadcumb(this)"><strong>' + value.breadcumb + '</strong></a></li>';


                            });
                            $('#rfqcatsrchHeading').removeClass('d-none');
                            $('#notsatisfied').removeClass('d-none');

                            $('#rfqcatsrchList').append(template);
                            $('#rfqcatsrchList').removeClass('d-none');
                            $('#catSearchloading').addClass('d-none');
                        } else {
                            $('#catSearchloading').addClass('d-none');
                            $('#rfqcatsrchHeading').addClass('d-none');
                            $('#notsatisfied').removeClass('d-none');
                            $('#rfqcatsrchList').html('').addClass('d-none');
                        }

                    },
                    error: function (response) {
                        alert("Error: " + response.message);
                    }

                });

            } else {
                $('#srchList').hide();
                $('#catSearchloading').addClass('d-none');
                $('#rfqcatsrchHeading').addClass('d-none');
                $('#rfqcatsrchList').html('');
                $('#notsatisfied').addClass('d-none');
            }

        }, 500);





    });

})

function getuomlists() {
    var base_url = $("#base_url").val();
    var values = {};
    values.lang = $("#viewLang").val();

    $.ajax({
        url: base_url + "api/main/getuomlist",
        data: values,
        method: "post",
        success: function (result) {
            if (result.status == 200) {
                $("#uom").empty();
                $.each(result.uom_data, function (index, value) {
                    $('#uom').append($('<option/>', {
                        value: value.id,
                        text: value.name
                    }));
                });

            }
        },
        error: function (result) {
            console.log(result.responseJSON.msg);
        }
    });
}


var abc = 0;

$(document).ready(function () {

    // Following function will executes on change event of file input to select different file.
    $('body').on('change', '#rfq-image-add', function () {
        if (this.files && this.files[0]) {
            abc += 1; // Incrementing global variable by 1.
            var z = abc - 1;
            $(this).find('#rfqpreviewimg' + z).remove();

            $('#rfqimgdv').append("<div style='display: inline-block; padding: 14px;'><div id='rfq_" + abc + "' class='abcd'><img id='rfqpreviewimg" + abc + "' src='' style='width: 139px; height: 89px;' /><div style='bottom: 0; width: 100%; height: 24px; line-height: 9px; background: #3b485a; opacity: .85;'><a class='remove' href='javascript:void(0);' onclick='return false;' style='display: inline-block; width: 100%; height: 24px; background: url(../assets/images/uploader-icons.png) no-repeat center 3px; text-align: center; background-position: center -37px;'><i class=\"fa fa-trash\" aria-hidden=\"true\" style=\"padding-top: 3px;font-size: 16px;\"></i></a></div></div></div>");

            var reader = new FileReader();
            reader.onload = rfqimageIsLoaded;
            reader.readAsDataURL(this.files[0]);
            $(this).hide();

            $("#rfq_" + abc).click(function () {
                $(this).parent().remove();
                var id = $(this).attr("id");
                var id_arr = id.split("_");
                var no = (parseInt(id_arr[1]) - 1);
                //console.log(abc,id_arr[1],no);
                if (id_arr.length == 2) {
                    $("#rfqfilediv_" + no).val("").trigger("change");
                    $("#rfqfilediv_" + no).remove();
                }

            });



            $("#rfqimgdv").after($("<div id='rfqfilediv_" + abc + "' class ='testcls' />").fadeIn('slow').prepend($("<input/>", {
                name: 'image[]',
                type: 'file',
                id: 'rfq-image-add'
            }), $("")));

        }
    });
});

// To Preview Image
function rfqimageIsLoaded(e) {
    $('#rfqpreviewimg' + abc).attr('src', e.target.result);
};


$(document).ready(function () {

    $("#rfqquoteForm").submit(function (e) {
        e.preventDefault();
        clearInterval();
        toastr.clear();

        var submit = true;

        if ($.trim($('#rfq_cat_id').val()) == '') {
            toastr.error('Category is missing! Please select a category.');
            submit = false;
            return false;
        }

        if ($.trim($('#uom').val()) == '') {
            toastr.error('Unit of measurement is missing! Please select an item.');
            submit = false;
            return false;
        }
        if ($.trim($('#qty').val()) == '') {
            toastr.error('Quantity is missing!');
            submit = false;
            return false;
        }
        if (parseInt($("#qty").val()) < 1) {
            toastr.error('Quantity cannot be less than 1');
            $('#qty').focus();
            submit = false;
            return false;
        }

        if ($.trim($('#description').val()) == '') {
            toastr.error('Specify your requirement!');
            submit = false;
            return false;
        }

        if (submit) {
            // disabled the submit button
            $("#rfqsubmit").prop("disabled", true);

            var form_data = new FormData(this);
            // Read selected files

            if ($.trim($('#rfq_cat_id').val()) != '') {
                form_data.set('cat_id', $('#rfq_cat_id').val());
            }



            // Display the key/value pairs
            /* for (var pair of form_data.entries()) {
                console.log(pair[0]+'='+pair[1]); 
            } */

            $.ajax({
                type: "POST",
                method: "post",
                enctype: 'multipart/form-data',
                url: $("#base_url").val() + "api/rfq/addRFQ",
                data: form_data,
                processData: false,
                contentType: false,
                cache: false,
                timeout: 600000,
                beforeSend: function (xhr) {
                    /* Authorization header */
                    xhr.setRequestHeader("Authorization", localStorage.getItem("miraimpex_token"));
                    xhr.setRequestHeader("X-Mobile", "false");
                },
                success: function (data) {
                    //console.log("SUCCESS : ", data);
                    toastr.success("RFQ sent successfully");
                    $("#rfqsubmit").prop("disabled", false);
                    setTimeout(
                        function () {
                            window.location.replace($("#base_url").val());

                        }, 1000);



                },
                error: function (result) {
                    toastr.clear();
                    toastr.error(result.responseJSON.msg);
                    $("#rfqsubmit").prop("disabled", false);

                }
            });
        }

    });
});

$(document).ready(function () {


    $('.show_panel').hide();

    $(".click_here").click(function () {
        $(".show_panel").toggle();
        $('#catKeyword').focus();
        $('#catSelectManual').addClass('d-none');

    });


});
