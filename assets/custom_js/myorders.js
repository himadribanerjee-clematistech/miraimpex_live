var pageno = 1;
$(document).ready(function () {
    if (localStorage.getItem("miraimpex_token") === null) {
        window.location.replace($("#base_url").val());
    } else {
        getorderlists();
    }
});

function getorderlists() {
    $('.loading').removeClass('hide');
    var total_info = JSON.parse(localStorage.getItem("miraimpex_total_info"));
    var base_url = $("#base_url").val();

    var data = {};
    data.pageno = pageno;

    $.ajax({
        url: $("#base_url").val() + "api/messagecenter/getOrderList",
        data: data,
        beforeSend: function (xhr) {
            /* Authorization header */
            xhr.setRequestHeader("Authorization", localStorage.getItem("miraimpex_token"));
            xhr.setRequestHeader("X-Mobile", "false");
        },
        method: "post",
        error: function (result) {
            //console.log("Unauthorised");
            toastr.clear();
            toastr.error(result.responseJSON.msg);

        },
        success: function (result) {
            console.log(result);
            if (result.status === 200) {
                var totalMessages = result.order_data.length;
                if (totalMessages == 0) {
                    $('#messageList').html('<h2 style="text-align:center;">No order/proposal found.</h2>');
                    $('.loading').addClass('hide');
                }
                $('#countTotal').html('Total ' + totalMessages + ' order(s) found.')
                $.each(result.order_data, function (index, value) {



                    if (value.proposal_id.length > 0) {
                        var toprofileImg = profileImg = $("#base_url").val() + 'assets/images/user.png';

                        if (value.from_profile_image) {
                            if (value.from_profile_image.length > 2) {
                                profileImg = $("#base_url").val() + 'admin/uploads/bussiness_logo/' + value.from_profile_image;
                            }
                        }


                        var proposalSchema = '<div class="col-md-4 col-sm-4"><a href="' + base_url + 'profile/orderdetails/' + value.order_id + '"><div class="my_order_info"><div class="order_img"><img src="' + $("#base_url").val() + 'assets/images/icon/proposal-icon.png" alt="img-1"/></div><div class="order_cnt"><p>Date: ' + value.created_date + '</p><h4>' + value.title + '</h4><div class="supplier_info"><p>From:</p><img src="' + profileImg + '" alt="img-1"/><h4>' + value.from_business_name + '</h4></div></div></div></a></div>';

                        $('#messageList').append(proposalSchema);
                    } else {
                        var neworder = '';

                        if (total_info.id == value.buyer_id) {
                            if (value.order_status == "0") {
                                neworder = '<span class="new offer" style="background:green; color:white; margin-top:15px; font-size:12px;">New</span>';
                            }

                            if (value.order_status == "1") {
                                neworder = '<span class="new offer" style="background:#1c84b5; color:white; margin-top:15px; font-size:12px;">Accepted</span>';
                            }
                            if (value.order_status == "2") {
                                neworder = '<span class="new offer" style="background:#1c84b5; color:white; margin-top:15px; font-size:12px;">Shipped</span>';
                            }
                            if (value.order_status == "3") {
                                neworder = '<span class="new offer" style="background:#1c84b5; color:white; margin-top:15px; font-size:12px;">Delivered</span>';
                            }
                            if (value.order_status == "4") {
                                neworder = '<span class="new offer" style="background:#6e3ce6; color:white; margin-top:15px; font-size:12px;">Closed</span>';
                            }
                            if (value.order_status == "5") {
                                neworder = '<span class="new offer" style="background:red; color:white; margin-top:15px; font-size:12px;">Cancelled</span>';
                            }

                            var paymentStatus = '';

                            if (value.status == "0") {
                                paymentStatus = '<p style="background-color: #fc5b5b; color: #fff; font-size: 14px; padding: 4px 20px; width:50%; text-align:center;" >Unpaid</p>';
                            }
                            if (value.status == "1") {
                                paymentStatus = '<p style="background-color: #008000; color: #fff; font-size: 14px; padding: 4px 20px; width:50%; text-align:center;" >Paid</p>';
                            }
                            if (value.status == "2") {
                                paymentStatus = '<p style="background-color: #f11010; color: #fff; font-size: 14px; padding: 4px 20px; width:50%; text-align:center;" >Failed</p>';
                            }

                        }
                        if (total_info.id == value.seller_id) {
                            if (value.order_status == "0") {
                                neworder = '<span class="new offer" style="background:green; color:white; margin-top:15px; font-size:12px;">New</span>';
                            }
                            if (value.order_status == "1") {
                                neworder = '<span class="new offer" style="background:#1c84b5; color:white; margin-top:15px; font-size:12px;">Accepted</span>';
                            }
                            if (value.order_status == "2") {
                                neworder = '<span class="new offer" style="background:#1c84b5; color:white; margin-top:15px; font-size:12px;">Shipped</span>';
                            }
                            if (value.order_status == "3") {
                                neworder = '<span class="new offer" style="background:#1c84b5; color:white; margin-top:15px; font-size:12px;">Delivered</span>';
                            }
                            if (value.order_status == "4") {
                                neworder = '<span class="new offer" style="background:#6e3ce6; color:white; margin-top:15px; font-size:12px;">Closed</span>';
                            }
                            if (value.order_status == "5") {
                                neworder = '<span class="new offer" style="background:red; color:white; margin-top:15px; font-size:12px;">Cancelled</span>';
                            }

                            var paymentStatus = '';
                            if (value.status == "0") {
                                paymentStatus = '<p style="background-color: #fc5b5b; color: #fff; font-size: 14px; padding: 4px 20px; width:50%; text-align:center;" >Unpaid</p>';
                            }
                            if (value.status == "1") {
                                paymentStatus = '<p style="background-color: #008000; color: #fff; font-size: 14px; padding: 4px 20px; width:50%; text-align:center;" >Paid</p>';
                            }
                            if (value.status == "2") {
                                paymentStatus = '<p style="background-color: #f11010; color: #fff; font-size: 14px; padding: 4px 20px; width:50%; text-align:center;" >Failed</p>';
                            }

                        }

                        var proposalSchema = '<div class="col-md-4 col-sm-4"><a href="' + base_url + 'profile/orderdetails/' + value.order_id + '"><div class="my_order_info">' + neworder + '<div class="order_img"><img src="' + $("#base_url").val() + 'assets/images/icon/my_order.png" alt="img-1"/></div><div class="order_cnt"><p>Date: ' + value.order_date + '</p><h4>Total: $' + value.total_amount + '</h4><div class="supplier_info"><p>Transaction#:</p><h4>' + value.transction_id + '</h4>' + paymentStatus + '</div></div></div></a></div>';

                        $('#messageList').append(proposalSchema);
                        $('.loading').addClass('hide');
                    }


                });
            }
        }
    });
}