$(document).ready(function () {
    if (localStorage.getItem("miraimpex_token") === null) {
        window.location.replace($("#base_url").val());
    } else {
        getenquirylists();
    }
});

function getenquirylists() {
    var base_url = $("#base_url").val();
    $("#quotation_mess").empty();
    $("#quotation_mess").show();
    $('#quotation_mess').html('<div style="text-align:center"><img src="' + $("#base_url").val() + 'assets/images/all_icon/Loading_icon.gif" alt=""></div>');

    var data = {};
    data.pageno = $("#pageNum").val();
    data.lang = $("#viewLang").val();

    $.ajax({
        url: $("#base_url").val() + "api/enquiry/getenquirylist",
        data: data,
        beforeSend: function (xhr) {
            /* Authorization header */
            xhr.setRequestHeader("Authorization", localStorage.getItem("miraimpex_token"));
            xhr.setRequestHeader("X-Mobile", "false");
        },
        method: "post",
        error: function (result) {
            //console.log("Unauthorised");
            //toastr.clear(); 
            //toastr.error(result.responseJSON.msg); 
            action = 'active';
        },
        success: function (result) {
            //console.log(result);
            if (result.status === 200) {
                $('#quotation_mess').html('');
                $("#pageNum").val(result.next);

                var newNotification = result.response_data.length;
                if (newNotification > 0) {
                    $('#totalUnreadRFQs').html('Total ' + newNotification + ' enquiry found.');

                    $.each(result.response_data, function (index, value) {

                        var profileImg = $("#base_url").val() + 'assets/images/user.png';

                        var userName = '';
                        if (value.login_customer_id == value.enq_from_id) {
                            if (value.seller_profile_image) {
                                if (value.seller_profile_image.length > 2) {
                                    profileImg = $("#base_url").val() + 'admin/uploads/bussiness_logo/' + value.seller_profile_image;
                                }
                            } else {
                                profileImg = $("#base_url").val() + 'assets/images/user.png';
                            }
                            userName = 'To: ' + value.seller_business_name;
                        } else if (value.login_customer_id == value.enq_to_id) {
                            if (value.buyer_profile_image) {
                                if (value.buyer_profile_image.length > 2) {
                                    profileImg = $("#base_url").val() + 'admin/uploads/bussiness_logo/' + value.buyer_profile_image;
                                }
                            } else {
                                profileImg = $("#base_url").val() + 'assets/images/user.png';
                            }
                            userName = 'From: ' + value.buyer_name;
                        }



                        var displayLi = '<p class="rfqCat"><span>' + value.itemdata.name + '</span></p>';



                        var imgP = '';
                        var imgP = '';
                        if (value.attachments) {
                            if (value.attachments.length > 0) {
                                $.each(value.attachments, function (ix, val) {

                                    imgP += '<div class="rfq_img"><img src="' + $("#base_url").val() + 'admin/uploads/enquiry/' + val + '" alt=""></div>';
                                });
                            }

                        }

                        var rfq_message = '';

                        if (value.enq_message) {

                            rfq_message = '<p class="quantity_messages"><b>Message:</b><span> ' + value.enq_message + '</span></p>';
                        }

                        var other_requerment = '';

                        var extendRfqExpiryButton = '';


                        var notifyHtml = '<div class="Message_list "><div class="Message_list_info"><a href="' + base_url + 'messagecenter?type=' + value.type + '&id=' + value.enq_id + '" onclick="javascript:void(0);" rel="' + value.enq_id + '" title="View details"><div class="Message_list_info"><div class="Message_list_info_img"><img src="' + profileImg + '" alt=""></div><div><div class="name_date"><p>' + userName + '</p><span class="date">' + value.enq_date + '</span></div>' + displayLi + '' + rfq_message + '' + other_requerment + '<div class="rfq_img_list">' + imgP + '</div></div></div></a></div></div>';

                        $('#quotation_mess').append(notifyHtml);

                    });

                } else {

                    $('#quotation_mess').html('<div style="text-align:center">No enquiry found.</div>');

                }


            } else {

            }

        }
    });

}