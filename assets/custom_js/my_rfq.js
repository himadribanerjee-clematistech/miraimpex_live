$(document).ready(function () {
    if (localStorage.getItem("miraimpex_token") === null) {
        window.location.replace($("#base_url").val());
    } else {
        getrfqlists();
    }
});

function getrfqlists() {
    var total_info = JSON.parse(localStorage.getItem("miraimpex_total_info"));
    var login_customer_id = total_info.id;

    var base_url = $("#base_url").val();
    var values = {};
    values.lang = $("#viewLang").val();
    $("#quotation_mess").empty();
    $("#quotation_mess").show();
    $('#quotation_mess').html('<div style="text-align:center"><img src="' + $("#base_url").val() + 'assets/images/all_icon/Loading_icon.gif" alt=""></div>');

    $.ajax({
        url: base_url + "api/rfq/getRfqlist",
        data: values,
        beforeSend: function (xhr) {
            /* Authorization header */
            xhr.setRequestHeader("Authorization", localStorage.getItem("miraimpex_token"));
            xhr.setRequestHeader("X-Mobile", "false");
        },
        method: "post",
        success: function (result) {
            //console.log(result);
            if (result.status == 200) {
                $("#quotation_mess").empty();
                $("#quotation_mess").show();
                $('#totalUnreadRFQs').html('Total ' + result.data.length + ' RFQ found.');

                if (result.data.length == 0) {
                    $('#quotation_mess').html('<div class=""><h2 style="text-align:center;">No RFQ found.</h2></div>');
                }
                $.each(result.data, function (index, value) {
                    var profileImg = $("#base_url").val() + 'assets/images/user.png';
                    if (value.business_logo) {
                        if (value.business_logo.length > 2) {
                            profileImg = $("#base_url").val() + 'admin/uploads/bussiness_logo/' + value.business_logo;
                        }
                    }


                    var unread = '';

                    if (value.is_seen == "0") {
                        unread = 'newunread';
                    }

                    var displayLi = '<p class="rfqCat">';
                    if (value.qrname) {
                        displayLi += '<span>' + value.qrname + '</span> in ';
                    }
                    if (value.category_name) {
                        displayLi += '<span>' + value.category_name + '</span>';
                    }


                    displayLi += '</p>';

                    var imgP = '';
                    if (value.images) {
                        if (value.images.length > 0) {
                            $.each(value.images, function (ix, val) {

                                imgP += '<div class="rfq_img"><img src="' + $("#base_url").val() + 'admin/uploads/rfq/' + val.image_name + '" alt=""></div>';
                            });
                        }

                    }

                    var rfq_message = '';

                    if (value.message) {
                        if (value.uom_name) {

                        } else {
                            value.uom_name = '';
                        }
                        rfq_message = '<p class="quantity_messages"><b>Quantity:</b><span>' + value.quantity + ' ' + value.uom_name + '</span></p><p class="quantity_messages"><b>Message:</b><span> ' + value.message + '</span></p>';
                    }

                    var other_requerment = '';
                    if (value.other_requerment) {
                        other_requerment = '<p class="quantity_messages"><b>Other Requirements [Optional] :</b><span>' + value.other_requerment + '</span></p>';
                    }

                    var extendRfqExpiryButton = '';
                    if (value.is_expiry_date == 1) {
                        extendRfqExpiryButton = '<div class="Message_list_info" style="text-align:right; color: #888;"><a href="javascript:void(0);" class="extendrfq" onclick="javascript:extendRfqExpiry(this);" rel="' + value.rfq_id + '">Extend RFQ</a> </div>';
                    } else if (value.is_expiry_date == 0) {
                        extendRfqExpiryButton = '<div class="Message_list_info" style="text-align:right; color: #888;"><a href="javascript:void(0);" class="extendrfq-disable">Extend RFQ</a> </div>';
                    } else {
                        extendRfqExpiryButton = '';
                    }
                    //extendRfqExpiryButton = '';

                    var targetUrl = base_url + 'messagecenter?type=rfq&id=' + value.rfq_id + '';
                    if (value.active_link == 0) {
                        targetUrl = 'javascript:void(0);';
                    }

                    var gts = '';
                    var newNot = '';

                    if (login_customer_id == value.buyer_id) {
                        value.created_date = 'Sent on ' + value.created_date;
                        value.buyer_name = 'You (' + value.buyer_name + ')';
                    } else {
                        value.buyer_name = 'From: ' + value.buyer_name;
                        value.created_date = 'Received on ' + value.created_date;
                        gts = 'style="border:1px solid #c7fbc7;"';

                        newNot = '<span style="background-color: #5aa20b; color: white !important; padding: 1px 5px; display: inline-block; border-radius: 3px; font-size: 12px; text-align: center; border: 1px solid #5fab0b; margin-top:10px; margin-left:1px;">Respond To This RFQ</span>';
                    }


                    var notifyHtml = '<div class="Message_list "><div class="Message_list_info" ' + gts + '><a href="' + targetUrl + '" rel="' + value.rfq_id + '" title="View details"><div class="Message_list_info"><div class="Message_list_info_img"><img src="' + profileImg + '" alt=""></div><div><div class="name_date"><p>' + value.buyer_name + '</p><span class="date">' + value.created_date + '</span></div>' + displayLi + '' + rfq_message + '' + other_requerment + '<div class="rfq_img_list">' + imgP + '</div><div class="rfq_img_list" style="padding-left:0;">' + newNot + '</div></div></div></a>' + extendRfqExpiryButton + '</div></div>';

                    $('#quotation_mess').append(notifyHtml);
                });

            }
        },
        error: function (result) {
            console.log(result.responseJSON.msg);
        }
    });
}