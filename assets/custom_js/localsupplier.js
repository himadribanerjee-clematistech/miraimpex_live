
var productIds = [];
var serviceIds = [];

$(document).ready(function () {
    if (localStorage.getItem("miraimpex_token") === null) {
        $('#localSupplierLoginProfile').html('Login');
        $('#localSupplierLoginProfile').attr('href', $("#base_url").val() + 'login');

    }

    $("html, body").animate({ scrollTop: 0 }, "slow");
    loadProductCategoryList();
});

function loadProductCategoryList() {
    var base_url = $("#base_url").val();
    var values = {};
    values.displayLocal = 1;
    values.type = 1;
    values.level = 3;
    values.lang = $("#viewLang").val();
    values.parent_id = 'MA==';
    values.limit = 3;
    $('#category-menu-product').empty().html('<div class="loading"><img src="' + base_url + 'assets/images/lg.ring-loading-gif.gif" alt="Loading Category"></div>');

    $.ajax({
        url: base_url + "api/main/getCategoryList",
        data: values,
        method: "post",
        success: function (result) {
            if (result.status == 200) {
                console.log(result.result);

                var menuSchema = '';
                var i = 1;
                $.each(result.result, function (i1, v1) {

                    var submenuSchema = '';
                    if (v1.children) {

                        var subcatarr = [];

                        $.each(v1.children, function (i1c, v1c) {
                            //productIds.push(v1c.id);
                            subcatarr.push({
                                image: v1c.image,
                                id: v1c.id,
                                category_name: v1c.category_name,
                            });

                        });

                        productIds.push({
                            type: v1.type,
                            image: v1.image,
                            id: v1.id,
                            category_name: v1.category_name,
                            subcarArr: subcatarr
                        });
                    }
                    i++;
                });
                loadFeaturedSubmenuPanel();
            }
        },
        error: function (result) {
            console.log(result.responseJSON.msg);
        }
    });

}

function interleave(a, b) {
    c = [];
    tot = a.length + b.length;
    while (c.length < tot) {
        if (aa = a.shift()) {
            c.push(aa);
        }
        if (bb = b.shift()) {
            c.push(bb);
        }
    }
    return c;
}

function loadFeaturedSubmenuPanel() {
    if (productIds.length > 0 || serviceIds.length > 0) {

        // console.log(productIds, typeof (productIds), productIds.length);
        $('#featuredSubmenuPanel').empty().html('');

        var productserviceIdsCombined = interleave(productIds, serviceIds);

        // console.log(productserviceIds);
        $.each(productserviceIdsCombined, function (pik, piv) {
            var base_url = $("#base_url").val();
            //console.log(piv);
            var img = base_url + 'admin/assets/images/no-item-image.png';
            if (piv.image != '') {
                img = base_url + 'admin/uploads/category/' + piv.image;
            }

            var divType = '-pro';
            if (piv.type == 1) {
                divType = 'featuredSubmenu-pro';
            } else if (piv.type == 2) {
                divType = 'featuredSubmenu-ser';
            }

            var fetSubmenu = '<div class="product-wrapper ' + divType + ' clearfix">\
            <div class="container">\
                <div class="row">\
                    <div class="col-sm-12">\
                        <div class="product clearfix">\
                            <h4 class="heading">\
                                <span>'+ piv.category_name + '</span>\
                            </h4>\
                            <div class="product-contant clearfix">\
                                <div class="product-banner">\
                                    <a href="' + base_url + 'localsupplier/view/' + piv.id + '">\
                                        <img src="'+ img + '" alt="' + piv.category_name + '"/>\
                                    </a>\
                                    <div class="banner-link">\
                                        <a href="' + base_url + 'localsupplier/view/' + piv.id + '">VIEW ALL</a>\
                                    </div>\
                                </div>\
                                <div class="product-block clearfix">\
                                    <div class="tab-content">\
                                        <div class="tab-pane fade in active" id="submenu-' + piv.category_name + '">\
                                            <div class="item-block clearfix">';
            var xx = 0;
            $.each(piv.subcarArr, function (i1c, v1c) {
                if (xx >= 6) {
                    return false;
                }
                var simg = base_url + 'admin/assets/images/no-item-image.png';
                if (v1c.image != '') {
                    simg = base_url + 'admin/uploads/category/' + v1c.image;
                }
                fetSubmenu += '<div class="product-item">\
                <ul class="products-row">\
                <li class="image-block">\
                <a href="' + base_url + 'localsupplier/view/' + v1c.id + '"><span><img src="' + simg + '" alt="' + v1c.category_name + '"/></span></a>\
                </li>\
                <li class="products-details">\
                <span>' + v1c.category_name + '</span>\
                </li>\
                </ul>\
            </div>';
                xx++;
            });

            fetSubmenu += '</div>\
                                        </div>\
                                    </div>\
                                </div>\
                            </div>\
                        </div>\
                    </div>\
                </div>\
            </div>\
            <div class="spacer30"></div>\
        </div>';
            //console.log(pik);
            $('#featuredSubmenuPanel').append(fetSubmenu);



        });

    }
}