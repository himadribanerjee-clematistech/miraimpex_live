$(document).ready(function () {
    if (localStorage.getItem("miraimpex_token") === null) {
        window.location.replace($("#base_url").val());
    } else {
        getorderdetails();
        $(".cl_details").trigger('click');
    }
});

function changeOrderStatus() {
    var orderStatus = $('#changeOrderStatus').val();
    changeOrderStatusboth(orderStatus);
}

function changeOrderStatusboth(orderStatus) {
    if (orderStatus == "5") {
        if (confirm("Do you want to cancel this order?")) {
            //alert(orderStatus);
            var data = {};
            data.order_id = $('#order_id').val();
            data.order_status = orderStatus;
            $.ajax({
                url: $("#base_url").val() + "api/messagecenter/updateOrderStatus",
                data: data,
                beforeSend: function (xhr) {
                    /* Authorization header */
                    xhr.setRequestHeader("Authorization", localStorage.getItem("miraimpex_token"));
                    xhr.setRequestHeader("X-Mobile", "false");
                },
                method: "post",
                error: function (result) {
                    toastr.clear();
                    toastr.error(result.responseJSON.msg);
                },
                success: function (result) {
                    if (result.status === 200) {
                        getorderdetails();
                    }
                }

            });

        }
    } else {
        var data = {};
        data.order_id = $('#order_id').val();
        data.order_status = orderStatus;
        $.ajax({
            url: $("#base_url").val() + "api/messagecenter/updateOrderStatus",
            data: data,
            beforeSend: function (xhr) {
                /* Authorization header */
                xhr.setRequestHeader("Authorization", localStorage.getItem("miraimpex_token"));
                xhr.setRequestHeader("X-Mobile", "false");
            },
            method: "post",
            error: function (result) {
                toastr.clear();
                toastr.error(result.responseJSON.msg);
            },
            success: function (result) {
                if (result.status === 200) {
                    getorderdetails();
                }
            }

        });
    }


}

function changePaymentStatus() {
    var paymentStatus = $('#changePaymentStatus').val();
    //alert(paymentStatus);
    var data = {};
    data.order_id = $('#order_id').val();
    data.status = paymentStatus;
    $.ajax({
        url: $("#base_url").val() + "api/messagecenter/PaymentReceived",
        data: data,
        beforeSend: function (xhr) {
            /* Authorization header */
            xhr.setRequestHeader("Authorization", localStorage.getItem("miraimpex_token"));
            xhr.setRequestHeader("X-Mobile", "false");
        },
        method: "post",
        error: function (result) {
            toastr.clear();
            toastr.error(result.responseJSON.msg);
        },
        success: function (result) {
            if (result.status === 200) {
                getorderdetails();
            }
        }

    });

}

function getorderdetails() {
    $('.loading').removeClass('hide');
    var total_info = JSON.parse(localStorage.getItem("miraimpex_total_info"));

    var data = {};
    data.order_id = $('#order_id').val();

    $.ajax({
        url: $("#base_url").val() + "api/messagecenter/getOrderDetails",
        data: data,
        beforeSend: function (xhr) {
            /* Authorization header */
            xhr.setRequestHeader("Authorization", localStorage.getItem("miraimpex_token"));
            xhr.setRequestHeader("X-Mobile", "false");
        },
        method: "post",
        error: function (result) {
            //console.log("Unauthorised");
            toastr.clear();
            toastr.error(result.responseJSON.msg);

        },
        success: function (result) {
            console.log(result);
            if (result.status === 200) {

                if (result.order_details.status == "0") {
                    $('#paymentStatus').html('<a href="javascript:void(0);" style="background-color: #fc5b5b; color: #fff; font-size: 14px; padding: 2px 20px;" >Unpaid</a>');
                }
                if (result.order_details.status == "1") {
                    $('#paymentStatus').html('<a href="javascript:void(0);" style="background-color: #008000; color: #fff; font-size: 14px; padding: 2px 20px;" >Paid</a>');
                }
                if (result.order_details.status == "2") {
                    $('#paymentStatus').html('<a href="javascript:void(0);" style="background-color: #f11010; color: #fff; font-size: 14px; padding: 2px 20px;" >Failed</a>');
                }

                $('#changeOrderStatus').val(result.order_details.order_status);
                $('#changePaymentStatus').val(result.order_details.status);

                if (result.order_details.order_status == "0") {
                    $('#orderStat0').addClass('active');
                }
                if (result.order_details.order_status == "1") {
                    $('#orderStat0').addClass('active');
                    $('#orderStat1').addClass('active');
                    $('#orderStat2').removeClass('active');
                    $('#orderStat3').removeClass('active');
                    $('#orderStat4').removeClass('active');
                }
                if (result.order_details.order_status == "2") {
                    $('#orderStat0').addClass('active');
                    $('#orderStat1').addClass('active');
                    $('#orderStat2').addClass('active');
                    $('#orderStat3').removeClass('active');
                    $('#orderStat4').removeClass('active');
                }
                if (result.order_details.order_status == "3") {
                    $('#orderStat0').addClass('active');
                    $('#orderStat1').addClass('active');
                    $('#orderStat2').addClass('active');
                    $('#orderStat3').addClass('active');
                    $('#orderStat4').removeClass('active');
                }
                if (result.order_details.order_status == "4") {
                    $('#orderStat0').addClass('active');
                    $('#orderStat1').addClass('active');
                    $('#orderStat2').addClass('active');
                    $('#orderStat3').addClass('active');
                    $('#orderStat4').addClass('active');
                }
                if (result.order_details.order_status == "5") {
                    $('#orderStat0').addClass('active');
                    $('#orderStat1').removeClass('active');
                    $('#orderStat2').removeClass('active');
                    $('#orderStat3').removeClass('active');
                    $('#orderStat4').addClass('active1').html('<span style="background:red"><i class="fa fa-close" aria-hidden="true"></i></span><p>Order Cancelled</p>');
                }



                if (result.order_details.proposal_id) {
                    var rfqDetails = result.order_details.reference_details;
                    $('#rfqCreateDate').html('Created on: ' + rfqDetails.created_date);

                    $('#transactionId').html(result.order_details.transction_id);

                    var rfqSchema = '';
                    if (rfqDetails.qrname) {
                        rfqSchema += '<div><h5>' + rfqDetails.qrname + '</h5></div>';
                        rfqSchema += '<div><p><b>Category:</b> ' + rfqDetails.category_name + '</p></div>';
                    }
                    if (rfqDetails.message) {
                        rfqSchema += '<div><p><b>Quantity:</b> ' + rfqDetails.quantity + ' ' + rfqDetails.uom_name + '</p><p><b>Message:</b> ' + rfqDetails.message + '</p></div>';
                    }
                    if (rfqDetails.other_requerment) {
                        rfqSchema += '<div><p><b>Other requirement:</b> ' + rfqDetails.other_requerment + '</p></div>';
                    }

                    if (rfqDetails.images) {
                        if (rfqDetails.images.length > 0) {
                            $.each(rfqDetails.images, function (ix, val) {

                                var imgP = '<div class="img_sect"><img src="' + $("#base_url").val() + 'admin/uploads/rfq/' + val.image_name + '" alt=""></div>';
                                rfqSchema += imgP;
                            });
                        }

                    }
                    $('#rfqDetailsInfo').html(rfqSchema);

                    var proposalDetails = result.order_details.proposal_details;

                    $('#viewinvoice').attr('href', $("#base_url").val() + "admin/uploads/proposal/" + proposalDetails.file);
                    $('#proposal_doc_link').attr('href', $("#base_url").val() + "admin/uploads/proposal/" + proposalDetails.file);
                    $('#proposal_title').attr('href', $("#base_url").val() + "admin/uploads/proposal/" + proposalDetails.file);
                    $('#proposal_title').html('<b style="font-weight:bold">' + proposalDetails.title + '</b>');
                    $('#proposal_desc').html(proposalDetails.description);
                    $('#proposal_date').html('<b>' + proposalDetails.created_date + '</b>');
                    $('#purchase_date').html('<b>' + result.order_details.order_date + '</b>');

                    $('#orderPlacedDate').html('Order placed on ' + result.order_details.order_date);

                    var totalToPayGMDprice = proposalDetails.amount * gambianDalasi2USDexchangeRate;
                    totalToPayGMDprice = (Math.round(totalToPayGMDprice * 100) / 100).toFixed(2);
                    $('#approxGMD').html('<p>(Approx. ' + totalToPayGMDprice + ' Gambian Dalassi.)</p>');

                    $('.proposal_amount').html('<b>' + proposalDetails.currency + '' + proposalDetails.amount + '</b>');


                    $('#purchased_from').html(result.order_details.from_business_name);

                    $('#compName').html('<b>Company Name: </b>' + result.order_details.from_business_name);
                    $('#compContName').html('<b>Contact Person: </b>' + result.order_details.from_name);
                    $('#compContNo').html('<b>Contact No: </b>' + result.order_details.from_phone);
                    $('#compEmail').html('<b>Email ID: </b>' + result.order_details.from_email);


                    $('#buyerContName').html('<b>Full Name: </b>' + result.order_details.to_name);
                    $('#buyerContNo').html('<b>Contact No: </b>' + result.order_details.to_phone);
                    $('#buyerEmail').html('<b>Email ID: </b>' + result.order_details.to_email);

                    if (total_info.id == proposalDetails.from_id) {
                        $('#completeOrderPanel').remove();
                        $('#changeOrderStatusPanel').removeClass('d-none');
                    }
                    if (total_info.id == proposalDetails.to_id) {
                        $('#changeOrderStatusPanel').remove();
                        $('#completeOrderPanel').removeClass('d-none');

                        if (result.order_details.order_status == "3") {
                            $('#completeOrderPanel').removeClass('d-none');
                        } else {
                            $('#completeOrderPanel').remove();
                        }
                    }

                    $('#paymentMode').html((result.order_details.payment_mode == "1") ? "Cash On Delivery" : (result.order_details.payment_mode == "2") ? "Bank Transfer (For Gambia)" : (result.order_details.payment_mode == "3") ? "Bank Transfer (For US)" : (result.order_details.payment_mode == "4") ? "Bank Transfer (For Europe)" : "NA");

                } else {

                    // ADD TO CART SECTION

                    if (total_info.id == result.order_details.seller_id) {

                        $('#completeOrderPanel').remove();
                        $('#changeOrderStatusPanel').removeClass('d-none');
                        $('#viewinvoice').html('Attach Invoice');
                    }

                    if (total_info.id == result.order_details.buyer_id) {
                        $('#viewinvoice').html('View Invoice');
                        $('#changeOrderStatusPanel').remove();
                        if (result.order_details.order_status == "3") {
                            $('#completeOrderPanel').removeClass('d-none');
                        } else {
                            $('#completeOrderPanel').remove();
                        }

                        if (result.order_details.order_status <= "2") {
                            $('#cancelOrderPanel').removeClass('d-none');
                        } else {
                            $('#cancelOrderPanel').remove();
                        }

                    }

                    $('#transactionId').html(result.order_details.transction_id);
                    $('#proposal_date').html('<b>' + result.order_details.order_date + '</b>');
                    $('.rfqsection').hide();
                    $('#rfqCreateDate').html('Created on: ' + result.order_details.order_date);
                    $('#orderType').html('Order Details');

                    var addtocart = result.order_details.item_details;
                    var totalToPay = 0;
                    var currency = '';

                    var cartSchema = '<table class="table" style="border: 1px solid lightgray;"><thead style="background: #e2e1e138; color: black;"><tr><th width="45%" style="border-right: 1px solid lightgray;"><center>Product</center></th><th width="18%" style="border-right: 1px solid lightgray;"><center>Price</center></th><th width="20%" style="border-right: 1px solid lightgray;"><center>Quantity</center></th><th width="15%" style="border-right: 1px solid lightgray;"><center>Total</center></th></tr></thead><tbody>';

                    if (addtocart) {
                        $.each(addtocart, function (key, input) {
                            var variance = '';
                            if (input.itemvariance != "") {
                                var addtocartvariance = JSON.parse(input.itemvariance);
                                $.each(addtocartvariance, function (vkey, vinput) {
                                    variance += '<span class="item-variance" style="padding-right:10px;">' + vinput.varianceName + ': ' + vinput.varianceAttribute + '</span>';
                                });
                            }

                            var currencySymbol = '';
                            if (input.currency == 'Dollar' || input.currency == 'USD') {
                                currencySymbol = '$';
                            } else if (input.currency == 'Euro' || input.currency == 'EUR') {
                                currencySymbol = '€';
                            } else {
                                currencySymbol = '$';
                            }

                            currency = currencySymbol;

                            var perItemTotalPrice = 0;

                            perItemTotalPrice += input.quantity * input.perItemPrice;
                            perItemTotalPrice = (Math.round(perItemTotalPrice * 100) / 100).toFixed(2);



                            cartSchema += '<tr><td style="border-right: 1px solid #f1eded;"><div class="row"><div class="col-lg-3"><img src="' + input.itemimg + '" style="width: 100px;"></div><div class="col-lg-9"><h5>' + input.itemname + '</h5><p style="font-size: 12px;">' + variance + '</p></h4></div></div></td><td style="border-right: 1px solid #f1eded;"><p style="text-align: center; font-size:16px; font-weight:600;">' + currencySymbol + '' + input.perItemPrice + '</p></td><td style="border-right: 1px solid #f1eded;"> <div class="input-group" style="padding: 16px; padding-top: 34px;"><div class="qty">' + input.quantity + '</div></div><div style="text-align: center;"></div></td><td style="border-right: 1px solid #f1eded;"><p style="text-align: center; font-size: 20px; font-weight: 600; padding: 34px 34px 0 34px;">' + currencySymbol + '' + perItemTotalPrice + '</p><p style="text-align: center;"><span>incl. all taxes</span></p></td></tr>';

                        });

                        totalToPay = (Math.round(result.order_details.total_amount * 100) / 100).toFixed(2);

                        cartSchema += '</tbody><tfoot style="background: #e2e1e138; color: black;"><tr><td colspan="2"></td><td><div><p style="font-size: 16px; font-weight: 600; color:#000">Total</p></div></td><td style="text-align: center;"><p style="font-size: 16px; font-weight: 600; color:#000">' + currency + ' ' + totalToPay + '</p></td></tr></tfoot>';

                        cartSchema += '</table>';

                        $('#rfqDetailsInfo').html(cartSchema);

                        $('.proposal_amount').html('<b>' + currency + ' ' + totalToPay + '</b>');

                        var totalToPayGMDprice = totalToPay * gambianDalasi2USDexchangeRate;
                        totalToPayGMDprice = (Math.round(totalToPayGMDprice * 100) / 100).toFixed(2);
                        $('#approxGMD').html('<p>(Approx. ' + totalToPayGMDprice + ' Gambian Dalassi.)</p>');


                    }

                    $('#buyerContName').html('<b>Full Name: </b>' + result.order_details.from_business_name);

                    var buyer_info = result.order_details.buyer_info;
                    $.each(buyer_info, function (bk, bv) {
                        if (bv.name == 'firstname') {
                            $('#buyerContNo').html('<b>Delivery Address: </b>' + bv.value);
                        }
                        if (bv.name == 'address') {
                            $('#buyerContNo').append(', ' + bv.value);
                        }

                        if (bv.name == 'city') {
                            $('#buyerContNo').append(', ' + bv.value);
                        }
                        if (bv.name == 'state') {
                            $('#buyerContNo').append(', ' + bv.value);
                        }
                        if (bv.name == 'zip') {
                            $('#buyerContNo').append(', ' + bv.value);
                        }
                        if (bv.name == 'email') {
                            $('#buyerEmail').html('<b>Email: </b>' + bv.value);
                        }
                        if (bv.name == 'contactnumber1') {
                            $('#buyercontactnumber1').html('<b>Contact No: </b>' + bv.value);
                        }
                        if (bv.name == 'contactnumber2') {
                            // $('#buyercontactnumber2').html('<b>Contact No(optional): </b>'+bv.value);
                        }
                    });

                    $('#compName').html('<b>Company Name: </b>' + result.order_details.seller_business_name);
                    $('#compContName').html('<b>Contact Person: </b>' + result.order_details.seller_name);
                    $('#compContNo').html('<b>Contact No: </b>' + result.order_details.seller_phone);
                    $('#compEmail').html('<b>Email ID: </b>' + result.order_details.seller_email);

                    $('#paymentMode').html((result.order_details.payment_mode == "1") ? "Cash On Delivery" : (result.order_details.payment_mode == "2") ? "Bank Transfer (For Gambia)" : (result.order_details.payment_mode == "3") ? "Bank Transfer (For US)" : (result.order_details.payment_mode == "4") ? "Bank Transfer (For Europe)" : "NA");

                    if (result.order_details.payment_mode == "2") {
                        $("#bankpay").slideDown("slow", function () { });
                    }

                }


            }
            $('.loading').addClass('hide');
        }
    });
}
$(".cl_activity").click(function () {
    $(".cl_activity").addClass("active");
    $(".cl_details").removeClass("active");
    $("#Activity").show();
    $("#order_details").hide();
});
$(".cl_details").click(function () {
    $(".cl_details").addClass("active");
    $(".cl_activity").removeClass("active");
    $("#Activity").hide();
    $("#order_details").show();
});