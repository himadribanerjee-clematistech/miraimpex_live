$(document).ready(function () {
    $("html, body").animate({ scrollTop: 0 }, "slow");

    loadCategoryDetails();
    loadItemsUnderCategory();

});


function loadCategoryDetails() {
    var base_url = $("#base_url").val();
    var values = {};
    values.level = 1;
    values.lang = $("#viewLang").val();
    values.parent_id = $("#viewCatId").val();

    $.ajax({

        url: base_url + "api/main/getCategoryList",
        data: values,
        method: "post",
        success: function (result) {
            if (result.status == 200) {
                //alert(result.result.length)
                if (result.result.length > 0) {

                    //console.log(result.result);
                    var sliderHtml = '';
                    $.each(result.result, function (i1, v1) {
                        var img = base_url + 'admin/assets/images/no-item-image.png';
                        if (v1.image != '') {
                            img = base_url + 'admin/uploads/category/' + v1.image;
                        }

                        sliderHtml += '<div class="item"><a href="' + base_url + 'category/view/' + v1.id + '"><div class="category_slider_img"><img src="' + img + '" alt="' + v1.category_name + '" title="' + v1.category_name + '"/></div><div class="category_slider_info"><h4>' + v1.category_name + '</h4></div></a></div>';


                    })

                    setTimeout(function () {
                        $('#sliderCategory').empty().html(sliderHtml).owlCarousel({
                            items: 10,
                            margin: 10,
                            itemsDesktop: [1200, 3],
                            itemsDesktopSmall: [1199, 3],
                            itemsTablet: [950, 3],
                            itemsTabletSmall: [767, 3],
                            itemsMobile: [479, 2],
                            slideSpeed: 600,
                            autoPlay: 3000,
                            stopOnHover: true,
                            navigation: true,
                            pagination: false,
                            responsive: true,
                            autoHeight: true,
                            navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"]
                        });
                    }, 500);

                } else {
                    $('#sliderCategoryPanel').remove();
                }

            } else {
                $('#sliderCategoryPanel').remove();
            }
        },
        error: function (result) {
            console.log(result.responseJSON.msg);
        }
    });

}

function viewLocalSupplierOnly(obj) {
    var base_url = $("#base_url").val();
    if ($('#localsupplier').is(":checked")) {
        $("#enableLocalSupplier").val("1");
    }
    else if ($('#localsupplier').is(":not(:checked)")) {
        $("#enableLocalSupplier").val("0");
    }

    $('#categoryitemsContent').fadeOut(200, function () {
        $('#categoryitemsContent').html('<div class="loading"><img src="' + base_url + 'assets/images/lg.ring-loading-gif.gif" alt="Loading Items"></div>').show();
        setTimeout(function () {
            loadItemsUnderCategory();
        }, 500);

    });

}

function loadItemsUnderCategory() {
    var base_url = $("#base_url").val();
    var values = {};
    values.type = 1;
    values.lang = $("#viewLang").val();
    values.category_id = $("#viewCatId").val();
    values.localSupplier = $("#enableLocalSupplier").val();

    $.ajax({

        url: base_url + "api/item/getitem",
        data: values,
        method: "post",
        success: function (result) {
            if (result.status == 200) {
                //alert(result.result.length)
                if (result.item.length > 0) {

                    //console.log(result.item);

                    $('#categoryitemsContent').empty();

                    $.each(result.item, function (i1, v1) {
                        var img = base_url + 'admin/assets/images/no-item-image.png';

                        if (v1.thumbnail != "") {
                            img = base_url + 'admin/uploads/selleritem/' + v1.thumbnail;

                        }

                        var attribute_variance_html = '';

                        if (v1.modified_variance != "") {
                            var attribute_variance = jQuery.parseJSON(v1.modified_variance);
                            attribute_variance_html += '<div class="attribute_variance_panel"><div class="product-info viewCate_attri attribute_variance"><div class="attribute clearfix">';
                            $.each(attribute_variance, function (i3, v3) {
                                attribute_variance_html += '<div><fieldset class="attribute_fieldset"><label class="attribute_label">' + i3 + ':</label><div class="attribute_list"><ul class="attribute_size">';
                                $.each(v3, function (v3k, v3v) {
                                    var colorHexCode = colorconvert(v3v);
                                    if (colorHexCode) {
                                        attribute_variance_html += '<li style="background-color:' + colorHexCode + '; width:25px; height:25px;" title="' + v3v + '"></li>';

                                    } else {
                                        attribute_variance_html += '<li>' + v3v + '</li>';

                                    }


                                })

                                attribute_variance_html += ' </ul></div></fieldset></div>';
                            });
                            attribute_variance_html += '</div></div></div>';
                        }

                        var currencySymbol = '';
                        if (v1.currency == 'Dollar') {
                            currencySymbol = '$';
                        } else if (v1.currency == 'Euro') {
                            currencySymbol = '€';
                        } else {
                            currencySymbol = '$';
                        }

                        var tracelog = base_url + 'contactsupplier?seller=' + v1.seller_id + '&item=' + v1.item_id;

                        if (localStorage.getItem("miraimpex_token") === null) {
                            tracelog = base_url + 'login?tracelog=contactsupplier&seller=' + v1.seller_id + '&item=' + v1.item_id;
                        }

                        var productHtml = '<div class="col-sm-3 categoryitems"><div class="itempanel"><div class="imagepanel"><a href="' + base_url + 'product/details/' + v1.item_id + '" title="View Item"><img src="' + img + '" alt="img-1"/></a></div><div class="infopanel"><a href="' + base_url + 'product/details/' + v1.item_id + '" title="View Item"><div class="other_cate_item_titel"><h4>' + v1.item_name + '</h4></div></a><div class="other_cate_item_price"><p>Price: <span> ' + currencySymbol + '' + v1.price_min + ' - ' + currencySymbol + '' + v1.price_max + ' /' + v1.price_uom_name_en + ' </span></p><p>MOQ:<span> ' + v1.moq + ' ' + v1.moq_uom_name_en + '</span></p></div>' + attribute_variance_html + '<div class="sellerlocation"><h5><i class="fa fa-map-marker"></i>  ' + v1.sellerinfo.state + ', ' + v1.sellerinfo.country + '</h5></div></div><div class="contactsellerpanel"><div class="other_cate_item_supplier"><a href="' + base_url + 'supplier/details/' + v1.seller_id + '" title="View supplier details"><p>Supplier: <span> ' + v1.sellerinfo.business_name + '</span></p></a></div><a href="' + tracelog + '" title="Contact Supplier"><div class="other_cate_item_contact"><h5><i class="fa fa-envelope"></i> Get Latest Price</h5></div></a></div></div></div>';

                        $('#categoryitemsContent').append(productHtml);

                    });


                } else {
                    // No item found
                    $('#categoryitemsContent').empty().html('<div class=""><h2 style="text-align:center;">Product coming soon...</h2></div>');
                }

            } else {
                // Something went wrong
            }
        },
        error: function (result) {
            console.log(result.responseJSON.msg);
        }


    });

}


