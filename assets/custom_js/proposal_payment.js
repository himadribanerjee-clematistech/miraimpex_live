$(document).ready(function () {
    getproposaldetails();

    $("#send_checkoutForm").submit(function (e) {
        e.preventDefault();
        toastr.clear();
        var submit = true;
        if ($.trim($('#fname').val()) == '') {
            toastr.error('Full name is blank!');
            submit = false;
            $('#fname').focus();
            return false;
        }

        if ($.trim($('#email').val()) == '') {
            toastr.error('Email is blank!');
            submit = false;
            $('#email').focus();
            return false;
        }

        if ($.trim($('#cname').val()) == '') {
            toastr.error('Name on card is blank!');
            submit = false;
            $('#cname').focus();
            return false;
        }

        if ($.trim($('#adr').val()) == '') {
            toastr.error('Address is blank!');
            submit = false;
            $('#adr').focus();
            return false;
        }

        if ($.trim($('#ccnum').val()) == '') {
            toastr.error('Card number is blank!');
            submit = false;
            $('#ccnum').focus();
            return false;
        }

        if ($.trim($('#city').val()) == '') {
            toastr.error('City is blank!');
            submit = false;
            $('#city').focus();
            return false;
        }

        if ($.trim($('#expmonth').val()) == '') {
            toastr.error('Expiry month is blank!');
            submit = false;
            $('#expmonth').focus();
            return false;
        }

        if ($.trim($('#state').val()) == '') {
            toastr.error('State is blank!');
            submit = false;
            $('#state').focus();
            return false;
        }

        if ($.trim($('#zip').val()) == '') {
            toastr.error('Zip is blank!');
            submit = false;
            $('#zip').focus();
            return false;
        }

        if ($.trim($('#expyear').val()) == '') {
            toastr.error('expyear is blank!');
            submit = false;
            $('#expyear').focus();
            return false;
        }

        if ($.trim($('#cvv').val()) == '') {
            toastr.error('cvv is blank!');
            submit = false;
            $('#cvv').focus();
            return false;
        }

        if (submit) {
            $("#confirmPayButton").val("Processing Payment... Please wait");
            $("#confirmPayButton").attr("disabled", true);
            var base_url = $("#base_url").val();
            var values = {};
            values.proposal_id = $("#proposal_id").val();
            values.buyer_info = JSON.stringify($('#send_checkoutForm').serializeArray());
            $.ajax({
                url: base_url + "api/messagecenter/processPayment",
                data: values,
                beforeSend: function (xhr) {
                    /* Authorization header */
                    xhr.setRequestHeader("Authorization", localStorage.getItem("miraimpex_token"));
                    xhr.setRequestHeader("X-Mobile", "false");
                },
                method: "post",
                success: function (result) {
                    //console.log(result);
                    if (result.status == 200) {

                        var proposal_order_id = result.order_id;
                        confirmPayment(proposal_order_id);


                    } else {
                        // Something went wrong
                    }


                },
                error: function (result) {
                    console.log(result.responseJSON.msg);
                }
            });

        }

    });

});

confirmPayment(proposal_order_id)
{
    var base_url = $("#base_url").val();
    var values = {};
    values.order_id = proposal_order_id;
    values.status = '1';
    values.transction_id = '121323732732';

    $.ajax({
        url: base_url + "api/messagecenter/confirmPayment",
        data: values,
        beforeSend: function (xhr) {
            /* Authorization header */
            xhr.setRequestHeader("Authorization", localStorage.getItem("miraimpex_token"));
            xhr.setRequestHeader("X-Mobile", "false");
        },
        method: "post",
        success: function (result) {
            //console.log(result);
            if (result.status == 200) {

                toastr.clear();
                toastr.success(result.msg);


            } else {
                // Something went wrong
                toastr.clear();
                toastr.error(result.msg);
            }


        },
        error: function (result) {
            console.log(result.responseJSON.msg);
        }
    });
}

function getproposaldetails() {
    var base_url = $("#base_url").val();
    var values = {};
    values.proposal_id = $("#proposal_id").val();
    $.ajax({
        url: base_url + "api/messagecenter/getProposalDetails",
        data: values,
        beforeSend: function (xhr) {
            /* Authorization header */
            xhr.setRequestHeader("Authorization", localStorage.getItem("miraimpex_token"));
            xhr.setRequestHeader("X-Mobile", "false");
        },
        method: "post",
        success: function (result) {
            //console.log(result);
            if (result.status == 200) {

                var proposalDetails = result.proposal_data;

                $('#proposal_doc_link').attr('href', $("#base_url").val() + "admin/uploads/proposal/" + proposalDetails.file);
                $('#proposal_title').attr('href', $("#base_url").val() + "admin/uploads/proposal/" + proposalDetails.file);
                $('#proposal_title').html('<b style="font-weight:bold">' + proposalDetails.title + '</b>');
                $('#proposal_desc').html(proposalDetails.description);
                $('#proposal_date').html('<b>' + proposalDetails.created_date + '</b>');
                $('#proposal_amount').html('<b>' + proposalDetails.currency + ' ' + proposalDetails.amount + '</b>');

            } else {
                // Something went wrong
            }


        },
        error: function (result) {
            console.log(result.responseJSON.msg);
        }
    });

}