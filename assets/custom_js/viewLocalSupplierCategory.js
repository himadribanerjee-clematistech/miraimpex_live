var cartArr = [];
var cartSellerId = localStorage.getItem("miraimpex_cartSellerId");

var productSearchTimeout = null;

if (localStorage.getItem("miraimpex_token") === null) {
    cartArr = [];
} else {
    if (localStorage.getItem("miraimpex_addtocart") === null) {
        cartArr = [];
    } else {
        cartArr = JSON.parse(localStorage.getItem("miraimpex_addtocart"));
    }

}

//console.log(typeof (cartArr));

$(document).ready(function () {
    $("html, body").animate({ scrollTop: 0 }, "slow");

    loadCategoryDetails();
    loadItemsUnderCategory();


    $('#searchq').keyup(function () {
        var base_url = $("#base_url").val();
        var key = $('#searchq').val();
        $('#catSearchloading').removeClass('d-none');
        $('#searchButt').html('<i class="fa fa-hourglass-half"></i>');
        if (productSearchTimeout !== null) {
            clearTimeout(productSearchTimeout);
        }
        productSearchTimeout = setTimeout(function () {
            $('#searchButt').html('<i class="fa fa-hourglass-end"></i>');
            loadItemsUnderCategory();

        }, 1000);





    });

});


var varianceArr = [];




function getObjectIndex(arr, varianceName) {
    // Loop over array to find the matching element/object
    for (var i = 0; i < arr.length; i++) {
        var obj = arr[i];
        if (obj.varianceName === varianceName) {
            // When all key-value matches return the array index
            return i;
        }
    }
    // When no match found, return false
    return false;
}

function getCartObjectIndex(arr, itemid) {
    // Loop over array to find the matching element/object
    for (var i = 0; i < arr.length; i++) {
        var obj = arr[i];

        if (obj.itemid === itemid) {
            // When all key-value matches return the array index
            return i;
        }
    }
    // When no match found, return false
    return false;
}

function getvariance(obj) {
    var varianceName = $(obj).attr('relparent');
    var varianceAttribute = $(obj).attr('relval');

    var targetid = $(obj).attr('targetid');
    //alert(varianceName+'-'+varianceAttribute);

    var index = getObjectIndex(varianceArr, varianceName);

    // If object found in the array
    if (index !== false) {
        // Update the quantity in the same element
        varianceArr[index].varianceName = varianceName;
        varianceArr[index].varianceAttribute = varianceAttribute;
    } else {
        // Add the element in the array
        varianceArr.push({
            varianceName: varianceName,
            varianceAttribute: varianceAttribute
        });
    }

    $(obj).closest('ul').children().removeClass('varianceActive');
    $(obj).closest('ul li').addClass('varianceActive');

    var xt = JSON.stringify(varianceArr);

    $("#" + targetid).val(xt);
    //console.log($("#" + targetid));
    //console.log(xt);

}

function addtocart(obj) {
    $('.noitemincart').remove();
    var itemvariance = $(obj).attr('itemvariance');
    var itemid = $(obj).attr('itemid');
    var varianceExists = $(obj).attr('varianceExists');

    var fixedPrice = $(obj).attr('fixedPrice');
    var currency = $(obj).attr('currency');
    var itemname = $(obj).attr('itemname');
    var itemimg = $(obj).attr('itemimg');
    var moq = $(obj).attr('moq');
    var uom = $(obj).attr('uom');
    var seller_id = $(obj).attr('seller_id');
    var business_name = $(obj).attr('business_name');

    var itemvarianceJs = $("#" + itemvariance).val();
    itemvarianceJs = itemvarianceJs.replace(/'/g, '"');

    if ($("#" + itemvariance).val() == '' && varianceExists == 'Y') {
        toastr.clear();
        toastr.error('Please select variance!');
    } else {
        if ((cartSellerId === null) || (cartSellerId != null && cartSellerId == seller_id)) {
            cartSellerId = seller_id;
            localStorage.setItem("miraimpex_cartSellerId", seller_id);
            var cartindex = getCartObjectIndex(cartArr, itemid, seller_id);
            // If object found in the array
            if (cartindex !== false) {
                // Update the quantity in the same element
                cartArr[cartindex].itemid = itemid;
                cartArr[cartindex].itemvariance = itemvarianceJs;
                cartArr[cartindex].perItemPrice = fixedPrice;
                cartArr[cartindex].currency = currency;
                cartArr[cartindex].itemname = itemname;
                cartArr[cartindex].itemimg = itemimg;
                cartArr[cartindex].quantity = moq;
                cartArr[cartindex].moq = moq;
                cartArr[cartindex].uom = uom;
                cartArr[cartindex].seller_id = seller_id;
                cartArr[cartindex].business_name = business_name;

                toastr.clear();
                toastr.success('Item updated successfully to cart!');
            } else {
                // Add the element in the array
                cartArr.push({
                    itemid: itemid,
                    itemvariance: itemvarianceJs,
                    perItemPrice: fixedPrice,
                    currency: currency,
                    itemname: itemname,
                    itemimg: itemimg,
                    quantity: moq,
                    moq: moq,
                    seller_id: seller_id,
                    business_name: business_name,
                    uom: uom
                });

                toastr.clear();
                toastr.success('Item added successfully to cart!');
            }
            //console.log(cartArr);
            localStorage.setItem("miraimpex_addtocart", JSON.stringify(cartArr));
            setTimeout(
                function () {

                    processAddToCart();

                }, 1000);
        } else {
            toastr.clear();
            toastr.error('You can\'t add item from different supplier!');
        }


    }

}


function loadCategoryDetails() {
    var base_url = $("#base_url").val();
    var values = {};
    values.level = 1;
    values.lang = $("#viewLang").val();
    values.parent_id = $("#viewCatId").val();

    $.ajax({

        url: base_url + "api/main/getCategoryList",
        data: values,
        method: "post",
        success: function (result) {
            if (result.status == 200) {
                //alert(result.result.length)
                if (result.result.length > 0) {

                    //console.log(result.result);
                    var sliderHtml = '';
                    $.each(result.result, function (i1, v1) {
                        var img = base_url + 'admin/assets/images/no-item-image.png';
                        if (v1.image != '') {
                            img = base_url + 'admin/uploads/category/' + v1.image;
                        }

                        sliderHtml += '<div class="item"><a href="' + base_url + 'localsupplier/view/' + v1.id + '"><div class="category_slider_img"><img src="' + img + '" alt="' + v1.category_name + '" title="' + v1.category_name + '"/></div><div class="category_slider_info"><h4>' + v1.category_name + '</h4></div></a></div>';


                    })

                    setTimeout(function () {
                        $('#sliderCategory').empty().html(sliderHtml).owlCarousel({
                            items: 10,
                            margin: 10,
                            itemsDesktop: [1200, 3],
                            itemsDesktopSmall: [1199, 3],
                            itemsTablet: [950, 3],
                            itemsTabletSmall: [767, 3],
                            itemsMobile: [479, 2],
                            slideSpeed: 600,
                            autoPlay: 3000,
                            stopOnHover: true,
                            navigation: true,
                            pagination: false,
                            responsive: true,
                            autoHeight: true,
                            navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"]
                        });
                    }, 500);

                } else {
                    $('#sliderCategoryPanel').remove();
                }

            } else {
                $('#sliderCategoryPanel').remove();
            }
        },
        error: function (result) {
            console.log(result.responseJSON.msg);
        }
    });

}

function viewLocalSupplierOnly(obj) {
    var base_url = $("#base_url").val();
    if ($('#localsupplier').is(":checked")) {
        $("#enableLocalSupplier").val("1");
    }
    else if ($('#localsupplier').is(":not(:checked)")) {
        $("#enableLocalSupplier").val("0");
    }

    $('#categoryitemsContent').fadeOut(200, function () {
        $('#categoryitemsContent').html('<div class="loading"><img src="' + base_url + 'assets/images/lg.ring-loading-gif.gif" alt="Loading Items"></div>').show();
        setTimeout(function () {
            loadItemsUnderCategory();
        }, 500);

    });

}

function loadItemsUnderCategory() {
    var base_url = $("#base_url").val();
    var values = {};
    values.type = 1;
    values.lang = $("#viewLang").val();
    values.category_id = $("#viewCatId").val();
    values.localSupplier = $("#enableLocalSupplier").val();
    values.displayLocal = 1;
    values.search_keyword = $("#searchq").val();

    $.ajax({

        url: base_url + "api/item/getitem",
        data: values,
        method: "post",
        success: function (result) {
            if (result.status == 200) {
                //alert(result.result.length)
                if (result.item.length > 0) {

                    //console.log(result.item);

                    $('#categoryitemsContent').empty();

                    $.each(result.item, function (i1, v1) {
                        var img = base_url + 'admin/assets/images/no-item-image.png';

                        if (v1.thumbnail != "") {
                            img = base_url + 'admin/uploads/selleritem/' + v1.thumbnail;

                        }

                        var attribute_variance_html = '';
                        var varianceExists = 'N';

                        var predefvarianceArr = [];
                        var pcount = 0;
                        var subcount = 0;
                        if (v1.modified_variance != "") {
                            var attribute_variance = jQuery.parseJSON(v1.modified_variance);
                            attribute_variance_html += '<div class="attribute_variance_panel"><div class="product-info viewCate_attri attribute_variance"><div class="attribute clearfix">';
                            $.each(attribute_variance, function (i3, v3) {
                                attribute_variance_html += '<div><fieldset class="attribute_fieldset"><label class="attribute_label">' + i3 + ':</label><div class="attribute_list"><ul class="attribute_size">';
                                $.each(v3, function (v3k, v3v) {
                                    var colorHexCode = colorconvert(v3v);
                                    if (colorHexCode) {
                                        attribute_variance_html += '<li style="background-color:' + colorHexCode + '; width:25px; height:25px;" title="' + v3v + '"><a href="javascript:void(0);" onclick="javascript:getvariance(this)" relparent="' + i3 + '" relval="' + v3v + '" targetid="itemVariance' + v1.sl + '"></a></li>';

                                    } else {
                                        attribute_variance_html += '<li><a href="javascript:void(0);" onclick="javascript:getvariance(this)" relparent="' + i3 + '" relval="' + v3v + '" targetid="itemVariance' + v1.sl + '">' + v3v + '</a></li>';

                                    }

                                    if (pcount == 0) {
                                        predefvarianceArr.push({
                                            varianceName: i3,
                                            varianceAttribute: v3v
                                        });
                                    }


                                    varianceExists = 'Y';
                                    pcount++;
                                    subcount++;
                                })

                                attribute_variance_html += ' </ul></div></fieldset></div>';
                                pcount = 0;
                            });
                            attribute_variance_html += '</div></div></div>';
                        }
                        // var predefvariance = JSON.stringify(predefvarianceArr).replace(/\\"/g, '"');

                        var predefvariance = JSON.stringify(predefvarianceArr);
                        predefvariance = predefvariance.replace(/"/g, "'");

                        var currencySymbol = '';
                        if (v1.currency == 'Dollar' || v1.currency == 'USD') {
                            currencySymbol = '$';
                        } else if (v1.currency == 'Euro' || v1.currency == 'EUR') {
                            currencySymbol = '€';
                        } else {
                            currencySymbol = '$';
                        }

                        if (predefvariance == '[]') {
                            predefvariance = '';
                        }

                        var productHtml = '<div class="col-sm-3 categoryitems"><div class="itempanel"><div class="imagepanel"><a href="' + base_url + 'product/details/' + v1.item_id + '" title="View Item"><img src="' + img + '" alt="img-1"/></a></div><div class="infopanel"><a href="' + base_url + 'product/details/' + v1.item_id + '" title="View Item"><div class="other_cate_item_titel"><h4>' + v1.item_name + '</h4></div></a><div class="other_cate_item_price"><p>Price: <span> ' + currencySymbol + '' + v1.fixedPrice + ' /' + v1.price_uom_name_en + ' </span></p><p>MOQ:<span> ' + v1.moq + ' ' + v1.moq_uom_name_en + '</span></p></div>' + attribute_variance_html + '<div class="sellerlocation"><h5><i class="fa fa-map-marker"></i>  ' + v1.sellerinfo.city + ', ' + v1.sellerinfo.country + '</h5></div></div><div class="contactsellerpanel"><div class="other_cate_item_supplier"><a href="' + base_url + 'supplier/details/' + v1.seller_id + '" title="View supplier details"><p>Supplier: <span> ' + v1.sellerinfo.business_name + '</span></p></a></div><a href="javascript:void(0);" onclick="javascript:addtocart(this);" itemid="' + v1.item_id + '" itemvariance="itemVariance' + v1.sl + '" varianceExists="' + varianceExists + '" seller_id="' + v1.seller_id + '" business_name="' + v1.sellerinfo.business_name + '" fixedPrice="' + v1.fixedPrice + '" currency="' + v1.currency + '" itemname="' + v1.item_name + '" moq="' + v1.moq + '" uom="' + v1.moq_uom_name_en + '" itemimg="' + img + '" title="Add To Cart"><div class="other_cate_item_contact"><h5><i class="fa fa-shopping-cart"></i> Add To Cart</h5><input type="hidden" id="itemVariance' + v1.sl + '" name="addtocartvariance" value="' + predefvariance + '"></div></a></div></div></div>';

                        $('#categoryitemsContent').append(productHtml);
                        $('#searchButt').html('<i class="fa fa-search"></i>');
                    });


                } else {
                    // No item found
                    $('#categoryitemsContent').empty().html('<div class=""><h2 style="text-align:center;">Product coming soon...</h2></div>');
                }

            } else {
                // Something went wrong
            }
        },
        error: function (result) {
            console.log(result.responseJSON.msg);
        }


    });

}


