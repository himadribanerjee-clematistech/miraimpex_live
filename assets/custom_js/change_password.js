$(document).ready(function () {

    if (localStorage.getItem("miraimpex_token") === null) {
        window.location.replace($("#base_url").val());
    }
    if (localStorage.getItem("miraimpex_isLoggedIn") == 'Y') {
        var total_info = JSON.parse(localStorage.getItem("miraimpex_total_info"));

        if (total_info.email_verify == "0") {
            $('#buyerEmail').html(total_info.email);

        } else {
            $('#buyerEmail').html(total_info.email + ' ' + '<a href="javascript:void(0);">[Verified]</a>');
        }
    }

    $("#passchange_otp").keypress(function (e) {
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            //display error message
            toastr.clear();
            toastr.error('Digits only!');
            return false;
        }
    });

    $("#updatePasswordForm").submit(function (e) {
        e.preventDefault();
        $('#changePasswordSubmit').prop("disabled", true);
        if (checkPasswordForm()) {
            //alert('success');
            var data = {};
            data.otp = $('#passchange_otp').val();
            data.new_password = $('#passchange_new_password').val();
            data.old_password = $('#passchange_old_password').val();
            data.confirm_password = $('#passchange_confirm_password').val();

            $.ajax({
                url: $("#base_url").val() + "api/login/doChangePassword",
                data: data,
                method: "post",
                beforeSend: function (xhr) {
                    /* Authorization header */
                    xhr.setRequestHeader("Authorization", localStorage.getItem("miraimpex_token"));
                    xhr.setRequestHeader("X-Mobile", "false");
                },
                error: function (result) {
                    toastr.clear();
                    toastr.error(result.responseJSON.msg);
                    $('#changePasswordSubmit').prop("disabled", false);
                },
                success: function (result) {
                    //console.log(result);
                    if (result.status === 200) {
                        toastr.clear();
                        toastr.success(result.msg);
                        logoutCustomer();
                        setTimeout(
                            function () {


                                window.location.replace($("#base_url").val());

                            }, 1000);


                    } else {

                        $('#changePasswordSubmit').prop("disabled", false);
                        console.log(result.status + ':/changePassword');
                    }

                }
            });
        }
    });

});

function checkPasswordForm() {

    var passOTP = $('#passchange_otp').val();
    var old_password = $('#passchange_old_password').val();
    var new_password = $('#passchange_new_password').val();
    var confirm_password = $('#passchange_confirm_password').val();

    if (passOTP.length < 4) {
        $('#changePasswordSubmit').prop("disabled", false);
        toastr.clear();
        toastr.error('Invalid OTP!');
        $('#passchange_otp').focus();
        return false;
    }
    if ($.trim(old_password) == '') {
        toastr.error('Old password is empty!');
        $('#passchange_old_password').focus();
        return false;
    }
    if ($.trim(new_password) == '') {
        toastr.error('New password is empty!');
        $('#passchange_new_password').focus();
        return false;
    }
    if ($.trim(confirm_password) == '') {
        toastr.error('Confirm password is empty!');
        $('#passchange_confirm_password').focus();
        return false;
    }
    if ($.trim(new_password) != $.trim(confirm_password)) {
        toastr.error('Password did not match!');
        $('#passchange_confirm_password').focus();
        return false;
    }


    return true;
}


function sendOtpForPasswordChange() {
    if (localStorage.getItem("token") === null) {
        toastr.clear();
        toastr.error('Something went wrong! Please login.');

    } else {
        $('#buttonOTP').prop("disabled", true);

        var data = {};

        $.ajax({
            url: $("#base_url").val() + "api/login/doSendOTPchangePassword",
            data: data,
            method: "post",
            beforeSend: function (xhr) {
                /* Authorization header */
                xhr.setRequestHeader("Authorization", localStorage.getItem("miraimpex_token"));
                xhr.setRequestHeader("X-Mobile", "false");
            },
            error: function (result) {
                console.log(result);
                toastr.clear();
                toastr.error(result.responseJSON.message);
                $('#buttonOTP').prop("disabled", false);
            },
            success: function (result) {
                //console.log(result);
                if (result.status === 200) {
                    toastr.clear();
                    toastr.success(result.msg);
                    $('#buttonOTP').prop("disabled", false);
                    $('.changePassword').removeClass('d-none');


                } else {
                    $('#buttonOTP').prop("disabled", false);
                    console.log(result.status + ':doSendOTP');
                }

            }
        });
    }
}