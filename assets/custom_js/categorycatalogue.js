$(document).ready(function () {
    $("html, body").animate({ scrollTop: 0 }, "slow");
    loadCategoryDetails();
});

function loadCategoryDetails() {
    var base_url = $("#base_url").val();
    var values = {};
    values.level = 1;
    values.lang = $("#viewLang").val();
    values.type = $("#typeToken").val();
    values.parent_id = 'MA==';

    $.ajax({

        url: base_url + "api/main/getCategoryList",
        data: values,
        method: "post",
        success: function (result) {
            if (result.status == 200) {
                console.log(result.result);
                //alert(result.result.length)
                if (result.result.length > 0) {


                    var sliderHtml = '';
                    $.each(result.result, function (i1, v1) {
                        var img = base_url + 'admin/assets/images/no-item-image.png';
                        if (v1.image != '') {
                            img = base_url + 'admin/uploads/category/' + v1.image;
                        }

                        sliderHtml += '<div class="item"><a href="' + base_url + 'supplier/view/' + v1.id + '"><div class="category_slider_img"><img src="' + img + '" alt="' + v1.category_name + '" title="' + v1.category_name + '"/></div><div class="category_slider_info"><h4>' + v1.category_name + '</h4></div></a></div>';


                    })

                    setTimeout(function () {
                        $('#sliderCategory').empty().html(sliderHtml).owlCarousel({
                            items: 10,
                            margin: 10,
                            itemsDesktop: [1200, 3],
                            itemsDesktopSmall: [1199, 3],
                            itemsTablet: [950, 3],
                            itemsTabletSmall: [767, 3],
                            itemsMobile: [479, 2],
                            slideSpeed: 600,
                            autoPlay: 3000,
                            stopOnHover: true,
                            navigation: true,
                            pagination: false,
                            responsive: true,
                            autoHeight: true,
                            navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"]
                        });
                    }, 500);

                } else {
                    $('#sliderCategoryPanel').remove();
                }

            } else {
                $('#sliderCategoryPanel').remove();
            }
        },
        error: function (result) {
            console.log(result.responseJSON.msg);
        }
    });

}