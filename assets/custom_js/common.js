var gambianDalasi2USDexchangeRate = localStorage.getItem("miraimpex_exchangeRate");

if (gambianDalasi2USDexchangeRate === null) {
  gambianDalasi2USDexchangeRate = 50.9413;
}

$(document).ready(function () {

  /* if (performance.navigation.type == 2) {
    location.reload(true);
  } */
  $("html, body").animate({ scrollTop: 0 }, "slow");

  var tracelogUrl = '';

  if (localStorage.getItem("miraimpex_token") === null) {

    var tracelog = window.location.href;
    if (tracelog.indexOf("tracelog") >= 0) {
      /*tracelogUrl = tracelog.substring(tracelog.lastIndexOf('/'));
      tracelogUrl = tracelogUrl.replace('/', '');
      $('#tracelogUrl').val(tracelogUrl); */
    } else {
      tracelogUrl = tracelog;
      tracelogUrl = tracelogUrl.replace($("#base_url").val(), '');
      if (tracelogUrl != '') {
        $('.signInButtonLink').attr('href', $("#base_url").val() + 'login?' + tracelogUrl);

      }

    }

    $('.linkrfq').attr('href', $("#base_url").val() + 'login?tracelog=requestforquotations');
    $('#linkrfqform').attr('action', $("#base_url").val() + 'login?tracelog=requestforquotations');

    removeLocalStore();
  }
  else {
    $('#comingsoonsignup').remove();
    doAuthenticateMe();
  }
  processAddToCart();
});

function processAddToCart() {
  if (localStorage.getItem("miraimpex_addtocart") === null) {
    $('.crttt').empty();
    $('.crttt').append('<li class="noitemincart">Your Cart is empty. Please add items that you would like to buy to your cart</li>');
    $('.chckout').html('');
  } else {
    var base_url = $("#base_url").val();
    $('.crttt').empty();
    $('.chckout').html('');
    var addtocart = JSON.parse(localStorage.getItem("miraimpex_addtocart"));
    if (addtocart.length == 0) {
      localStorage.removeItem("miraimpex_addtocart");
      localStorage.removeItem("miraimpex_cartSellerId");
    }
    if (addtocart) {
      var cartTotal = 0;
      $.each(addtocart, function (key, input) {
        var variance = '';
        if (input.itemvariance != "") {
          var addtocartvariance = JSON.parse(input.itemvariance);
          $.each(addtocartvariance, function (vkey, vinput) {
            variance += '<span class="item-variance">' + vinput.varianceName + ': ' + vinput.varianceAttribute + '</span>, ';
          });
        }
        var currencySymbol = '';
        if (input.currency == 'Dollar' || input.currency == 'USD') {
          currencySymbol = '$';
        } else if (input.currency == 'Euro' || input.currency == 'EUR') {
          currencySymbol = '€';
        } else {
          currencySymbol = '$';
        }

        var cartSchema = '<li><div class="col-sm-4"><img src="' + input.itemimg + '" style="float:left;margin-right: 5px;" alt="item" /></div><div class="col-sm-8" style="text-align:left;"><span class="item-name">' + input.itemname + '</span><span class="item-price">' + currencySymbol + '' + input.perItemPrice + '/' + input.uom + '</span><br/><span class="item-quantity">Quantity: ' + input.quantity + ' ' + input.uom + '</span><br>' + variance + '</div><div class="col-sm-12"><hr></div></li>';
        $('.crttt').append(cartSchema);
        cartTotal++;
      });

      if (cartTotal > 0) {
        $('.newCartItemTotal').removeClass('d-none').html(cartTotal);
      } else {
        $('.newCartItemTotal').html('0').addClass('d-none');
      }

      $('.chckout').html('<a href="' + base_url + 'checkout/viewcart" class="shopping-cart-checkout-button checkout-btn text-center">Proceed To Checkout <i style="margin-right: 0px;" class="fa fa-chevron-right" aria-hidden="true"></i> </a>');
    }
  }

}

function doLogin() {
  var base_url = $("#base_url").val();
  var values = {};
  toastr.clear();

  if ($.trim($('#uname').val()) == '') {

    toastr.error('Username is empty.');
  }
  if ($.trim($('#password').val()) == '') {

    toastr.error('Password is empty.');
  }

  if ($.trim($('#uname').val()) != '') {
    values.user_name = $('#uname').val();
  }
  if ($.trim($('#password').val()) != '') {
    values.password = $('#password').val();

  }


  if ((values.user_name.length > 1) && (values.user_name.length > 1)) {
    $.ajax({
      url: base_url + "api/login/doLogin",
      data: values,
      method: "post",
      success: function (result) {
        if (result.status == 200) {
          //console.log(result);
          toastr.success(result.msg);
          localStorage.setItem("miraimpex_token", result.token);

          setTimeout(
            function () {
              if ($('#tracelogUrl').val() != '') {
                window.location.replace($("#base_url").val() + '' + $("#tracelogUrl").val());
              } else {
                doAuthenticateMe();
                window.location.replace($("#base_url").val());
              }

            }, 1000);



        }
      },
      error: function (result) {
        toastr.clear();
        toastr.error(result.responseJSON.msg);
        removeLocalStore();
      }
    });

  }
}

function removeLocalStore() {
  localStorage.removeItem("miraimpex_token");
  localStorage.removeItem("miraimpex_customer_id");
  localStorage.removeItem("miraimpex_seller_id");
  localStorage.setItem("miraimpex_isLoggedIn", 'N');
  localStorage.removeItem("miraimpex_total_info");
  localStorage.removeItem("miraimpex_seller_info");
  localStorage.removeItem("miraimpex_buyer_info");


}

function doAuthenticateMe() {
  var data = {};

  $.ajax({
    url: $("#base_url").val() + "api/login/doAuthenticateMe",
    data: data,
    beforeSend: function (xhr) {
      /* Authorization header */
      xhr.setRequestHeader("Authorization", localStorage.getItem("miraimpex_token"));
      xhr.setRequestHeader("X-Mobile", "false");
    },
    method: "post",
    error: function (result) {
      //console.log("Unauthorised");
      toastr.clear();
      toastr.error(result.responseJSON.msg);
      removeLocalStore();
      setTimeout(
        function () {
          location.reload();

        }, 1000);
    },

    success: function (result) {
      //console.log(result);
      if (result.status === 200) {
        //console.log(result);
        localStorage.setItem("miraimpex_total_info", JSON.stringify(result.result));
        var customerId = result.result.id;

        if (result.result.seller_info) {
          var sellerId = result.result.seller_info.id;
        } else {
          var sellerId = '';
        }
        localStorage.setItem("miraimpex_exchangeRate", result.result.rate_exchange_info[0].rate);


        if (result.result.supplier_YN == 0) {
          $('#mySellerItemsNav').remove();
          $('#mySellerServicesNav').remove();
          $('#sellerInfoPanel').remove();
          $('#sellerImagePanel').remove();
          $('#sellerCertificatePanel').remove();
          $('#sellerDocumentPanel').remove();
          $('#mySellerSubscriptionNav').remove();
        } else {
          $('#upgrade_to_supplier_link').remove();
        }

        if (result.result.seller_info) {
          localStorage.setItem("miraimpex_seller_info", JSON.stringify(result.result.seller_info));
        } else {
          localStorage.removeItem("miraimpex_seller_info");
        }

        if (result.result.buyer_info) {
          localStorage.setItem("miraimpex_buyer_info", JSON.stringify(result.result.buyer_info));

          if (result.result.buyer_info.profile_image) {
            var profileImg = result.result.buyer_info.profile_image;
            $('#profileImgPanel').attr('src', $("#base_url").val() + 'admin/uploads/bussiness_logo/' + profileImg);
          }


        } else {
          localStorage.removeItem("miraimpex_buyer_info");
        }

        //console.log(customerId+'='+sellerId);
        localStorage.setItem("miraimpex_customer_id", customerId);
        localStorage.setItem("miraimpex_seller_id", sellerId);
        localStorage.setItem("miraimpex_isLoggedIn", 'Y');
        localStorage.setItem("miraimpex_is_seller", result.result.is_seller);

        $('.Account_ul_sec').css('display', 'block');
        $('.signInButtonLink').attr('href', 'javascript:void(0);');
        $('.signInButtonLink').attr('data-toggle', 'dropdown');

        var uname = result.result.name;
        //alert(uname);
        $('.unamepanel').html('Hi, ' + $.trim(uname));

        //getNotification();

      } else {
        $('.signInButtonLink').attr('href', $("#base_url").val() + 'login?' + tracelogUrl);
        $('.unamepanel').html('Sign In');
        $('.Account_ul_sec').remove();
        console.log(result.status + ':/login/doAuthenticateMe');
        removeLocalStore();
      }

    }
  });

}


function logoutCustomer(obj) {
  var currentPage = $(obj).attr('rel');
  localStorage.removeItem("miraimpex_addtocart");
  localStorage.removeItem("miraimpex_cartSellerId");
  if (localStorage.getItem("miraimpex_token") === null) {

  } else {
    $.ajax({
      url: $("#base_url").val() + "api/login/doLogout",
      beforeSend: function (xhr) {
        /* Authorization header */
        xhr.setRequestHeader("Authorization", localStorage.getItem("miraimpex_token"));
        xhr.setRequestHeader("X-Mobile", "false");
      },
      method: "post",
      error: function (result) {
        //console.log("Unauthorised");
        toastr.clear();
        toastr.error(result.responseJSON.msg);
        removeLocalStore();
        window.location.replace($("#base_url").val());
      },
      success: function (result) {
        //console.log(result);
        if (result.status === 200) {
          removeLocalStore();

          toastr.success(result.msg);

          var redirect = false;

          setTimeout(
            function () {
              if (redirect) {
                //alert('1');
                window.location.replace($("#base_url").val());
              } else {
                //alert('2');
                window.location.reload();
              }

            }, 1000);


        } else {
          removeLocalStore();
          window.location.replace($("#base_url").val());
        }

      }
    });
  }
}

function countChar(val) {
  var len = val.value.length;
  if (len >= 8000) {
    val.value = val.value.substring(0, 8000);
  } else {
    var text = 'You have ' + (8000 - len) + ' characters remaining.';
    $('#charNum').text(text);
  }
};


function getNotification() {

  if (localStorage.getItem("miraimpex_token") === null) {

  } else {
    var data = {};
    data.pageno = 1;

    $.ajax({
      url: $("#base_url").val() + "api/main/getNotificationUnReadCount",
      data: data,
      beforeSend: function (xhr) {
        /* Authorization header */
        xhr.setRequestHeader("Authorization", localStorage.getItem("miraimpex_token"));
        xhr.setRequestHeader("X-Mobile", "false");
      },
      method: "post",
      error: function (result) {
        //console.log("Unauthorised");
        toastr.clear();
        toastr.error(result.responseJSON.msg);
      },
      success: function (result) {
        //console.log(result);
        if (result.status === 200) {
          var newNotification = parseInt(result.unread);
          if (newNotification > 0) {
            $('.newUnreadNotification').html(newNotification).removeClass('d-none');
            $('#totalUnreadNotifications').html(newNotification + ' Unread');

          } else {
            $('.newUnreadNotification').html('').addClass('d-none');
            $('#totalUnreadNotifications').html('');
          }


        } else {

        }

      }
    });
  }
}

function colorconvert(color) {
  var colours = {
    "aliceblue": "#f0f8ff", "antiquewhite": "#faebd7", "aqua": "#00ffff", "aquamarine": "#7fffd4", "azure": "#f0ffff", "beige": "#f5f5dc", "bisque": "#ffe4c4", "black": "#000000", "blanchedalmond": "#ffebcd", "blue": "#0000ff", "blueviolet": "#8a2be2", "brown": "#a52a2a", "burlywood": "#deb887", "cadetblue": "#5f9ea0", "chartreuse": "#7fff00", "chocolate": "#d2691e", "coral": "#ff7f50", "cornflowerblue": "#6495ed", "cornsilk": "#fff8dc", "crimson": "#dc143c", "cyan": "#00ffff", "darkblue": "#00008b", "darkcyan": "#008b8b", "darkgoldenrod": "#b8860b", "darkgray": "#a9a9a9", "darkgreen": "#006400", "darkkhaki": "#bdb76b", "darkmagenta": "#8b008b", "darkolivegreen": "#556b2f", "darkorange": "#ff8c00", "darkorchid": "#9932cc", "darkred": "#8b0000", "darksalmon": "#e9967a", "darkseagreen": "#8fbc8f", "darkslateblue": "#483d8b", "darkslategray": "#2f4f4f", "darkturquoise": "#00ced1", "darkviolet": "#9400d3", "deeppink": "#ff1493", "deepskyblue": "#00bfff", "dimgray": "#696969", "dodgerblue": "#1e90ff", "firebrick": "#b22222", "floralwhite": "#fffaf0", "forestgreen": "#228b22", "fuchsia": "#ff00ff", "gainsboro": "#dcdcdc", "ghostwhite": "#f8f8ff", "gold": "#ffd700", "goldenrod": "#daa520", "gray": "#808080", "green": "#008000", "greenyellow": "#adff2f",
    "honeydew": "#f0fff0", "hotpink": "#ff69b4", "indianred ": "#cd5c5c", "indigo": "#4b0082", "ivory": "#fffff0", "khaki": "#f0e68c", "lavender": "#e6e6fa", "lavenderblush": "#fff0f5", "lawngreen": "#7cfc00", "lemonchiffon": "#fffacd", "lightblue": "#add8e6", "lightcoral": "#f08080", "lightcyan": "#e0ffff", "lightgoldenrodyellow": "#fafad2", "lightgrey": "#d3d3d3", "lightgreen": "#90ee90", "lightpink": "#ffb6c1", "lightsalmon": "#ffa07a", "lightseagreen": "#20b2aa", "lightskyblue": "#87cefa", "lightslategray": "#778899", "lightsteelblue": "#b0c4de", "lightyellow": "#ffffe0", "lime": "#00ff00", "limegreen": "#32cd32", "linen": "#faf0e6", "magenta": "#ff00ff", "maroon": "#800000", "mediumaquamarine": "#66cdaa", "mediumblue": "#0000cd", "mediumorchid": "#ba55d3", "mediumpurple": "#9370d8", "mediumseagreen": "#3cb371", "mediumslateblue": "#7b68ee", "mediumspringgreen": "#00fa9a", "mediumturquoise": "#48d1cc", "mediumvioletred": "#c71585", "midnightblue": "#191970", "mintcream": "#f5fffa", "mistyrose": "#ffe4e1", "moccasin": "#ffe4b5", "navajowhite": "#ffdead", "navy": "#000080", "oldlace": "#fdf5e6", "olive": "#808000", "olivedrab": "#6b8e23", "orange": "#ffa500", "orangered": "#ff4500", "orchid": "#da70d6", "palegoldenrod": "#eee8aa",
    "palegreen": "#98fb98", "paleturquoise": "#afeeee", "palevioletred": "#d87093", "papayawhip": "#ffefd5", "peachpuff": "#ffdab9", "peru": "#cd853f", "pink": "#ffc0cb", "plum": "#dda0dd", "powderblue": "#b0e0e6", "purple": "#800080", "rebeccapurple": "#663399", "red": "#ff0000", "rosybrown": "#bc8f8f", "royalblue": "#4169e1", "saddlebrown": "#8b4513", "salmon": "#fa8072", "sandybrown": "#f4a460", "seagreen": "#2e8b57", "seashell": "#fff5ee", "sienna": "#a0522d", "silver": "#c0c0c0", "skyblue": "#87ceeb", "slateblue": "#6a5acd", "slategray": "#708090", "snow": "#fffafa", "springgreen": "#00ff7f", "steelblue": "#4682b4", "tan": "#d2b48c", "teal": "#008080", "thistle": "#d8bfd8", "tomato": "#ff6347", "turquoise": "#40e0d0", "violet": "#ee82ee", "wheat": "#f5deb3", "white": "#ffffff", "whitesmoke": "#f5f5f5", "yellow": "#ffff00", "yellowgreen": "#9acd32"
  };

  if (typeof colours[color.toLowerCase()] != 'undefined')
    return colours[color.toLowerCase()];
  return false;
}


