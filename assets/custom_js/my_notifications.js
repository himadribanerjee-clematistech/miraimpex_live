$(document).ready(function () {
    if (localStorage.getItem("miraimpex_token") === null) {
        window.location.replace($("#base_url").val());
    } else {
        getnotificationlists();
    }
});

function getnotificationlists() {
    $("#quotation_mess").empty();
    $("#quotation_mess").show();
    $('#quotation_mess').html('<div class=""><h2 style="text-align:center;">No notification found.</h2></div>');
}