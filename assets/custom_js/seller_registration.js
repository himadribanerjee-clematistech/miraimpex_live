
var abcd = 0;
var abcde = 0;
var abcdef = 0;

$(document).ready(function () {
    if (localStorage.getItem("miraimpex_token") === null) {
        window.location.replace($("#base_url").val());
    } else {
        var total_info = JSON.parse(localStorage.getItem("miraimpex_total_info"));
        if (total_info.supplier_YN > 0) {
            window.location.replace($("#base_url").val());
        }

        $("#updatesellerProfileForm").submit(function (e) {

            toastr.clear();
            var submit = true;


            if ($.trim($('#business_name').val()) == '') {
                toastr.error('Business name is empty!');
                $('#business_name').focus();
                submit = false;
                return false;
            }
            if ($.trim($('#registrationRegion').val()) == '') {
                toastr.error('Region is empty!');
                $('#registrationRegion').focus();
                submit = false;
                return false;
            }
            if ($.trim($('#registrationCountry').val()) == '') {
                toastr.error('Country is empty!');
                $('#registrationCountry').focus();
                submit = false;
                return false;
            }
            if ($.trim($('#registrationState').val()) == '') {
                toastr.error('State is empty!');
                $('#registrationState').focus();
                submit = false;
                return false;
            }
            if ($.trim($('#city').val()) == '') {
                toastr.error('City name is empty!');
                $('#upd_sellerAddress').focus();
                submit = false;
                return false;
            }
            if ($.trim($('#address').val()) == '') {
                toastr.error('Address is empty!');
                $('#address').focus();
                submit = false;
                return false;
            }

            if ($.trim($('#contact_name').val()) == '') {
                toastr.error('contact name is empty!');
                $('#contact_name').focus();
                submit = false;
                return false;
            }

            if ($.trim($('#contact_number').val()) == '') {
                toastr.error('contact number is empty!');
                $('#contact_number').focus();
                submit = false;
                return false;
            }

            if ($('#gallery-certificate-add').get(0).files.length === 0) {
                toastr.error('Please attach company certificates!');
                submit = false;
                return false;
            }

            if ($('#gallery-document-add').get(0).files.length === 0) {
                toastr.error('Please attach company documents!');
                submit = false;
                return false;
            }

            if (submit) {
                $("#updatesellerProfileSubmit").prop("disabled", true);
                $("#updatesellerProfileSubmit").prop('value', 'Submitting...please wait.');

                var formData = new FormData(this);

                var upd_sellerContactNo = $.trim($('#contact_number').val());
                var new_upd_sellerContactNo = upd_sellerContactNo.replace(/-|\s/g, "");

                formData.set('contact_number', new_upd_sellerContactNo);

                $.ajax({
                    type: "POST",
                    method: "post",
                    enctype: 'multipart/form-data',
                    url: $("#base_url").val() + "api/login/upgreadeToSeller",
                    data: formData,
                    processData: false,
                    contentType: false,
                    cache: false,
                    timeout: 600000,
                    beforeSend: function (xhr) {
                        /* Authorization header */
                        xhr.setRequestHeader("Authorization", localStorage.getItem("miraimpex_token"));
                        xhr.setRequestHeader("X-Mobile", "false");
                    },
                    success: function (data) {
                        //console.log("SUCCESS : ", data);
                        toastr.success("Seller profile updated successfully");
                        doAuthenticateMe();
                        setTimeout(
                            function () {
                                window.location.replace($("#base_url").val());

                            }, 1000);

                    },
                    error: function (result) {
                        toastr.clear();
                        toastr.error(result.responseJSON.msg);
                        $("#updatesellerProfileSubmit").prop("disabled", false);
                        $("#updatesellerProfileSubmit").html('Upgrade to Seller');

                    }
                });
            }
        });

        for (region in countries) {
            $('#registrationRegion').append('<option value="' + region + '">' + region + '</option>');
        }

        var SPMaskBehavior = function (val) {
            return val.replace(/\D/g, '').length === 11 ? '00 00000-0000' : '00 0000-00009';
        },
            spOptions = {
                onKeyPress: function (val, e, field, options) {
                    field.mask(SPMaskBehavior.apply({}, arguments), options);
                }
            };

        $('.input-mask-phone').mask(SPMaskBehavior, spOptions);

        $("#contact_number").keyup(function () {
            var mobileNo = $.trim($('#updatesellerProfileForm').find('input[id="contact_number"]').val());
            var new_mobileNo = mobileNo.replace(/^0+/, '');
            $('#contact_number').val(new_mobileNo);
        });

        $("#contact_number").keypress(function (e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                //display error message
                toastr.clear();
                toastr.error('Digits only!');
                $("#contact_number").focus();
                return false;
            }
        });
    }
});


function sellercertificateIsLoaded(e) {
    $('#sellercertificatepreviewimg' + abcdef).attr('src', e.target.result);
};

function sellerdocumentIsLoaded(e) {
    var encoded = e.target.result;
    var mimeType = '';
    var mime = encoded.match(/data:([a-zA-Z0-9]+\/[a-zA-Z0-9-.+]+).*,.*/);

    if (mime && mime.length) {
        mimeType = mime[1];
    }
    if (mimeType == 'text/plain' || mimeType == 'application/msword' || mimeType == 'application/pdf' || mimeType == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
        $('#sellerdocpreviewimg' + abcde).attr('src', $("#base_url").val() + 'assets/images/icon/doc.png');
    } else {
        $('#sellerdocpreviewimg' + abcde).attr('src', e.target.result);
    }
};

$(document).ready(function () {
    $('body').on('change', '#gallery-certificate-add', function () {
        if (this.files && this.files[0]) {
            abcdef += 1; // Incrementing global variable by 1.
            var z = abcdef - 1;
            $(this).find('#previewimg' + z).remove();

            $('#testdvcertificate').append("<div style='display: inline-block; padding: 14px;'><div id='abcdef_" + abcdef + "' class='abcd'><img id='sellercertificatepreviewimg" + abcdef + "' src='' style='width: 139px; height: 89px;' /><div style='bottom: 0; width: 100%; height: 24px; line-height: 9px; background: #3b485a; opacity: .85;'><a class='remove' href='javascript:void(0);' onclick='return false;' style='display: inline-block; width: 100%; height: 24px; background: url(./assets/images/uploader-icons.png) no-repeat center 3px; text-align: center; background-position: center -37px;'><i class=\"fa fa-trash\" aria-hidden=\"true\" style=\"padding-top: 3px;font-size: 16px;\"></i></a></div></div></div>");

            var reader = new FileReader();
            reader.onload = sellercertificateIsLoaded;
            reader.readAsDataURL(this.files[0]);
            $(this).hide();

            $("#abcdef_" + abcdef).click(function () {
                $(this).parent().remove();
                var id = $(this).attr("id");
                var id_arr = id.split("_");
                var no = (parseInt(id_arr[1]) - 1);
                //console.log(abc,id_arr[1],no);
                if (id_arr.length == 2) {
                    $("#sellercertificatefilediv_" + no).val("").trigger("change");
                    $("#sellercertificatefilediv_" + no).remove();
                }

            });



            $("#testdvcertificate").prepend($("<div id='sellercertificatefilediv_" + abcdef + "' class ='testcls' />").fadeIn('slow').prepend($("<input/>", {
                name: 'certificate[]',
                type: 'file',
                id: 'gallery-certificate-add'
            }), $("")));

        }
    });
});


$(document).ready(function () {

    $('body').on('change', '#gallery-document-add', function () {
        if (this.files && this.files[0]) {
            abcde += 1; // Incrementing global variable by 1.
            var z = abcde - 1;
            $(this).find('#previewimg' + z).remove();

            $('#testdvdoc').append("<div style='display: inline-block; padding: 14px;'><div id='abcde_" + abcde + "' class='abcd'><img id='sellerdocpreviewimg" + abcde + "' src='' style='width: 139px; height: 89px;' /><div style='bottom: 0; width: 100%; height: 24px; line-height: 9px; background: #3b485a; opacity: .85;'><a class='remove' href='javascript:void(0);' onclick='return false;' style='display: inline-block; width: 100%; height: 24px; background: url(./assets/images/uploader-icons.png) no-repeat center 3px; text-align: center; background-position: center -37px;'><i class=\"fa fa-trash\" aria-hidden=\"true\" style=\"padding-top: 3px;font-size: 16px;\"></i></a></div></div></div>");

            var reader = new FileReader();
            reader.onload = sellerdocumentIsLoaded;
            reader.readAsDataURL(this.files[0]);
            $(this).hide();

            $("#abcde_" + abcde).click(function () {
                $(this).parent().remove();
                var id = $(this).attr("id");
                var id_arr = id.split("_");
                var no = (parseInt(id_arr[1]) - 1);
                //console.log(abc,id_arr[1],no);
                if (id_arr.length == 2) {
                    $("#sellerdocfilediv_" + no).val("").trigger("change");
                    $("#sellerdocfilediv_" + no).remove();
                }

            });



            $("#testdvdoc").prepend($("<div id='sellerdocfilediv_" + abcde + "' class ='testcls' />").fadeIn('slow').prepend($("<input/>", {
                name: 'document[]',
                type: 'file',
                id: 'gallery-document-add'
            }), $("")));

        }
    });

});


