var cartArr = JSON.parse(localStorage.getItem("miraimpex_addtocart"));


$(document).ready(function () {
    populatecart();

});

function getCartObjectIndex(arr, itemid) {
    // Loop over array to find the matching element/object
    for (var i = 0; i < arr.length; i++) {
        var obj = arr[i];
        if (obj.itemid === itemid) {
            // When all key-value matches return the array index
            return i;
        }
    }
    // When no match found, return false
    return false;
}


function populatecart() {
    $('.loading').removeClass('hide');
    var totalToPay = 0;
    var currency = '';
    var base_url = $("#base_url").val();
    var addtocart = cartArr;
    var itemIdArr = [];
    if (addtocart) {
        $.each(addtocart, function (key, input) {
            itemIdArr.push(input.itemid);
        });
        /* get updated price section start */

        var base_url = $("#base_url").val();
        var values = {};
        values.itemIdArr = itemIdArr;
        $.ajax({
            url: base_url + "api/item/getlatestprice",
            data: values,
            beforeSend: function (xhr) {
                /* Authorization header */
                xhr.setRequestHeader("Authorization", localStorage.getItem("miraimpex_token"));
                xhr.setRequestHeader("X-Mobile", "false");
            },
            method: "post",
            success: function (result) {
                //console.log(result);
                if (result.status == 200) {

                    var latestItemPricesArr = result.item;
                    $.each(latestItemPricesArr, function (index, value) {
                        var cartindex = getCartObjectIndex(addtocart, value.item_id);
                        if (cartindex !== false) {
                            // Update the quantity in the same element
                            addtocart[cartindex].perItemPrice = value.fixedPrice;
                        }
                    });

                    setTimeout(
                        function () {

                            localStorage.setItem("miraimpex_addtocart", JSON.stringify(addtocart));
                            getcartdetails();

                        }, 500);

                } else {
                    // Something went wrong
                }


            },
            error: function (result) {
                console.log(result.responseJSON.msg);
            }
        });
        /* end */
    } else {
        $('.loading').addClass('hide');
        $('#placeorderbut').remove();
    }

}


function getcartdetails() {
    var totalToPay = 0;
    var currency = '';
    var base_url = $("#base_url").val();
    var addtocart = cartArr;
    if (addtocart) {
        $('#myTabletbody').empty();
        $.each(addtocart, function (key, input) {

            totalToPay += input.perItemPrice * input.quantity;
            var variance = '';
            if (input.itemvariance != "") {
                var addtocartvariance = JSON.parse(input.itemvariance);
                $.each(addtocartvariance, function (vkey, vinput) {
                    variance += '<span class="item-variance" style="padding-right:10px;">' + vinput.varianceName + ': ' + vinput.varianceAttribute + '</span>';
                });
            }

            var currencySymbol = '';
            if (input.currency == 'Dollar' || input.currency == 'USD') {
                currencySymbol = '$';
            } else if (input.currency == 'Euro' || input.currency == 'EUR') {
                currencySymbol = '€';
            } else {
                currencySymbol = '$';
            }

            currency = currencySymbol;

            var perItemTotalPrice = 0;

            perItemTotalPrice += input.quantity * input.perItemPrice;
            perItemTotalPrice = (Math.round(perItemTotalPrice * 100) / 100).toFixed(2);

            /* var cartSchema = '<div class="right_pay_head"><div class="pay_img"><a href="'+base_url+'product/details/'+input.itemid+'" target="_blank" title="view Item details"><img src="' + input.itemimg + '" alt=""></a></div><div class="pay_cnt"><a href="'+base_url+'product/details/'+input.itemid+'" target="_blank" title="view Item">' + input.itemname + '</a><span class="item-price">Price: ' + currencySymbol + '' + input.perItemPrice + '</span><span class="item-quantity">Quantity: <input type="number" id="quantity" name="quantity[]" min="' + input.moq + '" value="' + input.quantity + '" itemid="'+input.itemid+'" moq="'+input.moq+'" onchange="javascript:updatecart(this);"></span><a href="javascript:void(0);" itemid="'+input.itemid+'" onclick="javascript:removeitemfromcart(this);">Remove Item</a></div></div>';
            $('#cartDetails').append(cartSchema); */
            var GMDprice = input.perItemPrice * gambianDalasi2USDexchangeRate;
            GMDprice = (Math.round(GMDprice * 100) / 100).toFixed(2);

            var perItemTotalPriceGMD = (input.quantity * input.perItemPrice) * gambianDalasi2USDexchangeRate;
            perItemTotalPriceGMD = (Math.round(perItemTotalPriceGMD * 100) / 100).toFixed(2);

            var cartSchema = '<tr><td style="border-right: 1px solid #f1eded;"><div class="media"><a class="thumbnail pull-left" href="' + base_url + 'product/details/' + input.itemid + '"><img class="media-object" src="' + input.itemimg + '" style="width: 72px; height: 72px;"></a><div class="media-body"><h4 class="media-heading"><a href="' + base_url + 'product/details/' + input.itemid + '">' + input.itemname + '</a></h4><h5 class="media-heading">' + variance + '</h5><span> by <a href="' + base_url + 'supplier/details/' + input.seller_id + '">' + input.business_name + '</a></span></div></div></td><td style="border-right: 1px solid #f1eded;"><p style="text-align: center; font-size:16px; font-weight:600;">' + currencySymbol + '' + input.perItemPrice + '</p><p style="text-align: center;">(' + GMDprice + ' GMD)</p><p style="text-align: center;"><span>incl. all taxes</span></p></td><td style="border-right: 1px solid #f1eded;"> <div class="input-group" style="padding: 0px; padding-top: 0px;"><span class="input-group-addon remove" style="background: #f1f1f1; color: #FFF;"><a href="javascript:void(0);" moq="' + input.moq + '" onclick="javascript:decrementqty(this);"><i class="fa fa-minus-circle" aria-hidden="true" id="remove" style="font-size: 20px;"></i></a></span><div class="qty"><input type="number" class="form-control" name="quantity[]" style="text-indent: 9px; font-size: 16px;" min="' + input.moq + '" value="' + input.quantity + '" itemid="' + input.itemid + '" moq="' + input.moq + '" onchange="javascript:updatecart(this);"></div><span class="input-group-addon add" style="background: #f1f1f1; color: #FFF;"><a href="javascript:void(0);" moq="' + input.moq + '" onclick="javascript:incrementqty(this);"><i class="fa fa-plus-circle" aria-hidden="true" id="add" style="font-size: 20px;"></i></a></span></div><p style="text-align:center;">' + input.uom + '</p></td><td style="border-right: 1px solid #f1eded;"><p style="text-align: center; font-size: 20px; font-weight: 600; padding: 0px;">' + currencySymbol + '' + perItemTotalPrice + '</p><p style="text-align: center;">(' + perItemTotalPriceGMD + ' GMD)</p></td><td style="border-right: 1px solid #f1eded;"><div style="text-align: center;"><div class="btn-group"><a href="javascript:void(0);" itemid="' + input.itemid + '" onclick="javascript:removeitemfromcart(this);"><button type="button" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span> Remove</button></a></div></div></td></tr>';
            // $('#myTable tr:last').after(cartSchema);
            $('#myTable tbody').append(cartSchema);
        });
        totalToPay = (Math.round(totalToPay * 100) / 100).toFixed(2);

        var totalToPayGMDprice = totalToPay * gambianDalasi2USDexchangeRate;
        totalToPayGMDprice = (Math.round(totalToPayGMDprice * 100) / 100).toFixed(2);

        $('.finalPayment').html(currency + '' + totalToPay);
        $('.gmdVal').html('<p>(Approx ' + totalToPayGMDprice + ' Gambian Dalasi)</p>');
        $('.loading').addClass('hide');
    }

}

function decrementqty(obj) {
    var v = parseInt($(obj).parent().parent().find("input[type=number]").val());
    v = v - 1;
    var moq = $(obj).attr('moq');
    if (v < moq) {
        toastr.clear();
        toastr.error('Minimum order quantity should be ' + moq);
    } else {
        $(obj).parent().parent().find("input[type=number]").val(v);
        $(obj).parent().parent().find("input[type=number]").trigger('change');
    }
    //console.log($(obj).parent().parent().find("input[type=number]").val()); 
}

function incrementqty(obj) {
    var v = parseInt($(obj).parent().parent().find("input[type=number]").val());
    v = v + 1;
    var moq = $(obj).attr('moq');
    if (v < moq) {
        toastr.clear();
        toastr.error('Minimum order quantity should be ' + moq);
    } else {
        $(obj).parent().parent().find("input[type=number]").val(v);
        $(obj).parent().parent().find("input[type=number]").trigger('change');
    }
    //console.log($(obj).parent().parent().find("input[type=number]").val()); 
}


function removeitemfromcart(obj) {

    if (confirm("This item will be removed from your cart!")) {
        var itemid = $(obj).attr('itemid');
        //alert(itemid);
        if (localStorage.getItem("miraimpex_addtocart") === null) {

        } else {
            var addtocartArr = cartArr;
            var cartindex = getCartObjectIndex(addtocartArr, itemid);
            if (cartindex !== false) {
                addtocartArr.splice(cartindex, 1);
                if (addtocartArr.length == 0) {
                    localStorage.removeItem("miraimpex_addtocart");
                    localStorage.removeItem("miraimpex_cartSellerId");
                    window.location.replace($("#base_url").val() + 'checkout/viewcart');
                } else {
                    toastr.clear();
                    toastr.success('Item removed successfully from cart!');
                    localStorage.setItem("miraimpex_addtocart", JSON.stringify(addtocartArr));
                }
                setTimeout(
                    function () {
                        //window.location.replace($("#base_url").val()+'checkout/viewcart');
                        populatecart();
                    }, 500);
            }
        }
    }
}

function updatecart(obj) {
    var itemid = $(obj).attr('itemid');
    var quantity = $(obj).val();
    var moq = $(obj).attr('moq');

    if (parseInt(quantity) < parseInt(moq)) {
        toastr.clear();
        toastr.error('Minimum order quantity should be ' + moq);
    } else {

        var cartindex = getCartObjectIndex(cartArr, itemid);
        if (cartindex !== false) {
            // Update the quantity in the same element
            cartArr[cartindex].quantity = quantity;
            toastr.clear();
            toastr.success('Item updated successfully to cart!');

            localStorage.setItem("miraimpex_addtocart", JSON.stringify(cartArr));

            setTimeout(
                function () {

                    //window.location.replace($("#base_url").val()+'checkout/viewcart');
                    populatecart();

                }, 500);
        }
    }

}