$(document).ready(function () {
    getcartdetails();

    $("input[name='paymentMode']").click(function () {
        var radioValue = $("input[name='paymentMode']:checked").val();
        if (radioValue == "2") {
            $("#bankpay-AF").slideDown("slow", function () { });
            $("#bankpay-US").slideUp("slow", function () { });
            $("#bankpay-EUR").slideUp("slow", function () { });
        } else if (radioValue == "3") {
            $("#bankpay-AF").slideUp("slow", function () { });
            $("#bankpay-US").slideDown("slow", function () { });
            $("#bankpay-EUR").slideUp("slow", function () { });
        } else if (radioValue == "4") {
            $("#bankpay-AF").slideUp("slow", function () { });
            $("#bankpay-US").slideUp("slow", function () { });
            $("#bankpay-EUR").slideDown("slow", function () { });
        } else if (radioValue == "1") {
            $("#bankpay-AF").slideUp("slow", function () { });
            $("#bankpay-US").slideUp("slow", function () { });
            $("#bankpay-EUR").slideUp("slow", function () { });
        }
    });

    $("#send_checkoutForm").submit(function (e) {
        e.preventDefault();
        toastr.clear();
        var submit = true;
        if ($.trim($('#fname').val()) == '') {
            toastr.error('Full name is blank!');
            submit = false;
            $('#fname').focus();
            return false;
        }

        if ($.trim($('#email').val()) == '') {
            toastr.error('Email is blank!');
            submit = false;
            $('#email').focus();
            return false;
        }

        // if ($.trim($('#cname').val()) == '') {
        //     toastr.error('Name on card is blank!');
        //     submit = false;
        //     $('#cname').focus();
        //     return false;
        // }

        if ($.trim($('#adr').val()) == '') {
            toastr.error('Address is blank!');
            submit = false;
            $('#adr').focus();
            return false;
        }

        // if ($.trim($('#ccnum').val()) == '') {
        //     toastr.error('Card number is blank!');
        //     submit = false;
        //     $('#ccnum').focus();
        //     return false;
        // }

        if ($.trim($('#city').val()) == '') {
            toastr.error('City is blank!');
            submit = false;
            $('#city').focus();
            return false;
        }

        // if ($.trim($('#expmonth').val()) == '') {
        //     toastr.error('Expiry month is blank!');
        //     submit = false;
        //     $('#expmonth').focus();
        //     return false;
        // }

        /* if ($.trim($('#state').val()) == '') {
          toastr.error('State is blank!');
          submit = false;
          $('#state').focus();
          return false;
        }
    
        if ($.trim($('#zip').val()) == '') {
          toastr.error('Zip is blank!');
          submit = false;
          $('#zip').focus();
          return false;
        } */

        // if ($.trim($('#expyear').val()) == '') {
        //     toastr.error('expyear is blank!');
        //     submit = false;
        //     $('#expyear').focus();
        //     return false;
        // }

        // if ($.trim($('#cvv').val()) == '') {
        //     toastr.error('cvv is blank!');
        //     submit = false;
        //     $('#cvv').focus();
        //     return false;
        // }

        if (submit) {
            $("#confirmPayButton").val("Processing Payment... Please wait");
            $("#confirmPayButton").attr("disabled", true);
            var base_url = $("#base_url").val();
            var values = {};
            values.proposal_id = '';
            values.payment_mode = $('input[name="paymentMode"]:checked').val();
            values.buyer_info = JSON.stringify($('#send_checkoutForm').serializeArray());
            values.item_details = localStorage.getItem("miraimpex_addtocart");
            $.ajax({
                url: base_url + "api/messagecenter/processPayment",
                data: values,
                beforeSend: function (xhr) {
                    /* Authorization header */
                    xhr.setRequestHeader("Authorization", localStorage.getItem("miraimpex_token"));
                    xhr.setRequestHeader("X-Mobile", "false");
                },
                method: "post",
                success: function (result) {
                    //console.log(result);
                    if (result.status == 200) {
                        // {"status":200,"msg":"process payment successfully.","order_id":7,"total_amount_paid":1525,"transction_id":1583914074}
                        toastr.clear();
                        toastr.success('Your Order has been successfully placed.');
                        localStorage.removeItem("miraimpex_addtocart");
                        setTimeout(
                            function () {
                                window.location.replace($("#base_url").val() + 'profile/myorders');
                            }, 1000);



                    } else {
                        // Something went wrong
                    }


                },
                error: function (result) {
                    console.log(result.responseJSON.msg);
                }
            });

        }

    });

});

function confirmPayment(proposal_order_id) {
    var base_url = $("#base_url").val();
    var values = {};
    values.order_id = proposal_order_id;
    values.status = '1';
    values.transction_id = '121323732732';

    $.ajax({
        url: base_url + "api/messagecenter/confirmPayment11",
        data: values,
        beforeSend: function (xhr) {
            /* Authorization header */
            xhr.setRequestHeader("Authorization", localStorage.getItem("miraimpex_token"));
            xhr.setRequestHeader("X-Mobile", "false");
        },
        method: "post",
        success: function (result) {
            //console.log(result);
            if (result.status == 200) {

                toastr.clear();
                toastr.success(result.msg);


            } else {
                // Something went wrong
                toastr.clear();
                toastr.error(result.msg);
            }


        },
        error: function (result) {
            console.log(result.responseJSON.msg);
        }
    });
}

function getcartdetails() {
    if (localStorage.getItem("miraimpex_token") === null) {
        window.location.replace($("#base_url").val() + 'login?tracelog=checkout/placeorder');
    }
    var totalToPay = 0;
    var currency = '';
    var base_url = $("#base_url").val();
    var addtocart = JSON.parse(localStorage.getItem("miraimpex_addtocart"));
    if (addtocart) {
        $.each(addtocart, function (key, input) {
            totalToPay += input.perItemPrice * input.quantity;
            var variance = '';
            if (input.itemvariance != "") {
                var addtocartvariance = JSON.parse(input.itemvariance);
                $.each(addtocartvariance, function (vkey, vinput) {
                    variance += '<span class="item-variance">' + vinput.varianceName + ': ' + vinput.varianceAttribute + '</span> ';
                });
            }

            var currencySymbol = '';
            if (input.currency == 'Dollar' || input.currency == 'USD') {
                currencySymbol = '$';
            } else if (input.currency == 'Euro' || input.currency == 'EUR') {
                currencySymbol = '€';
            } else {
                currencySymbol = '$';
            }

            currency = currencySymbol;

            /* var cartSchema = '<div class="right_pay_head"><div class="pay_img"><a href="'+base_url+'product/details/'+input.itemid+'" target="_blank" title="view Item details"><img src="' + input.itemimg + '" alt=""></a></div><div class="pay_cnt"><a href="'+base_url+'product/details/'+input.itemid+'" target="_blank" title="view Item">' + input.itemname + '</a><span class="item-price">Price: ' + currencySymbol + '' + input.perItemPrice + '</span><span class="item-quantity">Quantity: ' + input.quantity + '</span></div></div>'; */

            var perItemTotalPrice = 0;

            perItemTotalPrice += input.quantity * input.perItemPrice;
            perItemTotalPrice = (Math.round(perItemTotalPrice * 100) / 100).toFixed(2);

            var GMDprice = input.perItemPrice * gambianDalasi2USDexchangeRate;
            GMDprice = (Math.round(GMDprice * 100) / 100).toFixed(2);

            var perItemTotalPriceGMD = (input.quantity * input.perItemPrice) * gambianDalasi2USDexchangeRate;
            perItemTotalPriceGMD = (Math.round(perItemTotalPriceGMD * 100) / 100).toFixed(2);

            var cartSchema = '<li><div class="col-sm-4"><img src="' + input.itemimg + '" style="float:left;margin-right: 5px;" alt="item" /></div><div class="col-sm-8" style="text-align:left;"><span class="item-name">' + input.itemname + '</span><span class="item-price">' + currencySymbol + '' + input.perItemPrice + '/' + input.uom + '</span><br/><span class="item-quantity">Quantity: ' + input.quantity + ' ' + input.uom + '</span><br>' + variance + '</div><div class="col-sm-12"><p style="font-weight: 600; padding-top: 15px;">Sub Total: ' + currencySymbol + '' + perItemTotalPrice + '</p><p style="">(Approx. ' + perItemTotalPriceGMD + ' GMD)</p></div><div class="col-sm-12"><hr></div></li>';


            $('#cartDetails').append(cartSchema);
        });

        var totalToPayGMDprice = totalToPay * gambianDalasi2USDexchangeRate;
        totalToPayGMDprice = (Math.round(totalToPayGMDprice * 100) / 100).toFixed(2);

        totalToPay = (Math.round(totalToPay * 100) / 100).toFixed(2);

        $('#cartDetails').append('<div class="payment_page_info"><h5 class="pay_title"><b>Total To Pay</b></h5><h5 class="pay_value" id="proposal_amount">' + currency + ' ' + totalToPay + '</h5></div><div class="payment_page_info"><p>(Approx ' + totalToPayGMDprice + ' Gambian Dalasi)</p></div>');

        $('#totalToPay').val(totalToPay);
    }

}