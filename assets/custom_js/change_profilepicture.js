$(document).ready(function () {
    if (localStorage.getItem("miraimpex_isLoggedIn") == 'Y') {
        var total_info = JSON.parse(localStorage.getItem("miraimpex_total_info"));
        //console.log(total_info);
        if (total_info.supplier_YN == 1) {


        } else {
            $('#mySellerItemsNav').attr('href', 'javascript:void(0);');
            $('#mySellerItemsNav').html('');
            $('#mySellerItemsNav').remove();
            $('#mySellerServicesNav').attr('href', 'javascript:void(0);');
            $('#mySellerServicesNav').html('');
            $('#mySellerServicesNav').remove();
        }

        if (total_info.buyer_info.profile_image) {
            var profileImg = total_info.buyer_info.profile_image;
            $('#update_pro_img').attr('src', $("#base_url").val() + 'admin/uploads/bussiness_logo/' + profileImg);
        }
    } else {
        window.location.replace($("#base_url").val());
    }


});

function readProfilePictureURL(input) {
    if (input.files && input.files[0]) {
        $('#blah').removeClass('d-none');
        $('#saveBut').removeClass('d-none');
        var reader = new FileReader();

        reader.onload = function (e) {
            console.log(e.target);
            $('#blah').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#imgInp").change(function () {
    readProfilePictureURL(this);
});

function triggerClick() {
    $("#imgInp").trigger('click');
}

function uploadProfilePic() {
    $('#uploadProfilePicForm').submit();
}

$(document).ready(function () {

    $("#uploadProfilePicForm").submit(function (e) {
        e.preventDefault();
        toastr.clear();
        var submit = true;

        if (submit) {
            // disabled the submit button
            $("#ssaveBut").prop("disabled", true);
            var formData = new FormData(this);

            $.ajax({
                type: "POST",
                method: "post",
                enctype: 'multipart/form-data',
                url: $("#base_url").val() + "api/login/doBuyerUpdateImage",
                data: formData,
                processData: false,
                contentType: false,
                cache: false,
                timeout: 600000,
                beforeSend: function (xhr) {
                    /* Authorization header */
                    xhr.setRequestHeader("Authorization", localStorage.getItem("miraimpex_token"));
                    xhr.setRequestHeader("X-Mobile", "false");
                },
                success: function (data) {
                    //console.log("SUCCESS : ", data);
                    toastr.success("Profile picture updated successfully");
                    doAuthenticateMe();
                    setTimeout(
                        function () {
                            var total_info = JSON.parse(localStorage.getItem("miraimpex_total_info"));
                            //console.log(total_info);
                            if (total_info.buyer_info.profile_image) {
                                var profileImg = total_info.buyer_info.profile_image;
                                $('#update_pro_img').attr('src', $("#base_url").val() + 'admin/uploads/bussiness_logo/' + profileImg);
                            }

                        }, 1000);


                    $("#ssaveBut").prop("disabled", false);

                },
                error: function (e) {
                    toastr.clear();
                    toastr.error(e.responseJSON.msg);
                    $("#ssaveBut").prop("disabled", false);

                }
            });
        }

    });

});