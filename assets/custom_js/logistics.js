function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

function validatePhone(txtPhone) {
    var filter = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;
    if (filter.test(txtPhone)) {
        return true;
    }
    else {
        return false;
    }
}

$(document).ready(function () {
    $('#addSellerDocumentForm').submit(function (e) {
        e.preventDefault();
        var email = $('#email').val();
        var txtPhone = $('#phone').val();

        if ($.trim($('#name').val()) == '') {
            toastr.clear();
            toastr.error('Name is blank!');
            $('#name').focus();
            return false;
        }
        if ($.trim($('#email').val()) == '') {
            toastr.clear();
            toastr.error('Email is blank!');
            $('#email').focus();
            return false;
        } else {
            if (!isEmail(email)) {
                toastr.clear();
                toastr.error('Invalid Email!');
                $('#email').focus();
                return false;
            }
        }
        if ($.trim($('#phone').val()) == '') {
            toastr.clear();
            toastr.error('Phone is blank!');
            $('#phone').focus();
            return false;
        } else {
            if (!validatePhone(txtPhone)) {
                toastr.clear();
                toastr.error('Invalid Phone!');
                $('#phone').focus();
                return false;
            }
        }
        if ($.trim($('#description').val()) == '') {
            toastr.clear();
            toastr.error('Description is blank!');
            $('#description').focus();
            return false;
        }

        $('#quoteButton').prop("disabled", true).html('Sending request...please wait.');
        var data = {};
        data.name = $.trim($('#name').val());
        data.email = $.trim($('#email').val());
        data.phone = $.trim($('#phone').val());
        data.description = $.trim($('#description').val());
        $.ajax({
            url: $("#base_url").val() + "pages/sendemaillogistics",
            data: data,
            method: "post",
            error: function (result) {
                toastr.clear();
                toastr.error(result.responseJSON.message);
            },
            success: function (result) {
                //console.log(result);
                if (result.status === 1) {
                    toastr.clear();
                    toastr.success(result.msg);
                    $('#quoteButton').prop("disabled", true).html('Sent successfully.');


                } else {
                }

            }
        });
    });
});