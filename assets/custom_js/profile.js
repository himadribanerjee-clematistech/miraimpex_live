var abcd = 0;
var abcde = 0;
var abcdef = 0;

$(document).ready(function () {
  if (localStorage.getItem("miraimpex_token") === null) {
    window.location.replace($("#base_url").val() + 'login');
  }


  if (localStorage.getItem("miraimpex_isLoggedIn") == 'Y') {
    var total_info = JSON.parse(localStorage.getItem("miraimpex_total_info"));
    //console.log(total_info);
    if (total_info.supplier_YN > 0) {


    } else {
      $('#mySellerItemsNav').attr('href', 'javascript:void(0);');
      $('#mySellerItemsNav').html('');
      $('#mySellerItemsNav').remove();
      $('#mySellerServicesNav').attr('href', 'javascript:void(0);');
      $('#mySellerServicesNav').html('');
      $('#mySellerServicesNav').remove();
    }

    if (total_info.supplier_YN == "0") {

      $('#buyerAccountType').html('Buyer');
      $('#supplierType').remove();
      $('#subscriptionType').remove();

    } else {

      $('#buyerAccountType').html('Supplier');
      $('#suppliersubscriptionType').html('Free');

      $('#supplierAccountType').html((total_info.sellerCategory == "1") ? "Manufacturers" : (total_info.sellerCategory == "2") ? "Importers & Exporters" : (total_info.sellerCategory == "3") ? "Commission agents" : (total_info.sellerCategory == "4") ? "Local traders" : (total_info.sellerCategory == "5") ? "Local producers" : (total_info.sellerCategory == "6") ? "Distributors" : (total_info.sellerCategory == "7") ? "Wholesalers" : (total_info.sellerCategory == "8") ? "Service providers" : "NA");

      $('#upgrade_supplier_info').remove();
    }

    //alert('aa-' + total_info.name);
    $('#buyerName').html(total_info.name);
    $('#joindate').html(total_info.created_date);

    if (total_info.email_verify == "0") {
      $('#buyerEmail').html(total_info.email + ' ' + '<a href="javascript:verifyEmail();">Verify Email</a>');
      $('#verifyemail_customer_email').val(total_info.email);
      $('#verifyemail_customer_id').val(total_info.id);

    } else {
      $('#buyerEmail').html(total_info.email + ' ' + '<a href="javascript:void(0);">[Verified]</a>');
    }


    if (total_info.phone_verify == "0") {
      $('#buyerMobile').html('(+' + total_info.location_key + ') ' + total_info.phone);

    } else {
      $('#buyerMobile').html('(+' + total_info.location_key + ') ' + total_info.phone + ' ' + '<a href="javascript:void(0);">[Verified]</a>');
    }


    if (total_info.buyer_info.profile_image) {
      var profileImg = total_info.buyer_info.profile_image;
      $('#update_pro_img').attr('src', $("#base_url").val() + 'admin/uploads/bussiness_logo/' + profileImg);
    }

    if (total_info.buyer_info) {
      $('#buyerfullName').html(total_info.buyer_info.business_name);
      $('#upd_buyerfullName').val(total_info.buyer_info.business_name);
      $('#buyerAddress').html(total_info.buyer_info.address);
      $('#upd_buyerAddress').val(total_info.buyer_info.address);
      $('#buyerCity').html(total_info.buyer_info.city);
      $('#upd_buyerCity').val(total_info.buyer_info.city);
      $('#buyerState').html(total_info.buyer_info.state);
      $('#buyerCountry').html(total_info.buyer_info.country);
      $('#upd_buyerState').val(total_info.buyer_info.state);
      $('#buyerLanguage').html((total_info.buyer_info.cust_language == "fr") ? "French" : "English");
      $('#upd_buyerLanguage').val(total_info.buyer_info.cust_language);

      buyerprofilebuyerinitializeGoogleAddress();

    } else {
      $('.buyerInfoPanel').remove();
    }

    if (total_info.seller_info && total_info.seller_info.cust_id != '') {
      // display seller info
      $('#sellerBusinessname').html(total_info.seller_info.business_name);
      $('#upd_sellerBusinessname').val(total_info.seller_info.business_name);
      $('#sellerCity').html(total_info.seller_info.city);
      $('#upd_sellerCity').val(total_info.seller_info.city);
      $('#sellerContactname').html(total_info.seller_info.contact_name);
      $('#upd_sellerContactname').val(total_info.seller_info.contact_name);
      $('#sellerAddress').html(total_info.seller_info.address);
      $('#upd_sellerAddress').val(total_info.seller_info.address);
      $('#sellerState').html(total_info.seller_info.state);
      $('#sellerCountry').html(total_info.seller_info.country);
      $('#upd_sellerState').val(total_info.seller_info.state);
      $('#sellerContactNo').html(total_info.seller_info.contact_number);
      $('#upd_sellerContactNo').val(total_info.seller_info.contact_number);
      $('#sellerStoreCurrency').html((total_info.seller_info.store_currency == "€") ? "Euro" : "US Dollar");
      $('#upd_sellerStoreCurrency').val(total_info.seller_info.store_currency);

      if (total_info.seller_info.seller_images) {
        var seller_images_count = 0;
        $.each(total_info.seller_info.seller_images, function (index, iv) {

          var base_url = $("#base_url").val();

          var bigSchema = '<li><img src="' + base_url + 'admin/uploads/seller/' + iv.image + '" alt=""><a href="javascript:void(0);" rel="' + iv.image_id + '" onclick="javascript:delSellerImg(this);" ><i class="fa fa-trash-o" aria-hidden="true"></i></a></li>';

          $("#sellerImgUl").append(bigSchema);

          seller_images_count++;

        });

        if (seller_images_count == 0) {
          $("#sellerImgUl").html('<div style="text-align:center">Upload images to enhance your store profile.</div>');
        }
      }

      if (total_info.seller_info.certificate) {
        var seller_certificate_count = 0;
        $.each(total_info.seller_info.certificate, function (index, iv) {

          var base_url = $("#base_url").val();

          var bigSchema = '<li><img src="' + base_url + 'admin/uploads/seller/' + iv.certificate + '" alt=""><a href="javascript:void(0);" rel="' + iv.id + '" onclick="javascript:delSellerImg(this);" ><i class="fa fa-trash-o" aria-hidden="true"></i></a></li>';

          $("#sellerCertificateImgUl").append(bigSchema);

          seller_certificate_count++;

        });

        if (seller_certificate_count == 0) {
          $("#sellerCertificateImgUl").html('<div style="text-align:center">Upload certificates to enhance your store profile.</div>');
        }

      }


      if (total_info.seller_info.document) {
        var seller_document_count = 0;
        if (total_info.seller_info.document.length > 0) {
          $.each(total_info.seller_info.document, function (index, iv) {

            var base_url = $("#base_url").val();
            var baseImg = 'doc.png';
            if (iv.document_type == 'image/jpeg' || iv.document_type == 'image/jpg' || iv.document_type == 'image/png') {
              baseImg = base_url + 'admin/uploads/seller_docs/' + iv.document;
            } else if (iv.document_type == 'application/pdf') {
              baseImg = base_url + 'assets/images/icon/pdf.png';
            } else {
              baseImg = base_url + 'assets/images/icon/doc.png';
            }

            var bigSchema = '<a href="' + base_url + 'admin/uploads/seller/' + iv.document + '" target="_blank" ><img src="' + baseImg + '" alt="' + baseImg + '" style="width:100px; height:auto;"></a>';

            $("#sellerDocUl").append(bigSchema);

            seller_document_count++;

          });
        }

        if (seller_document_count == 0) {
          $("#sellerDocUl").html('<div style="text-align:center">Submit company documents to admin.</div>');
        }

      }


    } else {
      $('.sellerInfoPanel').remove();
    }



  } else {
    window.location.replace($("#base_url").val() + 'login');
  }

  $('#edit_buyerprofile_modal').on('shown.bs.modal', function () {
    // do something when the modal is shown.
    buyerprofilesellerinitializeGoogleAddress();
  });

  $("#updatebuyerProfileForm").submit(function (e) {
    e.preventDefault();
    var formData = new FormData(this);

    if (checkbuyerProfile()) {
      //alert('buyer done');
      $("#updatebuyerProfileSubmit").prop("disabled", true);
      var location_city = $.trim($('#buyerprofilebuyerlocation_city').val());


      formData.set('location_city', location_city);

      $.ajax({
        type: "POST",
        method: "post",
        enctype: 'multipart/form-data',
        url: $("#base_url").val() + "api/login/updateBuyerInfo",
        data: formData,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        beforeSend: function (xhr) {
          /* Authorization header */
          xhr.setRequestHeader("Authorization", localStorage.getItem("miraimpex_token"));
          xhr.setRequestHeader("X-Mobile", "false");
        },
        success: function (data) {
          //console.log("SUCCESS : ", data);
          toastr.success("Buyer profile updated successfully");
          $("#updatebuyerProfileSubmit").prop("disabled", false);
          document.getElementById('edit_buyerprofile_modal').style.display = 'none';
          doAuthenticateMe();
          setTimeout(
            function () {
              window.location.reload();

            }, 1000);

        },
        error: function (result) {
          toastr.clear();
          toastr.error(result.responseJSON.msg);
          $("#updatebuyerProfileSubmit").prop("disabled", false);

        }
      });

    }


  });

  $("#updatesellerProfileForm").submit(function (e) {
    e.preventDefault();

    if (checksellerProfile()) {
      //alert('seller done');
      $("#updatesellerProfileSubmit").prop("disabled", true);
      var formData = new FormData(this);

      var upd_sellerContactNo = $.trim($('#upd_sellerContactNo').val());
      var new_upd_sellerContactNo = upd_sellerContactNo.replace(/-|\s/g, "");

      formData.set('contact_number', new_upd_sellerContactNo);

      $.ajax({
        type: "POST",
        method: "post",
        enctype: 'multipart/form-data',
        url: $("#base_url").val() + "api/login/updateSellerInfo",
        data: formData,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        beforeSend: function (xhr) {
          /* Authorization header */
          xhr.setRequestHeader("Authorization", localStorage.getItem("miraimpex_token"));
          xhr.setRequestHeader("X-Mobile", "false");
        },
        success: function (data) {
          //console.log("SUCCESS : ", data);
          toastr.success("Seller profile updated successfully");
          $("#updatesellerProfileSubmit").prop("disabled", false);
          document.getElementById('edit_sellerprofile_modal').style.display = 'none';
          doAuthenticateMe();
          setTimeout(
            function () {
              window.location.reload();

            }, 1000);

        },
        error: function (result) {
          toastr.clear();
          toastr.error(result.responseJSON.msg);
          $("#updatesellerProfileSubmit").prop("disabled", false);

        }
      });

    }

  });

  $("#addSellerImageForm").submit(function (e) {
    e.preventDefault();
    //alert('buyer done');
    $("#addSellerImageSubmit").prop("disabled", true);
    var formData = new FormData(this);
    $.ajax({
      type: "POST",
      method: "post",
      enctype: 'multipart/form-data',
      url: $("#base_url").val() + "api/sellerImageAdd",
      data: formData,
      processData: false,
      contentType: false,
      cache: false,
      timeout: 600000,
      beforeSend: function (xhr) {
        /* Authorization header */
        xhr.setRequestHeader("Authorization", localStorage.getItem("miraimpex_token"));
        xhr.setRequestHeader("X-Mobile", "false");
      },
      success: function (data) {
        //console.log("SUCCESS : ", data);
        toastr.success("Seller image added successfully");
        $("#addSellerImageSubmit").prop("disabled", false);
        document.getElementById('add_sellerimage_modal').style.display = 'none';
        doAuthenticateMe();
        setTimeout(
          function () {
            //window.location.reload();

          }, 1000);

      },
      error: function (result) {
        toastr.clear();
        toastr.error(result.responseJSON.msg);
        $("#addSellerImageSubmit").prop("disabled", false);

      }
    });



  });


  // Following function will executes on change event of file input to select different file.
  $('body').on('change', '#gallery-photo-add', function () {
    if (this.files && this.files[0]) {
      abcd += 1; // Incrementing global variable by 1.
      var z = abcd - 1;
      $(this).find('#previewimg' + z).remove();

      $('#testdv').append("<div style='display: inline-block; padding: 14px;'><div id='abcd_" + abcd + "' class='abcd'><img id='sellerpreviewimg" + abcd + "' src='' style='width: 139px; height: 89px;' /><div style='bottom: 0; width: 100%; height: 24px; line-height: 9px; background: #3b485a; opacity: .85;'><a class='remove' href='javascript:void(0);' onclick='return false;' style='display: inline-block; width: 100%; height: 24px; background: url(./assets/images/uploader-icons.png) no-repeat center 3px; text-align: center; background-position: center -37px;'><i class=\"fa fa-trash\" aria-hidden=\"true\" style=\"padding-top: 3px;font-size: 16px;\"></i></a></div></div></div>");

      var reader = new FileReader();
      reader.onload = sellerimageIsLoaded;
      reader.readAsDataURL(this.files[0]);
      $(this).hide();

      $("#abcd_" + abcd).click(function () {
        $(this).parent().remove();
        var id = $(this).attr("id");
        var id_arr = id.split("_");
        var no = (parseInt(id_arr[1]) - 1);
        //console.log(abc,id_arr[1],no);
        if (id_arr.length == 2) {
          $("#sellerfilediv_" + no).val("").trigger("change");
          $("#sellerfilediv_" + no).remove();
        }

      });



      $("#testdv").prepend($("<div id='sellerfilediv_" + abcd + "' class ='testcls' />").fadeIn('slow').prepend($("<input/>", {
        name: 'business_logo[]',
        type: 'file',
        id: 'gallery-photo-add'
      }), $("")));

    }
  });



  $('body').on('change', '#gallery-document-add', function () {
    if (this.files && this.files[0]) {
      abcde += 1; // Incrementing global variable by 1.
      var z = abcde - 1;
      $(this).find('#previewimg' + z).remove();

      $('#testdvdoc').append("<div style='display: inline-block; padding: 14px;'><div id='abcde_" + abcde + "' class='abcd'><img id='sellerdocpreviewimg" + abcde + "' src='' style='width: 139px; height: 89px;' /><div style='bottom: 0; width: 100%; height: 24px; line-height: 9px; background: #3b485a; opacity: .85;'><a class='remove' href='javascript:void(0);' onclick='return false;' style='display: inline-block; width: 100%; height: 24px; background: url(./assets/images/uploader-icons.png) no-repeat center 3px; text-align: center; background-position: center -37px;'><i class=\"fa fa-trash\" aria-hidden=\"true\" style=\"padding-top: 3px;font-size: 16px;\"></i></a></div></div></div>");

      var reader = new FileReader();
      reader.onload = sellerdocumentIsLoaded;
      reader.readAsDataURL(this.files[0]);
      $(this).hide();

      $("#abcde_" + abcde).click(function () {
        $(this).parent().remove();
        var id = $(this).attr("id");
        var id_arr = id.split("_");
        var no = (parseInt(id_arr[1]) - 1);
        //console.log(abc,id_arr[1],no);
        if (id_arr.length == 2) {
          $("#sellerdocfilediv_" + no).val("").trigger("change");
          $("#sellerdocfilediv_" + no).remove();
        }

      });



      $("#testdvdoc").prepend($("<div id='sellerdocfilediv_" + abcde + "' class ='testcls' />").fadeIn('slow').prepend($("<input/>", {
        name: 'business_logo[]',
        type: 'file',
        id: 'gallery-document-add'
      }), $("")));

    }
  });




  $('body').on('change', '#gallery-certificate-add', function () {
    if (this.files && this.files[0]) {
      abcdef += 1; // Incrementing global variable by 1.
      var z = abcdef - 1;
      $(this).find('#previewimg' + z).remove();

      $('#testdvcertificate').append("<div style='display: inline-block; padding: 14px;'><div id='abcdef_" + abcdef + "' class='abcd'><img id='sellercertificatepreviewimg" + abcdef + "' src='' style='width: 139px; height: 89px;' /><div style='bottom: 0; width: 100%; height: 24px; line-height: 9px; background: #3b485a; opacity: .85;'><a class='remove' href='javascript:void(0);' onclick='return false;' style='display: inline-block; width: 100%; height: 24px; background: url(./assets/images/uploader-icons.png) no-repeat center 3px; text-align: center; background-position: center -37px;'><i class=\"fa fa-trash\" aria-hidden=\"true\" style=\"padding-top: 3px;font-size: 16px;\"></i></a></div></div></div>");

      var reader = new FileReader();
      reader.onload = sellercertificateIsLoaded;
      reader.readAsDataURL(this.files[0]);
      $(this).hide();

      $("#abcdef_" + abcdef).click(function () {
        $(this).parent().remove();
        var id = $(this).attr("id");
        var id_arr = id.split("_");
        var no = (parseInt(id_arr[1]) - 1);
        //console.log(abc,id_arr[1],no);
        if (id_arr.length == 2) {
          $("#sellercertificatefilediv_" + no).val("").trigger("change");
          $("#sellercertificatefilediv_" + no).remove();
        }

      });



      $("#testdvcertificate").prepend($("<div id='sellercertificatefilediv_" + abcdef + "' class ='testcls' />").fadeIn('slow').prepend($("<input/>", {
        name: 'business_logo[]',
        type: 'file',
        id: 'gallery-certificate-add'
      }), $("")));

    }
  });




  $("#addSellerCertificateForm").submit(function (e) {
    e.preventDefault();
    alert('buyer done');
    $("#addSellerCertificateSubmit").prop("disabled", true);
    var formData = new FormData(this);
    $.ajax({
      type: "POST",
      method: "post",
      enctype: 'multipart/form-data',
      url: $("#base_url").val() + "api/login/doSellerCertificateAdd",
      data: formData,
      processData: false,
      contentType: false,
      cache: false,
      timeout: 600000,
      beforeSend: function (xhr) {
        /* Authorization header */
        xhr.setRequestHeader("Authorization", localStorage.getItem("miraimpex_token"));
        xhr.setRequestHeader("X-Mobile", "false");
      },
      success: function (data) {
        //console.log("SUCCESS : ", data);
        toastr.success("Company certificate added successfully");
        $("#addSellerCertificateSubmit").prop("disabled", false);
        document.getElementById('add_sellercertificate_modal').style.display = 'none';
        doAuthenticateMe();
        setTimeout(
          function () {
            window.location.reload();

          }, 1000);

      },
      error: function (result) {
        toastr.clear();
        toastr.error(result.responseJSON.msg);
        $("#addSellerCertificateSubmit").prop("disabled", false);

      }
    });



  });


  $("#addSellerDocumentForm").submit(function (e) {
    e.preventDefault();
    //alert('buyer done');
    $("#addSellerDocumentSubmit").prop("disabled", true);
    var formData = new FormData(this);
    $.ajax({
      type: "POST",
      method: "post",
      enctype: 'multipart/form-data',
      url: $("#base_url").val() + "api/login/doSellerDocumentAdd",
      data: formData,
      processData: false,
      contentType: false,
      cache: false,
      timeout: 600000,
      beforeSend: function (xhr) {
        /* Authorization header */
        xhr.setRequestHeader("Authorization", localStorage.getItem("miraimpex_token"));
        xhr.setRequestHeader("X-Mobile", "false");
      },
      success: function (data) {
        //console.log("SUCCESS : ", data);
        toastr.success("Company document added successfully");
        $("#addSellerDocumentSubmit").prop("disabled", false);
        document.getElementById('add_sellerdocument_modal').style.display = 'none';
        doAuthenticateMe();
        setTimeout(
          function () {
            window.location.reload();

          }, 1000);

      },
      error: function (result) {
        toastr.clear();
        toastr.error(result.responseJSON.msg);
        $("#addSellerDocumentSubmit").prop("disabled", false);

      }
    });



  });

});


// To Preview Image
function sellerimageIsLoaded(e) {
  $('#sellerpreviewimg' + abcd).attr('src', e.target.result);
};

function sellercertificateIsLoaded(e) {
  $('#sellercertificatepreviewimg' + abcdef).attr('src', e.target.result);
};

// To Preview Image
function sellerdocumentIsLoaded(e) {
  var encoded = e.target.result;
  var mimeType = '';
  var mime = encoded.match(/data:([a-zA-Z0-9]+\/[a-zA-Z0-9-.+]+).*,.*/);

  if (mime && mime.length) {
    mimeType = mime[1];
  }
  if (mimeType == 'text/plain' || mimeType == 'application/msword' || mimeType == 'application/pdf' || mimeType == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
    $('#sellerdocpreviewimg' + abcde).attr('src', $("#base_url").val() + 'assets/images/icon/doc.png');
  } else {
    $('#sellerdocpreviewimg' + abcde).attr('src', e.target.result);
  }
};


function checkbuyerProfile() {
  toastr.clear();
  var submit = true;
  var total_info = JSON.parse(localStorage.getItem("miraimpex_total_info"));

  if (total_info.buyer_info) {
    if ($.trim($('#upd_buyerfullName').val()) == '') {
      toastr.error('Name is empty!');
      $('#upd_buyerfullName').focus();
      submit = false;
      return false;
    }
    if ($.trim($('#upd_buyerCity').val()) == '') {
      toastr.error('City is empty!');
      $('#upd_buyerCity').focus();
      submit = false;
      return false;
    }
    if ($.trim($('#upd_buyerState').val()) == '') {
      toastr.error('State is empty!');
      $('#upd_buyerState').focus();
      submit = false;
      return false;
    }
    if ($.trim($('#upd_buyerAddress').val()) == '') {
      toastr.error('Address is empty!');
      $('#upd_buyerAddress').focus();
      submit = false;
      return false;
    }
    return true;
  } else {
    return false;
  }


}


function delSellerImg(obj) {
  var itemId = $(obj).attr('rel');
  var that = $(obj);
  var result = confirm("This item will be deleted permanently. Want to delete?");
  if (result) {
    // process img delete
    var data = {};
    data.seller_image_id = itemId;

    $.ajax({
      url: $("#base_url").val() + "api/sellerImageRemove",
      data: data,
      beforeSend: function (xhr) {
        /* Authorization header */
        xhr.setRequestHeader("Authorization", localStorage.getItem("miraimpex_token"));
        xhr.setRequestHeader("X-Mobile", "false");
      },
      method: "post",
      error: function (result) {
        //console.log("Unauthorised");
        toastr.clear();
        toastr.error(result.responseJSON.msg);
      },

      success: function (result) {
        if (result.status === 200) {
          doAuthenticateMe();
          that.parent().remove();
          toastr.clear();
          toastr.success('Image deleted successfully');
        } else {
          console.log(result.status + ':/seller/sellerImageRemove');
        }

      }
    });

  }
}


function checksellerProfile() {
  toastr.clear();
  var submit = true;
  var total_info = JSON.parse(localStorage.getItem("miraimpex_total_info"));

  if (total_info.seller_info) {
    if ($.trim($('#upd_sellerBusinessname').val()) == '') {
      toastr.error('Business name is empty!');
      $('#upd_sellerBusinessname').focus();
      submit = false;
      return false;
    }
    if ($.trim($('#upd_sellerCity').val()) == '') {
      toastr.error('City is empty!');
      $('#upd_sellerCity').focus();
      submit = false;
      return false;
    }
    if ($.trim($('#upd_sellerState').val()) == '') {
      toastr.error('State is empty!');
      $('#upd_sellerState').focus();
      submit = false;
      return false;
    }
    if ($.trim($('#upd_sellerContactname').val()) == '') {
      toastr.error('Contact name is empty!');
      $('#upd_sellerContactname').focus();
      submit = false;
      return false;
    }
    if ($.trim($('#upd_sellerAddress').val()) == '') {
      toastr.error('Address is empty!');
      $('#upd_sellerAddress').focus();
      submit = false;
      return false;
    }
    if ($.trim($('#upd_sellerContactNo').val()) == '') {
      toastr.error('Contact number is empty!');
      $('#upd_sellerContactNo').focus();
      submit = false;
      return false;
    }
  }
  return submit;

}


function verifyEmail() {
  toastr.clear();
  toastr.success('4 digit otp has been sent to your registered email address. Please paste the otp below to complete verification process.');
  //alert('success');
  var data = {};
  data.email = $('#verifyemail_customer_email').val();
  $.ajax({
    url: $("#base_url").val() + "api/verifyemailOTPSend",
    data: data,
    method: "post",
    beforeSend: function (xhr) {
      /* Authorization header */
      xhr.setRequestHeader("Authorization", localStorage.getItem("miraimpex_token"));
      xhr.setRequestHeader("X-Mobile", "false");
    },
    error: function (result) {
      toastr.clear();
      toastr.error(result.responseJSON.message);
    },
    success: function (result) {
      //console.log(result);
      if (result.status === 200) {
        $('#email_verify_otp_modal').css('display', 'block');
      }

    }
  });
  $('#email_verify_otp_modal').css('display', 'block');
}

function verifyEmailOtpForm() {

  var emailOTP = $('#verifyemail_customer_otp').val();
  alert(emailOTP);
  if (emailOTP.length < 4) {
    toastr.clear();
    toastr.error('Invalid OTP!');
    $('#verifyemail_customer_otp').focus();
    return false;
  }

  return true;
}

$(document).ready(function () {

  $("#email_verify_otp_modal_Form").submit(function (e) {
    e.preventDefault();
    $('#email_verify_otp_modal_Submit').prop("disabled", true);
    if (verifyEmailOtpForm()) {
      alert('success');
      var data = {};
      data.email = $('#verifyemail_customer_email').val();
      data.otp = $('#verifyemail_customer_otp').val();
      $.ajax({
        url: $("#base_url").val() + "api/verifyemailupdate",
        data: data,
        method: "post",
        beforeSend: function (xhr) {
          /* Authorization header */
          xhr.setRequestHeader("Authorization", localStorage.getItem("miraimpex_token"));
          xhr.setRequestHeader("X-Mobile", "false");
        },
        error: function (result) {
          toastr.clear();
          toastr.error(result.responseJSON.message);
          $('#email_verify_otp_modal_Submit').prop("disabled", false);
        },
        success: function (result) {
          //console.log(result);
          if (result.status === 200) {
            toastr.clear();
            toastr.success(result.msg);
            doAuthenticateMe();
            setTimeout(
              function () {

                window.location.replace($("#base_url").val() + 'profile');

              }, 1000);


          } else {

            $('#email_verify_otp_modal_Submit').prop("disabled", false);
            console.log(result.status + ':verifyemailupdate');
          }

        }
      });
    }
  });

  $("#verifyemail_customer_otp").keypress(function (e) {
    //if the letter is not digit then display error and don't type anything
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
      //display error message
      toastr.clear();
      toastr.error('Digits only!');
      return false;
    }
  });
});