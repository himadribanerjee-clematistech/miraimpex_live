

$(function () {
	toastr.clear();

	//check Login
	if (localStorage.getItem("miraimpex_token") === null) {
		window.location.replace($("#base_url").val());
	}
	else if (JSON.parse(localStorage.getItem("miraimpex_total_info")).supplier_YN == 0) {
		toastr.error('You don\'t have product Rights.');
		window.location.replace($("#base_url").val());
	}

	getCategoryList();
	getUOMList();
	getVarianceList();
	getAttributeList();

	languageConvertor('item_name_en', 'item_name_fr', 'item_translator_title');
	languageConvertor('item_desc_en', 'item_desc_fr', 'item_desc_translator');

	$("#min_price,#max_price").keypress(function (e) {
		//if the letter is not digit then display error and don't type anything
		if (e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57)) {
			//display error message
			return false;
		}
	});

	$("#min_price,#max_price").blur(function (e) {
		var val = $(this).val();
		if ($.trim(val) != '')
			$(this).val((parseFloat(val).toFixed(2)));
	});



	$(".price").keyup(function (e) {
		var priceX = $(this).val();
		var origin = $(this).attr('id');
		var GMDprice = priceX * gambianDalasi2USDexchangeRate;
		GMDprice = (Math.round(GMDprice * 100) / 100).toFixed(2);
		$('#' + origin + '_approx').html('Approx ' + GMDprice + ' GMD');
		//console.log(origin + '=' + priceX);
	});



})


var cateGoryData;
var attributeData = [{
	id: '-1',
	text: 'Label( eg. Material, Flavour, Expiry date etc)'
}];
function getCategoryList() {

	$.ajax({
		url: $('#base_url').val() + "api/main/getCategoryChain",
		cache: false,
		type: "POST",
		async: "false",
		dataType: "json",
		data: {
			'lang': $('#viewLang').val(),
			'level': '4',
			'type': '1',
		},
		beforeSend: function (result) {
			$('.loading').removeClass('hide');
			toastr.clear();
		},
		success: function (res) {
			$('.loading').addClass('hide');
			cateGoryData = res.result;

			// $("#category_level_1").select2({
			// 	data: cateGoryData,
			// 	placeholder: 'Label',
			// 	minimumResultsForSearch: 3,
			// 	tags: true
			// });

			populateFirstcategory();
		}
	});

}

function populateFirstcategory() {

	for (const [key, value] of Object.entries(cateGoryData)) {
		if (atob(key) == 0) {
			for ($i = 0; $i < value.length; $i++) {
				$('#category_level_1').append('<option  value="' + atob(value[$i]['id']) + '">' + value[$i]['category_name'] + '</option>');
			}

		}
	}
}

function select_cat_level($index) {
	var seldr = $('#category_level_' + (parseInt($index) + 1));
	//console.log($index);
	seldr.html('').addClass('hide');
	var flag = false;
	for (const [key, value] of Object.entries(cateGoryData)) {
		if (atob(key) == $('#category_level_' + $index).val()) {
			seldr.append('<option  value="">-- Please select --</option>');
			for ($i = 0; $i < value.length; $i++) {
				seldr.append('<option  value="' + atob(value[$i]['id']) + '">' + value[$i]['category_name'] + '</option>');
				flag = true;
			}

		}
	}

	if (flag)
		seldr.removeClass('hide');
	for ($j = parseInt($index) + 2; $j <= 4; $j++)
		$('#category_level_' + $j).addClass('hide');
}

function select_cat($index) {
	eval(select_cat_level)($index);
}


function getUOMList() {

	$.ajax({
		url: $('#base_url').val() + "api/main/getuomlist",
		cache: false,
		type: "POST",
		async: "false",
		dataType: "json",
		data: {
			'lang': $('#viewLang').val(),
		},
		beforeSend: function (result) {
			$('.loading').removeClass('hide');
			toastr.clear();
		},
		success: function (result) {
			$('.loading').addClass('hide');
			for (const [key, value] of Object.entries(result.uom_data)) {

				if (value.type == '1') {
					if (value.name == 'Unit') var sel = 'selected'; else sel = '';
					$('#uom_1,#uom_2').append('<option ' + sel + '  value="' + atob(value.id) + '">' + value.name + '</option>');
				}

			}
		}
	});

}

var varienceList = [];
function getVarianceList() {

	$.ajax({
		url: $('#base_url').val() + "api/main/getVariance",
		cache: false,
		type: "POST",
		async: "false",
		dataType: "json",
		data: {
			'lang': $('#viewLang').val(),
			'type': 1
		},
		beforeSend: function (result) {
			$('.loading').removeClass('hide');
			toastr.clear();
		},
		success: function (result) {
			$('.loading').addClass('hide');
			$html = ''; varienceList = result.result;
			for (const [key, value] of Object.entries(result.result)) {
				$id = atob(value.id);
				$name = value.name;
				var example = '';
				if ($name == 'Color') {
					example = 'eg. Red,Green,Yellow';
				}
				if ($name == 'Size') {
					example = 'eg. XL,XXL,XXL';
				}
				if ($name == 'Variety') {
					example = 'eg. Toned Milk,Double Toned Milk,Full Cream Milk';
				}
				var $html = '<div class="form-group col-sm-4"><span style="font-weight:bold;">' + $name + '</span> (comma separated):'
					+ '<select class="form-control" id="vari_' + $id + '" name="variance" multiple="multiple" >'
					+ '</select>'
					+ '</div>';
				$('#varience_div').append($html);
				$('#vari_' + $id).select2({
					placeholder: example,
					tags: true,
					tokenSeparators: [','],
					minimumResultsForSearch: -1,
					language: {
						noResults: function (params) {
							return 'Please enter';
						}
					}
				});

			}
		}
	});

}


function getAttributeList() {

	$.ajax({
		url: $('#base_url').val() + "api/main/getAttribute",
		cache: false,
		type: "POST",
		async: "false",
		dataType: "json",
		data: {
			'lang': $('#viewLang').val(),
			'type': '1'
		},
		beforeSend: function (result) {
			$('.loading').removeClass('hide');
			toastr.clear();
		},
		success: function (result) {
			$('.loading').addClass('hide');
			for (const [key, value] of Object.entries(result.result)) {
				attributeData.push({
					id: value.name,
					text: value.name
				})
			}
			$("#attribute_1_sel").select2({
				data: attributeData,
				placeholder: 'Label( eg. Material, Flavour, Expiry date etc)',
				minimumResultsForSearch: 3,
				tags: true
			})
			$("#attribute_1_sel>option[value='-1']").attr('disabled', 'disabled');
		}
	});

}
var attributeIndex = 1;
function addAttributes() {
	attributeIndex++;
	$html = '<div class="col-sm-12" id="attribute_' + attributeIndex + '_div" >'
		+ '<div class="form-group col-sm-4">'
		+ '<select class="form-control" id="attribute_' + attributeIndex + '_sel" name="attribute_' + attributeIndex + '_sel"  >'
		+ '</select>'
		+ '</div>'
		+ '<div class="form-group col-sm-4">'
		+ '<input type="text" class="form-control" placeholder="Value"  id="attribute_' + attributeIndex + '_txt" name="attribute_' + attributeIndex + '_txt" />'
		+ '</div>'
		+ '<div class="form-group col-sm-2">'
		+ '<a  href="javascript:void(0)" onclick="$(\'#attribute_' + attributeIndex + '_div\').remove();"   >Remove</a>'
		+ '</div>'
		+ '</div>';
	$('#attribute_div').append($html);
	$("#attribute_" + attributeIndex + "_sel").select2({
		data: attributeData,
		placeholder: 'Label( eg. Material, Flavour, Expiry date etc)',
		minimumResultsForSearch: 3,
		tags: true
	})
	$("#attribute_" + attributeIndex + "_sel>option[value='-1']").attr('disabled', 'disabled');
}


function languageConvertor(leftel, rightel, btn) {


	$('#' + leftel).keyup(function () {
		if ($.trim($('#' + leftel).val()).length > 0) {
			$('#' + btn).html('>>');
			$('#' + btn).removeClass('hide');
			$('#' + btn).attr('lang', 'fr');
			$('#' + btn).attr('title', 'Transalate in french');
		}
		else {
			$('#' + btn).html('');
			$('#' + btn).addClass('hide');
		}
	})

	$('#' + rightel).keyup(function () {
		if ($.trim($('#' + rightel).val()).length > 0) {
			$('#' + btn).html('<<');
			$('#' + btn).removeClass('hide');
			$('#' + btn).attr('lang', 'en');
			$('#' + btn).attr('title', 'Transalate in english');
		}
		else {
			$('#' + btn).html('');
			$('#' + btn).addClass('hide');
		}
	})


	$('#' + btn).click(function () { return transalateApi(leftel, rightel, btn) });

}

function transalateApi(leftel, rightel, btn) {

	var lang = $('#' + btn).attr('lang');

	if (lang != '') {

		if (lang == 'fr')
			var $str = $('#' + leftel).val();
		else if (lang == 'en')
			var $str = $('#' + rightel).val();

		$.ajax({
			url: $('#base_url').val() + "seller/item/getLangApi",
			cache: false,
			type: "POST",
			async: "false",
			dataType: "json",
			data: {
				'lang': lang,
				'str': $str,
			},
			beforeSend: function () {
				$('.loading').removeClass('hide');
			},
			success: function (res) {
				$('.loading').addClass('hide');
				if (lang == 'fr')
					$('#' + rightel).val(res.lang);
				else if (lang == 'en')
					$('#' + leftel).val(res.lang);
			}
		});

	}

	return false;
}

function b64toBlob(b64Data, contentType, sliceSize) {
	contentType = contentType || '';
	sliceSize = sliceSize || 512;

	var byteCharacters = atob(b64Data);
	var byteArrays = [];

	for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
		var slice = byteCharacters.slice(offset, offset + sliceSize);

		var byteNumbers = new Array(slice.length);
		for (var i = 0; i < slice.length; i++) {
			byteNumbers[i] = slice.charCodeAt(i);
		}

		var byteArray = new Uint8Array(byteNumbers);

		byteArrays.push(byteArray);
	}

	var blob = new Blob(byteArrays, { type: contentType });
	return blob;
}


//Image Upload start here
var thumbnail = [];
$("#thumbnail_image").on('change', function () {

	//Get count of selected files
	var countFiles = $(this)[0].files.length;

	var imgPath = $(this)[0].value;
	var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
	if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
		if (typeof (FileReader) != "undefined") {

			//loop for each file selected for uploaded.
			for (var i = 0; i < countFiles; i++) {

				var reader = new FileReader();
				reader.onload = function (e) {
					var file = e.target;
					var filename = $("#thumbnail_image").val().split('\\').slice(-1)[0];
					$('#thmpname').html(filename);
					thumbnail = [{
						name: filename,
						value: e.target.result
					}];
				}

				reader.readAsDataURL($(this)[0].files[i]);
			}

		} else {
			alert("This browser does not support FileReader.");
		}
	} else {
		toastr.clear();
		toastr.error('Please upload only image!');
	}
});

var moreImageIndex = 0;
function uploadImage($index) {
	moreImageIndex = $index;
	$('#browse_image').click();
}

var moreImage = [];
$("#browse_image").on('change', function () {

	//Get count of selected files
	var countFiles = $(this)[0].files.length;

	var imgPath = $(this)[0].value;
	var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
	if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
		if (typeof (FileReader) != "undefined") {

			//loop for each file selected for uploaded.
			for (var i = 0; i < countFiles; i++) {

				var reader = new FileReader();
				reader.onload = function (e) {
					var file = e.target;
					var filename = $("#browse_image").val().split('\\').slice(-1)[0];
					moreImage.push({
						name: filename,
						value: e.target.result,
						index: moreImageIndex
					});
					$('#btn_browse_' + moreImageIndex).addClass('hide');
					$('#btn_browse_rm_' + moreImageIndex).removeClass('hide');
					$('#more_img_' + moreImageIndex).attr('src', e.target.result);
				}

				reader.readAsDataURL($(this)[0].files[i]);
			}

		} else {
			alert("This browser does not support FileReader.");
		}
	} else {
		toastr.clear();
		toastr.error('Please upload only image!');
	}
});


function removeImage($index) {

	var imagedata = [];
	console.log(moreImage);
	for ($i = 0; $i < moreImage.length; $i++) {
		if (moreImage[$i]['index'] == $index) {
			$('#btn_browse_' + $index).removeClass('hide');
			$('#btn_browse_rm_' + $index).addClass('hide');
			$('#more_img_' + $index).attr('src', $('#base_url').val() + 'assets/images/no-images.png');
			continue;
		}
		imagedata.push({
			name: moreImage[$i]['filename'],
			value: moreImage[$i]['value'],
			index: ($i + 1)
		});

	}
	moreImage = imagedata;
}

function add_item() {


	toastr.clear();

	if ($.trim($('#category_level_1').val()) == '') {
		toastr.error('Please select category!');
		return;
	}

	if (!$('#category_level_2').is(":hidden"))
		if ($.trim($('#category_level_2').val()) == '') {
			toastr.error('Please select sub category!');
			$('#category_level_2').focus();
			return;
		}

	if (!$('#category_level_3').is(":hidden"))
		if ($.trim($('#category_level_3').val()) == '') {
			toastr.error('Please select sub category!');
			$('#category_level_3').focus();
			return;
		}

	if (!$('#category_level_4').is(":hidden"))
		if ($.trim($('#category_level_4').val()) == '') {
			toastr.error('Please select sub category!');
			$('#category_level_4').focus();
			return;
		}


	if ($.trim($('#item_name_en').val()) == '') {
		toastr.error('Product title in english required!');
		return;
	}

	if ($.trim($('#item_name_fr').val()) == '') {
		toastr.error('Product title in french required!');
		return;
	}

	if ($.trim($('#item_desc_en').val()) == '') {
		toastr.error('Product Details in english required!');
		return;
	}

	if ($.trim($('#item_desc_fr').val()) == '') {
		toastr.error('Product Details in french required!');
		return;
	}


	if ($.trim($('#thumbnail_image').val()) == '') {
		toastr.error('Thumbnail image required!');
		return;
	}

	if ($.trim($('#item_keywords').val()) == '') {
		toastr.error('keywords required!');
		return;
	}

	if ($.trim($('#min_price').val()) == '') {
		toastr.error('Min Price required!');
		return;
	}

	if ($.trim($('#max_price').val()) == '') {
		toastr.error('Max Price required!');
		return;
	}

	if ($.trim($('#min_order').val()) == '') {
		toastr.error('Min order required!');
		return;
	}

	if ($.trim($('#min_order').val()) == '') {
		toastr.error('Min order required!');
		return;
	}


	if (($('#min_price').val() != '') && ($('#max_price').val() != ''))
		if (parseFloat($('#min_price').val()) >= parseFloat($('#max_price').val())) {
			toastr.clear();
			toastr.error('Max price should be greater min price');
			return false;
		}

	var attributeData = [];
	for ($j = 1; $j <= attributeIndex; $j++) {

		if ($('#attribute_' + $j + '_sel').length > 0) {
			if ($('#attribute_' + $j + '_sel').val() != null) {
				if ($.trim($('#attribute_' + $j + '_txt').val()) == '') {
					toastr.error('Attribute value required!');
					return;
				}

				var key = $('#attribute_' + $j + '_sel').val();
				var obj = {};
				obj[key] = $.trim($('#attribute_' + $j + '_txt').val());
				attributeData.push(obj);

			}
		}
	}
	attributeData = JSON.stringify(attributeData);


	var catArr = [];
	var catId = '';
	if ($.trim($('#category_level_1').val()) != '') {
		catArr.push($('#category_level_1').val());
		catId = $('#category_level_1').val();
	}

	if (!$('#category_level_2').is(":hidden"))
		if ($.trim($('#category_level_2').val()) != '') {
			catArr.push($('#category_level_2').val());
			catId = $('#category_level_2').val();
		}

	if (!$('#category_level_3').is(":hidden"))
		if ($.trim($('#category_level_3').val()) != '') {
			catArr.push($('#category_level_3').val());
			catId = $('#category_level_3').val();
		}

	if (!$('#category_level_4').is(":hidden"))
		if ($.trim($('#category_level_4').val()) != '') {
			catArr.push($('#category_level_4').val());
			catId = $('#category_level_4').val();
		}


	var formData = new FormData();
	formData.append('local_sup_id', btoa(localStorage.getItem("miraimpex_seller_id")));
	formData.append('item_name_en', $('#item_name_en').val());
	formData.append('item_name_fr', $('#item_name_fr').val());
	formData.append('description_en', $('#item_desc_en').val());
	formData.append('description_fr', $('#item_desc_fr').val());
	formData.append('category_id', btoa(catId));
	formData.append('fixedPrice', $('#fixed_price').val());
	formData.append('type', '1');
	formData.append('price_min', $('#min_price').val());
	formData.append('price_max', $('#max_price').val());
	formData.append('currency', $('#currency').val());
	formData.append('price_uom_id', btoa($('#uom_1').val()));
	formData.append('moq', $('#min_order').val());
	formData.append('moq_uom_id', btoa($('#uom_2').val()));
	formData.append('packaging', $('#packaging').val());
	formData.append('lead_time', $('#lead_time').val());
	formData.append('showcase', $('#showcase').val());
	formData.append('on_display', $('#status').val());
	formData.append('keywords', $('#item_keywords').val());

	formData.append('keywords', $('#item_keywords').val());
	formData.append('delivery_available', $('#delivery_available').val());
	formData.append('shipping_time', $('#shipping_time').val());

	for ($k = 0; $k < catArr.length; $k++)
		formData.append('category_json[]', '[' + btoa(catArr[$k]) + ']');

	//add thumbnail image
	var ImageURL = thumbnail[0]['value'];
	var block = ImageURL.split(";");
	var contentType = block[0].split(":")[1];
	var imageType = contentType.split("/")[1];
	var realData = block[1].split(",")[1];
	var blob = b64toBlob(realData, contentType);
	var imageName = thumbnail[0]['name'];
	formData.append('thumbnail', blob, imageName);

	var varianceData = [];
	for (const [key_data, value] of Object.entries(varienceList)) {
		$id = atob(value.id);
		if ($('#vari_' + $id).val() != null) {

			var key = $id;
			var obj = {};
			obj[key] = $('#vari_' + $id).val();
			varianceData.push(obj);

		}
	}
	varianceData = JSON.stringify(varianceData);
	formData.append('variance', varianceData);
	formData.append('attribute', attributeData);

	for ($k = 0; $k < moreImage.length; $k++) {

		var ImageURL = moreImage[$k]['value'];
		var block = ImageURL.split(";");
		var contentType = block[0].split(":")[1];
		var imageType = contentType.split("/")[1];
		var realData = block[1].split(",")[1];
		var blob = b64toBlob(realData, contentType);
		var imageName = moreImage[$k]['name'];
		formData.append('more_image[]', blob, imageName);
	}




	$.ajax({
		url: $('#base_url').val() + "api/item/addItem",
		data: formData,
		beforeSend: function (xhr) {
			xhr.setRequestHeader("Authorization", localStorage.getItem("miraimpex_token"));
			xhr.setRequestHeader("X-Mobile", "false");
			$('.loading').removeClass('hide');
		},
		processData: false,
		contentType: false,
		method: "post",
		error: function (result) {
			$('.loading').addClass('hide');
			toastr.clear();
			toastr.error(result.responseJSON.msg);
		},
		success: function (result) {
			$('.loading').addClass('hide');
			toastr.clear();
			toastr.success(result.msg);
			setTimeout(function () { window.location.href = $('#base_url').val() + 'profile/myproducts'; }, 4000);
		}
	})


}








