//Developer: Prosenjit Dutta
$(function () {
	getSellerProductList();
})

function deleteItem($itemId) {

	if (confirm(error_message['are_you_sure_delete'])) {


		var formData = new FormData();
		formData.append('item_id', btoa($itemId));
		formData.append('lang', $('#viewLang').val());

		$.ajax({
			url: $('#base_url').val() + "api/item/deleteSellerItem",
			data: formData,
			beforeSend: function (xhr) {
				xhr.setRequestHeader("Authorization", localStorage.getItem("miraimpex_token"));
				xhr.setRequestHeader("X-Mobile", "false");
				$('.loading').removeClass('hide');
			},
			processData: false,
			contentType: false,
			method: "post",
			error: function (result) {
				$('.loading').addClass('hide');
				console.log(result);
				toastr.clear();
				toastr.error(result.responseJSON.msg);
			},
			success: function (result) {
				$('.loading').addClass('hide');
				toastr.clear();
				toastr.success(result.msg);
				setTimeout(function () { window.location.reload(); }, 1500);
			}
		})


	}

	return true;
}


function getSellerProductList() {
	$html = '';
	$.ajax({
		url: $('#base_url').val() + "api/item/getsellerItem",
		cache: false,
		type: "POST",
		async: "false",
		dataType: "json",
		beforeSend: function (xhr) {
			xhr.setRequestHeader("Authorization", localStorage.getItem("miraimpex_token"));
			xhr.setRequestHeader("X-Mobile", "false");
			$('.loading').removeClass('hide');
		},
		error: function (result) {
			$('.loading').addClass('hide');
			toastr.clear();
			toastr.error(result.msg);
		},
		success: function (res) {
			$('.loading').addClass('hide');
			var ItemData = res.item;
			for ($i = 0; $i < ItemData.length; $i++) {
				$html += '<div class="col-sm-3 my_pro_item">'
					+ '<div class="pro_img">'
					+ '<img src="' + $('#base_url').val() + 'admin/uploads/selleritem/' + ItemData[$i]['thumbnail'] + '" alt="" />'
					+ '</div>'
					+ '<div class="product_info">'
					+ '<a href="' + $('#base_url').val() + 'product/details/' + ItemData[$i]['item_id'] + '" target="_blank ">'
					+ '<h4>' + (($('#viewLang').val() == 'en') ? ItemData[$i]['item_name_en'] : ItemData[$i]['item_name_fr']) + '</h4>'
					+ '</a>'
					+ '<p>' + (($('#viewLang').val() == 'fr') ? ItemData[$i]['description_fr'] : ItemData[$i]['description_en']) + '</p>'
					+ '<div>'
					+ '<a href="' + $('#base_url').val() + 'seller/item/editItem/' + ItemData[$i]['item_id'] + '"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</a>'
					+ '<a href="javascript:void(0)" onclick="deleteItem(\'' + atob(ItemData[$i]['item_id']) + '\')"   ><i class="fa fa-trash" aria-hidden="true"></i> Delete</a>'
					+ '</div>'
					+ '</div>'
					+ '</div>';
			}
			if (ItemData.length == 0) {
				$html = '<div class="col-sm-3 my_pro_item">You have no item</div>';
			}
			$('#productList').html($html);
		}
	});

}



