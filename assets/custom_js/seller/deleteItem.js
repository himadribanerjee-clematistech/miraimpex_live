//Developer: Prosenjit Dutta

function deleteItem($itemId){ 

if(confirm(error_message['are_you_sure_delete'])){
	   
	
	var formData = new FormData();
    formData.append('item_id', btoa($itemId));
	formData.append('lang', $('#viewLang').val());
	
	$.ajax({
      url: $('#base_url').val() + "api/item/deleteSellerItem",
      data: formData,
      beforeSend: function (xhr) {
        xhr.setRequestHeader("Authorization", localStorage.getItem("miraimpex_token"));
        xhr.setRequestHeader("X-Mobile", "false");
		$('.loading').removeClass('hide');  
      },
	  processData: false,
      contentType: false,	
      method: "post",
      error: function (result) {
		$('.loading').addClass('hide');      
        console.log(result);
        toastr.clear();
        toastr.error(result.responseJSON.msg);
      },
      success: function (result) {
		$('.loading').addClass('hide');    
        toastr.clear();
        toastr.success(result.msg);
		setTimeout(function(){  window.location.reload(); }, 4000);	   
	  }
	})
		
		
}

	return true;
}

	

