(function ($) {
	"use strict";
	var Eshop = {
		/*START OF ESHOP*/
		initialised: false,
		Init: function () {
			if (!this.initialised) {
				this.initialised = true;
			} else {
				return;
			}
			this.CategoriesMenu();
			this.OwlSlider();

		},
		CategoriesMenu: function () {

			$('.category-contant ul li a').on('click', function (e) {
				var $this = $(this);

			});

			$('#categories').on('click', function (e) {
				var $this = $(this),
					container = $('#category-menu');

				if ($this.hasClass('active')) {
					container.slideUp(300, function () {
						$this.removeClass('active');
					});

				} else {
					container.slideDown(300, function () {
						$this.addClass('active');
					});
				}
				e.preventDefault();
			});
		},


		OwlSlider: function () {

			$(".owl-carousel.top_slider").owlCarousel({
				items: 1,
				itemsDesktop: [1200, 3],
				itemsDesktopSmall: [1199, 3],
				itemsTablet: [950, 3],
				itemsTabletSmall: [767, 3],
				itemsMobile: [479, 2],
				slideSpeed: 2000,
				autoPlay: 4000,
				stopOnHover: true,
				navigation: true,
				pagination: false,
				responsive: true,
				autoHeight: true,
				navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"]
			});
			$(".owl-carousel.register_slider").owlCarousel({
				items: 5,
				itemsDesktop: [1200, 3],
				itemsDesktopSmall: [1199, 3],
				itemsTablet: [950, 3],
				itemsTabletSmall: [767, 3],
				itemsMobile: [479, 2],
				slideSpeed: 2000,
				autoPlay: 6000,
				stopOnHover: true,
				navigation: true,
				pagination: false,
				responsive: true,
				autoHeight: true,
				navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"]
			});
			$(".owl-carousel.week-deal-products").owlCarousel({
				items: 1,
				itemsDesktop: [1200, 3],
				itemsDesktopSmall: [1199, 3],
				itemsTablet: [950, 3],
				itemsTabletSmall: [767, 3],
				itemsMobile: [479, 2],
				slideSpeed: 600,
				autoPlay: 3000,
				stopOnHover: true,
				navigation: true,
				pagination: false,
				responsive: true,
				autoHeight: true,
				navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"]
			});
			$(".owl-carousel.week-deal-Services").owlCarousel({
				items: 1,
				itemsDesktop: [1200, 3],
				itemsDesktopSmall: [1199, 3],
				itemsTablet: [950, 3],
				itemsTabletSmall: [767, 3],
				itemsMobile: [479, 2],
				slideSpeed: 600,
				autoPlay: 6000,
				stopOnHover: true,
				navigation: true,
				pagination: false,
				responsive: true,
				autoHeight: true,
				navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"]
			});

			$(".owl-carousel.brand").owlCarousel({
				items: 5,
				itemsDesktop: [1200, 5],
				itemsDesktopSmall: [1199, 5],
				itemsTablet: [950, 4],
				itemsTabletSmall: [767, 3],
				itemsMobile: [479, 2],
				slideSpeed: 600,
				autoPlay: 6000,
				stopOnHover: true,
				navigation: true,
				pagination: false,
				responsive: true,
				autoHeight: true,
				navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"]
			});



			$(".owl-carousel.item-block").owlCarousel({
				items: 4,
				itemsDesktop: [1199, 4],
				itemsDesktopSmall: [1200, 4],
				itemsTablet: [950, 4],
				itemsTabletSmall: [767, 4],
				itemsMobile: [479, 2],
				slideSpeed: 600,
				autoPlay: false,
				stopOnHover: true,
				navigation: true,
				pagination: false,
				responsive: true,
				autoHeight: true,
				navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"]

			});

			$(".blog-main.owl-carousel").owlCarousel({
				items: 2,
				itemsDesktop: [1199, 2],
				itemsDesktopSmall: [1200, 2],
				itemsTablet: [950, 2],
				itemsTabletSmall: [767, 1],
				itemsMobile: [479, 1],
				slideSpeed: 600,
				autoPlay: false,
				stopOnHover: true,
				navigation: true,
				pagination: false,
				responsive: true,
				autoHeight: true,
				navigationText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"]
			});
		}

		/*END OF ESHOP*/
	};
	// Load Events
	$(window).on('load', function () {
		Eshop.Init();
	});




})(jQuery);






var btn = $('#top-to-bottom');

$(window).scroll(function () {
	if ($(window).scrollTop() > 300) {
		btn.addClass('show');
	} else {
		btn.removeClass('show');
	}
});

btn.on('click', function (e) {
	e.preventDefault();
	$('body,html').animate({
		scrollTop: 0                       // Scroll to top of body
	}, 2000);
});





















