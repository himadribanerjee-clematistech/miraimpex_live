-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 19, 2020 at 07:16 PM
-- Server version: 5.7.27-0ubuntu0.16.04.1
-- PHP Version: 7.0.33-0ubuntu0.16.04.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `miraimpex`
--

-- --------------------------------------------------------

--
-- Table structure for table `t_agent_document`
--

CREATE TABLE `t_agent_document` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cust_id` bigint(20) UNSIGNED NOT NULL,
  `document` varchar(255) DEFAULT NULL,
  `document_type` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_agent_document`
--

INSERT INTO `t_agent_document` (`id`, `cust_id`, `document`, `document_type`) VALUES
(1, 3, '1576054698-SampleDocumentImagePDF.png', 'image/png');

-- --------------------------------------------------------

--
-- Table structure for table `t_agent_package`
--

CREATE TABLE `t_agent_package` (
  `id` int(10) UNSIGNED NOT NULL,
  `no_of_months` bigint(20) UNSIGNED DEFAULT NULL,
  `price` double(11,2) UNSIGNED DEFAULT NULL,
  `status` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_attribute`
--

CREATE TABLE `t_attribute` (
  `id` int(11) NOT NULL,
  `name_en` varchar(100) NOT NULL,
  `name_fr` varchar(255) NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 - Products 2 - Services',
  `status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_attribute`
--

INSERT INTO `t_attribute` (`id`, `name_en`, `name_fr`, `type`, `status`) VALUES
(1, 'Back Camera', 'Caméra arrière', 1, 1),
(2, 'Brand', 'Marque', 1, 1),
(3, 'Camera', 'Caméra', 1, 1),
(4, 'Capacity', 'Capacité', 1, 1),
(5, 'Cellular', 'Cellulaire', 1, 1),
(6, 'Certificate', 'Certificat', 1, 1),
(7, 'CPU', 'CPU', 1, 1),
(8, 'Display Color', 'Couleur d\'affichage', 1, 1),
(9, 'Display Type', 'Type d\'affichage', 1, 1),
(10, 'Feature', 'Fonctionnalité', 1, 1),
(11, 'Gender', 'Le sexe', 1, 1),
(12, 'Ingredient', 'Ingrédient', 1, 1),
(13, 'Material', 'Matériel', 1, 1),
(14, 'Model', 'Modèle', 1, 1),
(15, 'Power', 'Puissance', 1, 1),
(16, 'Power Supply', 'Source de courant', 1, 1),
(17, 'Product Origin', 'Origine du produit', 1, 1),
(18, 'Product Type', 'type de produit', 1, 1),
(19, 'RAM', 'RAM', 1, 1),
(20, 'ROM', 'ROM', 1, 1),
(21, 'Resolution', 'Résolution', 1, 1),
(22, 'Screen', ' Écran', 1, 1),
(23, 'Screen Resolution', 'Résolution d\'écran', 1, 1),
(24, 'Shape', 'Forme', 1, 1),
(25, 'Smell', 'Odeur', 1, 1),
(26, 'Specification', 'spécification', 1, 1),
(27, 'Standard', ' la norme', 1, 1),
(28, 'Style', 'Style', 1, 1),
(29, 'Tolerance', 'Tolérance', 1, 1),
(30, 'Torque Capacity', 'Capacité de couple', 1, 1),
(31, 'Type', 'Type', 1, 1),
(32, 'Variety', 'Variété', 1, 1),
(33, 'Voltage', 'Tension', 1, 1),
(34, 'Warranty', 'garantie', 1, 1),
(35, 'Weight', 'Poids', 1, 1),
(36, 'Size', 'Taille', 1, 1),
(37, 'Color', ' Couleur', 1, 1),
(38, 'Length', 'Longueur', 1, 1),
(39, 'Height', 'la taille', 1, 1),
(40, 'Breadth', 'Largeur', 1, 1),
(41, 'Volume', 'Le volume', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_banner`
--

CREATE TABLE `t_banner` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_buyer_image`
--

CREATE TABLE `t_buyer_image` (
  `id` int(11) NOT NULL,
  `buyer_id` int(11) NOT NULL,
  `image` varchar(150) NOT NULL,
  `active` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_buyer_profile`
--

CREATE TABLE `t_buyer_profile` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cust_id` bigint(20) UNSIGNED DEFAULT NULL,
  `business_name` varchar(255) DEFAULT NULL,
  `business_logo` varchar(255) DEFAULT NULL,
  `address` text,
  `country` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `lat` varchar(255) DEFAULT NULL,
  `long` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  `update_by` bigint(20) UNSIGNED DEFAULT NULL,
  `update_on` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_buyer_profile`
--

INSERT INTO `t_buyer_profile` (`id`, `cust_id`, `business_name`, `business_logo`, `address`, `country`, `state`, `city`, `lat`, `long`, `active`, `update_by`, `update_on`) VALUES
(1, 1, 'Himadri Banerjee', '1582091690-Architect.jpeg', 'Kanchrapara, West Bengal, India', '', 'West Bengal', 'Kanchrapara', '22.9430915', '88.43611480000001', 1, 1, '2020-02-19 05:54:50'),
(2, 2, 'ani pvt ltd', '1581922092-house-boat.jpg', 'salt lake', 'Nigeria', ' west bengal', 'kolkata', '', '', 1, 2, '2020-02-17 06:48:12'),
(3, 3, '', '1581923211-Tractor-Operator-Training-kaoma-2.-13.9.19-1024x768.jpg', 'Roynagar. madhyapara. Kolkata.', '', 'West Bengal', 'Bansdroni', '', '', 1, 3, '2020-02-17 07:06:51'),
(4, 4, 'Clematis', NULL, NULL, 'Namibia', 'Windhoek', NULL, NULL, NULL, 1, 1, '2020-02-17 06:40:23'),
(7, 7, 'Zodiac Lifecare', NULL, NULL, 'Gambia', 'Estuaire', NULL, NULL, NULL, 1, NULL, NULL),
(8, 8, 'Clematis Engineering', NULL, NULL, 'Gambia', 'Moyen-Ogooue', NULL, NULL, NULL, 1, 1, '2020-02-19 06:39:10'),
(9, 9, 'HB_CLEMATISTECH', NULL, 'Addis Ababa, Ethiopia', '', 'Addis Ababa', 'Addis ababa', '8.9806034', '38.7577605', 1, NULL, NULL),
(10, 10, 'ani.daspvt ltd', NULL, NULL, 'India', 'West Bengal', NULL, NULL, NULL, 1, NULL, NULL),
(11, 11, 'hola@gmail.com', NULL, NULL, 'Gambia', 'Estuaire', NULL, NULL, NULL, 1, NULL, NULL),
(12, 12, 'gola@gmail.com', NULL, NULL, 'Ghana', 'Accra', NULL, NULL, NULL, 1, NULL, NULL),
(13, 13, 'bhola@gmail.com', NULL, NULL, 'Ghana', 'Accra', NULL, NULL, NULL, 1, NULL, NULL),
(14, 14, 'ghola@gmail.com', NULL, NULL, 'Ghana', 'Accra', NULL, NULL, NULL, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `t_category`
--

CREATE TABLE `t_category` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `displayLocal` enum('0','1') NOT NULL DEFAULT '0',
  `category_name_en` varchar(255) DEFAULT NULL,
  `category_name_fr` varchar(255) DEFAULT NULL,
  `image` varchar(255) NOT NULL,
  `parent_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `slug` varchar(255) DEFAULT NULL,
  `type` tinyint(4) DEFAULT '1' COMMENT '1:Item, 2: Service',
  `active` tinyint(1) DEFAULT '1' COMMENT '0:Inactive, 1:Active, 2: Delete',
  `metadata` varchar(255) DEFAULT NULL,
  `link_json` varchar(255) DEFAULT NULL,
  `request_seller_id` bigint(20) UNSIGNED DEFAULT NULL,
  `request_on` timestamp NULL DEFAULT NULL,
  `is_approved` tinyint(1) DEFAULT '1',
  `approved_by` bigint(20) UNSIGNED DEFAULT NULL,
  `approved_on` timestamp NULL DEFAULT NULL,
  `rejected_by` int(3) DEFAULT NULL,
  `rejected_on` datetime DEFAULT NULL,
  `uom_id` bigint(20) UNSIGNED DEFAULT NULL,
  `featured` tinyint(1) NOT NULL DEFAULT '1',
  `display` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_category`
--

INSERT INTO `t_category` (`id`, `displayLocal`, `category_name_en`, `category_name_fr`, `image`, `parent_id`, `slug`, `type`, `active`, `metadata`, `link_json`, `request_seller_id`, `request_on`, `is_approved`, `approved_by`, `approved_on`, `rejected_by`, `rejected_on`, `uom_id`, `featured`, `display`) VALUES
(1, '0', 'Agriculture', 'Agriculture', '1580980349.png', 0, 'agriculture', 1, 1, '', '[]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(2, '0', 'Apparel and Fashion', 'Vêtements et mode', '1580983488.png', 0, 'apparel-and-fashion', 1, 1, '', '[]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(3, '0', 'Beans', 'Des haricots', '1580972888.png', 1, 'Beans', 1, 1, '', '["{\\"id\\":\\"1\\",\\"category_name_en\\":\\"Agriculture\\",\\"category_name_fr\\":\\"Agriculture\\",\\"parent_id\\":\\"0\\"}"]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(4, '0', 'Fruit', 'Fruit', '1580972912.png', 1, 'Fruit', 1, 1, '', '["{\\"id\\":\\"1\\",\\"category_name_en\\":\\"Agriculture\\",\\"category_name_fr\\":\\"Agriculture\\",\\"parent_id\\":\\"0\\"}"]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(5, '0', 'Coffee Beans', 'Grains de café', '', 3, NULL, 1, 1, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 1, 1),
(6, '0', 'Cocoa Beans', 'Cocoa Beans', '1579850480.jpg', 3, NULL, 1, 1, '', '["{\\"id\\":\\"1\\",\\"category_name_en\\":\\"Agriculture\\",\\"category_name_fr\\":\\"Agriculture\\",\\"parent_id\\":\\"0\\"}","{\\"id\\":\\"3\\",\\"category_name_en\\":\\"Beans\\",\\"category_name_fr\\":\\"Beans\\",\\"parent_id\\":\\"1\\"}"]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(7, '0', 'Plant Seeds & Bulbs', 'Plant Seeds & Bulbs', '1579850985.jpeg', 1, NULL, 1, 1, '', '["{\\"id\\":\\"1\\",\\"category_name_en\\":\\"Agriculture\\",\\"category_name_fr\\":\\"Agriculture\\",\\"parent_id\\":\\"0\\"}"]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(8, '0', 'Farm Machinery & Equipment', 'Farm Machinery & Equipment', '1580972899.png', 1, NULL, 1, 1, '', '["{\\"id\\":\\"1\\",\\"category_name_en\\":\\"Agriculture\\",\\"category_name_fr\\":\\"Agriculture\\",\\"parent_id\\":\\"0\\"}"]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(9, '0', 'Tractor & Power', 'Tractor & Power', '1579851620.jpeg', 8, NULL, 1, 1, '', '["{\\"id\\":\\"1\\",\\"category_name_en\\":\\"Agriculture\\",\\"category_name_fr\\":\\"Agriculture\\",\\"parent_id\\":\\"0\\"}","{\\"id\\":\\"8\\",\\"category_name_en\\":\\"Farm Machinery & Equipment\\",\\"category_name_fr\\":\\"Farm Machinery & Equipment\\",\\"parent_id\\":\\"1\\"}"]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(10, '0', 'Automobile', 'Voiture', '1580984877.png', 0, NULL, 1, 1, '', '[]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(11, '0', 'Consumer electronics', 'Electronique grand public', '1580990183.png', 0, NULL, 1, 1, '', '[]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(12, '1', 'Construction and Real Estate', 'Construction et immobilier', '1580988076.png', 0, NULL, 1, 1, '', '[]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(13, '0', 'Electrical Supplies', 'Materiel electrique', '1580990510.png', 0, NULL, 1, 1, '', '[]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(14, '0', 'Energy and power', 'Énergie et puissance', '1579861018.jpeg', 0, NULL, 1, 1, '', '[]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(15, '1', 'Food and beverages', 'Aliments et boissons', '1580980286.png', 0, 'Food-and-beverages', 1, 1, '', '[]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(16, '0', 'Furniture and households', 'Meubles et ménages', '1580990909.png', 0, NULL, 1, 1, '', '[]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(17, '0', 'Beauty, Health and Nutrition', 'Beauté, santé et nutrition', '1580987025.png', 0, NULL, 1, 1, '', '[]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(18, '0', 'Industrial Supplies and machinery', 'Fournitures et machines industrielles', '1579861571.jpg', 0, NULL, 1, 1, '', '[]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(19, '0', 'Stationary', 'Stationnaire', '1579861798.jpeg', 0, NULL, 1, 1, '', '[]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(20, '0', 'Auditing', 'Audit', '1580984143.png', 0, NULL, 2, 1, '', '[]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(21, '0', 'Architect', 'Architecte', '1580980442.png', 0, NULL, 2, 1, '', '[]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(22, '0', 'Consultancy', 'Conseil', '1579862323.jpeg', 0, NULL, 2, 1, '', '[]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(23, '0', 'Construction', 'Construction', '1580987670.png', 0, NULL, 2, 1, '', '[]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(24, '0', 'Cleaning service', 'Service de nettoyage', '1580996246.png', 0, NULL, 2, 1, '', '[]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(25, '0', 'Freight forwarding and cargo clearing', 'Expédition de fret et dédouanement', '1579862743.jpeg', 0, NULL, 2, 1, '', '[]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(26, '0', 'Insurance and Insurance brokerage', 'Assurances et courtage d\'assurances', '1579862860.jpeg', 0, NULL, 2, 1, '', '[]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(27, '0', 'Interior design', 'Design d\'intérieur', '1579862947.jpg', 0, NULL, 2, 1, '', '[]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(28, '0', 'Legal', 'Légal', '1579863067.png', 0, NULL, 2, 1, '', '[]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(29, '0', 'PR and advertising', 'Relations publiques et publicité', '1579863270.jpeg', 0, NULL, 2, 1, '', '[]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(30, '0', 'Printing and graphic designs', 'Impression et graphisme', '1579863522.png', 0, NULL, 2, 1, '', '[]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(31, '0', 'Software', 'Logiciel', '1579863646.jpeg', 0, NULL, 2, 1, '', '[]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(32, '1', 'Baked Goods', 'Produits de boulangerie', '1580907823.png', 15, NULL, 1, 1, '', '["{\\"id\\":\\"15\\",\\"category_name_en\\":\\"Food and beverages\\",\\"category_name_fr\\":\\"Aliments et boissons\\",\\"parent_id\\":\\"0\\"}"]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(33, '1', 'Canned Food', 'Aliments en conserve', '1580907865.jpg', 15, NULL, 1, 1, '', '["{\\"id\\":\\"15\\",\\"category_name_en\\":\\"Food and beverages\\",\\"category_name_fr\\":\\"Aliments et boissons\\",\\"parent_id\\":\\"0\\"}"]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(34, '1', 'Confectionery', 'Confiserie', '1580907901.jpg', 15, NULL, 1, 1, '', '["{\\"id\\":\\"15\\",\\"category_name_en\\":\\"Food and beverages\\",\\"category_name_fr\\":\\"Aliments et boissons\\",\\"parent_id\\":\\"0\\"}"]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(35, '1', 'Dairy product', 'produit laitier', '1580907911.jpg', 15, NULL, 1, 1, '', '["{\\"id\\":\\"15\\",\\"category_name_en\\":\\"Food and beverages\\",\\"category_name_fr\\":\\"Aliments et boissons\\",\\"parent_id\\":\\"0\\"}"]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(36, '1', 'Egg & Egg Products', 'Oeufs et ovoproduits', '1580907921.jpg', 15, NULL, 1, 1, '', '["{\\"id\\":\\"15\\",\\"category_name_en\\":\\"Food and beverages\\",\\"category_name_fr\\":\\"Aliments et boissons\\",\\"parent_id\\":\\"0\\"}"]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(37, '1', 'Honey Products', 'Produits de miel', '1580907931.jpg', 15, NULL, 1, 1, '', '["{\\"id\\":\\"15\\",\\"category_name_en\\":\\"Food and beverages\\",\\"category_name_fr\\":\\"Aliments et boissons\\",\\"parent_id\\":\\"0\\"}"]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(38, '1', 'Meat & Poultry', 'Viande de volaille', '', 15, NULL, 1, 1, '', '["{\\"id\\":\\"15\\",\\"category_name_en\\":\\"Food and beverages\\",\\"category_name_fr\\":\\"Food and beverages\\",\\"parent_id\\":\\"0\\"}"]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(39, '1', 'Seafood', 'Fruit de mer', '', 15, NULL, 1, 1, '', '["{\\"id\\":\\"15\\",\\"category_name_en\\":\\"Food and beverages\\",\\"category_name_fr\\":\\"Food and beverages\\",\\"parent_id\\":\\"0\\"}"]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(40, '1', 'Cookies & Biscuits', 'Biscuits et biscuits', '', 32, NULL, 1, 1, '', '["{\\"id\\":\\"15\\",\\"category_name_en\\":\\"Food and beverages\\",\\"category_name_fr\\":\\"Food and beverages\\",\\"parent_id\\":\\"0\\"}","{\\"id\\":\\"32\\",\\"category_name_en\\":\\"Baked Goods\\",\\"category_name_fr\\":\\"Produits de boulangerie\\",\\"parent_id\\":\\"15\\"}"]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(41, '1', 'Canned Seafood', 'Fruits de mer en conserve', '', 33, NULL, 1, 1, '', '["{\\"id\\":\\"15\\",\\"category_name_en\\":\\"Food and beverages\\",\\"category_name_fr\\":\\"Food and beverages\\",\\"parent_id\\":\\"0\\"}","{\\"id\\":\\"33\\",\\"category_name_en\\":\\"Canned Food\\",\\"category_name_fr\\":\\"Aliments en conserve\\",\\"parent_id\\":\\"15\\"}"]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(42, '1', 'Canned Poultry & Meat', 'Volaille et viande en conserve', '', 33, NULL, 1, 1, '', '["{\\"id\\":\\"15\\",\\"category_name_en\\":\\"Food and beverages\\",\\"category_name_fr\\":\\"Food and beverages\\",\\"parent_id\\":\\"0\\"}","{\\"id\\":\\"33\\",\\"category_name_en\\":\\"Canned Food\\",\\"category_name_fr\\":\\"Aliments en conserve\\",\\"parent_id\\":\\"15\\"}"]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(43, '0', 'Vegetables', 'Des légumes', '', 1, NULL, 1, 1, '', '["{\\"id\\":\\"1\\",\\"category_name_en\\":\\"Agriculture\\",\\"category_name_fr\\":\\"Agriculture\\",\\"parent_id\\":\\"0\\"}"]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(44, '0', 'Plant & Animal Oil', 'Huile végétale et animale', '1580972955.png', 1, NULL, 1, 1, '', '["{\\"id\\":\\"1\\",\\"category_name_en\\":\\"Agriculture\\",\\"category_name_fr\\":\\"Agriculture\\",\\"parent_id\\":\\"0\\"}"]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(45, '0', 'Mushrooms & Truffles', 'Champignons et truffes', '1580972936.png', 1, NULL, 1, 1, '', '["{\\"id\\":\\"1\\",\\"category_name_en\\":\\"Agriculture\\",\\"category_name_fr\\":\\"Agriculture\\",\\"parent_id\\":\\"0\\"}"]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(46, '0', 'Casual Dresses', 'Robes décontractées', '1580997618.png', 2, NULL, 1, 1, '', '["{\\"id\\":\\"2\\",\\"category_name_en\\":\\"Apparel and Fashion\\",\\"category_name_fr\\":\\"V\\\\u00eatements et mode\\",\\"parent_id\\":\\"0\\"}"]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(47, '0', 'Women\'s Jackets & Coats', 'Vestes et manteaux pour femmes', '1580997638.jpg', 2, NULL, 1, 1, '', '["{\\"id\\":\\"2\\",\\"category_name_en\\":\\"Apparel and Fashion\\",\\"category_name_fr\\":\\"V\\\\u00eatements et mode\\",\\"parent_id\\":\\"0\\"}"]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(48, '0', 'Men\'s Hoodies & Sweatshirts', 'Sweats à capuche et sweat-shirts pour hommes', '1580997627.png', 2, NULL, 1, 1, '', '["{\\"id\\":\\"2\\",\\"category_name_en\\":\\"Apparel and Fashion\\",\\"category_name_fr\\":\\"V\\\\u00eatements et mode\\",\\"parent_id\\":\\"0\\"}"]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(49, '0', 'Electric Cars', 'Voiture électrique', '1580996706.png', 10, NULL, 1, 1, '', '["{\\"id\\":\\"10\\",\\"category_name_en\\":\\"Automobile\\",\\"category_name_fr\\":\\"Voiture\\",\\"parent_id\\":\\"0\\"}"]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(50, '0', 'New Cars', 'Nouvelles voitures', '1580996718.png', 10, NULL, 1, 1, '', '["{\\"id\\":\\"10\\",\\"category_name_en\\":\\"Automobile\\",\\"category_name_fr\\":\\"Voiture\\",\\"parent_id\\":\\"0\\"}"]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(51, '0', 'Makeup', 'Maquillage', '1580993357.jpg', 17, NULL, 1, 1, '', '["{\\"id\\":\\"17\\",\\"category_name_en\\":\\"Beauty, Health and Nutrition\\",\\"category_name_fr\\":\\"Beaut\\\\u00e9, sant\\\\u00e9 et nutrition\\",\\"parent_id\\":\\"0\\"}"]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(52, '0', 'Skin Care', 'Soin de la peau', '1580993384.jpg', 17, NULL, 1, 1, '', '["{\\"id\\":\\"17\\",\\"category_name_en\\":\\"Beauty, Health and Nutrition\\",\\"category_name_fr\\":\\"Beaut\\\\u00e9, sant\\\\u00e9 et nutrition\\",\\"parent_id\\":\\"0\\"}"]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(53, '0', 'Beauty Equipment', 'Équipement de beauté', '1580993369.png', 17, NULL, 1, 1, '', '["{\\"id\\":\\"17\\",\\"category_name_en\\":\\"Beauty, Health and Nutrition\\",\\"category_name_fr\\":\\"Beaut\\\\u00e9, sant\\\\u00e9 et nutrition\\",\\"parent_id\\":\\"0\\"}"]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(54, '1', 'Doors & Windows', 'Portes et fenêtres', '1580997241.jpg', 12, NULL, 1, 1, '', '["{\\"id\\":\\"12\\",\\"category_name_en\\":\\"Construction and Real Estate\\",\\"category_name_fr\\":\\"Construction et immobilier\\",\\"parent_id\\":\\"0\\"}"]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(55, '1', 'Tiles & Accessories', 'Tuiles et accessoires', '1580997260.jpg', 12, NULL, 1, 1, '', '["{\\"id\\":\\"12\\",\\"category_name_en\\":\\"Construction and Real Estate\\",\\"category_name_fr\\":\\"Construction et immobilier\\",\\"parent_id\\":\\"0\\"}"]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(56, '1', 'Flooring & Accessories', 'Revêtements de sol et accessoires', '1580997251.png', 12, NULL, 1, 1, '', '["{\\"id\\":\\"12\\",\\"category_name_en\\":\\"Construction and Real Estate\\",\\"category_name_fr\\":\\"Construction et immobilier\\",\\"parent_id\\":\\"0\\"}"]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(57, '0', 'Commonly Used Accessories & Parts', 'Accessoires et pièces couramment utilisés', '1580992923.jpg', 11, NULL, 1, 1, '', '["{\\"id\\":\\"11\\",\\"category_name_en\\":\\"Consumer electronics\\",\\"category_name_fr\\":\\"Electronique grand public\\",\\"parent_id\\":\\"0\\"}"]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(58, '0', 'Mobile Phone & Accessories', 'Des accessoires pour téléphone mobile', '1580992306.jpg', 11, NULL, 1, 1, '', '["{\\"id\\":\\"11\\",\\"category_name_en\\":\\"Consumer electronics\\",\\"category_name_fr\\":\\"Electronique grand public\\",\\"parent_id\\":\\"0\\"}"]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(59, '0', 'Home Audio, Video & Accessories', 'Audio, vidéo et accessoires pour la maison', '1580992931.png', 11, NULL, 1, 1, '', '["{\\"id\\":\\"11\\",\\"category_name_en\\":\\"Consumer electronics\\",\\"category_name_fr\\":\\"Electronique grand public\\",\\"parent_id\\":\\"0\\"}"]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(60, '0', 'Smart Electronics', 'Électronique intelligente', '1580992952.jpg', 11, NULL, 1, 1, '', '["{\\"id\\":\\"11\\",\\"category_name_en\\":\\"Consumer electronics\\",\\"category_name_fr\\":\\"Electronique grand public\\",\\"parent_id\\":\\"0\\"}"]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(61, '0', 'Solar Panels', 'Panneaux solaires', '1580991763.png', 13, NULL, 1, 1, '', '["{\\"id\\":\\"13\\",\\"category_name_en\\":\\"Electrical Supplies\\",\\"category_name_fr\\":\\"Materiel electrique\\",\\"parent_id\\":\\"0\\"}"]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(62, '0', 'Inverters & Converters', 'Onduleurs et convertisseurs', '1580991737.png', 13, NULL, 1, 1, '', '["{\\"id\\":\\"13\\",\\"category_name_en\\":\\"Electrical Supplies\\",\\"category_name_fr\\":\\"Materiel electrique\\",\\"parent_id\\":\\"0\\"}"]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(63, '0', 'Living Room Furniture', 'Meubles de salon', '1580991235.jpg', 16, NULL, 1, 1, '', '["{\\"id\\":\\"16\\",\\"category_name_en\\":\\"Furniture and households\\",\\"category_name_fr\\":\\"Meubles et m\\\\u00e9nages\\",\\"parent_id\\":\\"0\\"}"]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(64, '0', 'Home Decor', 'Décoration de maison', '1580991223.jpg', 16, NULL, 1, 1, '', '["{\\"id\\":\\"16\\",\\"category_name_en\\":\\"Furniture and households\\",\\"category_name_fr\\":\\"Meubles et m\\\\u00e9nages\\",\\"parent_id\\":\\"0\\"}"]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(65, '0', 'Residential Architect', 'Architecte résidentiel', '1580984331.png', 21, NULL, 2, 1, '', '["{\\"id\\":\\"21\\",\\"category_name_en\\":\\"Architect\\",\\"category_name_fr\\":\\"Architecte\\",\\"parent_id\\":\\"0\\"}"]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(66, '0', 'Commercial or Public Architect', 'Architecte commercial ou public', '1580977881.png', 21, NULL, 2, 1, '', '["{\\"id\\":\\"21\\",\\"category_name_en\\":\\"Architect\\",\\"category_name_fr\\":\\"Architecte\\",\\"parent_id\\":\\"0\\"}"]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(67, '0', 'Industrial Architect', 'Architecte industriel', '1580977862.png', 21, NULL, 2, 1, '', '["{\\"id\\":\\"21\\",\\"category_name_en\\":\\"Architect\\",\\"category_name_fr\\":\\"Architecte\\",\\"parent_id\\":\\"0\\"}"]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(68, '0', 'Personal Audit', 'Audit personnel', '1580997214.jpg', 20, NULL, 2, 1, '', '["{\\"id\\":\\"20\\",\\"category_name_en\\":\\"Auditing\\",\\"category_name_fr\\":\\"Audit\\",\\"parent_id\\":\\"0\\"}"]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(69, '0', 'Company Audit', 'Audit d\'entreprise', '1580997204.jpg', 20, NULL, 2, 1, '', '["{\\"id\\":\\"20\\",\\"category_name_en\\":\\"Auditing\\",\\"category_name_fr\\":\\"Audit\\",\\"parent_id\\":\\"0\\"}"]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(70, '0', 'Kitchen Cleaning', 'Nettoyage de cuisine', '1580994547.jpg', 24, NULL, 2, 1, '', '["{\\"id\\":\\"24\\",\\"category_name_en\\":\\"Cleaning service\\",\\"category_name_fr\\":\\"Service de nettoyage\\",\\"parent_id\\":\\"0\\"}"]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1),
(71, '0', 'Bathroom Cleaning', 'Nettoyage de salle de bain', '1580994533.jpg', 24, NULL, 2, 1, '', '["{\\"id\\":\\"24\\",\\"category_name_en\\":\\"Cleaning service\\",\\"category_name_fr\\":\\"Service de nettoyage\\",\\"parent_id\\":\\"0\\"}"]', NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_cat_uom_relation`
--

CREATE TABLE `t_cat_uom_relation` (
  `id` int(11) NOT NULL,
  `cat_id` int(5) NOT NULL,
  `uom_id` int(5) NOT NULL,
  `active` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_cat_uom_relation`
--

INSERT INTO `t_cat_uom_relation` (`id`, `cat_id`, `uom_id`, `active`) VALUES
(8, 1, 1, 1),
(9, 1, 2, 1),
(10, 16, 1, 1),
(13, 2, 1, 1),
(14, 2, 2, 1),
(16, 8, 2, 1),
(20, 13, 6, 1),
(21, 13, 1, 1),
(22, 13, 3, 1),
(24, 14, 4, 1),
(27, 5, 1, 1),
(28, 5, 2, 1),
(30, 10, 5, 1),
(34, 12, 6, 1),
(35, 12, 1, 1),
(36, 12, 3, 1),
(40, 11, 6, 1),
(41, 11, 1, 1),
(42, 11, 3, 1),
(45, 7, 1, 1),
(46, 7, 2, 1),
(48, 15, 4, 1),
(51, 17, 7, 1),
(52, 17, 3, 1),
(54, 6, 4, 1),
(56, 9, 2, 1),
(59, 4, 1, 1),
(60, 4, 2, 1),
(64, 3, 1, 1),
(65, 3, 2, 1),
(66, 3, 3, 1),
(73, 48, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_cms`
--

CREATE TABLE `t_cms` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `details` text,
  `slug` varchar(100) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `code` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_cms`
--

INSERT INTO `t_cms` (`id`, `name`, `details`, `slug`, `active`, `code`) VALUES
(8, 'About Us', 'Akpoti is the leading platform for global trade, serving millions of buyers and sellers around the world. Akpoti is a matchmaker for businesses - connecting buyers and sellers online, making it easier to do business anywhere in the world. the first business of Akpoti is the leading platform for global trade serving millions of buyers and suppliers around the world. Through Akpoti, small businesses can sell their products to companies in other countries. Sellers on Akpoti are typically manufacturers and distributors based in Nigeria and other Agro-productive countries such as Cameroun, Mali, Niger Republic, Ivory Coast, Ghana, Kenya, and Senegal.', 'about-us', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `t_customer`
--

CREATE TABLE `t_customer` (
  `id` bigint(11) UNSIGNED NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `email_verify` tinyint(1) DEFAULT '0',
  `phone` varchar(255) DEFAULT NULL,
  `phone_verify` tinyint(1) DEFAULT '0',
  `password` varchar(255) DEFAULT NULL,
  `email_otp` varchar(255) DEFAULT NULL,
  `change_email_otp` bigint(20) UNSIGNED DEFAULT NULL,
  `change_password_otp` bigint(20) UNSIGNED DEFAULT NULL,
  `phone_otp` varchar(255) DEFAULT NULL,
  `supplier_YN` tinyint(1) DEFAULT '0' COMMENT '0:Buyer, 1:Supplier, 2: Both',
  `sellerCategory` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1:Manufactures, 2:Importers & Exporters, 3:Commission agents, 4:Local traders,5:Local producers, 6:Distributors, 7:Wholesalers, 8:Service providers	',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `token_id` varchar(255) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` bigint(20) UNSIGNED DEFAULT NULL,
  `update_on` timestamp NULL DEFAULT NULL,
  `location_key` varchar(255) DEFAULT NULL,
  `location_city` varchar(255) DEFAULT NULL,
  `cust_language` varchar(255) DEFAULT 'en',
  `store_currency` varchar(255) DEFAULT NULL,
  `displayLocal` enum('0','1') DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_customer`
--

INSERT INTO `t_customer` (`id`, `username`, `name`, `email`, `email_verify`, `phone`, `phone_verify`, `password`, `email_otp`, `change_email_otp`, `change_password_otp`, `phone_otp`, `supplier_YN`, `sellerCategory`, `active`, `token_id`, `created_date`, `update_by`, `update_on`, `location_key`, `location_city`, `cust_language`, `store_currency`, `displayLocal`) VALUES
(1, 'reply2him@gmail.com', 'Himadri Banerjee', 'reply2him@gmail.com', 0, '9051739826', 1, '1587794eb3c7ccd606d8caaf194080e3', '1234', NULL, NULL, '', 2, 3, 1, '9e41869f6f098512698d8a9c67834baf', '2020-01-28 06:09:23', 1, '2020-03-06 07:31:06', NULL, NULL, 'fr', '€', '1'),
(2, 'anidas0407', 'aniruddha das', 'dasaniruddha0407@gmail.com', 1, '7003960614', 1, '1587794eb3c7ccd606d8caaf194080e3', NULL, NULL, NULL, NULL, 2, 2, 1, 'ffe5f37d8577fcce868a5155a9cfc204', '2020-02-04 07:40:09', 1, '2020-03-06 07:22:05', NULL, NULL, 'en', 'USD', '1'),
(3, 'koli@gmail.com', 'Koli Masudi', 'kolikata@gmail.com', 0, '1234567890', 1, '1587794eb3c7ccd606d8caaf194080e3', '1234', NULL, NULL, '', 1, 0, 1, '0394fb7f68be7b4784905ca45ed2c9bd', '2020-02-11 06:11:57', NULL, NULL, NULL, NULL, NULL, '€', '0'),
(4, 'prosenjit.dutta@clematistech.com', 'Prosenjit Dutta', 'prosenjit.dutta@clematistech.com', 0, '9883030032', 1, '23ba982af319b2572cc51220532e2b8a', NULL, NULL, NULL, '', 1, 0, 1, '9e914ea6ac6cca22658e8c9aa0055453', '2020-02-11 14:26:26', 1, '2020-03-18 11:56:39', NULL, NULL, NULL, NULL, '1'),
(5, 'mojibul.hoque@clematistech.com', 'Mojibul Hoque', 'mojibul.hoque@clematistech.com', 0, '8116098191', 1, 'de3023803bc493ddeb58b950453532fe', '1234', NULL, NULL, '', 1, 0, 1, 'fc2d21580bb2ee0d8f6dc1087b9a457b', '2020-02-12 07:17:35', 1, '2020-02-17 07:00:38', NULL, NULL, NULL, NULL, '0'),
(6, 'mojibulh61@gmail.com', 'Mojibul Hoque61', 'mojibulh61@gmail.com', 0, '8116098191', 1, '1587794eb3c7ccd606d8caaf194080e3', NULL, NULL, NULL, '', 0, 0, 1, '5780a51efcfe53a39074f0dd756de1f3', '2020-02-12 07:26:56', 1, '2020-03-18 11:42:35', NULL, NULL, NULL, NULL, '1'),
(7, 'rajarshi.dasgupta@clematisbiz.com', 'Rajarshi Dasgupta', 'rajarshi.dasgupta@clematisbiz.com', 0, '09830879839', 1, '40685922b224aa25eb4d521a856429b8', NULL, NULL, NULL, '', 1, 0, 1, '098e3ccf228b0016b86961787e9f9a44', '2020-02-12 07:27:17', NULL, NULL, NULL, NULL, NULL, NULL, '0'),
(8, 'dasgupta.rajarsh@gmail.com', 'Rajarshi Dasgupta', 'dasgupta.rajarsh@gmail.com', 0, '9830879839', 1, '40685922b224aa25eb4d521a856429b8', NULL, NULL, NULL, '', 0, 0, 1, 'fac19678890cb93604787bce77a69bfc', '2020-02-12 09:39:00', 1, '2020-02-19 06:39:10', NULL, NULL, NULL, NULL, '0'),
(9, 'himadri.banerjee@clematistech.com', 'Clematis Himadri Banerjee', 'himadri.banerjee@clematistech.com', 0, '8981009826', 1, '1587794eb3c7ccd606d8caaf194080e3', NULL, NULL, NULL, '', 1, 4, 1, '4c10b2c652e84d630a69e220f0830318', '2020-02-19 05:58:02', 1, '2020-03-06 07:25:10', NULL, NULL, 'en', 'USD', '0'),
(10, 'ani.das0407@gmail.com', 'ani das', 'ani.das0407@gmail.com', 0, '9674391164', 1, '23ba982af319b2572cc51220532e2b8a', NULL, NULL, NULL, '', 0, 7, 1, 'c98033053b3a90687d4c90cb9574f950', '2020-02-20 05:50:43', 1, '2020-03-06 07:25:07', NULL, NULL, 'en', 'USD', '0'),
(11, 'hola@gmail.com', 'hola dus', 'hola@gmail.com', 0, '4565787654', 1, '1587794eb3c7ccd606d8caaf194080e3', NULL, NULL, NULL, '', 0, 0, 1, NULL, '2020-03-18 06:58:34', NULL, NULL, NULL, NULL, 'en', NULL, '0'),
(12, 'gola@gmail.com', 'Gola Bhola', 'gola@gmail.com', 0, '7807659456', 1, '1587794eb3c7ccd606d8caaf194080e3', NULL, NULL, NULL, '', 0, 0, 1, NULL, '2020-03-18 07:06:09', NULL, NULL, NULL, NULL, 'en', NULL, '0'),
(13, 'bhola@gmail.com', 'Bhola Bhhola', 'bhola@gmail.com', 0, '233', 1, '1587794eb3c7ccd606d8caaf194080e3', NULL, NULL, NULL, '', 0, 0, 1, NULL, '2020-03-18 07:08:11', NULL, NULL, NULL, NULL, 'en', NULL, '0'),
(14, 'ghola@gmail.com', 'Ghola Ghola', 'ghola@gmail.com', 0, '9908989898', 1, '1587794eb3c7ccd606d8caaf194080e3', NULL, NULL, NULL, '', 0, 0, 1, NULL, '2020-03-18 07:14:27', NULL, NULL, '233', NULL, 'en', NULL, '0');

-- --------------------------------------------------------

--
-- Table structure for table `t_enquiry`
--

CREATE TABLE `t_enquiry` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `to_id` bigint(20) UNSIGNED DEFAULT NULL,
  `from_id` bigint(20) UNSIGNED DEFAULT NULL,
  `seller_item_id` bigint(20) UNSIGNED DEFAULT NULL,
  `type` tinyint(4) DEFAULT '1' COMMENT '1:-Item,2:service',
  `quantity` bigint(20) DEFAULT NULL,
  `uom_id` bigint(20) UNSIGNED DEFAULT NULL,
  `message` text,
  `attachments` text,
  `variance_json` text,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_enquiry`
--

INSERT INTO `t_enquiry` (`id`, `to_id`, `from_id`, `seller_item_id`, `type`, `quantity`, `uom_id`, `message`, `attachments`, `variance_json`, `date`) VALUES
(1, 2, 2, 44, 1, 6, 1, 'sassasasas', 'null', '', '2020-02-10 11:01:17'),
(2, 4, 9, 42, 1, 6, 1, 'I want 750 pieces 100% cotton full sleeve hoodies. My preferred price would be $8.5/piece.  I am attaching sample picture of my requirement.', '["1582196475-hoodi2.jpg"]', '', '2020-02-20 11:01:14'),
(3, 2, 6, 42, 1, NULL, NULL, 'test message', '["1582197383-3.jpg","1582197383-4.jpg"]', NULL, '2020-02-20 11:16:22');

-- --------------------------------------------------------

--
-- Table structure for table `t_enquiry_image`
--

CREATE TABLE `t_enquiry_image` (
  `id` bigint(20) NOT NULL,
  `enquiry_thread_id` bigint(20) DEFAULT NULL,
  `image` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_enquiry_thread`
--

CREATE TABLE `t_enquiry_thread` (
  `id` int(10) UNSIGNED NOT NULL,
  `enquiry_id` bigint(20) UNSIGNED DEFAULT NULL,
  `to_customer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `from_customer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `message` text,
  `images` text,
  `is_seen` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_enquiry_thread`
--

INSERT INTO `t_enquiry_thread` (`id`, `enquiry_id`, `to_customer_id`, `from_customer_id`, `date`, `message`, `images`, `is_seen`) VALUES
(1, 3, 2, 6, '2019-12-11 10:04:43', 'Tell Me What can i do for you?', NULL, 1),
(2, 3, 6, 2, '2020-02-26 13:05:44', 'hello123', '1582722344-woman-Nigeria-millet-grains-bowl-village-Sadongori-2005.jpg', 0),
(3, 2, 4, 9, '2020-02-27 06:59:20', 'Hi abc', NULL, 1),
(4, 2, 4, 9, '2020-02-27 06:59:40', 'hello', NULL, 1),
(5, 2, 4, 9, '2020-02-27 07:09:41', 'hi', NULL, 1),
(6, 2, 4, 9, '2020-02-27 07:09:56', '', '1582787396-Yielding.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_forum`
--

CREATE TABLE `t_forum` (
  `id` int(11) NOT NULL,
  `title_en` varchar(255) NOT NULL,
  `title_fr` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `type` varchar(150) NOT NULL,
  `quantity` bigint(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1: Active, 0: In-Active'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_forum`
--

INSERT INTO `t_forum` (`id`, `title_en`, `title_fr`, `description`, `type`, `quantity`, `image`, `date`, `status`) VALUES
(1, 'IEE Expo - International Elevator And Escalator Expo 2020', 'IEE Expo - Expo internationale des ascenseurs et escaliers mécaniques 2020', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.', 'Passes', 250, '1582024726img-13.png', '2020-02-18 08:12:37', 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_item`
--

CREATE TABLE `t_item` (
  `id` int(11) NOT NULL,
  `supp_id` int(11) NOT NULL,
  `local_sup_id` int(11) NOT NULL,
  `item_name_en` varchar(255) NOT NULL,
  `item_name_fr` varchar(255) NOT NULL,
  `description_en` text NOT NULL,
  `description_fr` text NOT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  `category_json` varchar(255) NOT NULL,
  `type` enum('1','2') NOT NULL DEFAULT '1' COMMENT '1:Item, 2: Service',
  `fixedPrice` float(10,2) NOT NULL DEFAULT '0.00',
  `price_min` varchar(50) NOT NULL,
  `price_max` varchar(50) NOT NULL,
  `currency` enum('USD','EUR') NOT NULL,
  `lead_time` varchar(255) DEFAULT NULL,
  `moq` varchar(10) DEFAULT NULL,
  `packaging` text,
  `variance` text,
  `attribute` text,
  `thumbnail` varchar(255) DEFAULT NULL,
  `item_image` varchar(500) DEFAULT NULL,
  `price_uom_id` int(11) NOT NULL COMMENT 'Unit of Measurement',
  `moq_uom_id` int(10) UNSIGNED DEFAULT NULL,
  `showcase` tinyint(1) DEFAULT '1',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `is_approved` tinyint(4) NOT NULL DEFAULT '1',
  `approved_by` int(11) DEFAULT NULL,
  `approved_on` timestamp NULL DEFAULT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_deleted` tinyint(1) DEFAULT '0',
  `on_display` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_item`
--

INSERT INTO `t_item` (`id`, `supp_id`, `local_sup_id`, `item_name_en`, `item_name_fr`, `description_en`, `description_fr`, `keywords`, `category_id`, `category_json`, `type`, `fixedPrice`, `price_min`, `price_max`, `currency`, `lead_time`, `moq`, `packaging`, `variance`, `attribute`, `thumbnail`, `item_image`, `price_uom_id`, `moq_uom_id`, `showcase`, `active`, `is_approved`, `approved_by`, `approved_on`, `created_on`, `is_deleted`, `on_display`) VALUES
(30, 2, 3, 'Quality Fresh Cocoa Beans From Peru Wholesale ', 'Fèves de cacao fraîches de qualité du Pérou en gros', 'Cacao nibs have more antioxidants ( flavonoids) than any food we have tested yet which includes red wine, green tea, black tea, and blueberries! As a matter of fact, raw unprocessed chocolate can have up to 4 times more antioxidants than green tea! This is just one of the many health benefits of cacao nibs. Remember, in order to benefit from chocolate’s health benefits, you must eat it as close to its raw form as possible. Whole raw cacao nibs are best. When you eat commercially produced chocolate, you lose a lot of the health benefits!', 'Les éclats de cacao contiennent plus d\'antioxydants (flavonoïdes) que tous les aliments que nous avons testés, ce qui comprend le vin rouge, le thé vert, le thé noir et les bleuets! En effet, le chocolat cru non transformé peut contenir jusqu\'à 4 fois plus d\'antioxydants que le thé vert! Ce n\'est là qu\'un des nombreux avantages pour la santé des graines de cacao. N\'oubliez pas que pour bénéficier des bienfaits du chocolat sur la santé, vous devez le manger le plus près possible de sa forme crue. Les éclats de cacao crus entiers sont les meilleurs. Lorsque vous mangez du chocolat produit dans le commerce, vous perdez beaucoup d\'avantages pour la santé!', 'test keywords', 5, '["1","3","5"]', '1', 0.00, '400', '426.04', 'USD', '5 jours', '6', 'test packaging', ' [{"1": ["red", "blue"]},{"2": ["xl", "XXL"]}]', '[{"model":"abc"},{"product":"xyz"}]', '1582019135-dsc019172235749.jpg', NULL, 1, 1, 1, 1, 1, NULL, NULL, '2020-02-18 15:15:35', 1, 1),
(33, 2, 3, 'abc1221', 'Fèves de cacao fraîches de qualité du Pérou en gros', 'Cacao nibs have more antioxidants ( flavonoids) than any food we have tested yet which includes red wine, green tea, black tea, and blueberries! As a matter of fact, raw unprocessed chocolate can have up to 4 times more antioxidants than green tea! This is just one of the many health benefits of cacao nibs. Remember, in order to benefit from chocolate’s health benefits, you must eat it as close to its raw form as possible. Whole raw cacao nibs are best. When you eat commercially produced chocolate, you lose a lot of the health benefits!', 'Les éclats de cacao contiennent plus d\'antioxydants (flavonoïdes) que tous les aliments que nous avons testés, ce qui comprend le vin rouge, le thé vert, le thé noir et les bleuets! En effet, le chocolat cru non transformé peut contenir jusqu\'à 4 fois plus d\'antioxydants que le thé vert! Ce n\'est là qu\'un des nombreux avantages pour la santé des graines de cacao. N\'oubliez pas que pour bénéficier des bienfaits du chocolat sur la santé, vous devez le manger le plus près possible de sa forme crue. Les éclats de cacao crus entiers sont les meilleurs. Lorsque vous mangez du chocolat produit dans le commerce, vous perdez beaucoup d\'avantages pour la santé!', 'test keywords', 5, '["1","3","5"]', '1', 0.00, '1400', '4126.04', 'USD', '5 jours', '6', 'test packaging', ' [{"1": ["red", "blue"]},{"2": ["xl", "XXL"]}]', '[{"model":"abc"},{"product":"xyz"}]', '1582022970-2016_365804d3e418b97jagannath-puri-tourist-guide-qpr.jpg', '[{"name":"1582020614-4.jpg"},{"name":"1582022970-img20160910125412753879.jpg"},{"name":"1582022970-1567432589banner-bikenight-cs.jpg"}]', 1, 1, 1, 0, 1, NULL, NULL, '2020-02-18 15:40:14', 0, 0),
(35, 4, 0, 'en title', 'fr title', 'en details', 'fr details', 'undefined', 5, '["1","3","5"]', '1', 0.00, '12.00', '30.00', 'USD', 'lead time', '11', 'packaging', '', '', '1582034493-ca14.png', NULL, 3, 3, 1, 1, 1, NULL, NULL, '2020-02-18 19:31:32', 1, 1),
(36, 4, 0, 'title en', 'title fr', 'details en', 'details fr', 'undefined', 5, '["1","3","5"]', '1', 0.00, '11.00', '22.00', 'USD', 'leadtime', '100', 'packaging', '[{"1":["red","green","blue"]},{"2":["Xl","XXL"]}]', '[{"Brand":"Product band"},{"Capacity":"Double"}]', '1582095155-ca6.png', NULL, 3, 3, 1, 1, 1, NULL, NULL, '2020-02-19 12:22:35', 0, 1),
(37, 4, 0, 'item title en', 'item title fr', 'item details en', 'item details fr', 'key1,key2', 5, '["1","3","5"]', '1', 0.00, '1200.00', '1400.00', 'USD', 'lead time', '10000', 'packaging', '[{"1":["red","blue"]},{"2":["XL"]}]', '[{"Camera":"no camera"}]', '1582095676-ca3.png', '[{"name":"1582095676-blob"},{"name":"1582095677-blob"},{"name":"1582095677-blob"},{"name":"1582095677-blob"}]', 3, 3, 1, 1, 1, NULL, NULL, '2020-02-19 12:31:16', 1, 1),
(39, 1, 0, 'Men\'s Clothing Fleece Button Hoodie Sweatshirts Jacket Men\'s Custom Hoodies', 'De Vêtements pour hommes de la Toison Bouton des Pulls molletonnés à Capuche de la Veste des Hommes Hoodies Personnalisé', '100% Cotton, cotton/spandex, cotton/polyester, cotton/nylon, 100% polyester,polyester/spandex,modal, bamboo fiber/cotton, viscose/cotton, viscose/spandex, viscose/polyester, etc', '100% Coton, Coton/spandex, coton/polyester, coton/nylon, 100% polyester,de polyester et d\'élasthanne,le modal, fibre de bambou/coton, viscose/coton, viscose/spandex, viscose/polyester, etc', '100% Cotton, cotton/spandex, cotton/polyester, cotton/nylon,   100% polyester,polyester/spandex,modal, bamboo fiber/cotton,    viscose/cotton, viscose/spandex, viscose/polyester, ', 46, '["2","46"]', '1', 9.15, '7.00', '9.00', 'USD', '3 working days.', '200', 'Plastic bag per product and 50-100 pcs/ ctn', '[{"1":["Red","Green","Orange","Violet","Gray"]},{"2":["34","36","38","40","42","44"]}]', '[]', '1582098122-hoodi.jpg', '[{"name":"1582098122-hoodi2.jpg"}]', 7, 7, 1, 1, 1, NULL, NULL, '2020-02-19 13:12:02', 0, 1),
(40, 4, 0, 'sadsad', 'sadsad', 'sadsa', 'asdsad', 'gsggs', 46, '["2","46"]', '1', 0.00, '12.00', '20.00', 'USD', '', '12', '', '[]', '[]', '1582100043-cat-icon.png', NULL, 3, 3, 1, 1, 1, NULL, NULL, '2020-02-19 13:44:02', 0, 1),
(41, 4, 0, 'Ladies Colorblocked Casual Shirt en', 'Ladies Colorblocked Casual Shirt fr', 'women and ladies fashion down winter padded jacket ', 'women and ladies fashion down winter padded jacket ', 'keywords', 46, '["2","46"]', '1', 0.00, '12.00', '15.00', 'USD', '', '100', '', '[]', '[]', '1582100381-cat-icon.png', NULL, 3, 3, 1, 1, 1, NULL, NULL, '2020-02-19 13:49:40', 0, 1),
(42, 4, 0, 'Men Colorblocked Casual Shirt', 'Les Hommes De Couleur Chemise Casual', '100% Cotton  (Mixed Cotton and Polyester)', '100% Cotton  (Mixed Cotton and Polyester)', 'Shirt', 46, '["2","46"]', '1', 0.00, '10.00', '20.00', 'USD', '', '200', '', '[{"1":["Indigo","Brown"]},{"2":["XL","XXl"]}]', '[{"model_name":"Product1"},{"Camera":"no"}]', '1582102388-H5d58c3a7d66444319d56d40bdeab707ci.jpg', '[{"name":"1582095676-blob"},{"name":"1582095677-blob"},{"name":"1582095677-blob"},{"name":"1582095677-blob"}]', 3, 3, 1, 1, 1, NULL, NULL, '2020-02-19 14:23:08', 0, 1),
(43, 9, 0, 'New coffee beans', 'De nouveaux grains de café', 'Superior quality coffee beans', 'Grains de café de qualité supérieure', 'Pure coffee beans, coffee beans,', 5, '["1","3","5"]', '1', 0.00, '20.00', '25.00', 'USD', '1 day', '10', 'Box packing', '[]', '[]', '1582522851-cocoa-beans-312.jpg', '[]', 1, 1, 1, 1, 1, NULL, NULL, '2020-02-24 11:10:51', 0, 1),
(44, 2, 0, '2019 4wheel new energy China electric vehicle/car ', '2019 4roues nouvelle énergie de la Chine véhicule électrique/voiture ', 'Jiangsu, China\r\nBrand Name:\r\nzibon\r\nSteering:\r\nLeft\r\nDrive:\r\nAWD\r\nMonth:\r\n2\r\nType:\r\nSedan\r\nFuel:\r\nElectric\r\nMade In:\r\nChina\r\nCondition:\r\nNew\r\nYear:\r\n2019, 2019\r\nGear Box:\r\nAutomatic\r\nPower type:\r\n100% electric\r\nMax Speed (km/h):\r\n180\r\nwarranty:\r\n150,000 km\r\nMAX Power(HP):\r\n160\r\nKeywords 1:\r\nelectric vehicles\r\nKeywords 2:\r\n2019 new car\r\nKeywords 3:\r\nbattery car\r\nKeywords 4:\r\nev car\r\nKeywords 5:\r\ne car', 'Jiangsu, Chine\r\nNom De Marque:\r\nzibon\r\nDirecteur:\r\nDe gauche\r\nEn voiture:\r\nAWD\r\nMois:\r\n2\r\nType:\r\nBerline\r\nCarburant:\r\nÉlectrique\r\nMade In:\r\nLa chine\r\nCondition:\r\nNouveau\r\nAnnée:\r\nÀ 2019 2019\r\nBoîte De Vitesses:\r\nAutomatique\r\nType d\'alimentation:\r\n100% électrique\r\nVitesse Max (km/h):\r\n180\r\ngarantie:\r\n150 000 km\r\nMAX Puissance(HP):\r\n160\r\nMots-clés 1:\r\nles véhicules électriques\r\nMots-clés 2:\r\n2019 nouvelle voiture\r\nMots-clés 3:\r\nbatterie de voiture\r\nMots-clés 4:\r\nev voiture\r\nMots-clés 5:\r\ne voiture', ' 2019 new car, ev car,electric vehicles,battery car,e car', 49, '["10","49"]', '1', 0.00, '15.00', '28.00', 'USD', '1', '1', 'Shanghai Ningbo Tianjing Dalian', '[{"1":["red","white"]},{"2":["160KW","200kw"]}]', '[{"Warranty":"150,000 km"}]', '1582523210-car1.jpg', NULL, 3, 3, 0, 1, 1, NULL, NULL, '2020-02-24 11:16:50', 0, 1),
(45, 1, 0, 'Ginger Coconut Hard Candy Sweets Confectionery', 'Gingembre, De La Noix De Coco Hard Candy Bonbons De La Confiserie', 'Ginger candy', 'Gingembre bonbons', 'Candy', 34, '["15","34"]', '1', 1.00, '0.80', '1.00', 'USD', '', '1500', '', '[]', '[]', '1583390618-toffee1.png', NULL, 3, 7, 1, 1, 1, NULL, NULL, '2020-03-05 12:13:38', 0, 1),
(46, 1, 0, 'Fruit Flavor Hard Candy Lollipop sweet confectionery ', 'La Saveur de Fruit Dur Sucette Bonbon sucré, de la confiserie ', 'Fruit Flavor Hard Candy Lollipop sweet confectionery ', 'La Saveur de Fruit Dur Sucette Bonbon sucré, de la confiserie ', 'Lollipop', 34, '["15","34"]', '1', 0.90, '0.80', '0.90', 'USD', '', '1000', '', '[{"1":["Red","Green","Orange","Violet","Gray"]},{"2":["34","36","38","40","42","44"]}]', '[]', '1583390984-toffee2.jpg', NULL, 3, 3, 1, 1, 1, NULL, NULL, '2020-03-05 12:19:43', 0, 1),
(47, 1, 0, 'Hard Candy', 'Hard Candy', 'Hard Candy', 'Hard Candy', 'Candy', 34, '["15","34"]', '1', 2.00, '1.00', '1.80', 'USD', '', '150', '', '[]', '[]', '1583391120-toffee3.jpg', NULL, 7, 7, 1, 1, 1, NULL, NULL, '2020-03-05 12:22:00', 0, 1),
(48, 2, 0, 'Sweet lollipop', 'Sweet lollipop', 'Sweet lollipop', 'Sweet lollipop', 'lollipop', 34, '["15","34"]', '1', 2.50, '2.00', '3.00', 'USD', '', '250', '', '[]', '[]', '1583488579-toffee1.png', '[{"name":"1583488579-toffee3.jpg"},{"name":"1583488579-toffee2.jpg"}]', 3, 3, 1, 1, 1, NULL, NULL, '2020-03-06 15:26:19', 0, 1),
(50, 9, 0, 'Electric Cars in Auto Sector', 'Les Voitures électriques dans le Secteur de l\'Automobile', 'Electric Cars are superb automobile in recent industry.', 'Les Voitures électriques sont superbe automobile récentes de l\'industrie.', 'electric cars, cars, car', 49, '["10","49"]', '1', 100.00, '100.00', '200.00', 'USD', '30 days', '100', 'Packaged with care', '[{"1":["Black","Blue","Red","Vanilla"]},{"2":["2 Seeter","4 Seeter","6 Seeter"]}]', '[{"Capacity":"1500"},{"Material":"Aluminium"}]', '1584008818-kia-seltos-punchy-orange.png', '[{"name":"1584008819-photo-1525609004556-c46c7d6cf023.jpg"},{"name":"1584014584-kia-seltos-punchy-orange.png"}]', 3, 3, 1, 1, 1, NULL, NULL, '2020-03-12 15:56:58', 0, 1),
(51, 9, 0, 'Fresh Canned Milk ', 'Fraîche, Lait En Boîte ', 'Dairy products or milk products are a type of food produced from or containing the milk of mammals. They are primarily produced from mammals such as cattle, water buffaloes, goats, sheep, camels. ', 'Les produits laitiers ou les produits laitiers sont un type de nourriture produite à partir de ou contenant du lait des mammifères. Ils sont principalement produits à partir de mammifères tels que les bovins, buffles d\'eau, des chèvres, des moutons, des chameaux. ', 'milk, dairy, cow milk, raw milk, cheese', 35, '["15","35"]', '1', 50.00, '10.00', '200.00', 'USD', '3 Days', '100', 'Stuffed and sealed', '[]', '[]', '1584011310-image.img.jpg', '[{"name":"1584011310-diary-products.jpg"},{"name":"1584011310-iStock-544807136.jpg"}]', 4, 4, 1, 1, 1, NULL, NULL, '2020-03-12 16:38:30', 0, 1),
(52, 1, 0, 'Fresh Meat', 'De La Viande Fraîche', 'fresh meat', 'de la viande fraîche', 'meat', 38, '["15","38"]', '1', 2.50, '2.00', '3.00', 'USD', '', '25', '', '[{"1":["Boneless","With Bone"]}]', '[{"Expiry":"3 months"}]', '1584529675-foodbeverages.jpeg', NULL, 1, 1, 1, 1, 1, NULL, NULL, '2020-03-18 16:37:55', 0, 1),
(53, 9, 0, 'ShirtE', 'Chemise', 'shirt', 'chemise', 'shirt', 46, '["2","46"]', '1', 3.00, '100.00', '400.00', '', '', '322', '', '[{"1":["red","green","blue","orange"]},{"2":["XL","XXL"]},{"3":["100% Cotton","95% Cotton- 5% Spandex"]}]', '[{"Capacity":"1500"},{"Material":"Aluminium"}]', '1584531250-1582102388-H5d58c3a7d66444319d56d40bdeab707ci.jpg', '[]', 3, 3, 1, 1, 1, NULL, NULL, '2020-03-18 17:04:10', 0, 1),
(54, 9, 0, 'aa', 'aa', 'aa', 'aa', 'aaa', 1, '["1"]', '1', 3.00, '2.00', '3.00', 'USD', '', '12', '', '[]', '[]', '1584534046-tractor.jpeg', NULL, 3, 3, 1, 1, 1, NULL, NULL, '2020-03-18 17:50:45', 0, 1),
(55, 9, 0, 'New shirt', 'Nouveau chemise', 'High quality shirts made from 100 % cotton', 'Haute qualité des maillots fabriqués à partir de 100 % coton', 'shirt, shirts for women,', 46, '["2","46"]', '1', 11.60, '12.00', '13.00', 'USD', '7 Days.', '20', 'individual packing', '[{"3":["Cotton","Silk"]}]', '[]', '1584604305-casual4.jpg', NULL, 7, 7, 1, 1, 1, NULL, NULL, '2020-03-19 13:21:45', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_local_supplier`
--

CREATE TABLE `t_local_supplier` (
  `id` bigint(20) NOT NULL,
  `customer_id` bigint(20) DEFAULT NULL,
  `supplier_name` varchar(255) DEFAULT NULL,
  `description` text,
  `status` tinyint(1) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_local_supplier`
--

INSERT INTO `t_local_supplier` (`id`, `customer_id`, `supplier_name`, `description`, `status`, `created_on`) VALUES
(1, 2, 'Michael', NULL, 1, '2020-02-04 07:44:45'),
(2, 2, 'DANIEL', NULL, 1, '2020-02-04 07:45:00'),
(3, 2, 'Charles', NULL, 0, '2020-02-04 07:45:13');

-- --------------------------------------------------------

--
-- Table structure for table `t_logistic`
--

CREATE TABLE `t_logistic` (
  `id` bigint(11) UNSIGNED NOT NULL,
  `logistic_partner_id` bigint(11) UNSIGNED NOT NULL,
  `email` varchar(100) NOT NULL,
  `port_of_origin` varchar(255) NOT NULL,
  `port_of_distribution` varchar(255) NOT NULL,
  `item` varchar(255) NOT NULL,
  `qty` bigint(11) UNSIGNED NOT NULL,
  `message` varchar(255) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_logistic`
--

INSERT INTO `t_logistic` (`id`, `logistic_partner_id`, `email`, `port_of_origin`, `port_of_distribution`, `item`, `qty`, `message`, `date`) VALUES
(1, 1, 'androbhikk@gmail.com', 'Kolkata', 'South 24 Parganas', 'Daffodils', 100, 'Please Send These ASAP', '2020-02-14 07:11:18');

-- --------------------------------------------------------

--
-- Table structure for table `t_logistic_user`
--

CREATE TABLE `t_logistic_user` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `contact` varchar(255) NOT NULL,
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_logistic_user`
--

INSERT INTO `t_logistic_user` (`id`, `name`, `email`, `contact`, `status`, `created`) VALUES
(1, 'Kousik Kayal', 'kousik.kayal@clematistech.com', '1234567890', 1, '2020-02-13 13:07:45');

-- --------------------------------------------------------

--
-- Table structure for table `t_news`
--

CREATE TABLE `t_news` (
  `news_id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `visible_to` tinyint(1) DEFAULT '1' COMMENT '1:-all,2:-login_user,3:-agents',
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `active` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_news_image`
--

CREATE TABLE `t_news_image` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `news_id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_notification`
--

CREATE TABLE `t_notification` (
  `notification_id` bigint(20) UNSIGNED NOT NULL,
  `to_id` bigint(20) UNSIGNED NOT NULL,
  `from_id` bigint(20) UNSIGNED NOT NULL,
  `message` text,
  `type` enum('rfq','enq','seller_req','seller_conf','agent_req','agent_conf','service_rfq','service_enq') DEFAULT NULL,
  `ref_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `is_seen` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_notification`
--

INSERT INTO `t_notification` (`notification_id`, `to_id`, `from_id`, `message`, `type`, `ref_id`, `created_date`, `is_seen`) VALUES
(1, 1, 2, 'Gentella have a new enquiry.', 'enq', 1, '2019-12-11 09:57:12', 1),
(2, 1, 2, 'Gentella have a new enquiry.', 'enq', 2, '2019-12-11 09:57:14', 1),
(3, 1, 2, 'Gentella have a new enquiry.', 'enq', 3, '2019-12-11 09:57:23', 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_order`
--

CREATE TABLE `t_order` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `plan_code` varchar(255) DEFAULT NULL,
  `amount` double UNSIGNED DEFAULT '0',
  `authorization_code` date DEFAULT NULL,
  `transaction_date` timestamp NULL DEFAULT NULL,
  `next_payment_date` date DEFAULT NULL,
  `order_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) DEFAULT '0' COMMENT '0:-pending,1:-suceess,2:-failed',
  `customer_ip_address` varchar(255) DEFAULT NULL,
  `paystack_customer_id` varchar(255) DEFAULT NULL,
  `order_request_data` text,
  `order_response_data` text,
  `customer_email_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_order`
--

INSERT INTO `t_order` (`id`, `customer_id`, `plan_code`, `amount`, `authorization_code`, `transaction_date`, `next_payment_date`, `order_date`, `status`, `customer_ip_address`, `paystack_customer_id`, `order_request_data`, `order_response_data`, `customer_email_id`) VALUES
(1, 1, 'PLN_rsj8pfhug4zh9v7', 3000000, NULL, NULL, NULL, '2019-12-17 07:15:08', 0, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `t_order_log`
--

CREATE TABLE `t_order_log` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `proposal_id` bigint(20) UNSIGNED DEFAULT NULL,
  `transction_id` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '1:-success,0:-pending,2:-failed',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_date` timestamp NULL DEFAULT NULL,
  `total_amount` double(11,2) UNSIGNED DEFAULT '0.00',
  `payment_mode` tinyint(1) DEFAULT NULL COMMENT '1:-cod,2:-bank tranfer',
  `item_details` text,
  `buyer_info` text,
  `response` text,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `seller_id` bigint(20) UNSIGNED DEFAULT NULL,
  `order_status` tinyint(1) DEFAULT '0' COMMENT '0:Order Placed, 1: Order Accepted, 2:Order Shipped, 3:Order Delivered, 4:Order Closed,5:Order Cancelled'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_order_log`
--

INSERT INTO `t_order_log` (`id`, `proposal_id`, `transction_id`, `status`, `created_date`, `updated_date`, `total_amount`, `payment_mode`, `item_details`, `buyer_info`, `response`, `user_id`, `seller_id`, `order_status`) VALUES
(1, 1, '', 0, '2020-02-28 06:27:11', '2020-02-28 07:21:07', 200.00, 1, NULL, NULL, NULL, 9, 2, 0),
(4, 0, '1583913740', 0, '2020-03-11 08:02:20', '2020-03-13 07:33:22', 0.00, 1, '[{"itemid":"NDY=","itemvariance":"[{\\"varianceName\\":\\"Color\\",\\"varianceAttribute\\":\\"Orange\\"},{\\"varianceName\\":\\"Size\\",\\"varianceAttribute\\":\\"36\\"}]","perItemPrice":"0.90","currency":"Dollar","itemname":"Fruit Flavor Hard Candy Lollipop sweet confectionery ","itemimg":"http:\\/\\/192.168.2.95\\/miraimpex\\/admin\\/uploads\\/selleritem\\/1583390984-toffee2.jpg","quantity":"1000","moq":"1000"},{"itemid":"NDg=","itemvariance":"","perItemPrice":"2.50","currency":"Dollar","itemname":"Sweet lollipop","itemimg":"http:\\/\\/192.168.2.95\\/miraimpex\\/admin\\/uploads\\/selleritem\\/1583488579-toffee1.png","quantity":"250","moq":"250"}]', '[{"name":"firstname","value":"Himadri"},{"name":"email","value":"reply2him@gmail.com"},{"name":"address","value":"kanchrapara"},{"name":"city","value":"Kanchrapara"},{"name":"state","value":"West Bengal"},{"name":"zip","value":"743134"},{"name":"paymentMode","value":"1"},{"name":"totalToPay","value":"1525"}]', NULL, 1, 9, 0),
(6, 0, '1583913877', 0, '2020-03-11 08:04:37', NULL, 3.40, 1, '[{"itemid":"NDY=","itemvariance":"[{\\"varianceName\\":\\"Color\\",\\"varianceAttribute\\":\\"Orange\\"},{\\"varianceName\\":\\"Size\\",\\"varianceAttribute\\":\\"36\\"}]","perItemPrice":"0.90","currency":"Dollar","itemname":"Fruit Flavor Hard Candy Lollipop sweet confectionery ","itemimg":"http:\\/\\/192.168.2.95\\/miraimpex\\/admin\\/uploads\\/selleritem\\/1583390984-toffee2.jpg","quantity":"1000","moq":"1000"},{"itemid":"NDg=","itemvariance":"","perItemPrice":"2.50","currency":"Dollar","itemname":"Sweet lollipop","itemimg":"http:\\/\\/192.168.2.95\\/miraimpex\\/admin\\/uploads\\/selleritem\\/1583488579-toffee1.png","quantity":"250","moq":"250"}]', '[{"name":"firstname","value":"Himadri"},{"name":"email","value":"reply2him@gmail.com"},{"name":"address","value":"kanchrapara"},{"name":"city","value":"Kanchrapara"},{"name":"state","value":"West Bengal"},{"name":"zip","value":"743134"},{"name":"paymentMode","value":"1"},{"name":"totalToPay","value":"1525"}]', NULL, 1, 9, 0),
(7, 0, '1583914074', 0, '2020-03-11 08:07:54', '2020-03-13 09:57:40', 1525.00, 1, '[{"itemid":"NDY=","itemvariance":"[{\\"varianceName\\":\\"Color\\",\\"varianceAttribute\\":\\"Orange\\"},{\\"varianceName\\":\\"Size\\",\\"varianceAttribute\\":\\"36\\"}]","perItemPrice":"0.90","currency":"Dollar","itemname":"Fruit Flavor Hard Candy Lollipop sweet confectionery ","itemimg":"http:\\/\\/192.168.2.95\\/miraimpex\\/admin\\/uploads\\/selleritem\\/1583390984-toffee2.jpg","quantity":"1000","moq":"1000"},{"itemid":"NDg=","itemvariance":"","perItemPrice":"2.50","currency":"Dollar","itemname":"Sweet lollipop","itemimg":"http:\\/\\/192.168.2.95\\/miraimpex\\/admin\\/uploads\\/selleritem\\/1583488579-toffee1.png","quantity":"250","moq":"250"}]', '[{"name":"firstname","value":"Himadri"},{"name":"email","value":"reply2him@gmail.com"},{"name":"address","value":"kanchrapara"},{"name":"city","value":"Kanchrapara"},{"name":"state","value":"West Bengal"},{"name":"zip","value":"743134"},{"name":"paymentMode","value":"1"},{"name":"totalToPay","value":"1525"}]', NULL, 3, 9, 2),
(8, 0, '1583914145', 0, '2020-03-11 08:09:05', '2020-03-17 08:39:02', 1525.00, 2, '[{"itemid":"NDY=","itemvariance":"[{\\"varianceName\\":\\"Color\\",\\"varianceAttribute\\":\\"Orange\\"},{\\"varianceName\\":\\"Size\\",\\"varianceAttribute\\":\\"36\\"}]","perItemPrice":"0.90","currency":"Dollar","itemname":"Fruit Flavor Hard Candy Lollipop sweet confectionery ","itemimg":"http:\\/\\/192.168.2.95\\/miraimpex\\/admin\\/uploads\\/selleritem\\/1583390984-toffee2.jpg","quantity":"1000","moq":"1000"},{"itemid":"NDg=","itemvariance":"","perItemPrice":"2.50","currency":"Dollar","itemname":"Sweet lollipop","itemimg":"http:\\/\\/192.168.2.95\\/miraimpex\\/admin\\/uploads\\/selleritem\\/1583488579-toffee1.png","quantity":"250","moq":"250"}]', '[{"name":"firstname","value":"Himadri"},{"name":"email","value":"reply2him@gmail.com"},{"name":"address","value":"kanchrapara"},{"name":"city","value":"Kanchrapara"},{"name":"state","value":"West Bengal"},{"name":"zip","value":"743134"},{"name":"paymentMode","value":"2"},{"name":"totalToPay","value":"1525"}]', NULL, 3, 9, 1),
(9, 0, '1584020017', 0, '2020-03-12 13:33:36', '2020-03-13 10:17:47', 625.00, 2, '[{"itemid":"NDg=","itemvariance":"","perItemPrice":"2.50","currency":"USD","itemname":"Sweet lollipop","itemimg":"http:\\/\\/192.168.2.95\\/miraimpex\\/admin\\/uploads\\/selleritem\\/1583488579-toffee1.png","quantity":"250","moq":"250","seller_id":"Mg==","business_name":"Smart Company"}]', '[{"name":"firstname","value":"Koli Masudi"},{"name":"email","value":"koli@test.com"},{"name":"address","value":"15 Hatton street. Green view. Near Highland Park."},{"name":"city","value":"London"},{"name":"state","value":"NY"},{"name":"zip","value":"10001"},{"name":"contactnumber1","value":"9087678765"},{"name":"contactnumber2","value":""},{"name":"paymentMode","value":"2"},{"name":"totalToPay","value":"625"}]', NULL, 3, 2, 3);

-- --------------------------------------------------------

--
-- Table structure for table `t_proposal`
--

CREATE TABLE `t_proposal` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `from_id` bigint(20) UNSIGNED NOT NULL,
  `to_id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `file` varchar(255) DEFAULT NULL,
  `amount` double(11,2) UNSIGNED DEFAULT '0.00',
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `currency` varchar(255) DEFAULT NULL,
  `refference_id` bigint(20) UNSIGNED DEFAULT NULL,
  `refference_type` varchar(255) DEFAULT NULL,
  `is_paid` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_proposal`
--

INSERT INTO `t_proposal` (`id`, `from_id`, `to_id`, `title`, `description`, `file`, `amount`, `created_date`, `currency`, `refference_id`, `refference_type`, `is_paid`) VALUES
(1, 2, 9, 'we provide a top-level product.It will help avoid downtime.', 'Printing has been making high-quality office printers for organizations just like yours for 15 years. We understand how frustrating it is when your supplier is disorganized, late, or hard to communicate with. We pride ourselves on our top-level customer service and a 36-hour turnaround time for all requests.', '1582801508-vehicle-details-26-04-2019.pdf', 200.00, '2020-02-27 11:05:08', 'USD', 14, 'rfq', 0),
(2, 9, 1, 'Revised Proposal', 'I will give you 20 kg of cocoa beans @10 USD per kg. ', '1582802599-PR and advertising.jpeg', 200.00, '2020-02-27 11:23:19', 'USD', 12, 'rfq', 0),
(7, 4, 9, 'High quality full sleeves at very attractive price!', '100% cotton shirt', '1582803285-Miraimpex-logo.pdf', 2.00, '2020-02-27 11:34:45', 'USD', 2, 'enq', 0),
(8, 9, 1, 'High quality Cocoa beans at very attractive price!', 'Fresh beans with 100 % satisfaction.', '1582809369-Miraimpex-logo.pdf', 600.00, '2020-02-27 13:16:09', 'USD', 12, 'rfq', 0),
(9, 2, 9, 'test proposal', 'test description', '1582870958-vehicle-details-30-04-2019 (17).pdf', 200.00, '2020-02-28 06:22:38', 'dolloar', 14, 'rfq', 0);

-- --------------------------------------------------------

--
-- Table structure for table `t_provider_service_item`
--

CREATE TABLE `t_provider_service_item` (
  `provider_service_item_id` int(10) UNSIGNED NOT NULL,
  `provider_id` bigint(20) UNSIGNED DEFAULT NULL,
  `service_item_id` bigint(20) UNSIGNED DEFAULT NULL,
  `uom_id` bigint(20) UNSIGNED DEFAULT NULL,
  `min_rate` double(11,2) DEFAULT NULL,
  `max_rate` double(11,2) DEFAULT '0.00',
  `description` text,
  `special_instruction` text,
  `terms_condition` text,
  `active` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_provider_service_item`
--

INSERT INTO `t_provider_service_item` (`provider_service_item_id`, `provider_id`, `service_item_id`, `uom_id`, `min_rate`, `max_rate`, `description`, `special_instruction`, `terms_condition`, `active`) VALUES
(1, 1, 18, 17, 2500.00, 5000.00, 'Hiring For Animal Farm\r\nadfdfdfdssdfsfsdf', 'fsdfsdfdsfds', 'fdsfdsfdsf', 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_provider_service_item_image`
--

CREATE TABLE `t_provider_service_item_image` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `provider_service_item_id` bigint(20) UNSIGNED DEFAULT NULL,
  `image` varchar(255) NOT NULL,
  `active` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_provider_service_item_image`
--

INSERT INTO `t_provider_service_item_image` (`id`, `provider_service_item_id`, `image`, `active`) VALUES
(1, 1, '1576144030-20000981_6-fresho-spring-onion.jpg', 1),
(2, 1, '1576144044-1546320860-9359.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_quotation`
--

CREATE TABLE `t_quotation` (
  `id` int(11) NOT NULL,
  `buyer_id` int(10) DEFAULT NULL,
  `title` varchar(225) DEFAULT NULL,
  `cat_id` int(10) DEFAULT NULL,
  `sub_cat_id` int(10) DEFAULT NULL,
  `item_id` int(10) DEFAULT NULL,
  `species_id` int(10) DEFAULT NULL,
  `qty` int(10) DEFAULT NULL,
  `uom_id` int(10) DEFAULT NULL,
  `details` text,
  `image` varchar(150) DEFAULT NULL,
  `other_requirement` text,
  `tc_accept` int(1) DEFAULT NULL,
  `submitted_on` datetime DEFAULT NULL,
  `active` int(1) DEFAULT '1',
  `seller_id` int(10) DEFAULT NULL,
  `seller_response` text,
  `seller_response_on` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_rate_exchange`
--

CREATE TABLE `t_rate_exchange` (
  `base_currency` varchar(255) NOT NULL,
  `to_currency` varchar(255) NOT NULL,
  `rate` double(11,2) DEFAULT '0.00',
  `updated_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_rate_exchange`
--

INSERT INTO `t_rate_exchange` (`base_currency`, `to_currency`, `rate`, `updated_date`) VALUES
('USD', 'GMD', 50.95, '2020-03-16 13:03:58');

-- --------------------------------------------------------

--
-- Table structure for table `t_report_incident`
--

CREATE TABLE `t_report_incident` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `to_id` bigint(20) UNSIGNED DEFAULT NULL,
  `message` text,
  `images` varchar(255) DEFAULT NULL,
  `from_id` bigint(20) UNSIGNED DEFAULT NULL,
  `type` enum('rfq','enq') DEFAULT NULL,
  `ref_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_request_for_quotation`
--

CREATE TABLE `t_request_for_quotation` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `to_id` bigint(20) UNSIGNED DEFAULT NULL,
  `from_id` bigint(20) UNSIGNED DEFAULT NULL,
  `seller_item_id` bigint(20) UNSIGNED DEFAULT NULL,
  `quantity` bigint(20) DEFAULT NULL,
  `message` text,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_rfq`
--

CREATE TABLE `t_rfq` (
  `rfq_id` bigint(20) UNSIGNED NOT NULL,
  `buyer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `cat_id` int(10) UNSIGNED DEFAULT NULL,
  `uom_id` bigint(20) UNSIGNED DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `message` text,
  `other_requerment` text,
  `expiry_date` timestamp NULL DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) DEFAULT '1',
  `qrname` varchar(255) DEFAULT NULL,
  `rfqlocation` varchar(255) DEFAULT NULL,
  `latitudes` varchar(255) DEFAULT NULL,
  `longitude` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_rfq`
--

INSERT INTO `t_rfq` (`rfq_id`, `buyer_id`, `cat_id`, `uom_id`, `quantity`, `message`, `other_requerment`, `expiry_date`, `created_date`, `status`, `qrname`, `rfqlocation`, `latitudes`, `longitude`) VALUES
(1, 6, 6, 10, 20, 'Dear Sir/Madam,\nI\'m looking for products with the following specifications: test', 'optional text', NULL, '2020-02-18 11:48:00', 1, 'bluetooth headset', '', '', ''),
(2, 1, 48, 10, 50, 'Genuine cotton shirt\r\n							', '', NULL, '2020-02-18 11:53:24', 1, '100% cotton shirt', 'Kolkata, West Bengal, India', '22.572646', '88.36389500000001'),
(3, 6, 6, 10, 20, 'Mojibul Dear Sir/Madam,\nI\'m looking for products with the following specifications: test', 'optional text', NULL, '2020-02-18 11:55:53', 1, 'bluetooth headset', '', '', ''),
(12, 1, 6, 1, 20, 'Dear Sir/Madam,\nI\'m looking for products with the following specifications:\n							', '', NULL, '2020-02-18 12:13:05', 1, 'kolki', 'Kolkata, West Bengal, India', '22.572646', '88.36389500000001'),
(13, 6, 5, 10, 20, 'Mojibul Dear Sir/Madam,\nI\'m looking for products with the following specifications: test', 'optional text', NULL, '2020-02-18 12:18:48', 1, 'bluetooth headset', '', '', ''),
(14, 9, 48, 1, 400, 'I\'m looking for casual printed shirt with the following specifications: fabric- cotton \n							', '', NULL, '2020-02-20 13:11:13', 1, 'casual printed shirt', 'Kolkata, West Bengal, India', '22.572646', '88.36389500000001'),
(15, 6, 5, 10, 20, 'Dear Sir/Madam,\r\nI\'m looking for products with the following specifications: test22', 'optional text2', NULL, '2020-02-19 12:18:48', 1, 'bluetooth headset2', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `t_rfq_image`
--

CREATE TABLE `t_rfq_image` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `req_id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_rfq_image`
--

INSERT INTO `t_rfq_image` (`id`, `req_id`, `image`) VALUES
(1, 1, '1582026481-3.jpg'),
(2, 1, '1582026481-4.jpg'),
(3, 2, '1582026805-fashion.jpeg'),
(4, 2, '1582026805-fashion.jpeg'),
(5, 3, '1582026954-3.jpg'),
(18, 12, '1582027986-cocoa-beans-312.jpg'),
(19, 13, '1582028329-3.jpg'),
(20, 14, '1582204274-fashion.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `t_rfq_thread`
--

CREATE TABLE `t_rfq_thread` (
  `id` bigint(20) NOT NULL,
  `rfq_id` bigint(20) DEFAULT NULL,
  `to_customer_id` bigint(20) DEFAULT NULL,
  `from_customer_id` bigint(20) DEFAULT NULL,
  `date` timestamp NULL DEFAULT NULL,
  `message` text,
  `images` text,
  `is_seen` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_rfq_thread`
--

INSERT INTO `t_rfq_thread` (`id`, `rfq_id`, `to_customer_id`, `from_customer_id`, `date`, `message`, `images`, `is_seen`) VALUES
(1, 13, 6, 2, '2020-02-02 18:30:00', 'hi 13', NULL, 0),
(2, 15, 6, 2, '2020-02-02 18:30:00', 'hi 1315', NULL, 0),
(3, 13, 6, 1, '2020-02-02 18:30:00', 'hi 13', NULL, 0),
(4, 13, 6, 2, '2020-02-05 18:30:00', 'hi 132', NULL, 0),
(5, 13, 2, 6, '2020-02-05 18:30:00', 'hi 132e', NULL, 0),
(6, 14, 9, 2, '2020-02-26 12:53:32', 'Hi ,abc', '', 0),
(7, 14, 9, 2, '2020-02-26 12:59:06', 'hello', '1582721946-woman-Nigeria-millet-grains-bowl-village-Sadongori-2005.jpg', 0),
(8, 13, 6, 2, '2020-02-26 13:33:53', 'i want to send proposal', '', 0),
(9, 14, 9, 2, '2020-02-27 05:22:20', 'Please tell me about proposal', '', 0),
(10, 12, 1, 9, '2020-02-27 05:29:46', 'hii', '', 0),
(11, 13, 6, 1, '2020-02-27 05:36:33', 'hi', '', 0),
(12, 14, 2, 9, '2020-02-27 06:33:04', 'thanks', '', 0),
(13, 14, 2, 9, '2020-02-27 07:33:27', '', '1582788807-tractor.jpeg', 0),
(14, 13, 6, 1, '2020-02-28 08:05:28', 'I will give you fresh coffee beans from Ghana. Please see', '1582877128-cocoa-beans-312.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `t_rfq_thread_image`
--

CREATE TABLE `t_rfq_thread_image` (
  `id` bigint(20) NOT NULL,
  `rfq_thread_id` bigint(20) DEFAULT NULL,
  `images` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_seller_image`
--

CREATE TABLE `t_seller_image` (
  `id` int(11) NOT NULL,
  `seller_id` int(10) DEFAULT NULL,
  `image` varchar(225) DEFAULT NULL,
  `type` tinyint(1) NOT NULL COMMENT '1:-image,2:certificate',
  `active` int(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_seller_image`
--

INSERT INTO `t_seller_image` (`id`, `seller_id`, `image`, `type`, `active`) VALUES
(1, 1, '1576054477-NimOil.jpg', 1, 1),
(2, 2, '1576054477-NimOil.jpg', 1, 1),
(3, 2, '1576054477-fishes.jpg', 1, 1),
(6, 5, '1581595972-tea1.jpg', 1, 1),
(7, 5, '1581595972-cat (1).jpg', 1, 1),
(8, 3, '1581596504-20140710200421tarama001.jpg', 1, 1),
(9, 3, '1581596504-dsc019172235749.jpg', 1, 1),
(10, 2, '1581596509-Screenshot from 2020-01-13 18-35-53.png', 1, 1),
(11, 3, '1581596743-Screenshot from 2020-01-13 18-35-53.png', 1, 1),
(18, 2, '1581599688-img20160910125412753879.jpg', 2, 1),
(19, 2, '1581599714-img20160910125412753879.jpg', 2, 1),
(20, 2, '1581599767-img20160910125412753879.jpg', 2, 1),
(21, 2, '1581599780-img20160910125412753879.jpg', 2, 1),
(22, 2, '1581599795-img20160910125412753879.jpg', 2, 1),
(23, 2, '1581599840-img20160910125412753879.jpg', 2, 1),
(28, 2, '1581600052-img20160910125412753879.jpg', 2, 1),
(29, 2, '1581600067-img20160910125412753879.jpg', 2, 1),
(30, 2, '1581600081-img20160910125412753879.jpg', 2, 1),
(31, 2, '1581600113-img20160910125412753879.jpg', 2, 1),
(32, 2, '1581600113-process-flow.docx', 2, 1),
(33, 2, '1581600113-description.pdf', 2, 1),
(34, 2, '1581600326-img20160910125412753879.jpg', 3, 1),
(35, 2, '1581600326-process-flow.docx', 3, 1),
(36, 2, '1581600326-description.pdf', 3, 1),
(37, 3, '1581600968-Tractor-Operator-Training-kaoma-2.-13.9.19-1024x768.jpg', 2, 1),
(38, 3, '1581600993-Screenshot from 2020-01-30 14-54-24.png', 2, 1),
(39, 3, '1581917454-Registration Process Flow.docx', 3, 1),
(40, 10, '1582182331-1MobileRC API.doc', 2, 1),
(41, 10, '1582182331-1.jpg', 2, 1),
(42, 10, '1582182332-A1RECHARGEApi-V1.0.0.pdf', 2, 1),
(43, 10, '1582182332-3.jpg', 3, 1),
(44, 10, '1582182332-API  v1.3.docx', 3, 1),
(45, 10, '1582182332-0001014550016238228_10152019_11042019.PDF', 3, 1),
(46, 9, '1582183431-Printing and graphic designs.png', 2, 1),
(47, 9, '1582183432-Miraimpex-logo.pdf', 3, 1),
(48, 9, '1582183432-logo.jpg', 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_seller_item`
--

CREATE TABLE `t_seller_item` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `item_id` bigint(20) UNSIGNED DEFAULT NULL,
  `species_id` bigint(20) UNSIGNED DEFAULT NULL,
  `uom_id` bigint(20) UNSIGNED DEFAULT NULL,
  `item_description` text,
  `special_information` text,
  `packing_information` text,
  `shipping_information` text,
  `item_image` varchar(255) DEFAULT NULL,
  `min_quantity` bigint(20) UNSIGNED DEFAULT NULL,
  `min_price` double(11,2) DEFAULT NULL,
  `max_price` double(11,2) DEFAULT NULL,
  `organic_YN` tinyint(1) DEFAULT '1',
  `is_priority` tinyint(1) DEFAULT '0',
  `active` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_seller_item`
--

INSERT INTO `t_seller_item` (`id`, `customer_id`, `item_id`, `species_id`, `uom_id`, `item_description`, `special_information`, `packing_information`, `shipping_information`, `item_image`, `min_quantity`, `min_price`, `max_price`, `organic_YN`, `is_priority`, `active`) VALUES
(1, 1, 88, 115, 4, 'Fresh Milk', 'Nil', 'Nil', 'Nil', NULL, 30, 1000.00, 5000.00, 1, 0, 1),
(2, 1, 94, 133, 7, 'Selling Dogs', 'Nil', 'Nil', 'Nil', NULL, 10, 3000.00, 5000.00, 1, 0, 1),
(3, 1, 91, 119, 4, 'Pure Honey', 'Nil', 'Nil', 'Nil', NULL, 12, 1000.00, 5000.00, 1, 0, 1),
(4, 1, 44, 55, 2, 'Sunflower Seeds', 'Nil', 'Nil', 'Nil', NULL, 15, 1000.00, 5000.00, 1, 0, 1),
(5, 1, 4, 19, 1, 'fsfdsfdsdsfs', '', '', '', NULL, 12, 2500.00, 5000.00, 1, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_seller_item_image`
--

CREATE TABLE `t_seller_item_image` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `seller_item_id` bigint(20) UNSIGNED NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_seller_item_image`
--

INSERT INTO `t_seller_item_image` (`id`, `seller_item_id`, `image`, `active`) VALUES
(1, 1, '1576050643-Milk.jpg', 1),
(2, 2, '1576050696-Dogs.jpg', 1),
(3, 3, '1576050740-Honey.jpeg', 1),
(4, 4, '1576050791-SunflowerSeeds.jpg', 1),
(5, 5, '1576143972-rice.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_seller_profile`
--

CREATE TABLE `t_seller_profile` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cust_id` bigint(20) UNSIGNED DEFAULT NULL,
  `business_name` varchar(255) DEFAULT NULL,
  `address` text,
  `country` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `lat` varchar(255) DEFAULT NULL,
  `long` varchar(255) DEFAULT NULL,
  `contact_name` varchar(255) DEFAULT NULL,
  `contact_number` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  `update_by` bigint(20) UNSIGNED DEFAULT NULL,
  `update_on` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_seller_profile`
--

INSERT INTO `t_seller_profile` (`id`, `cust_id`, `business_name`, `address`, `country`, `state`, `city`, `lat`, `long`, `contact_name`, `contact_number`, `active`, `update_by`, `update_on`) VALUES
(1, 1, 'HB Agency', 'kanchrapara', 'India', 'New Delhi', 'Halisahar', '', '', 'Himadri sekhar Bandyopadhyay', '9876545678', 1, 1, '2019-12-10 12:27:12'),
(2, 3, 'Demo Fish Bussiness', 'Abuja, Nigeriaaa', 'Nigeria', 'Abuja ss', 'Kuchigworo ss', '', '', 'XXX', '1234567890', 1, 1, '2019-12-11 08:56:11'),
(5, 2, 'Smart Company', 'ani pvt ltd', 'India', 'West Bengal', 'kolkata', '', '', 'Aniruddha.das', '9674391164', 1, 1, '2020-02-17 06:15:35'),
(6, 4, 'Dutta Imports & Exports Company', 'kolkata', 'India', 'West Bengal', 'Kolkata', '', '', 'Prosenjit kumar Dutta', '8456900322', 1, 1, '2020-02-17 07:00:38'),
(7, 10, 'ani pvt ltd', 'slatlake', 'india', ' west bengal', 'kolkata', '', '', 'Aniruddha.das', '9674391164', 1, NULL, NULL),
(8, 9, 'CHB Company ', '98, Gobinda basu lane. post halisahar. Near recreation club.', 'India', 'West Bengal', 'Kanchrapara', '', '', 'Bapi banerjee', '9051739826', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `t_service_category`
--

CREATE TABLE `t_service_category` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `service_category_name` varchar(255) DEFAULT NULL,
  `slug` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `parent_id` bigint(20) UNSIGNED DEFAULT NULL,
  `request_on` time DEFAULT NULL,
  `request_by` bigint(20) UNSIGNED DEFAULT NULL,
  `is_approved` tinyint(1) DEFAULT '1',
  `approved_by` bigint(20) UNSIGNED DEFAULT NULL,
  `approved_on` datetime DEFAULT NULL,
  `rejected_by` bigint(20) UNSIGNED DEFAULT NULL,
  `rejected_on` timestamp NULL DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_service_category`
--

INSERT INTO `t_service_category` (`id`, `service_category_name`, `slug`, `image`, `parent_id`, `request_on`, `request_by`, `is_approved`, `approved_by`, `approved_on`, `rejected_by`, `rejected_on`, `active`) VALUES
(1, 'Tractor Services', 'tractor-services', '1575981734Tractor.jpg', 0, NULL, NULL, 1, NULL, NULL, NULL, NULL, 1),
(2, 'Hiring of Labor', 'hiring-of-labor', '1575981206labour.jpg', 0, NULL, NULL, 1, NULL, NULL, NULL, NULL, 1),
(3, 'Renting of Farms, & Other Farming Equipments', 'renting-of-farms-&-other-farming-equipments', '1576050562labour.jpg', 0, NULL, NULL, 1, NULL, NULL, NULL, NULL, 1),
(4, 'Fertilizers', 'fertilizers', '1575981481fertilizer.jpg', 0, NULL, NULL, 1, NULL, NULL, NULL, NULL, 1),
(5, 'Feeds', 'feeds', '1575981536animal-feeds.jpg', 0, NULL, NULL, 1, NULL, NULL, NULL, NULL, 1),
(6, 'Micronutrients', 'micronutrients', '1575981599Micronutrients.jpg', 0, NULL, NULL, 1, NULL, NULL, NULL, NULL, 1),
(7, 'Medicines', 'medicines', '1575981663Medicines.jpg', 0, NULL, NULL, 1, NULL, NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_service_cat_uom_relation`
--

CREATE TABLE `t_service_cat_uom_relation` (
  `id` int(11) NOT NULL,
  `cat_id` int(5) NOT NULL,
  `uom_id` int(5) NOT NULL,
  `active` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_service_cat_uom_relation`
--

INSERT INTO `t_service_cat_uom_relation` (`id`, `cat_id`, `uom_id`, `active`) VALUES
(11, 5, 8, 1),
(12, 5, 9, 1),
(13, 4, 8, 1),
(14, 4, 9, 1),
(15, 2, 17, 1),
(16, 2, 16, 1),
(17, 2, 15, 1),
(18, 7, 12, 1),
(19, 7, 11, 1),
(20, 7, 10, 1),
(21, 6, 8, 1),
(22, 6, 9, 1),
(23, 3, 14, 1),
(24, 1, 13, 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_service_item`
--

CREATE TABLE `t_service_item` (
  `service_item_id` bigint(20) UNSIGNED NOT NULL,
  `service_category_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `description` text,
  `request_on` time DEFAULT NULL,
  `request_by` bigint(20) UNSIGNED DEFAULT NULL,
  `is_approved` tinyint(1) DEFAULT '1',
  `approved_by` bigint(20) UNSIGNED DEFAULT NULL,
  `approved_on` datetime DEFAULT NULL,
  `rejected_by` bigint(20) UNSIGNED DEFAULT NULL,
  `rejected_on` timestamp NULL DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_service_item`
--

INSERT INTO `t_service_item` (`service_item_id`, `service_category_id`, `name`, `slug`, `image`, `description`, `request_on`, `request_by`, `is_approved`, `approved_by`, `approved_on`, `rejected_by`, `rejected_on`, `status`) VALUES
(1, 4, 'Organic Fertilizer', 'organic-fertilizer', '', 'Organic Fertilizer', '00:20:19', 1, 0, NULL, NULL, 0, NULL, 1),
(2, 4, 'Inorganic Fertilizers', 'inorganic-fertilizers', '', 'Inorganic Fertilizers\r\n', '00:20:19', 1, 0, NULL, NULL, 0, NULL, 1),
(3, 5, 'Poultry Feeds (Broilers)', 'poultry-feeds--broilers-', '', 'Poultry Feeds (Broilers)\r\n\r\n', '00:20:19', 1, 0, NULL, NULL, 0, NULL, 1),
(4, 5, 'Poultry Feeds (Layers)', 'poultry-feeds--layers-', '', 'Poultry Feeds (Layers)\r\n\r\n\r\n', '00:20:19', 1, 0, NULL, NULL, 0, NULL, 1),
(5, 5, 'Cattle Feeds', 'cattle-feeds', '', 'Cattle Feeds\r\n\r\n\r\n\r\n', '00:20:19', 1, 0, NULL, NULL, 0, NULL, 1),
(6, 5, 'Other Feeds', 'other-feeds', '', 'Other Feeds\r\n\r\n\r\n\r\n\r\n', '00:20:19', 1, 0, NULL, NULL, 0, NULL, 1),
(7, 6, 'AZOMITE', 'azomite', '', 'AZOMITE\r\n\r\n\r\n\r\n', '00:20:19', 1, 0, NULL, NULL, 0, NULL, 1),
(8, 7, 'Vaccines', 'vaccines', '', 'Vaccines\r\n\r\n\r\n\r\n\r\n', '00:20:19', 1, 0, NULL, NULL, 0, NULL, 1),
(9, 7, 'Antibiotics', 'antibiotics', '', 'Antibiotics\r\n\r\n\r\n\r\n\r\n\r\n', '00:20:19', 1, 0, NULL, NULL, 0, NULL, 1),
(10, 7, 'Suplements ', 'suplements-', '', 'Suplements \r\n', '00:20:19', 1, 0, NULL, NULL, 0, NULL, 1),
(11, 7, 'Produce Boosters', 'produce-boosters', '', 'Produce Boosters\r\n', '00:20:19', 1, 0, NULL, NULL, 0, NULL, 1),
(12, 1, 'Tractor Hiring', 'tractor-hiring', '', 'Tractor Hiring\r\n\r\n', '00:20:19', 1, 0, NULL, NULL, 0, NULL, 1),
(13, 1, 'Tractor Servicing', 'tractor-servicing', '', 'Tractor Servicing\r\n\r\n\r\n', '00:20:19', 1, 0, NULL, NULL, 0, NULL, 1),
(14, 1, 'Tractor Sales', 'tractor-sales', '', 'Tractor Sales\r\n\r\n\r\n\r\n', '00:20:19', 1, 0, NULL, NULL, 0, NULL, 1),
(15, 1, 'Renting of Farms', 'renting-of-farms', '', 'Renting of Farms\r\n\r\n\r\n\r\n\r\n', '00:20:19', 1, 0, NULL, NULL, 0, NULL, 1),
(16, 1, 'Renting other Farming Materials', 'renting-other-farming-materials', '', 'Renting other Farming Materials\r\n\r\n\r\n\r\n\r\n', '00:20:19', 1, 0, NULL, NULL, 0, NULL, 1),
(17, 1, 'Hiring For Plant Farm', 'hiring-for-plant-farm', '', 'Hiring For Plant Farm\r\n\r\n\r\n\r\n\r\n\r\n', '00:20:19', 1, 0, NULL, NULL, 0, NULL, 1),
(18, 2, 'Hiring For Animal Farm', 'hiring-for-animal-farm', '', 'Hiring For Animal Farm\r\n', '00:20:19', 1, 0, NULL, NULL, 0, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_setting`
--

CREATE TABLE `t_setting` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `company_logo` varchar(255) DEFAULT NULL,
  `company_mobile` varchar(255) DEFAULT NULL,
  `company_fax` varchar(255) DEFAULT NULL,
  `company_address` text,
  `per_page` bigint(20) DEFAULT '10',
  `notification_time` bigint(20) UNSIGNED DEFAULT NULL,
  `news_time` bigint(20) UNSIGNED DEFAULT NULL,
  `information_time` bigint(20) UNSIGNED DEFAULT NULL,
  `delivery_charges` text,
  `rfq_closing` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_on` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_setting`
--

INSERT INTO `t_setting` (`id`, `company_name`, `company_logo`, `company_mobile`, `company_fax`, `company_address`, `per_page`, `notification_time`, `news_time`, `information_time`, `delivery_charges`, `rfq_closing`, `updated_by`, `updated_on`) VALUES
(1, NULL, NULL, NULL, NULL, NULL, 5, NULL, NULL, NULL, '', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `t_species`
--

CREATE TABLE `t_species` (
  `id` bigint(20) NOT NULL,
  `species_name` varchar(255) DEFAULT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `item_id` bigint(20) DEFAULT NULL,
  `species_image` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  `request_seller_id` bigint(20) DEFAULT NULL,
  `request_on` timestamp NULL DEFAULT NULL,
  `is_approved` tinyint(1) DEFAULT '1',
  `approved_by` bigint(20) DEFAULT NULL,
  `approved_on` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_species`
--

INSERT INTO `t_species` (`id`, `species_name`, `slug`, `item_id`, `species_image`, `active`, `request_seller_id`, `request_on`, `is_approved`, `approved_by`, `approved_on`) VALUES
(1, 'Basmati', 'basmati', 4, '1575977928rice.jpg', 1, NULL, NULL, 1, NULL, NULL),
(2, 'Sesame', 'sesame', 5, '1575978217SunflowerSeeds.jpg', 1, NULL, NULL, 1, NULL, NULL),
(3, 'Soy Beans', 'soy-beans', 7, '1575978258soybean.jpg', 1, NULL, NULL, 1, NULL, NULL),
(4, 'Wheat', 'wheat', 14, '1575978370Wheat.jpg', 1, NULL, NULL, 1, NULL, NULL),
(5, 'Mangoes', 'mangoes', 16, '1575978409Mangoes.jpg', 1, NULL, NULL, 1, NULL, NULL),
(6, 'Dates', 'dates', 51, '1575978690Dates.jpg', 1, NULL, NULL, 1, NULL, NULL),
(7, 'Cashew', 'cashew', 52, '1575978904r-s-cashew.jpg', 1, NULL, NULL, 1, NULL, NULL),
(8, 'Palm Oil ', 'palm-oil-', 60, '1576046251PalmOil.jpg', 1, NULL, NULL, 1, NULL, NULL),
(9, 'Chinese orgnic avocado for vegetable cooking oil ', 'chinese-orgnic-avocado-for-vegetable-cooking-oil-', 63, '1575979614Chinese-HACCP-factory-supplier-food-grade-edible.jpg', 1, NULL, NULL, 1, NULL, NULL),
(10, 'Fresh Cat Fish', 'fresh-cat-fish', 77, '1575979671Catfish.jpg', 1, NULL, NULL, 1, NULL, NULL),
(11, 'Fresh Beef', 'fresh-beef', 79, '1575979781Dry-Salted-Beef-Omasum-and-Buffalo-Offals.jpg', 1, NULL, NULL, 1, NULL, NULL),
(12, 'Pigs', 'pigs', 93, '1575982941pig.jpg', 1, NULL, NULL, 1, NULL, NULL),
(13, 'Parrot', 'parrot', 96, '1575983208Parrots.jpg', 1, NULL, NULL, 1, NULL, NULL),
(14, 'Rabbit', 'rabbit', 95, '1575983239Rabits.jpg', 1, NULL, NULL, 1, NULL, NULL),
(15, 'Citrus', 'citrus', 15, '1575985669Citrus.jpg', 1, NULL, NULL, 1, NULL, NULL),
(16, 'Guinea-corn (Sorghum)', 'guinea-corn-(sorghum)', 1, '1576044497Sorghum.jpg', 1, NULL, NULL, 1, NULL, NULL),
(17, 'Maize (Corn)', 'maize-(corn)', 2, '1576044544Maize.jpg', 1, NULL, NULL, 1, NULL, NULL),
(18, 'Beans', 'beans', 3, '1576044566Beans.jpg', 1, NULL, NULL, 1, NULL, NULL),
(19, 'Rice', 'rice', 4, '1576044588Rice.jpg', 1, NULL, NULL, 1, NULL, NULL),
(20, 'Melons', 'melons', 6, '1576044649Melons.JPG', 1, NULL, NULL, 1, NULL, NULL),
(21, 'Pigeon Peas', 'pigeon-peas', 97, '1576044697PigeonPeas.jpg', 1, NULL, NULL, 1, NULL, NULL),
(22, 'Groundnut', 'groundnut', 8, '1576044721Groundnut.jpg', 1, NULL, NULL, 1, NULL, NULL),
(23, 'Cowpea', 'cowpea', 9, '1576044754Cowpea.jpeg', 1, NULL, NULL, 1, NULL, NULL),
(24, 'Bambara Groundnut', 'bambara-groundnut', 10, '1576044775Groundnut1.jpg', 1, NULL, NULL, 1, NULL, NULL),
(25, 'Breadfruit (Ukwa)', 'breadfruit-(ukwa)', 11, '1576044801Ukwa.jpg', 1, NULL, NULL, 1, NULL, NULL),
(26, 'Fonio (Acha)', 'fonio-(acha)', 12, '1576044823Fonio.jpg', 1, NULL, NULL, 1, NULL, NULL),
(27, 'Millet', 'millet', 13, '1576044869millet06.jpg', 1, NULL, NULL, 1, NULL, NULL),
(28, 'Tomatoes', 'tomatoes', 17, '1576044931Tomatoes.jpg', 1, NULL, NULL, 1, NULL, NULL),
(29, 'Carrots', 'carrots', 18, '1576044967Carrots.jpg', 1, NULL, NULL, 1, NULL, NULL),
(30, 'Cucumbers', 'cucumbers', 19, '1576044991Cucumbers.jpg', 1, NULL, NULL, 1, NULL, NULL),
(31, 'Guavas', 'guavas', 20, '1576045022Guavas.jpg', 1, NULL, NULL, 1, NULL, NULL),
(32, 'Sherries', 'sherries', 21, '1576045048Sherries.jpg', 1, NULL, NULL, 1, NULL, NULL),
(33, 'Bannanas', 'bannanas', 22, '1576045070Bannanas.jpg', 1, NULL, NULL, 1, NULL, NULL),
(34, 'Plantains', 'plantains', 23, '1576045097Plantains.png', 1, NULL, NULL, 1, NULL, NULL),
(35, 'Water melons', 'water-melons', 24, '1576045174watermelon-slices-to-represent-allergy.jpg', 1, NULL, NULL, 1, NULL, NULL),
(36, 'Pears', 'pears', 25, '1576045197Pears.jpg', 1, NULL, NULL, 1, NULL, NULL),
(37, 'Coconuts', 'coconuts', 26, '1576045222Coconuts.jpg', 1, NULL, NULL, 1, NULL, NULL),
(38, 'Palm Fruits', 'palm-fruits', 27, '1576045252PalmFruits.jpg', 1, NULL, NULL, 1, NULL, NULL),
(39, 'Carbage', 'carbage', 28, '1576045311Carbage.jpg', 1, NULL, NULL, 1, NULL, NULL),
(40, 'Letus', 'letus', 29, '1576045334Letus.jpg', 1, NULL, NULL, 1, NULL, NULL),
(41, 'Amarantus', 'amarantus', 30, '1576045357Amarantus.jpg', 1, NULL, NULL, 1, NULL, NULL),
(42, 'Bitter Leafs', 'bitter-leafs', 31, '1576045384BitterLeafs.jpg', 1, NULL, NULL, 1, NULL, NULL),
(43, 'Water Leafs', 'water-leafs', 32, '1576045408WaterLeafs.jpg', 1, NULL, NULL, 1, NULL, NULL),
(44, 'Ugwu Leafs (Fluted Pumpkin)', 'ugwu-leafs-(fluted-pumpkin)', 33, '1576045464Ugwu-leaves.jpg', 1, NULL, NULL, 1, NULL, NULL),
(45, 'Spinash', 'spinash', 34, '1576045486Spinash.png', 1, NULL, NULL, 1, NULL, NULL),
(46, 'CocoYam (Taro) Leafs ', 'cocoyam-(taro)-leafs-', 35, '1576045510Taro.jpg', 1, NULL, NULL, 1, NULL, NULL),
(47, 'Yams', 'yams', 36, '1576045535Yams.jpg', 1, NULL, NULL, 1, NULL, NULL),
(48, 'Coco Yams ( Taro )', 'coco-yams-(-taro-)', 37, '1576045556Taro.jpg', 1, NULL, NULL, 1, NULL, NULL),
(49, 'Cassava (Seeds/Fresh/Dried/Chips)', 'cassava-(seeds/fresh/dried/chips)', 38, '1576045581Cassava.jpg', 1, NULL, NULL, 1, NULL, NULL),
(50, 'Sweet Potatoes', 'sweet-potatoes', 39, '1576045602SweetPotatoes.jpg', 1, NULL, NULL, 1, NULL, NULL),
(51, 'Irish Potatoes', 'irish-potatoes', 40, '1576045627IrishPotatoes.jpg', 1, NULL, NULL, 1, NULL, NULL),
(52, 'Bitter Yam', 'bitter-yam', 41, '1576045696Bitteryam.jpg', 1, NULL, NULL, 1, NULL, NULL),
(53, 'CoCoa Beans', 'cocoa-beans', 42, '1576045729CoCoaBeans.jpg', 1, NULL, NULL, 1, NULL, NULL),
(54, 'Moringa Seeds', 'moringa-seeds', 43, '1576045758MoringaSeeds.jpg', 1, NULL, NULL, 1, NULL, NULL),
(55, 'Sunflower Seeds', 'sunflower-seeds', 44, '1576045788SunflowerSeeds.jpg', 1, NULL, NULL, 1, NULL, NULL),
(56, 'Avocado Seeds', 'avocado-seeds', 45, '1576045811AvocadoSeeds.jpg', 1, NULL, NULL, 1, NULL, NULL),
(57, 'Nim Seeds', 'nim-seeds', 46, '1576045857neem-beej-500x500.jpg', 1, NULL, NULL, 1, NULL, NULL),
(58, 'Shea Nuts', 'shea-nuts', 47, '1576045880SheaNuts.jpg', 1, NULL, NULL, 1, NULL, NULL),
(59, 'Palm Kernels', 'palm-kernels', 48, '1576045900PalmKernels.jpg', 1, NULL, NULL, 1, NULL, NULL),
(60, 'Tiger Nuts', 'tiger-nuts', 50, '1576045976TigerNuts.jpg', 1, NULL, NULL, 1, NULL, NULL),
(61, 'Kashew Nuts', 'kashew-nuts', 52, '1576046015CashewNuts.jpg', 1, NULL, NULL, 1, NULL, NULL),
(62, 'Kola Nuts', 'kola-nuts', 53, '1576046095Kola-Nut-800x416.jpg', 1, NULL, NULL, 1, NULL, NULL),
(63, 'Bitter Kola Nuts', 'bitter-kola-nuts', 54, '1576046117Kola-Nut-800x416.jpg', 1, NULL, NULL, 1, NULL, NULL),
(64, 'Coconut Oil', 'coconut-oil', 55, '1576046140CoconutOil.jpg', 1, NULL, NULL, 1, NULL, NULL),
(65, 'Seseame Seed Oil', 'seseame-seed-oil', 56, '1576046165Sesame-Seed-Oil.jpg', 1, NULL, NULL, 1, NULL, NULL),
(66, 'Ginger Oil', 'ginger-oil', 57, '1576046187GingerOil.jpg', 1, NULL, NULL, 1, NULL, NULL),
(67, 'Groundnut Oil', 'groundnut-oil', 58, '1576046209GroundnutOil.jpg', 1, NULL, NULL, 1, NULL, NULL),
(68, 'Shea Butter', 'shea-butter', 59, '1576046232SheaButter.jpeg', 1, NULL, NULL, 1, NULL, NULL),
(69, 'Garlic Oil', 'garlic-oil', 61, '1576046280GarlicOil.jpg', 1, NULL, NULL, 1, NULL, NULL),
(70, 'Nim Oil', 'nim-oil', 62, '1576046299NimOil.jpg', 1, NULL, NULL, 1, NULL, NULL),
(71, 'Avocado Oil', 'avocado-oil', 63, '1576046320AvocadoOil.jpg', 1, NULL, NULL, 1, NULL, NULL),
(72, 'Moringa Oil', 'moringa-oil', 64, '1576046385MoringaOil.jpg', 1, NULL, NULL, 1, NULL, NULL),
(73, 'Soya Oil', 'soya-oil', 65, '1576046406SoyaOil.jpg', 1, NULL, NULL, 1, NULL, NULL),
(74, 'Sun Flower Oil', 'sun-flower-oil', 66, '1576046427SunFloweOil.jpg', 1, NULL, NULL, 1, NULL, NULL),
(75, 'Garlic', 'garlic', 68, '1576046452Garlic.jpg', 1, NULL, NULL, 1, NULL, NULL),
(76, 'Ginger', 'ginger', 68, '1576046479Ginger.jpg', 1, NULL, NULL, 1, NULL, NULL),
(77, 'Tumeric', 'tumeric', 69, '1576046501Tumeric.jpg', 1, NULL, NULL, 1, NULL, NULL),
(78, 'Chilli Pepper', 'chilli-pepper', 70, '1576046524ChilliPepper.jpg', 1, NULL, NULL, 1, NULL, NULL),
(79, 'Other Peppers', 'other-peppers', 98, '1576046601otherHerbs.jpg', 1, NULL, NULL, 1, NULL, NULL),
(80, 'Yello Pepper', 'yello-pepper', 71, '1576046629YelloPepper.jpg', 1, NULL, NULL, 1, NULL, NULL),
(81, 'Mushrooms', 'mushrooms', 72, '1576046658Mushrooms.jpg', 1, NULL, NULL, 1, NULL, NULL),
(82, 'Curry', 'curry', 99, '1576046680Curry.jpg', 1, NULL, NULL, 1, NULL, NULL),
(83, 'Thyme', 'thyme', 73, '1576046699Thyme.jpg', 1, NULL, NULL, 1, NULL, NULL),
(84, 'Hibiscus (Red Sore)', 'hibiscus-(red-sore)', 74, '1576046719Hibiscus.jpg', 1, NULL, NULL, 1, NULL, NULL),
(85, 'Chicken Eggs', 'chicken-eggs', 75, '1576046740ChickenEggs.jpg', 1, NULL, NULL, 1, NULL, NULL),
(86, 'Quail Eggs', 'quail-eggs', 76, '1576046763QuailEggs.jpg', 1, NULL, NULL, 1, NULL, NULL),
(87, 'Fresh Cat Fish (Pond)', 'fresh-cat-fish-(pond)', 77, '1576046789Catfish.jpg', 1, NULL, NULL, 1, NULL, NULL),
(88, 'Fresh Cat fish (Wild)', 'fresh-cat-fish-(wild)', 100, '1576046811Catfish.jpg', 1, NULL, NULL, 1, NULL, NULL),
(89, 'Dried Cat Fish (Pond)', 'dried-cat-fish-(pond)', 101, '1576046832dried_cat_fish.jpg', 1, NULL, NULL, 1, NULL, NULL),
(90, 'Dried Cat Fish (Wild)', 'dried-cat-fish-(wild)', 102, '1576046850dried_cat_fish.jpg', 1, NULL, NULL, 1, NULL, NULL),
(91, 'Fresh Tilapia Fish (Pond)', 'fresh-tilapia-fish-(pond)', 78, '1576046922Tilapia-Fish-Farming.jpg', 1, NULL, NULL, 1, NULL, NULL),
(92, 'Fresh Tilapia Fish (Wild)', 'fresh-tilapia-fish-(wild)', 103, '1576046945Tilapia-Fish-Farming.jpg', 1, NULL, NULL, 1, NULL, NULL),
(93, 'Dried Tilapia Fish (Pond)', 'dried-tilapia-fish-(pond)', 104, '1576046969DRIED-TILAPIA.jpg', 1, NULL, NULL, 1, NULL, NULL),
(94, 'Dried Tilapia Fish (Wild)', 'dried-tilapia-fish-(wild)', 105, '1576047168DRIED-TILAPIA.jpg', 1, NULL, NULL, 1, NULL, NULL),
(95, 'Fresh (Pond) Fishes (Other Varieties) ', 'fresh-(pond)-fishes-(other-varieties)-', 106, '1576047255Fresh(Pond)Fishes(Other Varieties).jpg', 1, NULL, NULL, 1, NULL, NULL),
(96, 'Dried (Pond) Fishes (Other Varieties)', 'dried-(pond)-fishes-(other-varieties)', 107, '1576047345Oreochromis-niloticus-Nairobi.jpg', 1, NULL, NULL, 1, NULL, NULL),
(97, 'Fresh (Wild) Fishes (Other Varieties)', 'fresh-(wild)-fishes-(other-varieties)', 108, '1576047374Oreochromis-niloticus-Nairobi.jpg', 1, NULL, NULL, 1, NULL, NULL),
(98, 'Dried (Wild) Fishes (Other Varieties)', 'dried-(wild)-fishes-(other-varieties)', 109, '1576047408dried_cat_fish.jpg', 1, NULL, NULL, 1, NULL, NULL),
(99, 'Dried Beef', 'dried-beef', 81, '1576047562Dry-Salted-Beef-Omasum-and-Buffalo-Offals.jpg', 1, NULL, NULL, 1, NULL, NULL),
(100, 'Fresh Skins (All Varieties)', 'fresh-skins-(all-varieties)', 110, '1576047587skin.jpg', 1, NULL, NULL, 1, NULL, NULL),
(101, 'Dried Skins (All Varieties)', 'dried-skins-(all-varieties)', 111, '1576047612skin.jpg', 1, NULL, NULL, 1, NULL, NULL),
(102, 'Fresh Goat Meat', 'fresh-goat-meat', 80, '1576047639FROZEN-SHEEP-MEAT-GOAT-MEAT-LAMB-MEAT.png', 1, NULL, NULL, 1, NULL, NULL),
(103, 'Dried Goat Meat', 'dried-goat-meat', 112, '1576047686FROZEN-SHEEP-MEAT-GOAT-MEAT-LAMB-MEAT.png', 1, NULL, NULL, 1, NULL, NULL),
(104, 'Beef Jerky (Kilishi)', 'beef-jerky-(kilishi)', 113, '1576047711beef_jerky.jpg', 1, NULL, NULL, 1, NULL, NULL),
(105, 'Grinded Meat (Beef)', 'grinded-meat-(beef)', 114, '1576047742ground-beef-800x450.jpg', 1, NULL, NULL, 1, NULL, NULL),
(106, 'Grinded Meat (Chicken)', 'grinded-meat-(chicken)', 115, '1576047792ground-chicken.jpg', 1, NULL, NULL, 1, NULL, NULL),
(107, 'Shrimps', 'shrimps', 82, '1576047876Shrimps.jpg', 1, NULL, NULL, 1, NULL, NULL),
(108, 'Crayfish', 'crayfish', 83, '1576047894CrayFish.jpg', 1, NULL, NULL, 1, NULL, NULL),
(109, 'Crabs', 'crabs', 84, '1576047913Crabs.jpg', 1, NULL, NULL, 1, NULL, NULL),
(110, 'Lobsters', 'lobsters', 85, '1576047939Lobsters.jpg', 1, NULL, NULL, 1, NULL, NULL),
(111, 'Prawns', 'prawns', 86, '1576047958Prawns.jpg', 1, NULL, NULL, 1, NULL, NULL),
(112, 'Periwinkles', 'periwinkles', 87, '1576047978Periwinkles.jpg', 1, NULL, NULL, 1, NULL, NULL),
(113, 'Snails', 'snails', 117, '1576048003snails.jpg', 1, NULL, NULL, 1, NULL, NULL),
(114, 'Oysters', 'oysters', 116, '1576048024Oysters.jpg', 1, NULL, NULL, 1, NULL, NULL),
(115, 'Fresh Milk', 'fresh-milk', 88, '1576048044Milk.jpg', 1, NULL, NULL, 1, NULL, NULL),
(116, 'Diary Fat', 'diary-fat', 118, '1576048062Milk-1.jpg', 1, NULL, NULL, 1, NULL, NULL),
(117, 'Curd', 'curd', 89, '1576048079Curd.jpg', 1, NULL, NULL, 1, NULL, NULL),
(118, 'Cheese', 'cheese', 90, '1576048102Cheese.jpeg', 1, NULL, NULL, 1, NULL, NULL),
(119, 'Pure Honey', 'pure-honey', 91, '1576048125Honey.jpeg', 1, NULL, NULL, 1, NULL, NULL),
(120, 'Gall stones', 'gall-stones', 92, '1576048144GallStones.jpg', 1, NULL, NULL, 1, NULL, NULL),
(121, 'Meat Cattle', 'meat-cattle', 119, '1576048192meat_cattle.jpg', 1, NULL, NULL, 1, NULL, NULL),
(122, 'Diary Cattle', 'diary-cattle', 120, '1576048218meat_cattle.jpg', 1, NULL, NULL, 1, NULL, NULL),
(123, 'Goats', 'goats', 121, '1576048241goats.jpg', 1, NULL, NULL, 1, NULL, NULL),
(124, 'Donkeys', 'donkeys', 122, '1576048271Donkeys.jpg', 1, NULL, NULL, 1, NULL, NULL),
(125, 'Rams & Sheeps', 'rams-&-sheeps', 123, '1576048305Rams_Sheeps.jpg', 1, NULL, NULL, 1, NULL, NULL),
(126, 'Camels', 'camels', 124, '1576048325Camels.jpg', 1, NULL, NULL, 1, NULL, NULL),
(127, 'Live Snails', 'live-snails', 125, '1576048344snails.jpg', 1, NULL, NULL, 1, NULL, NULL),
(128, 'Day-old Chicks (All Kinds)', 'day-old-chicks-(all-kinds)', 126, '1576048368chicks.jpeg', 1, NULL, NULL, 1, NULL, NULL),
(129, 'Grasscutters (Cane Rats)', 'grasscutters-(cane-rats)', 127, '1576048391grasscut.jpg', 1, NULL, NULL, 1, NULL, NULL),
(130, 'Rabits', 'rabits', 95, '1576048412Rabits.jpg', 1, NULL, NULL, 1, NULL, NULL),
(131, 'Quails', 'quails', 128, '1576048430Quails.jpg', 1, NULL, NULL, 1, NULL, NULL),
(132, 'Ducks & Geese', 'ducks-&-geese', 129, '1576048455Ducks_Geese.jpg', 1, NULL, NULL, 1, NULL, NULL),
(133, 'Dogs (For meat, Guard & Pet)', 'dogs-(for-meat-guard-&-pet)', 94, '1576048477Dogs.jpg', 1, NULL, NULL, 1, NULL, NULL),
(134, 'Fishes (All kinds)', 'fishes-(all-kinds)', 131, '1576048510fishes.jpg', 1, NULL, NULL, 1, NULL, NULL),
(135, 'Parrots & other Luxerious Birds', 'parrots-&-other-luxerious-birds', 96, '1576048535Parrots.jpg', 1, NULL, NULL, 1, NULL, NULL),
(136, 'Other Animals', 'other-animals', 132, '1576048664oxe.jpg', 1, NULL, NULL, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `t_uom`
--

CREATE TABLE `t_uom` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name_en` varchar(255) DEFAULT NULL,
  `name_fr` varchar(225) NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:-Item,2:-service',
  `active` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_uom`
--

INSERT INTO `t_uom` (`id`, `name_en`, `name_fr`, `type`, `active`) VALUES
(1, 'kg', 'kg', 1, 1),
(2, 'Metric Tonne (MT)', 'tonne métrique (MT)', 1, 1),
(3, 'Unit', 'unité', 1, 1),
(4, 'Litres', 'litres', 1, 1),
(5, 'Crate', 'caisse', 1, 1),
(6, 'Basket', 'Pour le basket-ball', 1, 1),
(7, 'Set', 'ensemble', 1, 1),
(8, 'bags', 'sacs', 2, 1),
(9, 'Tonne', 'Tonne', 2, 1),
(10, 'Pack', 'paquet', 2, 1),
(11, 'Dozen', 'douzaine', 2, 1),
(12, 'Doze', 'Doze', 2, 1),
(13, 'Depending on Specific requirements', 'Selon les exigences spécifiques', 2, 1),
(14, 'Unit', 'unité', 2, 1),
(15, 'Hour', 'heure', 2, 1),
(16, 'Day', 'jour', 2, 1),
(17, 'By Contract', 'Par contrat', 2, 1),
(18, 'piece', 'piece', 1, 1),
(19, 'goal', 'goal', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_user`
--

CREATE TABLE `t_user` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `token_id` varchar(255) DEFAULT NULL,
  `is_admin` tinyint(1) NOT NULL DEFAULT '0',
  `actve` tinyint(1) NOT NULL DEFAULT '0',
  `email_otp` varchar(255) DEFAULT NULL,
  `phone_otp` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_user`
--

INSERT INTO `t_user` (`id`, `user_name`, `password`, `email`, `mobile`, `Name`, `token_id`, `is_admin`, `actve`, `email_otp`, `phone_otp`) VALUES
(1, 'admin', '9909f3c324d7ff09bf7102f8445e4162', 'Admin@clematistech.com', '7003960614', 'Super Admin', '1ff5ff6599f0bbbed6df37a27bb45fda', 1, 1, NULL, NULL),
(5, 'admin_temp', '9909f3c324d7ff09bf7102f8445e4162', 'test.clematis@gmail.com', '9674391164', 'Howard Morgan', '5af7f9951e0228df6348960a7f80d104', 1, 1, '', NULL),
(6, 'manager', 'abff75a1a97b554db255b7bcca8a7c6d', 'manager@clematistech.com', '9887654321', 'Chris Fralic', NULL, 0, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `t_variance`
--

CREATE TABLE `t_variance` (
  `id` int(11) NOT NULL,
  `name_en` varchar(100) NOT NULL,
  `name_fr` varchar(255) NOT NULL,
  `type` int(1) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_variance`
--

INSERT INTO `t_variance` (`id`, `name_en`, `name_fr`, `type`, `status`) VALUES
(1, 'Color', 'Couleur', 1, 1),
(2, 'Size', 'Taille', 1, 1),
(3, 'Variety', 'Variety', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_weather`
--

CREATE TABLE `t_weather` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `location_key` varchar(255) DEFAULT NULL,
  `daily_status` text,
  `current_status` text,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_weather`
--

INSERT INTO `t_weather` (`id`, `customer_id`, `location_key`, `daily_status`, `current_status`, `date`) VALUES
(1, 0, '254085', '{"Headline":{"EffectiveDate":"2019-12-10T07:00:00+01:00","EffectiveEpochDate":1575957600,"Severity":7,"Text":"Not as hot Tuesday","Category":"cooler","EndDate":"2019-12-10T19:00:00+01:00","EndEpochDate":1576000800,"MobileLink":"http://m.accuweather.com/en/ng/abuja/254085/extended-weather-forecast/254085?unit=c&lang=en-us","Link":"http://www.accuweather.com/en/ng/abuja/254085/daily-weather-forecast/254085?unit=c&lang=en-us"},"DailyForecasts":[{"Date":"2019-12-10T07:00:00+01:00","EpochDate":1575957600,"Sun":{"Rise":"2019-12-10T06:34:00+01:00","EpochRise":1575956040,"Set":"2019-12-10T18:11:00+01:00","EpochSet":1575997860},"Moon":{"Rise":"2019-12-10T16:55:00+01:00","EpochRise":1575993300,"Set":"2019-12-11T05:44:00+01:00","EpochSet":1576039440,"Phase":"WaxingGibbous","Age":14},"Temperature":{"Minimum":{"Value":17.3,"Unit":"C","UnitType":17},"Maximum":{"Value":29.6,"Unit":"C","UnitType":17}},"RealFeelTemperature":{"Minimum":{"Value":17.8,"Unit":"C","UnitType":17},"Maximum":{"Value":31.7,"Unit":"C","UnitType":17}},"RealFeelTemperatureShade":{"Minimum":{"Value":17.8,"Unit":"C","UnitType":17},"Maximum":{"Value":28.4,"Unit":"C","UnitType":17}},"HoursOfSun":11.6,"DegreeDaySummary":{"Heating":{"Value":0.0,"Unit":"C","UnitType":17},"Cooling":{"Value":5.0,"Unit":"C","UnitType":17}},"AirAndPollen":[{"Name":"AirQuality","Value":0,"Category":"Good","CategoryValue":1,"Type":"Ozone"},{"Name":"Grass","Value":0,"Category":"Low","CategoryValue":1},{"Name":"Mold","Value":0,"Category":"Low","CategoryValue":1},{"Name":"Ragweed","Value":0,"Category":"Low","CategoryValue":1},{"Name":"Tree","Value":0,"Category":"Low","CategoryValue":1},{"Name":"UVIndex","Value":7,"Category":"High","CategoryValue":3}],"Day":{"Icon":1,"IconPhrase":"Sunny","HasPrecipitation":false,"ShortPhrase":"Sunny and not as hot","LongPhrase":"Not as hot but pleasant with plenty of sun","PrecipitationProbability":0,"ThunderstormProbability":0,"RainProbability":0,"SnowProbability":0,"IceProbability":0,"Wind":{"Speed":{"Value":7.4,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":144,"Localized":"SE","English":"SE"}},"WindGust":{"Speed":{"Value":13.0,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":164,"Localized":"SSE","English":"SSE"}},"TotalLiquid":{"Value":0.0,"Unit":"mm","UnitType":3},"Rain":{"Value":0.0,"Unit":"mm","UnitType":3},"Snow":{"Value":0.0,"Unit":"cm","UnitType":4},"Ice":{"Value":0.0,"Unit":"mm","UnitType":3},"HoursOfPrecipitation":0.0,"HoursOfRain":0.0,"HoursOfSnow":0.0,"HoursOfIce":0.0,"CloudCover":0},"Night":{"Icon":33,"IconPhrase":"Clear","HasPrecipitation":false,"ShortPhrase":"Clear","LongPhrase":"Clear","PrecipitationProbability":0,"ThunderstormProbability":0,"RainProbability":0,"SnowProbability":0,"IceProbability":0,"Wind":{"Speed":{"Value":5.6,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":252,"Localized":"WSW","English":"WSW"}},"WindGust":{"Speed":{"Value":9.3,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":143,"Localized":"SE","English":"SE"}},"TotalLiquid":{"Value":0.0,"Unit":"mm","UnitType":3},"Rain":{"Value":0.0,"Unit":"mm","UnitType":3},"Snow":{"Value":0.0,"Unit":"cm","UnitType":4},"Ice":{"Value":0.0,"Unit":"mm","UnitType":3},"HoursOfPrecipitation":0.0,"HoursOfRain":0.0,"HoursOfSnow":0.0,"HoursOfIce":0.0,"CloudCover":1},"Sources":["AccuWeather"],"MobileLink":"http://m.accuweather.com/en/ng/abuja/254085/daily-weather-forecast/254085?day=1&unit=c&lang=en-us","Link":"http://www.accuweather.com/en/ng/abuja/254085/daily-weather-forecast/254085?day=1&unit=c&lang=en-us"}]}', '[{"LocalObservationDateTime":"2019-12-10T13:12:00+01:00","EpochTime":1575979920,"WeatherText":"Cloudy","WeatherIcon":7,"HasPrecipitation":false,"PrecipitationType":null,"IsDayTime":true,"Temperature":{"Metric":{"Value":28.2,"Unit":"C","UnitType":17},"Imperial":{"Value":83.0,"Unit":"F","UnitType":18}},"MobileLink":"http://m.accuweather.com/en/ng/abuja/254085/current-weather/254085?lang=en-us","Link":"http://www.accuweather.com/en/ng/abuja/254085/current-weather/254085?lang=en-us"}]', '2019-12-10 12:23:41'),
(2, 0, '254085', '{"Headline":{"EffectiveDate":"2019-12-14T07:00:00+01:00","EffectiveEpochDate":1576303200,"Severity":4,"Text":"Pleasant Saturday","Category":"","EndDate":null,"EndEpochDate":null,"MobileLink":"http://m.accuweather.com/en/ng/abuja/254085/extended-weather-forecast/254085?unit=c&lang=en-us","Link":"http://www.accuweather.com/en/ng/abuja/254085/daily-weather-forecast/254085?unit=c&lang=en-us"},"DailyForecasts":[{"Date":"2019-12-11T07:00:00+01:00","EpochDate":1576044000,"Sun":{"Rise":"2019-12-11T06:35:00+01:00","EpochRise":1576042500,"Set":"2019-12-11T18:11:00+01:00","EpochSet":1576084260},"Moon":{"Rise":"2019-12-11T17:44:00+01:00","EpochRise":1576082640,"Set":"2019-12-12T06:38:00+01:00","EpochSet":1576129080,"Phase":"WaxingGibbous","Age":15},"Temperature":{"Minimum":{"Value":18.9,"Unit":"C","UnitType":17},"Maximum":{"Value":30.0,"Unit":"C","UnitType":17}},"RealFeelTemperature":{"Minimum":{"Value":17.8,"Unit":"C","UnitType":17},"Maximum":{"Value":32.5,"Unit":"C","UnitType":17}},"RealFeelTemperatureShade":{"Minimum":{"Value":17.8,"Unit":"C","UnitType":17},"Maximum":{"Value":28.8,"Unit":"C","UnitType":17}},"HoursOfSun":10.8,"DegreeDaySummary":{"Heating":{"Value":0.0,"Unit":"C","UnitType":17},"Cooling":{"Value":6.0,"Unit":"C","UnitType":17}},"AirAndPollen":[{"Name":"AirQuality","Value":0,"Category":"Good","CategoryValue":1,"Type":"Ozone"},{"Name":"Grass","Value":0,"Category":"Low","CategoryValue":1},{"Name":"Mold","Value":0,"Category":"Low","CategoryValue":1},{"Name":"Ragweed","Value":0,"Category":"Low","CategoryValue":1},{"Name":"Tree","Value":0,"Category":"Low","CategoryValue":1},{"Name":"UVIndex","Value":7,"Category":"High","CategoryValue":3}],"Day":{"Icon":1,"IconPhrase":"Sunny","HasPrecipitation":false,"ShortPhrase":"Sunshine and not as hot","LongPhrase":"Sunny and not as hot but pleasant","PrecipitationProbability":1,"ThunderstormProbability":0,"RainProbability":1,"SnowProbability":0,"IceProbability":0,"Wind":{"Speed":{"Value":7.4,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":123,"Localized":"ESE","English":"ESE"}},"WindGust":{"Speed":{"Value":11.1,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":122,"Localized":"ESE","English":"ESE"}},"TotalLiquid":{"Value":0.0,"Unit":"mm","UnitType":3},"Rain":{"Value":0.0,"Unit":"mm","UnitType":3},"Snow":{"Value":0.0,"Unit":"cm","UnitType":4},"Ice":{"Value":0.0,"Unit":"mm","UnitType":3},"HoursOfPrecipitation":0.0,"HoursOfRain":0.0,"HoursOfSnow":0.0,"HoursOfIce":0.0,"CloudCover":9},"Night":{"Icon":35,"IconPhrase":"Partly cloudy","HasPrecipitation":false,"ShortPhrase":"Patchy clouds","LongPhrase":"Patchy clouds","PrecipitationProbability":2,"ThunderstormProbability":0,"RainProbability":2,"SnowProbability":0,"IceProbability":0,"Wind":{"Speed":{"Value":5.6,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":69,"Localized":"ENE","English":"ENE"}},"WindGust":{"Speed":{"Value":9.3,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":125,"Localized":"SE","English":"SE"}},"TotalLiquid":{"Value":0.0,"Unit":"mm","UnitType":3},"Rain":{"Value":0.0,"Unit":"mm","UnitType":3},"Snow":{"Value":0.0,"Unit":"cm","UnitType":4},"Ice":{"Value":0.0,"Unit":"mm","UnitType":3},"HoursOfPrecipitation":0.0,"HoursOfRain":0.0,"HoursOfSnow":0.0,"HoursOfIce":0.0,"CloudCover":34},"Sources":["AccuWeather"],"MobileLink":"http://m.accuweather.com/en/ng/abuja/254085/daily-weather-forecast/254085?day=1&unit=c&lang=en-us","Link":"http://www.accuweather.com/en/ng/abuja/254085/daily-weather-forecast/254085?day=1&unit=c&lang=en-us"}]}', '[{"LocalObservationDateTime":"2019-12-11T08:11:00+01:00","EpochTime":1576048260,"WeatherText":"Cloudy","WeatherIcon":7,"HasPrecipitation":false,"PrecipitationType":null,"IsDayTime":true,"Temperature":{"Metric":{"Value":20.0,"Unit":"C","UnitType":17},"Imperial":{"Value":68.0,"Unit":"F","UnitType":18}},"MobileLink":"http://m.accuweather.com/en/ng/abuja/254085/current-weather/254085?lang=en-us","Link":"http://www.accuweather.com/en/ng/abuja/254085/current-weather/254085?lang=en-us"}]', '2019-12-11 07:19:28'),
(3, 0, '932298', '{"Headline":{"EffectiveDate":"2019-12-14T07:00:00+01:00","EffectiveEpochDate":1576303200,"Severity":4,"Text":"Pleasant Saturday","Category":"","EndDate":null,"EndEpochDate":null,"MobileLink":"http://m.accuweather.com/en/ng/karu/932298/extended-weather-forecast/932298?unit=c&lang=en-us","Link":"http://www.accuweather.com/en/ng/karu/932298/daily-weather-forecast/932298?unit=c&lang=en-us"},"DailyForecasts":[{"Date":"2019-12-11T07:00:00+01:00","EpochDate":1576044000,"Sun":{"Rise":"2019-12-11T06:34:00+01:00","EpochRise":1576042440,"Set":"2019-12-11T18:11:00+01:00","EpochSet":1576084260},"Moon":{"Rise":"2019-12-11T17:44:00+01:00","EpochRise":1576082640,"Set":"2019-12-12T06:38:00+01:00","EpochSet":1576129080,"Phase":"WaxingGibbous","Age":15},"Temperature":{"Minimum":{"Value":18.9,"Unit":"C","UnitType":17},"Maximum":{"Value":30.0,"Unit":"C","UnitType":17}},"RealFeelTemperature":{"Minimum":{"Value":17.8,"Unit":"C","UnitType":17},"Maximum":{"Value":32.5,"Unit":"C","UnitType":17}},"RealFeelTemperatureShade":{"Minimum":{"Value":17.8,"Unit":"C","UnitType":17},"Maximum":{"Value":28.8,"Unit":"C","UnitType":17}},"HoursOfSun":10.8,"DegreeDaySummary":{"Heating":{"Value":0.0,"Unit":"C","UnitType":17},"Cooling":{"Value":6.0,"Unit":"C","UnitType":17}},"AirAndPollen":[{"Name":"AirQuality","Value":0,"Category":"Good","CategoryValue":1,"Type":"Ozone"},{"Name":"Grass","Value":0,"Category":"Low","CategoryValue":1},{"Name":"Mold","Value":0,"Category":"Low","CategoryValue":1},{"Name":"Ragweed","Value":0,"Category":"Low","CategoryValue":1},{"Name":"Tree","Value":0,"Category":"Low","CategoryValue":1},{"Name":"UVIndex","Value":7,"Category":"High","CategoryValue":3}],"Day":{"Icon":1,"IconPhrase":"Sunny","HasPrecipitation":false,"ShortPhrase":"Sunshine and not as hot","LongPhrase":"Sunny and not as hot but pleasant","PrecipitationProbability":1,"ThunderstormProbability":0,"RainProbability":1,"SnowProbability":0,"IceProbability":0,"Wind":{"Speed":{"Value":7.4,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":123,"Localized":"ESE","English":"ESE"}},"WindGust":{"Speed":{"Value":11.1,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":122,"Localized":"ESE","English":"ESE"}},"TotalLiquid":{"Value":0.0,"Unit":"mm","UnitType":3},"Rain":{"Value":0.0,"Unit":"mm","UnitType":3},"Snow":{"Value":0.0,"Unit":"cm","UnitType":4},"Ice":{"Value":0.0,"Unit":"mm","UnitType":3},"HoursOfPrecipitation":0.0,"HoursOfRain":0.0,"HoursOfSnow":0.0,"HoursOfIce":0.0,"CloudCover":9},"Night":{"Icon":35,"IconPhrase":"Partly cloudy","HasPrecipitation":false,"ShortPhrase":"Patchy clouds","LongPhrase":"Patchy clouds","PrecipitationProbability":2,"ThunderstormProbability":0,"RainProbability":2,"SnowProbability":0,"IceProbability":0,"Wind":{"Speed":{"Value":5.6,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":69,"Localized":"ENE","English":"ENE"}},"WindGust":{"Speed":{"Value":9.3,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":125,"Localized":"SE","English":"SE"}},"TotalLiquid":{"Value":0.0,"Unit":"mm","UnitType":3},"Rain":{"Value":0.0,"Unit":"mm","UnitType":3},"Snow":{"Value":0.0,"Unit":"cm","UnitType":4},"Ice":{"Value":0.0,"Unit":"mm","UnitType":3},"HoursOfPrecipitation":0.0,"HoursOfRain":0.0,"HoursOfSnow":0.0,"HoursOfIce":0.0,"CloudCover":34},"Sources":["AccuWeather"],"MobileLink":"http://m.accuweather.com/en/ng/karu/932298/daily-weather-forecast/932298?day=1&unit=c&lang=en-us","Link":"http://www.accuweather.com/en/ng/karu/932298/daily-weather-forecast/932298?day=1&unit=c&lang=en-us"}]}', '[{"LocalObservationDateTime":"2019-12-11T10:56:00+01:00","EpochTime":1576058160,"WeatherText":"Cloudy","WeatherIcon":7,"HasPrecipitation":false,"PrecipitationType":null,"IsDayTime":true,"Temperature":{"Metric":{"Value":24.8,"Unit":"C","UnitType":17},"Imperial":{"Value":77.0,"Unit":"F","UnitType":18}},"MobileLink":"http://m.accuweather.com/en/ng/karu/932298/current-weather/932298?lang=en-us","Link":"http://www.accuweather.com/en/ng/karu/932298/current-weather/932298?lang=en-us"}]', '2019-12-11 10:04:10'),
(4, 0, '254085', '{"Headline":{"EffectiveDate":"2019-12-14T07:00:00+01:00","EffectiveEpochDate":1576303200,"Severity":4,"Text":"Pleasant this weekend","Category":"","EndDate":null,"EndEpochDate":null,"MobileLink":"http://m.accuweather.com/en/ng/abuja/254085/extended-weather-forecast/254085?unit=c&lang=en-us","Link":"http://www.accuweather.com/en/ng/abuja/254085/daily-weather-forecast/254085?unit=c&lang=en-us"},"DailyForecasts":[{"Date":"2019-12-12T07:00:00+01:00","EpochDate":1576130400,"Sun":{"Rise":"2019-12-12T06:35:00+01:00","EpochRise":1576128900,"Set":"2019-12-12T18:12:00+01:00","EpochSet":1576170720},"Moon":{"Rise":"2019-12-12T18:37:00+01:00","EpochRise":1576172220,"Set":"2019-12-13T07:36:00+01:00","EpochSet":1576218960,"Phase":"Full","Age":16},"Temperature":{"Minimum":{"Value":19.4,"Unit":"C","UnitType":17},"Maximum":{"Value":29.8,"Unit":"C","UnitType":17}},"RealFeelTemperature":{"Minimum":{"Value":18.8,"Unit":"C","UnitType":17},"Maximum":{"Value":31.7,"Unit":"C","UnitType":17}},"RealFeelTemperatureShade":{"Minimum":{"Value":18.8,"Unit":"C","UnitType":17},"Maximum":{"Value":28.6,"Unit":"C","UnitType":17}},"HoursOfSun":11.5,"DegreeDaySummary":{"Heating":{"Value":0.0,"Unit":"C","UnitType":17},"Cooling":{"Value":7.0,"Unit":"C","UnitType":17}},"AirAndPollen":[{"Name":"AirQuality","Value":0,"Category":"Good","CategoryValue":1,"Type":"Ozone"},{"Name":"Grass","Value":0,"Category":"Low","CategoryValue":1},{"Name":"Mold","Value":0,"Category":"Low","CategoryValue":1},{"Name":"Ragweed","Value":0,"Category":"Low","CategoryValue":1},{"Name":"Tree","Value":0,"Category":"Low","CategoryValue":1},{"Name":"UVIndex","Value":7,"Category":"High","CategoryValue":3}],"Day":{"Icon":1,"IconPhrase":"Sunny","HasPrecipitation":false,"ShortPhrase":"Pleasant with plenty of sun","LongPhrase":"Pleasant with plenty of sun","PrecipitationProbability":1,"ThunderstormProbability":0,"RainProbability":1,"SnowProbability":0,"IceProbability":0,"Wind":{"Speed":{"Value":7.4,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":127,"Localized":"SE","English":"SE"}},"WindGust":{"Speed":{"Value":13.0,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":139,"Localized":"SE","English":"SE"}},"TotalLiquid":{"Value":0.0,"Unit":"mm","UnitType":3},"Rain":{"Value":0.0,"Unit":"mm","UnitType":3},"Snow":{"Value":0.0,"Unit":"cm","UnitType":4},"Ice":{"Value":0.0,"Unit":"mm","UnitType":3},"HoursOfPrecipitation":0.0,"HoursOfRain":0.0,"HoursOfSnow":0.0,"HoursOfIce":0.0,"CloudCover":1},"Night":{"Icon":34,"IconPhrase":"Mostly clear","HasPrecipitation":false,"ShortPhrase":"Clear to partly cloudy","LongPhrase":"Clear to partly cloudy","PrecipitationProbability":0,"ThunderstormProbability":0,"RainProbability":0,"SnowProbability":0,"IceProbability":0,"Wind":{"Speed":{"Value":7.4,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":71,"Localized":"ENE","English":"ENE"}},"WindGust":{"Speed":{"Value":11.1,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":42,"Localized":"NE","English":"NE"}},"TotalLiquid":{"Value":0.0,"Unit":"mm","UnitType":3},"Rain":{"Value":0.0,"Unit":"mm","UnitType":3},"Snow":{"Value":0.0,"Unit":"cm","UnitType":4},"Ice":{"Value":0.0,"Unit":"mm","UnitType":3},"HoursOfPrecipitation":0.0,"HoursOfRain":0.0,"HoursOfSnow":0.0,"HoursOfIce":0.0,"CloudCover":14},"Sources":["AccuWeather"],"MobileLink":"http://m.accuweather.com/en/ng/abuja/254085/daily-weather-forecast/254085?day=1&unit=c&lang=en-us","Link":"http://www.accuweather.com/en/ng/abuja/254085/daily-weather-forecast/254085?day=1&unit=c&lang=en-us"}]}', '[{"LocalObservationDateTime":"2019-12-12T09:42:00+01:00","EpochTime":1576140120,"WeatherText":"Cloudy","WeatherIcon":7,"HasPrecipitation":false,"PrecipitationType":null,"IsDayTime":true,"Temperature":{"Metric":{"Value":23.3,"Unit":"C","UnitType":17},"Imperial":{"Value":74.0,"Unit":"F","UnitType":18}},"MobileLink":"http://m.accuweather.com/en/ng/abuja/254085/current-weather/254085?lang=en-us","Link":"http://www.accuweather.com/en/ng/abuja/254085/current-weather/254085?lang=en-us"}]', '2019-12-12 09:01:11'),
(5, 0, '932298', '{"Headline":{"EffectiveDate":"2019-12-14T07:00:00+01:00","EffectiveEpochDate":1576303200,"Severity":4,"Text":"Pleasant this weekend","Category":"","EndDate":null,"EndEpochDate":null,"MobileLink":"http://m.accuweather.com/en/ng/karu/932298/extended-weather-forecast/932298?unit=c&lang=en-us","Link":"http://www.accuweather.com/en/ng/karu/932298/daily-weather-forecast/932298?unit=c&lang=en-us"},"DailyForecasts":[{"Date":"2019-12-12T07:00:00+01:00","EpochDate":1576130400,"Sun":{"Rise":"2019-12-12T06:35:00+01:00","EpochRise":1576128900,"Set":"2019-12-12T18:12:00+01:00","EpochSet":1576170720},"Moon":{"Rise":"2019-12-12T18:37:00+01:00","EpochRise":1576172220,"Set":"2019-12-13T07:36:00+01:00","EpochSet":1576218960,"Phase":"Full","Age":16},"Temperature":{"Minimum":{"Value":19.4,"Unit":"C","UnitType":17},"Maximum":{"Value":29.8,"Unit":"C","UnitType":17}},"RealFeelTemperature":{"Minimum":{"Value":18.8,"Unit":"C","UnitType":17},"Maximum":{"Value":31.7,"Unit":"C","UnitType":17}},"RealFeelTemperatureShade":{"Minimum":{"Value":18.8,"Unit":"C","UnitType":17},"Maximum":{"Value":28.6,"Unit":"C","UnitType":17}},"HoursOfSun":11.5,"DegreeDaySummary":{"Heating":{"Value":0.0,"Unit":"C","UnitType":17},"Cooling":{"Value":7.0,"Unit":"C","UnitType":17}},"AirAndPollen":[{"Name":"AirQuality","Value":0,"Category":"Good","CategoryValue":1,"Type":"Ozone"},{"Name":"Grass","Value":0,"Category":"Low","CategoryValue":1},{"Name":"Mold","Value":0,"Category":"Low","CategoryValue":1},{"Name":"Ragweed","Value":0,"Category":"Low","CategoryValue":1},{"Name":"Tree","Value":0,"Category":"Low","CategoryValue":1},{"Name":"UVIndex","Value":7,"Category":"High","CategoryValue":3}],"Day":{"Icon":1,"IconPhrase":"Sunny","HasPrecipitation":false,"ShortPhrase":"Pleasant with plenty of sun","LongPhrase":"Pleasant with plenty of sun","PrecipitationProbability":1,"ThunderstormProbability":0,"RainProbability":1,"SnowProbability":0,"IceProbability":0,"Wind":{"Speed":{"Value":7.4,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":127,"Localized":"SE","English":"SE"}},"WindGust":{"Speed":{"Value":13.0,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":139,"Localized":"SE","English":"SE"}},"TotalLiquid":{"Value":0.0,"Unit":"mm","UnitType":3},"Rain":{"Value":0.0,"Unit":"mm","UnitType":3},"Snow":{"Value":0.0,"Unit":"cm","UnitType":4},"Ice":{"Value":0.0,"Unit":"mm","UnitType":3},"HoursOfPrecipitation":0.0,"HoursOfRain":0.0,"HoursOfSnow":0.0,"HoursOfIce":0.0,"CloudCover":1},"Night":{"Icon":34,"IconPhrase":"Mostly clear","HasPrecipitation":false,"ShortPhrase":"Clear to partly cloudy","LongPhrase":"Clear to partly cloudy","PrecipitationProbability":0,"ThunderstormProbability":0,"RainProbability":0,"SnowProbability":0,"IceProbability":0,"Wind":{"Speed":{"Value":7.4,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":71,"Localized":"ENE","English":"ENE"}},"WindGust":{"Speed":{"Value":11.1,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":42,"Localized":"NE","English":"NE"}},"TotalLiquid":{"Value":0.0,"Unit":"mm","UnitType":3},"Rain":{"Value":0.0,"Unit":"mm","UnitType":3},"Snow":{"Value":0.0,"Unit":"cm","UnitType":4},"Ice":{"Value":0.0,"Unit":"mm","UnitType":3},"HoursOfPrecipitation":0.0,"HoursOfRain":0.0,"HoursOfSnow":0.0,"HoursOfIce":0.0,"CloudCover":14},"Sources":["AccuWeather"],"MobileLink":"http://m.accuweather.com/en/ng/karu/932298/daily-weather-forecast/932298?day=1&unit=c&lang=en-us","Link":"http://www.accuweather.com/en/ng/karu/932298/daily-weather-forecast/932298?day=1&unit=c&lang=en-us"}]}', '[{"LocalObservationDateTime":"2019-12-12T10:21:00+01:00","EpochTime":1576142460,"WeatherText":"Cloudy","WeatherIcon":7,"HasPrecipitation":false,"PrecipitationType":null,"IsDayTime":true,"Temperature":{"Metric":{"Value":25.5,"Unit":"C","UnitType":17},"Imperial":{"Value":78.0,"Unit":"F","UnitType":18}},"MobileLink":"http://m.accuweather.com/en/ng/karu/932298/current-weather/932298?lang=en-us","Link":"http://www.accuweather.com/en/ng/karu/932298/current-weather/932298?lang=en-us"}]', '2019-12-12 09:35:04'),
(6, 0, '254085', '{"Headline":{"EffectiveDate":"2019-12-14T07:00:00+01:00","EffectiveEpochDate":1576303200,"Severity":4,"Text":"Pleasant this weekend","Category":"","EndDate":null,"EndEpochDate":null,"MobileLink":"http://m.accuweather.com/en/ng/abuja/254085/extended-weather-forecast/254085?unit=c&lang=en-us","Link":"http://www.accuweather.com/en/ng/abuja/254085/daily-weather-forecast/254085?unit=c&lang=en-us"},"DailyForecasts":[{"Date":"2019-12-13T07:00:00+01:00","EpochDate":1576216800,"Sun":{"Rise":"2019-12-13T06:36:00+01:00","EpochRise":1576215360,"Set":"2019-12-13T18:12:00+01:00","EpochSet":1576257120},"Moon":{"Rise":"2019-12-13T19:34:00+01:00","EpochRise":1576262040,"Set":"2019-12-14T08:34:00+01:00","EpochSet":1576308840,"Phase":"WaningGibbous","Age":17},"Temperature":{"Minimum":{"Value":18.2,"Unit":"C","UnitType":17},"Maximum":{"Value":30.0,"Unit":"C","UnitType":17}},"RealFeelTemperature":{"Minimum":{"Value":17.9,"Unit":"C","UnitType":17},"Maximum":{"Value":31.6,"Unit":"C","UnitType":17}},"RealFeelTemperatureShade":{"Minimum":{"Value":17.9,"Unit":"C","UnitType":17},"Maximum":{"Value":28.9,"Unit":"C","UnitType":17}},"HoursOfSun":5.2,"DegreeDaySummary":{"Heating":{"Value":0.0,"Unit":"C","UnitType":17},"Cooling":{"Value":6.0,"Unit":"C","UnitType":17}},"AirAndPollen":[{"Name":"AirQuality","Value":0,"Category":"Good","CategoryValue":1,"Type":"Ozone"},{"Name":"Grass","Value":0,"Category":"Low","CategoryValue":1},{"Name":"Mold","Value":0,"Category":"Low","CategoryValue":1},{"Name":"Ragweed","Value":0,"Category":"Low","CategoryValue":1},{"Name":"Tree","Value":0,"Category":"Low","CategoryValue":1},{"Name":"UVIndex","Value":6,"Category":"High","CategoryValue":3}],"Day":{"Icon":6,"IconPhrase":"Mostly cloudy","HasPrecipitation":false,"ShortPhrase":"Mostly cloudy","LongPhrase":"Mostly cloudy","PrecipitationProbability":0,"ThunderstormProbability":0,"RainProbability":0,"SnowProbability":0,"IceProbability":0,"Wind":{"Speed":{"Value":7.4,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":127,"Localized":"SE","English":"SE"}},"WindGust":{"Speed":{"Value":13.0,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":150,"Localized":"SSE","English":"SSE"}},"TotalLiquid":{"Value":0.0,"Unit":"mm","UnitType":3},"Rain":{"Value":0.0,"Unit":"mm","UnitType":3},"Snow":{"Value":0.0,"Unit":"cm","UnitType":4},"Ice":{"Value":0.0,"Unit":"mm","UnitType":3},"HoursOfPrecipitation":0.0,"HoursOfRain":0.0,"HoursOfSnow":0.0,"HoursOfIce":0.0,"CloudCover":60},"Night":{"Icon":35,"IconPhrase":"Partly cloudy","HasPrecipitation":false,"ShortPhrase":"Partly cloudy","LongPhrase":"Partly cloudy","PrecipitationProbability":0,"ThunderstormProbability":0,"RainProbability":0,"SnowProbability":0,"IceProbability":0,"Wind":{"Speed":{"Value":5.6,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":64,"Localized":"ENE","English":"ENE"}},"WindGust":{"Speed":{"Value":9.3,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":27,"Localized":"NNE","English":"NNE"}},"TotalLiquid":{"Value":0.0,"Unit":"mm","UnitType":3},"Rain":{"Value":0.0,"Unit":"mm","UnitType":3},"Snow":{"Value":0.0,"Unit":"cm","UnitType":4},"Ice":{"Value":0.0,"Unit":"mm","UnitType":3},"HoursOfPrecipitation":0.0,"HoursOfRain":0.0,"HoursOfSnow":0.0,"HoursOfIce":0.0,"CloudCover":45},"Sources":["AccuWeather"],"MobileLink":"http://m.accuweather.com/en/ng/abuja/254085/daily-weather-forecast/254085?day=1&unit=c&lang=en-us","Link":"http://www.accuweather.com/en/ng/abuja/254085/daily-weather-forecast/254085?day=1&unit=c&lang=en-us"}]}', '[{"LocalObservationDateTime":"2019-12-13T07:22:00+01:00","EpochTime":1576218120,"WeatherText":"Cloudy","WeatherIcon":7,"HasPrecipitation":false,"PrecipitationType":null,"IsDayTime":true,"Temperature":{"Metric":{"Value":19.9,"Unit":"C","UnitType":17},"Imperial":{"Value":68.0,"Unit":"F","UnitType":18}},"MobileLink":"http://m.accuweather.com/en/ng/abuja/254085/current-weather/254085?lang=en-us","Link":"http://www.accuweather.com/en/ng/abuja/254085/current-weather/254085?lang=en-us"}]', '2019-12-13 06:26:36'),
(7, 0, '932298', '{"Headline":{"EffectiveDate":"2019-12-14T07:00:00+01:00","EffectiveEpochDate":1576303200,"Severity":4,"Text":"Pleasant this weekend","Category":"","EndDate":null,"EndEpochDate":null,"MobileLink":"http://m.accuweather.com/en/ng/karu/932298/extended-weather-forecast/932298?unit=c&lang=en-us","Link":"http://www.accuweather.com/en/ng/karu/932298/daily-weather-forecast/932298?unit=c&lang=en-us"},"DailyForecasts":[{"Date":"2019-12-13T07:00:00+01:00","EpochDate":1576216800,"Sun":{"Rise":"2019-12-13T06:36:00+01:00","EpochRise":1576215360,"Set":"2019-12-13T18:12:00+01:00","EpochSet":1576257120},"Moon":{"Rise":"2019-12-13T19:33:00+01:00","EpochRise":1576261980,"Set":"2019-12-14T08:33:00+01:00","EpochSet":1576308780,"Phase":"WaningGibbous","Age":17},"Temperature":{"Minimum":{"Value":18.2,"Unit":"C","UnitType":17},"Maximum":{"Value":30.0,"Unit":"C","UnitType":17}},"RealFeelTemperature":{"Minimum":{"Value":17.9,"Unit":"C","UnitType":17},"Maximum":{"Value":31.6,"Unit":"C","UnitType":17}},"RealFeelTemperatureShade":{"Minimum":{"Value":17.9,"Unit":"C","UnitType":17},"Maximum":{"Value":28.9,"Unit":"C","UnitType":17}},"HoursOfSun":5.2,"DegreeDaySummary":{"Heating":{"Value":0.0,"Unit":"C","UnitType":17},"Cooling":{"Value":6.0,"Unit":"C","UnitType":17}},"AirAndPollen":[{"Name":"AirQuality","Value":0,"Category":"Good","CategoryValue":1,"Type":"Ozone"},{"Name":"Grass","Value":0,"Category":"Low","CategoryValue":1},{"Name":"Mold","Value":0,"Category":"Low","CategoryValue":1},{"Name":"Ragweed","Value":0,"Category":"Low","CategoryValue":1},{"Name":"Tree","Value":0,"Category":"Low","CategoryValue":1},{"Name":"UVIndex","Value":6,"Category":"High","CategoryValue":3}],"Day":{"Icon":6,"IconPhrase":"Mostly cloudy","HasPrecipitation":false,"ShortPhrase":"Mostly cloudy","LongPhrase":"Mostly cloudy","PrecipitationProbability":0,"ThunderstormProbability":0,"RainProbability":0,"SnowProbability":0,"IceProbability":0,"Wind":{"Speed":{"Value":7.4,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":127,"Localized":"SE","English":"SE"}},"WindGust":{"Speed":{"Value":13.0,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":150,"Localized":"SSE","English":"SSE"}},"TotalLiquid":{"Value":0.0,"Unit":"mm","UnitType":3},"Rain":{"Value":0.0,"Unit":"mm","UnitType":3},"Snow":{"Value":0.0,"Unit":"cm","UnitType":4},"Ice":{"Value":0.0,"Unit":"mm","UnitType":3},"HoursOfPrecipitation":0.0,"HoursOfRain":0.0,"HoursOfSnow":0.0,"HoursOfIce":0.0,"CloudCover":60},"Night":{"Icon":35,"IconPhrase":"Partly cloudy","HasPrecipitation":false,"ShortPhrase":"Partly cloudy","LongPhrase":"Partly cloudy","PrecipitationProbability":0,"ThunderstormProbability":0,"RainProbability":0,"SnowProbability":0,"IceProbability":0,"Wind":{"Speed":{"Value":5.6,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":64,"Localized":"ENE","English":"ENE"}},"WindGust":{"Speed":{"Value":9.3,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":27,"Localized":"NNE","English":"NNE"}},"TotalLiquid":{"Value":0.0,"Unit":"mm","UnitType":3},"Rain":{"Value":0.0,"Unit":"mm","UnitType":3},"Snow":{"Value":0.0,"Unit":"cm","UnitType":4},"Ice":{"Value":0.0,"Unit":"mm","UnitType":3},"HoursOfPrecipitation":0.0,"HoursOfRain":0.0,"HoursOfSnow":0.0,"HoursOfIce":0.0,"CloudCover":45},"Sources":["AccuWeather"],"MobileLink":"http://m.accuweather.com/en/ng/karu/932298/daily-weather-forecast/932298?day=1&unit=c&lang=en-us","Link":"http://www.accuweather.com/en/ng/karu/932298/daily-weather-forecast/932298?day=1&unit=c&lang=en-us"}]}', '[{"LocalObservationDateTime":"2019-12-13T08:11:00+01:00","EpochTime":1576221060,"WeatherText":"Cloudy","WeatherIcon":7,"HasPrecipitation":false,"PrecipitationType":null,"IsDayTime":true,"Temperature":{"Metric":{"Value":21.2,"Unit":"C","UnitType":17},"Imperial":{"Value":70.0,"Unit":"F","UnitType":18}},"MobileLink":"http://m.accuweather.com/en/ng/karu/932298/current-weather/932298?lang=en-us","Link":"http://www.accuweather.com/en/ng/karu/932298/current-weather/932298?lang=en-us"}]', '2019-12-13 07:20:20'),
(8, 0, '932298', '{"Headline":{"EffectiveDate":"2019-12-21T07:00:00+01:00","EffectiveEpochDate":1576908000,"Severity":7,"Text":"Sunny this weekend","Category":"","EndDate":null,"EndEpochDate":null,"MobileLink":"http://m.accuweather.com/en/ng/karu/932298/extended-weather-forecast/932298?unit=c&lang=en-us","Link":"http://www.accuweather.com/en/ng/karu/932298/daily-weather-forecast/932298?unit=c&lang=en-us"},"DailyForecasts":[{"Date":"2019-12-16T07:00:00+01:00","EpochDate":1576476000,"Sun":{"Rise":"2019-12-16T06:37:00+01:00","EpochRise":1576474620,"Set":"2019-12-16T18:13:00+01:00","EpochSet":1576516380},"Moon":{"Rise":"2019-12-16T22:30:00+01:00","EpochRise":1576531800,"Set":"2019-12-17T11:17:00+01:00","EpochSet":1576577820,"Phase":"WaningGibbous","Age":20},"Temperature":{"Minimum":{"Value":19.6,"Unit":"C","UnitType":17},"Maximum":{"Value":32.0,"Unit":"C","UnitType":17}},"RealFeelTemperature":{"Minimum":{"Value":18.4,"Unit":"C","UnitType":17},"Maximum":{"Value":34.4,"Unit":"C","UnitType":17}},"RealFeelTemperatureShade":{"Minimum":{"Value":18.4,"Unit":"C","UnitType":17},"Maximum":{"Value":30.8,"Unit":"C","UnitType":17}},"HoursOfSun":6.8,"DegreeDaySummary":{"Heating":{"Value":0.0,"Unit":"C","UnitType":17},"Cooling":{"Value":8.0,"Unit":"C","UnitType":17}},"AirAndPollen":[{"Name":"AirQuality","Value":0,"Category":"Good","CategoryValue":1,"Type":"Ozone"},{"Name":"Grass","Value":0,"Category":"Low","CategoryValue":1},{"Name":"Mold","Value":0,"Category":"Low","CategoryValue":1},{"Name":"Ragweed","Value":0,"Category":"Low","CategoryValue":1},{"Name":"Tree","Value":0,"Category":"Low","CategoryValue":1},{"Name":"UVIndex","Value":7,"Category":"High","CategoryValue":3}],"Day":{"Icon":3,"IconPhrase":"Partly sunny","HasPrecipitation":false,"ShortPhrase":"Becoming cloudy","LongPhrase":"Mostly sunny this morning followed by sun and areas of high clouds","PrecipitationProbability":0,"ThunderstormProbability":0,"RainProbability":0,"SnowProbability":0,"IceProbability":0,"Wind":{"Speed":{"Value":7.4,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":92,"Localized":"E","English":"E"}},"WindGust":{"Speed":{"Value":13.0,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":129,"Localized":"SE","English":"SE"}},"TotalLiquid":{"Value":0.0,"Unit":"mm","UnitType":3},"Rain":{"Value":0.0,"Unit":"mm","UnitType":3},"Snow":{"Value":0.0,"Unit":"cm","UnitType":4},"Ice":{"Value":0.0,"Unit":"mm","UnitType":3},"HoursOfPrecipitation":0.0,"HoursOfRain":0.0,"HoursOfSnow":0.0,"HoursOfIce":0.0,"CloudCover":42},"Night":{"Icon":33,"IconPhrase":"Clear","HasPrecipitation":false,"ShortPhrase":"Clear","LongPhrase":"Clear","PrecipitationProbability":0,"ThunderstormProbability":0,"RainProbability":0,"SnowProbability":0,"IceProbability":0,"Wind":{"Speed":{"Value":7.4,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":320,"Localized":"NW","English":"NW"}},"WindGust":{"Speed":{"Value":9.3,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":340,"Localized":"NNW","English":"NNW"}},"TotalLiquid":{"Value":0.0,"Unit":"mm","UnitType":3},"Rain":{"Value":0.0,"Unit":"mm","UnitType":3},"Snow":{"Value":0.0,"Unit":"cm","UnitType":4},"Ice":{"Value":0.0,"Unit":"mm","UnitType":3},"HoursOfPrecipitation":0.0,"HoursOfRain":0.0,"HoursOfSnow":0.0,"HoursOfIce":0.0,"CloudCover":0},"Sources":["AccuWeather"],"MobileLink":"http://m.accuweather.com/en/ng/karu/932298/daily-weather-forecast/932298?day=1&unit=c&lang=en-us","Link":"http://www.accuweather.com/en/ng/karu/932298/daily-weather-forecast/932298?day=1&unit=c&lang=en-us"}]}', '[{"LocalObservationDateTime":"2019-12-16T06:21:00+01:00","EpochTime":1576473660,"WeatherText":"Clear","WeatherIcon":33,"HasPrecipitation":false,"PrecipitationType":null,"IsDayTime":false,"Temperature":{"Metric":{"Value":19.4,"Unit":"C","UnitType":17},"Imperial":{"Value":67.0,"Unit":"F","UnitType":18}},"MobileLink":"http://m.accuweather.com/en/ng/karu/932298/current-weather/932298?lang=en-us","Link":"http://www.accuweather.com/en/ng/karu/932298/current-weather/932298?lang=en-us"}]', '2019-12-16 05:32:29'),
(9, 0, '932298', '{"Headline":{"EffectiveDate":"2019-12-21T07:00:00+01:00","EffectiveEpochDate":1576908000,"Severity":7,"Text":"Sunny this weekend","Category":"","EndDate":null,"EndEpochDate":null,"MobileLink":"http://m.accuweather.com/en/ng/karu/932298/extended-weather-forecast/932298?unit=c&lang=en-us","Link":"http://www.accuweather.com/en/ng/karu/932298/daily-weather-forecast/932298?unit=c&lang=en-us"},"DailyForecasts":[{"Date":"2019-12-17T07:00:00+01:00","EpochDate":1576562400,"Sun":{"Rise":"2019-12-17T06:38:00+01:00","EpochRise":1576561080,"Set":"2019-12-17T18:14:00+01:00","EpochSet":1576602840},"Moon":{"Rise":"2019-12-17T23:27:00+01:00","EpochRise":1576621620,"Set":"2019-12-18T12:06:00+01:00","EpochSet":1576667160,"Phase":"WaningGibbous","Age":21},"Temperature":{"Minimum":{"Value":18.9,"Unit":"C","UnitType":17},"Maximum":{"Value":31.4,"Unit":"C","UnitType":17}},"RealFeelTemperature":{"Minimum":{"Value":19.0,"Unit":"C","UnitType":17},"Maximum":{"Value":33.5,"Unit":"C","UnitType":17}},"RealFeelTemperatureShade":{"Minimum":{"Value":19.0,"Unit":"C","UnitType":17},"Maximum":{"Value":30.3,"Unit":"C","UnitType":17}},"HoursOfSun":7.1,"DegreeDaySummary":{"Heating":{"Value":0.0,"Unit":"C","UnitType":17},"Cooling":{"Value":7.0,"Unit":"C","UnitType":17}},"AirAndPollen":[{"Name":"AirQuality","Value":0,"Category":"Good","CategoryValue":1,"Type":"Ozone"},{"Name":"Grass","Value":0,"Category":"Low","CategoryValue":1},{"Name":"Mold","Value":0,"Category":"Low","CategoryValue":1},{"Name":"Ragweed","Value":0,"Category":"Low","CategoryValue":1},{"Name":"Tree","Value":0,"Category":"Low","CategoryValue":1},{"Name":"UVIndex","Value":7,"Category":"High","CategoryValue":3}],"Day":{"Icon":6,"IconPhrase":"Mostly cloudy","HasPrecipitation":false,"ShortPhrase":"Mostly cloudy","LongPhrase":"Mostly cloudy","PrecipitationProbability":0,"ThunderstormProbability":0,"RainProbability":0,"SnowProbability":0,"IceProbability":0,"Wind":{"Speed":{"Value":7.4,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":122,"Localized":"ESE","English":"ESE"}},"WindGust":{"Speed":{"Value":11.1,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":128,"Localized":"SE","English":"SE"}},"TotalLiquid":{"Value":0.0,"Unit":"mm","UnitType":3},"Rain":{"Value":0.0,"Unit":"mm","UnitType":3},"Snow":{"Value":0.0,"Unit":"cm","UnitType":4},"Ice":{"Value":0.0,"Unit":"mm","UnitType":3},"HoursOfPrecipitation":0.0,"HoursOfRain":0.0,"HoursOfSnow":0.0,"HoursOfIce":0.0,"CloudCover":43},"Night":{"Icon":34,"IconPhrase":"Mostly clear","HasPrecipitation":false,"ShortPhrase":"Mainly clear","LongPhrase":"Mainly clear","PrecipitationProbability":0,"ThunderstormProbability":0,"RainProbability":0,"SnowProbability":0,"IceProbability":0,"Wind":{"Speed":{"Value":5.6,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":73,"Localized":"ENE","English":"ENE"}},"WindGust":{"Speed":{"Value":9.3,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":121,"Localized":"ESE","English":"ESE"}},"TotalLiquid":{"Value":0.0,"Unit":"mm","UnitType":3},"Rain":{"Value":0.0,"Unit":"mm","UnitType":3},"Snow":{"Value":0.0,"Unit":"cm","UnitType":4},"Ice":{"Value":0.0,"Unit":"mm","UnitType":3},"HoursOfPrecipitation":0.0,"HoursOfRain":0.0,"HoursOfSnow":0.0,"HoursOfIce":0.0,"CloudCover":16},"Sources":["AccuWeather"],"MobileLink":"http://m.accuweather.com/en/ng/karu/932298/daily-weather-forecast/932298?day=1&unit=c&lang=en-us","Link":"http://www.accuweather.com/en/ng/karu/932298/daily-weather-forecast/932298?day=1&unit=c&lang=en-us"}]}', '[{"LocalObservationDateTime":"2019-12-17T06:11:00+01:00","EpochTime":1576559460,"WeatherText":"Clear","WeatherIcon":33,"HasPrecipitation":false,"PrecipitationType":null,"IsDayTime":false,"Temperature":{"Metric":{"Value":19.3,"Unit":"C","UnitType":17},"Imperial":{"Value":67.0,"Unit":"F","UnitType":18}},"MobileLink":"http://m.accuweather.com/en/ng/karu/932298/current-weather/932298?lang=en-us","Link":"http://www.accuweather.com/en/ng/karu/932298/current-weather/932298?lang=en-us"}]', '2019-12-17 05:18:48'),
(10, 0, '254085', '{"Headline":{"EffectiveDate":"2019-12-21T07:00:00+01:00","EffectiveEpochDate":1576908000,"Severity":4,"Text":"Pleasant Saturday","Category":"","EndDate":null,"EndEpochDate":null,"MobileLink":"http://m.accuweather.com/en/ng/abuja/254085/extended-weather-forecast/254085?unit=c&lang=en-us","Link":"http://www.accuweather.com/en/ng/abuja/254085/daily-weather-forecast/254085?unit=c&lang=en-us"},"DailyForecasts":[{"Date":"2019-12-17T07:00:00+01:00","EpochDate":1576562400,"Sun":{"Rise":"2019-12-17T06:38:00+01:00","EpochRise":1576561080,"Set":"2019-12-17T18:14:00+01:00","EpochSet":1576602840},"Moon":{"Rise":"2019-12-17T23:27:00+01:00","EpochRise":1576621620,"Set":"2019-12-18T12:06:00+01:00","EpochSet":1576667160,"Phase":"WaningGibbous","Age":21},"Temperature":{"Minimum":{"Value":18.9,"Unit":"C","UnitType":17},"Maximum":{"Value":31.4,"Unit":"C","UnitType":17}},"RealFeelTemperature":{"Minimum":{"Value":19.0,"Unit":"C","UnitType":17},"Maximum":{"Value":33.5,"Unit":"C","UnitType":17}},"RealFeelTemperatureShade":{"Minimum":{"Value":19.0,"Unit":"C","UnitType":17},"Maximum":{"Value":30.3,"Unit":"C","UnitType":17}},"HoursOfSun":7.1,"DegreeDaySummary":{"Heating":{"Value":0.0,"Unit":"C","UnitType":17},"Cooling":{"Value":7.0,"Unit":"C","UnitType":17}},"AirAndPollen":[{"Name":"AirQuality","Value":0,"Category":"Good","CategoryValue":1,"Type":"Ozone"},{"Name":"Grass","Value":0,"Category":"Low","CategoryValue":1},{"Name":"Mold","Value":0,"Category":"Low","CategoryValue":1},{"Name":"Ragweed","Value":0,"Category":"Low","CategoryValue":1},{"Name":"Tree","Value":0,"Category":"Low","CategoryValue":1},{"Name":"UVIndex","Value":7,"Category":"High","CategoryValue":3}],"Day":{"Icon":6,"IconPhrase":"Mostly cloudy","HasPrecipitation":false,"ShortPhrase":"Mostly cloudy","LongPhrase":"Mostly cloudy","PrecipitationProbability":0,"ThunderstormProbability":0,"RainProbability":0,"SnowProbability":0,"IceProbability":0,"Wind":{"Speed":{"Value":7.4,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":122,"Localized":"ESE","English":"ESE"}},"WindGust":{"Speed":{"Value":11.1,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":128,"Localized":"SE","English":"SE"}},"TotalLiquid":{"Value":0.0,"Unit":"mm","UnitType":3},"Rain":{"Value":0.0,"Unit":"mm","UnitType":3},"Snow":{"Value":0.0,"Unit":"cm","UnitType":4},"Ice":{"Value":0.0,"Unit":"mm","UnitType":3},"HoursOfPrecipitation":0.0,"HoursOfRain":0.0,"HoursOfSnow":0.0,"HoursOfIce":0.0,"CloudCover":43},"Night":{"Icon":34,"IconPhrase":"Mostly clear","HasPrecipitation":false,"ShortPhrase":"Mainly clear","LongPhrase":"Mainly clear","PrecipitationProbability":0,"ThunderstormProbability":0,"RainProbability":0,"SnowProbability":0,"IceProbability":0,"Wind":{"Speed":{"Value":5.6,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":73,"Localized":"ENE","English":"ENE"}},"WindGust":{"Speed":{"Value":9.3,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":121,"Localized":"ESE","English":"ESE"}},"TotalLiquid":{"Value":0.0,"Unit":"mm","UnitType":3},"Rain":{"Value":0.0,"Unit":"mm","UnitType":3},"Snow":{"Value":0.0,"Unit":"cm","UnitType":4},"Ice":{"Value":0.0,"Unit":"mm","UnitType":3},"HoursOfPrecipitation":0.0,"HoursOfRain":0.0,"HoursOfSnow":0.0,"HoursOfIce":0.0,"CloudCover":16},"Sources":["AccuWeather"],"MobileLink":"http://m.accuweather.com/en/ng/abuja/254085/daily-weather-forecast/254085?day=1&unit=c&lang=en-us","Link":"http://www.accuweather.com/en/ng/abuja/254085/daily-weather-forecast/254085?day=1&unit=c&lang=en-us"}]}', '[{"LocalObservationDateTime":"2019-12-17T07:11:00+01:00","EpochTime":1576563060,"WeatherText":"Sunny","WeatherIcon":1,"HasPrecipitation":false,"PrecipitationType":null,"IsDayTime":true,"Temperature":{"Metric":{"Value":20.2,"Unit":"C","UnitType":17},"Imperial":{"Value":68.0,"Unit":"F","UnitType":18}},"MobileLink":"http://m.accuweather.com/en/ng/abuja/254085/current-weather/254085?lang=en-us","Link":"http://www.accuweather.com/en/ng/abuja/254085/current-weather/254085?lang=en-us"}]', '2019-12-17 06:23:37'),
(11, 0, '254085', '{"Headline":{"EffectiveDate":"2019-12-21T07:00:00+01:00","EffectiveEpochDate":1576908000,"Severity":4,"Text":"Pleasant this weekend","Category":"","EndDate":null,"EndEpochDate":null,"MobileLink":"http://m.accuweather.com/en/ng/abuja/254085/extended-weather-forecast/254085?unit=c&lang=en-us","Link":"http://www.accuweather.com/en/ng/abuja/254085/daily-weather-forecast/254085?unit=c&lang=en-us"},"DailyForecasts":[{"Date":"2019-12-20T07:00:00+01:00","EpochDate":1576821600,"Sun":{"Rise":"2019-12-20T06:40:00+01:00","EpochRise":1576820400,"Set":"2019-12-20T18:15:00+01:00","EpochSet":1576862100},"Moon":{"Rise":"2019-12-20T01:16:00+01:00","EpochRise":1576800960,"Set":"2019-12-20T13:39:00+01:00","EpochSet":1576845540,"Phase":"WaningCrescent","Age":24},"Temperature":{"Minimum":{"Value":17.3,"Unit":"C","UnitType":17},"Maximum":{"Value":31.4,"Unit":"C","UnitType":17}},"RealFeelTemperature":{"Minimum":{"Value":17.4,"Unit":"C","UnitType":17},"Maximum":{"Value":34.1,"Unit":"C","UnitType":17}},"RealFeelTemperatureShade":{"Minimum":{"Value":17.4,"Unit":"C","UnitType":17},"Maximum":{"Value":30.4,"Unit":"C","UnitType":17}},"HoursOfSun":10.0,"DegreeDaySummary":{"Heating":{"Value":0.0,"Unit":"C","UnitType":17},"Cooling":{"Value":6.0,"Unit":"C","UnitType":17}},"AirAndPollen":[{"Name":"AirQuality","Value":0,"Category":"Good","CategoryValue":1,"Type":"Ozone"},{"Name":"Grass","Value":0,"Category":"Low","CategoryValue":1},{"Name":"Mold","Value":0,"Category":"Low","CategoryValue":1},{"Name":"Ragweed","Value":0,"Category":"Low","CategoryValue":1},{"Name":"Tree","Value":0,"Category":"Low","CategoryValue":1},{"Name":"UVIndex","Value":7,"Category":"High","CategoryValue":3}],"Day":{"Icon":2,"IconPhrase":"Mostly sunny","HasPrecipitation":false,"ShortPhrase":"Mostly sunny and pleasant","LongPhrase":"Mostly sunny and pleasant","PrecipitationProbability":0,"ThunderstormProbability":0,"RainProbability":0,"SnowProbability":0,"IceProbability":0,"Wind":{"Speed":{"Value":7.4,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":138,"Localized":"SE","English":"SE"}},"WindGust":{"Speed":{"Value":11.1,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":109,"Localized":"ESE","English":"ESE"}},"TotalLiquid":{"Value":0.0,"Unit":"mm","UnitType":3},"Rain":{"Value":0.0,"Unit":"mm","UnitType":3},"Snow":{"Value":0.0,"Unit":"cm","UnitType":4},"Ice":{"Value":0.0,"Unit":"mm","UnitType":3},"HoursOfPrecipitation":0.0,"HoursOfRain":0.0,"HoursOfSnow":0.0,"HoursOfIce":0.0,"CloudCover":14},"Night":{"Icon":33,"IconPhrase":"Clear","HasPrecipitation":false,"ShortPhrase":"Clear","LongPhrase":"Clear","PrecipitationProbability":0,"ThunderstormProbability":0,"RainProbability":0,"SnowProbability":0,"IceProbability":0,"Wind":{"Speed":{"Value":5.6,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":17,"Localized":"NNE","English":"NNE"}},"WindGust":{"Speed":{"Value":9.3,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":48,"Localized":"NE","English":"NE"}},"TotalLiquid":{"Value":0.0,"Unit":"mm","UnitType":3},"Rain":{"Value":0.0,"Unit":"mm","UnitType":3},"Snow":{"Value":0.0,"Unit":"cm","UnitType":4},"Ice":{"Value":0.0,"Unit":"mm","UnitType":3},"HoursOfPrecipitation":0.0,"HoursOfRain":0.0,"HoursOfSnow":0.0,"HoursOfIce":0.0,"CloudCover":8},"Sources":["AccuWeather"],"MobileLink":"http://m.accuweather.com/en/ng/abuja/254085/daily-weather-forecast/254085?day=1&unit=c&lang=en-us","Link":"http://www.accuweather.com/en/ng/abuja/254085/daily-weather-forecast/254085?day=1&unit=c&lang=en-us"}]}', '[{"LocalObservationDateTime":"2019-12-20T08:22:00+01:00","EpochTime":1576826520,"WeatherText":"Mostly sunny","WeatherIcon":2,"HasPrecipitation":false,"PrecipitationType":null,"IsDayTime":true,"Temperature":{"Metric":{"Value":21.4,"Unit":"C","UnitType":17},"Imperial":{"Value":71.0,"Unit":"F","UnitType":18}},"MobileLink":"http://m.accuweather.com/en/ng/abuja/254085/current-weather/254085?lang=en-us","Link":"http://www.accuweather.com/en/ng/abuja/254085/current-weather/254085?lang=en-us"}]', '2019-12-20 07:32:39'),
(12, 0, '254085', '{"Headline":{"EffectiveDate":"2019-12-28T07:00:00+01:00","EffectiveEpochDate":1577512800,"Severity":4,"Text":"Pleasant this weekend","Category":"","EndDate":null,"EndEpochDate":null,"MobileLink":"http://m.accuweather.com/en/ng/abuja/254085/extended-weather-forecast/254085?unit=c&lang=en-us","Link":"http://www.accuweather.com/en/ng/abuja/254085/daily-weather-forecast/254085?unit=c&lang=en-us"},"DailyForecasts":[{"Date":"2019-12-24T07:00:00+01:00","EpochDate":1577167200,"Sun":{"Rise":"2019-12-24T06:42:00+01:00","EpochRise":1577166120,"Set":"2019-12-24T18:17:00+01:00","EpochSet":1577207820},"Moon":{"Rise":"2019-12-24T04:54:00+01:00","EpochRise":1577159640,"Set":"2019-12-24T16:53:00+01:00","EpochSet":1577202780,"Phase":"WaningCrescent","Age":28},"Temperature":{"Minimum":{"Value":18.0,"Unit":"C","UnitType":17},"Maximum":{"Value":31.3,"Unit":"C","UnitType":17}},"RealFeelTemperature":{"Minimum":{"Value":17.7,"Unit":"C","UnitType":17},"Maximum":{"Value":33.7,"Unit":"C","UnitType":17}},"RealFeelTemperatureShade":{"Minimum":{"Value":17.7,"Unit":"C","UnitType":17},"Maximum":{"Value":30.1,"Unit":"C","UnitType":17}},"HoursOfSun":11.6,"DegreeDaySummary":{"Heating":{"Value":0.0,"Unit":"C","UnitType":17},"Cooling":{"Value":7.0,"Unit":"C","UnitType":17}},"AirAndPollen":[{"Name":"AirQuality","Value":0,"Category":"Good","CategoryValue":1,"Type":"Ozone"},{"Name":"Grass","Value":0,"Category":"Low","CategoryValue":1},{"Name":"Mold","Value":0,"Category":"Low","CategoryValue":1},{"Name":"Ragweed","Value":0,"Category":"Low","CategoryValue":1},{"Name":"Tree","Value":0,"Category":"Low","CategoryValue":1},{"Name":"UVIndex","Value":7,"Category":"High","CategoryValue":3}],"Day":{"Icon":1,"IconPhrase":"Sunny","HasPrecipitation":false,"ShortPhrase":"Nice with plenty of sunshine","LongPhrase":"Nice with plenty of sunshine","PrecipitationProbability":0,"ThunderstormProbability":0,"RainProbability":0,"SnowProbability":0,"IceProbability":0,"Wind":{"Speed":{"Value":7.4,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":173,"Localized":"S","English":"S"}},"WindGust":{"Speed":{"Value":11.1,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":241,"Localized":"WSW","English":"WSW"}},"TotalLiquid":{"Value":0.0,"Unit":"mm","UnitType":3},"Rain":{"Value":0.0,"Unit":"mm","UnitType":3},"Snow":{"Value":0.0,"Unit":"cm","UnitType":4},"Ice":{"Value":0.0,"Unit":"mm","UnitType":3},"HoursOfPrecipitation":0.0,"HoursOfRain":0.0,"HoursOfSnow":0.0,"HoursOfIce":0.0,"CloudCover":0},"Night":{"Icon":33,"IconPhrase":"Clear","HasPrecipitation":false,"ShortPhrase":"Clear","LongPhrase":"Clear","PrecipitationProbability":0,"ThunderstormProbability":0,"RainProbability":0,"SnowProbability":0,"IceProbability":0,"Wind":{"Speed":{"Value":7.4,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":360,"Localized":"N","English":"N"}},"WindGust":{"Speed":{"Value":9.3,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":30,"Localized":"NNE","English":"NNE"}},"TotalLiquid":{"Value":0.0,"Unit":"mm","UnitType":3},"Rain":{"Value":0.0,"Unit":"mm","UnitType":3},"Snow":{"Value":0.0,"Unit":"cm","UnitType":4},"Ice":{"Value":0.0,"Unit":"mm","UnitType":3},"HoursOfPrecipitation":0.0,"HoursOfRain":0.0,"HoursOfSnow":0.0,"HoursOfIce":0.0,"CloudCover":0},"Sources":["AccuWeather"],"MobileLink":"http://m.accuweather.com/en/ng/abuja/254085/daily-weather-forecast/254085?day=1&unit=c&lang=en-us","Link":"http://www.accuweather.com/en/ng/abuja/254085/daily-weather-forecast/254085?day=1&unit=c&lang=en-us"}]}', '[{"LocalObservationDateTime":"2019-12-24T08:11:00+01:00","EpochTime":1577171460,"WeatherText":"Sunny","WeatherIcon":1,"HasPrecipitation":false,"PrecipitationType":null,"IsDayTime":true,"Temperature":{"Metric":{"Value":19.5,"Unit":"C","UnitType":17},"Imperial":{"Value":67.0,"Unit":"F","UnitType":18}},"MobileLink":"http://m.accuweather.com/en/ng/abuja/254085/current-weather/254085?lang=en-us","Link":"http://www.accuweather.com/en/ng/abuja/254085/current-weather/254085?lang=en-us"}]', '2019-12-24 07:16:25');
INSERT INTO `t_weather` (`id`, `customer_id`, `location_key`, `daily_status`, `current_status`, `date`) VALUES
(13, 0, '254085', '{"Headline":{"EffectiveDate":"2020-01-04T07:00:00+01:00","EffectiveEpochDate":1578117600,"Severity":7,"Text":"Partly sunny this weekend","Category":"","EndDate":null,"EndEpochDate":null,"MobileLink":"http://m.accuweather.com/en/ng/abuja/254085/extended-weather-forecast/254085?unit=c&lang=en-us","Link":"http://www.accuweather.com/en/ng/abuja/254085/daily-weather-forecast/254085?unit=c&lang=en-us"},"DailyForecasts":[{"Date":"2019-12-30T07:00:00+01:00","EpochDate":1577685600,"Sun":{"Rise":"2019-12-30T06:44:00+01:00","EpochRise":1577684640,"Set":"2019-12-30T18:21:00+01:00","EpochSet":1577726460},"Moon":{"Rise":"2019-12-30T10:08:00+01:00","EpochRise":1577696880,"Set":"2019-12-30T22:09:00+01:00","EpochSet":1577740140,"Phase":"WaxingCrescent","Age":4},"Temperature":{"Minimum":{"Value":17.4,"Unit":"C","UnitType":17},"Maximum":{"Value":31.1,"Unit":"C","UnitType":17}},"RealFeelTemperature":{"Minimum":{"Value":17.2,"Unit":"C","UnitType":17},"Maximum":{"Value":33.6,"Unit":"C","UnitType":17}},"RealFeelTemperatureShade":{"Minimum":{"Value":17.2,"Unit":"C","UnitType":17},"Maximum":{"Value":29.9,"Unit":"C","UnitType":17}},"HoursOfSun":9.6,"DegreeDaySummary":{"Heating":{"Value":0.0,"Unit":"C","UnitType":17},"Cooling":{"Value":6.0,"Unit":"C","UnitType":17}},"AirAndPollen":[{"Name":"AirQuality","Value":0,"Category":"Good","CategoryValue":1,"Type":"Ozone"},{"Name":"Grass","Value":0,"Category":"Low","CategoryValue":1},{"Name":"Mold","Value":0,"Category":"Low","CategoryValue":1},{"Name":"Ragweed","Value":0,"Category":"Low","CategoryValue":1},{"Name":"Tree","Value":0,"Category":"Low","CategoryValue":1},{"Name":"UVIndex","Value":7,"Category":"High","CategoryValue":3}],"Day":{"Icon":2,"IconPhrase":"Mostly sunny","HasPrecipitation":false,"ShortPhrase":"Mostly sunny and nice","LongPhrase":"Mostly sunny and nice","PrecipitationProbability":0,"ThunderstormProbability":0,"RainProbability":0,"SnowProbability":0,"IceProbability":0,"Wind":{"Speed":{"Value":7.4,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":185,"Localized":"S","English":"S"}},"WindGust":{"Speed":{"Value":13.0,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":249,"Localized":"WSW","English":"WSW"}},"TotalLiquid":{"Value":0.0,"Unit":"mm","UnitType":3},"Rain":{"Value":0.0,"Unit":"mm","UnitType":3},"Snow":{"Value":0.0,"Unit":"cm","UnitType":4},"Ice":{"Value":0.0,"Unit":"mm","UnitType":3},"HoursOfPrecipitation":0.0,"HoursOfRain":0.0,"HoursOfSnow":0.0,"HoursOfIce":0.0,"CloudCover":18},"Night":{"Icon":33,"IconPhrase":"Clear","HasPrecipitation":false,"ShortPhrase":"Clear","LongPhrase":"Clear","PrecipitationProbability":0,"ThunderstormProbability":0,"RainProbability":0,"SnowProbability":0,"IceProbability":0,"Wind":{"Speed":{"Value":7.4,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":1,"Localized":"N","English":"N"}},"WindGust":{"Speed":{"Value":11.1,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":31,"Localized":"NNE","English":"NNE"}},"TotalLiquid":{"Value":0.0,"Unit":"mm","UnitType":3},"Rain":{"Value":0.0,"Unit":"mm","UnitType":3},"Snow":{"Value":0.0,"Unit":"cm","UnitType":4},"Ice":{"Value":0.0,"Unit":"mm","UnitType":3},"HoursOfPrecipitation":0.0,"HoursOfRain":0.0,"HoursOfSnow":0.0,"HoursOfIce":0.0,"CloudCover":0},"Sources":["AccuWeather"],"MobileLink":"http://m.accuweather.com/en/ng/abuja/254085/daily-weather-forecast/254085?day=1&unit=c&lang=en-us","Link":"http://www.accuweather.com/en/ng/abuja/254085/daily-weather-forecast/254085?day=1&unit=c&lang=en-us"}]}', '[{"LocalObservationDateTime":"2019-12-30T08:12:00+01:00","EpochTime":1577689920,"WeatherText":"Sunny","WeatherIcon":1,"HasPrecipitation":false,"PrecipitationType":null,"IsDayTime":true,"Temperature":{"Metric":{"Value":19.5,"Unit":"C","UnitType":17},"Imperial":{"Value":67.0,"Unit":"F","UnitType":18}},"MobileLink":"http://m.accuweather.com/en/ng/abuja/254085/current-weather/254085?lang=en-us","Link":"http://www.accuweather.com/en/ng/abuja/254085/current-weather/254085?lang=en-us"}]', '2019-12-30 07:17:54'),
(14, 0, '254085', '{"Headline":{"EffectiveDate":"2020-01-18T07:00:00+01:00","EffectiveEpochDate":1579327200,"Severity":7,"Text":"Mostly sunny this weekend","Category":"","EndDate":null,"EndEpochDate":null,"MobileLink":"http://m.accuweather.com/en/ng/abuja/254085/extended-weather-forecast/254085?unit=c&lang=en-us","Link":"http://www.accuweather.com/en/ng/abuja/254085/daily-weather-forecast/254085?unit=c&lang=en-us"},"DailyForecasts":[{"Date":"2020-01-13T07:00:00+01:00","EpochDate":1578895200,"Sun":{"Rise":"2020-01-13T06:49:00+01:00","EpochRise":1578894540,"Set":"2020-01-13T18:28:00+01:00","EpochSet":1578936480},"Moon":{"Rise":"2020-01-13T21:20:00+01:00","EpochRise":1578946800,"Set":"2020-01-14T10:03:00+01:00","EpochSet":1578992580,"Phase":"WaningGibbous","Age":18},"Temperature":{"Minimum":{"Value":17.9,"Unit":"C","UnitType":17},"Maximum":{"Value":33.2,"Unit":"C","UnitType":17}},"RealFeelTemperature":{"Minimum":{"Value":18.7,"Unit":"C","UnitType":17},"Maximum":{"Value":35.3,"Unit":"C","UnitType":17}},"RealFeelTemperatureShade":{"Minimum":{"Value":18.7,"Unit":"C","UnitType":17},"Maximum":{"Value":32.0,"Unit":"C","UnitType":17}},"HoursOfSun":11.3,"DegreeDaySummary":{"Heating":{"Value":0.0,"Unit":"C","UnitType":17},"Cooling":{"Value":8.0,"Unit":"C","UnitType":17}},"AirAndPollen":[{"Name":"AirQuality","Value":0,"Category":"Good","CategoryValue":1,"Type":"Ozone"},{"Name":"Grass","Value":0,"Category":"Low","CategoryValue":1},{"Name":"Mold","Value":0,"Category":"Low","CategoryValue":1},{"Name":"Ragweed","Value":0,"Category":"Low","CategoryValue":1},{"Name":"Tree","Value":0,"Category":"Low","CategoryValue":1},{"Name":"UVIndex","Value":8,"Category":"Very High","CategoryValue":4}],"Day":{"Icon":1,"IconPhrase":"Sunny","HasPrecipitation":false,"ShortPhrase":"Brilliant sunshine","LongPhrase":"Brilliant sunshine","PrecipitationProbability":0,"ThunderstormProbability":0,"RainProbability":0,"SnowProbability":0,"IceProbability":0,"Wind":{"Speed":{"Value":7.4,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":124,"Localized":"SE","English":"SE"}},"WindGust":{"Speed":{"Value":13.0,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":143,"Localized":"SE","English":"SE"}},"TotalLiquid":{"Value":0.0,"Unit":"mm","UnitType":3},"Rain":{"Value":0.0,"Unit":"mm","UnitType":3},"Snow":{"Value":0.0,"Unit":"cm","UnitType":4},"Ice":{"Value":0.0,"Unit":"mm","UnitType":3},"HoursOfPrecipitation":0.0,"HoursOfRain":0.0,"HoursOfSnow":0.0,"HoursOfIce":0.0,"CloudCover":3},"Night":{"Icon":33,"IconPhrase":"Clear","HasPrecipitation":false,"ShortPhrase":"Clear","LongPhrase":"Clear","PrecipitationProbability":0,"ThunderstormProbability":0,"RainProbability":0,"SnowProbability":0,"IceProbability":0,"Wind":{"Speed":{"Value":5.6,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":216,"Localized":"SW","English":"SW"}},"WindGust":{"Speed":{"Value":9.3,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":233,"Localized":"SW","English":"SW"}},"TotalLiquid":{"Value":0.0,"Unit":"mm","UnitType":3},"Rain":{"Value":0.0,"Unit":"mm","UnitType":3},"Snow":{"Value":0.0,"Unit":"cm","UnitType":4},"Ice":{"Value":0.0,"Unit":"mm","UnitType":3},"HoursOfPrecipitation":0.0,"HoursOfRain":0.0,"HoursOfSnow":0.0,"HoursOfIce":0.0,"CloudCover":0},"Sources":["AccuWeather"],"MobileLink":"http://m.accuweather.com/en/ng/abuja/254085/daily-weather-forecast/254085?day=1&unit=c&lang=en-us","Link":"http://www.accuweather.com/en/ng/abuja/254085/daily-weather-forecast/254085?day=1&unit=c&lang=en-us"}]}', '[{"LocalObservationDateTime":"2020-01-13T07:11:00+01:00","EpochTime":1578895860,"WeatherText":"Mostly sunny","WeatherIcon":2,"HasPrecipitation":false,"PrecipitationType":null,"IsDayTime":true,"Temperature":{"Metric":{"Value":20.5,"Unit":"C","UnitType":17},"Imperial":{"Value":69.0,"Unit":"F","UnitType":18}},"MobileLink":"http://m.accuweather.com/en/ng/abuja/254085/current-weather/254085?lang=en-us","Link":"http://www.accuweather.com/en/ng/abuja/254085/current-weather/254085?lang=en-us"}]', '2020-01-13 06:27:02'),
(15, 0, '254085', '{"Headline":{"EffectiveDate":"2020-01-18T07:00:00+01:00","EffectiveEpochDate":1579327200,"Severity":4,"Text":"Pleasant Saturday","Category":"","EndDate":null,"EndEpochDate":null,"MobileLink":"http://m.accuweather.com/en/ng/abuja/254085/extended-weather-forecast/254085?unit=c&lang=en-us","Link":"http://www.accuweather.com/en/ng/abuja/254085/daily-weather-forecast/254085?unit=c&lang=en-us"},"DailyForecasts":[{"Date":"2020-01-14T07:00:00+01:00","EpochDate":1578981600,"Sun":{"Rise":"2020-01-14T06:50:00+01:00","EpochRise":1578981000,"Set":"2020-01-14T18:29:00+01:00","EpochSet":1579022940},"Moon":{"Rise":"2020-01-14T22:16:00+01:00","EpochRise":1579036560,"Set":"2020-01-15T10:51:00+01:00","EpochSet":1579081860,"Phase":"WaningGibbous","Age":19},"Temperature":{"Minimum":{"Value":20.0,"Unit":"C","UnitType":17},"Maximum":{"Value":33.1,"Unit":"C","UnitType":17}},"RealFeelTemperature":{"Minimum":{"Value":19.6,"Unit":"C","UnitType":17},"Maximum":{"Value":34.3,"Unit":"C","UnitType":17}},"RealFeelTemperatureShade":{"Minimum":{"Value":19.6,"Unit":"C","UnitType":17},"Maximum":{"Value":31.8,"Unit":"C","UnitType":17}},"HoursOfSun":11.6,"DegreeDaySummary":{"Heating":{"Value":0.0,"Unit":"C","UnitType":17},"Cooling":{"Value":9.0,"Unit":"C","UnitType":17}},"AirAndPollen":[{"Name":"AirQuality","Value":0,"Category":"Good","CategoryValue":1,"Type":"Ozone"},{"Name":"Grass","Value":0,"Category":"Low","CategoryValue":1},{"Name":"Mold","Value":0,"Category":"Low","CategoryValue":1},{"Name":"Ragweed","Value":0,"Category":"Low","CategoryValue":1},{"Name":"Tree","Value":0,"Category":"Low","CategoryValue":1},{"Name":"UVIndex","Value":8,"Category":"Very High","CategoryValue":4}],"Day":{"Icon":1,"IconPhrase":"Sunny","HasPrecipitation":false,"ShortPhrase":"Plenty of sunshine","LongPhrase":"Plenty of sunshine","PrecipitationProbability":0,"ThunderstormProbability":0,"RainProbability":0,"SnowProbability":0,"IceProbability":0,"Wind":{"Speed":{"Value":9.3,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":139,"Localized":"SE","English":"SE"}},"WindGust":{"Speed":{"Value":14.8,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":118,"Localized":"ESE","English":"ESE"}},"TotalLiquid":{"Value":0.0,"Unit":"mm","UnitType":3},"Rain":{"Value":0.0,"Unit":"mm","UnitType":3},"Snow":{"Value":0.0,"Unit":"cm","UnitType":4},"Ice":{"Value":0.0,"Unit":"mm","UnitType":3},"HoursOfPrecipitation":0.0,"HoursOfRain":0.0,"HoursOfSnow":0.0,"HoursOfIce":0.0,"CloudCover":0},"Night":{"Icon":33,"IconPhrase":"Clear","HasPrecipitation":false,"ShortPhrase":"Clear","LongPhrase":"Clear","PrecipitationProbability":0,"ThunderstormProbability":0,"RainProbability":0,"SnowProbability":0,"IceProbability":0,"Wind":{"Speed":{"Value":7.4,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":110,"Localized":"ESE","English":"ESE"}},"WindGust":{"Speed":{"Value":13.0,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":52,"Localized":"NE","English":"NE"}},"TotalLiquid":{"Value":0.0,"Unit":"mm","UnitType":3},"Rain":{"Value":0.0,"Unit":"mm","UnitType":3},"Snow":{"Value":0.0,"Unit":"cm","UnitType":4},"Ice":{"Value":0.0,"Unit":"mm","UnitType":3},"HoursOfPrecipitation":0.0,"HoursOfRain":0.0,"HoursOfSnow":0.0,"HoursOfIce":0.0,"CloudCover":0},"Sources":["AccuWeather"],"MobileLink":"http://m.accuweather.com/en/ng/abuja/254085/daily-weather-forecast/254085?day=1&unit=c&lang=en-us","Link":"http://www.accuweather.com/en/ng/abuja/254085/daily-weather-forecast/254085?day=1&unit=c&lang=en-us"}]}', '[{"LocalObservationDateTime":"2020-01-14T07:41:00+01:00","EpochTime":1578984060,"WeatherText":"Sunny","WeatherIcon":1,"HasPrecipitation":false,"PrecipitationType":null,"IsDayTime":true,"Temperature":{"Metric":{"Value":18.9,"Unit":"C","UnitType":17},"Imperial":{"Value":66.0,"Unit":"F","UnitType":18}},"MobileLink":"http://m.accuweather.com/en/ng/abuja/254085/current-weather/254085?lang=en-us","Link":"http://www.accuweather.com/en/ng/abuja/254085/current-weather/254085?lang=en-us"}]', '2020-01-14 06:56:50'),
(16, 0, '254085', '{"Headline":{"EffectiveDate":"2020-01-18T07:00:00+01:00","EffectiveEpochDate":1579327200,"Severity":4,"Text":"Pleasant Saturday","Category":"","EndDate":null,"EndEpochDate":null,"MobileLink":"http://m.accuweather.com/en/ng/abuja/254085/extended-weather-forecast/254085?unit=c&lang=en-us","Link":"http://www.accuweather.com/en/ng/abuja/254085/daily-weather-forecast/254085?unit=c&lang=en-us"},"DailyForecasts":[{"Date":"2020-01-14T07:00:00+01:00","EpochDate":1578981600,"Sun":{"Rise":"2020-01-14T06:50:00+01:00","EpochRise":1578981000,"Set":"2020-01-14T18:29:00+01:00","EpochSet":1579022940},"Moon":{"Rise":"2020-01-14T22:16:00+01:00","EpochRise":1579036560,"Set":"2020-01-14T10:03:00+01:00","EpochSet":1578992580,"Phase":"WaningGibbous","Age":19},"Temperature":{"Minimum":{"Value":20.0,"Unit":"C","UnitType":17},"Maximum":{"Value":33.1,"Unit":"C","UnitType":17}},"RealFeelTemperature":{"Minimum":{"Value":19.6,"Unit":"C","UnitType":17},"Maximum":{"Value":34.3,"Unit":"C","UnitType":17}},"RealFeelTemperatureShade":{"Minimum":{"Value":19.6,"Unit":"C","UnitType":17},"Maximum":{"Value":31.8,"Unit":"C","UnitType":17}},"HoursOfSun":11.6,"DegreeDaySummary":{"Heating":{"Value":0.0,"Unit":"C","UnitType":17},"Cooling":{"Value":9.0,"Unit":"C","UnitType":17}},"AirAndPollen":[{"Name":"AirQuality","Value":0,"Category":"Good","CategoryValue":1,"Type":"Ozone"},{"Name":"Grass","Value":0,"Category":"Low","CategoryValue":1},{"Name":"Mold","Value":0,"Category":"Low","CategoryValue":1},{"Name":"Ragweed","Value":0,"Category":"Low","CategoryValue":1},{"Name":"Tree","Value":0,"Category":"Low","CategoryValue":1},{"Name":"UVIndex","Value":8,"Category":"Very High","CategoryValue":4}],"Day":{"Icon":1,"IconPhrase":"Sunny","HasPrecipitation":false,"ShortPhrase":"Plenty of sunshine","LongPhrase":"Plenty of sunshine","PrecipitationProbability":0,"ThunderstormProbability":0,"RainProbability":0,"SnowProbability":0,"IceProbability":0,"Wind":{"Speed":{"Value":9.3,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":139,"Localized":"SE","English":"SE"}},"WindGust":{"Speed":{"Value":14.8,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":118,"Localized":"ESE","English":"ESE"}},"TotalLiquid":{"Value":0.0,"Unit":"mm","UnitType":3},"Rain":{"Value":0.0,"Unit":"mm","UnitType":3},"Snow":{"Value":0.0,"Unit":"cm","UnitType":4},"Ice":{"Value":0.0,"Unit":"mm","UnitType":3},"HoursOfPrecipitation":0.0,"HoursOfRain":0.0,"HoursOfSnow":0.0,"HoursOfIce":0.0,"CloudCover":0},"Night":{"Icon":33,"IconPhrase":"Clear","HasPrecipitation":false,"ShortPhrase":"Clear","LongPhrase":"Clear","PrecipitationProbability":0,"ThunderstormProbability":0,"RainProbability":0,"SnowProbability":0,"IceProbability":0,"Wind":{"Speed":{"Value":7.4,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":110,"Localized":"ESE","English":"ESE"}},"WindGust":{"Speed":{"Value":13.0,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":52,"Localized":"NE","English":"NE"}},"TotalLiquid":{"Value":0.0,"Unit":"mm","UnitType":3},"Rain":{"Value":0.0,"Unit":"mm","UnitType":3},"Snow":{"Value":0.0,"Unit":"cm","UnitType":4},"Ice":{"Value":0.0,"Unit":"mm","UnitType":3},"HoursOfPrecipitation":0.0,"HoursOfRain":0.0,"HoursOfSnow":0.0,"HoursOfIce":0.0,"CloudCover":0},"Sources":["AccuWeather"],"MobileLink":"http://m.accuweather.com/en/ng/abuja/254085/daily-weather-forecast/254085?day=1&unit=c&lang=en-us","Link":"http://www.accuweather.com/en/ng/abuja/254085/daily-weather-forecast/254085?day=1&unit=c&lang=en-us"}]}', '[{"LocalObservationDateTime":"2020-01-14T07:41:00+01:00","EpochTime":1578984060,"WeatherText":"Sunny","WeatherIcon":1,"HasPrecipitation":false,"PrecipitationType":null,"IsDayTime":true,"Temperature":{"Metric":{"Value":18.9,"Unit":"C","UnitType":17},"Imperial":{"Value":66.0,"Unit":"F","UnitType":18}},"MobileLink":"http://m.accuweather.com/en/ng/abuja/254085/current-weather/254085?lang=en-us","Link":"http://www.accuweather.com/en/ng/abuja/254085/current-weather/254085?lang=en-us"}]', '2020-01-14 06:56:50'),
(17, 0, '254085', '{"Headline":{"EffectiveDate":"2020-01-25T07:00:00+01:00","EffectiveEpochDate":1579932000,"Severity":4,"Text":"Pleasant Saturday","Category":"","EndDate":null,"EndEpochDate":null,"MobileLink":"http://m.accuweather.com/en/ng/abuja/254085/extended-weather-forecast/254085?unit=c&lang=en-us","Link":"http://www.accuweather.com/en/ng/abuja/254085/daily-weather-forecast/254085?unit=c&lang=en-us"},"DailyForecasts":[{"Date":"2020-01-23T07:00:00+01:00","EpochDate":1579759200,"Sun":{"Rise":"2020-01-23T06:51:00+01:00","EpochRise":1579758660,"Set":"2020-01-23T18:33:00+01:00","EpochSet":1579800780},"Moon":{"Rise":"2020-01-23T05:34:00+01:00","EpochRise":1579754040,"Set":"2020-01-23T17:27:00+01:00","EpochSet":1579796820,"Phase":"WaningCrescent","Age":28},"Temperature":{"Minimum":{"Value":18.7,"Unit":"C","UnitType":17},"Maximum":{"Value":32.8,"Unit":"C","UnitType":17}},"RealFeelTemperature":{"Minimum":{"Value":17.8,"Unit":"C","UnitType":17},"Maximum":{"Value":33.4,"Unit":"C","UnitType":17}},"RealFeelTemperatureShade":{"Minimum":{"Value":17.8,"Unit":"C","UnitType":17},"Maximum":{"Value":31.4,"Unit":"C","UnitType":17}},"HoursOfSun":3.3,"DegreeDaySummary":{"Heating":{"Value":0.0,"Unit":"C","UnitType":17},"Cooling":{"Value":8.0,"Unit":"C","UnitType":17}},"AirAndPollen":[{"Name":"AirQuality","Value":0,"Category":"Good","CategoryValue":1,"Type":"Ozone"},{"Name":"Grass","Value":0,"Category":"Low","CategoryValue":1},{"Name":"Mold","Value":0,"Category":"Low","CategoryValue":1},{"Name":"Ragweed","Value":0,"Category":"Low","CategoryValue":1},{"Name":"Tree","Value":0,"Category":"Low","CategoryValue":1},{"Name":"UVIndex","Value":7,"Category":"High","CategoryValue":3}],"Day":{"Icon":4,"IconPhrase":"Intermittent clouds","HasPrecipitation":false,"ShortPhrase":"Sun and areas of high clouds","LongPhrase":"Sun and areas of high clouds","PrecipitationProbability":0,"ThunderstormProbability":0,"RainProbability":0,"SnowProbability":0,"IceProbability":0,"Wind":{"Speed":{"Value":13.0,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":96,"Localized":"E","English":"E"}},"WindGust":{"Speed":{"Value":18.5,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":72,"Localized":"ENE","English":"ENE"}},"TotalLiquid":{"Value":0.0,"Unit":"mm","UnitType":3},"Rain":{"Value":0.0,"Unit":"mm","UnitType":3},"Snow":{"Value":0.0,"Unit":"cm","UnitType":4},"Ice":{"Value":0.0,"Unit":"mm","UnitType":3},"HoursOfPrecipitation":0.0,"HoursOfRain":0.0,"HoursOfSnow":0.0,"HoursOfIce":0.0,"CloudCover":76},"Night":{"Icon":35,"IconPhrase":"Partly cloudy","HasPrecipitation":false,"ShortPhrase":"Partly cloudy","LongPhrase":"Partly cloudy","PrecipitationProbability":0,"ThunderstormProbability":0,"RainProbability":0,"SnowProbability":0,"IceProbability":0,"Wind":{"Speed":{"Value":13.0,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":65,"Localized":"ENE","English":"ENE"}},"WindGust":{"Speed":{"Value":20.4,"Unit":"km/h","UnitType":7},"Direction":{"Degrees":51,"Localized":"NE","English":"NE"}},"TotalLiquid":{"Value":0.0,"Unit":"mm","UnitType":3},"Rain":{"Value":0.0,"Unit":"mm","UnitType":3},"Snow":{"Value":0.0,"Unit":"cm","UnitType":4},"Ice":{"Value":0.0,"Unit":"mm","UnitType":3},"HoursOfPrecipitation":0.0,"HoursOfRain":0.0,"HoursOfSnow":0.0,"HoursOfIce":0.0,"CloudCover":39},"Sources":["AccuWeather"],"MobileLink":"http://m.accuweather.com/en/ng/abuja/254085/daily-weather-forecast/254085?day=1&unit=c&lang=en-us","Link":"http://www.accuweather.com/en/ng/abuja/254085/daily-weather-forecast/254085?day=1&unit=c&lang=en-us"}]}', '[{"LocalObservationDateTime":"2020-01-23T06:56:00+01:00","EpochTime":1579758960,"WeatherText":"Partly sunny","WeatherIcon":3,"HasPrecipitation":false,"PrecipitationType":null,"IsDayTime":true,"Temperature":{"Metric":{"Value":20.0,"Unit":"C","UnitType":17},"Imperial":{"Value":68.0,"Unit":"F","UnitType":18}},"MobileLink":"http://m.accuweather.com/en/ng/abuja/254085/current-weather/254085?lang=en-us","Link":"http://www.accuweather.com/en/ng/abuja/254085/current-weather/254085?lang=en-us"}]', '2020-01-23 06:02:55');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_agent_document`
--
ALTER TABLE `t_agent_document`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cust_id` (`cust_id`);

--
-- Indexes for table `t_agent_package`
--
ALTER TABLE `t_agent_package`
  ADD PRIMARY KEY (`id`),
  ADD KEY `status` (`status`);

--
-- Indexes for table `t_attribute`
--
ALTER TABLE `t_attribute`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_banner`
--
ALTER TABLE `t_banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_buyer_image`
--
ALTER TABLE `t_buyer_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_buyer_profile`
--
ALTER TABLE `t_buyer_profile`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `business_name` (`business_name`),
  ADD KEY `cust_id` (`cust_id`);

--
-- Indexes for table `t_category`
--
ALTER TABLE `t_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_name` (`category_name_en`),
  ADD KEY `parent_id` (`parent_id`),
  ADD KEY `category_name_fr` (`category_name_fr`);

--
-- Indexes for table `t_cat_uom_relation`
--
ALTER TABLE `t_cat_uom_relation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_cms`
--
ALTER TABLE `t_cms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_customer`
--
ALTER TABLE `t_customer`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `username_2` (`username`),
  ADD KEY `email_2` (`email`),
  ADD KEY `token_id` (`token_id`),
  ADD KEY `location_key` (`location_key`);

--
-- Indexes for table `t_enquiry`
--
ALTER TABLE `t_enquiry`
  ADD PRIMARY KEY (`id`),
  ADD KEY `to_id` (`to_id`),
  ADD KEY `from_id` (`from_id`),
  ADD KEY `seller_item_id` (`seller_item_id`),
  ADD KEY `date` (`date`);

--
-- Indexes for table `t_enquiry_image`
--
ALTER TABLE `t_enquiry_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `enquiry_thread_id` (`enquiry_thread_id`);

--
-- Indexes for table `t_enquiry_thread`
--
ALTER TABLE `t_enquiry_thread`
  ADD PRIMARY KEY (`id`),
  ADD KEY `from_customer_id` (`from_customer_id`),
  ADD KEY `date` (`date`),
  ADD KEY `to_customer_id` (`to_customer_id`),
  ADD KEY `enquiry_id` (`enquiry_id`);

--
-- Indexes for table `t_forum`
--
ALTER TABLE `t_forum`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_item`
--
ALTER TABLE `t_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_local_supplier`
--
ALTER TABLE `t_local_supplier`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `supplier_name` (`supplier_name`);

--
-- Indexes for table `t_logistic`
--
ALTER TABLE `t_logistic`
  ADD PRIMARY KEY (`id`),
  ADD KEY `logistic_partner_id` (`logistic_partner_id`);

--
-- Indexes for table `t_logistic_user`
--
ALTER TABLE `t_logistic_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`),
  ADD KEY `email` (`email`);

--
-- Indexes for table `t_news`
--
ALTER TABLE `t_news`
  ADD PRIMARY KEY (`news_id`),
  ADD KEY `title` (`title`),
  ADD KEY `date` (`date`);

--
-- Indexes for table `t_news_image`
--
ALTER TABLE `t_news_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `news_id` (`news_id`);

--
-- Indexes for table `t_notification`
--
ALTER TABLE `t_notification`
  ADD PRIMARY KEY (`notification_id`),
  ADD KEY `to_id` (`to_id`),
  ADD KEY `from_id` (`from_id`),
  ADD KEY `created_date` (`created_date`);

--
-- Indexes for table `t_order`
--
ALTER TABLE `t_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_order_log`
--
ALTER TABLE `t_order_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `proposal_id` (`proposal_id`),
  ADD KEY `transction_id` (`transction_id`),
  ADD KEY `status` (`status`),
  ADD KEY `created_date` (`created_date`);

--
-- Indexes for table `t_proposal`
--
ALTER TABLE `t_proposal`
  ADD PRIMARY KEY (`id`),
  ADD KEY `from_id` (`from_id`),
  ADD KEY `to_id` (`to_id`),
  ADD KEY `title` (`title`),
  ADD KEY `refference_id` (`refference_id`),
  ADD KEY `refference_type` (`refference_type`);

--
-- Indexes for table `t_provider_service_item`
--
ALTER TABLE `t_provider_service_item`
  ADD PRIMARY KEY (`provider_service_item_id`),
  ADD KEY `provider_id` (`provider_id`),
  ADD KEY `service_item_id` (`service_item_id`),
  ADD KEY `uom_id` (`uom_id`),
  ADD KEY `min_rate` (`min_rate`),
  ADD KEY `max_rate` (`max_rate`),
  ADD KEY `active` (`active`);

--
-- Indexes for table `t_provider_service_item_image`
--
ALTER TABLE `t_provider_service_item_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `provider_service_item_id` (`provider_service_item_id`),
  ADD KEY `active` (`active`);

--
-- Indexes for table `t_quotation`
--
ALTER TABLE `t_quotation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_rate_exchange`
--
ALTER TABLE `t_rate_exchange`
  ADD UNIQUE KEY `base_currency` (`base_currency`,`to_currency`);

--
-- Indexes for table `t_report_incident`
--
ALTER TABLE `t_report_incident`
  ADD PRIMARY KEY (`id`),
  ADD KEY `seller_customer_id` (`to_id`),
  ADD KEY `from_id` (`from_id`),
  ADD KEY `ref_id` (`ref_id`);

--
-- Indexes for table `t_request_for_quotation`
--
ALTER TABLE `t_request_for_quotation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `to_id` (`to_id`),
  ADD KEY `from_id` (`from_id`),
  ADD KEY `seller_item_id` (`seller_item_id`),
  ADD KEY `date` (`date`);

--
-- Indexes for table `t_rfq`
--
ALTER TABLE `t_rfq`
  ADD PRIMARY KEY (`rfq_id`);

--
-- Indexes for table `t_rfq_image`
--
ALTER TABLE `t_rfq_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `req_id` (`req_id`);

--
-- Indexes for table `t_rfq_thread`
--
ALTER TABLE `t_rfq_thread`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rfq_id` (`rfq_id`),
  ADD KEY `to_customer_id` (`to_customer_id`),
  ADD KEY `from_customer_id` (`from_customer_id`),
  ADD KEY `date` (`date`);

--
-- Indexes for table `t_rfq_thread_image`
--
ALTER TABLE `t_rfq_thread_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rfq_thread_id` (`rfq_thread_id`);

--
-- Indexes for table `t_seller_image`
--
ALTER TABLE `t_seller_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_seller_item`
--
ALTER TABLE `t_seller_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `item_id` (`item_id`),
  ADD KEY `species_id` (`species_id`);

--
-- Indexes for table `t_seller_item_image`
--
ALTER TABLE `t_seller_item_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `seller_item_id` (`seller_item_id`) USING BTREE;

--
-- Indexes for table `t_seller_profile`
--
ALTER TABLE `t_seller_profile`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `business_name` (`business_name`),
  ADD KEY `cust_id` (`cust_id`),
  ADD KEY `business_name_2` (`business_name`),
  ADD KEY `contact_name` (`contact_name`),
  ADD KEY `lat` (`lat`),
  ADD KEY `long` (`long`);

--
-- Indexes for table `t_service_category`
--
ALTER TABLE `t_service_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `service_category_name` (`service_category_name`),
  ADD KEY `parent_id` (`parent_id`),
  ADD KEY `slug` (`slug`);

--
-- Indexes for table `t_service_cat_uom_relation`
--
ALTER TABLE `t_service_cat_uom_relation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_service_item`
--
ALTER TABLE `t_service_item`
  ADD PRIMARY KEY (`service_item_id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `t_setting`
--
ALTER TABLE `t_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_species`
--
ALTER TABLE `t_species`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_uom`
--
ALTER TABLE `t_uom`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_user`
--
ALTER TABLE `t_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `user_name_2` (`user_name`),
  ADD UNIQUE KEY `mobile` (`mobile`),
  ADD KEY `email_2` (`email`),
  ADD KEY `user_name` (`user_name`),
  ADD KEY `Name` (`Name`);

--
-- Indexes for table `t_variance`
--
ALTER TABLE `t_variance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_weather`
--
ALTER TABLE `t_weather`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `location_key` (`location_key`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t_agent_document`
--
ALTER TABLE `t_agent_document`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `t_agent_package`
--
ALTER TABLE `t_agent_package`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_attribute`
--
ALTER TABLE `t_attribute`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `t_banner`
--
ALTER TABLE `t_banner`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_buyer_image`
--
ALTER TABLE `t_buyer_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_buyer_profile`
--
ALTER TABLE `t_buyer_profile`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `t_category`
--
ALTER TABLE `t_category`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;
--
-- AUTO_INCREMENT for table `t_cat_uom_relation`
--
ALTER TABLE `t_cat_uom_relation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;
--
-- AUTO_INCREMENT for table `t_cms`
--
ALTER TABLE `t_cms`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `t_customer`
--
ALTER TABLE `t_customer`
  MODIFY `id` bigint(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `t_enquiry`
--
ALTER TABLE `t_enquiry`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `t_enquiry_image`
--
ALTER TABLE `t_enquiry_image`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_enquiry_thread`
--
ALTER TABLE `t_enquiry_thread`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `t_forum`
--
ALTER TABLE `t_forum`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `t_item`
--
ALTER TABLE `t_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT for table `t_local_supplier`
--
ALTER TABLE `t_local_supplier`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `t_logistic`
--
ALTER TABLE `t_logistic`
  MODIFY `id` bigint(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `t_logistic_user`
--
ALTER TABLE `t_logistic_user`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `t_news`
--
ALTER TABLE `t_news`
  MODIFY `news_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_news_image`
--
ALTER TABLE `t_news_image`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_notification`
--
ALTER TABLE `t_notification`
  MODIFY `notification_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `t_order`
--
ALTER TABLE `t_order`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `t_order_log`
--
ALTER TABLE `t_order_log`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `t_proposal`
--
ALTER TABLE `t_proposal`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `t_provider_service_item`
--
ALTER TABLE `t_provider_service_item`
  MODIFY `provider_service_item_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `t_provider_service_item_image`
--
ALTER TABLE `t_provider_service_item_image`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `t_quotation`
--
ALTER TABLE `t_quotation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_report_incident`
--
ALTER TABLE `t_report_incident`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_request_for_quotation`
--
ALTER TABLE `t_request_for_quotation`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_rfq`
--
ALTER TABLE `t_rfq`
  MODIFY `rfq_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `t_rfq_image`
--
ALTER TABLE `t_rfq_image`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `t_rfq_thread`
--
ALTER TABLE `t_rfq_thread`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `t_rfq_thread_image`
--
ALTER TABLE `t_rfq_thread_image`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_seller_image`
--
ALTER TABLE `t_seller_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `t_seller_item`
--
ALTER TABLE `t_seller_item`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `t_seller_item_image`
--
ALTER TABLE `t_seller_item_image`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `t_seller_profile`
--
ALTER TABLE `t_seller_profile`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `t_service_category`
--
ALTER TABLE `t_service_category`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `t_service_cat_uom_relation`
--
ALTER TABLE `t_service_cat_uom_relation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `t_service_item`
--
ALTER TABLE `t_service_item`
  MODIFY `service_item_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `t_setting`
--
ALTER TABLE `t_setting`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `t_species`
--
ALTER TABLE `t_species`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=137;
--
-- AUTO_INCREMENT for table `t_uom`
--
ALTER TABLE `t_uom`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `t_user`
--
ALTER TABLE `t_user`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `t_variance`
--
ALTER TABLE `t_variance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `t_weather`
--
ALTER TABLE `t_weather`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
